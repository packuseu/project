#!/bin/bash

set -e

NFQ_DEPLOY_NAME="$2"

PROJECT_ROOT="$(dirname $(dirname $(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)))"
echo -e "Triggering ${NFQ_DEPLOY_NAME} deploy\n\n"

function setPerms {
	mkdir -p $1
	sudo setfacl -R  -m m:rwx -m u:33:rwX -m u:1000:rwX $1
	sudo setfacl -dR -m m:rwx -m u:33:rwX -m u:1000:rwX $1
}

echo -e '\n## Install mage ... '
mkdir -p /tmp/package/tools/mage
cd /tmp/package/tools/mage
composer init -n
composer --no-interaction
composer --no-interaction require 'andres-montanez/magallanes' '^3.4'

echo -e '\n## Setting up permissions ... '
setPerms "${PROJECT_ROOT}/var"
setPerms "${PROJECT_ROOT}/public/media"
setPerms "${PROJECT_ROOT}/shipping_labels"

cd ${PROJECT_ROOT}

SYMFONY_ENV=prod /tmp/package/tools/mage/vendor/bin/mage deploy "${NFQ_DEPLOY_NAME}"
