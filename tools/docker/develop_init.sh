#!/bin/bash

set -e
set -x

PROJECT_ROOT="$(dirname "$(dirname "$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)")")"

echo "PROJECT ROOT: ${PROJECT_ROOT}"
cd "${PROJECT_ROOT}"


function setPerms {
	mkdir -p "$1"
	sudo setfacl -R  -m m:rwx -m u:33:rwX -m u:1000:rwX "$1"
	sudo setfacl -dR -m m:rwx -m u:33:rwX -m u:1000:rwX "$1"
}

echo -e '\n## Setting up permissions ... '
setPerms "${PROJECT_ROOT}/var"
setPerms "${PROJECT_ROOT}/public/media"
setPerms "${PROJECT_ROOT}/shipping_labels"

time composer --no-interaction install

bin/console doctrine:database:create --if-not-exists -n
bin/console doctrine:database:create --env=test --if-not-exists -n
bin/console sylius:install:assets
bin/console sylius:theme:assets:install
bin/console sylius:install:database -n
bin/console sylius:fixtures:load -n
bin/console omni:filter:index
bin/console omni:search:index
bin/console lexik:translations:import

yarn install
yarn run gulp
yarn encore production
