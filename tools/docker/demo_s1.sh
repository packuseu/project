#!/bin/bash

set -e

PROJECT_ROOT="$(dirname $(dirname $(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)))"

function setPerms {
	mkdir -p $1
	sudo setfacl -R  -m m:rwx -m u:33:rwX -m u:1000:rwX $1
	sudo setfacl -dR -m m:rwx -m u:33:rwX -m u:1000:rwX $1
}


echo -e '\n## Setting up permissions ... '
setPerms "${PROJECT_ROOT}/var"
setPerms "${PROJECT_ROOT}/public/media"
setPerms "${PROJECT_ROOT}/shipping_labels"

cd ${PROJECT_ROOT}

SYMFONY_ENV=prod

echo "APP_ENV: prod"

mysql -hmysql -uproject -pproject -Dproject -e "UPDATE sylius_channel SET hostname = '${NFQ_DEPLOY_HOST}', theme_name = 'sylius/bootstrap-child-theme-product-v3' WHERE code = 'popieriaus_bankas'"

bin/console cache:clear --env=prod
