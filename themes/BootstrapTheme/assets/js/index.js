/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/* eslint-env browser */

import GLightbox from 'glightbox';
import axios from 'axios';
import bsn from 'bootstrap.native/dist/bootstrap-native-v4';

import SyliusRating from './sylius-rating';
import SyliusToggle from './sylius-toggle';
import SyliusAddToCart from './sylius-add-to-cart';
import SyliusRemoveFromCart from './sylius-remove-from-cart';
import SyliusApiToggle from './sylius-api-toggle';
import SyliusApiLogin from './sylius-api-login';
import initCookies from './cookies';
import SyliusVariantsPrices from './sylius-variants-prices';
import SyliusVariantImages from './sylius-variant-images';
import SyliusProvinceField from './sylius-province-field';
import SyliusAddressBook from './sylius-address-book';
import SyliusLoadableForms from './sylius-loadable-forms';
import OmniParcelMachine from './omni-parcel-machine';

// Global axios settings
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
axios.defaults.headers.post.accept = 'application/json, text/javascript, */*; q=0.01';
axios.defaults.headers.post['X-Requested-With'] = 'XMLHttpRequest';

document.addEventListener('DOMContentLoaded', () => {
  // Filter
  const searchPlaceholder = document.querySelector('#search').getAttribute('placeholder');
  const criteriaSearchInput = document.querySelector('#criteria_search_value') ? document.querySelector('#criteria_search_value') : null;

  if (criteriaSearchInput) {
    criteriaSearchInput.placeholder = searchPlaceholder;
  }

  // Lightbox
  const glightbox = GLightbox({ selector: 'glightbox' });

  // Add to cart
  document.querySelectorAll('[data-js-add-to-cart="form"]')
    .forEach(el => SyliusAddToCart(el));

  // Omni parcel machine plugin
  document.querySelectorAll('form[name="sylius_checkout_select_shipping"]')
    .forEach(el => OmniParcelMachine(el));

  // Remove from cart
  document.querySelectorAll('[data-js-remove-from-cart-button]')
    .forEach(el => SyliusRemoveFromCart(el));

  // Province field
  SyliusProvinceField();

  // Address book
  const syliusShippingAddress = document.querySelector('[data-js-address-book="sylius-shipping-address"]');
  if (syliusShippingAddress && syliusShippingAddress.querySelector('.dropdown')) {
    SyliusAddressBook(syliusShippingAddress);
  }
  const syliusBillingAddress = document.querySelector('[data-js-address-book="sylius-billing-address"]');
  if (syliusBillingAddress && syliusBillingAddress.querySelector('.dropdown')) {
    SyliusAddressBook(syliusBillingAddress);
  }

  // Variant prices
  SyliusVariantsPrices();

  // Cookies
  initCookies();

  // Star rating
  document.querySelectorAll('[data-js-rating]').forEach((elem) => {
    new SyliusRating(elem, {
      onRate(value) {
        document.querySelector(`#sylius_product_review_rating_${value - 1}`).checked = true;
      },
    });
  });

  // Toggle and login from checkout
  if (document.querySelector('[data-js-login]')) {
    SyliusApiToggle(document.querySelector('[data-js-login="email"]'));
    SyliusApiLogin(document.querySelector('[data-js-login]'));
  }

  // Toggle billing address on the checkout page
  document.querySelectorAll('[data-js-toggle]').forEach(elem => new SyliusToggle(elem));

  // Product images for variants
  if (document.querySelector('[data-variant-options], [data-variant-code]')) { new SyliusVariantImages(); }

  // Loadable forms
  SyliusLoadableForms();
});

if (document.getElementById('lead-catcher')) {
  var leadCatcherShown = false;
  var check = 0;
  var leadCatcher = new bsn.Modal('#lead-catcher', { backdrop: true });

  window.setInterval(checkLeadCatcher, 2000);

  function checkLeadCatcher() {
    if (leadCatcherShown || check < 60) {
      check++;

      return;
    }

    leadCatcherShown = true;
    leadCatcher.show();
  }
}

// main navbar
var topElements = document.getElementsByClassName('top-main-nav-item');
var toggableElements = document.getElementsByClassName('nav-right-container');
var searchForm = document.getElementById('productSearch');

Array.prototype.forEach.call(topElements, function (topElement, index) {
  topElement.addEventListener('mouseover', function () {
    Array.prototype.forEach.call(toggableElements, function (toggableElement, index) {
      if (toggableElement.getAttribute('data-node') == topElement.getAttribute('data-node')) {
        toggableElement.classList.remove('hidden');
      } else {
        toggableElement.classList.add('hidden');
      }
    });
  });
});
