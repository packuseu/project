// Main scripts file
import './js/index';

// Main styles file
import './scss/index.scss';

// Images
/*import './media/logo.svg';
import './media/product-placeholder.png';*/

// Font awesome icons
import './js/fontawesome';
