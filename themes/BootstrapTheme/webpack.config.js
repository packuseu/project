const Encore = require('@symfony/webpack-encore');

Encore.configureRuntimeEnvironment('dev');

Encore
  .setOutputPath('public/bootstrap-theme')
  .setPublicPath('/bootstrap-theme')
  .addEntry('app', './themes/BootstrapTheme/assets/app.js')
    .copyFiles({
        from: './themes/BootstrapTheme/assets/media',
        to: 'images/[path][name].[ext]',
    })
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSassLoader()
  .enableSourceMaps(!Encore.isProduction());

const config = Encore.getWebpackConfig();
config.name = 'bootstrapTheme';

module.exports = config;
