import Swiper from 'swiper';
import 'swiper/css'

var mySwiper = new Swiper ('.swiper-container', {
  loop: true,
  slidesPerView: 'auto',
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
