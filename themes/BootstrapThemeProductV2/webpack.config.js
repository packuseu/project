const Encore = require('@symfony/webpack-encore');

Encore.configureRuntimeEnvironment('dev');

Encore
  .setOutputPath('public/bootstrap-theme-product-v2')
  .setPublicPath('/bootstrap-theme-product-v2')
  .addEntry('appProductV2', './themes/BootstrapThemeProductV2/assets/appProductV2.js')
    .copyFiles({
        from: './themes/BootstrapThemeProductV2/assets/media',
        to: 'images/[path][name].[ext]',
    })
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSassLoader()
  .enableSourceMaps(!Encore.isProduction());

const config = Encore.getWebpackConfig();
config.name = 'bootstrapThemeProductV2';

module.exports = config;
