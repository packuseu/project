document.addEventListener('DOMContentLoaded', () => {
  if (document.querySelector('.js-quantity-step')) {
    document.querySelectorAll('.js-quantity-step').forEach(
      quantityInput => quantityInput.addEventListener('change', (event) => {
        let stepAmount = parseInt(event.currentTarget.getAttribute('data-step'));
        if (isNaN(stepAmount)) {
          return;
        }

        event.currentTarget.value = changeQuantity(event.currentTarget.value, stepAmount);
    }));
  }

  function changeQuantity(quantity, stepAmount) {
    if (quantity < stepAmount) {
      return stepAmount;
    }

    if (quantity % stepAmount === 0) {
      return quantity;
    }

    return Math.floor(quantity / stepAmount) * stepAmount + stepAmount;
  }
})
