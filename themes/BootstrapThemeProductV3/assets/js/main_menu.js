const menuItem = document.querySelectorAll('.js-show-image');

menuItem.forEach(
    function(menuItem) {
        menuItem.addEventListener('mouseover', function() {
            let imageSrc = this.parentElement.getAttribute('data-img');
            document.getElementById('nav-item-image').src = imageSrc;
        });
    }
);
