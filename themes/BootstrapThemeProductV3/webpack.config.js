const Encore = require('@symfony/webpack-encore');

Encore.configureRuntimeEnvironment('dev');

Encore
  .setOutputPath('public/bootstrap-theme-product-v3')
  .setPublicPath('/bootstrap-theme-product-v3')
  .addEntry('appProductV3', './themes/BootstrapThemeProductV3/assets/appProductV3.js')
    .copyFiles({
        from: './themes/BootstrapThemeProductV3/assets/media',
        to: 'images/[path][name].[ext]',
    })
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSassLoader()
  .enableSourceMaps(!Encore.isProduction());

const config = Encore.getWebpackConfig();
config.name = 'bootstrapThemeProductV3';

module.exports = config;
