const Encore = require('@symfony/webpack-encore');

Encore.configureRuntimeEnvironment('dev');

Encore
  .setOutputPath('public/bootstrap-theme-product-v4')
  .setPublicPath('/bootstrap-theme-product-v4')
  .addEntry('appProductV4', './themes/BootstrapThemeProductV4/assets/appProductV4.js')
    .copyFiles({
        from: './themes/BootstrapThemeProductV4/assets/media',
        to: 'images/[path][name].[ext]',
    })
  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableSassLoader()
  .enableSourceMaps(!Encore.isProduction());

const config = Encore.getWebpackConfig();
config.name = 'bootstrapThemeProductV4';

module.exports = config;
