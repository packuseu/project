const handleHamburgerMenu = () => {
  const toggleOpener = document.querySelector('[hamburgerMenuOpen]');
  const toggleCloser = document.querySelector('[hamburgerMenuClose]');
  const mobileMenu = document.querySelector('.category-menu--mobile');
  const mobileMenuActiveClass = 'category-menu--mobile--active';

  toggleOpener.onclick = () => {
    mobileMenu.classList.add(mobileMenuActiveClass);
  };

  toggleCloser.onclick = () => {
    mobileMenu.classList.remove(mobileMenuActiveClass);
  };
};

const handleDesktopMenu = () => {

  if (!document.querySelectorAll('.toplevel-categories__item')[0]) {
    return;
  }

  const categoryBtn = document.querySelector('.category-menu__left__category-btn');
  const categoryDropdown = document.querySelector('.category-dropdown');
  const categoryDropdownActiveCssClass = 'category-dropdown--active';
  const categoryDropdownVisibleCssClass = 'category-dropdown--visible';
  const topLevelCategories = document.querySelectorAll(`[topLevelCategoryId]`);
  const topLevelCategoryId = "topLevelCategoryId";
  const childCategoryGridId = "childCategoryGridId";
  const topLevelCategoryItemActiveCssClass = "toplevel-categories__item--active";
  const childCategoryGridVisibleCssClass = "child-categories__grid--visible";
  const childCategoryGridActiveCssClass = "child-categories__grid--active";
  const transitionFast = 200;

  categoryBtn.onmouseenter = () => {
    categoryDropdown.classList.add(categoryDropdownActiveCssClass);
    setTimeout(() => {
      categoryDropdown.classList.add(categoryDropdownVisibleCssClass);
    }, transitionFast);
  }

  categoryBtn.onmouseleave = () => {
    categoryDropdown.classList.remove(categoryDropdownVisibleCssClass);
    setTimeout(() => {
      categoryDropdown.classList.remove(categoryDropdownActiveCssClass);
    }, transitionFast);
  }

  topLevelCategories.forEach((category) => {
    category.onmouseenter = () => {
      topLevelCategories.forEach((category) =>
        category.classList.remove(topLevelCategoryItemActiveCssClass)
      );
      category.classList.add(topLevelCategoryItemActiveCssClass);

      const categoryId = category.getAttribute(topLevelCategoryId);

      document
        .querySelectorAll(`[${childCategoryGridId}]`)
        .forEach((category) => {
          category.classList.remove(childCategoryGridActiveCssClass);
          setTimeout(() => {
            category.classList.remove(childCategoryGridVisibleCssClass);
          }, transitionFast);
        });

      const childCategory = document.querySelector(
        `[${childCategoryGridId}='${categoryId}']`
      );
      childCategory.classList.add(childCategoryGridActiveCssClass);
      setTimeout(() => {
        childCategory.classList.add(childCategoryGridVisibleCssClass);
      }, transitionFast);
    };
  });
}

export default function initMenuCategories() {
  handleDesktopMenu();
  handleHamburgerMenu();
}
