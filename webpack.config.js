const Encore = require('@symfony/webpack-encore');

const bootstrapTheme = require('./themes/BootstrapTheme/webpack.config');
const bootstrapThemeProductV2 = require('./themes/BootstrapThemeProductV2/webpack.config');
const bootstrapThemeProductV3 = require('./themes/BootstrapThemeProductV3/webpack.config');
const bootstrapThemeProductV4 = require('./themes/BootstrapThemeProductV4/webpack.config');

module.exports = [bootstrapTheme, bootstrapThemeProductV2, bootstrapThemeProductV3, bootstrapThemeProductV4];
