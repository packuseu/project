/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./themes/BootstrapThemeProductV3/assets/js/main_menu.js":
/*!***************************************************************!*\
  !*** ./themes/BootstrapThemeProductV3/assets/js/main_menu.js ***!
  \***************************************************************/
/***/ (() => {

var menuItem = document.querySelectorAll('.js-show-image');
menuItem.forEach(function (menuItem) {
  menuItem.addEventListener('mouseover', function () {
    var imageSrc = this.parentElement.getAttribute('data-img');
    document.getElementById('nav-item-image').src = imageSrc;
  });
});

/***/ }),

/***/ "./themes/BootstrapThemeProductV3/assets/js/product_step_quantity.js":
/*!***************************************************************************!*\
  !*** ./themes/BootstrapThemeProductV3/assets/js/product_step_quantity.js ***!
  \***************************************************************************/
/***/ (() => {

document.addEventListener('DOMContentLoaded', function () {
  if (document.querySelector('.js-quantity-step')) {
    document.querySelectorAll('.js-quantity-step').forEach(function (quantityInput) {
      return quantityInput.addEventListener('change', function (event) {
        var stepAmount = parseInt(event.currentTarget.getAttribute('data-step'));

        if (isNaN(stepAmount)) {
          return;
        }

        event.currentTarget.value = changeQuantity(event.currentTarget.value, stepAmount);
      });
    });
  }

  function changeQuantity(quantity, stepAmount) {
    if (quantity < stepAmount) {
      return stepAmount;
    }

    if (quantity % stepAmount === 0) {
      return quantity;
    }

    return Math.floor(quantity / stepAmount) * stepAmount + stepAmount;
  }
});

/***/ }),

/***/ "./themes/BootstrapThemeProductV3/assets/scss/index.scss":
/*!***************************************************************!*\
  !*** ./themes/BootstrapThemeProductV3/assets/scss/index.scss ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be in strict mode.
(() => {
"use strict";
/*!***************************************************************!*\
  !*** ./themes/BootstrapThemeProductV3/assets/appProductV3.js ***!
  \***************************************************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _js_main_menu__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./js/main_menu */ "./themes/BootstrapThemeProductV3/assets/js/main_menu.js");
/* harmony import */ var _js_main_menu__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_js_main_menu__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_product_step_quantity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/product_step_quantity */ "./themes/BootstrapThemeProductV3/assets/js/product_step_quantity.js");
/* harmony import */ var _js_product_step_quantity__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_js_product_step_quantity__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _scss_index_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scss/index.scss */ "./themes/BootstrapThemeProductV3/assets/scss/index.scss");
// Main scripts file
// import './js';

 // Main styles file


})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwUHJvZHVjdFYzLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLElBQU1BLFFBQVEsR0FBR0MsUUFBUSxDQUFDQyxnQkFBVCxDQUEwQixnQkFBMUIsQ0FBakI7QUFFQUYsUUFBUSxDQUFDRyxPQUFULENBQ0ksVUFBU0gsUUFBVCxFQUFtQjtBQUNmQSxFQUFBQSxRQUFRLENBQUNJLGdCQUFULENBQTBCLFdBQTFCLEVBQXVDLFlBQVc7QUFDOUMsUUFBSUMsUUFBUSxHQUFHLEtBQUtDLGFBQUwsQ0FBbUJDLFlBQW5CLENBQWdDLFVBQWhDLENBQWY7QUFDQU4sSUFBQUEsUUFBUSxDQUFDTyxjQUFULENBQXdCLGdCQUF4QixFQUEwQ0MsR0FBMUMsR0FBZ0RKLFFBQWhEO0FBQ0gsR0FIRDtBQUlILENBTkw7Ozs7Ozs7Ozs7QUNGQUosUUFBUSxDQUFDRyxnQkFBVCxDQUEwQixrQkFBMUIsRUFBOEMsWUFBTTtBQUNsRCxNQUFJSCxRQUFRLENBQUNTLGFBQVQsQ0FBdUIsbUJBQXZCLENBQUosRUFBaUQ7QUFDL0NULElBQUFBLFFBQVEsQ0FBQ0MsZ0JBQVQsQ0FBMEIsbUJBQTFCLEVBQStDQyxPQUEvQyxDQUNFLFVBQUFRLGFBQWE7QUFBQSxhQUFJQSxhQUFhLENBQUNQLGdCQUFkLENBQStCLFFBQS9CLEVBQXlDLFVBQUNRLEtBQUQsRUFBVztBQUNuRSxZQUFJQyxVQUFVLEdBQUdDLFFBQVEsQ0FBQ0YsS0FBSyxDQUFDRyxhQUFOLENBQW9CUixZQUFwQixDQUFpQyxXQUFqQyxDQUFELENBQXpCOztBQUNBLFlBQUlTLEtBQUssQ0FBQ0gsVUFBRCxDQUFULEVBQXVCO0FBQ3JCO0FBQ0Q7O0FBRURELFFBQUFBLEtBQUssQ0FBQ0csYUFBTixDQUFvQkUsS0FBcEIsR0FBNEJDLGNBQWMsQ0FBQ04sS0FBSyxDQUFDRyxhQUFOLENBQW9CRSxLQUFyQixFQUE0QkosVUFBNUIsQ0FBMUM7QUFDSCxPQVBrQixDQUFKO0FBQUEsS0FEZjtBQVNEOztBQUVELFdBQVNLLGNBQVQsQ0FBd0JDLFFBQXhCLEVBQWtDTixVQUFsQyxFQUE4QztBQUM1QyxRQUFJTSxRQUFRLEdBQUdOLFVBQWYsRUFBMkI7QUFDekIsYUFBT0EsVUFBUDtBQUNEOztBQUVELFFBQUlNLFFBQVEsR0FBR04sVUFBWCxLQUEwQixDQUE5QixFQUFpQztBQUMvQixhQUFPTSxRQUFQO0FBQ0Q7O0FBRUQsV0FBT0MsSUFBSSxDQUFDQyxLQUFMLENBQVdGLFFBQVEsR0FBR04sVUFBdEIsSUFBb0NBLFVBQXBDLEdBQWlEQSxVQUF4RDtBQUNEO0FBQ0YsQ0F4QkQ7Ozs7Ozs7Ozs7OztBQ0FBOzs7Ozs7O1VDQUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7VUFFQTtVQUNBOztVQUVBO1VBQ0E7VUFDQTs7Ozs7V0N0QkE7V0FDQTtXQUNBO1dBQ0E7V0FDQTtXQUNBLGlDQUFpQyxXQUFXO1dBQzVDO1dBQ0E7Ozs7O1dDUEE7V0FDQTtXQUNBO1dBQ0E7V0FDQSx5Q0FBeUMsd0NBQXdDO1dBQ2pGO1dBQ0E7V0FDQTs7Ozs7V0NQQTs7Ozs7V0NBQTtXQUNBO1dBQ0E7V0FDQSx1REFBdUQsaUJBQWlCO1dBQ3hFO1dBQ0EsZ0RBQWdELGFBQWE7V0FDN0Q7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTkE7QUFDQTtBQUNBO0NBR0EiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9raXJvLnRlY2gvLi90aGVtZXMvQm9vdHN0cmFwVGhlbWVQcm9kdWN0VjMvYXNzZXRzL2pzL21haW5fbWVudS5qcyIsIndlYnBhY2s6Ly9raXJvLnRlY2gvLi90aGVtZXMvQm9vdHN0cmFwVGhlbWVQcm9kdWN0VjMvYXNzZXRzL2pzL3Byb2R1Y3Rfc3RlcF9xdWFudGl0eS5qcyIsIndlYnBhY2s6Ly9raXJvLnRlY2gvLi90aGVtZXMvQm9vdHN0cmFwVGhlbWVQcm9kdWN0VjMvYXNzZXRzL3Njc3MvaW5kZXguc2NzcyIsIndlYnBhY2s6Ly9raXJvLnRlY2gvd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8va2lyby50ZWNoL3dlYnBhY2svcnVudGltZS9jb21wYXQgZ2V0IGRlZmF1bHQgZXhwb3J0Iiwid2VicGFjazovL2tpcm8udGVjaC93ZWJwYWNrL3J1bnRpbWUvZGVmaW5lIHByb3BlcnR5IGdldHRlcnMiLCJ3ZWJwYWNrOi8va2lyby50ZWNoL3dlYnBhY2svcnVudGltZS9oYXNPd25Qcm9wZXJ0eSBzaG9ydGhhbmQiLCJ3ZWJwYWNrOi8va2lyby50ZWNoL3dlYnBhY2svcnVudGltZS9tYWtlIG5hbWVzcGFjZSBvYmplY3QiLCJ3ZWJwYWNrOi8va2lyby50ZWNoLy4vdGhlbWVzL0Jvb3RzdHJhcFRoZW1lUHJvZHVjdFYzL2Fzc2V0cy9hcHBQcm9kdWN0VjMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgbWVudUl0ZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanMtc2hvdy1pbWFnZScpO1xuXG5tZW51SXRlbS5mb3JFYWNoKFxuICAgIGZ1bmN0aW9uKG1lbnVJdGVtKSB7XG4gICAgICAgIG1lbnVJdGVtLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlb3ZlcicsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgbGV0IGltYWdlU3JjID0gdGhpcy5wYXJlbnRFbGVtZW50LmdldEF0dHJpYnV0ZSgnZGF0YS1pbWcnKTtcbiAgICAgICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCduYXYtaXRlbS1pbWFnZScpLnNyYyA9IGltYWdlU3JjO1xuICAgICAgICB9KTtcbiAgICB9XG4pO1xuIiwiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsICgpID0+IHtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5qcy1xdWFudGl0eS1zdGVwJykpIHtcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuanMtcXVhbnRpdHktc3RlcCcpLmZvckVhY2goXG4gICAgICBxdWFudGl0eUlucHV0ID0+IHF1YW50aXR5SW5wdXQuYWRkRXZlbnRMaXN0ZW5lcignY2hhbmdlJywgKGV2ZW50KSA9PiB7XG4gICAgICAgIGxldCBzdGVwQW1vdW50ID0gcGFyc2VJbnQoZXZlbnQuY3VycmVudFRhcmdldC5nZXRBdHRyaWJ1dGUoJ2RhdGEtc3RlcCcpKTtcbiAgICAgICAgaWYgKGlzTmFOKHN0ZXBBbW91bnQpKSB7XG4gICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgZXZlbnQuY3VycmVudFRhcmdldC52YWx1ZSA9IGNoYW5nZVF1YW50aXR5KGV2ZW50LmN1cnJlbnRUYXJnZXQudmFsdWUsIHN0ZXBBbW91bnQpO1xuICAgIH0pKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGNoYW5nZVF1YW50aXR5KHF1YW50aXR5LCBzdGVwQW1vdW50KSB7XG4gICAgaWYgKHF1YW50aXR5IDwgc3RlcEFtb3VudCkge1xuICAgICAgcmV0dXJuIHN0ZXBBbW91bnQ7XG4gICAgfVxuXG4gICAgaWYgKHF1YW50aXR5ICUgc3RlcEFtb3VudCA9PT0gMCkge1xuICAgICAgcmV0dXJuIHF1YW50aXR5O1xuICAgIH1cblxuICAgIHJldHVybiBNYXRoLmZsb29yKHF1YW50aXR5IC8gc3RlcEFtb3VudCkgKiBzdGVwQW1vdW50ICsgc3RlcEFtb3VudDtcbiAgfVxufSlcbiIsIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpblxuZXhwb3J0IHt9OyIsIi8vIFRoZSBtb2R1bGUgY2FjaGVcbnZhciBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX18gPSB7fTtcblxuLy8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbmZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG5cdHZhciBjYWNoZWRNb2R1bGUgPSBfX3dlYnBhY2tfbW9kdWxlX2NhY2hlX19bbW9kdWxlSWRdO1xuXHRpZiAoY2FjaGVkTW9kdWxlICE9PSB1bmRlZmluZWQpIHtcblx0XHRyZXR1cm4gY2FjaGVkTW9kdWxlLmV4cG9ydHM7XG5cdH1cblx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcblx0dmFyIG1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF0gPSB7XG5cdFx0Ly8gbm8gbW9kdWxlLmlkIG5lZWRlZFxuXHRcdC8vIG5vIG1vZHVsZS5sb2FkZWQgbmVlZGVkXG5cdFx0ZXhwb3J0czoge31cblx0fTtcblxuXHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cblx0X193ZWJwYWNrX21vZHVsZXNfX1ttb2R1bGVJZF0obW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cblx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcblx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xufVxuXG4iLCIvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuX193ZWJwYWNrX3JlcXVpcmVfXy5uID0gKG1vZHVsZSkgPT4ge1xuXHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cblx0XHQoKSA9PiAobW9kdWxlWydkZWZhdWx0J10pIDpcblx0XHQoKSA9PiAobW9kdWxlKTtcblx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgeyBhOiBnZXR0ZXIgfSk7XG5cdHJldHVybiBnZXR0ZXI7XG59OyIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IChleHBvcnRzLCBkZWZpbml0aW9uKSA9PiB7XG5cdGZvcih2YXIga2V5IGluIGRlZmluaXRpb24pIHtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZGVmaW5pdGlvbiwga2V5KSAmJiAhX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIGtleSkpIHtcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBrZXksIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBkZWZpbml0aW9uW2tleV0gfSk7XG5cdFx0fVxuXHR9XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IChvYmosIHByb3ApID0+IChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKSkiLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSAoZXhwb3J0cykgPT4ge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCIvLyBNYWluIHNjcmlwdHMgZmlsZVxuLy8gaW1wb3J0ICcuL2pzJztcbmltcG9ydCAnLi9qcy9tYWluX21lbnUnO1xuaW1wb3J0ICcuL2pzL3Byb2R1Y3Rfc3RlcF9xdWFudGl0eSc7XG5cbi8vIE1haW4gc3R5bGVzIGZpbGVcbmltcG9ydCAnLi9zY3NzL2luZGV4LnNjc3MnO1xuIl0sIm5hbWVzIjpbIm1lbnVJdGVtIiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yQWxsIiwiZm9yRWFjaCIsImFkZEV2ZW50TGlzdGVuZXIiLCJpbWFnZVNyYyIsInBhcmVudEVsZW1lbnQiLCJnZXRBdHRyaWJ1dGUiLCJnZXRFbGVtZW50QnlJZCIsInNyYyIsInF1ZXJ5U2VsZWN0b3IiLCJxdWFudGl0eUlucHV0IiwiZXZlbnQiLCJzdGVwQW1vdW50IiwicGFyc2VJbnQiLCJjdXJyZW50VGFyZ2V0IiwiaXNOYU4iLCJ2YWx1ZSIsImNoYW5nZVF1YW50aXR5IiwicXVhbnRpdHkiLCJNYXRoIiwiZmxvb3IiXSwic291cmNlUm9vdCI6IiJ9