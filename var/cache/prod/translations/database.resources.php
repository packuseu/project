<?php return array (
  0 => 
  array (
    'locale' => 'en',
    'domain' => 'flashes',
  ),
  1 => 
  array (
    'locale' => 'en',
    'domain' => 'FOSOAuthServerBundle',
  ),
  2 => 
  array (
    'locale' => 'en',
    'domain' => 'HWIOAuthBundle',
  ),
  3 => 
  array (
    'locale' => 'en',
    'domain' => 'LexikTranslationBundle',
  ),
  4 => 
  array (
    'locale' => 'en',
    'domain' => 'messages',
  ),
  5 => 
  array (
    'locale' => 'en',
    'domain' => 'pagerfanta',
  ),
  6 => 
  array (
    'locale' => 'en',
    'domain' => 'PayumBundle',
  ),
  7 => 
  array (
    'locale' => 'en',
    'domain' => 'security',
  ),
  8 => 
  array (
    'locale' => 'en',
    'domain' => 'SonataBlockBundle',
  ),
  9 => 
  array (
    'locale' => 'en',
    'domain' => 'SonataCoreBundle',
  ),
  10 => 
  array (
    'locale' => 'en',
    'domain' => 'validators',
  ),
  11 => 
  array (
    'locale' => 'lt',
    'domain' => 'flashes',
  ),
  12 => 
  array (
    'locale' => 'lt',
    'domain' => 'LexikTranslationBundle',
  ),
  13 => 
  array (
    'locale' => 'lt',
    'domain' => 'messages',
  ),
  14 => 
  array (
    'locale' => 'lt',
    'domain' => 'security',
  ),
  15 => 
  array (
    'locale' => 'lt',
    'domain' => 'SonataCoreBundle',
  ),
  16 => 
  array (
    'locale' => 'lt',
    'domain' => 'validators',
  ),
);