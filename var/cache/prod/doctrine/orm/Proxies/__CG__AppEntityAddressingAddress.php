<?php

namespace Proxies\__CG__\App\Entity\Addressing;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Address extends \App\Entity\Addressing\Address implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'houseNumber', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'apartmentNumber', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'vatCode', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'companyCode', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'phoneNumberPrefix', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'phoneNumberWithoutPrefix', 'customer', 'id', 'firstName', 'lastName', 'phoneNumber', 'company', 'countryCode', 'provinceCode', 'provinceName', 'street', 'city', 'postcode', 'createdAt', 'updatedAt'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'houseNumber', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'apartmentNumber', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'vatCode', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'companyCode', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'phoneNumberPrefix', '' . "\0" . 'App\\Entity\\Addressing\\Address' . "\0" . 'phoneNumberWithoutPrefix', 'customer', 'id', 'firstName', 'lastName', 'phoneNumber', 'company', 'countryCode', 'provinceCode', 'provinceName', 'street', 'city', 'postcode', 'createdAt', 'updatedAt'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Address $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getApartmentNumber(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getApartmentNumber', []);

        return parent::getApartmentNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setApartmentNumber(?string $apartmentNumber): \App\Entity\Addressing\Address
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setApartmentNumber', [$apartmentNumber]);

        return parent::setApartmentNumber($apartmentNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getHouseNumber(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getHouseNumber', []);

        return parent::getHouseNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setHouseNumber(?string $houseNumber): \App\Entity\Addressing\Address
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setHouseNumber', [$houseNumber]);

        return parent::setHouseNumber($houseNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getPhoneNumberPrefix(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPhoneNumberPrefix', []);

        return parent::getPhoneNumberPrefix();
    }

    /**
     * {@inheritDoc}
     */
    public function getPhoneNumberWithoutPrefix(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPhoneNumberWithoutPrefix', []);

        return parent::getPhoneNumberWithoutPrefix();
    }

    /**
     * {@inheritDoc}
     */
    public function setPhoneNumberWithoutPrefix(?string $phoneNumberWithoutPrefix): \App\Entity\Addressing\Address
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPhoneNumberWithoutPrefix', [$phoneNumberWithoutPrefix]);

        return parent::setPhoneNumberWithoutPrefix($phoneNumberWithoutPrefix);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompanyCode(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompanyCode', []);

        return parent::getCompanyCode();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompanyCode(?string $companyCode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompanyCode', [$companyCode]);

        return parent::setCompanyCode($companyCode);
    }

    /**
     * {@inheritDoc}
     */
    public function getVatCode(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getVatCode', []);

        return parent::getVatCode();
    }

    /**
     * {@inheritDoc}
     */
    public function setVatCode(?string $vatCode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setVatCode', [$vatCode]);

        return parent::setVatCode($vatCode);
    }

    /**
     * {@inheritDoc}
     */
    public function getCustomer(): ?\Sylius\Component\Customer\Model\CustomerInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCustomer', []);

        return parent::getCustomer();
    }

    /**
     * {@inheritDoc}
     */
    public function setCustomer(?\Sylius\Component\Customer\Model\CustomerInterface $customer): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCustomer', [$customer]);

        parent::setCustomer($customer);
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstName(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFirstName', []);

        return parent::getFirstName();
    }

    /**
     * {@inheritDoc}
     */
    public function setFirstName(?string $firstName): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFirstName', [$firstName]);

        parent::setFirstName($firstName);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastName(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastName', []);

        return parent::getLastName();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastName(?string $lastName): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastName', [$lastName]);

        parent::setLastName($lastName);
    }

    /**
     * {@inheritDoc}
     */
    public function getFullName(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFullName', []);

        return parent::getFullName();
    }

    /**
     * {@inheritDoc}
     */
    public function getPhoneNumber(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPhoneNumber', []);

        return parent::getPhoneNumber();
    }

    /**
     * {@inheritDoc}
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPhoneNumber', [$phoneNumber]);

        parent::setPhoneNumber($phoneNumber);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompany(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompany', []);

        return parent::getCompany();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompany(?string $company): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompany', [$company]);

        parent::setCompany($company);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountryCode(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountryCode', []);

        return parent::getCountryCode();
    }

    /**
     * {@inheritDoc}
     */
    public function setCountryCode(?string $countryCode): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCountryCode', [$countryCode]);

        parent::setCountryCode($countryCode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProvinceCode(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProvinceCode', []);

        return parent::getProvinceCode();
    }

    /**
     * {@inheritDoc}
     */
    public function setProvinceCode(?string $provinceCode): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProvinceCode', [$provinceCode]);

        parent::setProvinceCode($provinceCode);
    }

    /**
     * {@inheritDoc}
     */
    public function getProvinceName(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProvinceName', []);

        return parent::getProvinceName();
    }

    /**
     * {@inheritDoc}
     */
    public function setProvinceName(?string $provinceName): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProvinceName', [$provinceName]);

        parent::setProvinceName($provinceName);
    }

    /**
     * {@inheritDoc}
     */
    public function getStreet(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStreet', []);

        return parent::getStreet();
    }

    /**
     * {@inheritDoc}
     */
    public function setStreet(?string $street): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStreet', [$street]);

        parent::setStreet($street);
    }

    /**
     * {@inheritDoc}
     */
    public function getCity(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCity', []);

        return parent::getCity();
    }

    /**
     * {@inheritDoc}
     */
    public function setCity(?string $city): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCity', [$city]);

        parent::setCity($city);
    }

    /**
     * {@inheritDoc}
     */
    public function getPostcode(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPostcode', []);

        return parent::getPostcode();
    }

    /**
     * {@inheritDoc}
     */
    public function setPostcode(?string $postcode): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPostcode', [$postcode]);

        parent::setPostcode($postcode);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt(): ?\DateTimeInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt(?\DateTimeInterface $createdAt): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', [$createdAt]);

        parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', []);

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', [$updatedAt]);

        parent::setUpdatedAt($updatedAt);
    }

}
