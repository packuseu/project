<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/OmniSyliusSearchPlugin/search_results.html.twig */
class __TwigTemplate_1c599cf7d543eac21decbc474753883a95dca88f4805fb8b85f58961bbe2d7fa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/OmniSyliusSearchPlugin/search_results.html.twig"));

        // line 2
        $macros["pagination"] = $this->macros["pagination"] = $this->loadTemplate("@SyliusShop/Common/Macro/pagination.html.twig", "bundles/OmniSyliusSearchPlugin/search_results.html.twig", 2)->unwrap();
        // line 1
        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "bundles/OmniSyliusSearchPlugin/search_results.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 5
        echo "    ";
        if ((twig_length_filter($this->env, (isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 5, $this->source); })())) > 0)) {
            // line 6
            echo "        <h1 class=\"ui monster section dividing header\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("shop24.ui.results_for"), "html", null, true);
            echo ": \"";
            echo twig_escape_filter($this->env, (isset($context["searchTerm"]) || array_key_exists("searchTerm", $context) ? $context["searchTerm"] : (function () { throw new RuntimeError('Variable "searchTerm" does not exist.', 6, $this->source); })()), "html", null, true);
            echo "\"</h1>
        <div class=\"row mt-5\">
            ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["results"]) || array_key_exists("results", $context) ? $context["results"] : (function () { throw new RuntimeError('Variable "results" does not exist.', 8, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["resource"]) {
                // line 9
                echo "                <div class=\"col-12 col-md-4 col-lg-3\">
                    ";
                // line 10
                $this->loadTemplate("@SyliusShop/Product/_box.html.twig", "bundles/OmniSyliusSearchPlugin/search_results.html.twig", 10)->display(twig_array_merge($context, ["product" => $context["resource"]]));
                // line 11
                echo "                </div>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['resource'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "        </div>
        <div class=\"ui hidden divider\"></div>
        <div class=\"search-result-pagination\">
            ";
            // line 16
            echo twig_call_macro($macros["pagination"], "macro_simple", [(isset($context["pager"]) || array_key_exists("pager", $context) ? $context["pager"] : (function () { throw new RuntimeError('Variable "pager" does not exist.', 16, $this->source); })())], 16, $context, $this->getSourceContext());
            echo "
        </div>
    ";
        } else {
            // line 19
            echo "        <h1 class=\"ui monster section dividing header\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("shop24.ui.results_for_not_found_a"), "html", null, true);
            echo ": \"";
            echo twig_escape_filter($this->env, (isset($context["searchTerm"]) || array_key_exists("searchTerm", $context) ? $context["searchTerm"] : (function () { throw new RuntimeError('Variable "searchTerm" does not exist.', 19, $this->source); })()), "html", null, true);
            echo "\" ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("shop24.ui.results_for_not_found_b"), "html", null, true);
            echo "</h1>
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/OmniSyliusSearchPlugin/search_results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 19,  115 => 16,  110 => 13,  95 => 11,  93 => 10,  90 => 9,  73 => 8,  65 => 6,  62 => 5,  55 => 4,  47 => 1,  45 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}
{% import '@SyliusShop/Common/Macro/pagination.html.twig' as pagination %}

{% block content %}
    {% if results|length > 0 %}
        <h1 class=\"ui monster section dividing header\">{{ 'shop24.ui.results_for'|trans }}: \"{{ searchTerm }}\"</h1>
        <div class=\"row mt-5\">
            {% for resource in results %}
                <div class=\"col-12 col-md-4 col-lg-3\">
                    {% include '@SyliusShop/Product/_box.html.twig' with {'product': resource} %}
                </div>
            {% endfor %}
        </div>
        <div class=\"ui hidden divider\"></div>
        <div class=\"search-result-pagination\">
            {{ pagination.simple(pager) }}
        </div>
    {% else %}
        <h1 class=\"ui monster section dividing header\">{{ 'shop24.ui.results_for_not_found_a'|trans }}: \"{{ searchTerm }}\" {{ 'shop24.ui.results_for_not_found_b'|trans }}</h1>
    {% endif %}
{% endblock %}
", "bundles/OmniSyliusSearchPlugin/search_results.html.twig", "/var/www/html/templates/bundles/OmniSyliusSearchPlugin/search_results.html.twig");
    }
}
