<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusCmsPlugin:Node:create.html.twig */
class __TwigTemplate_080e68eefab71ae235ad06a9605282bbe0a55e3dccb2f1ac5062b20599ad89b4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusAdmin/Crud/create.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusCmsPlugin:Node:create.html.twig"));

        $this->parent = $this->loadTemplate("@SyliusAdmin/Crud/create.html.twig", "OmniSyliusCmsPlugin:Node:create.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <div class=\"ui two column stackable grid\">
        <div class=\"five wide column\">
            ";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_partial_node_tree", ["template" => "@OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig"]));
        echo "
        </div>
        <div class=\"eleven wide column\">
            ";
        // line 9
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 9, $this->source); })()) . ".before_header"), ["resource" => (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 9, $this->source); })())]]);
        echo "

            ";
        // line 11
        $this->loadTemplate("@SyliusAdmin/Crud/Create/_header.html.twig", "OmniSyliusCmsPlugin:Node:create.html.twig", 11)->display($context);
        // line 12
        echo "
            ";
        // line 13
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 13, $this->source); })()) . ".after_header"), ["resource" => (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 13, $this->source); })())]]);
        echo "

            <div class=\"ui segment\">
                ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "request", [], "any", false, false, false, 16), "attributes", [], "any", false, false, false, 16), "get", [0 => "_route"], "method", false, false, false, 16), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 17
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 17, $this->source); })()), "request", [], "any", false, false, false, 17), "attributes", [], "any", false, false, false, 17), "get", [0 => "_route_params"], "method", false, false, false, 17)), "attr" => ["class" => "ui loadable form", "novalidate" => "novalidate"]]);
        echo "
                ";
        // line 18
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 18), "templates", [], "any", false, true, false, 18), "form", [], "any", true, true, false, 18)) {
            // line 19
            echo "                    ";
            $this->loadTemplate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 19, $this->source); })()), "vars", [], "any", false, false, false, 19), "templates", [], "any", false, false, false, 19), "form", [], "any", false, false, false, 19), "OmniSyliusCmsPlugin:Node:create.html.twig", 19)->display($context);
            // line 20
            echo "                ";
        } else {
            // line 21
            echo "                    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), 'widget');
            echo "
                ";
        }
        // line 23
        echo "
                ";
        // line 24
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 24, $this->source); })()) . ".form"), ["resource" => (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 24, $this->source); })())]]);
        echo "

                ";
        // line 26
        $this->loadTemplate("@SyliusUi/Form/Buttons/_create.html.twig", "OmniSyliusCmsPlugin:Node:create.html.twig", 26)->display(twig_array_merge($context, ["paths" => ["cancel" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 26, $this->source); })()), "getRouteName", [0 => "index"], "method", false, false, false, 26), ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 26), "route", [], "any", false, true, false, 26), "parameters", [], "any", true, true, false, 26)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 26), "route", [], "any", false, true, false, 26), "parameters", [], "any", false, false, false, 26), [])) : ([])))]]));
        // line 27
        echo "
                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 28, $this->source); })()), "_token", [], "any", false, false, false, 28), 'row');
        echo "
                ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), 'form_end', ["render_rest" => false]);
        echo "
            </div>

            ";
        // line 32
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 32, $this->source); })()) . ".after_content"), ["resource" => (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 32, $this->source); })())]]);
        echo "
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusCmsPlugin:Node:create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 32,  121 => 29,  117 => 28,  114 => 27,  112 => 26,  107 => 24,  104 => 23,  98 => 21,  95 => 20,  92 => 19,  90 => 18,  86 => 17,  85 => 16,  79 => 13,  76 => 12,  74 => 11,  69 => 9,  63 => 6,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusAdmin/Crud/create.html.twig' %}

{% block content %}
    <div class=\"ui two column stackable grid\">
        <div class=\"five wide column\">
            {{ render(path('omni_sylius_partial_node_tree', {'template': '@OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig'})) }}
        </div>
        <div class=\"eleven wide column\">
            {{ sonata_block_render_event(event_prefix ~ '.before_header', {'resource': resource}) }}

            {% include '@SyliusAdmin/Crud/Create/_header.html.twig' %}

            {{ sonata_block_render_event(event_prefix ~ '.after_header', {'resource': resource}) }}

            <div class=\"ui segment\">
                {{ form_start(form, {'action': path(app.request.attributes.get('_route'),
                    app.request.attributes.get('_route_params')), 'attr': {'class': 'ui loadable form', 'novalidate': 'novalidate'}}) }}
                {% if configuration.vars.templates.form is defined %}
                    {% include configuration.vars.templates.form %}
                {% else %}
                    {{ form_widget(form) }}
                {% endif %}

                {{ sonata_block_render_event(event_prefix ~ '.form', {'resource': resource}) }}

                {% include '@SyliusUi/Form/Buttons/_create.html.twig' with {'paths': {'cancel': path(configuration.getRouteName('index'), configuration.vars.route.parameters|default({}))}} %}

                {{ form_row(form._token) }}
                {{ form_end(form, {'render_rest': false}) }}
            </div>

            {{ sonata_block_render_event(event_prefix ~ '.after_content', {'resource': resource}) }}
        </div>
    </div>
{% endblock %}
", "OmniSyliusCmsPlugin:Node:create.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Node/create.html.twig");
    }
}
