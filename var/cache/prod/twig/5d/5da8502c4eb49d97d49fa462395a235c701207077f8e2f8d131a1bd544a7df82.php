<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Show:_images.html.twig */
class __TwigTemplate_cb51d4065fbccbec2b3c10f0d091e259d9180fbdc5746fe0614d90b93f4e6f6b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Show:_images.html.twig"));

        // line 1
        echo "<div class=\"row mt-3\">
    ";
        // line 2
        $context["i"] = 1;
        // line 3
        echo "    ";
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 3, $this->source); })()), "images", [], "any", false, false, false, 3)) > 1)) {
            // line 4
            echo "    <div class=\"col-lg-2 col-md-3 col-2\">
        ";
            // line 5
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.before_thumbnails", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 5, $this->source); })())]]);
            echo "
        ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_array_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "images", [], "any", false, false, false, 6), function ($__image__) use ($context, $macros) { $context["image"] = $__image__; return ((twig_get_attribute($this->env, $this->source, $context["image"], "type", [], "any", false, false, false, 6) != "right") && (twig_get_attribute($this->env, $this->source, $context["image"], "type", [], "any", false, false, false, 6) != "left")); }));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 7
                echo "        ";
                $context["path"] = (( !(null === twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 7))) ? ($this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 7), "sylius_shop_product_small_thumbnail")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bootstrap-theme/images/product-placeholder.png", "bootstrapTheme")));
                // line 8
                echo "        <div data-js-product-thumbnail ";
                if (((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 8, $this->source); })()) > 8)) {
                    echo "class=\"hidden\"";
                }
                echo ">
        ";
                // line 9
                if ((twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 9, $this->source); })()), "isConfigurable", [], "method", false, false, false, 9) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 9, $this->source); })()), "variants", [], "any", false, false, false, 9)) > 0))) {
                    // line 10
                    echo "            ";
                    $this->loadTemplate("@SyliusShop/Product/Show/_imageVariants.html.twig", "SyliusShopBundle:Product/Show:_images.html.twig", 10)->display($context);
                    // line 11
                    echo "        ";
                }
                // line 12
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 12), "sylius_shop_product_original"), "html", null, true);
                echo "\" class=\"glightbox\">
                <img class=\"img-fluid\" src=\"";
                // line 13
                echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 13, $this->source); })()), "html", null, true);
                echo "\" data-large-thumbnail=\"";
                echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 13), "sylius_shop_product_large_thumbnail"), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 13, $this->source); })()), "name", [], "any", false, false, false, 13), "html", null, true);
                echo "\" />
            </a>
        </div>
        ";
                // line 16
                $context["i"] = ((isset($context["i"]) || array_key_exists("i", $context) ? $context["i"] : (function () { throw new RuntimeError('Variable "i" does not exist.', 16, $this->source); })()) + 1);
                // line 17
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "    </div>
    ";
        }
        // line 20
        echo "
    ";
        // line 21
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 21, $this->source); })()), "imagesByType", [0 => "main"], "method", false, false, false, 21))) {
            // line 22
            echo "        ";
            $context["source_path"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 22, $this->source); })()), "imagesByType", [0 => "main"], "method", false, false, false, 22), "first", [], "any", false, false, false, 22), "path", [], "any", false, false, false, 22);
            // line 23
            echo "        ";
            $context["original_path"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter((isset($context["source_path"]) || array_key_exists("source_path", $context) ? $context["source_path"] : (function () { throw new RuntimeError('Variable "source_path" does not exist.', 23, $this->source); })()), "sylius_shop_product_original");
            // line 24
            echo "        ";
            $context["path"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter((isset($context["source_path"]) || array_key_exists("source_path", $context) ? $context["source_path"] : (function () { throw new RuntimeError('Variable "source_path" does not exist.', 24, $this->source); })()), ((array_key_exists("filter", $context)) ? (_twig_default_filter((isset($context["filter"]) || array_key_exists("filter", $context) ? $context["filter"] : (function () { throw new RuntimeError('Variable "filter" does not exist.', 24, $this->source); })()), "sylius_shop_product_large_thumbnail")) : ("sylius_shop_product_large_thumbnail")));
            // line 25
            echo "    ";
        } elseif (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 25, $this->source); })()), "images", [], "any", false, false, false, 25), "first", [], "any", false, false, false, 25)) {
            // line 26
            echo "        ";
            $context["source_path"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 26, $this->source); })()), "images", [], "any", false, false, false, 26), "first", [], "any", false, false, false, 26), "path", [], "any", false, false, false, 26);
            // line 27
            echo "        ";
            $context["original_path"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter((isset($context["source_path"]) || array_key_exists("source_path", $context) ? $context["source_path"] : (function () { throw new RuntimeError('Variable "source_path" does not exist.', 27, $this->source); })()), "sylius_shop_product_original");
            // line 28
            echo "        ";
            $context["path"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter((isset($context["source_path"]) || array_key_exists("source_path", $context) ? $context["source_path"] : (function () { throw new RuntimeError('Variable "source_path" does not exist.', 28, $this->source); })()), ((array_key_exists("filter", $context)) ? (_twig_default_filter((isset($context["filter"]) || array_key_exists("filter", $context) ? $context["filter"] : (function () { throw new RuntimeError('Variable "filter" does not exist.', 28, $this->source); })()), "sylius_shop_product_large_thumbnail")) : ("sylius_shop_product_large_thumbnail")));
            // line 29
            echo "    ";
        } else {
            // line 30
            echo "        ";
            $context["original_path"] = $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bootstrap-theme/images/product-placeholder.png", "bootstrapTheme");
            // line 31
            echo "        ";
            $context["path"] = (isset($context["original_path"]) || array_key_exists("original_path", $context) ? $context["original_path"] : (function () { throw new RuntimeError('Variable "original_path" does not exist.', 31, $this->source); })());
            // line 32
            echo "    ";
        }
        // line 33
        echo "
    <div class=\"";
        // line 34
        echo (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 34, $this->source); })()), "images", [], "any", false, false, false, 34)) > 1)) ? ("col-lg-10 col-md-9 col-10") : ("col-12"));
        echo "\">
        <div data-product-image=\"";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 35, $this->source); })()), "html", null, true);
        echo "\" data-product-link=\"";
        echo twig_escape_filter($this->env, (isset($context["original_path"]) || array_key_exists("original_path", $context) ? $context["original_path"] : (function () { throw new RuntimeError('Variable "original_path" does not exist.', 35, $this->source); })()), "html", null, true);
        echo "\"></div>
        <a href=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["original_path"]) || array_key_exists("original_path", $context) ? $context["original_path"] : (function () { throw new RuntimeError('Variable "original_path" does not exist.', 36, $this->source); })()), "html", null, true);
        echo "\" class=\"glightbox\" data-js-product-image>
            <img class=\"img-fluid\" style=\"width: 100%;\" src=\"";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 37, $this->source); })()), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 37, $this->source); })()), "name", [], "any", false, false, false, 37), "html", null, true);
        echo "\" />
        </a>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Show:_images.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 37,  176 => 36,  170 => 35,  166 => 34,  163 => 33,  160 => 32,  157 => 31,  154 => 30,  151 => 29,  148 => 28,  145 => 27,  142 => 26,  139 => 25,  136 => 24,  133 => 23,  130 => 22,  128 => 21,  125 => 20,  121 => 18,  107 => 17,  105 => 16,  95 => 13,  90 => 12,  87 => 11,  84 => 10,  82 => 9,  75 => 8,  72 => 7,  55 => 6,  51 => 5,  48 => 4,  45 => 3,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"row mt-3\">
    {% set i = 1 %}
    {% if product.images|length > 1 %}
    <div class=\"col-lg-2 col-md-3 col-2\">
        {{ sonata_block_render_event('sylius.shop.product.show.before_thumbnails', {'product': product}) }}
        {% for image in product.images|filter(image => (image.type != 'right' and image.type != 'left')) %}
        {% set path = image.path is not null ? image.path|imagine_filter('sylius_shop_product_small_thumbnail') : asset('bootstrap-theme/images/product-placeholder.png', 'bootstrapTheme') %}
        <div data-js-product-thumbnail {% if i > 8 %}class=\"hidden\"{% endif %}>
        {% if product.isConfigurable() and product.variants|length > 0 %}
            {% include '@SyliusShop/Product/Show/_imageVariants.html.twig' %}
        {% endif %}
            <a href=\"{{ image.path|imagine_filter('sylius_shop_product_original') }}\" class=\"glightbox\">
                <img class=\"img-fluid\" src=\"{{ path }}\" data-large-thumbnail=\"{{ image.path|imagine_filter('sylius_shop_product_large_thumbnail') }}\" alt=\"{{ product.name }}\" />
            </a>
        </div>
        {% set i = i + 1 %}
        {% endfor %}
    </div>
    {% endif %}

    {% if product.imagesByType('main') is not empty %}
        {% set source_path = product.imagesByType('main').first.path %}
        {% set original_path = source_path|imagine_filter('sylius_shop_product_original') %}
        {% set path = source_path|imagine_filter(filter|default('sylius_shop_product_large_thumbnail')) %}
    {% elseif product.images.first %}
        {% set source_path = product.images.first.path %}
        {% set original_path = source_path|imagine_filter('sylius_shop_product_original') %}
        {% set path = source_path|imagine_filter(filter|default('sylius_shop_product_large_thumbnail')) %}
    {% else %}
        {% set original_path = asset('bootstrap-theme/images/product-placeholder.png', 'bootstrapTheme') %}
        {% set path = original_path %}
    {% endif %}

    <div class=\"{{ product.images|length > 1 ? 'col-lg-10 col-md-9 col-10' : 'col-12' }}\">
        <div data-product-image=\"{{ path }}\" data-product-link=\"{{ original_path }}\"></div>
        <a href=\"{{ original_path }}\" class=\"glightbox\" data-js-product-image>
            <img class=\"img-fluid\" style=\"width: 100%;\" src=\"{{ path }}\" alt=\"{{ product.name }}\" />
        </a>
    </div>
</div>

", "SyliusShopBundle:Product/Show:_images.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Product/Show/_images.html.twig");
    }
}
