<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusManifestPlugin:Manifest/Gateways:_generate.html.twig */
class __TwigTemplate_0a516d27fe6dd8cff728ccb2eb0456a58ff6a7bdb8f034b7db647948b6185300 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusManifestPlugin:Manifest/Gateways:_generate.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["shippingGateways"]) || array_key_exists("shippingGateways", $context) ? $context["shippingGateways"] : (function () { throw new RuntimeError('Variable "shippingGateways" does not exist.', 1, $this->source); })()));
        foreach ($context['_seq'] as $context["code"] => $context["label"]) {
            // line 2
            echo "    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_manifest_admin_manifest_generate", ["code" => $context["code"]]), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, $context["code"], "html", null, true);
            echo "\" class=\"item\">
        ";
            // line 3
            echo twig_escape_filter($this->env, $context["label"], "html", null, true);
            echo "
    </a>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['code'], $context['label'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusManifestPlugin:Manifest/Gateways:_generate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 3,  44 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for code, label in shippingGateways %}
    <a href=\"{{ path('omni_sylius_manifest_admin_manifest_generate', {'code': code }) }}\" id=\"{{ code }}\" class=\"item\">
        {{ label }}
    </a>
{% endfor %}
", "OmniSyliusManifestPlugin:Manifest/Gateways:_generate.html.twig", "/var/www/html/vendor/omni/sylius-manifest-plugin/src/Resources/views/Manifest/Gateways/_generate.html.twig");
    }
}
