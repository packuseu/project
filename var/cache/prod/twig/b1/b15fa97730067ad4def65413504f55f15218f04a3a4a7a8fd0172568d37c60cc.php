<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_exportShipment.html.twig */
class __TwigTemplate_cbfa524bd1c96f5b87d0dd03599b42db63b36c8629dc876cc1ea1bddd2e6b309 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_exportShipment.html.twig"));

        // line 1
        echo "<form action=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("bitbag_admin_export_single_shipment", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 1, $this->source); })()), "id", [], "any", false, false, false, 1)]), "html", null, true);
        echo "\" method=\"POST\">
    <input type=\"hidden\" name=\"_method\" value=\"PUT\">
    <button class=\"ui labeled icon primary button mini shipping-export-state\"><i class=\"arrow up icon\"></i> ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new RuntimeError('Variable "value" does not exist.', 3, $this->source); })())), "html", null, true);
        echo "</button>
</form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_exportShipment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form action=\"{{ path('bitbag_admin_export_single_shipment', {'id' : data.id}) }}\" method=\"POST\">
    <input type=\"hidden\" name=\"_method\" value=\"PUT\">
    <button class=\"ui labeled icon primary button mini shipping-export-state\"><i class=\"arrow up icon\"></i> {{ value|trans }}</button>
</form>
", "@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_exportShipment.html.twig", "/var/www/html/vendor/bitbag/shipping-export-plugin/src/Resources/views/ShippingExport/Partial/_exportShipment.html.twig");
    }
}
