<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusCorePlugin/Form/theme.html.twig */
class __TwigTemplate_b78f98b6b96119974517a70ae62c9806056f6e44b5e845d0a2f1253a00cff9e3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            '_sylius_channel_images_entry_type_row' => [$this, 'block__sylius_channel_images_entry_type_row'],
            '_sylius_channel_images_entry_file_widget' => [$this, 'block__sylius_channel_images_entry_file_widget'],
            '_sylius_channel_images_entry_widget' => [$this, 'block__sylius_channel_images_entry_widget'],
            '_sylius_channel_logos_entry_type_row' => [$this, 'block__sylius_channel_logos_entry_type_row'],
            '_sylius_channel_logos_entry_widget' => [$this, 'block__sylius_channel_logos_entry_widget'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusAdmin/Form/theme.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusCorePlugin/Form/theme.html.twig"));

        $this->parent = $this->loadTemplate("@SyliusAdmin/Form/theme.html.twig", "@OmniSyliusCorePlugin/Form/theme.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block__sylius_channel_images_entry_type_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_sylius_channel_images_entry_type_row"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block__sylius_channel_images_entry_file_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_sylius_channel_images_entry_file_widget"));

        // line 8
        echo "    ";
        if ( !(null === ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 8), "vars", [], "any", false, true, false, 8), "value", [], "any", false, true, false, 8), "path", [], "any", true, true, false, 8)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 8), "vars", [], "any", false, true, false, 8), "value", [], "any", false, true, false, 8), "path", [], "any", false, false, false, 8), null)) : (null)))) {
            // line 9
            echo "        <img src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "parent", [], "any", false, false, false, 9), "vars", [], "any", false, false, false, 9), "value", [], "any", false, false, false, 9), "path", [], "any", false, false, false, 9), "sylius_small"), "html", null, true);
            echo "\" alt=\"watermark\">
    ";
        }
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), 'widget');
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 15
    public function block__sylius_channel_images_entry_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_sylius_channel_images_entry_widget"));

        // line 16
        echo "    <label for=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), "file", [], "any", false, false, false, 16), "vars", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16), "html", null, true);
        echo "\" class=\"ui icon labeled button\">
        <i class=\"cloud upload icon\"></i> ";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.choose_file"), "html", null, true);
        echo "
    </label>

    ";
        // line 20
        if ( !(null === ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 20), "value", [], "any", false, true, false, 20), "path", [], "any", true, true, false, 20)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 20), "value", [], "any", false, true, false, 20), "path", [], "any", false, false, false, 20), null)) : (null)))) {
            // line 21
            echo "        <img class=\"ui bordered image\" src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "vars", [], "any", false, false, false, 21), "value", [], "any", false, false, false, 21), "path", [], "any", false, false, false, 21), "sylius_small"), "html", null, true);
            echo "\" alt=\"watermark\">
    ";
        }
        // line 23
        echo "
    <div class=\"ui hidden element\">
        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), "file", [], "any", false, false, false, 25), 'widget');
        echo "
    </div>

    <div class=\"ui element\">
        ";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), "file", [], "any", false, false, false, 29), 'errors');
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 34
    public function block__sylius_channel_logos_entry_type_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_sylius_channel_logos_entry_type_row"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 37
    public function block__sylius_channel_logos_entry_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_sylius_channel_logos_entry_widget"));

        // line 38
        echo "    <label for=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 38, $this->source); })()), "file", [], "any", false, false, false, 38), "vars", [], "any", false, false, false, 38), "id", [], "any", false, false, false, 38), "html", null, true);
        echo "\" class=\"ui icon labeled button\">
        <i class=\"cloud upload icon\"></i> ";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.choose_file"), "html", null, true);
        echo "
    </label>

    ";
        // line 42
        if ( !(null === ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 42), "value", [], "any", false, true, false, 42), "path", [], "any", true, true, false, 42)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 42), "value", [], "any", false, true, false, 42), "path", [], "any", false, false, false, 42), null)) : (null)))) {
            // line 43
            echo "        ";
            if ((is_string($__internal_compile_0 = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 43, $this->source); })()), "vars", [], "any", false, false, false, 43), "value", [], "any", false, false, false, 43), "path", [], "any", false, false, false, 43)) && is_string($__internal_compile_1 = ".svg") && ('' === $__internal_compile_1 || $__internal_compile_1 === substr($__internal_compile_0, -strlen($__internal_compile_1))))) {
                // line 44
                echo "            ";
                $context["src"] = ("/media/image/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 44, $this->source); })()), "vars", [], "any", false, false, false, 44), "value", [], "any", false, false, false, 44), "path", [], "any", false, false, false, 44));
                // line 45
                echo "        ";
            } else {
                // line 46
                echo "            ";
                $context["src"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 46, $this->source); })()), "vars", [], "any", false, false, false, 46), "value", [], "any", false, false, false, 46), "path", [], "any", false, false, false, 46), "sylius_small");
                // line 47
                echo "        ";
            }
            // line 48
            echo "
        <img class=\"ui bordered image img-sm-thumbnail\" src=\"";
            // line 49
            echo twig_escape_filter($this->env, (isset($context["src"]) || array_key_exists("src", $context) ? $context["src"] : (function () { throw new RuntimeError('Variable "src" does not exist.', 49, $this->source); })()), "html", null, true);
            echo "\" alt=\"logo\">
    ";
        }
        // line 51
        echo "
    <div class=\"ui hidden element\">
        ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 53, $this->source); })()), "file", [], "any", false, false, false, 53), 'widget');
        echo "
    </div>

    <div class=\"ui element\">
        ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 57, $this->source); })()), "file", [], "any", false, false, false, 57), 'errors');
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusCorePlugin/Form/theme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 57,  197 => 53,  193 => 51,  188 => 49,  185 => 48,  182 => 47,  179 => 46,  176 => 45,  173 => 44,  170 => 43,  168 => 42,  162 => 39,  157 => 38,  150 => 37,  138 => 34,  128 => 29,  121 => 25,  117 => 23,  111 => 21,  109 => 20,  103 => 17,  98 => 16,  91 => 15,  84 => 12,  78 => 9,  75 => 8,  68 => 7,  56 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusAdmin/Form/theme.html.twig' %}

{# hide image 'type' field #}
{% block _sylius_channel_images_entry_type_row %}
{% endblock %}

{% block _sylius_channel_images_entry_file_widget %}
    {% if form.parent.vars.value.path|default(null) is not null %}
        <img src=\"{{ form.parent.vars.value.path|imagine_filter('sylius_small') }}\" alt=\"watermark\">
    {% endif %}

    {{- form_widget(form) -}}
{% endblock %}

{% block _sylius_channel_images_entry_widget %}
    <label for=\"{{ form.file.vars.id }}\" class=\"ui icon labeled button\">
        <i class=\"cloud upload icon\"></i> {{ 'sylius.ui.choose_file'|trans }}
    </label>

    {% if form.vars.value.path|default(null) is not null %}
        <img class=\"ui bordered image\" src=\"{{ form.vars.value.path|imagine_filter('sylius_small') }}\" alt=\"watermark\">
    {% endif %}

    <div class=\"ui hidden element\">
        {{ form_widget(form.file) }}
    </div>

    <div class=\"ui element\">
        {{ form_errors(form.file) }}
    </div>
{% endblock %}

{# hide image 'type' field #}
{% block _sylius_channel_logos_entry_type_row %}
{% endblock %}

{% block _sylius_channel_logos_entry_widget %}
    <label for=\"{{ form.file.vars.id }}\" class=\"ui icon labeled button\">
        <i class=\"cloud upload icon\"></i> {{ 'sylius.ui.choose_file'|trans }}
    </label>

    {% if form.vars.value.path|default(null) is not null %}
        {% if form.vars.value.path ends with '.svg' %}
            {% set src = '/media/image/' ~ form.vars.value.path %}
        {% else %}
            {% set src = form.vars.value.path|imagine_filter('sylius_small') %}
        {% endif %}

        <img class=\"ui bordered image img-sm-thumbnail\" src=\"{{ src }}\" alt=\"logo\">
    {% endif %}

    <div class=\"ui hidden element\">
        {{ form_widget(form.file) }}
    </div>

    <div class=\"ui element\">
        {{ form_errors(form.file) }}
    </div>
{% endblock %}
", "@OmniSyliusCorePlugin/Form/theme.html.twig", "/var/www/html/vendor/omni/sylius-core-plugin/src/Resources/views/Form/theme.html.twig");
    }
}
