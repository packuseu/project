<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusCmsPlugin:Example:_mainMenu.html.twig */
class __TwigTemplate_08ad706986cf5f1efac1e995e0c173969ac7e81d7fca7fc4abb97d78b587fbb9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusCmsPlugin:Example:_mainMenu.html.twig"));

        // line 9
        echo "
";
        // line 27
        echo "
";
        // line 28
        $macros["macros"] = $this->macros["macros"] = $this;
        // line 29
        echo "
<div class=\"ui large stackable menu\">
    ";
        // line 31
        $context["headerNode"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeWithChildrenByType("main_menu", 6);
        // line 32
        echo "
    ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(((twig_get_attribute($this->env, $this->source, ($context["headerNode"] ?? null), "children", [], "any", true, true, false, 33)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["headerNode"] ?? null), "children", [], "any", false, false, false, 33), [])) : ([])));
        foreach ($context['_seq'] as $context["_key"] => $context["node"]) {
            if (twig_get_attribute($this->env, $this->source, $context["node"], "enabled", [], "any", false, false, false, 33)) {
                // line 34
                echo "        ";
                echo twig_call_macro($macros["macros"], "macro_node_item", [$context["node"]], 34, $context, $this->getSourceContext());
                echo "
    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['node'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 1
    public function macro_node_url($__node__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "node_url"));

            // line 2
            echo "    ";
            $context["url"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeUrl((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 2, $this->source); })()));
            // line 3
            echo "    ";
            if (twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 3, $this->source); })()), "url", [], "any", false, false, false, 3))) {
                echo " {
        <span class=\"text\">";
                // line 4
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 4, $this->source); })()), "translation", [], "any", false, false, false, 4), "title", [], "any", false, false, false, 4), "html", null, true);
                echo "</span>
    ";
            } else {
                // line 6
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 6, $this->source); })()), "url", [], "any", false, false, false, 6), "html", null, true);
                echo "\"";
                if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 6, $this->source); })()), "target", [], "any", false, false, false, 6)) {
                    echo " target=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 6, $this->source); })()), "target", [], "any", false, false, false, 6), "html", null, true);
                    echo "\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 6, $this->source); })()), "translation", [], "any", false, false, false, 6), "title", [], "any", false, false, false, 6), "html", null, true);
                echo "</a>
    ";
            }
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 10
    public function macro_node_item($__node__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "node_item"));

            // line 11
            echo "    ";
            $macros["macros"] = $this;
            // line 12
            echo "
    ";
            // line 13
            if (twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 13, $this->source); })()), "hasEnabledChildren", [], "any", false, false, false, 13)) {
                // line 14
                echo "        <div class=\"ui dropdown item\">
            ";
                // line 15
                echo twig_call_macro($macros["macros"], "macro_node_url", [(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 15, $this->source); })())], 15, $context, $this->getSourceContext());
                echo "
            <i class=\"dropdown icon\"></i>
            <div class=\"menu\">
                ";
                // line 18
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 18, $this->source); })()), "children", [], "any", false, false, false, 18));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    if (twig_get_attribute($this->env, $this->source, $context["child"], "enabled", [], "any", false, false, false, 18)) {
                        // line 19
                        echo "                    ";
                        echo twig_call_macro($macros["macros"], "macro_node_item", [$context["child"]], 19, $context, $this->getSourceContext());
                        echo "
                ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 21
                echo "            </div>
        </div>
    ";
            } else {
                // line 24
                echo "        <span class=\"item\">";
                echo twig_call_macro($macros["macros"], "macro_node_url", [(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 24, $this->source); })())], 24, $context, $this->getSourceContext());
                echo "</span>
    ";
            }
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "OmniSyliusCmsPlugin:Example:_mainMenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 24,  181 => 21,  171 => 19,  166 => 18,  160 => 15,  157 => 14,  155 => 13,  152 => 12,  149 => 11,  133 => 10,  109 => 6,  104 => 4,  99 => 3,  96 => 2,  80 => 1,  72 => 36,  62 => 34,  57 => 33,  54 => 32,  52 => 31,  48 => 29,  46 => 28,  43 => 27,  40 => 9,);
    }

    public function getSourceContext()
    {
        return new Source("{% macro node_url(node) %}
    {% set url = omni_sylius_get_node_url(node) %}
    {% if url.url is empty %} {
        <span class=\"text\">{{ node.translation.title }}</span>
    {% else %}
        <a href=\"{{ url.url }}\"{% if url.target %} target=\"{{ url.target }}\"{% endif %}>{{ node.translation.title }}</a>
    {% endif %}
{% endmacro %}

{% macro node_item(node) %}
    {% import _self as macros %}

    {% if node.hasEnabledChildren %}
        <div class=\"ui dropdown item\">
            {{ macros.node_url(node) }}
            <i class=\"dropdown icon\"></i>
            <div class=\"menu\">
                {% for child in node.children if child.enabled %}
                    {{ macros.node_item(child) }}
                {% endfor %}
            </div>
        </div>
    {% else %}
        <span class=\"item\">{{ macros.node_url(node) }}</span>
    {% endif %}
{% endmacro %}

{% import _self as macros %}

<div class=\"ui large stackable menu\">
    {% set headerNode = omni_sylius_get_node_with_children_by_type('main_menu', 6) %}

    {% for node in headerNode.children|default([]) if node.enabled %}
        {{ macros.node_item(node) }}
    {% endfor %}
</div>
", "OmniSyliusCmsPlugin:Example:_mainMenu.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Example/_mainMenu.html.twig");
    }
}
