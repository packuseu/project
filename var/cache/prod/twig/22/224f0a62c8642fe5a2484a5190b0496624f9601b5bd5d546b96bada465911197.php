<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/SyliusAdminBundle/Order/Show/_shipments.html.twig */
class __TwigTemplate_da6d6f5401cd4ef97b9db9e7fcff8f810ce2376eb47740ec5ff94649c0bfd8c9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/SyliusAdminBundle/Order/Show/_shipments.html.twig"));

        // line 1
        echo "<div class=\"ui attached segment\" id=\"shipping-state\">
    ";
        // line 2
        $this->loadTemplate((("@SyliusAdmin/Order/Label/ShippingState/" . twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 2, $this->source); })()), "shippingState", [], "any", false, false, false, 2)) . ".html.twig"), "bundles/SyliusAdminBundle/Order/Show/_shipments.html.twig", 2)->display(twig_array_merge($context, ["value" => ("sylius.ui." . twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 2, $this->source); })()), "shippingState", [], "any", false, false, false, 2)), "attached" => true]));
        // line 3
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 3, $this->source); })()), "hasShipments", [], "any", false, false, false, 3)) {
            // line 4
            echo "        ";
            $context["shipment"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 4, $this->source); })()), "shipments", [], "any", false, false, false, 4), "first", [], "any", false, false, false, 4);
            // line 5
            echo "        <h3 class=\"ui dividing header\" id=\"shipping-state\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipments"), "html", null, true);
            echo "</h3>
        <div class=\"ui relaxed divided list\" id=\"sylius-shipments\">
            ";
            // line 7
            $this->loadTemplate("@SyliusAdmin/Order/Show/_shipment.html.twig", "bundles/SyliusAdminBundle/Order/Show/_shipments.html.twig", 7)->display($context);
            // line 8
            echo "        </div>
    ";
        }
        // line 10
        echo "</div>
";
        // line 11
        if (( !twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 11, $this->source); })()), "tracking", [], "any", false, false, false, 11)) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 11, $this->source); })()), "shippingExports", [], "any", false, false, false, 11)) > 0))) {
            // line 12
            echo "    <div class=\"ui attached segment\">
        ";
            // line 13
            echo $this->extensions['App\Twig\AppExtension']->renderDownloadLabelButton((isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 13, $this->source); })()));
            echo "
    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/SyliusAdminBundle/Order/Show/_shipments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 13,  68 => 12,  66 => 11,  63 => 10,  59 => 8,  57 => 7,  51 => 5,  48 => 4,  45 => 3,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"ui attached segment\" id=\"shipping-state\">
    {% include '@SyliusAdmin/Order/Label/ShippingState/' ~ order.shippingState ~ '.html.twig' with { 'value': 'sylius.ui.' ~ order.shippingState, 'attached': true } %}
    {% if order.hasShipments %}
        {% set shipment = order.shipments.first %}
        <h3 class=\"ui dividing header\" id=\"shipping-state\">{{ 'sylius.ui.shipments'|trans }}</h3>
        <div class=\"ui relaxed divided list\" id=\"sylius-shipments\">
            {% include '@SyliusAdmin/Order/Show/_shipment.html.twig' %}
        </div>
    {% endif %}
</div>
{% if shipment.tracking is not empty and shipment.shippingExports|length > 0 %}
    <div class=\"ui attached segment\">
        {{ app_render_shipping_label_download_button(shipment)|raw }}
    </div>
{% endif %}
", "bundles/SyliusAdminBundle/Order/Show/_shipments.html.twig", "/var/www/html/templates/bundles/SyliusAdminBundle/Order/Show/_shipments.html.twig");
    }
}
