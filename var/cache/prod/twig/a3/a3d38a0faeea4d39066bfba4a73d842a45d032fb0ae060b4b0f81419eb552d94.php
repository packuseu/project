<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusBannerPlugin/Admin/Banner/_form.html.twig */
class __TwigTemplate_b4b049fc188fa81e4529f70bb6b28fca7b60942c125155bae7255d6ff46cdebb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusBannerPlugin/Admin/Banner/_form.html.twig"));

        // line 1
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'errors');
        echo "

";
        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 3, $this->source); })()), [0 => "@OmniSyliusBannerPlugin/Form/imagesTheme.html.twig"], true);
        // line 4
        echo "
<div class=\"ui stackable grid\">
    <div class=\"three wide column\">
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), "enabled", [], "any", false, false, false, 7), 'row');
        echo "
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "publishFrom", [], "any", false, false, false, 8), 'row');
        echo "
        ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "publishTo", [], "any", false, false, false, 9), 'row');
        echo "
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), "channels", [], "any", false, false, false, 10), 'row');
        echo "
    </div>
    <div class=\"thirteen wide column\">
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 13, $this->source); })()), "code", [], "any", false, false, false, 13), 'row');
        echo "
        ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "position", [], "any", false, false, false, 14), 'row');
        echo "

        ";
        // line 16
        $context["imagePositionCode"] = null;
        // line 17
        echo "
        ";
        // line 18
        if ((null === twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 18, $this->source); })()), "position", [], "any", false, false, false, 18))) {
            // line 19
            echo "            ";
            // line 20
            echo "            ";
            // line 21
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "children", [], "any", false, false, false, 21), "position", [], "any", false, false, false, 21), "vars", [], "any", false, false, false, 21), "choices", [], "any", false, false, false, 21));
            foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
                // line 22
                echo "                ";
                if ((null === (isset($context["imagePositionCode"]) || array_key_exists("imagePositionCode", $context) ? $context["imagePositionCode"] : (function () { throw new RuntimeError('Variable "imagePositionCode" does not exist.', 22, $this->source); })()))) {
                    // line 23
                    echo "                    ";
                    $context["imagePositionCode"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["choice"], "data", [], "any", false, false, false, 23), "type", [], "any", false, false, false, 23);
                    // line 24
                    echo "                ";
                }
                // line 25
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "        ";
        } else {
            // line 27
            echo "            ";
            $context["imagePositionCode"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 27, $this->source); })()), "position", [], "any", false, false, false, 27), "type", [], "any", false, false, false, 27);
            // line 28
            echo "        ";
        }
        // line 29
        echo "
        <div class=\"field position-type-images\" data-position-types-url=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_banner_position_type", ["id" => "__id__"]);
        echo "\">
            <label>";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.banner.position.example_image"), "html", null, true);
        echo "</label>

            ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->getPositionTypeConfig());
        foreach ($context['_seq'] as $context["position"] => $context["config"]) {
            // line 34
            echo "                <img
                    id=\"position-";
            // line 35
            echo twig_escape_filter($this->env, $context["position"], "html", null, true);
            echo "\"
                    class=\"ui element ";
            // line 36
            if (((isset($context["imagePositionCode"]) || array_key_exists("imagePositionCode", $context) ? $context["imagePositionCode"] : (function () { throw new RuntimeError('Variable "imagePositionCode" does not exist.', 36, $this->source); })()) != $context["position"])) {
                echo "hidden";
            } else {
                echo "active";
            }
            echo "\"
                    src=\"";
            // line 37
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 37, $this->source); })()), "request", [], "any", false, false, false, 37), "getSchemeAndHttpHost", [], "any", false, false, false, 37) . "/") . twig_get_attribute($this->env, $this->source, $context["config"], "example_image", [], "any", false, false, false, 37)), "html", null, true);
            echo "\" alt=\"\" style=\"height: 200px; margin-top: 10px;\">
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['position'], $context['config'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "        </div>

        <div class=\"ui upload box segment\">
            ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 42, $this->source); })()), "images", [], "any", false, false, false, 42), 'row');
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusBannerPlugin/Admin/Banner/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 42,  158 => 39,  150 => 37,  142 => 36,  138 => 35,  135 => 34,  131 => 33,  126 => 31,  122 => 30,  119 => 29,  116 => 28,  113 => 27,  110 => 26,  104 => 25,  101 => 24,  98 => 23,  95 => 22,  90 => 21,  88 => 20,  86 => 19,  84 => 18,  81 => 17,  79 => 16,  74 => 14,  70 => 13,  64 => 10,  60 => 9,  56 => 8,  52 => 7,  47 => 4,  45 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_errors(form) }}

{% form_theme form '@OmniSyliusBannerPlugin/Form/imagesTheme.html.twig' %}

<div class=\"ui stackable grid\">
    <div class=\"three wide column\">
        {{ form_row(form.enabled) }}
        {{ form_row(form.publishFrom) }}
        {{ form_row(form.publishTo) }}
        {{ form_row(form.channels) }}
    </div>
    <div class=\"thirteen wide column\">
        {{ form_row(form.code) }}
        {{ form_row(form.position) }}

        {% set imagePositionCode = null %}

        {% if resource.position is null %}
            {# we build the first choice here to suppliment the create action where the first form choice image #}
            {# should be displayed #}
            {% for choice in form.children.position.vars.choices %}
                {% if imagePositionCode is null %}
                    {% set imagePositionCode = choice.data.type %}
                {% endif %}
            {% endfor %}
        {% else %}
            {% set imagePositionCode = resource.position.type %}
        {% endif %}

        <div class=\"field position-type-images\" data-position-types-url=\"{{ path('omni_sylius_banner_position_type', {id: '__id__'}) }}\">
            <label>{{ 'omni_sylius.banner.position.example_image'|trans }}</label>

            {% for position, config in omni_sylius_banner_position_type_config() %}
                <img
                    id=\"position-{{ position }}\"
                    class=\"ui element {% if imagePositionCode != position %}hidden{% else %}active{% endif %}\"
                    src=\"{{ app.request.getSchemeAndHttpHost ~ '/' ~ config.example_image }}\" alt=\"\" style=\"height: 200px; margin-top: 10px;\">
            {% endfor %}
        </div>

        <div class=\"ui upload box segment\">
            {{ form_row(form.images) }}
        </div>
    </div>
</div>
", "@OmniSyliusBannerPlugin/Admin/Banner/_form.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/Admin/Banner/_form.html.twig");
    }
}
