<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusCmsPlugin/Frontend/Show/promotion.html.twig */
class __TwigTemplate_b607737cb60bd7d829e17b72c4178b9fed03b4679ec4c21afe3cbab4b3d16f82 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusCmsPlugin/Frontend/Show/promotion.html.twig"));

        // line 1
        echo "<div class=\"ui column stackable grid\">
    <div class=\"column\">
        <div class=\"ui basic segment\">
            <p>";
        // line 4
        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 4, $this->source); })()), "translation", [], "any", false, false, false, 4), "content", [], "any", false, false, false, 4);
        echo "</p>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusCmsPlugin/Frontend/Show/promotion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"ui column stackable grid\">
    <div class=\"column\">
        <div class=\"ui basic segment\">
            <p>{{ node.translation.content|raw }}</p>
        </div>
    </div>
</div>
", "@OmniSyliusCmsPlugin/Frontend/Show/promotion.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Frontend/Show/promotion.html.twig");
    }
}
