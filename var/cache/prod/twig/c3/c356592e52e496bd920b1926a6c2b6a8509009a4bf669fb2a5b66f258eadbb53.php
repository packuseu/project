<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusAdminBundle:Product:_showInShopButton.html.twig */
class __TwigTemplate_eae1d0aae89ecce243a5a84ba2566dc1580a1d239763851caa570f4d4a5a80f9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusAdminBundle:Product:_showInShopButton.html.twig"));

        // line 1
        $context["enabledChannels"] = twig_array_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 1, $this->source); })()), "channels", [], "any", false, false, false, 1), function ($__channel__) use ($context, $macros) { $context["channel"] = $__channel__; return (twig_get_attribute($this->env, $this->source, (isset($context["channel"]) || array_key_exists("channel", $context) ? $context["channel"] : (function () { throw new RuntimeError('Variable "channel" does not exist.', 1, $this->source); })()), "enabled", [], "any", false, false, false, 1) == true); });
        // line 2
        echo "
";
        // line 3
        if ($this->extensions['Sylius\Bundle\CoreBundle\Twig\BundleLoadedCheckerExtension']->isBundleLoaded("SyliusShopBundle")) {
            // line 4
            echo "    <div class=\"middle aligned column\">
        <div class=\"ui right floated buttons\">
            ";
            // line 6
            if (( !twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "enabled", [], "any", false, false, false, 6) || (twig_length_filter($this->env, (isset($context["enabledChannels"]) || array_key_exists("enabledChannels", $context) ? $context["enabledChannels"] : (function () { throw new RuntimeError('Variable "enabledChannels" does not exist.', 6, $this->source); })())) < 1))) {
                // line 7
                echo "                <a class=\"ui labeled icon button disabled\" href=\"#\">
                    <i class=\"angle right icon\"></i>
                    ";
                // line 9
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.show_product_in_shop_page"), "html", null, true);
                echo "
                </a>
            ";
            } elseif ((twig_length_filter($this->env,             // line 11
(isset($context["enabledChannels"]) || array_key_exists("enabledChannels", $context) ? $context["enabledChannels"] : (function () { throw new RuntimeError('Variable "enabledChannels" does not exist.', 11, $this->source); })())) > 1)) {
                // line 12
                echo "                <div class=\"ui floating dropdown labeled icon button\">
                    <i class=\"share alternate icon\"></i>
                    <span class=\"text\">
                        ";
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.show_product_in_shop_page"), "html", null, true);
                echo "
                    </span>
                    <div class=\"menu\">
                        <div class=\"scrolling menu\">
                            ";
                // line 19
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["enabledChannels"]) || array_key_exists("enabledChannels", $context) ? $context["enabledChannels"] : (function () { throw new RuntimeError('Variable "enabledChannels" does not exist.', 19, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
                    // line 20
                    echo "                                ";
                    $context["url"] = (( !(null === twig_get_attribute($this->env, $this->source, $context["channel"], "hostname", [], "any", false, false, false, 20))) ? ((("http://" . twig_get_attribute($this->env, $this->source, $context["channel"], "hostname", [], "any", false, false, false, 20)) . $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 20, $this->source); })()), "slug", [], "any", false, false, false, 20), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["channel"], "defaultLocale", [], "any", false, false, false, 20), "code", [], "any", false, false, false, 20)]))) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sylius_shop_product_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 20, $this->source); })()), "slug", [], "any", false, false, false, 20), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["channel"], "defaultLocale", [], "any", false, false, false, 20), "code", [], "any", false, false, false, 20)])));
                    // line 21
                    echo "                                <a href=\"";
                    echo (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 21, $this->source); })());
                    echo "\" class=\"item\">
                                    <i class=\"angle right icon\"></i>
                                    ";
                    // line 23
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.show_in"), "html", null, true);
                    echo "
                                    ";
                    // line 24
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "name", [], "any", false, false, false, 24), "html", null, true);
                    echo " (";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "code", [], "any", false, false, false, 24), "html", null, true);
                    echo ")
                                </a>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "                        </div>
                    </div>
                </div>
            ";
            } else {
                // line 31
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["enabledChannels"]) || array_key_exists("enabledChannels", $context) ? $context["enabledChannels"] : (function () { throw new RuntimeError('Variable "enabledChannels" does not exist.', 31, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
                    // line 32
                    echo "                    ";
                    $context["url"] = (( !(null === twig_get_attribute($this->env, $this->source, $context["channel"], "hostname", [], "any", false, false, false, 32))) ? ((("http://" . twig_get_attribute($this->env, $this->source, $context["channel"], "hostname", [], "any", false, false, false, 32)) . $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 32, $this->source); })()), "slug", [], "any", false, false, false, 32), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["channel"], "defaultLocale", [], "any", false, false, false, 32), "code", [], "any", false, false, false, 32)]))) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sylius_shop_product_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 32, $this->source); })()), "slug", [], "any", false, false, false, 32), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["channel"], "defaultLocale", [], "any", false, false, false, 32), "code", [], "any", false, false, false, 32)])));
                    // line 33
                    echo "                    <a class=\"ui labeled icon button\" href=\"";
                    echo (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 33, $this->source); })());
                    echo "\">
                        <i class=\"angle right icon\"></i>
                        ";
                    // line 35
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.show_product_in_shop_page"), "html", null, true);
                    echo "
                    </a>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 38
                echo "            ";
            }
            // line 39
            echo "        </div>
    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusAdminBundle:Product:_showInShopButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 39,  133 => 38,  124 => 35,  118 => 33,  115 => 32,  110 => 31,  104 => 27,  93 => 24,  89 => 23,  83 => 21,  80 => 20,  76 => 19,  69 => 15,  64 => 12,  62 => 11,  57 => 9,  53 => 7,  51 => 6,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set enabledChannels = product.channels|filter(channel => channel.enabled == true) %}

{% if sylius_bundle_loaded_checker('SyliusShopBundle') %}
    <div class=\"middle aligned column\">
        <div class=\"ui right floated buttons\">
            {% if not product.enabled or enabledChannels|length < 1 %}
                <a class=\"ui labeled icon button disabled\" href=\"#\">
                    <i class=\"angle right icon\"></i>
                    {{ 'sylius.ui.show_product_in_shop_page'|trans }}
                </a>
            {% elseif enabledChannels|length > 1 %}
                <div class=\"ui floating dropdown labeled icon button\">
                    <i class=\"share alternate icon\"></i>
                    <span class=\"text\">
                        {{ 'sylius.ui.show_product_in_shop_page'|trans }}
                    </span>
                    <div class=\"menu\">
                        <div class=\"scrolling menu\">
                            {% for channel in enabledChannels %}
                                {% set url = channel.hostname is not null ? 'http://' ~ channel.hostname ~ path('sylius_shop_product_show', {'slug': product.slug, '_locale': channel.defaultLocale.code}) : url('sylius_shop_product_show', {'slug': product.slug, '_locale': channel.defaultLocale.code}) %}
                                <a href=\"{{ url|raw }}\" class=\"item\">
                                    <i class=\"angle right icon\"></i>
                                    {{ 'sylius.ui.show_in'|trans }}
                                    {{ channel.name }} ({{ channel.code }})
                                </a>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            {% else %}
                {% for channel in enabledChannels %}
                    {% set url = channel.hostname is not null ? 'http://' ~ channel.hostname ~ path('sylius_shop_product_show', {'slug': product.slug, '_locale': channel.defaultLocale.code}) : url('sylius_shop_product_show', {'slug': product.slug, '_locale': channel.defaultLocale.code}) %}
                    <a class=\"ui labeled icon button\" href=\"{{ url|raw }}\">
                        <i class=\"angle right icon\"></i>
                        {{ 'sylius.ui.show_product_in_shop_page'|trans }}
                    </a>
                {% endfor %}
            {% endif %}
        </div>
    </div>
{% endif %}
", "SyliusAdminBundle:Product:_showInShopButton.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/Product/_showInShopButton.html.twig");
    }
}
