<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SyliusUi/Grid/Field/logAction.html.twig */
class __TwigTemplate_874fee47f7754e607df09212cd3951bcbefb399f3f33757c4a31877217eadb5c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@SyliusUi/Grid/Field/logAction.html.twig"));

        // line 1
        if (((isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 1, $this->source); })()) == "create")) {
            // line 2
            echo "    <span class=\"ui teal label\">
        <i class=\"plus icon\"></i>
        ";
            // line 4
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.created"), "html", null, true);
            echo "
    </span>
";
        } elseif ((        // line 6
(isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 6, $this->source); })()) == "update")) {
            // line 7
            echo "    <span class=\"ui blue label\">
        <i class=\"pencil icon\"></i>
        ";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.updated"), "html", null, true);
            echo "
    </span>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@SyliusUi/Grid/Field/logAction.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 9,  53 => 7,  51 => 6,  46 => 4,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if data == 'create' %}
    <span class=\"ui teal label\">
        <i class=\"plus icon\"></i>
        {{ 'sylius.ui.created'|trans }}
    </span>
{% elseif data == 'update' %}
    <span class=\"ui blue label\">
        <i class=\"pencil icon\"></i>
        {{ 'sylius.ui.updated'|trans }}
    </span>
{% endif %}
", "@SyliusUi/Grid/Field/logAction.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/UiBundle/Resources/views/Grid/Field/logAction.html.twig");
    }
}
