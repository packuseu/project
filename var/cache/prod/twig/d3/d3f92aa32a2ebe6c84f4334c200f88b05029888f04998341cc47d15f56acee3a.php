<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusFilterPlugin:layouts:stylesheets.html.twig */
class __TwigTemplate_b6119d0c8de83560a0ca98ab98917f6c6ad14023eee09a75d0973f9d90fd00bd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusFilterPlugin:layouts:stylesheets.html.twig"));

        // line 1
        echo "<link href=\"https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/10.1.0/nouislider.min.css\" rel=\"stylesheet\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusFilterPlugin:layouts:stylesheets.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<link href=\"https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/10.1.0/nouislider.min.css\" rel=\"stylesheet\">
", "OmniSyliusFilterPlugin:layouts:stylesheets.html.twig", "/var/www/html/vendor/omni/sylius-filter-plugin/src/Resources/views/layouts/stylesheets.html.twig");
    }
}
