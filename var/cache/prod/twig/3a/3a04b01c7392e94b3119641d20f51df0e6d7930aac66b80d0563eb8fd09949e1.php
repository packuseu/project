<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SyliusAdmin/Product/Show/_attributes.html.twig */
class __TwigTemplate_d7cc9b3b2ae36b1e8517d11ab4027a1a53a7702ff9718597406a1ed789f0af98 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@SyliusAdmin/Product/Show/_attributes.html.twig"));

        // line 1
        echo "<div id=\"attributes\">
    <h4 class=\"ui top attached large header\">";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.attributes"), "html", null, true);
        echo "</h4>
    <div class=\"ui attached segment\">
        <div class=\"ui top attached tabular menu\">
            ";
        // line 5
        $context["setLocales"] = [];
        // line 6
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "attributes", [], "any", false, false, false, 6));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["attributeValue"]) {
            // line 7
            echo "                ";
            if (!twig_in_filter(twig_get_attribute($this->env, $this->source, $context["attributeValue"], "localeCode", [], "any", false, false, false, 7), (isset($context["setLocales"]) || array_key_exists("setLocales", $context) ? $context["setLocales"] : (function () { throw new RuntimeError('Variable "setLocales" does not exist.', 7, $this->source); })()))) {
                // line 8
                echo "                    <a class=\"item";
                if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 8)) {
                    echo " active";
                }
                echo "\" data-tab=\"";
                echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_locale_name')->getCallable(), [twig_get_attribute($this->env, $this->source, $context["attributeValue"], "localeCode", [], "any", false, false, false, 8)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_locale_name')->getCallable(), [twig_get_attribute($this->env, $this->source, $context["attributeValue"], "localeCode", [], "any", false, false, false, 8)]), "html", null, true);
                echo "</a>
                    ";
                // line 9
                $context["setLocales"] = twig_array_merge((isset($context["setLocales"]) || array_key_exists("setLocales", $context) ? $context["setLocales"] : (function () { throw new RuntimeError('Variable "setLocales" does not exist.', 9, $this->source); })()), [0 => twig_get_attribute($this->env, $this->source, $context["attributeValue"], "localeCode", [], "any", false, false, false, 9)]);
                // line 10
                echo "                ";
            }
            // line 11
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attributeValue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "        </div>
        ";
        // line 13
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["setLocales"]) || array_key_exists("setLocales", $context) ? $context["setLocales"] : (function () { throw new RuntimeError('Variable "setLocales" does not exist.', 13, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["locale"]) {
            // line 14
            echo "            <div class=\"ui bottom attached tab segment";
            if (twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 14)) {
                echo " active";
            }
            echo "\" data-tab=\"";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_locale_name')->getCallable(), [$context["locale"]]), "html", null, true);
            echo "\">
                <table class=\"ui very basic celled table\">
                    <tbody>
                    ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 17, $this->source); })()), "attributes", [], "any", false, false, false, 17));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["_key"] => $context["attributeValue"]) {
                if ((twig_get_attribute($this->env, $this->source, $context["attributeValue"], "localeCode", [], "any", false, false, false, 17) == $context["locale"])) {
                    // line 18
                    echo "                        <tr>
                            <td class=\"five wide\">
                                <strong class=\"gray text\">";
                    // line 20
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["attributeValue"], "name", [], "any", false, false, false, 20), "html", null, true);
                    echo "</strong>
                            </td>
                            <td>
                                ";
                    // line 23
                    $this->loadTemplate([0 => (("@SyliusAdmin/Product/Show/Types/" . twig_get_attribute($this->env, $this->source, $context["attributeValue"], "type", [], "any", false, false, false, 23)) . ".html.twig"), 1 => "@SyliusAttribute/Types/default.html.twig"], "@SyliusAdmin/Product/Show/_attributes.html.twig", 23)->display(twig_array_merge($context, ["attribute" => $context["attributeValue"], "locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 23, $this->source); })()), "request", [], "any", false, false, false, 23), "locale", [], "any", false, false, false, 23), "fallbackLocale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["configuration"]) || array_key_exists("configuration", $context) ? $context["configuration"] : (function () { throw new RuntimeError('Variable "configuration" does not exist.', 23, $this->source); })()), "request", [], "any", false, false, false, 23), "defaultLocale", [], "any", false, false, false, 23)]));
                    // line 24
                    echo "                            </td>
                        </tr>
                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attributeValue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "                    </tbody>
                </table>
            </div>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['locale'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@SyliusAdmin/Product/Show/_attributes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 31,  168 => 27,  156 => 24,  154 => 23,  148 => 20,  144 => 18,  133 => 17,  122 => 14,  105 => 13,  102 => 12,  88 => 11,  85 => 10,  83 => 9,  72 => 8,  69 => 7,  51 => 6,  49 => 5,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"attributes\">
    <h4 class=\"ui top attached large header\">{{ 'sylius.ui.attributes'|trans }}</h4>
    <div class=\"ui attached segment\">
        <div class=\"ui top attached tabular menu\">
            {% set setLocales = [] %}
            {% for attributeValue in product.attributes %}
                {% if attributeValue.localeCode not in setLocales %}
                    <a class=\"item{% if loop.first %} active{% endif %}\" data-tab=\"{{ attributeValue.localeCode|sylius_locale_name }}\">{{ attributeValue.localeCode|sylius_locale_name }}</a>
                    {%  set setLocales = setLocales|merge([attributeValue.localeCode]) %}
                {% endif %}
            {% endfor %}
        </div>
        {% for locale in setLocales %}
            <div class=\"ui bottom attached tab segment{% if loop.first %} active{% endif %}\" data-tab=\"{{ locale|sylius_locale_name }}\">
                <table class=\"ui very basic celled table\">
                    <tbody>
                    {% for attributeValue in product.attributes if attributeValue.localeCode == locale %}
                        <tr>
                            <td class=\"five wide\">
                                <strong class=\"gray text\">{{ attributeValue.name }}</strong>
                            </td>
                            <td>
                                {% include [('@SyliusAdmin/Product/Show/Types/'~attributeValue.type~'.html.twig'), '@SyliusAttribute/Types/default.html.twig'] with {'attribute': attributeValue, 'locale': configuration.request.locale, 'fallbackLocale': configuration.request.defaultLocale} %}
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        {% endfor %}
    </div>
</div>
", "@SyliusAdmin/Product/Show/_attributes.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/Product/Show/_attributes.html.twig");
    }
}
