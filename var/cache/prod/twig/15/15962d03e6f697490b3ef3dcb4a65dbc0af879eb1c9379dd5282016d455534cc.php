<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusBannerPlugin/PositionTypes/_wide_content.html.twig */
class __TwigTemplate_cc12798e84a802396570c59f7587bb9aadd1478117ae428f4017e9d8bb8033e5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusBannerPlugin/PositionTypes/_wide_content.html.twig"));

        // line 1
        $context["useContainer"] = (twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "full_screen", [], "any", true, true, false, 1) && (twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 1, $this->source); })()), "full_screen", [], "any", false, false, false, 1) == false));
        // line 2
        echo "
";
        // line 3
        if (twig_length_filter($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 3, $this->source); })()))) {
            // line 4
            echo "    ";
            $context["banner"] = twig_get_attribute($this->env, $this->source, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 4, $this->source); })()), twig_random($this->env, (twig_length_filter($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 4, $this->source); })())) - 1)), [], "array", false, false, false, 4);
            // line 5
            echo "
    ";
            // line 6
            if ((twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 6, $this->source); })()), "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 6, $this->source); })()), "channel", [], "array", false, false, false, 6)], "method", false, false, false, 6) && twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 6, $this->source); })()), "images", [], "any", false, false, false, 6)))) {
                // line 7
                echo "        ";
                $context["image"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 7, $this->source); })()), "images", [], "any", false, false, false, 7), "toArray", [], "any", false, false, false, 7), twig_random($this->env, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 7, $this->source); })()), "images", [], "any", false, false, false, 7)) - 1)), [], "array", false, false, false, 7);
                // line 8
                echo "
        <div class=\"banner\">
            ";
                // line 10
                if ((isset($context["useContainer"]) || array_key_exists("useContainer", $context) ? $context["useContainer"] : (function () { throw new RuntimeError('Variable "useContainer" does not exist.', 10, $this->source); })())) {
                    // line 11
                    echo "                <div class=\"container\">
            ";
                }
                // line 13
                echo "
            <a class=\"banner-link\" href=\"";
                // line 14
                echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 14), "link", [], "any", true, true, false, 14)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 14), "link", [], "any", false, false, false, 14), "javascript:;")) : ("javascript:;")), "html", null, true);
                echo "\">
                <img class=\"banner-image\" src=\"";
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 15, $this->source); })()), "path", [], "any", false, false, false, 15), "omni_sylius_banner"), "html", null, true);
                echo "\" alt=\"-\">

                <div class=\"banner-caption caption--";
                // line 17
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 17, $this->source); })()), "contentPosition", [], "any", false, false, false, 17), "html", null, true);
                echo "\"
                     style=\"";
                // line 18
                if (twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 18, $this->source); })()), "contentSpace", [], "any", false, false, false, 18)) {
                    echo "width: ";
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 18, $this->source); })()), "contentSpace", [], "any", false, false, false, 18)), "html", null, true);
                    echo "%";
                }
                echo "\"
                >
                    ";
                // line 20
                echo twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 20, $this->source); })()), "content", [], "any", false, false, false, 20);
                echo "
                </div>
            </a>

            ";
                // line 24
                if ((isset($context["useContainer"]) || array_key_exists("useContainer", $context) ? $context["useContainer"] : (function () { throw new RuntimeError('Variable "useContainer" does not exist.', 24, $this->source); })())) {
                    // line 25
                    echo "                </div>
            ";
                }
                // line 27
                echo "        </div>
    ";
            }
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusBannerPlugin/PositionTypes/_wide_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 27,  102 => 25,  100 => 24,  93 => 20,  84 => 18,  80 => 17,  75 => 15,  71 => 14,  68 => 13,  64 => 11,  62 => 10,  58 => 8,  55 => 7,  53 => 6,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set useContainer = options.full_screen is defined and options.full_screen == false %}

{% if banners|length %}
    {% set banner = banners[random(banners|length - 1) ] %}

    {% if banner.hasChannelCode(options['channel']) and banner.images|length %}
        {% set image = banner.images.toArray[random(banner.images|length - 1)] %}

        <div class=\"banner\">
            {% if useContainer %}
                <div class=\"container\">
            {% endif %}

            <a class=\"banner-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                <img class=\"banner-image\" src=\"{{ image.path|imagine_filter('omni_sylius_banner') }}\" alt=\"-\">

                <div class=\"banner-caption caption--{{ image.contentPosition }}\"
                     style=\"{% if image.contentSpace %}width: {{ image.contentSpace|number_format }}%{% endif %}\"
                >
                    {{ image.content|raw }}
                </div>
            </a>

            {% if useContainer %}
                </div>
            {% endif %}
        </div>
    {% endif %}
{% endif %}
", "@OmniSyliusBannerPlugin/PositionTypes/_wide_content.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/PositionTypes/_wide_content.html.twig");
    }
}
