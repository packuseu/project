<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_shipping_details_download.html.twig */
class __TwigTemplate_bae4c9768f00d5725db1eb8df203910abd93f212ad02d811f025aabcbfde77ac extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_shipping_details_download.html.twig"));

        // line 1
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("bitbag_admin_get_shipping_label", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 1, $this->source); })()), "id", [], "any", false, false, false, 1)]), "html", null, true);
        echo "\" class=\"ui icon teal labeled tiny fluid button\" style=\"margin-bottom: 5px\">
    <i class=\"file pdf outline icon\"></i>
        ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("bitbag.ui.download_shipping_label"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 3, $this->source); })()), "externalId", [], "any", false, false, false, 3), "html", null, true);
        echo "
</a>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_shipping_details_download.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<a href=\"{{ path('bitbag_admin_get_shipping_label', {'id' : data.id}) }}\" class=\"ui icon teal labeled tiny fluid button\" style=\"margin-bottom: 5px\">
    <i class=\"file pdf outline icon\"></i>
        {{ 'bitbag.ui.download_shipping_label'|trans }} {{ data.externalId }}
</a>
", "bundles/BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_shipping_details_download.html.twig", "/var/www/html/templates/bundles/BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_shipping_details_download.html.twig");
    }
}
