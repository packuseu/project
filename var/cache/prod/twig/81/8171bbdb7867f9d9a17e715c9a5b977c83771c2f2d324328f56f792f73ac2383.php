<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Checkout:_summary.html.twig */
class __TwigTemplate_31d4c9e99f17b90fa1e9a2072fe589dc78f7abee47a61b438ff5eca7ec52449e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Checkout:_summary.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Checkout:_summary.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["itemsSubtotal"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderItemsSubtotalExtension']->getSubtotal((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 3, $this->source); })()));
        // line 4
        $context["taxIncluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getIncludedTax((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 4, $this->source); })()));
        // line 5
        $context["taxExcluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getExcludedTax((isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 5, $this->source); })()));
        // line 6
        echo "
<div class=\"card\">
    <table class=\"table\">
        <thead class=\"card-header\">
            <tr>
                <th class=\"border-0\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.item"), "html", null, true);
        echo "</th>
                <th class=\"border-0 text-right\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.quantity"), "html", null, true);
        echo "</th>
                <th class=\"border-0 text-right\">";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.subtotal"), "html", null, true);
        echo "</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 17, $this->source); })()), "items", [], "any", false, false, false, 17));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 18
            echo "            <tr>
                <td class=\"text-dark\">";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "getVariant", [], "any", false, false, false, 19), "product", [], "any", false, false, false, 19), "name", [], "any", false, false, false, 19), "html", null, true);
            echo "</td>
                <td class=\"text-right\">
                    ";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 21), "html", null, true);
            echo "
                </td>
                <td class=\"text-right\">";
            // line 23
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, $context["item"], "subtotal", [], "any", false, false, false, 23)], 23, $context, $this->getSourceContext());
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "        </tbody>
        <tfoot>
        <tr>
            <td colspan=\"1\" class=\"border-top border-dark\">
                <strong>";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.items_total"), "html", null, true);
        echo ":</strong>
            </td>
            <td colspan=\"2\" class=\"text-right border-top border-dark\">
                ";
        // line 33
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["itemsSubtotal"]) || array_key_exists("itemsSubtotal", $context) ? $context["itemsSubtotal"] : (function () { throw new RuntimeError('Variable "itemsSubtotal" does not exist.', 33, $this->source); })())], 33, $context, $this->getSourceContext());
        echo "
            </td>
        </tr>
        <tr ";
        // line 36
        if (((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 36, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 36, $this->source); })()))) {
            echo "class=\"bg-light\" ";
        }
        echo ">
            <td colspan=\"1\">
                <strong>";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.taxes_total"), "html", null, true);
        echo ":</strong>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                ";
        // line 41
        if (( !(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 41, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 41, $this->source); })()))) {
            // line 42
            echo "                    <div>";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [0], 42, $context, $this->getSourceContext());
            echo "</div>
                ";
        }
        // line 44
        echo "                ";
        if ((isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 44, $this->source); })())) {
            // line 45
            echo "                    <div>";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 45, $this->source); })())], 45, $context, $this->getSourceContext());
            echo "</div>
                ";
        }
        // line 47
        echo "                ";
        if ((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 47, $this->source); })())) {
            // line 48
            echo "                    <div>
                        <small>(";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.included_in_price"), "html", null, true);
            echo ")</small>
                        <span>";
            // line 50
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 50, $this->source); })())], 50, $context, $this->getSourceContext());
            echo "</span>
                    </div>
                ";
        }
        // line 53
        echo "            </td>
        </tr>
        <tr>
            <td colspan=\"1\">
                <strong>";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.discount"), "html", null, true);
        echo ":</strong>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                ";
        // line 60
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 60, $this->source); })()), "orderPromotionTotal", [], "any", false, false, false, 60)], 60, $context, $this->getSourceContext());
        echo "
            </td>
        </tr>
        <tr>
            <td colspan=\"1\">
                <strong>";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_estimated_cost"), "html", null, true);
        echo ":</strong>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                ";
        // line 68
        if ((twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 68, $this->source); })()), "getAdjustmentsTotal", [0 => "shipping"], "method", false, false, false, 68) > twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 68, $this->source); })()), "shippingTotal", [], "any", false, false, false, 68))) {
            // line 69
            echo "                    <small><s>";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 69, $this->source); })()), "getAdjustmentsTotal", [0 => "shipping"], "method", false, false, false, 69)], 69, $context, $this->getSourceContext());
            echo "</s></small>
                ";
        }
        // line 71
        echo "                <span>";
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 71, $this->source); })()), "shippingTotal", [], "any", false, false, false, 71)], 71, $context, $this->getSourceContext());
        echo "</span>
            </td>
        </tr>
        <tr>
            <td colspan=\"1\">
                <div class=\"h3\">";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.order_total"), "html", null, true);
        echo ":</div>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                <div class=\"h3\">";
        // line 79
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 79, $this->source); })()), "total", [], "any", false, false, false, 79)], 79, $context, $this->getSourceContext());
        echo "</div>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Checkout:_summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  212 => 79,  206 => 76,  197 => 71,  191 => 69,  189 => 68,  183 => 65,  175 => 60,  169 => 57,  163 => 53,  157 => 50,  153 => 49,  150 => 48,  147 => 47,  141 => 45,  138 => 44,  132 => 42,  130 => 41,  124 => 38,  117 => 36,  111 => 33,  105 => 30,  99 => 26,  90 => 23,  85 => 21,  80 => 19,  77 => 18,  73 => 17,  66 => 13,  62 => 12,  58 => 11,  51 => 6,  49 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}

{% set itemsSubtotal = sylius_order_items_subtotal(order) %}
{% set taxIncluded = sylius_order_tax_included(order) %}
{% set taxExcluded = sylius_order_tax_excluded(order) %}

<div class=\"card\">
    <table class=\"table\">
        <thead class=\"card-header\">
            <tr>
                <th class=\"border-0\">{{ 'sylius.ui.item'|trans }}</th>
                <th class=\"border-0 text-right\">{{ 'sylius.ui.quantity'|trans }}</th>
                <th class=\"border-0 text-right\">{{ 'sylius.ui.subtotal'|trans }}</th>
            </tr>
        </thead>
        <tbody>
        {% for item in order.items %}
            <tr>
                <td class=\"text-dark\">{{ item.getVariant.product.name }}</td>
                <td class=\"text-right\">
                    {{ item.quantity }}
                </td>
                <td class=\"text-right\">{{ money.convertAndFormat(item.subtotal) }}</td>
            </tr>
        {% endfor %}
        </tbody>
        <tfoot>
        <tr>
            <td colspan=\"1\" class=\"border-top border-dark\">
                <strong>{{ 'sylius.ui.items_total'|trans }}:</strong>
            </td>
            <td colspan=\"2\" class=\"text-right border-top border-dark\">
                {{ money.convertAndFormat(itemsSubtotal) }}
            </td>
        </tr>
        <tr {% if taxIncluded and not taxExcluded %}class=\"bg-light\" {% endif %}>
            <td colspan=\"1\">
                <strong>{{ 'sylius.ui.taxes_total'|trans }}:</strong>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                {% if not taxIncluded and not taxExcluded %}
                    <div>{{ money.convertAndFormat(0) }}</div>
                {% endif %}
                {% if taxExcluded %}
                    <div>{{ money.convertAndFormat(taxExcluded) }}</div>
                {% endif %}
                {% if taxIncluded %}
                    <div>
                        <small>({{ 'sylius.ui.included_in_price'|trans }})</small>
                        <span>{{ money.convertAndFormat(taxIncluded) }}</span>
                    </div>
                {% endif %}
            </td>
        </tr>
        <tr>
            <td colspan=\"1\">
                <strong>{{ 'sylius.ui.discount'|trans }}:</strong>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                {{ money.convertAndFormat(order.orderPromotionTotal) }}
            </td>
        </tr>
        <tr>
            <td colspan=\"1\">
                <strong>{{ 'sylius.ui.shipping_estimated_cost'|trans }}:</strong>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                {% if order.getAdjustmentsTotal('shipping') > order.shippingTotal %}
                    <small><s>{{ money.convertAndFormat(order.getAdjustmentsTotal('shipping')) }}</s></small>
                {% endif %}
                <span>{{ money.convertAndFormat(order.shippingTotal) }}</span>
            </td>
        </tr>
        <tr>
            <td colspan=\"1\">
                <div class=\"h3\">{{ 'sylius.ui.order_total'|trans }}:</div>
            </td>
            <td colspan=\"2\" class=\"text-right\">
                <div class=\"h3\">{{ money.convertAndFormat(order.total) }}</div>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
", "SyliusShopBundle:Checkout:_summary.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Checkout/_summary.html.twig");
    }
}
