<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusBannerPlugin/PositionTypes/_half_screen_adoptive_grid.html.twig */
class __TwigTemplate_0239fcb64127df2ac5b21083e12ca58ef545aa91efbc1fbcb3a9266858e7a4ba extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusBannerPlugin/PositionTypes/_half_screen_adoptive_grid.html.twig"));

        // line 7
        echo "
";
        // line 17
        echo "
";
        // line 43
        echo "
";
        // line 69
        echo "
";
        // line 102
        echo "
";
        // line 103
        $macros["row"] = $this->macros["row"] = $this;
        // line 104
        echo "
";
        // line 105
        $context["imagesCounter"] = 0;
        // line 106
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 106, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 106, $this->source); })()), "channel", [], "array", false, false, false, 106)], "method", false, false, false, 106)) {
                // line 107
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 107));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 108
                    echo "        ";
                    $context["imagesCounter"] = ((isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 108, $this->source); })()) + 1);
                    // line 109
                    echo "    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 111
        echo "
";
        // line 112
        if ((isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 112, $this->source); })())) {
            // line 113
            echo "    <div class=\"col-12 col-md-6 banners-grid\">
        <div class=\"row\">
            ";
            // line 115
            $context["counter"] = 0;
            // line 116
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 116, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 116, $this->source); })()), "channel", [], "array", false, false, false, 116)], "method", false, false, false, 116)) {
                    // line 117
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 117));
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        // line 118
                        echo "                    ";
                        $context["counter"] = ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 118, $this->source); })()) + 1);
                        // line 119
                        echo "
                    ";
                        // line 120
                        if (((isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 120, $this->source); })()) == 1)) {
                            // line 121
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_one_col", [$context["image"]], 121, $context, $this->getSourceContext());
                            echo "
                    ";
                        } elseif ((                        // line 122
(isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 122, $this->source); })()) == 2)) {
                            // line 123
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_two_cols", [$context["image"], false], 123, $context, $this->getSourceContext());
                            echo "
                    ";
                        } elseif ((                        // line 124
(isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 124, $this->source); })()) == 3)) {
                            // line 125
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_three_cols", [$context["image"], (isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 125, $this->source); })())], 125, $context, $this->getSourceContext());
                            echo "
                    ";
                        } elseif (((                        // line 126
(isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 126, $this->source); })()) > 3) && ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 126, $this->source); })()) < 5))) {
                            // line 127
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_two_cols", [$context["image"], true], 127, $context, $this->getSourceContext());
                            echo "
                    ";
                        }
                        // line 129
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 130
                    echo "            ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 131
            echo "        </div>
    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function macro_card_img($__imgPath__ = null, $__small__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "imgPath" => $__imgPath__,
            "small" => $__small__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "card_img"));

            // line 3
            echo "    <div class=\"card-bg-img ";
            if ((isset($context["small"]) || array_key_exists("small", $context) ? $context["small"] : (function () { throw new RuntimeError('Variable "small" does not exist.', 3, $this->source); })())) {
                echo "bg--small";
            }
            echo "\"
         style=\"background-image: url(";
            // line 4
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter((isset($context["imgPath"]) || array_key_exists("imgPath", $context) ? $context["imgPath"] : (function () { throw new RuntimeError('Variable "imgPath" does not exist.', 4, $this->source); })()), "omni_sylius_banner"), "html", null, true);
            echo ");\"
    ></div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 9
    public function macro_card_body($__content__ = null, $__contentBackground__ = null, $__size__ = null, $__contentPosition__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "content" => $__content__,
            "contentBackground" => $__contentBackground__,
            "size" => $__size__,
            "contentPosition" => $__contentPosition__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "card_body"));

            // line 10
            echo "    <div class=\"card-body body--";
            echo twig_escape_filter($this->env, (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 10, $this->source); })()), "html", null, true);
            echo "\"
         style=\"";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 11, $this->source); })()), "html", null, true);
            echo " background-color: ";
            echo twig_escape_filter($this->env, (isset($context["contentBackground"]) || array_key_exists("contentBackground", $context) ? $context["contentBackground"] : (function () { throw new RuntimeError('Variable "contentBackground" does not exist.', 11, $this->source); })()), "html", null, true);
            echo "\"
    >
        ";
            // line 14
            echo "        ";
            echo (isset($context["content"]) || array_key_exists("content", $context) ? $context["content"] : (function () { throw new RuntimeError('Variable "content" does not exist.', 14, $this->source); })());
            echo "
    </div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 19
    public function macro_one_col($__image__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "image" => $__image__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "one_col"));

            // line 20
            echo "    ";
            $macros["card"] = $this;
            // line 21
            echo "
    <div class=\"col-12\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"";
            // line 24
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 24), "link", [], "any", true, true, false, 24)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 24), "link", [], "any", false, false, false, 24), "javascript:;")) : ("javascript:;")), "html", null, true);
            echo "\">
                ";
            // line 25
            echo twig_call_macro($macros["card"], "macro_card_img", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 25, $this->source); })()), "path", [], "any", false, false, false, 25), false], 25, $context, $this->getSourceContext());
            echo "

                ";
            // line 27
            if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 27, $this->source); })()), "contentSpace", [], "any", false, false, false, 27)) && (twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 27, $this->source); })()), "contentSpace", [], "any", false, false, false, 27) > 0))) {
                // line 28
                echo "                    ";
                // line 29
                echo "                    ";
                $context["size"] = (("height: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 29, $this->source); })()), "contentSpace", [], "any", false, false, false, 29))) . "%;");
                // line 30
                echo "                    ";
                $context["contentPosition"] = twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 30, $this->source); })()), "contentPosition", [], "any", false, false, false, 30);
                // line 31
                echo "
                    ";
                // line 33
                echo "                    ";
                if ((((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 33, $this->source); })()) == "left") || ((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 33, $this->source); })()) == "right"))) {
                    // line 34
                    echo "                        ";
                    $context["size"] = (("width: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 34, $this->source); })()), "contentSpace", [], "any", false, false, false, 34))) . "%;");
                    // line 35
                    echo "                    ";
                }
                // line 36
                echo "
                    ";
                // line 37
                echo twig_call_macro($macros["card"], "macro_card_body", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 37, $this->source); })()), "content", [], "any", false, false, false, 37), twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 37, $this->source); })()), "contentBackground", [], "any", false, false, false, 37), (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 37, $this->source); })()), (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 37, $this->source); })())], 37, $context, $this->getSourceContext());
                echo "
                ";
            }
            // line 39
            echo "            </a>
        </div>
    </div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 45
    public function macro_two_cols($__image__ = null, $__isSmallImg__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "image" => $__image__,
            "isSmallImg" => $__isSmallImg__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "two_cols"));

            // line 46
            echo "    ";
            $macros["card"] = $this;
            // line 47
            echo "
    <div class=\"col-6\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"";
            // line 50
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 50), "link", [], "any", true, true, false, 50)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 50), "link", [], "any", false, false, false, 50), "javascript:;")) : ("javascript:;")), "html", null, true);
            echo "\">
                ";
            // line 51
            echo twig_call_macro($macros["card"], "macro_card_img", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 51, $this->source); })()), "path", [], "any", false, false, false, 51), (isset($context["isSmallImg"]) || array_key_exists("isSmallImg", $context) ? $context["isSmallImg"] : (function () { throw new RuntimeError('Variable "isSmallImg" does not exist.', 51, $this->source); })())], 51, $context, $this->getSourceContext());
            echo "

                ";
            // line 53
            if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 53, $this->source); })()), "contentSpace", [], "any", false, false, false, 53)) && (twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 53, $this->source); })()), "contentSpace", [], "any", false, false, false, 53) > 0))) {
                // line 54
                echo "                    ";
                // line 55
                echo "                    ";
                $context["size"] = (("height: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 55, $this->source); })()), "contentSpace", [], "any", false, false, false, 55))) . "%;");
                // line 56
                echo "                    ";
                $context["contentPosition"] = twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 56, $this->source); })()), "contentPosition", [], "any", false, false, false, 56);
                // line 57
                echo "
                    ";
                // line 59
                echo "                    ";
                if ((((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 59, $this->source); })()) == "left") || ((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 59, $this->source); })()) == "right"))) {
                    // line 60
                    echo "                        ";
                    $context["contentPosition"] = "bottom";
                    // line 61
                    echo "                    ";
                }
                // line 62
                echo "
                    ";
                // line 63
                echo twig_call_macro($macros["card"], "macro_card_body", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 63, $this->source); })()), "content", [], "any", false, false, false, 63), twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 63, $this->source); })()), "contentBackground", [], "any", false, false, false, 63), (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 63, $this->source); })()), (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 63, $this->source); })())], 63, $context, $this->getSourceContext());
                echo "
                ";
            }
            // line 65
            echo "            </a>
        </div>
    </div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 71
    public function macro_three_cols($__image__ = null, $__counter__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "image" => $__image__,
            "counter" => $__counter__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "three_cols"));

            // line 72
            echo "    ";
            $macros["card"] = $this;
            // line 73
            echo "
    ";
            // line 74
            if ((((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 74, $this->source); })()) == 1) || ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 74, $this->source); })()) == 2))) {
                // line 75
                echo "        <div class=\"col-6\">
    ";
            }
            // line 77
            echo "
    <div class=\"card\">
        <a class=\"card-link\" href=\"";
            // line 79
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 79), "link", [], "any", true, true, false, 79)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 79), "link", [], "any", false, false, false, 79), "javascript:;")) : ("javascript:;")), "html", null, true);
            echo "\">
            ";
            // line 80
            $context["isSmall"] = (((((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 80, $this->source); })()) == 2) || ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 80, $this->source); })()) == 3))) ? (true) : (false));
            // line 81
            echo "            ";
            echo twig_call_macro($macros["card"], "macro_card_img", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 81, $this->source); })()), "path", [], "any", false, false, false, 81), (isset($context["isSmall"]) || array_key_exists("isSmall", $context) ? $context["isSmall"] : (function () { throw new RuntimeError('Variable "isSmall" does not exist.', 81, $this->source); })())], 81, $context, $this->getSourceContext());
            echo "

            ";
            // line 83
            if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 83, $this->source); })()), "contentSpace", [], "any", false, false, false, 83)) && (twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 83, $this->source); })()), "contentSpace", [], "any", false, false, false, 83) > 0))) {
                // line 84
                echo "                ";
                // line 85
                echo "                ";
                $context["size"] = (("height: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 85, $this->source); })()), "contentSpace", [], "any", false, false, false, 85))) . "%;");
                // line 86
                echo "                ";
                $context["contentPosition"] = twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 86, $this->source); })()), "contentPosition", [], "any", false, false, false, 86);
                // line 87
                echo "
                ";
                // line 89
                echo "                ";
                if ((((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 89, $this->source); })()) == "left") || ((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 89, $this->source); })()) == "right"))) {
                    // line 90
                    echo "                    ";
                    $context["contentPosition"] = "bottom";
                    // line 91
                    echo "                ";
                }
                // line 92
                echo "
                ";
                // line 93
                echo twig_call_macro($macros["card"], "macro_card_body", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 93, $this->source); })()), "content", [], "any", false, false, false, 93), twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 93, $this->source); })()), "contentBackground", [], "any", false, false, false, 93), (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 93, $this->source); })()), (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 93, $this->source); })())], 93, $context, $this->getSourceContext());
                echo "
            ";
            }
            // line 95
            echo "        </a>
    </div>

    ";
            // line 98
            if ((((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 98, $this->source); })()) == 1) || ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 98, $this->source); })()) == 3))) {
                // line 99
                echo "        </div>
    ";
            }
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OmniSyliusBannerPlugin/PositionTypes/_half_screen_adoptive_grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  491 => 99,  489 => 98,  484 => 95,  479 => 93,  476 => 92,  473 => 91,  470 => 90,  467 => 89,  464 => 87,  461 => 86,  458 => 85,  456 => 84,  454 => 83,  448 => 81,  446 => 80,  442 => 79,  438 => 77,  434 => 75,  432 => 74,  429 => 73,  426 => 72,  409 => 71,  394 => 65,  389 => 63,  386 => 62,  383 => 61,  380 => 60,  377 => 59,  374 => 57,  371 => 56,  368 => 55,  366 => 54,  364 => 53,  359 => 51,  355 => 50,  350 => 47,  347 => 46,  330 => 45,  315 => 39,  310 => 37,  307 => 36,  304 => 35,  301 => 34,  298 => 33,  295 => 31,  292 => 30,  289 => 29,  287 => 28,  285 => 27,  280 => 25,  276 => 24,  271 => 21,  268 => 20,  252 => 19,  236 => 14,  229 => 11,  224 => 10,  205 => 9,  190 => 4,  183 => 3,  166 => 2,  156 => 131,  149 => 130,  143 => 129,  137 => 127,  135 => 126,  130 => 125,  128 => 124,  123 => 123,  121 => 122,  116 => 121,  114 => 120,  111 => 119,  108 => 118,  103 => 117,  97 => 116,  95 => 115,  91 => 113,  89 => 112,  86 => 111,  75 => 109,  72 => 108,  67 => 107,  62 => 106,  60 => 105,  57 => 104,  55 => 103,  52 => 102,  49 => 69,  46 => 43,  43 => 17,  40 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("{# render card image #}
{% macro card_img(imgPath, small) %}
    <div class=\"card-bg-img {% if small %}bg--small{% endif %}\"
         style=\"background-image: url({{ imgPath|imagine_filter('omni_sylius_banner') }});\"
    ></div>
{% endmacro %}

{# render card body #}
{% macro card_body(content, contentBackground, size, contentPosition) %}
    <div class=\"card-body body--{{ contentPosition }}\"
         style=\"{{ size }} background-color: {{ contentBackground }}\"
    >
        {#<h5 class=\"card-title\">{{ content|raw }}</h5>#}
        {{ content|raw }}
    </div>
{% endmacro %}

{# render grid col-12 #}
{% macro one_col(image) %}
    {% import _self as card %}

    <div class=\"col-12\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                {{ card.card_img(image.path, false) }}

                {% if image.contentSpace is not null and image.contentSpace > 0 %}
                    {# if position is top or bottom, contentSpace is height #}
                    {% set size = 'height: ' ~ image.contentSpace|number_format ~ '%;' %}
                    {% set contentPosition = image.contentPosition %}

                    {# if position is left or right, contentSpace is width #}
                    {% if contentPosition == 'left' or contentPosition == 'right' %}
                        {% set size = 'width: ' ~ image.contentSpace|number_format ~ '%;' %}
                    {% endif %}

                    {{ card.card_body(image.content, image.contentBackground, size, contentPosition) }}
                {% endif %}
            </a>
        </div>
    </div>
{% endmacro %}

{# render grid col-6 #}
{% macro two_cols(image, isSmallImg) %}
    {% import _self as card %}

    <div class=\"col-6\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                {{ card.card_img(image.path, isSmallImg) }}

                {% if image.contentSpace is not null and image.contentSpace > 0 %}
                    {# contentSpace is height #}
                    {% set size = 'height: ' ~ image.contentSpace|number_format ~ '%;' %}
                    {% set contentPosition = image.contentPosition %}

                    {# if position is left or right, set it to bottom #}
                    {% if contentPosition == 'left' or contentPosition == 'right' %}
                        {% set contentPosition = 'bottom' %}
                    {% endif %}

                    {{ card.card_body(image.content, image.contentBackground, size, contentPosition) }}
                {% endif %}
            </a>
        </div>
    </div>
{% endmacro %}

{# render grid special layout for 3 columns #}
{% macro three_cols(image, counter) %}
    {% import _self as card %}

    {% if counter == 1 or counter == 2 %}
        <div class=\"col-6\">
    {% endif %}

    <div class=\"card\">
        <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
            {% set isSmall = (counter == 2 or counter == 3) ? true : false %}
            {{ card.card_img(image.path, isSmall) }}

            {% if image.contentSpace is not null and image.contentSpace > 0 %}
                {# contentSpace is height #}
                {% set size = 'height: ' ~ image.contentSpace|number_format ~ '%;' %}
                {% set contentPosition = image.contentPosition %}

                {# if position is left or right, set it to bottom #}
                {% if contentPosition == 'left' or contentPosition == 'right' %}
                    {% set contentPosition = 'bottom' %}
                {% endif %}

                {{ card.card_body(image.content, image.contentBackground, size, contentPosition) }}
            {% endif %}
        </a>
    </div>

    {% if counter == 1 or counter == 3 %}
        </div>
    {% endif %}
{% endmacro %}

{% import _self as row %}

{% set imagesCounter = 0 %}
{% for banner in banners if banner.hasChannelCode(options['channel']) %}
    {% for image in banner.images %}
        {% set imagesCounter = imagesCounter + 1 %}
    {% endfor %}
{% endfor %}

{% if imagesCounter %}
    <div class=\"col-12 col-md-6 banners-grid\">
        <div class=\"row\">
            {% set counter = 0 %}
            {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                {% for image in banner.images %}
                    {% set counter = counter + 1 %}

                    {% if imagesCounter == 1 %}
                        {{ row.one_col(image) }}
                    {% elseif imagesCounter == 2 %}
                        {{ row.two_cols(image, false) }}
                    {% elseif imagesCounter == 3 %}
                        {{ row.three_cols(image, counter) }}
                    {% elseif imagesCounter > 3 and counter < 5 %}
                        {{ row.two_cols(image, true) }}
                    {% endif %}
                {% endfor %}
            {% endfor %}
        </div>
    </div>
{% endif %}
", "@OmniSyliusBannerPlugin/PositionTypes/_half_screen_adoptive_grid.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/OmniSyliusBannerPlugin/views/PositionTypes/_half_screen_adoptive_grid.html.twig");
    }
}
