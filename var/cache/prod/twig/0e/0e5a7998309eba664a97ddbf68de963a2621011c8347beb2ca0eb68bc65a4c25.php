<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusManifestPlugin/manifest.html.twig */
class __TwigTemplate_83f96f19472d00dd90fbf7b11b8c2dfda1e1a0e4670b1c595828f42f3cb83af0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusManifestPlugin/manifest.html.twig"));

        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <style>
        table {
            width: 100%;
            margin-top: 30px;
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }

        th td {
            font-weight: bold;
        }

        th, td {
            padding: 5px;
        }

        p {
            padding: 0;
            margin: 0;
        }
    </style>
</head>
<body>
    ";
        // line 34
        $context["shippingGateway"] = twig_get_attribute($this->env, $this->source, (isset($context["manifest"]) || array_key_exists("manifest", $context) ? $context["manifest"] : (function () { throw new RuntimeError('Variable "manifest" does not exist.', 34, $this->source); })()), "shippingGateway", [], "any", false, false, false, 34);
        // line 35
        echo "    <p align=\"right\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius_manifest.template.total_parcels"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["manifest"]) || array_key_exists("manifest", $context) ? $context["manifest"] : (function () { throw new RuntimeError('Variable "manifest" does not exist.', 35, $this->source); })()), "shippingExports", [], "any", false, false, false, 35)), "html", null, true);
        echo "</p>
    <p><strong>";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius_manifest.template.client_name"), "html", null, true);
        echo ":</strong> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shippingGateway"]) || array_key_exists("shippingGateway", $context) ? $context["shippingGateway"] : (function () { throw new RuntimeError('Variable "shippingGateway" does not exist.', 36, $this->source); })()), "configValue", [0 => "name"], "method", false, false, false, 36), "html", null, true);
        echo "</p>
    <p><strong>";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius_manifest.template.asembly"), "html", null, true);
        echo ":</strong> ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shippingGateway"]) || array_key_exists("shippingGateway", $context) ? $context["shippingGateway"] : (function () { throw new RuntimeError('Variable "shippingGateway" does not exist.', 37, $this->source); })()), "configValue", [0 => "street"], "method", false, false, false, 37), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shippingGateway"]) || array_key_exists("shippingGateway", $context) ? $context["shippingGateway"] : (function () { throw new RuntimeError('Variable "shippingGateway" does not exist.', 37, $this->source); })()), "configValue", [0 => "post_code"], "method", false, false, false, 37), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shippingGateway"]) || array_key_exists("shippingGateway", $context) ? $context["shippingGateway"] : (function () { throw new RuntimeError('Variable "shippingGateway" does not exist.', 37, $this->source); })()), "configValue", [0 => "city"], "method", false, false, false, 37), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shippingGateway"]) || array_key_exists("shippingGateway", $context) ? $context["shippingGateway"] : (function () { throw new RuntimeError('Variable "shippingGateway" does not exist.', 37, $this->source); })()), "configValue", [0 => "country"], "method", false, false, false, 37), "html", null, true);
        echo "</p>
    <p><strong>";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.date"), "html", null, true);
        echo ":</strong> ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["manifest"]) || array_key_exists("manifest", $context) ? $context["manifest"] : (function () { throw new RuntimeError('Variable "manifest" does not exist.', 38, $this->source); })()), "createdAt", [], "any", false, false, false, 38), "Y-m-d H:i"), "html", null, true);
        echo "</p>

    <table>
        <tr>
            <th>";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius_manifest.ui.number"), "html", null, true);
        echo "</th>
            <th>";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius_manifest.ui.tracking_code"), "html", null, true);
        echo "</th>
            <th>";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius_manifest.ui.receiver_name"), "html", null, true);
        echo "</th>
            <th>";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius_manifest.ui.receiver_address"), "html", null, true);
        echo "</th>
        </tr>
        ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["manifest"]) || array_key_exists("manifest", $context) ? $context["manifest"] : (function () { throw new RuntimeError('Variable "manifest" does not exist.', 47, $this->source); })()), "shippingExports", [], "any", false, false, false, 47));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["export"]) {
            // line 48
            echo "            ";
            $context["address"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["export"], "shipment", [], "any", false, false, false, 48), "order", [], "any", false, false, false, 48), "shippingAddress", [], "any", false, false, false, 48);
            // line 49
            echo "            <tr>
                <td>";
            // line 50
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 50), "html", null, true);
            echo "</td>
                <td>";
            // line 51
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["export"], "shipment", [], "any", false, false, false, 51), "tracking", [], "any", false, false, false, 51), "html", null, true);
            echo "</td>
                <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 52, $this->source); })()), "fullName", [], "any", false, false, false, 52), "html", null, true);
            echo "</td>
                <td>";
            // line 53
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 53, $this->source); })()), "street", [], "any", false, false, false, 53), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 53, $this->source); })()), "postCode", [], "any", false, false, false, 53), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 53, $this->source); })()), "city", [], "any", false, false, false, 53), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 53, $this->source); })()), "countryCode", [], "any", false, false, false, 53), "html", null, true);
            echo "</td>
            </tr>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['export'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 56
        echo "    </table>
</body>
</html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusManifestPlugin/manifest.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 56,  163 => 53,  159 => 52,  155 => 51,  151 => 50,  148 => 49,  145 => 48,  128 => 47,  123 => 45,  119 => 44,  115 => 43,  111 => 42,  102 => 38,  90 => 37,  84 => 36,  77 => 35,  75 => 34,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!doctype html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\"
          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
    <style>
        table {
            width: 100%;
            margin-top: 30px;
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }

        th td {
            font-weight: bold;
        }

        th, td {
            padding: 5px;
        }

        p {
            padding: 0;
            margin: 0;
        }
    </style>
</head>
<body>
    {% set shippingGateway = manifest.shippingGateway %}
    <p align=\"right\">{{ 'omni_sylius_manifest.template.total_parcels'|trans }}: {{ manifest.shippingExports|length }}</p>
    <p><strong>{{ 'omni_sylius_manifest.template.client_name'|trans }}:</strong> {{ shippingGateway.configValue('name') }}</p>
    <p><strong>{{ 'omni_sylius_manifest.template.asembly'|trans }}:</strong> {{ shippingGateway.configValue('street') }} {{ shippingGateway.configValue('post_code') }}, {{ shippingGateway.configValue('city') }}, {{ shippingGateway.configValue('country') }}</p>
    <p><strong>{{ 'sylius.ui.date'|trans }}:</strong> {{ manifest.createdAt|date('Y-m-d H:i') }}</p>

    <table>
        <tr>
            <th>{{ 'omni_sylius_manifest.ui.number'|trans }}</th>
            <th>{{ 'omni_sylius_manifest.ui.tracking_code'|trans }}</th>
            <th>{{ 'omni_sylius_manifest.ui.receiver_name'|trans }}</th>
            <th>{{ 'omni_sylius_manifest.ui.receiver_address'|trans }}</th>
        </tr>
        {% for export in manifest.shippingExports %}
            {% set address = export.shipment.order.shippingAddress %}
            <tr>
                <td>{{ loop.index }}</td>
                <td>{{ export.shipment.tracking }}</td>
                <td>{{ address.fullName }}</td>
                <td>{{ address.street }} {{ address.postCode }}, {{ address.city }}, {{ address.countryCode }}</td>
            </tr>
        {% endfor %}
    </table>
</body>
</html>
", "@OmniSyliusManifestPlugin/manifest.html.twig", "/var/www/html/vendor/omni/sylius-manifest-plugin/src/Resources/views/manifest.html.twig");
    }
}
