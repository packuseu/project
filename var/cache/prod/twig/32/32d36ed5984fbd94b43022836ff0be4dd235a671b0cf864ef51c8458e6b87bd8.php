<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/OmniSyliusSearchPlugin/SearchResultSnippets/_product.html.twig */
class __TwigTemplate_7b3da6f5ba561b8d6ae526bbf4886c2bd52ec5dfda8b067be1826a3b2c055c23 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/OmniSyliusSearchPlugin/SearchResultSnippets/_product.html.twig"));

        // line 1
        echo "<a
    href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath($this->extensions['Omni\Sylius\SearchPlugin\Twig\IndexExtension']->getRoute((isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 2, $this->source); })())), ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 2, $this->source); })()), "translation", [], "any", false, false, false, 2), "slug", [], "any", false, false, false, 2)]), "html", null, true);
        echo "\"
    class=\"search-result-grid__item\"
>
    ";
        // line 5
        $context["title"] = "";
        // line 6
        echo "
    ";
        // line 7
        if ((twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "name", [], "any", true, true, false, 7) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 7, $this->source); })()), "name", [], "any", false, false, false, 7)))) {
            // line 8
            echo "        ";
            $context["title"] = twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 8, $this->source); })()), "name", [], "any", false, false, false, 8);
            // line 9
            echo "    ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "title", [], "any", true, true, false, 9) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 9, $this->source); })()), "title", [], "any", false, false, false, 9)))) {
            // line 10
            echo "        ";
            $context["title"] = twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 10, $this->source); })()), "title", [], "any", false, false, false, 10);
            // line 11
            echo "    ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "translation", [], "any", false, true, false, 11), "title", [], "any", true, true, false, 11) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 11, $this->source); })()), "translation", [], "any", false, false, false, 11), "title", [], "any", false, false, false, 11)))) {
            // line 12
            echo "        ";
            $context["title"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 12, $this->source); })()), "translation", [], "any", false, false, false, 12), "title", [], "any", false, false, false, 12);
            // line 13
            echo "    ";
        }
        // line 14
        echo "    <img src=\"";
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 14, $this->source); })()), "images", [], "any", false, false, false, 14), "first", [], "any", false, false, false, 14)) ? ($this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 14, $this->source); })()), "images", [], "any", false, false, false, 14), "first", [], "any", false, false, false, 14), "path", [], "any", false, false, false, 14), "product_thumbnail_white_border")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bootstrap-theme/images/product-placeholder.png", "bootstrapTheme"))), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 14, $this->source); })()), "html", null, true);
        echo "\" class=\"search-result-grid__item__img\">
    <div class=\"search-result-grid__item__content\">
        <h6 class=\"search-result-grid__item__content__title\">
            ";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 17, $this->source); })()), "html", null, true);
        echo "
        </h6>
        <div class=\"search-result-grid__item__content__label\">
            ";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.view_more"), "html", null, true);
        echo "
        </div>
    </div>
</a>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/OmniSyliusSearchPlugin/SearchResultSnippets/_product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 20,  83 => 17,  74 => 14,  71 => 13,  68 => 12,  65 => 11,  62 => 10,  59 => 9,  56 => 8,  54 => 7,  51 => 6,  49 => 5,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<a
    href=\"{{ path(omni_sylius_search_get_route(resource), {slug: resource.translation.slug}) }}\"
    class=\"search-result-grid__item\"
>
    {% set title = '' %}

    {% if resource.name is defined and resource.name is not null %}
        {% set title = resource.name %}
    {% elseif resource.title is defined and resource.title is not null %}
        {% set title = resource.title %}
    {% elseif resource.translation.title is defined and resource.translation.title is not null %}
        {% set title = resource.translation.title %}
    {% endif %}
    <img src=\"{{ resource.images.first ? resource.images.first.path|imagine_filter('product_thumbnail_white_border') : asset('bootstrap-theme/images/product-placeholder.png', 'bootstrapTheme') }}\" alt=\"{{ title }}\" class=\"search-result-grid__item__img\">
    <div class=\"search-result-grid__item__content\">
        <h6 class=\"search-result-grid__item__content__title\">
            {{ title }}
        </h6>
        <div class=\"search-result-grid__item__content__label\">
            {{ 'sylius.ui.view_more'|trans }}
        </div>
    </div>
</a>
", "bundles/OmniSyliusSearchPlugin/SearchResultSnippets/_product.html.twig", "/var/www/html/templates/bundles/OmniSyliusSearchPlugin/SearchResultSnippets/_product.html.twig");
    }
}
