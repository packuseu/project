<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusBannerPlugin:Admin/BannerPosition:banner_type_theme.html.twig */
class __TwigTemplate_f0adfab1b130232d821fa121067a89265f7798906db51d440e3cf6960ff1bf3a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_banner_position_type_entry_widget' => [$this, 'block__banner_position_type_entry_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusBannerPlugin:Admin/BannerPosition:banner_type_theme.html.twig"));

        // line 1
        $this->displayBlock('_banner_position_type_entry_widget', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block__banner_position_type_entry_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_banner_position_type_entry_widget"));

        // line 2
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 2, $this->source); })()), 'widget');
        echo "

    ";
        // line 4
        $context["currentType"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "vars", [], "any", false, false, false, 4), "value", [], "any", false, false, false, 4);
        // line 5
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "parent", [], "any", false, false, false, 5), "vars", [], "any", false, false, false, 5), "types_config", [], "any", false, false, false, 5));
        foreach ($context['_seq'] as $context["type"] => $context["config"]) {
            // line 6
            echo "        ";
            if (($context["type"] == (isset($context["currentType"]) || array_key_exists("currentType", $context) ? $context["currentType"] : (function () { throw new RuntimeError('Variable "currentType" does not exist.', 6, $this->source); })()))) {
                // line 7
                echo "            <img src=\"";
                echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "request", [], "any", false, false, false, 7), "getSchemeAndHttpHost", [], "any", false, false, false, 7) . "/") . twig_get_attribute($this->env, $this->source, $context["config"], "example_image", [], "any", false, false, false, 7)), "html", null, true);
                echo "\" alt=\"\" style=\"max-height: 200px; margin-top: 10px;\">
        ";
            }
            // line 9
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['config'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusBannerPlugin:Admin/BannerPosition:banner_type_theme.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  76 => 9,  70 => 7,  67 => 6,  62 => 5,  60 => 4,  54 => 2,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block _banner_position_type_entry_widget %}
    {{ form_widget(form) }}

    {% set currentType = form.vars.value %}
    {% for type, config in form.parent.vars.types_config %}
        {% if type == currentType %}
            <img src=\"{{ app.request.getSchemeAndHttpHost ~ '/' ~ config.example_image }}\" alt=\"\" style=\"max-height: 200px; margin-top: 10px;\">
        {% endif %}
    {% endfor %}
{% endblock %}
", "OmniSyliusBannerPlugin:Admin/BannerPosition:banner_type_theme.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/Admin/BannerPosition/banner_type_theme.html.twig");
    }
}
