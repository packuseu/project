<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SonataIntlBundle:CRUD:history_revision_timestamp.html.twig */
class __TwigTemplate_58f19f1aec4633e723dc08a170d135d1cb246bbb03233a5291ad85f09b4680d0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SonataIntlBundle:CRUD:history_revision_timestamp.html.twig"));

        // line 12
        $this->loadTemplate("@SonataIntl/CRUD/display_datetime.html.twig", "SonataIntlBundle:CRUD:history_revision_timestamp.html.twig", 12)->display(twig_array_merge($context, ["value" => twig_get_attribute($this->env, $this->source, (isset($context["revision"]) || array_key_exists("revision", $context) ? $context["revision"] : (function () { throw new RuntimeError('Variable "revision" does not exist.', 12, $this->source); })()), "timestamp", [], "any", false, false, false, 12)]));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SonataIntlBundle:CRUD:history_revision_timestamp.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 12,);
    }

    public function getSourceContext()
    {
        return new Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{%- include '@SonataIntl/CRUD/display_datetime.html.twig' with { value: revision.timestamp } -%}
", "SonataIntlBundle:CRUD:history_revision_timestamp.html.twig", "/var/www/html/vendor/sonata-project/intl-bundle/src/Resources/views/CRUD/history_revision_timestamp.html.twig");
    }
}
