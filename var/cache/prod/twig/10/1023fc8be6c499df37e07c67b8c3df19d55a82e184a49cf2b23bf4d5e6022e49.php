<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Homepage:index.html.twig */
class __TwigTemplate_20c6bf09281a975635006fc93310d6a83495810a46a04fa31493c2179b91b8d1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'lead_catcher' => [$this, 'block_lead_catcher'],
            'head_extra' => [$this, 'block_head_extra'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Homepage:index.html.twig"));

        // line 3
        $macros["headers"] = $this->macros["headers"] = $this->loadTemplate("@SyliusShop/Common/Macro/sectionHeaders.html.twig", "SyliusShopBundle:Homepage:index.html.twig", 3)->unwrap();
        // line 4
        $macros["dividers"] = $this->macros["dividers"] = $this->loadTemplate("@SyliusShop/Common/Macro/sectionDivider.html.twig", "SyliusShopBundle:Homepage:index.html.twig", 4)->unwrap();
        // line 1
        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "SyliusShopBundle:Homepage:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    ";
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "homepage-header-top");
        echo "

    ";
        // line 9
        $this->loadTemplate("@SyliusShop/Homepage/_benefits.html.twig", "SyliusShopBundle:Homepage:index.html.twig", 9)->display($context);
        // line 10
        echo "
    ";
        // line 11
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "homepage-middle-top");
        echo "

    ";
        // line 13
        echo twig_call_macro($macros["headers"], "macro_default", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.latest_products")], 13, $context, $this->getSourceContext());
        echo "

    ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sylius_shop_partial_product_index_latest", ["count" => 4, "template" => "@SyliusShop/Product/_horizontalList.html.twig"]));
        echo "

    ";
        // line 17
        echo twig_call_macro($macros["dividers"], "macro_default", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.all_products"), $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_index", ["slug" => "katalogas"])], 17, $context, $this->getSourceContext());
        echo "

    ";
        // line 19
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "homepage-bottom-bottom");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 22
    public function block_lead_catcher($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lead_catcher"));

        $this->loadTemplate("LeadCatcher/lead_catcher.html.twig", "SyliusShopBundle:Homepage:index.html.twig", 22)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_head_extra($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head_extra"));

        // line 25
        echo "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 28
    public function block_extra_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        // line 29
        echo "    ";
        $this->loadTemplate("LeadCatcher/_scripts.html.twig", "SyliusShopBundle:Homepage:index.html.twig", 29)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Homepage:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 29,  135 => 28,  127 => 25,  120 => 24,  107 => 22,  98 => 19,  93 => 17,  88 => 15,  83 => 13,  78 => 11,  75 => 10,  73 => 9,  67 => 7,  60 => 6,  52 => 1,  50 => 4,  48 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% import '@SyliusShop/Common/Macro/sectionHeaders.html.twig' as headers %}
{% import '@SyliusShop/Common/Macro/sectionDivider.html.twig' as dividers %}

{% block content %}
    {{ omni_sylius_banner_zone('homepage-header-top') }}

    {% include '@SyliusShop/Homepage/_benefits.html.twig' %}

    {{ omni_sylius_banner_zone('homepage-middle-top') }}

    {{ headers.default('sylius.ui.latest_products'|trans) }}

    {{ render(url('sylius_shop_partial_product_index_latest', {'count': 4, 'template': '@SyliusShop/Product/_horizontalList.html.twig'})) }}

    {{ dividers.default('app.ui.all_products'|trans, path('sylius_shop_product_index', {slug: 'katalogas'})) }}

    {{ omni_sylius_banner_zone('homepage-bottom-bottom') }}
{% endblock %}

{% block lead_catcher %}{% include 'LeadCatcher/lead_catcher.html.twig' %}{% endblock %}

{% block head_extra %}
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css\">
{% endblock %}

{% block extra_javascripts %}
    {% include 'LeadCatcher/_scripts.html.twig' %}
{% endblock %}
", "SyliusShopBundle:Homepage:index.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Homepage/index.html.twig");
    }
}
