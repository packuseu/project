<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Common/Form:_login.html.twig */
class __TwigTemplate_33f8b9ec8f60a6a03371a1387ce283b3928dff5ca5492517a49b739dcf418ed8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Common/Form:_login.html.twig"));

        // line 1
        echo "<div data-js-login>
    ";
        // line 2
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 2, $this->source); })()), "email", [], "any", false, false, false, 2), 'row', ["attr" => ["data-js-login" => "email", "data-js-login-check-email-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_ajax_user_check_action")]]);
        echo "

    <div data-js-login=\"form\" class=\"mb-3 d-none\">
        <input class=\"form-control\" type=\"password\" placeholder=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.password"), "html", null, true);
        echo "\">
        <input type=\"hidden\" name=\"_csrf_shop_security_token\" value=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("shop_authenticate"), "html", null, true);
        echo "\">
        <button class=\"btn btn-primary mt-3\" data-js-login-url=\"";
        // line 7
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_login_check");
        echo "\" type=\"button\">
            ";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.sign_in"), "html", null, true);
        echo "
        </button>
    </div>
    <div class=\"alert alert-danger d-none\"></div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Common/Form:_login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 8,  57 => 7,  53 => 6,  49 => 5,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div data-js-login>
    {{ form_row(form.email, {'attr': {'data-js-login': 'email', 'data-js-login-check-email-url': path('sylius_shop_ajax_user_check_action')}}) }}

    <div data-js-login=\"form\" class=\"mb-3 d-none\">
        <input class=\"form-control\" type=\"password\" placeholder=\"{{ 'sylius.ui.password'|trans }}\">
        <input type=\"hidden\" name=\"_csrf_shop_security_token\" value=\"{{ csrf_token('shop_authenticate') }}\">
        <button class=\"btn btn-primary mt-3\" data-js-login-url=\"{{ path('sylius_shop_login_check') }}\" type=\"button\">
            {{ 'sylius.ui.sign_in'|trans }}
        </button>
    </div>
    <div class=\"alert alert-danger d-none\"></div>
</div>
", "SyliusShopBundle:Common/Form:_login.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Common/Form/_login.html.twig");
    }
}
