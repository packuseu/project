<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/OmniSyliusFilterPlugin/Grid/Filter/product_attribute.html.twig */
class __TwigTemplate_71ee47d5dc01661b808a7858ac15f69ec5f844d7037ef51c5a985c424a7081ff extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/OmniSyliusFilterPlugin/Grid/Filter/product_attribute.html.twig"));

        // line 1
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), [0 => "Sylius/theme.html.twig"], true);
        // line 2
        echo "

";
        // line 4
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "children", [], "any", false, false, false, 4))) {
            // line 5
            echo "    ";
            $context["bool_filters"] = [];
            // line 6
            echo "
    ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), "children", [], "any", false, false, false, 7));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_filter"]) {
                // line 8
                echo "        ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "vars", [], "any", false, true, false, 8), "attr", [], "any", false, true, false, 8), "type", [], "array", true, true, false, 8) && (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "vars", [], "any", false, false, false, 8), "attr", [], "any", false, false, false, 8), "type", [], "array", false, false, false, 8) == "checkbox"))) {
                    // line 9
                    echo "            ";
                    $context["bool_filters"] = twig_array_merge((isset($context["bool_filters"]) || array_key_exists("bool_filters", $context) ? $context["bool_filters"] : (function () { throw new RuntimeError('Variable "bool_filters" does not exist.', 9, $this->source); })()), [0 => $context["attribute_filter"]]);
                    // line 10
                    echo "        ";
                }
                // line 11
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "
    <div class=\"ui stackable grid\">
        ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "children", [], "any", false, false, false, 14));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_filter"]) {
                // line 15
                echo "            ";
                if (( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "vars", [], "any", false, true, false, 15), "attr", [], "any", false, true, false, 15), "type", [], "array", true, true, false, 15) || (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "vars", [], "any", false, false, false, 15), "attr", [], "any", false, false, false, 15), "type", [], "array", false, false, false, 15) != "checkbox"))) {
                    // line 16
                    echo "                <div class=\"five wide column\">
                    ";
                    // line 17
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["attribute_filter"], 'row');
                    echo "
                </div>
            ";
                }
                // line 20
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "
        ";
            // line 22
            if (twig_length_filter($this->env, (isset($context["bool_filters"]) || array_key_exists("bool_filters", $context) ? $context["bool_filters"] : (function () { throw new RuntimeError('Variable "bool_filters" does not exist.', 22, $this->source); })()))) {
                // line 23
                echo "            <div class=\"five wide column\">
                <div class=\"field\">
                    <label>";
                // line 25
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.filters"), "html", null, true);
                echo "</label>
                </div>
                <div id=\"criteria_attributes_checkbox_filters\" type=\"text\" class=\"ui dropdown\">
                    <div class=\"grouped fields\">
                        <div class=\"field\">
                            ";
                // line 30
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["bool_filters"]) || array_key_exists("bool_filters", $context) ? $context["bool_filters"] : (function () { throw new RuntimeError('Variable "bool_filters" does not exist.', 30, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["attribute_filter"]) {
                    // line 31
                    echo "                                ";
                    $context["true_option"] = null;
                    // line 32
                    echo "
                                ";
                    // line 33
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "children", [], "any", false, false, false, 33));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 34
                        echo "                                    ";
                        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 34), "value", [], "any", false, false, false, 34)) {
                            echo " ";
                            $context["true_option"] = $context["child"];
                            echo " ";
                        }
                        // line 35
                        echo "                                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 36
                    echo "
                                ";
                    // line 37
                    if ((isset($context["true_option"]) || array_key_exists("true_option", $context) ? $context["true_option"] : (function () { throw new RuntimeError('Variable "true_option" does not exist.', 37, $this->source); })())) {
                        // line 38
                        echo "                                    <div class=\"ui toggle checkbox\">
                                        <div class=\"checkbox-group\">
                                            <label class=\"control control--checkbox\">
                                                <span class=\"control--checkbox__text\">
                                                    ";
                        // line 43
                        echo "                                                    ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "vars", [], "any", false, false, false, 43), "label", [], "any", false, false, false, 43), "html", null, true);
                        echo " ";
                        // line 44
                        echo "                                                </span>
                                                ";
                        // line 45
                        $context["is_selected"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "vars", [], "any", false, false, false, 45), "is_selected", [], "any", false, false, false, 45);
                        // line 46
                        echo "                                                <input type=\"checkbox\" id=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["true_option"]) || array_key_exists("true_option", $context) ? $context["true_option"] : (function () { throw new RuntimeError('Variable "true_option" does not exist.', 46, $this->source); })()), "vars", [], "any", false, false, false, 46), "id", [], "any", false, false, false, 46), "html", null, true);
                        echo "\" name=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["true_option"]) || array_key_exists("true_option", $context) ? $context["true_option"] : (function () { throw new RuntimeError('Variable "true_option" does not exist.', 46, $this->source); })()), "vars", [], "any", false, false, false, 46), "full_name", [], "any", false, false, false, 46), "html", null, true);
                        echo "\" value=\"1\" ";
                        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["true_option"]) || array_key_exists("true_option", $context) ? $context["true_option"] : (function () { throw new RuntimeError('Variable "true_option" does not exist.', 46, $this->source); })()), "vars", [], "any", false, false, false, 46), "checked", [], "any", false, false, false, 46)) {
                            echo "checked=\"checked\"";
                        }
                        echo "/>
                                                <div class=\"control__indicator\"></div>
                                            </label>
                                        </div>
                                    </div>
                                ";
                    }
                    // line 52
                    echo "                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_filter'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 53
                echo "                        </div>
                    </div>
                </div>
            </div>
        ";
            }
            // line 58
            echo "    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/OmniSyliusFilterPlugin/Grid/Filter/product_attribute.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 58,  185 => 53,  179 => 52,  163 => 46,  161 => 45,  158 => 44,  154 => 43,  148 => 38,  146 => 37,  143 => 36,  137 => 35,  130 => 34,  126 => 33,  123 => 32,  120 => 31,  116 => 30,  108 => 25,  104 => 23,  102 => 22,  99 => 21,  93 => 20,  87 => 17,  84 => 16,  81 => 15,  77 => 14,  73 => 12,  67 => 11,  64 => 10,  61 => 9,  58 => 8,  54 => 7,  51 => 6,  48 => 5,  46 => 4,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% form_theme form 'Sylius/theme.html.twig' %}


{% if form.children|length %}
    {% set bool_filters = [] %}

    {% for attribute_filter in form.children %}
        {% if attribute_filter.vars.attr['type'] is defined and attribute_filter.vars.attr['type'] == 'checkbox' %}
            {%  set bool_filters = bool_filters|merge([attribute_filter]) %}
        {% endif %}
    {% endfor %}

    <div class=\"ui stackable grid\">
        {% for attribute_filter in form.children %}
            {% if attribute_filter.vars.attr['type'] is not defined or attribute_filter.vars.attr['type'] != 'checkbox' %}
                <div class=\"five wide column\">
                    {{ form_row(attribute_filter) }}
                </div>
            {% endif %}
        {% endfor %}

        {% if bool_filters|length %}
            <div class=\"five wide column\">
                <div class=\"field\">
                    <label>{{ 'app.ui.filters'|trans }}</label>
                </div>
                <div id=\"criteria_attributes_checkbox_filters\" type=\"text\" class=\"ui dropdown\">
                    <div class=\"grouped fields\">
                        <div class=\"field\">
                            {% for attribute_filter in bool_filters %}
                                {% set true_option = null %}

                                {% for child in attribute_filter.children %}
                                    {% if child.vars.value %} {% set true_option = child %} {% endif %}
                                {% endfor %}

                                {% if true_option %}
                                    <div class=\"ui toggle checkbox\">
                                        <div class=\"checkbox-group\">
                                            <label class=\"control control--checkbox\">
                                                <span class=\"control--checkbox__text\">
                                                    {# the commented out code is in case the amounts would be needed for the filter #}
                                                    {{ attribute_filter.vars.label }} {#({{ attribute_filter.vars.attr.amounts.1 }})#}
                                                </span>
                                                {% set is_selected = attribute_filter.vars.is_selected %}
                                                <input type=\"checkbox\" id=\"{{ true_option.vars.id }}\" name=\"{{ true_option.vars.full_name }}\" value=\"1\" {% if true_option.vars.checked %}checked=\"checked\"{% endif %}/>
                                                <div class=\"control__indicator\"></div>
                                            </label>
                                        </div>
                                    </div>
                                {% endif %}
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        {% endif %}
    </div>
{% endif %}
", "bundles/OmniSyliusFilterPlugin/Grid/Filter/product_attribute.html.twig", "/var/www/html/templates/bundles/OmniSyliusFilterPlugin/Grid/Filter/product_attribute.html.twig");
    }
}
