<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SyliusAdmin/Product/Show/_variants.html.twig */
class __TwigTemplate_c0ee2286a9e900b05e1cd615c9b7e02962ae570f0442e60b62ae530697629579 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@SyliusAdmin/Product/Show/_variants.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusAdmin/Common/Macro/money.html.twig", "@SyliusAdmin/Product/Show/_variants.html.twig", 1)->unwrap();
        // line 2
        echo "
<div id=\"variants\">
    <h4 class=\"ui top attached large header\">";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.list_variants"), "html", null, true);
        echo "</h4>
    <div class=\"ui attached spaceless segment\">
        <table class=\"ui very basic table\" style=\"padding-top: 20px\">
            <thead>
                <tr>
                    <th></th>
                    <th>";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.name"), "html", null, true);
        echo "</th>
                    <th>";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.options"), "html", null, true);
        echo "</th>
                    <th>";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.tracked"), "html", null, true);
        echo "</th>
                    <th>";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_required"), "html", null, true);
        echo "</th>
                    <th>";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.tax_category"), "html", null, true);
        echo "</th>
                    <th style=\"padding-right: 15px;\">";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.current_stock"), "html", null, true);
        echo "</th>
                </tr>
            </thead>
            <tbody class=\"variants-accordion\">
                ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 19, $this->source); })()), "variants", [], "any", false, false, false, 19));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["variant"]) {
            // line 20
            echo "                    <tr class=\"variants-accordion__title item-";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 20), "html", null, true);
            echo "\">
                        <td class=\"center aligned\">
                            <button class=\"ui basic icon button\" style=\"box-shadow: none\">
                                <i class=\"counterclockwise rotated dropdown icon\"></i>
                            </button>
                        </td>
                        <td>
                            <div class=\"ui items\">
                                <div class=\"item\">
                                    <div class=\"ui tiny image\">
                                        ";
            // line 30
            if (twig_get_attribute($this->env, $this->source, $context["variant"], "hasImages", [], "any", false, false, false, 30)) {
                // line 31
                echo "                                            ";
                $this->loadTemplate("@SyliusAdmin/Product/_mainImage.html.twig", "@SyliusAdmin/Product/Show/_variants.html.twig", 31)->display(twig_array_merge($context, ["product" => $context["variant"], "filter" => "sylius_admin_product_large_thumbnail"]));
                // line 32
                echo "                                        ";
            } else {
                // line 33
                echo "                                            ";
                $this->loadTemplate("@SyliusAdmin/Product/_mainImage.html.twig", "@SyliusAdmin/Product/Show/_variants.html.twig", 33)->display(twig_array_merge($context, ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 33, $this->source); })()), "filter" => "sylius_admin_product_large_thumbnail"]));
                // line 34
                echo "                                        ";
            }
            // line 35
            echo "                                    </div>
                                    <div class=\"middle aligned content\">
                                        <div><strong class=\"variant-name\">";
            // line 37
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["variant"], "product", [], "any", false, false, false, 37), "name", [], "any", false, false, false, 37), "html", null, true);
            echo "</strong></div>
                                        <small class=\"gray text variant-code\">";
            // line 38
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "code", [], "any", false, false, false, 38), "html", null, true);
            echo "</small>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["variant"], "optionValues", [], "any", false, false, false, 44));
            foreach ($context['_seq'] as $context["_key"] => $context["optionValue"]) {
                // line 45
                echo "                                <div><span class=\"gray text\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["optionValue"], "option", [], "any", false, false, false, 45), "name", [], "any", false, false, false, 45), "html", null, true);
                echo ":</span> ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["optionValue"], "value", [], "any", false, false, false, 45), "html", null, true);
                echo "</div>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['optionValue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "                        </td>
                        <td>
                            ";
            // line 49
            if (twig_get_attribute($this->env, $this->source, $context["variant"], "tracked", [], "any", false, false, false, 49)) {
                // line 50
                echo "                                <span class=\"ui teal label\"><i class=\"checkmark icon\"></i>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.tracked"), "html", null, true);
                echo "</span>
                            ";
            } else {
                // line 52
                echo "                                <span class=\"ui red label\"><i class=\"remove icon\"></i>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.not_tracked"), "html", null, true);
                echo "</span>
                            ";
            }
            // line 54
            echo "                        </td>
                        <td>
                            ";
            // line 56
            if (twig_get_attribute($this->env, $this->source, $context["variant"], "shippingRequired", [], "any", false, false, false, 56)) {
                // line 57
                echo "                                <span class=\"ui teal label\"><i class=\"checkmark icon\"></i>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_required"), "html", null, true);
                echo "</span>
                            ";
            } else {
                // line 59
                echo "                                <span class=\"ui orange label\"><i class=\"remove icon\"></i>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_not_required"), "html", null, true);
                echo "</span>
                            ";
            }
            // line 61
            echo "                        </td>
                        <td>
                            ";
            // line 63
            if (twig_get_attribute($this->env, $this->source, $context["variant"], "taxCategory", [], "any", false, false, false, 63)) {
                // line 64
                echo "                                ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "taxCategory", [], "any", false, false, false, 64), "html", null, true);
                echo "
                            ";
            } else {
                // line 66
                echo "                                <div class=\"gray text\">-</div>
                            ";
            }
            // line 68
            echo "                        </td>
                        <td class=\"current-stock\" style=\"padding-right: 15px;\">";
            // line 69
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "onHand", [], "any", false, false, false, 69), "html", null, true);
            echo "</td>
                    </tr>
                    <tr class=\"variants-accordion__content item-";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 71), "html", null, true);
            echo "\" style=\"display: none; background: rgba(0,0,0,.04);\">
                        <td colspan=\"7\" style=\"padding: 20px\">
                            <div class=\"ui grid\">
                                <div class=\"doubling two column row\">
                                    <div class=\"column\">
                                        <div class=\"ui segment\">
                                            <div class=\"ui small header\">
                                                ";
            // line 78
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping"), "html", null, true);
            echo "
                                            </div>
                                            ";
            // line 80
            if ((((((twig_get_attribute($this->env, $this->source, $context["variant"], "shippingCategory", [], "any", false, false, false, 80) === null) && (twig_get_attribute($this->env, $this->source, $context["variant"], "width", [], "any", false, false, false, 80) === null)) && (twig_get_attribute($this->env, $this->source, $context["variant"], "height", [], "any", false, false, false, 80) === null)) && (twig_get_attribute($this->env, $this->source, $context["variant"], "depth", [], "any", false, false, false, 80) === null)) && (twig_get_attribute($this->env, $this->source, $context["variant"], "weight", [], "any", false, false, false, 80) === null))) {
                // line 81
                echo "                                                <div class=\"ui teal message\">
                                                    <p>";
                // line 82
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.no_shipping_data"), "html", null, true);
                echo "</p>
                                                </div>
                                            ";
            } else {
                // line 85
                echo "                                                <table class=\"ui very basic celled table\">
                                                    <tbody>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>";
                // line 88
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_category"), "html", null, true);
                echo "</strong></td>
                                                            <td>";
                // line 89
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "shippingCategory", [], "any", false, false, false, 89), "html", null, true);
                echo "</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>";
                // line 92
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.width"), "html", null, true);
                echo "</strong></td>
                                                            <td>";
                // line 93
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "width", [], "any", false, false, false, 93), "html", null, true);
                echo "</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>";
                // line 96
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.height"), "html", null, true);
                echo "</strong></td>
                                                            <td>";
                // line 97
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "height", [], "any", false, false, false, 97), "html", null, true);
                echo "</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>";
                // line 100
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.depth"), "html", null, true);
                echo "</strong></td>
                                                            <td>";
                // line 101
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "depth", [], "any", false, false, false, 101), "html", null, true);
                echo "</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>";
                // line 104
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.weight"), "html", null, true);
                echo "</strong></td>
                                                            <td>";
                // line 105
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "weight", [], "any", false, false, false, 105), "html", null, true);
                echo "</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            ";
            }
            // line 110
            echo "                                        </div>
                                    </div>
                                    <div class=\"column\">
                                        <div class=\"ui segment\">
                                            <div class=\"ui small header\">
                                                ";
            // line 115
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.pricing"), "html", null, true);
            echo "
                                            </div>
                                            <table class=\"ui very basic celled table\">
                                                <tbody>
                                                    <tr>
                                                        <td class=\"gray text\"><strong>";
            // line 120
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.channels"), "html", null, true);
            echo "</strong></td>
                                                        <td class=\"gray text\"><strong>";
            // line 121
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.price"), "html", null, true);
            echo "</strong></td>
                                                        <td class=\"gray text\"><strong>";
            // line 122
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.original_price"), "html", null, true);
            echo "</strong></td>
                                                    </tr>
                                                    ";
            // line 124
            $context["currencies"] = $this->extensions['Sylius\Bundle\AdminBundle\Twig\ChannelsCurrenciesExtension']->getAllCurrencies();
            // line 125
            echo "                                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["variant"], "channelPricings", [], "any", false, false, false, 125));
            foreach ($context['_seq'] as $context["_key"] => $context["channelPricing"]) {
                // line 126
                echo "                                                        <tr class=\"pricing\">
                                                            <td class=\"five wide gray text\">
                                                                <strong>";
                // line 128
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channelPricing"], "channelCode", [], "any", false, false, false, 128), "html", null, true);
                echo "</strong>
                                                            </td>
                                                            ";
                // line 130
                $context["channelCode"] = twig_get_attribute($this->env, $this->source, $context["channelPricing"], "channelCode", [], "any", false, false, false, 130);
                // line 131
                echo "                                                            <td>";
                echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, $context["channelPricing"], "price", [], "any", false, false, false, 131), twig_get_attribute($this->env, $this->source, (isset($context["currencies"]) || array_key_exists("currencies", $context) ? $context["currencies"] : (function () { throw new RuntimeError('Variable "currencies" does not exist.', 131, $this->source); })()), (isset($context["channelCode"]) || array_key_exists("channelCode", $context) ? $context["channelCode"] : (function () { throw new RuntimeError('Variable "channelCode" does not exist.', 131, $this->source); })()), [], "array", false, false, false, 131)], 131, $context, $this->getSourceContext());
                echo "</td>
                                                            ";
                // line 132
                if ((twig_get_attribute($this->env, $this->source, $context["channelPricing"], "originalPrice", [], "any", false, false, false, 132) != null)) {
                    // line 133
                    echo "                                                                <td>";
                    echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, $context["channelPricing"], "originalPrice", [], "any", false, false, false, 133), twig_get_attribute($this->env, $this->source, (isset($context["currencies"]) || array_key_exists("currencies", $context) ? $context["currencies"] : (function () { throw new RuntimeError('Variable "currencies" does not exist.', 133, $this->source); })()), (isset($context["channelCode"]) || array_key_exists("channelCode", $context) ? $context["channelCode"] : (function () { throw new RuntimeError('Variable "channelCode" does not exist.', 133, $this->source); })()), [], "array", false, false, false, 133)], 133, $context, $this->getSourceContext());
                    echo "</td>
                                                            ";
                } else {
                    // line 135
                    echo "                                                                <td><span class=\"gray text\">-</span></td>
                                                            ";
                }
                // line 137
                echo "                                                        </tr>
                                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channelPricing'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 139
            echo "                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['variant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 148
        echo "            </tbody>
        </table>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@SyliusAdmin/Product/Show/_variants.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  394 => 148,  372 => 139,  365 => 137,  361 => 135,  355 => 133,  353 => 132,  348 => 131,  346 => 130,  341 => 128,  337 => 126,  332 => 125,  330 => 124,  325 => 122,  321 => 121,  317 => 120,  309 => 115,  302 => 110,  294 => 105,  290 => 104,  284 => 101,  280 => 100,  274 => 97,  270 => 96,  264 => 93,  260 => 92,  254 => 89,  250 => 88,  245 => 85,  239 => 82,  236 => 81,  234 => 80,  229 => 78,  219 => 71,  214 => 69,  211 => 68,  207 => 66,  201 => 64,  199 => 63,  195 => 61,  189 => 59,  183 => 57,  181 => 56,  177 => 54,  171 => 52,  165 => 50,  163 => 49,  159 => 47,  148 => 45,  144 => 44,  135 => 38,  131 => 37,  127 => 35,  124 => 34,  121 => 33,  118 => 32,  115 => 31,  113 => 30,  99 => 20,  82 => 19,  75 => 15,  71 => 14,  67 => 13,  63 => 12,  59 => 11,  55 => 10,  46 => 4,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusAdmin/Common/Macro/money.html.twig\" as money %}

<div id=\"variants\">
    <h4 class=\"ui top attached large header\">{{ 'sylius.ui.list_variants'|trans }}</h4>
    <div class=\"ui attached spaceless segment\">
        <table class=\"ui very basic table\" style=\"padding-top: 20px\">
            <thead>
                <tr>
                    <th></th>
                    <th>{{ 'sylius.ui.name'|trans }}</th>
                    <th>{{ 'sylius.ui.options'|trans }}</th>
                    <th>{{ 'sylius.ui.tracked'|trans }}</th>
                    <th>{{ 'sylius.ui.shipping_required'|trans }}</th>
                    <th>{{ 'sylius.ui.tax_category'|trans }}</th>
                    <th style=\"padding-right: 15px;\">{{ 'sylius.ui.current_stock'|trans }}</th>
                </tr>
            </thead>
            <tbody class=\"variants-accordion\">
                {% for variant in product.variants %}
                    <tr class=\"variants-accordion__title item-{{ loop.index\t}}\">
                        <td class=\"center aligned\">
                            <button class=\"ui basic icon button\" style=\"box-shadow: none\">
                                <i class=\"counterclockwise rotated dropdown icon\"></i>
                            </button>
                        </td>
                        <td>
                            <div class=\"ui items\">
                                <div class=\"item\">
                                    <div class=\"ui tiny image\">
                                        {% if variant.hasImages %}
                                            {% include '@SyliusAdmin/Product/_mainImage.html.twig' with {'product': variant, 'filter': 'sylius_admin_product_large_thumbnail'} %}
                                        {% else %}
                                            {% include '@SyliusAdmin/Product/_mainImage.html.twig' with {'product': product, 'filter': 'sylius_admin_product_large_thumbnail'} %}
                                        {% endif %}
                                    </div>
                                    <div class=\"middle aligned content\">
                                        <div><strong class=\"variant-name\">{{ variant.product.name }}</strong></div>
                                        <small class=\"gray text variant-code\">{{ variant.code }}</small>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            {% for optionValue in variant.optionValues %}
                                <div><span class=\"gray text\">{{ optionValue.option.name }}:</span> {{ optionValue.value }}</div>
                            {% endfor %}
                        </td>
                        <td>
                            {% if variant.tracked %}
                                <span class=\"ui teal label\"><i class=\"checkmark icon\"></i>{{ 'sylius.ui.tracked'|trans }}</span>
                            {% else %}
                                <span class=\"ui red label\"><i class=\"remove icon\"></i>{{ 'sylius.ui.not_tracked'|trans }}</span>
                            {% endif %}
                        </td>
                        <td>
                            {% if variant.shippingRequired %}
                                <span class=\"ui teal label\"><i class=\"checkmark icon\"></i>{{ 'sylius.ui.shipping_required'|trans }}</span>
                            {% else %}
                                <span class=\"ui orange label\"><i class=\"remove icon\"></i>{{ 'sylius.ui.shipping_not_required'|trans }}</span>
                            {% endif %}
                        </td>
                        <td>
                            {% if variant.taxCategory %}
                                {{ variant.taxCategory }}
                            {% else %}
                                <div class=\"gray text\">-</div>
                            {% endif %}
                        </td>
                        <td class=\"current-stock\" style=\"padding-right: 15px;\">{{ variant.onHand }}</td>
                    </tr>
                    <tr class=\"variants-accordion__content item-{{ loop.index }}\" style=\"display: none; background: rgba(0,0,0,.04);\">
                        <td colspan=\"7\" style=\"padding: 20px\">
                            <div class=\"ui grid\">
                                <div class=\"doubling two column row\">
                                    <div class=\"column\">
                                        <div class=\"ui segment\">
                                            <div class=\"ui small header\">
                                                {{ 'sylius.ui.shipping'|trans }}
                                            </div>
                                            {% if variant.shippingCategory is same as(null) and variant.width is same as(null) and variant.height is same as(null) and variant.depth is same as(null) and variant.weight is same as(null) %}
                                                <div class=\"ui teal message\">
                                                    <p>{{ 'sylius.ui.no_shipping_data'|trans }}</p>
                                                </div>
                                            {% else %}
                                                <table class=\"ui very basic celled table\">
                                                    <tbody>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>{{ 'sylius.ui.shipping_category'|trans }}</strong></td>
                                                            <td>{{ variant.shippingCategory }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>{{ 'sylius.ui.width'|trans }}</strong></td>
                                                            <td>{{ variant.width }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>{{ 'sylius.ui.height'|trans }}</strong></td>
                                                            <td>{{ variant.height }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>{{ 'sylius.ui.depth'|trans }}</strong></td>
                                                            <td>{{ variant.depth }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td class=\"five wide gray text\"><strong>{{ 'sylius.ui.weight'|trans }}</strong></td>
                                                            <td>{{ variant.weight }}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            {% endif %}
                                        </div>
                                    </div>
                                    <div class=\"column\">
                                        <div class=\"ui segment\">
                                            <div class=\"ui small header\">
                                                {{ 'sylius.ui.pricing'|trans }}
                                            </div>
                                            <table class=\"ui very basic celled table\">
                                                <tbody>
                                                    <tr>
                                                        <td class=\"gray text\"><strong>{{ 'sylius.ui.channels'|trans }}</strong></td>
                                                        <td class=\"gray text\"><strong>{{ 'sylius.ui.price'|trans }}</strong></td>
                                                        <td class=\"gray text\"><strong>{{ 'sylius.ui.original_price'|trans }}</strong></td>
                                                    </tr>
                                                    {% set currencies = sylius_channels_currencies() %}
                                                    {% for channelPricing in variant.channelPricings %}
                                                        <tr class=\"pricing\">
                                                            <td class=\"five wide gray text\">
                                                                <strong>{{ channelPricing.channelCode }}</strong>
                                                            </td>
                                                            {% set channelCode = channelPricing.channelCode %}
                                                            <td>{{ money.format(channelPricing.price, currencies[channelCode]) }}</td>
                                                            {% if channelPricing.originalPrice != null %}
                                                                <td>{{ money.format(channelPricing.originalPrice, currencies[channelCode]) }}</td>
                                                            {% else %}
                                                                <td><span class=\"gray text\">-</span></td>
                                                            {% endif %}
                                                        </tr>
                                                    {% endfor %}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                {% endfor %}
            </tbody>
        </table>
    </div>
</div>
", "@SyliusAdmin/Product/Show/_variants.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/Product/Show/_variants.html.twig");
    }
}
