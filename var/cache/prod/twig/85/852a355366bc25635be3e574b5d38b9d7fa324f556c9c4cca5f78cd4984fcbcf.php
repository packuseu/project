<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Cart/Summary:_item.html.twig */
class __TwigTemplate_476554387ebe4cfa87189d1b5970cb085f2d384563c1e0fd8b18481a5055759f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Cart/Summary:_item.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Cart/Summary:_item.html.twig", 1)->unwrap();
        // line 2
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Cart/Summary:_item.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        $context["product_variant"] = twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 4, $this->source); })()), "variant", [], "any", false, false, false, 4);
        // line 5
        $context["product"] = twig_get_attribute($this->env, $this->source, (isset($context["product_variant"]) || array_key_exists("product_variant", $context) ? $context["product_variant"] : (function () { throw new RuntimeError('Variable "product_variant" does not exist.', 5, $this->source); })()), "product", [], "any", false, false, false, 5);
        // line 6
        if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "quantityStep", [], "any", false, false, false, 6)) && (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "quantityStep", [], "any", false, false, false, 6) > 1))) {
            // line 7
            echo "    ";
            $context["step"] = twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 7, $this->source); })()), "quantityStep", [], "any", false, false, false, 7);
        }
        // line 9
        echo "
<tr>
    <td>
        <button type=\"button\"
                data-js-remove-from-cart-button
                data-js-remove-from-cart-redirect-url=\"";
        // line 14
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_cart_summary");
        echo "\"
                data-js-remove-from-cart-csrf-token=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 15, $this->source); })()), "id", [], "any", false, false, false, 15)), "html", null, true);
        echo "\"
                data-js-remove-from-cart-api-url=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_ajax_cart_item_remove", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 16, $this->source); })()), "id", [], "any", false, false, false, 16)]), "html", null, true);
        echo "\"
                class=\"btn btn-sm btn-link text-danger\">";
        // line 17
        echo twig_call_macro($macros["icons"], "macro_times", [], 17, $context, $this->getSourceContext());
        echo "</button>
    </td>
    <td>
        ";
        // line 20
        $this->loadTemplate("@SyliusShop/Product/_info.html.twig", "SyliusShopBundle:Cart/Summary:_item.html.twig", 20)->display(twig_array_merge($context, ["variant" => (isset($context["product_variant"]) || array_key_exists("product_variant", $context) ? $context["product_variant"] : (function () { throw new RuntimeError('Variable "product_variant" does not exist.', 20, $this->source); })())]));
        // line 21
        echo "    </td>
    <td class=\"text-center\">
        ";
        // line 23
        if ((twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 23, $this->source); })()), "unitPrice", [], "any", false, false, false, 23) != twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 23, $this->source); })()), "discountedUnitPrice", [], "any", false, false, false, 23))) {
            // line 24
            echo "            <small><s>";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 24, $this->source); })()), "unitPrice", [], "any", false, false, false, 24)], 24, $context, $this->getSourceContext());
            echo "</s></small>
        ";
        }
        // line 26
        echo "        <span>";
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 26, $this->source); })()), "discountedUnitPrice", [], "any", false, false, false, 26)], 26, $context, $this->getSourceContext());
        echo "</span>
    </td>
    <td class=\"text-center\">
        <span>
            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source,         // line 31
(isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "quantity", [], "any", false, false, false, 31), 'widget', ["attr" => ["step" =>         // line 33
(isset($context["step"]) || array_key_exists("step", $context) ? $context["step"] : (function () { throw new RuntimeError('Variable "step" does not exist.', 33, $this->source); })()), "min" =>         // line 34
(isset($context["step"]) || array_key_exists("step", $context) ? $context["step"] : (function () { throw new RuntimeError('Variable "step" does not exist.', 34, $this->source); })()), "class" => "js-quantity-step", "data-step" =>         // line 36
(isset($context["step"]) || array_key_exists("step", $context) ? $context["step"] : (function () { throw new RuntimeError('Variable "step" does not exist.', 36, $this->source); })())]]);
        // line 38
        echo "
        </span>
    </td>
    <td class=\"text-right\">
        <span>";
        // line 42
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 42, $this->source); })()), "subtotal", [], "any", false, false, false, 42)], 42, $context, $this->getSourceContext());
        echo "</span>
    </td>
</tr>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Cart/Summary:_item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 42,  110 => 38,  108 => 36,  107 => 34,  106 => 33,  105 => 31,  104 => 30,  96 => 26,  90 => 24,  88 => 23,  84 => 21,  82 => 20,  76 => 17,  72 => 16,  68 => 15,  64 => 14,  57 => 9,  53 => 7,  51 => 6,  49 => 5,  47 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}
{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% set product_variant = item.variant %}
{% set product = product_variant.product %}
{% if product.quantityStep is not null and product.quantityStep > 1 %}
    {% set step = product.quantityStep %}
{% endif %}

<tr>
    <td>
        <button type=\"button\"
                data-js-remove-from-cart-button
                data-js-remove-from-cart-redirect-url=\"{{ path('sylius_shop_cart_summary') }}\"
                data-js-remove-from-cart-csrf-token=\"{{ csrf_token(item.id) }}\"
                data-js-remove-from-cart-api-url=\"{{ path('sylius_shop_ajax_cart_item_remove', {'id': item.id}) }}\"
                class=\"btn btn-sm btn-link text-danger\">{{ icons.times() }}</button>
    </td>
    <td>
        {% include '@SyliusShop/Product/_info.html.twig' with {'variant': product_variant} %}
    </td>
    <td class=\"text-center\">
        {% if item.unitPrice != item.discountedUnitPrice %}
            <small><s>{{ money.convertAndFormat(item.unitPrice) }}</s></small>
        {% endif %}
        <span>{{ money.convertAndFormat(item.discountedUnitPrice) }}</span>
    </td>
    <td class=\"text-center\">
        <span>
            {{ form_widget(
                form.quantity,
                { 'attr': {
                    'step': step,
                    'min': step,
                    'class': 'js-quantity-step',
                    'data-step': step
                }})
            }}
        </span>
    </td>
    <td class=\"text-right\">
        <span>{{ money.convertAndFormat(item.subtotal) }}</span>
    </td>
</tr>
", "SyliusShopBundle:Cart/Summary:_item.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Cart/Summary/_item.html.twig");
    }
}
