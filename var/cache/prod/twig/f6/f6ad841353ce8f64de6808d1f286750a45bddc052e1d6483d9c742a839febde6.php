<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusBannerPlugin/_zone.html.twig */
class __TwigTemplate_b90b014d7f9c9f33264e25d32c8ec9e5fd373777c92b71e1bc0dfa052aa38651 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusBannerPlugin/_zone.html.twig"));

        // line 1
        $context["renderWrapper"] = false;
        // line 2
        echo "
";
        // line 3
        if (((isset($context["zone"]) || array_key_exists("zone", $context) ? $context["zone"] : (function () { throw new RuntimeError('Variable "zone" does not exist.', 3, $this->source); })()) && twig_get_attribute($this->env, $this->source, (isset($context["zone"]) || array_key_exists("zone", $context) ? $context["zone"] : (function () { throw new RuntimeError('Variable "zone" does not exist.', 3, $this->source); })()), "positions", [], "any", false, false, false, 3))) {
            // line 4
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["zone"]) || array_key_exists("zone", $context) ? $context["zone"] : (function () { throw new RuntimeError('Variable "zone" does not exist.', 4, $this->source); })()), "positions", [], "any", false, false, false, 4));
            foreach ($context['_seq'] as $context["_key"] => $context["position"]) {
                // line 5
                echo "        ";
                $context["positionTypeConfig"] = twig_get_attribute($this->env, $this->source, $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->getPositionTypeConfig($context["position"]), twig_get_attribute($this->env, $this->source, $context["position"], "type", [], "any", false, false, false, 5), [], "array", false, false, false, 5);
                // line 6
                echo "        ";
                if ((twig_get_attribute($this->env, $this->source, ($context["positionTypeConfig"] ?? null), "zone_wrapper", [], "any", true, true, false, 6) && twig_get_attribute($this->env, $this->source, (isset($context["positionTypeConfig"]) || array_key_exists("positionTypeConfig", $context) ? $context["positionTypeConfig"] : (function () { throw new RuntimeError('Variable "positionTypeConfig" does not exist.', 6, $this->source); })()), "zone_wrapper", [], "any", false, false, false, 6))) {
                    // line 7
                    echo "            ";
                    $context["renderWrapper"] = true;
                    // line 8
                    echo "        ";
                }
                // line 9
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['position'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "
    ";
            // line 11
            if ((isset($context["renderWrapper"]) || array_key_exists("renderWrapper", $context) ? $context["renderWrapper"] : (function () { throw new RuntimeError('Variable "renderWrapper" does not exist.', 11, $this->source); })())) {
                // line 12
                echo "        ";
                // line 13
                echo "        ";
                if ((twig_length_filter($this->env, (isset($context["position_content"]) || array_key_exists("position_content", $context) ? $context["position_content"] : (function () { throw new RuntimeError('Variable "position_content" does not exist.', 13, $this->source); })())) > 0)) {
                    // line 14
                    echo "            <div class=\"container\">
                <div class=\"banners-multi-grid\">
                    <h4 class=\"heading heading--banners-grid grid--multi\">";
                    // line 16
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["zone"]) || array_key_exists("zone", $context) ? $context["zone"] : (function () { throw new RuntimeError('Variable "zone" does not exist.', 16, $this->source); })()), "title", [], "any", false, false, false, 16), "html", null, true);
                    echo "</h4>

                    <div class=\"row\">
                        ";
                    // line 19
                    echo (isset($context["position_content"]) || array_key_exists("position_content", $context) ? $context["position_content"] : (function () { throw new RuntimeError('Variable "position_content" does not exist.', 19, $this->source); })());
                    echo "
                    </div>
                </div>
            </div>
        ";
                }
                // line 24
                echo "    ";
            } else {
                // line 25
                echo "        ";
                echo (isset($context["position_content"]) || array_key_exists("position_content", $context) ? $context["position_content"] : (function () { throw new RuntimeError('Variable "position_content" does not exist.', 25, $this->source); })());
                echo "
    ";
            }
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusBannerPlugin/_zone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 25,  98 => 24,  90 => 19,  84 => 16,  80 => 14,  77 => 13,  75 => 12,  73 => 11,  70 => 10,  64 => 9,  61 => 8,  58 => 7,  55 => 6,  52 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set renderWrapper = false %}

{% if zone and zone.positions %}
    {% for position in zone.positions %}
        {% set positionTypeConfig = omni_sylius_banner_position_type_config(position)[position.type] %}
        {% if positionTypeConfig.zone_wrapper is defined and positionTypeConfig.zone_wrapper %}
            {% set renderWrapper = true %}
        {% endif %}
    {% endfor %}

    {% if renderWrapper %}
        {# this zone is dedicated to _half_screen_adoptive_grid tpl #}
        {% if position_content|length > 0 %}
            <div class=\"container\">
                <div class=\"banners-multi-grid\">
                    <h4 class=\"heading heading--banners-grid grid--multi\">{{ zone.title }}</h4>

                    <div class=\"row\">
                        {{ position_content|raw }}
                    </div>
                </div>
            </div>
        {% endif %}
    {% else %}
        {{ position_content|raw }}
    {% endif %}
{% endif %}
", "@OmniSyliusBannerPlugin/_zone.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/_zone.html.twig");
    }
}
