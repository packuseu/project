<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Checkout/Address:_form.html.twig */
class __TwigTemplate_afba32a6c1daf1c829efae347d7e75c43bd3fe51c025603df9ffc23e479109e3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Checkout/Address:_form.html.twig"));

        // line 1
        echo "<div data-js-address-book=\"sylius-shipping-address\">
    <h1 class=\"mb-4\">";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_address"), "html", null, true);
        echo "</h1>

    <div class=\"card\">
        <div class=\"card-body\">
            <div class=\"mb-3\">
                ";
        // line 7
        $this->loadTemplate("@SyliusShop/Checkout/Address/_addressBookSelect.html.twig", "SyliusShopBundle:Checkout/Address:_form.html.twig", 7)->display($context);
        // line 8
        echo "            </div>

            ";
        // line 10
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "customer", [], "any", true, true, false, 10)) {
            // line 11
            echo "                ";
            $this->loadTemplate("@SyliusShop/Common/Form/_login.html.twig", "SyliusShopBundle:Checkout/Address:_form.html.twig", 11)->display(twig_array_merge($context, ["form" => twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "customer", [], "any", false, false, false, 11)]));
            // line 12
            echo "            ";
        }
        // line 13
        echo "            ";
        $this->loadTemplate("@SyliusShop/Common/Form/_address.html.twig", "SyliusShopBundle:Checkout/Address:_form.html.twig", 13)->display(twig_array_merge($context, ["form" => twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 13, $this->source); })()), "shippingAddress", [], "any", false, false, false, 13), "order" => (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 13, $this->source); })())]));
        // line 14
        echo "
            ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "differentBillingAddress", [], "any", false, false, false, 15), 'row', ["attr" => ["data-js-toggle" => "[data-js-address-book=\"sylius-billing-address\"]"]]);
        echo "

            ";
        // line 17
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.checkout.address.shipping_address_form", ["order" => (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 17, $this->source); })())]]);
        echo "
        </div>
    </div>
</div>

<div data-js-address-book=\"sylius-billing-address\">
    <h1 class=\"my-4\">";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.billing_address"), "html", null, true);
        echo "</h1>

    <div class=\"card\">
        <div class=\"card-body\">
            <div class=\"mb-3\">
                ";
        // line 28
        $this->loadTemplate("@SyliusShop/Checkout/Address/_addressBookSelect.html.twig", "SyliusShopBundle:Checkout/Address:_form.html.twig", 28)->display($context);
        // line 29
        echo "            </div>

            ";
        // line 31
        $this->loadTemplate("@SyliusShop/Common/Form/_address.html.twig", "SyliusShopBundle:Checkout/Address:_form.html.twig", 31)->display(twig_array_merge($context, ["form" => twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "billingAddress", [], "any", false, false, false, 31), "order" => (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 31, $this->source); })())]));
        // line 32
        echo "
            ";
        // line 33
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.checkout.address.billing_address_form", ["order" => (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 33, $this->source); })())]]);
        echo "
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Checkout/Address:_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 33,  101 => 32,  99 => 31,  95 => 29,  93 => 28,  85 => 23,  76 => 17,  71 => 15,  68 => 14,  65 => 13,  62 => 12,  59 => 11,  57 => 10,  53 => 8,  51 => 7,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div data-js-address-book=\"sylius-shipping-address\">
    <h1 class=\"mb-4\">{{ 'sylius.ui.shipping_address'|trans }}</h1>

    <div class=\"card\">
        <div class=\"card-body\">
            <div class=\"mb-3\">
                {% include '@SyliusShop/Checkout/Address/_addressBookSelect.html.twig' %}
            </div>

            {% if form.customer is defined %}
                {% include '@SyliusShop/Common/Form/_login.html.twig' with {'form': form.customer} %}
            {% endif %}
            {% include '@SyliusShop/Common/Form/_address.html.twig' with {'form': form.shippingAddress, 'order': order} %}

            {{ form_row(form.differentBillingAddress, {'attr': {'data-js-toggle': '[data-js-address-book=\"sylius-billing-address\"]'}}) }}

            {{ sonata_block_render_event('sylius.shop.checkout.address.shipping_address_form', {'order': order}) }}
        </div>
    </div>
</div>

<div data-js-address-book=\"sylius-billing-address\">
    <h1 class=\"my-4\">{{ 'sylius.ui.billing_address'|trans }}</h1>

    <div class=\"card\">
        <div class=\"card-body\">
            <div class=\"mb-3\">
                {% include '@SyliusShop/Checkout/Address/_addressBookSelect.html.twig' %}
            </div>

            {% include '@SyliusShop/Common/Form/_address.html.twig' with {'form': form.billingAddress, 'order': order} %}

            {{ sonata_block_render_event('sylius.shop.checkout.address.billing_address_form', {'order': order}) }}
        </div>
    </div>
</div>
", "SyliusShopBundle:Checkout/Address:_form.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Checkout/Address/_form.html.twig");
    }
}
