<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Banner/PositionType/four_columns.html.twig */
class __TwigTemplate_c290b5767a4dc3c386b1e854a82724d1313d80718c0567070a765a9b559cd69f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Banner/PositionType/four_columns.html.twig"));

        // line 1
        $macros["dividers"] = $this->macros["dividers"] = $this->loadTemplate("@SyliusShop/Common/Macro/sectionDivider.html.twig", "Banner/PositionType/four_columns.html.twig", 1)->unwrap();
        // line 2
        $macros["headers"] = $this->macros["headers"] = $this->loadTemplate("@SyliusShop/Common/Macro/sectionHeaders.html.twig", "Banner/PositionType/four_columns.html.twig", 2)->unwrap();
        // line 3
        echo "
<div class=\"container\">
    <div class=\"banners-four-columns\">
        <div class=\"banner-title\">
            ";
        // line 7
        echo twig_call_macro($macros["headers"], "macro_default", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 7, $this->source); })()), "translation", [], "any", false, false, false, 7), "title", [], "any", false, false, false, 7)], 7, $context, $this->getSourceContext());
        echo "
        </div>
        <div class=\"row\">
            ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 10, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 10, $this->source); })()), "channel", [], "array", false, false, false, 10)], "method", false, false, false, 10)) {
                // line 11
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 11));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 12
                    echo "                    <div class=\"col-12 col-md-3 four-columns-card\">
                        <a class=\"card-link\" href=\"";
                    // line 13
                    echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 13), "link", [], "any", true, true, false, 13)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 13), "link", [], "any", false, false, false, 13), "javascript:;")) : ("javascript:;")), "html", null, true);
                    echo "\">
                            <img class=\"four-columns-card-img\"
                                 src=\"";
                    // line 15
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 15), "omni_sylius_banner"), "html", null, true);
                    echo "\"
                                 alt=\"-\"
                            >
                        </a>
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 21
                echo "            ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "        </div>
    </div>

    ";
        // line 25
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 25, $this->source); })()), "translation", [], "any", false, false, false, 25), "description", [], "any", false, false, false, 25)) {
            // line 26
            echo "        ";
            echo twig_call_macro($macros["dividers"], "macro_default", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 26, $this->source); })()), "translation", [], "any", false, false, false, 26), "description", [], "any", false, false, false, 26), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 26, $this->source); })()), "translation", [], "any", false, false, false, 26), "link", [], "any", false, false, false, 26)], 26, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 28
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Banner/PositionType/four_columns.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 28,  100 => 26,  98 => 25,  93 => 22,  86 => 21,  74 => 15,  69 => 13,  66 => 12,  61 => 11,  56 => 10,  50 => 7,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusShop/Common/Macro/sectionDivider.html.twig' as dividers %}
{% import '@SyliusShop/Common/Macro/sectionHeaders.html.twig' as headers %}

<div class=\"container\">
    <div class=\"banners-four-columns\">
        <div class=\"banner-title\">
            {{ headers.default(position.translation.title) }}
        </div>
        <div class=\"row\">
            {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                {% for image in banner.images %}
                    <div class=\"col-12 col-md-3 four-columns-card\">
                        <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                            <img class=\"four-columns-card-img\"
                                 src=\"{{ image.path|imagine_filter('omni_sylius_banner') }}\"
                                 alt=\"-\"
                            >
                        </a>
                    </div>
                {% endfor %}
            {% endfor %}
        </div>
    </div>

    {% if position.translation.description %}
        {{ dividers.default(position.translation.description, position.translation.link) }}
    {% endif %}
</div>
", "Banner/PositionType/four_columns.html.twig", "/var/www/html/templates/Banner/PositionType/four_columns.html.twig");
    }
}
