<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusCmsPlugin:Form:imagesTheme.html.twig */
class __TwigTemplate_b4a917f202b82c52384b04e87efec4436891e96558bb0cd3b00d690c2a11a4fe extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            '_omni_sylius_node_images_widget' => [$this, 'block__omni_sylius_node_images_widget'],
            '_omni_sylius_node_images_entry_relation_type_widget' => [$this, 'block__omni_sylius_node_images_entry_relation_type_widget'],
            '_omni_sylius_node_translations_entry_seoMetadata_row' => [$this, 'block__omni_sylius_node_translations_entry_seoMetadata_row'],
            '_omni_sylius_node_translations_entry_seoMetadata_extraHttp_row' => [$this, 'block__omni_sylius_node_translations_entry_seoMetadata_extraHttp_row'],
            '_omni_sylius_node_translations_entry_seoMetadata_extraNames_row' => [$this, 'block__omni_sylius_node_translations_entry_seoMetadata_extraNames_row'],
            '_omni_sylius_node_translations_entry_seoMetadata_extraProperties_row' => [$this, 'block__omni_sylius_node_translations_entry_seoMetadata_extraProperties_row'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusUi/Form/imagesTheme.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusCmsPlugin:Form:imagesTheme.html.twig"));

        $this->parent = $this->loadTemplate("@SyliusUi/Form/imagesTheme.html.twig", "OmniSyliusCmsPlugin:Form:imagesTheme.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block__omni_sylius_node_images_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_omni_sylius_node_images_widget"));

        // line 4
        $macros["macros"] = $this;
        // line 5
        echo "    ";
        $macros["__internal_parse_31"] = $this->loadTemplate("SyliusResourceBundle:Macros:notification.html.twig", "OmniSyliusCmsPlugin:Form:imagesTheme.html.twig", 5)->unwrap();
        // line 6
        echo "    ";
        $context["attr"] = twig_array_merge((isset($context["attr"]) || array_key_exists("attr", $context) ? $context["attr"] : (function () { throw new RuntimeError('Variable "attr" does not exist.', 6, $this->source); })()), ["class" => (((twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 6)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 6))) : ("")) . " controls collection-widget")]);
        // line 7
        echo "
    ";
        // line 8
        ob_start();
        // line 9
        echo "        <div data-form-type=\"collection\" ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo "
                ";
        // line 10
        if ((array_key_exists("prototype", $context) && (isset($context["allow_add"]) || array_key_exists("allow_add", $context) ? $context["allow_add"] : (function () { throw new RuntimeError('Variable "allow_add" does not exist.', 10, $this->source); })()))) {
            // line 11
            echo "                    data-prototype='";
            echo twig_escape_filter($this->env, twig_call_macro($macros["macros"], "macro_collection_item", [(isset($context["prototype"]) || array_key_exists("prototype", $context) ? $context["prototype"] : (function () { throw new RuntimeError('Variable "prototype" does not exist.', 11, $this->source); })()), (isset($context["allow_delete"]) || array_key_exists("allow_delete", $context) ? $context["allow_delete"] : (function () { throw new RuntimeError('Variable "allow_delete" does not exist.', 11, $this->source); })()), (isset($context["button_delete_label"]) || array_key_exists("button_delete_label", $context) ? $context["button_delete_label"] : (function () { throw new RuntimeError('Variable "button_delete_label" does not exist.', 11, $this->source); })()), "__name__"], 11, $context, $this->getSourceContext()));
            echo "'";
        }
        // line 13
        echo ">
            ";
        // line 14
        echo twig_call_macro($macros["__internal_parse_31"], "macro_error", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "vars", [], "any", false, false, false, 14), "errors", [], "any", false, false, false, 14)], 14, $context, $this->getSourceContext());
        echo "

            ";
        // line 16
        if (twig_test_iterable(((array_key_exists("prototypes", $context)) ? (_twig_default_filter((isset($context["prototypes"]) || array_key_exists("prototypes", $context) ? $context["prototypes"] : (function () { throw new RuntimeError('Variable "prototypes" does not exist.', 16, $this->source); })()))) : ("")))) {
            // line 17
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["prototypes"]) || array_key_exists("prototypes", $context) ? $context["prototypes"] : (function () { throw new RuntimeError('Variable "prototypes" does not exist.', 17, $this->source); })()));
            foreach ($context['_seq'] as $context["key"] => $context["subPrototype"]) {
                // line 18
                echo "                    <input type=\"hidden\" data-form-prototype=\"";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" value=\"";
                echo twig_escape_filter($this->env, twig_call_macro($macros["macros"], "macro_collection_item", [$context["subPrototype"], (isset($context["allow_delete"]) || array_key_exists("allow_delete", $context) ? $context["allow_delete"] : (function () { throw new RuntimeError('Variable "allow_delete" does not exist.', 18, $this->source); })()), (isset($context["button_delete_label"]) || array_key_exists("button_delete_label", $context) ? $context["button_delete_label"] : (function () { throw new RuntimeError('Variable "button_delete_label" does not exist.', 18, $this->source); })()), "__name__"], 18, $context, $this->getSourceContext()));
                echo "\" />
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['subPrototype'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "            ";
        }
        // line 21
        echo "
            <div data-form-collection=\"list\" class=\"ui three column stackable grid\">
                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 24
            echo "                    ";
            echo twig_call_macro($macros["macros"], "macro_collection_item", [$context["child"], (isset($context["allow_delete"]) || array_key_exists("allow_delete", $context) ? $context["allow_delete"] : (function () { throw new RuntimeError('Variable "allow_delete" does not exist.', 24, $this->source); })()), (isset($context["button_delete_label"]) || array_key_exists("button_delete_label", $context) ? $context["button_delete_label"] : (function () { throw new RuntimeError('Variable "button_delete_label" does not exist.', 24, $this->source); })()), twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, false, 24)], 24, $context, $this->getSourceContext());
            echo "
                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "            </div>

            ";
        // line 28
        if ((array_key_exists("prototype", $context) && (isset($context["allow_add"]) || array_key_exists("allow_add", $context) ? $context["allow_add"] : (function () { throw new RuntimeError('Variable "allow_add" does not exist.', 28, $this->source); })()))) {
            // line 29
            echo "                <a href=\"#\" class=\"ui labeled icon button\" data-form-collection=\"add\">
                    <i class=\"plus square outline icon\"></i>
                    ";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["button_add_label"]) || array_key_exists("button_add_label", $context) ? $context["button_add_label"] : (function () { throw new RuntimeError('Variable "button_add_label" does not exist.', 31, $this->source); })())), "html", null, true);
            echo "
                </a>
            ";
        }
        // line 34
        echo "        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 38
    public function block__omni_sylius_node_images_entry_relation_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_omni_sylius_node_images_entry_relation_type_widget"));

        // line 39
        if ((((((isset($context["required"]) || array_key_exists("required", $context) ? $context["required"] : (function () { throw new RuntimeError('Variable "required" does not exist.', 39, $this->source); })()) && (null === (isset($context["placeholder"]) || array_key_exists("placeholder", $context) ? $context["placeholder"] : (function () { throw new RuntimeError('Variable "placeholder" does not exist.', 39, $this->source); })()))) &&  !(isset($context["placeholder_in_choices"]) || array_key_exists("placeholder_in_choices", $context) ? $context["placeholder_in_choices"] : (function () { throw new RuntimeError('Variable "placeholder_in_choices" does not exist.', 39, $this->source); })())) &&  !(isset($context["multiple"]) || array_key_exists("multiple", $context) ? $context["multiple"] : (function () { throw new RuntimeError('Variable "multiple" does not exist.', 39, $this->source); })())) && ( !twig_get_attribute($this->env, $this->source, ($context["attr"] ?? null), "size", [], "any", true, true, false, 39) || (twig_get_attribute($this->env, $this->source, (isset($context["attr"]) || array_key_exists("attr", $context) ? $context["attr"] : (function () { throw new RuntimeError('Variable "attr" does not exist.', 39, $this->source); })()), "size", [], "any", false, false, false, 39) <= 1)))) {
            // line 40
            $context["required"] = false;
        }
        // line 42
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if ((isset($context["multiple"]) || array_key_exists("multiple", $context) ? $context["multiple"] : (function () { throw new RuntimeError('Variable "multiple" does not exist.', 42, $this->source); })())) {
            echo " multiple=\"multiple\"";
        }
        echo " onchange=\"resetDropdown(\$(this));\">";
        // line 43
        if ( !(null === (isset($context["placeholder"]) || array_key_exists("placeholder", $context) ? $context["placeholder"] : (function () { throw new RuntimeError('Variable "placeholder" does not exist.', 43, $this->source); })()))) {
            // line 44
            echo "<option value=\"\"";
            if (((isset($context["required"]) || array_key_exists("required", $context) ? $context["required"] : (function () { throw new RuntimeError('Variable "required" does not exist.', 44, $this->source); })()) && twig_test_empty((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new RuntimeError('Variable "value" does not exist.', 44, $this->source); })())))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            ((((isset($context["placeholder"]) || array_key_exists("placeholder", $context) ? $context["placeholder"] : (function () { throw new RuntimeError('Variable "placeholder" does not exist.', 44, $this->source); })()) != "")) ? (print (twig_escape_filter($this->env, ((((isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new RuntimeError('Variable "translation_domain" does not exist.', 44, $this->source); })()) === false)) ? ((isset($context["placeholder"]) || array_key_exists("placeholder", $context) ? $context["placeholder"] : (function () { throw new RuntimeError('Variable "placeholder" does not exist.', 44, $this->source); })())) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["placeholder"]) || array_key_exists("placeholder", $context) ? $context["placeholder"] : (function () { throw new RuntimeError('Variable "placeholder" does not exist.', 44, $this->source); })()), [], (isset($context["translation_domain"]) || array_key_exists("translation_domain", $context) ? $context["translation_domain"] : (function () { throw new RuntimeError('Variable "translation_domain" does not exist.', 44, $this->source); })())))), "html", null, true))) : (print ("")));
            echo "</option>";
        }
        // line 46
        if ((twig_length_filter($this->env, (isset($context["preferred_choices"]) || array_key_exists("preferred_choices", $context) ? $context["preferred_choices"] : (function () { throw new RuntimeError('Variable "preferred_choices" does not exist.', 46, $this->source); })())) > 0)) {
            // line 47
            $context["options"] = (isset($context["preferred_choices"]) || array_key_exists("preferred_choices", $context) ? $context["preferred_choices"] : (function () { throw new RuntimeError('Variable "preferred_choices" does not exist.', 47, $this->source); })());
            // line 48
            $this->displayBlock("choice_widget_options", $context, $blocks);
            // line 49
            if (((twig_length_filter($this->env, (isset($context["choices"]) || array_key_exists("choices", $context) ? $context["choices"] : (function () { throw new RuntimeError('Variable "choices" does not exist.', 49, $this->source); })())) > 0) &&  !(null === (isset($context["separator"]) || array_key_exists("separator", $context) ? $context["separator"] : (function () { throw new RuntimeError('Variable "separator" does not exist.', 49, $this->source); })())))) {
                // line 50
                echo "<option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, (isset($context["separator"]) || array_key_exists("separator", $context) ? $context["separator"] : (function () { throw new RuntimeError('Variable "separator" does not exist.', 50, $this->source); })()), "html", null, true);
                echo "</option>";
            }
        }
        // line 53
        $context["options"] = (isset($context["choices"]) || array_key_exists("choices", $context) ? $context["choices"] : (function () { throw new RuntimeError('Variable "choices" does not exist.', 53, $this->source); })());
        // line 54
        $this->displayBlock("choice_widget_options", $context, $blocks);
        // line 55
        echo "</select>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 58
    public function block__omni_sylius_node_translations_entry_seoMetadata_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_omni_sylius_node_translations_entry_seoMetadata_row"));

        // line 59
        echo "<h4 class=\"ui\">";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 59, $this->source); })()), 'label');
        echo "</h4>
    <div class=\"ui box segment\">
        ";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 61, $this->source); })()), 'widget');
        echo "
    </div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 65
    public function block__omni_sylius_node_translations_entry_seoMetadata_extraHttp_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_omni_sylius_node_translations_entry_seoMetadata_extraHttp_row"));

        // line 66
        $macros["macros"] = $this;
        // line 67
        echo "    ";
        echo twig_call_macro($macros["macros"], "macro_metadata_collection_item", [(isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 67, $this->source); })())], 67, $context, $this->getSourceContext());
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 70
    public function block__omni_sylius_node_translations_entry_seoMetadata_extraNames_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_omni_sylius_node_translations_entry_seoMetadata_extraNames_row"));

        // line 71
        $macros["macros"] = $this;
        // line 72
        echo "    ";
        echo twig_call_macro($macros["macros"], "macro_metadata_collection_item", [(isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 72, $this->source); })())], 72, $context, $this->getSourceContext());
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 75
    public function block__omni_sylius_node_translations_entry_seoMetadata_extraProperties_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "_omni_sylius_node_translations_entry_seoMetadata_extraProperties_row"));

        // line 76
        $macros["macros"] = $this;
        // line 77
        echo "    ";
        echo twig_call_macro($macros["macros"], "macro_metadata_collection_item", [(isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 77, $this->source); })())], 77, $context, $this->getSourceContext());
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 80
    public function macro_metadata_collection_item($__form__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "metadata_collection_item"));

            // line 81
            echo "    <div class=\"ui segment\">
        <div class=\"ui dividing header\" style=\"font-size: .92857143em\">
            ";
            // line 83
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 83, $this->source); })()), 'label');
            echo "
        </div>
        <br>
        <div class=\"field\">
            ";
            // line 87
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 87, $this->source); })()), 'widget');
            echo "
        </div>
    </div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 92
    public function macro_collection_item($__form__ = null, $__allow_delete__ = null, $__button_delete_label__ = null, $__index__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "allow_delete" => $__allow_delete__,
            "button_delete_label" => $__button_delete_label__,
            "index" => $__index__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "collection_item"));

            // line 93
            echo "    ";
            ob_start();
            // line 94
            echo "        <div data-form-collection=\"item\" data-form-collection-index=\"";
            echo twig_escape_filter($this->env, (isset($context["index"]) || array_key_exists("index", $context) ? $context["index"] : (function () { throw new RuntimeError('Variable "index" does not exist.', 94, $this->source); })()), "html", null, true);
            echo "\" class=\"column\">
            <div class=\"ui upload box segment\">
                ";
            // line 96
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 96, $this->source); })()), "type", [], "any", false, false, false, 96), 'row');
            echo "
                <label for=\"";
            // line 97
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 97, $this->source); })()), "file", [], "any", false, false, false, 97), "vars", [], "any", false, false, false, 97), "id", [], "any", false, false, false, 97), "html", null, true);
            echo "\" class=\"ui icon labeled button\"><i class=\"cloud upload icon\"></i> ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.choose_file"), "html", null, true);
            echo "</label>
                ";
            // line 98
            if ( !(null === ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 98), "value", [], "any", false, true, false, 98), "path", [], "any", true, true, false, 98)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 98), "value", [], "any", false, true, false, 98), "path", [], "any", false, false, false, 98), null)) : (null)))) {
                // line 99
                echo "                    <img class=\"ui small bordered image\" src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 99, $this->source); })()), "vars", [], "any", false, false, false, 99), "value", [], "any", false, false, false, 99), "path", [], "any", false, false, false, 99), "sylius_small"), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 99, $this->source); })()), "vars", [], "any", false, false, false, 99), "value", [], "any", false, false, false, 99), "type", [], "any", false, false, false, 99), "html", null, true);
                echo "\" />
                ";
            }
            // line 101
            echo "                <div class=\"ui hidden element\">
                    ";
            // line 102
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 102, $this->source); })()), "file", [], "any", false, false, false, 102), 'widget');
            echo "
                </div>
                ";
            // line 104
            $context["nodeImageConfig"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeImageConfig(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 104, $this->source); })()), "parent", [], "any", false, false, false, 104), "parent", [], "any", false, false, false, 104), "vars", [], "any", false, false, false, 104), "data", [], "any", false, false, false, 104));
            // line 105
            echo "                ";
            $context["relation_type"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 105), "data", [], "any", false, true, false, 105), "relationType", [], "any", true, true, false, 105)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 105), "data", [], "any", false, true, false, 105), "relationType", [], "any", false, false, false, 105), "0")) : ("0"));
            // line 106
            echo "
                ";
            // line 107
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["nodeImageConfig"]) || array_key_exists("nodeImageConfig", $context) ? $context["nodeImageConfig"] : (function () { throw new RuntimeError('Variable "nodeImageConfig" does not exist.', 107, $this->source); })()), "available_positions", [], "any", false, false, false, 107)) > 0)) {
                // line 108
                echo "                    ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 108, $this->source); })()), "position", [], "any", false, false, false, 108), 'row');
                echo "
                ";
            }
            // line 110
            echo "
                ";
            // line 111
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["nodeImageConfig"]) || array_key_exists("nodeImageConfig", $context) ? $context["nodeImageConfig"] : (function () { throw new RuntimeError('Variable "nodeImageConfig" does not exist.', 111, $this->source); })()), "relations", [], "any", false, false, false, 111)) > 0)) {
                // line 112
                echo "                    <div class=\"ui element image-relations\">
                        ";
                // line 113
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 113, $this->source); })()), "relation_type", [], "any", false, false, false, 113), 'row');
                echo "
                        ";
                // line 114
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 114, $this->source); })()), "relation", [], "any", false, false, false, 114), 'row', ["remote_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_autocomplete_resource", ["type" =>                 // line 115
(isset($context["relation_type"]) || array_key_exists("relation_type", $context) ? $context["relation_type"] : (function () { throw new RuntimeError('Variable "relation_type" does not exist.', 115, $this->source); })())]), "remote_criteria_type" => "contains", "remote_criteria_name" => "phrase", "load_edit_url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_autocomplete_resource_id", ["type" =>                 // line 118
(isset($context["relation_type"]) || array_key_exists("relation_type", $context) ? $context["relation_type"] : (function () { throw new RuntimeError('Variable "relation_type" does not exist.', 118, $this->source); })())])]);
                echo "
                    </div>
                ";
            }
            // line 121
            echo "
                ";
            // line 122
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["nodeImageConfig"]) || array_key_exists("nodeImageConfig", $context) ? $context["nodeImageConfig"] : (function () { throw new RuntimeError('Variable "nodeImageConfig" does not exist.', 122, $this->source); })()), "available_positions", [], "any", false, false, false, 122)) > 0)) {
                // line 123
                echo "                    ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 123, $this->source); })()), "position", [], "any", false, false, false, 123), 'row');
                echo "
                ";
            }
            // line 125
            echo "
                ";
            // line 126
            if (twig_get_attribute($this->env, $this->source, (isset($context["nodeImageConfig"]) || array_key_exists("nodeImageConfig", $context) ? $context["nodeImageConfig"] : (function () { throw new RuntimeError('Variable "nodeImageConfig" does not exist.', 126, $this->source); })()), "title_aware", [], "any", false, false, false, 126)) {
                // line 127
                echo "                    <div class=\"ui divider\"></div>

                    ";
                // line 129
                $macros["__internal_parse_32"] = $this->loadTemplate("@SyliusAdmin/Macro/translationForm.html.twig", "OmniSyliusCmsPlugin:Form:imagesTheme.html.twig", 129)->unwrap();
                // line 130
                echo "
                    ";
                // line 131
                echo twig_call_macro($macros["__internal_parse_32"], "macro_translationForm", [twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 131, $this->source); })()), "translations", [], "any", false, false, false, 131)], 131, $context, $this->getSourceContext());
                echo "
                ";
            }
            // line 133
            echo "
                <div class=\"ui element\">";
            // line 135
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 135, $this->source); })()), "file", [], "any", false, false, false, 135), 'errors');
            // line 136
            echo "</div>
            </div>
            ";
            // line 138
            if ((isset($context["allow_delete"]) || array_key_exists("allow_delete", $context) ? $context["allow_delete"] : (function () { throw new RuntimeError('Variable "allow_delete" does not exist.', 138, $this->source); })())) {
                // line 139
                echo "                <a href=\"#\" data-form-collection=\"delete\" class=\"ui red labeled icon button\" style=\"margin-bottom: 1em;\">
                    <i class=\"trash icon\"></i>
                    ";
                // line 141
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["button_delete_label"]) || array_key_exists("button_delete_label", $context) ? $context["button_delete_label"] : (function () { throw new RuntimeError('Variable "button_delete_label" does not exist.', 141, $this->source); })())), "html", null, true);
                echo "
                </a>
            ";
            }
            // line 144
            echo "        </div>
    ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "OmniSyliusCmsPlugin:Form:imagesTheme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  495 => 144,  489 => 141,  485 => 139,  483 => 138,  479 => 136,  477 => 135,  474 => 133,  469 => 131,  466 => 130,  464 => 129,  460 => 127,  458 => 126,  455 => 125,  449 => 123,  447 => 122,  444 => 121,  438 => 118,  437 => 115,  436 => 114,  432 => 113,  429 => 112,  427 => 111,  424 => 110,  418 => 108,  416 => 107,  413 => 106,  410 => 105,  408 => 104,  403 => 102,  400 => 101,  392 => 99,  390 => 98,  384 => 97,  380 => 96,  374 => 94,  371 => 93,  352 => 92,  336 => 87,  329 => 83,  325 => 81,  309 => 80,  301 => 77,  299 => 76,  292 => 75,  284 => 72,  282 => 71,  275 => 70,  267 => 67,  265 => 66,  258 => 65,  249 => 61,  243 => 59,  236 => 58,  229 => 55,  227 => 54,  225 => 53,  219 => 50,  217 => 49,  215 => 48,  213 => 47,  211 => 46,  202 => 44,  200 => 43,  193 => 42,  190 => 40,  188 => 39,  181 => 38,  172 => 34,  166 => 31,  162 => 29,  160 => 28,  156 => 26,  139 => 24,  122 => 23,  118 => 21,  115 => 20,  104 => 18,  99 => 17,  97 => 16,  92 => 14,  89 => 13,  84 => 11,  82 => 10,  77 => 9,  75 => 8,  72 => 7,  69 => 6,  66 => 5,  64 => 4,  57 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusUi/Form/imagesTheme.html.twig' %}

{% block _omni_sylius_node_images_widget -%}
    {% import _self as macros %}
    {% from 'SyliusResourceBundle:Macros:notification.html.twig' import error %}
    {% set attr = attr|merge({'class': attr.class|default ~ ' controls collection-widget'}) %}

    {% spaceless %}
        <div data-form-type=\"collection\" {{ block('widget_container_attributes') }}
                {% if prototype is defined and allow_add %}
                    data-prototype='{{ macros.collection_item(prototype, allow_delete, button_delete_label, '__name__')|e }}'
                {%- endif -%}
        >
            {{ error(form.vars.errors) }}

            {% if prototypes|default is iterable %}
                {% for key, subPrototype in prototypes %}
                    <input type=\"hidden\" data-form-prototype=\"{{ key }}\" value=\"{{ macros.collection_item(subPrototype, allow_delete, button_delete_label, '__name__')|e }}\" />
                {% endfor %}
            {% endif %}

            <div data-form-collection=\"list\" class=\"ui three column stackable grid\">
                {% for child in form %}
                    {{ macros.collection_item(child, allow_delete, button_delete_label, loop.index0) }}
                {% endfor %}
            </div>

            {% if prototype is defined and allow_add %}
                <a href=\"#\" class=\"ui labeled icon button\" data-form-collection=\"add\">
                    <i class=\"plus square outline icon\"></i>
                    {{ button_add_label|trans }}
                </a>
            {% endif %}
        </div>
    {% endspaceless %}
{%- endblock _omni_sylius_node_images_widget %}

{% block _omni_sylius_node_images_entry_relation_type_widget -%}
    {%- if required and placeholder is none and not placeholder_in_choices and not multiple and (attr.size is not defined or attr.size <= 1) -%}
        {% set required = false %}
    {%- endif -%}
    <select {{ block('widget_attributes') }}{% if multiple %} multiple=\"multiple\"{% endif %} onchange=\"resetDropdown(\$(this));\">
        {%- if placeholder is not none -%}
            <option value=\"\"{% if required and value is empty %} selected=\"selected\"{% endif %}>{{ placeholder != '' ? (translation_domain is same as(false) ? placeholder : placeholder|trans({}, translation_domain)) }}</option>
        {%- endif -%}
        {%- if preferred_choices|length > 0 -%}
            {% set options = preferred_choices %}
            {{- block('choice_widget_options') -}}
            {%- if choices|length > 0 and separator is not none -%}
                <option disabled=\"disabled\">{{ separator }}</option>
            {%- endif -%}
        {%- endif -%}
        {%- set options = choices -%}
        {{- block('choice_widget_options') -}}
    </select>
{%- endblock _omni_sylius_node_images_entry_relation_type_widget %}

{% block _omni_sylius_node_translations_entry_seoMetadata_row -%}
    <h4 class=\"ui\">{{ form_label(form) }}</h4>
    <div class=\"ui box segment\">
        {{ form_widget(form) }}
    </div>
{%- endblock _omni_sylius_node_translations_entry_seoMetadata_row %}

{% block _omni_sylius_node_translations_entry_seoMetadata_extraHttp_row -%}
    {% import _self as macros %}
    {{ macros.metadata_collection_item(form) }}
{%- endblock _omni_sylius_node_translations_entry_seoMetadata_extraHttp_row %}

{% block _omni_sylius_node_translations_entry_seoMetadata_extraNames_row -%}
    {% import _self as macros %}
    {{ macros.metadata_collection_item(form) }}
{%- endblock _omni_sylius_node_translations_entry_seoMetadata_extraNames_row %}

{% block _omni_sylius_node_translations_entry_seoMetadata_extraProperties_row -%}
    {% import _self as macros %}
    {{ macros.metadata_collection_item(form) }}
{%- endblock _omni_sylius_node_translations_entry_seoMetadata_extraProperties_row %}

{% macro metadata_collection_item(form) %}
    <div class=\"ui segment\">
        <div class=\"ui dividing header\" style=\"font-size: .92857143em\">
            {{ form_label(form) }}
        </div>
        <br>
        <div class=\"field\">
            {{ form_widget(form) }}
        </div>
    </div>
{% endmacro %}

{% macro collection_item(form, allow_delete, button_delete_label, index) %}
    {% spaceless %}
        <div data-form-collection=\"item\" data-form-collection-index=\"{{ index }}\" class=\"column\">
            <div class=\"ui upload box segment\">
                {{ form_row(form.type) }}
                <label for=\"{{ form.file.vars.id }}\" class=\"ui icon labeled button\"><i class=\"cloud upload icon\"></i> {{ 'sylius.ui.choose_file'|trans }}</label>
                {% if form.vars.value.path|default(null) is not null %}
                    <img class=\"ui small bordered image\" src=\"{{ form.vars.value.path|imagine_filter('sylius_small') }}\" alt=\"{{ form.vars.value.type }}\" />
                {% endif %}
                <div class=\"ui hidden element\">
                    {{ form_widget(form.file) }}
                </div>
                {% set nodeImageConfig = omni_sylius_get_node_image_config(form.parent.parent.vars.data) %}
                {% set relation_type = form.vars.data.relationType | default('0') %}

                {% if nodeImageConfig.available_positions|length > 0 %}
                    {{ form_row(form.position) }}
                {% endif %}

                {% if nodeImageConfig.relations|length > 0 %}
                    <div class=\"ui element image-relations\">
                        {{ form_row(form.relation_type) }}
                        {{ form_row(form.relation, {
                            'remote_url': path('omni_sylius_admin_autocomplete_resource', {'type': relation_type }),
                            'remote_criteria_type': 'contains',
                            'remote_criteria_name': 'phrase',
                            'load_edit_url': path('omni_sylius_admin_autocomplete_resource_id', {'type': relation_type })}) }}
                    </div>
                {% endif %}

                {% if nodeImageConfig.available_positions|length > 0 %}
                    {{ form_row(form.position) }}
                {% endif %}

                {% if nodeImageConfig.title_aware %}
                    <div class=\"ui divider\"></div>

                    {% from '@SyliusAdmin/Macro/translationForm.html.twig' import translationForm %}

                    {{ translationForm(form.translations) }}
                {% endif %}

                <div class=\"ui element\">
                    {{- form_errors(form.file) -}}
                </div>
            </div>
            {% if allow_delete %}
                <a href=\"#\" data-form-collection=\"delete\" class=\"ui red labeled icon button\" style=\"margin-bottom: 1em;\">
                    <i class=\"trash icon\"></i>
                    {{ button_delete_label|trans }}
                </a>
            {% endif %}
        </div>
    {% endspaceless %}
{% endmacro %}
", "OmniSyliusCmsPlugin:Form:imagesTheme.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Form/imagesTheme.html.twig");
    }
}
