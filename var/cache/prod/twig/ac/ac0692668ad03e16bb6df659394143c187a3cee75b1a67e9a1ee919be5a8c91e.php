<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusBannerPlugin/PositionTypes/_big_wide_slider.html.twig */
class __TwigTemplate_57a2dc99cf1eed0a2ead3206e66b509c02a94de54a570454891432ebf49fb351 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusBannerPlugin/PositionTypes/_big_wide_slider.html.twig"));

        // line 2
        echo "
";
        // line 3
        if ((twig_length_filter($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 3, $this->source); })())) > 0)) {
            // line 4
            echo "    ";
            $context["bannerImagesCount"] = 0;
            // line 5
            echo "
    ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 6, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 6, $this->source); })()), "channel", [], "array", false, false, false, 6)], "method", false, false, false, 6)) {
                    // line 7
                    echo "        ";
                    $context["bannerImagesCount"] = ((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 7, $this->source); })()) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 7)));
                    // line 8
                    echo "    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "
    <div class=\"container\">
        ";
            // line 11
            $context["idName"] = twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 11, $this->source); })()), "code", [], "any", false, false, false, 11);
            // line 12
            echo "
        <div id=\"";
            // line 13
            echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 13, $this->source); })()), "html", null, true);
            echo "\" class=\"carousel slide banners-slider\" data-ride=\"carousel\"
             ";
            // line 14
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 14, $this->source); })()) == 1)) {
                echo "data-interval=\"false\"";
            }
            // line 15
            echo "        >
            ";
            // line 16
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 16, $this->source); })()) > 1)) {
                // line 17
                echo "                <ol class=\"carousel-indicators\">
                    ";
                // line 18
                $context["bannerIndex"] = 0;
                // line 19
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 19, $this->source); })()));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                    if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 19, $this->source); })()), "channel", [], "array", false, false, false, 19)], "method", false, false, false, 19)) {
                        // line 20
                        echo "                        ";
                        $context["isFirstBanner"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 20)) ? (true) : (false));
                        // line 21
                        echo "
                        ";
                        // line 22
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 22));
                        $context['loop'] = [
                          'parent' => $context['_parent'],
                          'index0' => 0,
                          'index'  => 1,
                          'first'  => true,
                        ];
                        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                            $length = count($context['_seq']);
                            $context['loop']['revindex0'] = $length - 1;
                            $context['loop']['revindex'] = $length;
                            $context['loop']['length'] = $length;
                            $context['loop']['last'] = 1 === $length;
                        }
                        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                            // line 23
                            echo "                            ";
                            $context["isFirstImage"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 23)) ? (true) : (false));
                            // line 24
                            echo "                            ";
                            $context["isActive"] = ((((isset($context["isFirstBanner"]) || array_key_exists("isFirstBanner", $context) ? $context["isFirstBanner"] : (function () { throw new RuntimeError('Variable "isFirstBanner" does not exist.', 24, $this->source); })()) && (isset($context["isFirstImage"]) || array_key_exists("isFirstImage", $context) ? $context["isFirstImage"] : (function () { throw new RuntimeError('Variable "isFirstImage" does not exist.', 24, $this->source); })()))) ? ("active") : (""));
                            // line 25
                            echo "
                            <li class=\"carousel-indicator ";
                            // line 26
                            echo twig_escape_filter($this->env, (isset($context["isActive"]) || array_key_exists("isActive", $context) ? $context["isActive"] : (function () { throw new RuntimeError('Variable "isActive" does not exist.', 26, $this->source); })()), "html", null, true);
                            echo "\"
                                data-target=\"#";
                            // line 27
                            echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 27, $this->source); })()), "html", null, true);
                            echo "\"
                                data-slide-to=\"";
                            // line 28
                            echo twig_escape_filter($this->env, (isset($context["bannerIndex"]) || array_key_exists("bannerIndex", $context) ? $context["bannerIndex"] : (function () { throw new RuntimeError('Variable "bannerIndex" does not exist.', 28, $this->source); })()), "html", null, true);
                            echo "\"
                            >
                            </li>

                            ";
                            // line 32
                            $context["bannerIndex"] = ((isset($context["bannerIndex"]) || array_key_exists("bannerIndex", $context) ? $context["bannerIndex"] : (function () { throw new RuntimeError('Variable "bannerIndex" does not exist.', 32, $this->source); })()) + 1);
                            // line 33
                            echo "                        ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                            if (isset($context['loop']['length'])) {
                                --$context['loop']['revindex0'];
                                --$context['loop']['revindex'];
                                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 34
                        echo "                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 35
                echo "                </ol>
            ";
            }
            // line 37
            echo "
            <div class=\"carousel-inner\" role=\"listbox\">
                ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 39, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 39, $this->source); })()), "channel", [], "array", false, false, false, 39)], "method", false, false, false, 39)) {
                    // line 40
                    echo "                    ";
                    $context["isFirstBanner"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 40)) ? (true) : (false));
                    // line 41
                    echo "
                    ";
                    // line 42
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 42));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        // line 43
                        echo "                        ";
                        $context["isFirstImage"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 43)) ? (true) : (false));
                        // line 44
                        echo "                        ";
                        $context["isActive"] = ((((isset($context["isFirstBanner"]) || array_key_exists("isFirstBanner", $context) ? $context["isFirstBanner"] : (function () { throw new RuntimeError('Variable "isFirstBanner" does not exist.', 44, $this->source); })()) && (isset($context["isFirstImage"]) || array_key_exists("isFirstImage", $context) ? $context["isFirstImage"] : (function () { throw new RuntimeError('Variable "isFirstImage" does not exist.', 44, $this->source); })()))) ? ("active") : (""));
                        // line 45
                        echo "
                        <div class=\"carousel-item ";
                        // line 46
                        echo twig_escape_filter($this->env, (isset($context["isActive"]) || array_key_exists("isActive", $context) ? $context["isActive"] : (function () { throw new RuntimeError('Variable "isActive" does not exist.', 46, $this->source); })()), "html", null, true);
                        echo "\">
                            <a class=\"carousel-item-link\" href=\"";
                        // line 47
                        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 47), "link", [], "any", true, true, false, 47)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 47), "link", [], "any", false, false, false, 47), "javascript:;")) : ("javascript:;")), "html", null, true);
                        echo "\">
                                ";
                        // line 48
                        $context["imagePath"] = "";
                        // line 49
                        echo "
                                ";
                        // line 50
                        if ((array_key_exists("image", $context) && twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 50))) {
                            // line 51
                            echo "                                    ";
                            $context["imagePath"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 51), "omni_sylius_banner");
                            // line 52
                            echo "                                ";
                        }
                        // line 53
                        echo "
                                <img class=\"carousel-item-link__img d-block img-fluid\" src=\"";
                        // line 54
                        echo twig_escape_filter($this->env, (isset($context["imagePath"]) || array_key_exists("imagePath", $context) ? $context["imagePath"] : (function () { throw new RuntimeError('Variable "imagePath" does not exist.', 54, $this->source); })()), "html", null, true);
                        echo "\" alt=\"-\">

                                <div class=\"carousel-caption caption--";
                        // line 56
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentPosition", [], "any", false, false, false, 56), "html", null, true);
                        echo "\"
                                    ";
                        // line 57
                        if (twig_get_attribute($this->env, $this->source, $context["image"], "contentSpace", [], "any", false, false, false, 57)) {
                            echo "style=\"width: ";
                            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentSpace", [], "any", false, false, false, 57)), "html", null, true);
                            echo "%\"";
                        }
                        // line 58
                        echo "                                >
                                    ";
                        // line 59
                        echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 59), "content", [], "any", false, false, false, 59);
                        echo "
                                </div>
                            </a>
                        </div>
                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 64
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 65
            echo "            </div>

            ";
            // line 67
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 67, $this->source); })()) > 1)) {
                // line 68
                echo "                <a class=\"carousel-control-prev circle-icon\" href=\"#";
                echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 68, $this->source); })()), "html", null, true);
                echo "\" role=\"button\" data-slide=\"prev\">
                    <span class=\"carousel-control-prev-icon chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">";
                // line 70
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.previous"), "html", null, true);
                echo "</span>
                </a>

                <a class=\"carousel-control-next circle-icon\" href=\"#";
                // line 73
                echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 73, $this->source); })()), "html", null, true);
                echo "\" role=\"button\" data-slide=\"next\">
                    <span class=\"carousel-control-next-icon chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">";
                // line 75
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.next"), "html", null, true);
                echo "</span>
                </a>
            ";
            }
            // line 78
            echo "        </div>
    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusBannerPlugin/PositionTypes/_big_wide_slider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  329 => 78,  323 => 75,  318 => 73,  312 => 70,  306 => 68,  304 => 67,  300 => 65,  290 => 64,  271 => 59,  268 => 58,  262 => 57,  258 => 56,  253 => 54,  250 => 53,  247 => 52,  244 => 51,  242 => 50,  239 => 49,  237 => 48,  233 => 47,  229 => 46,  226 => 45,  223 => 44,  220 => 43,  203 => 42,  200 => 41,  197 => 40,  186 => 39,  182 => 37,  178 => 35,  168 => 34,  154 => 33,  152 => 32,  145 => 28,  141 => 27,  137 => 26,  134 => 25,  131 => 24,  128 => 23,  111 => 22,  108 => 21,  105 => 20,  93 => 19,  91 => 18,  88 => 17,  86 => 16,  83 => 15,  79 => 14,  75 => 13,  72 => 12,  70 => 11,  66 => 9,  59 => 8,  56 => 7,  51 => 6,  48 => 5,  45 => 4,  43 => 3,  40 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# this is demo slider made with Bootstrap 4 #}

{% if banners|length > 0 %}
    {% set bannerImagesCount = 0 %}

    {% for banner in banners if banner.hasChannelCode(options['channel']) %}
        {% set bannerImagesCount = bannerImagesCount + banner.images|length %}
    {% endfor %}

    <div class=\"container\">
        {% set idName = position.code %}

        <div id=\"{{ idName }}\" class=\"carousel slide banners-slider\" data-ride=\"carousel\"
             {% if bannerImagesCount == 1 %}data-interval=\"false\"{% endif %}
        >
            {% if bannerImagesCount > 1 %}
                <ol class=\"carousel-indicators\">
                    {% set bannerIndex = 0 %}
                    {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                        {% set isFirstBanner = loop.first ? true : false %}

                        {% for image in banner.images %}
                            {% set isFirstImage = loop.first ? true : false %}
                            {% set isActive = (isFirstBanner and isFirstImage) ? 'active': '' %}

                            <li class=\"carousel-indicator {{ isActive }}\"
                                data-target=\"#{{ idName }}\"
                                data-slide-to=\"{{ bannerIndex }}\"
                            >
                            </li>

                            {% set bannerIndex = bannerIndex + 1 %}
                        {% endfor %}
                    {% endfor %}
                </ol>
            {% endif %}

            <div class=\"carousel-inner\" role=\"listbox\">
                {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                    {% set isFirstBanner = loop.first ? true : false %}

                    {% for image in banner.images %}
                        {% set isFirstImage = loop.first ? true : false %}
                        {% set isActive = (isFirstBanner and isFirstImage) ? 'active': '' %}

                        <div class=\"carousel-item {{ isActive }}\">
                            <a class=\"carousel-item-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                                {% set imagePath = '' %}

                                {% if image is defined and image.path %}
                                    {% set imagePath = image.path|imagine_filter('omni_sylius_banner') %}
                                {% endif %}

                                <img class=\"carousel-item-link__img d-block img-fluid\" src=\"{{ imagePath }}\" alt=\"-\">

                                <div class=\"carousel-caption caption--{{ image.contentPosition }}\"
                                    {% if image.contentSpace %}style=\"width: {{ image.contentSpace|number_format }}%\"{% endif %}
                                >
                                    {{ image.translation.content|raw }}
                                </div>
                            </a>
                        </div>
                    {% endfor %}
                {% endfor %}
            </div>

            {% if bannerImagesCount > 1 %}
                <a class=\"carousel-control-prev circle-icon\" href=\"#{{ idName }}\" role=\"button\" data-slide=\"prev\">
                    <span class=\"carousel-control-prev-icon chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">{{ 'omni.ui.previous'|trans }}</span>
                </a>

                <a class=\"carousel-control-next circle-icon\" href=\"#{{ idName }}\" role=\"button\" data-slide=\"next\">
                    <span class=\"carousel-control-next-icon chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">{{ 'omni.ui.next'|trans }}</span>
                </a>
            {% endif %}
        </div>
    </div>
{% endif %}
", "@OmniSyliusBannerPlugin/PositionTypes/_big_wide_slider.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/PositionTypes/_big_wide_slider.html.twig");
    }
}
