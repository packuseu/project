<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Banner/PositionType/four_columns_slider.html.twig */
class __TwigTemplate_b120786dad97c6e598662553bf93b814e5034906f3c413dc2fa05d3199caab75 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Banner/PositionType/four_columns_slider.html.twig"));

        // line 1
        $macros["dividers"] = $this->macros["dividers"] = $this->loadTemplate("@SyliusShop/Common/Macro/sectionDivider.html.twig", "Banner/PositionType/four_columns_slider.html.twig", 1)->unwrap();
        // line 2
        $macros["headers"] = $this->macros["headers"] = $this->loadTemplate("@SyliusShop/Common/Macro/sectionHeaders.html.twig", "Banner/PositionType/four_columns_slider.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        if ((twig_length_filter($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 4, $this->source); })())) > 0)) {
            // line 5
            $context["bannerImagesCount"] = 0;
            // line 6
            echo "
";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 7, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 7, $this->source); })()), "channel", [], "array", false, false, false, 7)], "method", false, false, false, 7)) {
                    // line 8
                    echo "    ";
                    $context["bannerImagesCount"] = ((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 8, $this->source); })()) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 8)));
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-12 banner-title banner-title--request\">
            ";
            // line 13
            echo twig_call_macro($macros["headers"], "macro_default", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 13, $this->source); })()), "translation", [], "any", false, false, false, 13), "title", [], "any", false, false, false, 13)], 13, $context, $this->getSourceContext());
            echo "

            <div class=\"banner-request\">
                <a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("quote", ["locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "request", [], "any", false, false, false, 16), "locale", [], "any", false, false, false, 16)]), "html", null, true);
            echo "\"
                   class=\"primary-button\"
                   id=\"getQuoteHeader\">
                    ";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.get_a_quote"), "html", null, true);
            echo "
                </a>
            </div>
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-12 custom-carousel\">
            ";
            // line 27
            $context["idName"] = twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 27, $this->source); })()), "code", [], "any", false, false, false, 27);
            // line 28
            echo "            <div id=\"";
            echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 28, $this->source); })()), "html", null, true);
            echo "\" class=\"carousel slide\" data-ride=\"carousel\"
                 ";
            // line 29
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 29, $this->source); })()) < 4)) {
                echo "data-interval=\"false\" ";
            } else {
                echo " data-interval=\"100000\" ";
            }
            echo ">

                <div class=\"w-100 carousel-inner\" role=\"listbox\">

                ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 33, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 33, $this->source); })()), "channel", [], "array", false, false, false, 33)], "method", false, false, false, 33)) {
                    // line 34
                    echo "                    ";
                    $context["isFirstBanner"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 34)) ? (true) : (false));
                    // line 35
                    echo "                    ";
                    $context["count"] = 0;
                    // line 36
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 36));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        if ((twig_get_attribute($this->env, $this->source, $context["image"], "contentPosition", [], "any", false, false, false, 36) == "default")) {
                            // line 37
                            echo "                        ";
                            $context["isFirstImage"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 37)) ? (true) : (false));
                            // line 38
                            echo "                        ";
                            $context["isActive"] = ((((isset($context["isFirstBanner"]) || array_key_exists("isFirstBanner", $context) ? $context["isFirstBanner"] : (function () { throw new RuntimeError('Variable "isFirstBanner" does not exist.', 38, $this->source); })()) && (isset($context["isFirstImage"]) || array_key_exists("isFirstImage", $context) ? $context["isFirstImage"] : (function () { throw new RuntimeError('Variable "isFirstImage" does not exist.', 38, $this->source); })()))) ? ("active") : (""));
                            // line 39
                            echo "
                        ";
                            // line 40
                            ob_start();
                            // line 41
                            echo "                            <div class=\"col-md-3 four-columns-card ";
                            if (((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 41, $this->source); })()) != 0)) {
                                echo "mobile-hidden";
                            }
                            echo "\">
                                <a class=\"carousel-item-link\" href=\"";
                            // line 42
                            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 42), "link", [], "any", true, true, false, 42)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 42), "link", [], "any", false, false, false, 42), "javascript:;")) : ("javascript:;")), "html", null, true);
                            echo "\">
                                    ";
                            // line 43
                            $context["imagePath"] = "";
                            // line 44
                            echo "
                                    ";
                            // line 45
                            if ((array_key_exists("image", $context) && twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 45))) {
                                // line 46
                                echo "                                        ";
                                $context["imagePath"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 46), "omni_sylius_banner");
                                // line 47
                                echo "                                    ";
                            }
                            // line 48
                            echo "                                    <img class=\"four-columns-card-img\" src=\"";
                            echo twig_escape_filter($this->env, (isset($context["imagePath"]) || array_key_exists("imagePath", $context) ? $context["imagePath"] : (function () { throw new RuntimeError('Variable "imagePath" does not exist.', 48, $this->source); })()), "html", null, true);
                            echo "\" alt=\"-\">
                                    <div class=\"caption caption--";
                            // line 49
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentPosition", [], "any", false, false, false, 49), "html", null, true);
                            echo " mobile-hidden\"
                                         ";
                            // line 50
                            if (twig_get_attribute($this->env, $this->source, $context["image"], "contentSpace", [], "any", false, false, false, 50)) {
                                echo "style=\"width: ";
                                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentSpace", [], "any", false, false, false, 50)), "html", null, true);
                                echo "%\"";
                            }
                            // line 51
                            echo "                                    >
                                        ";
                            // line 52
                            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 52), "link", [], "any", false, false, false, 52)) {
                                // line 53
                                echo "                                            <span>";
                                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.branner.assoc_cat"), "html", null, true);
                                echo ":</span> <a href=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 53), "link", [], "any", false, false, false, 53), "html", null, true);
                                echo "\">";
                                echo twig_escape_filter($this->env, twig_replace_filter(twig_striptags(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 53), "content", [], "any", false, false, false, 53)), ["&scaron;" => "š"]), "html", null, true);
                                echo "</a>
                                        ";
                            }
                            // line 55
                            echo "                                    </div>
                                </a>
                            </div>
                        ";
                            $context["column"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                            // line 59
                            echo "
                        ";
                            // line 60
                            if ((((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 60, $this->source); })()) == 0) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 60)) == twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 60)))) {
                                // line 61
                                echo "                            <div class=\"carousel-item ";
                                echo twig_escape_filter($this->env, (isset($context["isActive"]) || array_key_exists("isActive", $context) ? $context["isActive"] : (function () { throw new RuntimeError('Variable "isActive" does not exist.', 61, $this->source); })()), "html", null, true);
                                echo "\">
                                <div class=\"row\">
                                    ";
                                // line 63
                                echo twig_escape_filter($this->env, (isset($context["column"]) || array_key_exists("column", $context) ? $context["column"] : (function () { throw new RuntimeError('Variable "column" does not exist.', 63, $this->source); })()), "html", null, true);
                                echo "
                                </div>
                            </div>
                            ";
                                // line 66
                                $context["count"] = 0;
                                // line 67
                                echo "                        ";
                            } elseif (((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 67, $this->source); })()) == 0)) {
                                // line 68
                                echo "                            <div class=\"carousel-item ";
                                echo twig_escape_filter($this->env, (isset($context["isActive"]) || array_key_exists("isActive", $context) ? $context["isActive"] : (function () { throw new RuntimeError('Variable "isActive" does not exist.', 68, $this->source); })()), "html", null, true);
                                echo "\">
                            <div class=\"row\">
                                ";
                                // line 70
                                echo twig_escape_filter($this->env, (isset($context["column"]) || array_key_exists("column", $context) ? $context["column"] : (function () { throw new RuntimeError('Variable "column" does not exist.', 70, $this->source); })()), "html", null, true);
                                echo "
                            ";
                                // line 71
                                $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 71, $this->source); })()) + 1);
                                // line 72
                                echo "                        ";
                            } elseif ((((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 72, $this->source); })()) == 3) || (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 72)) == twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 72)))) {
                                // line 73
                                echo "                                    ";
                                echo twig_escape_filter($this->env, (isset($context["column"]) || array_key_exists("column", $context) ? $context["column"] : (function () { throw new RuntimeError('Variable "column" does not exist.', 73, $this->source); })()), "html", null, true);
                                echo "
                                </div>
                            </div>
                            ";
                                // line 76
                                $context["count"] = 0;
                                // line 77
                                echo "                        ";
                            } else {
                                // line 78
                                echo "                                ";
                                echo twig_escape_filter($this->env, (isset($context["column"]) || array_key_exists("column", $context) ? $context["column"] : (function () { throw new RuntimeError('Variable "column" does not exist.', 78, $this->source); })()), "html", null, true);
                                echo "
                            ";
                                // line 79
                                $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 79, $this->source); })()) + 1);
                                // line 80
                                echo "                        ";
                            }
                            // line 81
                            echo "
                    ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 83
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 84
            echo "
                </div>

                ";
            // line 87
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 87, $this->source); })()) > 4)) {
                // line 88
                echo "                <div class=\"carousel-control\">
                    <a class=\"carousel-control-prev\" href=\"#";
                // line 89
                echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 89, $this->source); })()), "html", null, true);
                echo "\" role=\"button\" data-slide=\"prev\">
                        <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\">
                            <i class=\"fas fa-chevron-left\"></i>
                        </span>
                        <span class=\"sr-only\">";
                // line 93
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.previous"), "html", null, true);
                echo "</span>
                    </a>
                    <a class=\"carousel-control-next\" href=\"#";
                // line 95
                echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 95, $this->source); })()), "html", null, true);
                echo "\" role=\"button\" data-slide=\"next\">
                        <span class=\"carousel-control-next-icon\" aria-hidden=\"true\">
                            <i class=\"fas fa-chevron-right\"></i>
                        </span>
                        <span class=\"sr-only\">";
                // line 99
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.next"), "html", null, true);
                echo "</span>
                    </a>
                </div>
                ";
            }
            // line 103
            echo "
            </div>
        </div>
    </div>

    ";
            // line 108
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 108, $this->source); })()), "translation", [], "any", false, false, false, 108), "description", [], "any", false, false, false, 108)) {
                // line 109
                echo "        ";
                echo twig_call_macro($macros["dividers"], "macro_default", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 109, $this->source); })()), "translation", [], "any", false, false, false, 109), "description", [], "any", false, false, false, 109), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 109, $this->source); })()), "translation", [], "any", false, false, false, 109), "link", [], "any", false, false, false, 109)], 109, $context, $this->getSourceContext());
                echo "
    ";
            }
            // line 111
            echo "</div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Banner/PositionType/four_columns_slider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  344 => 111,  338 => 109,  336 => 108,  329 => 103,  322 => 99,  315 => 95,  310 => 93,  303 => 89,  300 => 88,  298 => 87,  293 => 84,  283 => 83,  272 => 81,  269 => 80,  267 => 79,  262 => 78,  259 => 77,  257 => 76,  250 => 73,  247 => 72,  245 => 71,  241 => 70,  235 => 68,  232 => 67,  230 => 66,  224 => 63,  218 => 61,  216 => 60,  213 => 59,  207 => 55,  197 => 53,  195 => 52,  192 => 51,  186 => 50,  182 => 49,  177 => 48,  174 => 47,  171 => 46,  169 => 45,  166 => 44,  164 => 43,  160 => 42,  153 => 41,  151 => 40,  148 => 39,  145 => 38,  142 => 37,  130 => 36,  127 => 35,  124 => 34,  113 => 33,  102 => 29,  97 => 28,  95 => 27,  84 => 19,  78 => 16,  72 => 13,  67 => 10,  59 => 8,  54 => 7,  51 => 6,  49 => 5,  47 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusShop/Common/Macro/sectionDivider.html.twig' as dividers %}
{% import '@SyliusShop/Common/Macro/sectionHeaders.html.twig' as headers %}

{% if banners|length > 0 %}
{% set bannerImagesCount = 0 %}

{% for banner in banners if banner.hasChannelCode(options['channel']) %}
    {% set bannerImagesCount = bannerImagesCount + banner.images|length %}
{% endfor %}
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-12 banner-title banner-title--request\">
            {{ headers.default(position.translation.title) }}

            <div class=\"banner-request\">
                <a href=\"{{ path('quote', {locale: app.request.locale}) }}\"
                   class=\"primary-button\"
                   id=\"getQuoteHeader\">
                    {{ 'app.ui.get_a_quote'|trans }}
                </a>
            </div>
        </div>
    </div>

    <div class=\"row\">
        <div class=\"col-12 custom-carousel\">
            {% set idName = position.code %}
            <div id=\"{{ idName }}\" class=\"carousel slide\" data-ride=\"carousel\"
                 {% if bannerImagesCount < 4 %}data-interval=\"false\" {% else %} data-interval=\"100000\" {% endif %}>

                <div class=\"w-100 carousel-inner\" role=\"listbox\">

                {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                    {% set isFirstBanner = loop.first ? true : false %}
                    {% set count = 0 %}
                    {% for image in banner.images if image.contentPosition == 'default' %}
                        {% set isFirstImage = loop.first ? true : false %}
                        {% set isActive = (isFirstBanner and isFirstImage) ? 'active': '' %}

                        {% set column %}
                            <div class=\"col-md-3 four-columns-card {% if count != 0 %}mobile-hidden{% endif %}\">
                                <a class=\"carousel-item-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                                    {% set imagePath = '' %}

                                    {% if image is defined and image.path %}
                                        {% set imagePath = image.path|imagine_filter('omni_sylius_banner') %}
                                    {% endif %}
                                    <img class=\"four-columns-card-img\" src=\"{{ imagePath }}\" alt=\"-\">
                                    <div class=\"caption caption--{{ image.contentPosition }} mobile-hidden\"
                                         {% if image.contentSpace %}style=\"width: {{ image.contentSpace|number_format }}%\"{% endif %}
                                    >
                                        {% if image.translation.link %}
                                            <span>{{ 'app.branner.assoc_cat'|trans }}:</span> <a href=\"{{ image.translation.link }}\">{{ image.translation.content|striptags|replace({'&scaron;': \"š\"}) }}</a>
                                        {% endif %}
                                    </div>
                                </a>
                            </div>
                        {% endset %}

                        {% if count == 0 and banner.images|length == loop.index %}
                            <div class=\"carousel-item {{ isActive }}\">
                                <div class=\"row\">
                                    {{ column }}
                                </div>
                            </div>
                            {% set count = 0 %}
                        {% elseif count == 0 %}
                            <div class=\"carousel-item {{ isActive }}\">
                            <div class=\"row\">
                                {{ column }}
                            {% set count = count + 1 %}
                        {% elseif count == 3 or banner.images|length == loop.index %}
                                    {{ column }}
                                </div>
                            </div>
                            {% set count = 0 %}
                        {% else %}
                                {{ column }}
                            {% set count = count + 1 %}
                        {% endif %}

                    {% endfor %}
                {% endfor %}

                </div>

                {% if bannerImagesCount > 4 %}
                <div class=\"carousel-control\">
                    <a class=\"carousel-control-prev\" href=\"#{{ idName }}\" role=\"button\" data-slide=\"prev\">
                        <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\">
                            <i class=\"fas fa-chevron-left\"></i>
                        </span>
                        <span class=\"sr-only\">{{ 'omni.ui.previous'|trans }}</span>
                    </a>
                    <a class=\"carousel-control-next\" href=\"#{{ idName }}\" role=\"button\" data-slide=\"next\">
                        <span class=\"carousel-control-next-icon\" aria-hidden=\"true\">
                            <i class=\"fas fa-chevron-right\"></i>
                        </span>
                        <span class=\"sr-only\">{{ 'omni.ui.next'|trans }}</span>
                    </a>
                </div>
                {% endif %}

            </div>
        </div>
    </div>

    {% if position.translation.description %}
        {{ dividers.default(position.translation.description, position.translation.link) }}
    {% endif %}
</div>
{% endif %}
", "Banner/PositionType/four_columns_slider.html.twig", "/var/www/html/templates/Banner/PositionType/four_columns_slider.html.twig");
    }
}
