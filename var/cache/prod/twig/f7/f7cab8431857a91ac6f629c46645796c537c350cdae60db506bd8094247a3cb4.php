<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Index:_main.html.twig */
class __TwigTemplate_9b4d6c726476b59db308ccac3730fa8acf21dda9e5b344be1e74c1bfa1782727 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Index:_main.html.twig"));

        // line 1
        $macros["messages"] = $this->macros["messages"] = $this->loadTemplate("@SyliusShop/Common/Macro/messages.html.twig", "SyliusShopBundle:Product/Index:_main.html.twig", 1)->unwrap();
        // line 2
        $macros["pagination"] = $this->macros["pagination"] = $this->loadTemplate("@SyliusShop/Common/Macro/pagination.html.twig", "SyliusShopBundle:Product/Index:_main.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.index.before_search", ["products" => twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 4, $this->source); })()), "data", [], "any", false, false, false, 4)]]);
        echo "

";
        // line 6
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.index.after_search", ["products" => twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 6, $this->source); })()), "data", [], "any", false, false, false, 6)]]);
        echo "

";
        // line 8
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.index.before_list", ["products" => twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 8, $this->source); })()), "data", [], "any", false, false, false, 8)]]);
        echo "

";
        // line 10
        $context["count"] = twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 10, $this->source); })()), "data", [], "any", false, false, false, 10), "currentPageResults", [], "any", false, false, false, 10));
        // line 11
        echo "
";
        // line 12
        if (((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 12, $this->source); })()) > 0)) {
            // line 13
            echo "    <div class=\"row\">
        ";
            // line 14
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 14, $this->source); })()), "data", [], "any", false, false, false, 14));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 15
                echo "            <div class=\"col-6 col-md-6 col-lg-4 mb-3\">
                ";
                // line 16
                $this->loadTemplate("@SyliusShop/Product/_box.html.twig", "SyliusShopBundle:Product/Index:_main.html.twig", 16)->display($context);
                // line 17
                echo "            </div>

            ";
                // line 19
                if (((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 19) == 6) || ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 19) == (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 19, $this->source); })())) && ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 19, $this->source); })()) < 6)))) {
                    // line 20
                    echo "                <div class=\"col-12\" style=\"margin-bottom: 10px;\">
                    ";
                    // line 21
                    echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "product-page-middle");
                    echo "
                </div>
            ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "    </div>

    ";
            // line 27
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.index.before_pagination", ["products" => twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 27, $this->source); })()), "data", [], "any", false, false, false, 27)]]);
            echo "

    <div class=\"d-flex justify-content-end mt-3\">
        ";
            // line 30
            echo twig_call_macro($macros["pagination"], "macro_simple", [twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 30, $this->source); })()), "data", [], "any", false, false, false, 30)], 30, $context, $this->getSourceContext());
            echo "
    </div>
";
        } else {
            // line 33
            echo "    ";
            echo twig_call_macro($macros["messages"], "macro_info", ["sylius.ui.no_results_to_display"], 33, $context, $this->getSourceContext());
            echo "
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Index:_main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 33,  133 => 30,  127 => 27,  123 => 25,  109 => 24,  103 => 21,  100 => 20,  98 => 19,  94 => 17,  92 => 16,  89 => 15,  72 => 14,  69 => 13,  67 => 12,  64 => 11,  62 => 10,  57 => 8,  52 => 6,  47 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusShop/Common/Macro/messages.html.twig' as messages %}
{% import '@SyliusShop/Common/Macro/pagination.html.twig' as pagination %}

{{ sonata_block_render_event('sylius.shop.product.index.before_search', {'products': resources.data}) }}

{{ sonata_block_render_event('sylius.shop.product.index.after_search', {'products': resources.data}) }}

{{ sonata_block_render_event('sylius.shop.product.index.before_list', {'products': resources.data}) }}

{% set count = resources.data.currentPageResults|length %}

{% if count > 0 %}
    <div class=\"row\">
        {% for product in resources.data %}
            <div class=\"col-6 col-md-6 col-lg-4 mb-3\">
                {% include '@SyliusShop/Product/_box.html.twig' %}
            </div>

            {% if loop.index == 6 or (loop.index == count and count < 6) %}
                <div class=\"col-12\" style=\"margin-bottom: 10px;\">
                    {{ omni_sylius_banner_zone('product-page-middle') }}
                </div>
            {% endif %}
        {% endfor %}
    </div>

    {{ sonata_block_render_event('sylius.shop.product.index.before_pagination', {'products': resources.data}) }}

    <div class=\"d-flex justify-content-end mt-3\">
        {{ pagination.simple(resources.data) }}
    </div>
{% else %}
    {{ messages.info('sylius.ui.no_results_to_display') }}
{% endif %}
", "SyliusShopBundle:Product/Index:_main.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Product/Index/_main.html.twig");
    }
}
