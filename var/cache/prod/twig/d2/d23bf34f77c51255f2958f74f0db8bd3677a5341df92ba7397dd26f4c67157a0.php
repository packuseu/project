<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @LexikTranslationBundle/Translation/grid.html.twig */
class __TwigTemplate_a93e8349881b866b25def4754f603eee82c9e31a21eedf10fb726ec0a1fc03a4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'lexik_stylesheets' => [$this, 'block_lexik_stylesheets'],
            'lexik_title' => [$this, 'block_lexik_title'],
            'lexik_content' => [$this, 'block_lexik_content'],
            'lexik_toolbar' => [$this, 'block_lexik_toolbar'],
            'lexik_data_grid' => [$this, 'block_lexik_data_grid'],
            'lexik_javascript_footer' => [$this, 'block_lexik_javascript_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate((isset($context["layout"]) || array_key_exists("layout", $context) ? $context["layout"] : (function () { throw new RuntimeError('Variable "layout" does not exist.', 1, $this->source); })()), "@LexikTranslationBundle/Translation/grid.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@LexikTranslationBundle/Translation/grid.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_lexik_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lexik_stylesheets"));

        // line 6
        echo "    ";
        $this->displayParentBlock("lexik_stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/lexiktranslation/css/translation.css"), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 10
    public function block_lexik_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lexik_title"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.page_title", [], "LexikTranslationBundle"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 12
    public function block_lexik_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lexik_content"));

        // line 13
        echo "    <div class=\"container\">
        ";
        // line 14
        $this->displayBlock('lexik_toolbar', $context, $blocks);
        // line 17
        echo "
        ";
        // line 18
        $this->displayBlock('lexik_data_grid', $context, $blocks);
        // line 21
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 14
    public function block_lexik_toolbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lexik_toolbar"));

        // line 15
        echo "            ";
        $this->loadTemplate("@LexikTranslationBundle/Translation/_gridToolbar.html.twig", "@LexikTranslationBundle/Translation/grid.html.twig", 15)->display($context);
        // line 16
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 18
    public function block_lexik_data_grid($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lexik_data_grid"));

        // line 19
        echo "            ";
        $this->loadTemplate("@LexikTranslationBundle/Translation/_ngGrid.html.twig", "@LexikTranslationBundle/Translation/grid.html.twig", 19)->display($context);
        // line 20
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_lexik_javascript_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lexik_javascript_footer"));

        // line 25
        echo "    ";
        $this->displayParentBlock("lexik_javascript_footer", $context, $blocks);
        echo "
    <script>
        var translationCfg = {
            locales: ";
        // line 28
        echo json_encode((isset($context["locales"]) || array_key_exists("locales", $context) ? $context["locales"] : (function () { throw new RuntimeError('Variable "locales" does not exist.', 28, $this->source); })()));
        echo ",
            inputType: '";
        // line 29
        echo twig_escape_filter($this->env, (isset($context["inputType"]) || array_key_exists("inputType", $context) ? $context["inputType"] : (function () { throw new RuntimeError('Variable "inputType" does not exist.', 29, $this->source); })()), "html", null, true);
        echo "',
            autoCacheClean: ";
        // line 30
        echo (((isset($context["autoCacheClean"]) || array_key_exists("autoCacheClean", $context) ? $context["autoCacheClean"] : (function () { throw new RuntimeError('Variable "autoCacheClean" does not exist.', 30, $this->source); })())) ? ("true") : ("false"));
        echo ",
            profilerTokens: ";
        // line 31
        echo (( !(null === (isset($context["tokens"]) || array_key_exists("tokens", $context) ? $context["tokens"] : (function () { throw new RuntimeError('Variable "tokens" does not exist.', 31, $this->source); })()))) ? (json_encode((isset($context["tokens"]) || array_key_exists("tokens", $context) ? $context["tokens"] : (function () { throw new RuntimeError('Variable "tokens" does not exist.', 31, $this->source); })()))) : ("null"));
        echo ",
            toggleSimilar: '";
        // line 32
        echo twig_escape_filter($this->env, (isset($context["toggleSimilar"]) || array_key_exists("toggleSimilar", $context) ? $context["toggleSimilar"] : (function () { throw new RuntimeError('Variable "toggleSimilar" does not exist.', 32, $this->source); })()), "html", null, true);
        echo "',
            csrfToken: '";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("lexik-translation"), "html", null, true);
        echo "',
            url: {
                list: '";
        // line 35
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_list");
        echo "',
                listByToken: '";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_profiler", ["token" => "-token-"]);
        echo "',
                update: '";
        // line 37
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_update", ["id" => "-id-"]);
        echo "',
                delete: '";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_delete", ["id" => "-id-"]);
        echo "',
                deleteLocale: '";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_delete_locale", ["id" => "-id-", "locale" => "-locale-"]), "html", null, true);
        echo "',
                invalidateCache: '";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_invalidate_cache");
        echo "'
            },
            label: {
                hideCol: '";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.show_hide_columns", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                toggleAllCol: '";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.toggle_all_columns", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                invalidateCache: '";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.invalidate_cache", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                allTranslations: '";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.all_translations", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                profiler: '";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.profiler", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                dataSource: '";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.data_source", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                latestProfiles: '";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.latest_profiles", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                profile: '";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.profile", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                saveRow: '";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.save_row", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                domain: '";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.domain", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                key: '";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.key", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                save: '";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.save", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                updateSuccess: '";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.successfully_updated", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                updateFail: '";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.update_failed", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                deleteSuccess: '";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.successfully_deleted", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                deleteFail: '";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.delete_failed", [], "LexikTranslationBundle"), "html", null, true);
        echo "',
                noTranslations: '";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.no_translations", [], "LexikTranslationBundle"), "html", null, true);
        echo "'
            }
        };
    </script>
    <script src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/lexiktranslation/js/translation.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@LexikTranslationBundle/Translation/grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 63,  278 => 59,  274 => 58,  270 => 57,  266 => 56,  262 => 55,  258 => 54,  254 => 53,  250 => 52,  246 => 51,  242 => 50,  238 => 49,  234 => 48,  230 => 47,  226 => 46,  222 => 45,  218 => 44,  214 => 43,  208 => 40,  204 => 39,  200 => 38,  196 => 37,  192 => 36,  188 => 35,  183 => 33,  179 => 32,  175 => 31,  171 => 30,  167 => 29,  163 => 28,  156 => 25,  149 => 24,  142 => 20,  139 => 19,  132 => 18,  125 => 16,  122 => 15,  115 => 14,  107 => 21,  105 => 18,  102 => 17,  100 => 14,  97 => 13,  90 => 12,  77 => 10,  68 => 7,  63 => 6,  56 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends layout %}

{% trans_default_domain 'LexikTranslationBundle' %}

{% block lexik_stylesheets %}
    {{ parent() }}
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/lexiktranslation/css/translation.css') }}\">
{% endblock %}

{% block lexik_title %}{{ 'translations.page_title'|trans({}, 'LexikTranslationBundle') }}{% endblock %}

{% block lexik_content %}
    <div class=\"container\">
        {% block lexik_toolbar %}
            {% include '@LexikTranslationBundle/Translation/_gridToolbar.html.twig' %}
        {% endblock lexik_toolbar %}

        {% block lexik_data_grid %}
            {% include '@LexikTranslationBundle/Translation/_ngGrid.html.twig' %}
        {% endblock lexik_data_grid %}
    </div>
{% endblock %}

{% block lexik_javascript_footer %}
    {{ parent() }}
    <script>
        var translationCfg = {
            locales: {{ locales | json_encode | raw }},
            inputType: '{{ inputType }}',
            autoCacheClean: {{ autoCacheClean ? 'true' : 'false' }},
            profilerTokens: {{ tokens is not null ? (tokens | json_encode | raw) : 'null' }},
            toggleSimilar: '{{ toggleSimilar }}',
            csrfToken: '{{ csrf_token('lexik-translation') }}',
            url: {
                list: '{{ path('lexik_translation_list') }}',
                listByToken: '{{ path('lexik_translation_profiler', {'token': '-token-'}) }}',
                update: '{{ path('lexik_translation_update', {'id': '-id-'}) }}',
                delete: '{{ path('lexik_translation_delete', {'id': '-id-'}) }}',
                deleteLocale: '{{ path('lexik_translation_delete_locale', {'id': '-id-', 'locale': '-locale-'}) }}',
                invalidateCache: '{{ path('lexik_translation_invalidate_cache') }}'
            },
            label: {
                hideCol: '{{ 'translations.show_hide_columns'|trans }}',
                toggleAllCol: '{{ 'translations.toggle_all_columns'|trans }}',
                invalidateCache: '{{ 'translations.invalidate_cache'|trans }}',
                allTranslations: '{{ 'translations.all_translations'|trans }}',
                profiler: '{{ 'translations.profiler'|trans }}',
                dataSource: '{{ 'translations.data_source'|trans }}',
                latestProfiles: '{{ 'translations.latest_profiles'|trans }}',
                profile: '{{ 'translations.profile'|trans }}',
                saveRow: '{{ 'translations.save_row'|trans }}',
                domain: '{{ 'translations.domain'|trans }}',
                key: '{{ 'translations.key'|trans }}',
                save: '{{ 'translations.save'|trans }}',
                updateSuccess: '{{ 'translations.successfully_updated'|trans }}',
                updateFail: '{{ 'translations.update_failed'|trans }}',
                deleteSuccess: '{{ 'translations.successfully_deleted'|trans }}',
                deleteFail: '{{ 'translations.delete_failed'|trans }}',
                noTranslations: '{{ 'translations.no_translations'|trans }}'
            }
        };
    </script>
    <script src=\"{{ asset('bundles/lexiktranslation/js/translation.js') }}\"></script>
{% endblock %}
", "@LexikTranslationBundle/Translation/grid.html.twig", "/var/www/html/vendor/lexik/translation-bundle/Resources/views/Translation/grid.html.twig");
    }
}
