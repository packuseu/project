<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @LexikTranslationBundle/Translation/new.html.twig */
class __TwigTemplate_301bc7c3ae7769fdcfebffebacacad574d3337dea906b574d77e084df0250775 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'lexik_content' => [$this, 'block_lexik_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate((isset($context["layout"]) || array_key_exists("layout", $context) ? $context["layout"] : (function () { throw new RuntimeError('Variable "layout" does not exist.', 1, $this->source); })()), "@LexikTranslationBundle/Translation/new.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@LexikTranslationBundle/Translation/new.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_lexik_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lexik_content"));

        // line 4
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <h3>";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.add_translation", [], "LexikTranslationBundle"), "html", null, true);
        echo "</h3>
                <hr />
            </div>

            <div class=\"col-md-6\">

                ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 13, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_new"), "method" => "POST", "attr" => ["class" => "form form-vertical"]]);
        echo "

                <div class=\"form-group\">
                    ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), "key", [], "any", false, false, false, 16), 'label');
        echo "
                    ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 17, $this->source); })()), "key", [], "any", false, false, false, 17), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    <span class=\"text-danger\">";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), "key", [], "any", false, false, false, 18), 'errors');
        echo "</span>
                </div>

                <div class=\"form-group\">
                    ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 22, $this->source); })()), "domain", [], "any", false, false, false, 22), 'label');
        echo "
                    ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "domain", [], "any", false, false, false, 23), 'widget', ["attr" => ["class" => "form-control"]]);
        echo "
                    <span class=\"text-danger\">";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), "domain", [], "any", false, false, false, 24), 'errors');
        echo "</span>
                </div>

                <div class=\"form-group\">
                    ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 28, $this->source); })()), "translations", [], "any", false, false, false, 28), 'label');
        echo "
                </div>

                <div class=\"form-group\">
                    ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 32, $this->source); })()), "translations", [], "any", false, false, false, 32));
        foreach ($context['_seq'] as $context["_key"] => $context["translation"]) {
            // line 33
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["translation"], 'label');
            echo "
                        ";
            // line 34
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["translation"], "content", [], "any", false, false, false, 34), 'widget', ["attr" => ["class" => "form-control"]]);
            echo "
                        <span class=\"text-danger\">";
            // line 35
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["translation"], "content", [], "any", false, false, false, 35), 'errors');
            echo "</span>
                        ";
            // line 36
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, $context["translation"], "locale", [], "any", false, false, false, 36), 'widget');
            echo "
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translation'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "                </div>

                <div class=\"form-group\">
                    <a href=\"";
        // line 41
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_grid");
        echo "\" class=\"btn btn-default\">
                        <span class=\"glyphicon glyphicon-arrow-left\"></span>
                        ";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.back_to_list", [], "LexikTranslationBundle"), "html", null, true);
        echo "
                    </a>

                    <div class=\"btn-group pull-right\">
                        ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 47, $this->source); })()), "save", [], "any", false, false, false, 47), 'widget', ["attr" => ["id" => "trans-unit-submit", "name" => "trans-unit-submit", "class" => "btn btn-primary"]]);
        echo "
                        ";
        // line 48
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 48, $this->source); })()), "save_add", [], "any", false, false, false, 48), 'widget', ["attr" => ["id" => "trans-unit-submit-2", "name" => "trans-unit-submit-2", "class" => "btn btn-primary"]]);
        echo "
                    </div>
                </div>

                ";
        // line 52
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 52, $this->source); })()), 'form_end');
        echo "

            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@LexikTranslationBundle/Translation/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 52,  161 => 48,  157 => 47,  150 => 43,  145 => 41,  140 => 38,  132 => 36,  128 => 35,  124 => 34,  119 => 33,  115 => 32,  108 => 28,  101 => 24,  97 => 23,  93 => 22,  86 => 18,  82 => 17,  78 => 16,  72 => 13,  63 => 7,  58 => 4,  51 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends layout %}

{% block lexik_content %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-12\">
                <h3>{{ 'translations.add_translation'|trans({}, 'LexikTranslationBundle') }}</h3>
                <hr />
            </div>

            <div class=\"col-md-6\">

                {{ form_start(form, {'action': path('lexik_translation_new'), 'method': 'POST', 'attr': {'class': 'form form-vertical'}}) }}

                <div class=\"form-group\">
                    {{ form_label(form.key) }}
                    {{ form_widget(form.key, { 'attr': {'class': 'form-control'} }) }}
                    <span class=\"text-danger\">{{ form_errors(form.key) }}</span>
                </div>

                <div class=\"form-group\">
                    {{ form_label(form.domain) }}
                    {{ form_widget(form.domain, { 'attr': {'class': 'form-control'} }) }}
                    <span class=\"text-danger\">{{ form_errors(form.domain) }}</span>
                </div>

                <div class=\"form-group\">
                    {{ form_label(form.translations) }}
                </div>

                <div class=\"form-group\">
                    {% for translation in form.translations %}
                        {{ form_label(translation) }}
                        {{ form_widget(translation.content, { 'attr': {'class': 'form-control'} }) }}
                        <span class=\"text-danger\">{{ form_errors(translation.content) }}</span>
                        {{ form_widget(translation.locale) }}
                    {% endfor %}
                </div>

                <div class=\"form-group\">
                    <a href=\"{{ path('lexik_translation_grid') }}\" class=\"btn btn-default\">
                        <span class=\"glyphicon glyphicon-arrow-left\"></span>
                        {{ 'translations.back_to_list'|trans({}, 'LexikTranslationBundle') }}
                    </a>

                    <div class=\"btn-group pull-right\">
                        {{ form_widget(form.save, { 'attr': {'id': 'trans-unit-submit', 'name': 'trans-unit-submit', 'class': 'btn btn-primary'} }) }}
                        {{ form_widget(form.save_add, { 'attr': {'id': 'trans-unit-submit-2', 'name': 'trans-unit-submit-2', 'class': 'btn btn-primary'} }) }}
                    </div>
                </div>

                {{ form_end(form) }}

            </div>
        </div>
    </div>
{% endblock %}
", "@LexikTranslationBundle/Translation/new.html.twig", "/var/www/html/vendor/lexik/translation-bundle/Resources/views/Translation/new.html.twig");
    }
}
