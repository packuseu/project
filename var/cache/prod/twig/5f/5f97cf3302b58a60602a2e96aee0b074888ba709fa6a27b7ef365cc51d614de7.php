<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Show:_reviews.html.twig */
class __TwigTemplate_28d7a4b52cb854229472cc57aab609311742aaf1082348e12733904dd5395ca1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Show:_reviews.html.twig"));

        // line 1
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Product/Show:_reviews.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 3, $this->source); })()), "variants", [], "any", false, false, false, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["variant"]) {
            // line 4
            echo "    ";
            $context["tier_prices_count"] = twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "tierPrices", [], "any", false, false, false, 4));
            // line 5
            echo "    ";
            $context["price_list"] = "";
            // line 6
            echo "    ";
            $context["full_price_list"] = "";
            // line 7
            echo "
    ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["variant"], "tierPrices", [], "any", false, false, false, 8));
            foreach ($context['_seq'] as $context["_key"] => $context["price"]) {
                // line 9
                echo "        ";
                $context["price_list"] = ((((((isset($context["price_list"]) || array_key_exists("price_list", $context) ? $context["price_list"] : (function () { throw new RuntimeError('Variable "price_list" does not exist.', 9, $this->source); })()) . "<tr><td>") . twig_get_attribute($this->env, $this->source, $context["price"], "qty", [], "any", false, false, false, 9)) . "</td><td>") . call_user_func_array($this->env->getFilter('sylius_format_money')->getCallable(), [twig_get_attribute($this->env, $this->source, $context["price"], "price", [], "any", false, false, false, 9), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["price"], "channel", [], "any", false, false, false, 9), "baseCurrency", [], "any", false, false, false, 9), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 9, $this->source); })()), "request", [], "any", false, false, false, 9), "locale", [], "any", false, false, false, 9)])) . "</td></tr>");
                // line 10
                echo "        ";
                $context["full_price_list"] = ((((((isset($context["full_price_list"]) || array_key_exists("full_price_list", $context) ? $context["full_price_list"] : (function () { throw new RuntimeError('Variable "full_price_list" does not exist.', 10, $this->source); })()) . "<tr><td>") . twig_get_attribute($this->env, $this->source, $context["price"], "qty", [], "any", false, false, false, 10)) . "</td><td>") . $this->extensions['App\Twig\AppExtension']->getPriceWithTax(twig_get_attribute($this->env, $this->source, $context["price"], "price", [], "any", false, false, false, 10), $context["variant"])) . "</td></tr>");
                // line 11
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['price'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "

    ";
            // line 14
            $context["table_with_price"] = (((((("
        <div>
            <table>
                <thead>
                    <tr>
                        <th>" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.info.quantity")) . "</th>
                        <th>") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.info.price")) . "</th>
                    </tr>
                </thead>
                <tbody>") .             // line 23
(isset($context["price_list"]) || array_key_exists("price_list", $context) ? $context["price_list"] : (function () { throw new RuntimeError('Variable "price_list" does not exist.', 23, $this->source); })())) . "
                </tbody>
            </table>
        </div>
    ");
            // line 28
            echo "    ";
            $context["table_with_full_price"] = (((((("
        <div>
            <table>
                <thead>
                    <tr>
                        <th>" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.info.quantity")) . "</th>
                        <th>") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.info.price")) . "</th>
                    </tr>
                </thead>
                <tbody>") .             // line 37
(isset($context["full_price_list"]) || array_key_exists("full_price_list", $context) ? $context["full_price_list"] : (function () { throw new RuntimeError('Variable "full_price_list" does not exist.', 37, $this->source); })())) . "
                </tbody>
            </table>
        </div>
    ");
            // line 42
            echo "
    ";
            // line 43
            if (((isset($context["tier_prices_count"]) || array_key_exists("tier_prices_count", $context) ? $context["tier_prices_count"] : (function () { throw new RuntimeError('Variable "tier_prices_count" does not exist.', 43, $this->source); })()) > 0)) {
                // line 44
                echo "        <a tabindex=\"0\" id=\"price-map-";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "code", [], "any", false, false, false, 44), "html", null, true);
                echo "\" class=\"btn btn-md btn-primary price-map\" data-variant=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "code", [], "any", false, false, false, 44), "html", null, true);
                echo "\" role=\"button\" data-toggle=\"tooltip\" data-html=\"true\" style=\"color: white; display: none\" title=\"";
                echo twig_escape_filter($this->env, (isset($context["table_with_price"]) || array_key_exists("table_with_price", $context) ? $context["table_with_price"] : (function () { throw new RuntimeError('Variable "table_with_price" does not exist.', 44, $this->source); })()), "html", null, true);
                echo "\" data-price_list=\"";
                echo twig_escape_filter($this->env, (isset($context["table_with_price"]) || array_key_exists("table_with_price", $context) ? $context["table_with_price"] : (function () { throw new RuntimeError('Variable "table_with_price" does not exist.', 44, $this->source); })()), "html", null, true);
                echo "\" data-full_price_list=\"";
                echo twig_escape_filter($this->env, (isset($context["table_with_full_price"]) || array_key_exists("table_with_full_price", $context) ? $context["table_with_full_price"] : (function () { throw new RuntimeError('Variable "table_with_full_price" does not exist.', 44, $this->source); })()), "html", null, true);
                echo "\">";
                echo twig_call_macro($macros["icons"], "macro_info", [], 44, $context, $this->getSourceContext());
                echo " ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.info.price_map"), "html", null, true);
                echo "</a>
    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['variant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Show:_reviews.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 44,  117 => 43,  114 => 42,  108 => 37,  97 => 28,  91 => 23,  81 => 14,  77 => 12,  71 => 11,  68 => 10,  65 => 9,  61 => 8,  58 => 7,  55 => 6,  52 => 5,  49 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% for variant in product.variants %}
    {% set tier_prices_count =  variant.tierPrices|length %}
    {% set price_list = '' %}
    {% set full_price_list = '' %}

    {% for price in variant.tierPrices %}
        {% set price_list = price_list ~ '<tr><td>' ~ price.qty ~ '</td><td>' ~ price.price|sylius_format_money(price.channel.baseCurrency, app.request.locale) ~ '</td></tr>' %}
        {% set full_price_list = full_price_list ~ '<tr><td>' ~ price.qty ~ '</td><td>' ~ app_get_price_with_tax(price.price, variant) ~ '</td></tr>' %}
    {% endfor %}


    {% set table_with_price = '
        <div>
            <table>
                <thead>
                    <tr>
                        <th>' ~ 'app.info.quantity'|trans ~ '</th>
                        <th>' ~ 'app.info.price'|trans ~ '</th>
                    </tr>
                </thead>
                <tbody>' ~ price_list ~ '
                </tbody>
            </table>
        </div>
    ' %}
    {% set table_with_full_price = '
        <div>
            <table>
                <thead>
                    <tr>
                        <th>' ~ 'app.info.quantity'|trans ~ '</th>
                        <th>' ~ 'app.info.price'|trans ~ '</th>
                    </tr>
                </thead>
                <tbody>' ~ full_price_list ~ '
                </tbody>
            </table>
        </div>
    ' %}

    {% if tier_prices_count > 0 %}
        <a tabindex=\"0\" id=\"price-map-{{ variant.code }}\" class=\"btn btn-md btn-primary price-map\" data-variant=\"{{ variant.code }}\" role=\"button\" data-toggle=\"tooltip\" data-html=\"true\" style=\"color: white; display: none\" title=\"{{ table_with_price }}\" data-price_list=\"{{ table_with_price }}\" data-full_price_list=\"{{ table_with_full_price }}\">{{ icons.info() }} {{ 'app.info.price_map'|trans }}</a>
    {% endif %}
{% endfor %}
", "SyliusShopBundle:Product/Show:_reviews.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Product/Show/_reviews.html.twig");
    }
}
