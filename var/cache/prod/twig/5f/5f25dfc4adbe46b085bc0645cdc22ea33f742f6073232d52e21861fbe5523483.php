<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Order:thankYou.html.twig */
class __TwigTemplate_02286b2127b5f20ab28dc1b60bf7b1fe1155998b2442cf706cb473e4fa7ae8a0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Order:thankYou.html.twig"));

        // line 3
        $macros["headers"] = $this->macros["headers"] = $this->loadTemplate("@SyliusShop/Common/Macro/headers.html.twig", "SyliusShopBundle:Order:thankYou.html.twig", 3)->unwrap();
        // line 4
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Order:thankYou.html.twig", 4)->unwrap();
        // line 1
        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "SyliusShopBundle:Order:thankYou.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "    <div class=\"\" style=\"text-align: center; margin-bottom: 40px\">
        ";
        // line 8
        $context["lastPayment"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 8, $this->source); })()), "payments", [], "any", false, false, false, 8), "last", [], "method", false, false, false, 8);
        // line 9
        echo "        <span class=\"circled-check\">";
        echo twig_call_macro($macros["icons"], "macro_success", [], 9, $context, $this->getSourceContext());
        echo "</span>
        <h1 style=\"line-height: 44px;\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.thank_you.order_submitted"), "html", null, true);
        echo "</h1>

        ";
        // line 12
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.order.thank_you.after_message", ["order" => (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 12, $this->source); })())]]);
        echo "

        <p class=\"item-subheader\" style=\"margin: auto; margin-bottom: 20px\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.thank_you.order_will_be_sent"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 14, $this->source); })()), "customer", [], "any", false, false, false, 14), "email", [], "any", false, false, false, 14), "html", null, true);
        echo "</p>

        <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_order_show", ["tokenValue" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 16, $this->source); })()), "tokenValue", [], "any", false, false, false, 16)]), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.view_your_order_or_change_payment_method"), "html", null, true);
        echo "</a>
        ";
        // line 17
        if ((null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 17, $this->source); })()), "customer", [], "any", false, false, false, 17), "user", [], "any", false, false, false, 17))) {
            // line 18
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_register_after_checkout", ["tokenValue" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 18, $this->source); })()), "tokenValue", [], "any", false, false, false, 18)]), "html", null, true);
            echo "\" class=\"btn btn-secondary\">
                ";
            // line 19
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.create_an_account"), "html", null, true);
            echo "
            </a>
        ";
        }
        // line 22
        echo "    </div>

    ";
        // line 24
        $this->loadTemplate("@SyliusShop/Common/Order/_summary.html.twig", "SyliusShopBundle:Order:thankYou.html.twig", 24)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Order:thankYou.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 24,  110 => 22,  104 => 19,  99 => 18,  97 => 17,  91 => 16,  84 => 14,  79 => 12,  74 => 10,  69 => 9,  67 => 8,  64 => 7,  57 => 6,  49 => 1,  47 => 4,  45 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% import '@SyliusShop/Common/Macro/headers.html.twig' as headers %}
{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% block content %}
    <div class=\"\" style=\"text-align: center; margin-bottom: 40px\">
        {% set lastPayment = order.payments.last() %}
        <span class=\"circled-check\">{{ icons.success }}</span>
        <h1 style=\"line-height: 44px;\">{{ 'app.thank_you.order_submitted'|trans }}</h1>

        {{ sonata_block_render_event('sylius.shop.order.thank_you.after_message', {'order': order}) }}

        <p class=\"item-subheader\" style=\"margin: auto; margin-bottom: 20px\">{{ 'app.thank_you.order_will_be_sent'|trans }} {{ order.customer.email }}</p>

        <a href=\"{{ path('sylius_shop_order_show', {'tokenValue': order.tokenValue}) }}\" class=\"btn btn-primary\">{{ 'sylius.ui.view_your_order_or_change_payment_method'|trans }}</a>
        {% if order.customer.user is null %}
            <a href=\"{{ path('sylius_shop_register_after_checkout', {'tokenValue': order.tokenValue}) }}\" class=\"btn btn-secondary\">
                {{ 'sylius.ui.create_an_account'|trans }}
            </a>
        {% endif %}
    </div>

    {% include '@SyliusShop/Common/Order/_summary.html.twig' %}
{% endblock %}
", "SyliusShopBundle:Order:thankYou.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Order/thankYou.html.twig");
    }
}
