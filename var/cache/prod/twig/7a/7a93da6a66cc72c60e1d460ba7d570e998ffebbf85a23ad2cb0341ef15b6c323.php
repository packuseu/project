<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SyliusAdmin/PromotionCoupon/Index/_promotionHeader.html.twig */
class __TwigTemplate_22dedba23a4e52cccea19dbb9eaed64d9a4c410a5aee3c771ce1fab3cc00d1ba extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@SyliusAdmin/PromotionCoupon/Index/_promotionHeader.html.twig"));

        // line 1
        $macros["headers"] = $this->macros["headers"] = $this->loadTemplate("@SyliusUi/Macro/headers.html.twig", "@SyliusAdmin/PromotionCoupon/Index/_promotionHeader.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"column\">
    ";
        // line 4
        echo twig_call_macro($macros["headers"], "macro_default", [twig_get_attribute($this->env, $this->source, (isset($context["promotion"]) || array_key_exists("promotion", $context) ? $context["promotion"] : (function () { throw new RuntimeError('Variable "promotion" does not exist.', 4, $this->source); })()), "name", [], "any", false, false, false, 4), ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 4), "icon", [], "any", true, true, false, 4)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 4), "icon", [], "any", false, false, false, 4), "tags")) : ("tags")), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 4), "subheader", [], "any", true, true, false, 4)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 4), "subheader", [], "any", false, false, false, 4), "sylius.ui.manage_coupons")) : ("sylius.ui.manage_coupons")))], 4, $context, $this->getSourceContext());
        echo "

    ";
        // line 6
        $this->loadTemplate(((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 6), "templates", [], "any", false, true, false, 6), "breadcrumb", [], "any", true, true, false, 6)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 6), "templates", [], "any", false, true, false, 6), "breadcrumb", [], "any", false, false, false, 6), "@SyliusAdmin/PromotionCoupon/Index/_breadcrumb.html.twig")) : ("@SyliusAdmin/PromotionCoupon/Index/_breadcrumb.html.twig")), "@SyliusAdmin/PromotionCoupon/Index/_promotionHeader.html.twig", 6)->display($context);
        // line 7
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@SyliusAdmin/PromotionCoupon/Index/_promotionHeader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 7,  51 => 6,  46 => 4,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusUi/Macro/headers.html.twig' as headers %}

<div class=\"column\">
    {{ headers.default(promotion.name, configuration.vars.icon|default('tags'), configuration.vars.subheader|default('sylius.ui.manage_coupons')|trans) }}

    {% include configuration.vars.templates.breadcrumb|default('@SyliusAdmin/PromotionCoupon/Index/_breadcrumb.html.twig') %}
</div>
", "@SyliusAdmin/PromotionCoupon/Index/_promotionHeader.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/PromotionCoupon/Index/_promotionHeader.html.twig");
    }
}
