<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Order:_summary.html.twig */
class __TwigTemplate_e04107304a5ab74135411935a4d6d6f2c59b2c29e0648a456890c90abbb79cd7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Order:_summary.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Order:_summary.html.twig", 1)->unwrap();
        // line 2
        $macros["headers"] = $this->macros["headers"] = $this->loadTemplate("@SyliusShop/Common/Macro/headers.html.twig", "SyliusShopBundle:Order:_summary.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        echo twig_call_macro($macros["headers"], "macro_default", [(($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.summary_of_your_order") . " #") . twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 4, $this->source); })()), "number", [], "any", false, false, false, 4))], 4, $context, $this->getSourceContext());
        echo "

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"d-flex\">
            <div>
                ";
        // line 10
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 10, $this->source); })()), "checkoutCompletedAt", [], "any", false, false, false, 10)), "html", null, true);
        echo "
            </div>
            <div class=\"pl-2\">
                ";
        // line 13
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 13, $this->source); })()), "total", [], "any", false, false, false, 13)], 13, $context, $this->getSourceContext());
        echo "
            </div>
            <div class=\"pl-2\">
                ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 16, $this->source); })()), "totalQuantity", [], "any", false, false, false, 16), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.items")), "html", null, true);
        echo "
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Order:_summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 16,  62 => 13,  56 => 10,  47 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}
{% import '@SyliusShop/Common/Macro/headers.html.twig' as headers %}

{{ headers.default('sylius.ui.summary_of_your_order'|trans ~ ' #' ~ order.number) }}

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"d-flex\">
            <div>
                {{ order.checkoutCompletedAt|date }}
            </div>
            <div class=\"pl-2\">
                {{ money.convertAndFormat(order.total) }}
            </div>
            <div class=\"pl-2\">
                {{ order.totalQuantity }} {{ 'sylius.ui.items'|trans|lower }}
            </div>
        </div>
    </div>
</div>
", "SyliusShopBundle:Order:_summary.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Order/_summary.html.twig");
    }
}
