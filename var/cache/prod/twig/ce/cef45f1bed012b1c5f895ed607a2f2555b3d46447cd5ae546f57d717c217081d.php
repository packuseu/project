<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @BitBagSyliusShippingExportPlugin/ShippingGateway/Gateways/shippingGateways.html.twig */
class __TwigTemplate_42962dfa6d2a8b6ae452369d556dfe5ddf8c4d146074ba4251ec325776a9cc94 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@BitBagSyliusShippingExportPlugin/ShippingGateway/Gateways/shippingGateways.html.twig"));

        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["shippingGateways"]) || array_key_exists("shippingGateways", $context) ? $context["shippingGateways"] : (function () { throw new RuntimeError('Variable "shippingGateways" does not exist.', 1, $this->source); })()));
        foreach ($context['_seq'] as $context["code"] => $context["label"]) {
            // line 2
            echo "    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("bitbag_admin_shipping_gateway_create", ["code" => $context["code"]]), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, $context["code"], "html", null, true);
            echo "\" class=\"item\">
        ";
            // line 3
            echo twig_escape_filter($this->env, $context["label"], "html", null, true);
            echo "
    </a>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['code'], $context['label'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@BitBagSyliusShippingExportPlugin/ShippingGateway/Gateways/shippingGateways.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 3,  44 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% for code, label in shippingGateways %}
    <a href=\"{{ path('bitbag_admin_shipping_gateway_create', { 'code': code }) }}\" id=\"{{ code }}\" class=\"item\">
        {{ label }}
    </a>
{% endfor %}
", "@BitBagSyliusShippingExportPlugin/ShippingGateway/Gateways/shippingGateways.html.twig", "/var/www/html/vendor/bitbag/shipping-export-plugin/src/Resources/views/ShippingGateway/Gateways/shippingGateways.html.twig");
    }
}
