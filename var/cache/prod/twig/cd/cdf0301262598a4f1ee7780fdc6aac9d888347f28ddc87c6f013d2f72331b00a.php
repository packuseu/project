<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle::_flashes.html.twig */
class __TwigTemplate_0a7c8b6b8c53e67d43d0fdc6555d37c810760ab7044376f0f1e3129d8101042c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle::_flashes.html.twig"));

        // line 1
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle::_flashes.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 3, $this->source); })()), "session", [], "any", false, false, false, 3)) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 3, $this->source); })()), "session", [], "any", false, false, false, 3), "started", [], "any", false, false, false, 3))) {
            // line 4
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable([0 => "success", 1 => "error", 2 => "info", 3 => "warning"]);
            foreach ($context['_seq'] as $context["_key"] => $context["type"]) {
                // line 5
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "session", [], "any", false, false, false, 5), "flashbag", [], "any", false, false, false, 5), "get", [0 => $context["type"]], "method", false, false, false, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["flash"]) {
                    // line 6
                    echo "            ";
                    if (("error" == $context["type"])) {
                        // line 7
                        echo "                ";
                        $context["result"] = "danger";
                        // line 8
                        echo "                ";
                        $context["icon"] = twig_call_macro($macros["icons"], "macro_danger", [], 8, $context, $this->getSourceContext());
                        // line 9
                        echo "            ";
                    }
                    // line 10
                    echo "            ";
                    if (("info" == $context["type"])) {
                        // line 11
                        echo "                ";
                        $context["result"] = "info";
                        // line 12
                        echo "                ";
                        $context["icon"] = twig_call_macro($macros["icons"], "macro_info", [], 12, $context, $this->getSourceContext());
                        // line 13
                        echo "            ";
                    }
                    // line 14
                    echo "            <div class=\"alert alert-";
                    echo twig_escape_filter($this->env, ((array_key_exists("result", $context)) ? (_twig_default_filter((isset($context["result"]) || array_key_exists("result", $context) ? $context["result"] : (function () { throw new RuntimeError('Variable "result" does not exist.', 14, $this->source); })()), "success")) : ("success")), "html", null, true);
                    echo " alert-dismissible fade show\">
                <div class=\"d-flex align-items-center\">
                    <span class=\"pr-3 fa-2x\">
                        ";
                    // line 17
                    echo twig_escape_filter($this->env, ((array_key_exists("icon", $context)) ? (_twig_default_filter((isset($context["icon"]) || array_key_exists("icon", $context) ? $context["icon"] : (function () { throw new RuntimeError('Variable "icon" does not exist.', 17, $this->source); })()), twig_call_macro($macros["icons"], "macro_success", [], 17, $context, $this->getSourceContext()))) : (twig_call_macro($macros["icons"], "macro_success", [], 17, $context, $this->getSourceContext()))), "html", null, true);
                    echo "
                    </span>
                    <strong>
                        ";
                    // line 20
                    $context["header"] = ("sylius.ui." . $context["type"]);
                    // line 21
                    echo "                        ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 21, $this->source); })())), "html", null, true);
                    echo "&nbsp;
                    </strong>
                    ";
                    // line 23
                    if (twig_test_iterable($context["flash"])) {
                        // line 24
                        echo "                        ";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["flash"], "message", [], "any", false, false, false, 24), twig_get_attribute($this->env, $this->source, $context["flash"], "parameters", [], "any", false, false, false, 24), "flashes"), "html", null, true);
                        echo "
                    ";
                    } else {
                        // line 26
                        echo "                        ";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["flash"], [], "flashes"), "html", null, true);
                        echo "
                    ";
                    }
                    // line 28
                    echo "                </div>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">
                    <span>&times;</span>
                </button>
            </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flash'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 34
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['type'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle::_flashes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 34,  116 => 28,  110 => 26,  104 => 24,  102 => 23,  96 => 21,  94 => 20,  88 => 17,  81 => 14,  78 => 13,  75 => 12,  72 => 11,  69 => 10,  66 => 9,  63 => 8,  60 => 7,  57 => 6,  52 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% if app.session is not null and app.session.started %}
    {% for type in ['success', 'error', 'info', 'warning'] %}
        {% for flash in app.session.flashbag.get(type) %}
            {% if 'error' == type %}
                {% set result = 'danger' %}
                {% set icon = icons.danger() %}
            {% endif %}
            {% if 'info' == type %}
                {% set result = 'info' %}
                {% set icon = icons.info() %}
            {% endif %}
            <div class=\"alert alert-{{ result|default('success') }} alert-dismissible fade show\">
                <div class=\"d-flex align-items-center\">
                    <span class=\"pr-3 fa-2x\">
                        {{ icon|default(icons.success()) }}
                    </span>
                    <strong>
                        {% set header = 'sylius.ui.'~type %}
                        {{ header|trans }}&nbsp;
                    </strong>
                    {% if flash is iterable %}
                        {{ flash.message|trans(flash.parameters, 'flashes') }}
                    {% else %}
                        {{ flash|trans({}, 'flashes') }}
                    {% endif %}
                </div>
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">
                    <span>&times;</span>
                </button>
            </div>
        {% endfor %}
    {% endfor %}
{% endif %}
", "SyliusShopBundle::_flashes.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/_flashes.html.twig");
    }
}
