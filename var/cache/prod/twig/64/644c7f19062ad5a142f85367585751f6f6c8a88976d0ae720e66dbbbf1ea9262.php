<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusBannerPlugin:PositionTypes:_content_line_right.html.twig */
class __TwigTemplate_ab370355e313acf5e0dd5b3833d8c275f9162fcf97bff94c41df5a67a3c1be63 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusBannerPlugin:PositionTypes:_content_line_right.html.twig"));

        // line 1
        $context["useContainer"] = (twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "full_screen", [], "any", true, true, false, 1) && (twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 1, $this->source); })()), "full_screen", [], "any", false, false, false, 1) == false));
        // line 2
        echo "
";
        // line 3
        if ((twig_length_filter($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 3, $this->source); })())) > 0)) {
            // line 4
            echo "    ";
            $context["banner"] = twig_first($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 4, $this->source); })()));
            // line 5
            echo "
    ";
            // line 6
            if ((twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 6, $this->source); })()), "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 6, $this->source); })()), "channel", [], "any", false, false, false, 6)], "method", false, false, false, 6) && twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 6, $this->source); })()), "images", [], "any", false, false, false, 6)))) {
                // line 7
                echo "        ";
                $context["image"] = twig_first($this->env, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 7, $this->source); })()), "images", [], "any", false, false, false, 7));
                // line 8
                echo "
        ";
                // line 9
                if ((twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "use_container", [], "any", true, true, false, 9) && twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 9, $this->source); })()), "use_container", [], "any", false, false, false, 9))) {
                    // line 10
                    echo "            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xl-10 offset-xl-2\">
        ";
                }
                // line 14
                echo "
        <div class=\"banner-line ";
                // line 15
                if ((twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "use_container", [], "any", true, true, false, 15) && twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 15, $this->source); })()), "use_container", [], "any", false, false, false, 15))) {
                    echo "negative-margin";
                }
                echo "\" style=\"background-color: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 15, $this->source); })()), "contentBackground", [], "any", false, false, false, 15), "html", null, true);
                echo ";\">

            ";
                // line 17
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 17, $this->source); })()), "translation", [], "any", false, false, false, 17), "link", [], "any", false, false, false, 17))) {
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 17, $this->source); })()), "translation", [], "any", false, false, false, 17), "link", [], "any", false, false, false, 17), "html", null, true);
                    echo "\">";
                }
                // line 18
                echo "                ";
                echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 18, $this->source); })()), "translation", [], "any", false, false, false, 18), "content", [], "any", false, false, false, 18);
                echo "
            ";
                // line 19
                if ( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 19, $this->source); })()), "translation", [], "any", false, false, false, 19), "link", [], "any", false, false, false, 19))) {
                    echo "</a>";
                }
                // line 20
                echo "        </div>

        ";
                // line 22
                if ((twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "use_container", [], "any", true, true, false, 22) && twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 22, $this->source); })()), "use_container", [], "any", false, false, false, 22))) {
                    // line 23
                    echo "                    </div>
                </div>
            </div>
        ";
                }
                // line 27
                echo "    ";
            }
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusBannerPlugin:PositionTypes:_content_line_right.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 27,  102 => 23,  100 => 22,  96 => 20,  92 => 19,  87 => 18,  81 => 17,  72 => 15,  69 => 14,  63 => 10,  61 => 9,  58 => 8,  55 => 7,  53 => 6,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set useContainer = options.full_screen is defined and options.full_screen == false %}

{% if banners|length > 0 %}
    {% set banner = banners|first %}

    {% if banner.hasChannelCode(options.channel) and banner.images|length %}
        {% set image = banner.images|first %}

        {% if options.use_container is defined and options.use_container %}
            <div class=\"container\">
                <div class=\"row\">
                    <div class=\"col-xl-10 offset-xl-2\">
        {% endif %}

        <div class=\"banner-line {% if options.use_container is defined and options.use_container %}negative-margin{% endif %}\" style=\"background-color: {{ image.contentBackground }};\">

            {% if image.translation.link is not null %}<a href=\"{{ image.translation.link }}\">{% endif %}
                {{ image.translation.content|raw }}
            {% if image.translation.link is not null %}</a>{% endif %}
        </div>

        {% if options.use_container is defined and  options.use_container %}
                    </div>
                </div>
            </div>
        {% endif %}
    {% endif %}
{% endif %}
", "OmniSyliusBannerPlugin:PositionTypes:_content_line_right.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/PositionTypes/_content_line_right.html.twig");
    }
}
