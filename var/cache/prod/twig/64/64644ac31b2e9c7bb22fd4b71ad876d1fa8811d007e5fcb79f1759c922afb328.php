<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Show:_tabs.html.twig */
class __TwigTemplate_125385029f563651a0e5dde910f8751203c3c3231dde03cfc2a22c32816140d3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Show:_tabs.html.twig"));

        // line 1
        echo "<div class=\"card\">
    <div class=\"card-header\">
        <nav class=\"nav nav-tabs card-header-tabs nav-fill\">
            ";
        // line 4
        $this->loadTemplate("@SyliusShop/Product/Show/Tabs/_menu.html.twig", "SyliusShopBundle:Product/Show:_tabs.html.twig", 4)->display($context);
        // line 5
        echo "        </nav>
    </div>
    <div class=\"card-body\">
        <div class=\"tab-content\">
            ";
        // line 9
        $this->loadTemplate("@SyliusShop/Product/Show/Tabs/_content.html.twig", "SyliusShopBundle:Product/Show:_tabs.html.twig", 9)->display($context);
        // line 10
        echo "        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Show:_tabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 10,  53 => 9,  47 => 5,  45 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card\">
    <div class=\"card-header\">
        <nav class=\"nav nav-tabs card-header-tabs nav-fill\">
            {% include '@SyliusShop/Product/Show/Tabs/_menu.html.twig' %}
        </nav>
    </div>
    <div class=\"card-body\">
        <div class=\"tab-content\">
            {% include '@SyliusShop/Product/Show/Tabs/_content.html.twig' %}
        </div>
    </div>
</div>
", "SyliusShopBundle:Product/Show:_tabs.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Product/Show/_tabs.html.twig");
    }
}
