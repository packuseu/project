<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product:_info.html.twig */
class __TwigTemplate_15a7b4348d5bd5f1f4cafc4de7477b00981318ebe66184ffed8a2facc4ff1eca extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product:_info.html.twig"));

        // line 1
        $context["product"] = twig_get_attribute($this->env, $this->source, (isset($context["variant"]) || array_key_exists("variant", $context) ? $context["variant"] : (function () { throw new RuntimeError('Variable "variant" does not exist.', 1, $this->source); })()), "product", [], "any", false, false, false, 1);
        // line 2
        echo "
<div class=\"media\">
    <div class=\"mr-3\">
        ";
        // line 5
        $this->loadTemplate("@SyliusShop/Product/_mainImage.html.twig", "SyliusShopBundle:Product:_info.html.twig", 5)->display(twig_array_merge($context, ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 5, $this->source); })()), "filter" => "sylius_shop_product_tiny_thumbnail"]));
        // line 6
        echo "    </div>
    <div class=\"media-body\">
        <h6 class=\"mt-0\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 8, $this->source); })()), "productName", [], "any", false, false, false, 8), "html", null, true);
        echo "</h6>
        <small>";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["variant"]) || array_key_exists("variant", $context) ? $context["variant"] : (function () { throw new RuntimeError('Variable "variant" does not exist.', 9, $this->source); })()), "code", [], "any", false, false, false, 9), "html", null, true);
        echo "</small>

        ";
        // line 11
        if (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 11, $this->source); })()), "hasOptions", [], "method", false, false, false, 11)) {
            // line 12
            echo "            <div>
                ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["variant"]) || array_key_exists("variant", $context) ? $context["variant"] : (function () { throw new RuntimeError('Variable "variant" does not exist.', 13, $this->source); })()), "optionValues", [], "any", false, false, false, 13));
            foreach ($context['_seq'] as $context["_key"] => $context["optionValue"]) {
                // line 14
                echo "                    <div data-sylius-option-name=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["optionValue"], "name", [], "any", false, false, false, 14), "html", null, true);
                echo "\">
                        <small>";
                // line 15
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["optionValue"], "value", [], "any", false, false, false, 15), "html", null, true);
                echo "</small>
                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['optionValue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "            </div>
        ";
        } elseif ( !(null === twig_get_attribute($this->env, $this->source,         // line 19
(isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 19, $this->source); })()), "variantName", [], "any", false, false, false, 19))) {
            // line 20
            echo "            <div>
                <small>";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["item"]) || array_key_exists("item", $context) ? $context["item"] : (function () { throw new RuntimeError('Variable "item" does not exist.', 21, $this->source); })()), "variantName", [], "any", false, false, false, 21), "html", null, true);
            echo "</small>
            </div>
        ";
        }
        // line 24
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product:_info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 24,  93 => 21,  90 => 20,  88 => 19,  85 => 18,  76 => 15,  71 => 14,  67 => 13,  64 => 12,  62 => 11,  57 => 9,  53 => 8,  49 => 6,  47 => 5,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set product = variant.product %}

<div class=\"media\">
    <div class=\"mr-3\">
        {% include '@SyliusShop/Product/_mainImage.html.twig' with {'product': product, 'filter': 'sylius_shop_product_tiny_thumbnail'} %}
    </div>
    <div class=\"media-body\">
        <h6 class=\"mt-0\">{{ item.productName }}</h6>
        <small>{{ variant.code }}</small>

        {% if product.hasOptions() %}
            <div>
                {% for optionValue in variant.optionValues %}
                    <div data-sylius-option-name=\"{{ optionValue.name }}\">
                        <small>{{ optionValue.value }}</small>
                    </div>
                {% endfor %}
            </div>
        {% elseif item.variantName is not null %}
            <div>
                <small>{{ item.variantName }}</small>
            </div>
        {% endif %}
    </div>
</div>
", "SyliusShopBundle:Product:_info.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Product/_info.html.twig");
    }
}
