<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Checkout/SelectPayment:_choice.html.twig */
class __TwigTemplate_31af916b0ec7244ed6aea0979c06700fea61e077b819f31c93c2cdfa6620e5a8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Checkout/SelectPayment:_choice.html.twig"));

        // line 1
        echo "<div class=\"list-group mb-2\">
    <div class=\"list-group-item flex-column align-items-start\">
        <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">
                ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), 'widget');
        echo "
                ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 6, $this->source); })()), 'label');
        echo "
            </h5>
        </div>
        ";
        // line 9
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["method"]) || array_key_exists("method", $context) ? $context["method"] : (function () { throw new RuntimeError('Variable "method" does not exist.', 9, $this->source); })()), "description", [], "any", false, false, false, 9))) {
            // line 10
            echo "            <p class=\"mb-1\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["method"]) || array_key_exists("method", $context) ? $context["method"] : (function () { throw new RuntimeError('Variable "method" does not exist.', 10, $this->source); })()), "description", [], "any", false, false, false, 10), "html", null, true);
            echo "</p>
        ";
        }
        // line 12
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Checkout/SelectPayment:_choice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 12,  58 => 10,  56 => 9,  50 => 6,  46 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"list-group mb-2\">
    <div class=\"list-group-item flex-column align-items-start\">
        <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">
                {{ form_widget(form) }}
                {{ form_label(form) }}
            </h5>
        </div>
        {% if method.description is not null %}
            <p class=\"mb-1\">{{ method.description }}</p>
        {% endif %}
    </div>
</div>
", "SyliusShopBundle:Checkout/SelectPayment:_choice.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Checkout/SelectPayment/_choice.html.twig");
    }
}
