<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusSearchPlugin:SearchResultSnippets:_product.html.twig */
class __TwigTemplate_362166def029df727143749172b1102b40f835b10b4fcd97235e14c519a1bf32 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusSearchPlugin:SearchResultSnippets:_product.html.twig"));

        // line 1
        echo "<div class=\"column\">
    <div class=\"ui fluid card\">
        <a href=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath($this->extensions['Omni\Sylius\SearchPlugin\Twig\IndexExtension']->getRoute((isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 3, $this->source); })())), ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 3, $this->source); })()), "translation", [], "any", false, false, false, 3), "slug", [], "any", false, false, false, 3)]), "html", null, true);
        echo "\" class=\"blurring dimmable image\">
            <div class=\"ui dimmer transition hidden\">
                <div class=\"content\">
                    <div class=\"center\">
                        <div class=\"ui inverted button\">View more</div>
                    </div>
                </div>
            </div>
            ";
        // line 11
        $context["title"] = "";
        // line 12
        echo "
            ";
        // line 13
        if ((twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "name", [], "any", true, true, false, 13) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 13, $this->source); })()), "name", [], "any", false, false, false, 13)))) {
            // line 14
            echo "                ";
            $context["title"] = twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 14, $this->source); })()), "name", [], "any", false, false, false, 14);
            // line 15
            echo "            ";
        } elseif ((twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "title", [], "any", true, true, false, 15) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 15, $this->source); })()), "title", [], "any", false, false, false, 15)))) {
            // line 16
            echo "                ";
            $context["title"] = twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 16, $this->source); })()), "title", [], "any", false, false, false, 16);
            // line 17
            echo "            ";
        } elseif ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "translation", [], "any", false, true, false, 17), "title", [], "any", true, true, false, 17) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 17, $this->source); })()), "translation", [], "any", false, false, false, 17), "title", [], "any", false, false, false, 17)))) {
            // line 18
            echo "                ";
            $context["title"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 18, $this->source); })()), "translation", [], "any", false, false, false, 18), "title", [], "any", false, false, false, 18);
            // line 19
            echo "            ";
        }
        // line 20
        echo "
            <img src=\"";
        // line 21
        ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 21, $this->source); })()), "images", [], "any", false, false, false, 21), "first", [], "any", false, false, false, 21)) ? (print (twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 21, $this->source); })()), "images", [], "any", false, false, false, 21), "first", [], "any", false, false, false, 21), "path", [], "any", false, false, false, 21), "sylius_shop_product_thumbnail"), "html", null, true))) : (print ("https://placehold.it/262x255")));
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 21, $this->source); })()), "html", null, true);
        echo "\" class=\"ui bordered image dimmable\">
        </a>
        <div class=\"content\">
            <a href=\"#\" class=\"header sylius-product-name\">";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["title"]) || array_key_exists("title", $context) ? $context["title"] : (function () { throw new RuntimeError('Variable "title" does not exist.', 24, $this->source); })()), "html", null, true);
        echo "</a>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusSearchPlugin:SearchResultSnippets:_product.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 24,  83 => 21,  80 => 20,  77 => 19,  74 => 18,  71 => 17,  68 => 16,  65 => 15,  62 => 14,  60 => 13,  57 => 12,  55 => 11,  44 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"column\">
    <div class=\"ui fluid card\">
        <a href=\"{{ path(omni_sylius_search_get_route(resource), {slug: resource.translation.slug}) }}\" class=\"blurring dimmable image\">
            <div class=\"ui dimmer transition hidden\">
                <div class=\"content\">
                    <div class=\"center\">
                        <div class=\"ui inverted button\">View more</div>
                    </div>
                </div>
            </div>
            {% set title = '' %}

            {% if resource.name is defined and resource.name is not null %}
                {% set title = resource.name %}
            {% elseif resource.title is defined and resource.title is not null %}
                {% set title = resource.title %}
            {% elseif resource.translation.title is defined and resource.translation.title is not null %}
                {% set title = resource.translation.title %}
            {% endif %}

            <img src=\"{{ resource.images.first ? resource.images.first.path|imagine_filter('sylius_shop_product_thumbnail') : 'https://placehold.it/262x255' }}\" alt=\"{{ title }}\" class=\"ui bordered image dimmable\">
        </a>
        <div class=\"content\">
            <a href=\"#\" class=\"header sylius-product-name\">{{ title }}</a>
        </div>
    </div>
</div>
", "OmniSyliusSearchPlugin:SearchResultSnippets:_product.html.twig", "/var/www/html/vendor/omni/sylius-search-plugin/src/Resources/views/SearchResultSnippets/_product.html.twig");
    }
}
