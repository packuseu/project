<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Email/quote.html.twig */
class __TwigTemplate_fcb31fd14870d46695600941e51e04659b7687d11a93e88ac241e6349db30c8f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "Email/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Email/quote.html.twig"));

        $this->parent = $this->loadTemplate("Email/layout.html.twig", "Email/quote.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.quote.new_quote"), "html", null, true);
        echo "</h2>

    <p>
        ";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.quote.new_quote_message"), "html", null, true);
        echo "
    </p>

    <table>
        <tr>
            <td>";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.form.email"), "html", null, true);
        echo "</td>
            <td>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 13, $this->source); })()), "email", [], "any", false, false, false, 13), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td>";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.form.name"), "html", null, true);
        echo "</td>
            <td>";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 17, $this->source); })()), "name", [], "any", false, false, false, 17), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td>";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.form.phone"), "html", null, true);
        echo "</td>
            <td>";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 21, $this->source); })()), "phone", [], "any", false, false, false, 21), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td>";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.form.product_code"), "html", null, true);
        echo "</td>
            <td>";
        // line 25
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 25, $this->source); })()), "product", [], "any", false, false, false, 25), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <td>";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.quote.deadline"), "html", null, true);
        echo "</td>
            <td>";
        // line 29
        if (twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 29, $this->source); })()), "deadline", [], "any", false, false, false, 29)) {
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 29, $this->source); })()), "deadline", [], "any", false, false, false, 29), "format", [0 => "Y-m-d"], "method", false, false, false, 29), "html", null, true);
            echo " ";
        } else {
            echo " -- ";
        }
        echo "</td>
        </tr>
        <tr>
            <td>";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.form.quantity"), "html", null, true);
        echo "</td>
            <td>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 33, $this->source); })()), "quantity", [], "any", false, false, false, 33), "html", null, true);
        echo "</td>
        </tr>
    </table>

    <h2 style=\"margin-top: 15px\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.quote.note"), "html", null, true);
        echo "</h2>
    <p>
        ";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["quote"]) || array_key_exists("quote", $context) ? $context["quote"] : (function () { throw new RuntimeError('Variable "quote" does not exist.', 39, $this->source); })()), "note", [], "any", false, false, false, 39), "html", null, true);
        echo "
    </p>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Email/quote.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 39,  141 => 37,  134 => 33,  130 => 32,  118 => 29,  114 => 28,  108 => 25,  104 => 24,  98 => 21,  94 => 20,  88 => 17,  84 => 16,  78 => 13,  74 => 12,  66 => 7,  59 => 4,  52 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'Email/layout.html.twig' %}

{% block content %}
    <h2>{{ 'app.quote.new_quote'|trans }}</h2>

    <p>
        {{ 'app.quote.new_quote_message'|trans }}
    </p>

    <table>
        <tr>
            <td>{{ 'app.form.email'|trans }}</td>
            <td>{{ quote.email }}</td>
        </tr>
        <tr>
            <td>{{ 'app.form.name'|trans }}</td>
            <td>{{ quote.name }}</td>
        </tr>
        <tr>
            <td>{{ 'app.form.phone'|trans }}</td>
            <td>{{ quote.phone }}</td>
        </tr>
        <tr>
            <td>{{ 'app.form.product_code'|trans }}</td>
            <td>{{ quote.product }}</td>
        </tr>
        <tr>
            <td>{{ 'app.quote.deadline'|trans }}</td>
            <td>{% if quote.deadline %} {{ quote.deadline.format('Y-m-d') }} {% else %} -- {% endif %}</td>
        </tr>
        <tr>
            <td>{{ 'app.form.quantity'|trans }}</td>
            <td>{{ quote.quantity }}</td>
        </tr>
    </table>

    <h2 style=\"margin-top: 15px\">{{ 'app.quote.note'|trans }}</h2>
    <p>
        {{ quote.note }}
    </p>
{% endblock %}
", "Email/quote.html.twig", "/var/www/html/templates/Email/quote.html.twig");
    }
}
