<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Checkout:_support.html.twig */
class __TwigTemplate_69425ab8dcad95944c57fc54383a8dec587756ae3058c044cf0ddc984cd37bec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Checkout:_support.html.twig"));

        // line 1
        echo "<div class=\"text-center my-3\">
    <h3>";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.checkout.phone_number"), "html", null, true);
        echo "</h3>
    <div class=\"mb-3\">";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.need_assistance"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.call_us"), "html", null, true);
        echo "</div>
    <div class=\"h1\">
        <i class=\"fab fa-cc-mastercard\"></i>
        <i class=\"fab fa-cc-visa\"></i>
        <i class=\"fab fa-cc-paypal\"></i>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Checkout:_support.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 3,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"text-center my-3\">
    <h3>{{ 'app.checkout.phone_number'|trans }}</h3>
    <div class=\"mb-3\">{{ 'sylius.ui.need_assistance'|trans }} {{ 'sylius.ui.call_us'|trans }}</div>
    <div class=\"h1\">
        <i class=\"fab fa-cc-mastercard\"></i>
        <i class=\"fab fa-cc-visa\"></i>
        <i class=\"fab fa-cc-paypal\"></i>
    </div>
</div>
", "SyliusShopBundle:Checkout:_support.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Checkout/_support.html.twig");
    }
}
