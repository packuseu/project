<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusBannerPlugin/PositionTypes/_static_image.html.twig */
class __TwigTemplate_100e027e7d9e7b6f68a2e9fe31cb9a5dd2c81e4f3100a62b2309272e2f2dc12f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusBannerPlugin/PositionTypes/_static_image.html.twig"));

        // line 1
        $context["useContainer"] = (twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "full_screen", [], "any", true, true, false, 1) && (twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 1, $this->source); })()), "full_screen", [], "any", false, false, false, 1) == false));
        // line 2
        echo "
";
        // line 3
        if (twig_length_filter($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 3, $this->source); })()))) {
            // line 4
            echo "    ";
            $context["banner"] = twig_first($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 4, $this->source); })()));
            // line 5
            echo "
    ";
            // line 6
            if ((twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 6, $this->source); })()), "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 6, $this->source); })()), "channel", [], "array", false, false, false, 6)], "method", false, false, false, 6) && twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 6, $this->source); })()), "images", [], "any", false, false, false, 6)))) {
                // line 7
                echo "        ";
                $context["image"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 7, $this->source); })()), "images", [], "any", false, false, false, 7), "toArray", [], "any", false, false, false, 7), twig_random($this->env, (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 7, $this->source); })()), "images", [], "any", false, false, false, 7)) - 1)), [], "array", false, false, false, 7);
                // line 8
                echo "        <div class=\"col-xs-12\">
            <div class=\"banner\">
                <a class=\"banner-link\" href=\"";
                // line 10
                echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 10), "link", [], "any", true, true, false, 10)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 10), "link", [], "any", false, false, false, 10), "javascript:;")) : ("javascript:;")), "html", null, true);
                echo "\">
                    <img class=\"banner-image\" src=\"";
                // line 11
                echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 11, $this->source); })()), "path", [], "any", false, false, false, 11), "omni_sylius_banner"), "html", null, true);
                echo "\" alt=\"-\">

                    <div class=\"banner-caption caption--";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 13, $this->source); })()), "contentPosition", [], "any", false, false, false, 13), "html", null, true);
                echo "\"
                         style=\"";
                // line 14
                if (twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()), "contentSpace", [], "any", false, false, false, 14)) {
                    echo "width: ";
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()), "contentSpace", [], "any", false, false, false, 14)), "html", null, true);
                    echo "%";
                }
                echo " background-color: ";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()), "contentBackground", [], "any", false, false, false, 14), "html", null, true);
                echo ";\"
                    >
                        ";
                // line 16
                echo twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 16, $this->source); })()), "content", [], "any", false, false, false, 16);
                echo "
                    </div>
                </a>
            </div>
        </div>
    ";
            }
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusBannerPlugin/PositionTypes/_static_image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 16,  75 => 14,  71 => 13,  66 => 11,  62 => 10,  58 => 8,  55 => 7,  53 => 6,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set useContainer = options.full_screen is defined and options.full_screen == false %}

{% if banners|length %}
    {% set banner = banners|first %}

    {% if banner.hasChannelCode(options['channel']) and banner.images|length %}
        {% set image = banner.images.toArray[random(banner.images|length - 1)] %}
        <div class=\"col-xs-12\">
            <div class=\"banner\">
                <a class=\"banner-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                    <img class=\"banner-image\" src=\"{{ image.path|imagine_filter('omni_sylius_banner') }}\" alt=\"-\">

                    <div class=\"banner-caption caption--{{ image.contentPosition }}\"
                         style=\"{% if image.contentSpace %}width: {{ image.contentSpace|number_format }}%{% endif %} background-color: {{ image.contentBackground }};\"
                    >
                        {{ image.content|raw }}
                    </div>
                </a>
            </div>
        </div>
    {% endif %}
{% endif %}
", "@OmniSyliusBannerPlugin/PositionTypes/_static_image.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/PositionTypes/_static_image.html.twig");
    }
}
