<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusCmsPlugin/Frontend/show.html.twig */
class __TwigTemplate_997f021322e71679dbf90bf5d382bf3f1d71ef4bcc1efbbffd1b157f8d7b9034 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'metatags' => [$this, 'block_metatags'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusCmsPlugin/Frontend/show.html.twig"));

        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "@OmniSyliusCmsPlugin/Frontend/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "
    ";
        // line 5
        $this->loadTemplate("@OmniSyliusCmsPlugin/Frontend/_breadcrumb.html.twig", "@OmniSyliusCmsPlugin/Frontend/show.html.twig", 5)->display($context);
        // line 6
        echo "
    ";
        // line 7
        $macros["macros"] = $this;
        // line 8
        echo "    ";
        // line 27
        echo "
    ";
        // line 28
        echo twig_call_macro($macros["macros"], "macro_node_item", [(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 28, $this->source); })())], 28, $context, $this->getSourceContext());
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 31
    public function block_metatags($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "metatags"));

        // line 32
        echo "    ";
        echo call_user_func_array($this->env->getFunction('omni_seo_meta')->getCallable(), []);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function macro_node_item($__node__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "node_item"));

            // line 9
            echo "    <div class=\"content-page\">
    ";
            // line 10
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 10, $this->source); })()), "images", [], "any", false, false, false, 10), "first", [], "any", false, false, false, 10)) > 0)) {
                // line 11
                echo "        <div class=\"row w-100 justify-content-center pb-4\">
        ";
                // line 12
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 12, $this->source); })()), "images", [], "any", false, false, false, 12));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 13
                    echo "            <div class=\"col-6 text-center\">
                ";
                    // line 14
                    $context["imagePath"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 14), "omni_sylius_banner");
                    // line 15
                    echo "                <img class=\"content-image\" src=\"";
                    echo twig_escape_filter($this->env, (isset($context["imagePath"]) || array_key_exists("imagePath", $context) ? $context["imagePath"] : (function () { throw new RuntimeError('Variable "imagePath" does not exist.', 15, $this->source); })()), "html", null, true);
                    echo "\" alt=\"";
                    echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 15), "link", [], "any", false, false, false, 15);
                    echo "\">
            </div>
        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 18
                echo "        </div>
    ";
            }
            // line 20
            echo "        <div class=\"card-body\">
            <p class=\"text-justify\">
                ";
            // line 22
            echo $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->renderNode($this->env, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 22, $this->source); })()));
            echo "
            </p>
        </div>
    </div>
    ";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OmniSyliusCmsPlugin/Frontend/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 22,  150 => 20,  146 => 18,  134 => 15,  132 => 14,  129 => 13,  125 => 12,  122 => 11,  120 => 10,  117 => 9,  101 => 8,  91 => 32,  84 => 31,  75 => 28,  72 => 27,  70 => 8,  68 => 7,  65 => 6,  63 => 5,  60 => 4,  53 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% block content %}

    {% include '@OmniSyliusCmsPlugin/Frontend/_breadcrumb.html.twig' %}

    {% import _self as macros %}
    {% macro node_item(node) %}
    <div class=\"content-page\">
    {% if node.images.first|length > 0 %}
        <div class=\"row w-100 justify-content-center pb-4\">
        {% for image in node.images %}
            <div class=\"col-6 text-center\">
                {% set imagePath = image.path|imagine_filter('omni_sylius_banner') %}
                <img class=\"content-image\" src=\"{{ imagePath }}\" alt=\"{{ image.translation.link|raw }}\">
            </div>
        {% endfor %}
        </div>
    {% endif %}
        <div class=\"card-body\">
            <p class=\"text-justify\">
                {{ omni_sylius_cms_render_node(node) }}
            </p>
        </div>
    </div>
    {% endmacro %}

    {{ macros.node_item(node) }}
{% endblock %}

{% block metatags %}
    {{ omni_seo_meta() }}
{% endblock %}
", "@OmniSyliusCmsPlugin/Frontend/show.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/OmniSyliusCmsPlugin/views/Frontend/show.html.twig");
    }
}
