<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:state.html.twig */
class __TwigTemplate_f46276eb72804a576a514d8867122a4088b38ab5c3db1ae818ffde017ffb3ab2 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:state.html.twig"));

        // line 1
        $context["value"] = ("bitbag.ui." . twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 1, $this->source); })()), "state", [], "any", false, false, false, 1));
        // line 2
        echo "
";
        // line 3
        if ((twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 3, $this->source); })()), "state", [], "any", false, false, false, 3) == "new")) {
            // line 4
            echo "    ";
            $this->loadTemplate("@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_exportShipment.html.twig", "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:state.html.twig", 4)->display($context);
        } else {
            // line 6
            echo "    <span class=\"shipping-export-state ui green label\"><i class=\"check icon\"></i> ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new RuntimeError('Variable "value" does not exist.', 6, $this->source); })())), "html", null, true);
            echo "</span>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:state.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 6,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set value = 'bitbag.ui.' ~ data.state %}

{% if  data.state == 'new' %}
    {% include '@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_exportShipment.html.twig' %}
{% else %}
    <span class=\"shipping-export-state ui green label\"><i class=\"check icon\"></i> {{ value|trans }}</span>
{% endif %}
", "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:state.html.twig", "/var/www/html/vendor/bitbag/shipping-export-plugin/src/Resources/views/ShippingExport/Grid/Field/state.html.twig");
    }
}
