<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SyliusAdmin/Product/Show/_header.html.twig */
class __TwigTemplate_2992ded3d5212751a8d9a809f4ee09f9ae842ad824b8bb393253b44cdb304881 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@SyliusAdmin/Product/Show/_header.html.twig"));

        // line 1
        echo "<div id=\"header\" class=\"ui stackable two column grid\">
    <div class=\"column\">
        <h1 class=\"ui header\">
            <i class=\"circular cube icon\"></i>
            <div class=\"content\">
                <span>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "name", [], "any", false, false, false, 6), "html", null, true);
        echo "</span>
                <div class=\"sub header\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.show_product"), "html", null, true);
        echo "</div>
            </div>
        </h1>
        ";
        // line 10
        $this->loadTemplate("@SyliusAdmin/Product/Show/_breadcrumb.html.twig", "@SyliusAdmin/Product/Show/_header.html.twig", 10)->display($context);
        // line 11
        echo "    </div>
    ";
        // line 12
        $this->loadTemplate("@SyliusAdmin/Product/_showInShopButton.html.twig", "@SyliusAdmin/Product/Show/_header.html.twig", 12)->display($context);
        // line 13
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@SyliusAdmin/Product/Show/_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 13,  62 => 12,  59 => 11,  57 => 10,  51 => 7,  47 => 6,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div id=\"header\" class=\"ui stackable two column grid\">
    <div class=\"column\">
        <h1 class=\"ui header\">
            <i class=\"circular cube icon\"></i>
            <div class=\"content\">
                <span>{{ product.name }}</span>
                <div class=\"sub header\">{{ 'sylius.ui.show_product'|trans }}</div>
            </div>
        </h1>
        {% include \"@SyliusAdmin/Product/Show/_breadcrumb.html.twig\" %}
    </div>
    {% include '@SyliusAdmin/Product/_showInShopButton.html.twig' %}
</div>
", "@SyliusAdmin/Product/Show/_header.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/Product/Show/_header.html.twig");
    }
}
