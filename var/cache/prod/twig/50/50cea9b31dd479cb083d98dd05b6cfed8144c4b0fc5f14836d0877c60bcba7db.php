<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusFilterPlugin:Grid/Filter:product_attribute.html.twig */
class __TwigTemplate_a56a334fd67c93395c7ae958ae00d9748865160f9514ea96dfc86dbaf728b35e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusFilterPlugin:Grid/Filter:product_attribute.html.twig"));

        // line 1
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), [0 => "@SyliusUi/Form/theme.html.twig"], true);
        // line 2
        echo "
";
        // line 3
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 3, $this->source); })()), "children", [], "any", false, false, false, 3))) {
            // line 4
            echo "    <div class=\"ui stackable grid\">
        ";
            // line 5
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "vars", [], "any", false, false, false, 5), "clear_all_button", [], "any", false, false, false, 5)) {
                // line 6
                echo "            <div class=\"sixteen wide column\">
                <a class=\"ui primary icon labeled button\" href=\"";
                // line 7
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_index", $this->extensions['Omni\Sylius\FilterPlugin\Twig\FilterParamsExtension']->getResetAllParams(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "request", [], "any", false, false, false, 7))), "html", null, true);
                echo "\">
                    ";
                // line 8
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_filter.clear_attributes"), "html", null, true);
                echo "
                </a>
            </div>
        ";
            }
            // line 12
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), "children", [], "any", false, false, false, 12));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute_filter"]) {
                // line 13
                echo "            <div class=\"five wide column\">
                ";
                // line 14
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["attribute_filter"], 'row');
                echo "
                ";
                // line 15
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "vars", [], "any", false, false, false, 15), "clear_attribute_button", [], "any", false, false, false, 15)) {
                    // line 16
                    echo "                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_index", $this->extensions['Omni\Sylius\FilterPlugin\Twig\FilterParamsExtension']->getResetQueryParams(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 16, $this->source); })()), "request", [], "any", false, false, false, 16), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["attribute_filter"], "vars", [], "any", false, false, false, 16), "name", [], "any", false, false, false, 16))), "html", null, true);
                    echo "\">
                        ";
                    // line 17
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.form.product_attribute.clear"), "html", null, true);
                    echo "
                    </a>
                ";
                }
                // line 20
                echo "            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute_filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusFilterPlugin:Grid/Filter:product_attribute.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 22,  91 => 20,  85 => 17,  80 => 16,  78 => 15,  74 => 14,  71 => 13,  66 => 12,  59 => 8,  55 => 7,  52 => 6,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% form_theme form '@SyliusUi/Form/theme.html.twig' %}

{% if form.children|length %}
    <div class=\"ui stackable grid\">
        {% if form.vars.clear_all_button %}
            <div class=\"sixteen wide column\">
                <a class=\"ui primary icon labeled button\" href=\"{{ path('sylius_shop_product_index', omni_filter_get_reset_all_query_params(app.request)) }}\">
                    {{ 'omni_filter.clear_attributes'|trans}}
                </a>
            </div>
        {% endif %}
        {% for attribute_filter in form.children %}
            <div class=\"five wide column\">
                {{ form_row(attribute_filter) }}
                {% if form.vars.clear_attribute_button %}
                    <a href=\"{{ path('sylius_shop_product_index', omni_filter_get_reset_query_params(app.request, attribute_filter.vars.name)) }}\">
                        {{ 'omni_sylius.form.product_attribute.clear'|trans }}
                    </a>
                {% endif %}
            </div>
        {% endfor %}
    </div>
{% endif %}
", "OmniSyliusFilterPlugin:Grid/Filter:product_attribute.html.twig", "/var/www/html/vendor/omni/sylius-filter-plugin/src/Resources/views/Grid/Filter/product_attribute.html.twig");
    }
}
