<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Banner/PositionType/_smart_slider.html.twig */
class __TwigTemplate_d6ba3783f6013355b1b817703f6a8d9857d96d8e6cdde58094c1cafaf8de15f1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Banner/PositionType/_smart_slider.html.twig"));

        // line 1
        if ((twig_length_filter($this->env, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 1, $this->source); })())) > 0)) {
            // line 2
            echo "    ";
            $context["bannerImagesCount"] = 0;
            // line 3
            echo "
    ";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 4, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 4, $this->source); })()), "channel", [], "array", false, false, false, 4)], "method", false, false, false, 4)) {
                    // line 5
                    echo "        ";
                    $context["bannerImagesCount"] = ((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 5, $this->source); })()) + twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 5)));
                    // line 6
                    echo "    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "
    <div class=\"container\">
        ";
            // line 9
            $context["idName"] = twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 9, $this->source); })()), "code", [], "any", false, false, false, 9);
            // line 10
            echo "
        <div id=\"";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 11, $this->source); })()), "html", null, true);
            echo "\" class=\"carousel slide banners-slider\" data-ride=\"carousel\"
             ";
            // line 12
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 12, $this->source); })()) == 1)) {
                echo "data-interval=\"false\"";
            }
            // line 13
            echo "        >
            ";
            // line 14
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 14, $this->source); })()) > 1)) {
                // line 15
                echo "                <ol class=\"carousel-indicators\">
                    ";
                // line 16
                $context["bannerIndex"] = 0;
                // line 17
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 17, $this->source); })()));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                    if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 17, $this->source); })()), "channel", [], "array", false, false, false, 17)], "method", false, false, false, 17)) {
                        // line 18
                        echo "                        ";
                        $context["isFirstBanner"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 18)) ? (true) : (false));
                        // line 19
                        echo "
                        ";
                        // line 20
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 20));
                        $context['loop'] = [
                          'parent' => $context['_parent'],
                          'index0' => 0,
                          'index'  => 1,
                          'first'  => true,
                        ];
                        foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                            if ((twig_get_attribute($this->env, $this->source, $context["image"], "contentPosition", [], "any", false, false, false, 20) == "default")) {
                                // line 21
                                echo "                            ";
                                $context["isFirstImage"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 21)) ? (true) : (false));
                                // line 22
                                echo "                            ";
                                $context["isActive"] = ((((isset($context["isFirstBanner"]) || array_key_exists("isFirstBanner", $context) ? $context["isFirstBanner"] : (function () { throw new RuntimeError('Variable "isFirstBanner" does not exist.', 22, $this->source); })()) && (isset($context["isFirstImage"]) || array_key_exists("isFirstImage", $context) ? $context["isFirstImage"] : (function () { throw new RuntimeError('Variable "isFirstImage" does not exist.', 22, $this->source); })()))) ? ("active") : (""));
                                // line 23
                                echo "
                            <li class=\"carousel-indicator ";
                                // line 24
                                echo twig_escape_filter($this->env, (isset($context["isActive"]) || array_key_exists("isActive", $context) ? $context["isActive"] : (function () { throw new RuntimeError('Variable "isActive" does not exist.', 24, $this->source); })()), "html", null, true);
                                echo "\"
                                data-target=\"#";
                                // line 25
                                echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 25, $this->source); })()), "html", null, true);
                                echo "\"
                                data-slide-to=\"";
                                // line 26
                                echo twig_escape_filter($this->env, (isset($context["bannerIndex"]) || array_key_exists("bannerIndex", $context) ? $context["bannerIndex"] : (function () { throw new RuntimeError('Variable "bannerIndex" does not exist.', 26, $this->source); })()), "html", null, true);
                                echo "\"
                            >
                            </li>

                            ";
                                // line 30
                                $context["bannerIndex"] = ((isset($context["bannerIndex"]) || array_key_exists("bannerIndex", $context) ? $context["bannerIndex"] : (function () { throw new RuntimeError('Variable "bannerIndex" does not exist.', 30, $this->source); })()) + 1);
                                // line 31
                                echo "                        ";
                                ++$context['loop']['index0'];
                                ++$context['loop']['index'];
                                $context['loop']['first'] = false;
                            }
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 32
                        echo "                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 33
                echo "                </ol>
            ";
            }
            // line 35
            echo "
            <div class=\"carousel-inner\" role=\"listbox\">
                ";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 37, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 37, $this->source); })()), "channel", [], "array", false, false, false, 37)], "method", false, false, false, 37)) {
                    // line 38
                    echo "                    ";
                    $context["isFirstBanner"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 38)) ? (true) : (false));
                    // line 39
                    echo "
                    ";
                    // line 40
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 40));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        if ((twig_get_attribute($this->env, $this->source, $context["image"], "contentPosition", [], "any", false, false, false, 40) == "default")) {
                            // line 41
                            echo "                        ";
                            $context["isFirstImage"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 41)) ? (true) : (false));
                            // line 42
                            echo "                        ";
                            $context["isActive"] = ((((isset($context["isFirstBanner"]) || array_key_exists("isFirstBanner", $context) ? $context["isFirstBanner"] : (function () { throw new RuntimeError('Variable "isFirstBanner" does not exist.', 42, $this->source); })()) && (isset($context["isFirstImage"]) || array_key_exists("isFirstImage", $context) ? $context["isFirstImage"] : (function () { throw new RuntimeError('Variable "isFirstImage" does not exist.', 42, $this->source); })()))) ? ("active") : (""));
                            // line 43
                            echo "
                        <div class=\"carousel-item ";
                            // line 44
                            echo twig_escape_filter($this->env, (isset($context["isActive"]) || array_key_exists("isActive", $context) ? $context["isActive"] : (function () { throw new RuntimeError('Variable "isActive" does not exist.', 44, $this->source); })()), "html", null, true);
                            echo "\">
                            <a class=\"carousel-item-link\" href=\"";
                            // line 45
                            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 45), "link", [], "any", true, true, false, 45)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 45), "link", [], "any", false, false, false, 45), "javascript:;")) : ("javascript:;")), "html", null, true);
                            echo "\">
                                ";
                            // line 46
                            $context["imagePath"] = "";
                            // line 47
                            echo "
                                ";
                            // line 48
                            if ((array_key_exists("image", $context) && twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 48))) {
                                // line 49
                                echo "                                    ";
                                $context["imagePath"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 49), "omni_sylius_banner");
                                // line 50
                                echo "                                ";
                            }
                            // line 51
                            echo "
                                <img class=\"carousel-item-link__img d-block img-fluid\" src=\"";
                            // line 52
                            echo twig_escape_filter($this->env, (isset($context["imagePath"]) || array_key_exists("imagePath", $context) ? $context["imagePath"] : (function () { throw new RuntimeError('Variable "imagePath" does not exist.', 52, $this->source); })()), "html", null, true);
                            echo "\" alt=\"-\">

                                <div class=\"carousel-caption caption--";
                            // line 54
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentPosition", [], "any", false, false, false, 54), "html", null, true);
                            echo "\"
                                     ";
                            // line 55
                            if (twig_get_attribute($this->env, $this->source, $context["image"], "contentSpace", [], "any", false, false, false, 55)) {
                                echo "style=\"width: ";
                                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentSpace", [], "any", false, false, false, 55)), "html", null, true);
                                echo "%\"";
                            }
                            // line 56
                            echo "                                >
                                    ";
                            // line 57
                            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 57), "content", [], "any", false, false, false, 57);
                            echo "
                                </div>
                            </a>
                        </div>
                    ";
                            ++$context['loop']['index0'];
                            ++$context['loop']['index'];
                            $context['loop']['first'] = false;
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 62
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "            </div>

            ";
            // line 65
            if (((isset($context["bannerImagesCount"]) || array_key_exists("bannerImagesCount", $context) ? $context["bannerImagesCount"] : (function () { throw new RuntimeError('Variable "bannerImagesCount" does not exist.', 65, $this->source); })()) > 1)) {
                // line 66
                echo "                <a class=\"carousel-control-prev circle-icon\" href=\"#";
                echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 66, $this->source); })()), "html", null, true);
                echo "\" role=\"button\" data-slide=\"prev\">
                    <span class=\"carousel-control-prev-icon chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">";
                // line 68
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.previous"), "html", null, true);
                echo "</span>
                </a>

                <a class=\"carousel-control-next circle-icon\" href=\"#";
                // line 71
                echo twig_escape_filter($this->env, (isset($context["idName"]) || array_key_exists("idName", $context) ? $context["idName"] : (function () { throw new RuntimeError('Variable "idName" does not exist.', 71, $this->source); })()), "html", null, true);
                echo "\" role=\"button\" data-slide=\"next\">
                    <span class=\"carousel-control-next-icon chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">";
                // line 73
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.next"), "html", null, true);
                echo "</span>
                </a>
            ";
            }
            // line 76
            echo "        </div>

        <div style=\"display: flex; justify-content: space-between; margin-bottom: 20px;\">
            ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 79, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 79, $this->source); })()), "channel", [], "array", false, false, false, 79)], "method", false, false, false, 79)) {
                    // line 80
                    echo "                ";
                    $context["count"] = 0;
                    // line 81
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 81));
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        if (((twig_get_attribute($this->env, $this->source, $context["image"], "contentPosition", [], "any", false, false, false, 81) == "bottom") && ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 81, $this->source); })()) < 3))) {
                            // line 82
                            echo "                    <a href=\"";
                            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 82), "link", [], "any", true, true, false, 82)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 82), "link", [], "any", false, false, false, 82), "javascript:;")) : ("javascript:;")), "html", null, true);
                            echo "\">
                        ";
                            // line 83
                            $context["imagePath"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 83), "omni_sylius_banner");
                            // line 84
                            echo "
                        <img src=\"";
                            // line 85
                            echo twig_escape_filter($this->env, (isset($context["imagePath"]) || array_key_exists("imagePath", $context) ? $context["imagePath"] : (function () { throw new RuntimeError('Variable "imagePath" does not exist.', 85, $this->source); })()), "html", null, true);
                            echo "\" style=\"width: 100%; height: 100%;\" alt=\"";
                            echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 85), "content", [], "any", false, false, false, 85);
                            echo "\">
                    </a>
                    ";
                            // line 87
                            $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new RuntimeError('Variable "count" does not exist.', 87, $this->source); })()) + 1);
                            // line 88
                            echo "                ";
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 89
                    echo "            ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 90
            echo "        </div>
    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Banner/PositionType/_smart_slider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  358 => 90,  351 => 89,  344 => 88,  342 => 87,  335 => 85,  332 => 84,  330 => 83,  325 => 82,  319 => 81,  316 => 80,  311 => 79,  306 => 76,  300 => 73,  295 => 71,  289 => 68,  283 => 66,  281 => 65,  277 => 63,  267 => 62,  252 => 57,  249 => 56,  243 => 55,  239 => 54,  234 => 52,  231 => 51,  228 => 50,  225 => 49,  223 => 48,  220 => 47,  218 => 46,  214 => 45,  210 => 44,  207 => 43,  204 => 42,  201 => 41,  190 => 40,  187 => 39,  184 => 38,  173 => 37,  169 => 35,  165 => 33,  155 => 32,  145 => 31,  143 => 30,  136 => 26,  132 => 25,  128 => 24,  125 => 23,  122 => 22,  119 => 21,  108 => 20,  105 => 19,  102 => 18,  90 => 17,  88 => 16,  85 => 15,  83 => 14,  80 => 13,  76 => 12,  72 => 11,  69 => 10,  67 => 9,  63 => 7,  56 => 6,  53 => 5,  48 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if banners|length > 0 %}
    {% set bannerImagesCount = 0 %}

    {% for banner in banners if banner.hasChannelCode(options['channel']) %}
        {% set bannerImagesCount = bannerImagesCount + banner.images|length %}
    {% endfor %}

    <div class=\"container\">
        {% set idName = position.code %}

        <div id=\"{{ idName }}\" class=\"carousel slide banners-slider\" data-ride=\"carousel\"
             {% if bannerImagesCount == 1 %}data-interval=\"false\"{% endif %}
        >
            {% if bannerImagesCount > 1 %}
                <ol class=\"carousel-indicators\">
                    {% set bannerIndex = 0 %}
                    {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                        {% set isFirstBanner = loop.first ? true : false %}

                        {% for image in banner.images if image.contentPosition == 'default' %}
                            {% set isFirstImage = loop.first ? true : false %}
                            {% set isActive = (isFirstBanner and isFirstImage) ? 'active': '' %}

                            <li class=\"carousel-indicator {{ isActive }}\"
                                data-target=\"#{{ idName }}\"
                                data-slide-to=\"{{ bannerIndex }}\"
                            >
                            </li>

                            {% set bannerIndex = bannerIndex + 1 %}
                        {% endfor %}
                    {% endfor %}
                </ol>
            {% endif %}

            <div class=\"carousel-inner\" role=\"listbox\">
                {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                    {% set isFirstBanner = loop.first ? true : false %}

                    {% for image in banner.images if image.contentPosition == 'default' %}
                        {% set isFirstImage = loop.first ? true : false %}
                        {% set isActive = (isFirstBanner and isFirstImage) ? 'active': '' %}

                        <div class=\"carousel-item {{ isActive }}\">
                            <a class=\"carousel-item-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                                {% set imagePath = '' %}

                                {% if image is defined and image.path %}
                                    {% set imagePath = image.path|imagine_filter('omni_sylius_banner') %}
                                {% endif %}

                                <img class=\"carousel-item-link__img d-block img-fluid\" src=\"{{ imagePath }}\" alt=\"-\">

                                <div class=\"carousel-caption caption--{{ image.contentPosition }}\"
                                     {% if image.contentSpace %}style=\"width: {{ image.contentSpace|number_format }}%\"{% endif %}
                                >
                                    {{ image.translation.content|raw }}
                                </div>
                            </a>
                        </div>
                    {% endfor %}
                {% endfor %}
            </div>

            {% if bannerImagesCount > 1 %}
                <a class=\"carousel-control-prev circle-icon\" href=\"#{{ idName }}\" role=\"button\" data-slide=\"prev\">
                    <span class=\"carousel-control-prev-icon chevron-left\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">{{ 'omni.ui.previous'|trans }}</span>
                </a>

                <a class=\"carousel-control-next circle-icon\" href=\"#{{ idName }}\" role=\"button\" data-slide=\"next\">
                    <span class=\"carousel-control-next-icon chevron-right\" aria-hidden=\"true\"></span>
                    <span class=\"sr-only\">{{ 'omni.ui.next'|trans }}</span>
                </a>
            {% endif %}
        </div>

        <div style=\"display: flex; justify-content: space-between; margin-bottom: 20px;\">
            {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                {% set count = 0 %}
                {% for image in banner.images if image.contentPosition == 'bottom' and count < 3 %}
                    <a href=\"{{ image.translation.link|default('javascript:;') }}\">
                        {% set imagePath = image.path|imagine_filter('omni_sylius_banner') %}

                        <img src=\"{{ imagePath }}\" style=\"width: 100%; height: 100%;\" alt=\"{{ image.translation.content|raw }}\">
                    </a>
                    {% set count = count + 1 %}
                {% endfor %}
            {% endfor %}
        </div>
    </div>
{% endif %}
", "Banner/PositionType/_smart_slider.html.twig", "/var/www/html/templates/Banner/PositionType/_smart_slider.html.twig");
    }
}
