<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Cart:_widget.html.twig */
class __TwigTemplate_81053d8b22d352b77623fa8f2737f4f0ac5664b90a8c3e2f52648b8e651267c3 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Cart:_widget.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Cart:_widget.html.twig", 1)->unwrap();
        // line 2
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Cart:_widget.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.partial.cart.summary.before_widget_content", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 4, $this->source); })())]]);
        echo "

<div class=\"btn-group\">
    <a class=\"cart-toggle\" data-toggle=\"dropdown\" href=\"#\">
        <span class=\"header-icon\"><i class=\"fa fa-shopping-cart\"></i></span>
        <span style=\"color: #21282C;font-family: \"Nunito Sans\";\">";
        // line 9
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 9, $this->source); })()), "itemsTotal", [], "any", false, false, false, 9)], 9, $context, $this->getSourceContext());
        echo "</span>
    </a>

    <div class=\"dropdown-menu dropdown-menu-right\" style=\"width:300px\">
        ";
        // line 13
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.partial.cart.summary.before_popup_content", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 13, $this->source); })())]]);
        echo "

        ";
        // line 15
        if (twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 15, $this->source); })()), "empty", [], "any", false, false, false, 15)) {
            // line 16
            echo "            <div class=\"card-body text-center\">
                ";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.your_cart_is_empty"), "html", null, true);
            echo ".
            </div>
        ";
        } else {
            // line 20
            echo "
            <table class=\"table table-borderless\">
                <tbody>
                ";
            // line 23
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 23, $this->source); })()), "items", [], "any", false, false, false, 23));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 24
                echo "                    <tr>
                        <td>";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "product", [], "any", false, false, false, 25), "html", null, true);
                echo "</td>
                        <td class=\"text-right\">
                            <small class=\"text-muted\">";
                // line 27
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 27), "html", null, true);
                echo " x </small>
                            ";
                // line 28
                echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, $context["item"], "unitPrice", [], "any", false, false, false, 28)], 28, $context, $this->getSourceContext());
                echo "
                        </td>
                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "                    <tr class=\"bg-light\">
                        <td><strong class=\"text-dark\">";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.subtotal"), "html", null, true);
            echo ":</strong></td>
                        <td class=\"text-right text-dark\"><strong>";
            // line 34
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 34, $this->source); })()), "itemsTotal", [], "any", false, false, false, 34)], 34, $context, $this->getSourceContext());
            echo "</strong></td>
                    </tr>
                </tbody>
            </table>

            <div class=\"px-3 pb-2\">
                <a href=\"";
            // line 40
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_cart_summary");
            echo "\" class=\"btn btn-outline-primary btn-block\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.view_and_edit_cart"), "html", null, true);
            echo "</a>
                <a href=\"";
            // line 41
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_checkout_start");
            echo "\" class=\"btn btn-primary btn-block\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.checkout"), "html", null, true);
            echo "</a>
            </div>
        ";
        }
        // line 44
        echo "
        ";
        // line 45
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.partial.cart.summary.after_popup_content", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 45, $this->source); })())]]);
        echo "
    </div>
</div>

";
        // line 49
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.partial.cart.summary.after_widget_content", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 49, $this->source); })())]]);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Cart:_widget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 49,  142 => 45,  139 => 44,  131 => 41,  125 => 40,  116 => 34,  112 => 33,  109 => 32,  99 => 28,  95 => 27,  90 => 25,  87 => 24,  83 => 23,  78 => 20,  72 => 17,  69 => 16,  67 => 15,  62 => 13,  55 => 9,  47 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}
{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{{ sonata_block_render_event('sylius.shop.partial.cart.summary.before_widget_content', {'cart': cart}) }}

<div class=\"btn-group\">
    <a class=\"cart-toggle\" data-toggle=\"dropdown\" href=\"#\">
        <span class=\"header-icon\"><i class=\"fa fa-shopping-cart\"></i></span>
        <span style=\"color: #21282C;font-family: \"Nunito Sans\";\">{{ money.convertAndFormat(cart.itemsTotal) }}</span>
    </a>

    <div class=\"dropdown-menu dropdown-menu-right\" style=\"width:300px\">
        {{ sonata_block_render_event('sylius.shop.partial.cart.summary.before_popup_content', {'cart': cart}) }}

        {% if cart.empty %}
            <div class=\"card-body text-center\">
                {{ 'sylius.ui.your_cart_is_empty'|trans }}.
            </div>
        {% else %}

            <table class=\"table table-borderless\">
                <tbody>
                {% for item in cart.items %}
                    <tr>
                        <td>{{ item.product }}</td>
                        <td class=\"text-right\">
                            <small class=\"text-muted\">{{ item.quantity }} x </small>
                            {{ money.convertAndFormat(item.unitPrice) }}
                        </td>
                    </tr>
                {% endfor %}
                    <tr class=\"bg-light\">
                        <td><strong class=\"text-dark\">{{ 'sylius.ui.subtotal'|trans }}:</strong></td>
                        <td class=\"text-right text-dark\"><strong>{{ money.convertAndFormat(cart.itemsTotal) }}</strong></td>
                    </tr>
                </tbody>
            </table>

            <div class=\"px-3 pb-2\">
                <a href=\"{{ path('sylius_shop_cart_summary') }}\" class=\"btn btn-outline-primary btn-block\">{{ 'sylius.ui.view_and_edit_cart'|trans }}</a>
                <a href=\"{{ path('sylius_shop_checkout_start') }}\" class=\"btn btn-primary btn-block\">{{ 'sylius.ui.checkout'|trans }}</a>
            </div>
        {% endif %}

        {{ sonata_block_render_event('sylius.shop.partial.cart.summary.after_popup_content', {'cart': cart}) }}
    </div>
</div>

{{ sonata_block_render_event('sylius.shop.partial.cart.summary.after_widget_content', {'cart': cart}) }}
", "SyliusShopBundle:Cart:_widget.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Cart/_widget.html.twig");
    }
}
