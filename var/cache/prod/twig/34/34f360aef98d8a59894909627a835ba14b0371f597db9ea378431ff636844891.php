<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusBannerPlugin/PositionTypes/_multislider.html.twig */
class __TwigTemplate_ac23ee3b2aa5884233aa995aeb99b2b9efeb1a520093bcd7bacf699f853b9132 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusBannerPlugin/PositionTypes/_multislider.html.twig"));

        // line 2
        echo "
<div class=\"container\">
    <div class=\"banners-list banners-small-carousel\">
        ";
        // line 5
        if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 5, $this->source); })()), "title", [], "any", false, false, false, 5)) && twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 5, $this->source); })()), "descriptive", [], "any", false, false, false, 5))) {
            // line 6
            echo "            <h4 class=\"heading heading--carousel\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 6, $this->source); })()), "title", [], "any", false, false, false, 6), "html", null, true);
            echo "</h4>
        ";
        }
        // line 8
        echo "
        ";
        // line 9
        if (( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 9, $this->source); })()), "translation", [], "any", false, false, false, 9), "description", [], "any", false, false, false, 9)) && twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 9, $this->source); })()), "descriptive", [], "any", false, false, false, 9))) {
            // line 10
            echo "            <div class=\"banners-list__text banners-small-carousel__text\">
                ";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 11, $this->source); })()), "translation", [], "any", false, false, false, 11), "description", [], "any", false, false, false, 11), "html", null, true);
            echo "
            </div>
        ";
        }
        // line 14
        echo "
        <div class=\"card-carousel-section carousel--banners\"
             id=\"banners-card-carousel\"
        >
            <a href=\"javascript:;\" class=\"card-carousel-control position--left is--disabled circle-icon\">
                <span class=\"card-carousel-control__img icon chevron-left\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.previous"), "html", null, true);
        echo "</span>
            </a>

            <a href=\"javascript:;\" class=\"card-carousel-control position--right circle-icon\">
                <span class=\"card-carousel-control__img icon chevron-right\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni.ui.next"), "html", null, true);
        echo "</span>
            </a>

            <div class=\"card-carousel-wrapper\">
                <div class=\"card-carousel row flex-nowrap\">
                    ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 30, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 30, $this->source); })()), "channel", [], "array", false, false, false, 30)], "method", false, false, false, 30)) {
                // line 31
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 31));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 32
                    echo "
                            <div class=\"card-carousel-item col-12 col-md-6 col-lg-2\">
                                <div class=\"card\">
                                    <img class=\"card-img-top\" src=\"";
                    // line 35
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 35), "omni_sylius_banner"), "html", null, true);
                    echo "\" alt=\"\">
                                </div>
                            </div>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 39
                echo "                    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "                </div>
            </div>

            <ul class=\"card-carousel-indicators d-md-none list-unstyled list-inline text-center\">
                ";
        // line 44
        $context["index"] = 0;
        // line 45
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 45, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 45, $this->source); })()), "channel", [], "array", false, false, false, 45)], "method", false, false, false, 45)) {
                // line 46
                echo "                    ";
                $context["isFirstBanner"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 46)) ? (true) : (false));
                // line 47
                echo "
                    ";
                // line 48
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 48));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 49
                    echo "                        ";
                    $context["isFirstImage"] = ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 49)) ? (true) : (false));
                    // line 50
                    echo "                        ";
                    $context["isActive"] = ((((isset($context["isFirstBanner"]) || array_key_exists("isFirstBanner", $context) ? $context["isFirstBanner"] : (function () { throw new RuntimeError('Variable "isFirstBanner" does not exist.', 50, $this->source); })()) && (isset($context["isFirstImage"]) || array_key_exists("isFirstImage", $context) ? $context["isFirstImage"] : (function () { throw new RuntimeError('Variable "isFirstImage" does not exist.', 50, $this->source); })()))) ? ("is--active") : (""));
                    // line 51
                    echo "                        ";
                    $context["index"] = ((isset($context["index"]) || array_key_exists("index", $context) ? $context["index"] : (function () { throw new RuntimeError('Variable "index" does not exist.', 51, $this->source); })()) + 1);
                    // line 52
                    echo "
                        <li class=\"card-carousel-indicator list-inline-item indicator--";
                    // line 53
                    echo twig_escape_filter($this->env, (isset($context["index"]) || array_key_exists("index", $context) ? $context["index"] : (function () { throw new RuntimeError('Variable "index" does not exist.', 53, $this->source); })()), "html", null, true);
                    echo " ";
                    echo twig_escape_filter($this->env, (isset($context["isActive"]) || array_key_exists("isActive", $context) ? $context["isActive"] : (function () { throw new RuntimeError('Variable "isActive" does not exist.', 53, $this->source); })()), "html", null, true);
                    echo "\"
                            data-slide-to=\"";
                    // line 54
                    echo twig_escape_filter($this->env, (isset($context["index"]) || array_key_exists("index", $context) ? $context["index"] : (function () { throw new RuntimeError('Variable "index" does not exist.', 54, $this->source); })()), "html", null, true);
                    echo "\"
                        >
                            <i class=\"card-carousel-indicator__img\" data-feather=\"circle\"></i>
                        </li>
                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "            </ul>
        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusBannerPlugin/PositionTypes/_multislider.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 60,  203 => 59,  184 => 54,  178 => 53,  175 => 52,  172 => 51,  169 => 50,  166 => 49,  149 => 48,  146 => 47,  143 => 46,  131 => 45,  129 => 44,  123 => 40,  116 => 39,  106 => 35,  101 => 32,  96 => 31,  91 => 30,  83 => 25,  75 => 20,  67 => 14,  61 => 11,  58 => 10,  56 => 9,  53 => 8,  47 => 6,  45 => 5,  40 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# this demo is made with Bootstrap 4 #}

<div class=\"container\">
    <div class=\"banners-list banners-small-carousel\">
        {% if position.title is not null and options.descriptive %}
            <h4 class=\"heading heading--carousel\">{{ position.title }}</h4>
        {% endif %}

        {% if position.translation.description is not null and options.descriptive %}
            <div class=\"banners-list__text banners-small-carousel__text\">
                {{ position.translation.description }}
            </div>
        {% endif %}

        <div class=\"card-carousel-section carousel--banners\"
             id=\"banners-card-carousel\"
        >
            <a href=\"javascript:;\" class=\"card-carousel-control position--left is--disabled circle-icon\">
                <span class=\"card-carousel-control__img icon chevron-left\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">{{ 'omni.ui.previous'|trans }}</span>
            </a>

            <a href=\"javascript:;\" class=\"card-carousel-control position--right circle-icon\">
                <span class=\"card-carousel-control__img icon chevron-right\" aria-hidden=\"true\"></span>
                <span class=\"sr-only\">{{ 'omni.ui.next'|trans }}</span>
            </a>

            <div class=\"card-carousel-wrapper\">
                <div class=\"card-carousel row flex-nowrap\">
                    {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                        {% for image in banner.images %}

                            <div class=\"card-carousel-item col-12 col-md-6 col-lg-2\">
                                <div class=\"card\">
                                    <img class=\"card-img-top\" src=\"{{ image.path|imagine_filter('omni_sylius_banner') }}\" alt=\"\">
                                </div>
                            </div>
                        {% endfor %}
                    {% endfor %}
                </div>
            </div>

            <ul class=\"card-carousel-indicators d-md-none list-unstyled list-inline text-center\">
                {% set index = 0 %}
                {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                    {% set isFirstBanner = loop.first ? true : false %}

                    {% for image in banner.images %}
                        {% set isFirstImage = loop.first ? true : false %}
                        {% set isActive = (isFirstBanner and isFirstImage) ? 'is--active': '' %}
                        {% set index = index + 1 %}

                        <li class=\"card-carousel-indicator list-inline-item indicator--{{ index }} {{ isActive }}\"
                            data-slide-to=\"{{ index }}\"
                        >
                            <i class=\"card-carousel-indicator__img\" data-feather=\"circle\"></i>
                        </li>
                    {% endfor %}
                {% endfor %}
            </ul>
        </div>
    </div>
</div>

", "@OmniSyliusBannerPlugin/PositionTypes/_multislider.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/PositionTypes/_multislider.html.twig");
    }
}
