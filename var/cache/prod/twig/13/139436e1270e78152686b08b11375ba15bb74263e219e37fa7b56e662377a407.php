<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Cart:summary.html.twig */
class __TwigTemplate_933e9c5c4d46cd3aaad7e3189602d6c3252be823c2d15909c02fc068cbc8225e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Cart:summary.html.twig"));

        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 3, $this->source); })()), [0 => "@SyliusShop/Form/theme.html.twig"], true);
        // line 5
        $macros["messages"] = $this->macros["messages"] = $this->loadTemplate("@SyliusShop/Common/Macro/messages.html.twig", "SyliusShopBundle:Cart:summary.html.twig", 5)->unwrap();
        // line 7
        $context["header"] = "sylius.ui.your_shopping_cart";
        // line 1
        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "SyliusShopBundle:Cart:summary.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 9
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $this->displayParentBlock("title", $context, $blocks);
        echo " | ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 9, $this->source); })())), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 12
        echo "    ";
        if ( !twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 12, $this->source); })()), "empty", [], "any", false, false, false, 12)) {
            // line 13
            echo "        ";
            $this->loadTemplate("@SyliusShop/Cart/Summary/_header.html.twig", "SyliusShopBundle:Cart:summary.html.twig", 13)->display($context);
            // line 14
            echo "
        ";
            // line 15
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.after_content_header", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 15, $this->source); })())]]);
            echo "

        <div class=\"row mb-5\">
            <div class=\"col-12 col-lg-8 mb-4\">
                ";
            // line 19
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.before_items", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 19, $this->source); })())]]);
            echo "

                ";
            // line 21
            $this->loadTemplate("@SyliusShop/Cart/Summary/_items.html.twig", "SyliusShopBundle:Cart:summary.html.twig", 21)->display($context);
            // line 22
            echo "
                ";
            // line 23
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.after_items", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 23, $this->source); })())]]);
            echo "
            </div>
            <div class=\"col-12 col-lg-4\">
                ";
            // line 26
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.before_totals", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 26, $this->source); })())]]);
            echo "

                ";
            // line 28
            $this->loadTemplate("@SyliusShop/Cart/Summary/_totals.html.twig", "SyliusShopBundle:Cart:summary.html.twig", 28)->display($context);
            // line 29
            echo "
                ";
            // line 30
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.after_totals", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 30, $this->source); })())]]);
            echo "

                ";
            // line 32
            $this->loadTemplate("@SyliusShop/Cart/Summary/_checkout.html.twig", "SyliusShopBundle:Cart:summary.html.twig", 32)->display($context);
            // line 33
            echo "            </div>
        </div>

        ";
            // line 36
            echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.before_suggestions", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 36, $this->source); })())]]);
            echo "

        ";
            // line 38
            $this->loadTemplate("@SyliusShop/Cart/Summary/_suggestions.html.twig", "SyliusShopBundle:Cart:summary.html.twig", 38)->display($context);
            // line 39
            echo "    ";
        } else {
            // line 40
            echo "        ";
            echo twig_call_macro($macros["messages"], "macro_info", ["sylius.ui.your_cart_is_empty"], 40, $context, $this->getSourceContext());
            echo "
    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Cart:summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 40,  143 => 39,  141 => 38,  136 => 36,  131 => 33,  129 => 32,  124 => 30,  121 => 29,  119 => 28,  114 => 26,  108 => 23,  105 => 22,  103 => 21,  98 => 19,  91 => 15,  88 => 14,  85 => 13,  82 => 12,  75 => 11,  60 => 9,  52 => 1,  50 => 7,  48 => 5,  46 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% form_theme form '@SyliusShop/Form/theme.html.twig' %}

{% import '@SyliusShop/Common/Macro/messages.html.twig' as messages %}

{% set header = 'sylius.ui.your_shopping_cart' %}

{% block title %}{{ parent() }} | {{ header|trans }}{% endblock %}

{% block content %}
    {% if not cart.empty %}
        {% include '@SyliusShop/Cart/Summary/_header.html.twig' %}

        {{ sonata_block_render_event('sylius.shop.cart.summary.after_content_header', {'cart': cart}) }}

        <div class=\"row mb-5\">
            <div class=\"col-12 col-lg-8 mb-4\">
                {{ sonata_block_render_event('sylius.shop.cart.summary.before_items', {'cart': cart}) }}

                {% include '@SyliusShop/Cart/Summary/_items.html.twig' %}

                {{ sonata_block_render_event('sylius.shop.cart.summary.after_items', {'cart': cart}) }}
            </div>
            <div class=\"col-12 col-lg-4\">
                {{ sonata_block_render_event('sylius.shop.cart.summary.before_totals', {'cart': cart}) }}

                {% include '@SyliusShop/Cart/Summary/_totals.html.twig' %}

                {{ sonata_block_render_event('sylius.shop.cart.summary.after_totals', {'cart': cart}) }}

                {% include '@SyliusShop/Cart/Summary/_checkout.html.twig' %}
            </div>
        </div>

        {{ sonata_block_render_event('sylius.shop.cart.summary.before_suggestions', {'cart': cart}) }}

        {% include '@SyliusShop/Cart/Summary/_suggestions.html.twig' %}
    {% else %}
        {{ messages.info('sylius.ui.your_cart_is_empty') }}
    {% endif %}
{% endblock %}
", "SyliusShopBundle:Cart:summary.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Cart/summary.html.twig");
    }
}
