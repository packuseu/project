<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusFilterPlugin:layouts:javascripts.html.twig */
class __TwigTemplate_11496dd91d73d0e2682ca8ffa429aed8c055444007d3b9501db6bac8be3db472 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusFilterPlugin:layouts:javascripts.html.twig"));

        // line 1
        echo "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/10.1.0/nouislider.min.js\"></script>

<script>
    var slider = document.getElementById('slider');
    var lower = document.getElementById('criteria_price_lower');
    var upper = document.getElementById('criteria_price_upper');

    noUiSlider.create(slider, {
        start: [
            Number(lower.value),
            Number(upper.value)
        ],
        connect: true,
        tooltips: true,
        range: {
            'min': Number(lower.getAttribute('data-lower')),
            'max': Number(upper.getAttribute('data-upper'))
        }
    });

    slider.noUiSlider.on('update', function( values, handle ) {
        var value = values[handle];

        if ( handle ) {
            upper.value = value;
        } else {
            lower.value = value;
        }
    });
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusFilterPlugin:layouts:javascripts.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/noUiSlider/10.1.0/nouislider.min.js\"></script>

<script>
    var slider = document.getElementById('slider');
    var lower = document.getElementById('criteria_price_lower');
    var upper = document.getElementById('criteria_price_upper');

    noUiSlider.create(slider, {
        start: [
            Number(lower.value),
            Number(upper.value)
        ],
        connect: true,
        tooltips: true,
        range: {
            'min': Number(lower.getAttribute('data-lower')),
            'max': Number(upper.getAttribute('data-upper'))
        }
    });

    slider.noUiSlider.on('update', function( values, handle ) {
        var value = values[handle];

        if ( handle ) {
            upper.value = value;
        } else {
            lower.value = value;
        }
    });
</script>
", "OmniSyliusFilterPlugin:layouts:javascripts.html.twig", "/var/www/html/vendor/omni/sylius-filter-plugin/src/Resources/views/layouts/javascripts.html.twig");
    }
}
