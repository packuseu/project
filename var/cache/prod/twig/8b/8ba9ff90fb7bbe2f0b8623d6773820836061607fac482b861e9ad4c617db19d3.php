<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Checkout:_steps.html.twig */
class __TwigTemplate_f82dc808f2e2591ad51f53aa9559e4b82b833ff3b076f82b2e211fb97c89d788 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Checkout:_steps.html.twig"));

        // line 1
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Checkout:_steps.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if (( !array_key_exists("active", $context) || ((isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new RuntimeError('Variable "active" does not exist.', 3, $this->source); })()) == "address"))) {
            // line 4
            echo "    ";
            $context["steps"] = ["address" => "active", "select_shipping" => "disabled", "select_payment" => "disabled", "complete" => "disabled"];
        } elseif ((        // line 5
(isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new RuntimeError('Variable "active" does not exist.', 5, $this->source); })()) == "select_shipping")) {
            // line 6
            echo "    ";
            $context["steps"] = ["address" => "completed", "select_shipping" => "active", "select_payment" => "disabled", "complete" => "disabled"];
        } elseif ((        // line 7
(isset($context["active"]) || array_key_exists("active", $context) ? $context["active"] : (function () { throw new RuntimeError('Variable "active" does not exist.', 7, $this->source); })()) == "select_payment")) {
            // line 8
            echo "    ";
            $context["steps"] = ["address" => "completed", "select_shipping" => "completed", "select_payment" => "active", "complete" => "disabled"];
        } else {
            // line 10
            echo "    ";
            $context["steps"] = ["address" => "completed", "select_shipping" => "completed", "select_payment" => "completed", "complete" => "active"];
        }
        // line 12
        echo "
";
        // line 13
        $context["order_requires_payment"] = call_user_func_array($this->env->getFunction('sylius_is_payment_required')->getCallable(), [(isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 13, $this->source); })())]);
        // line 14
        $context["order_requires_shipping"] = call_user_func_array($this->env->getFunction('sylius_is_shipping_required')->getCallable(), [(isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 14, $this->source); })())]);
        // line 15
        echo "
";
        // line 16
        $context["steps_count"] = "four";
        // line 17
        if (( !(isset($context["order_requires_payment"]) || array_key_exists("order_requires_payment", $context) ? $context["order_requires_payment"] : (function () { throw new RuntimeError('Variable "order_requires_payment" does not exist.', 17, $this->source); })()) &&  !(isset($context["order_requires_shipping"]) || array_key_exists("order_requires_shipping", $context) ? $context["order_requires_shipping"] : (function () { throw new RuntimeError('Variable "order_requires_shipping" does not exist.', 17, $this->source); })()))) {
            // line 18
            echo "    ";
            $context["steps_count"] = "two";
        } elseif (( !        // line 19
(isset($context["order_requires_payment"]) || array_key_exists("order_requires_payment", $context) ? $context["order_requires_payment"] : (function () { throw new RuntimeError('Variable "order_requires_payment" does not exist.', 19, $this->source); })()) ||  !(isset($context["order_requires_shipping"]) || array_key_exists("order_requires_shipping", $context) ? $context["order_requires_shipping"] : (function () { throw new RuntimeError('Variable "order_requires_shipping" does not exist.', 19, $this->source); })()))) {
            // line 20
            echo "    ";
            $context["steps_count"] = "three";
        }
        // line 22
        echo "
<div class=\"steps mb-5 ";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["steps_count"]) || array_key_exists("steps_count", $context) ? $context["steps_count"] : (function () { throw new RuntimeError('Variable "steps_count" does not exist.', 23, $this->source); })()), "html", null, true);
        echo "\">
    <a class=\"steps-item ";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["steps"]) || array_key_exists("steps", $context) ? $context["steps"] : (function () { throw new RuntimeError('Variable "steps" does not exist.', 24, $this->source); })()), "address", [], "array", false, false, false, 24), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_checkout_address");
        echo "\">
        <div class=\"steps-icon\">";
        // line 25
        echo twig_call_macro($macros["icons"], "macro_address", [], 25, $context, $this->getSourceContext());
        echo "</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.address"), "html", null, true);
        echo "</h6>
            <div class=\"steps-text\">";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.fill_in_your_billing_and_shipping_addresses"), "html", null, true);
        echo "</div>
        </div>
    </a>
    ";
        // line 31
        if ((isset($context["order_requires_shipping"]) || array_key_exists("order_requires_shipping", $context) ? $context["order_requires_shipping"] : (function () { throw new RuntimeError('Variable "order_requires_shipping" does not exist.', 31, $this->source); })())) {
            // line 32
            echo "    <a class=\"steps-item ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["steps"]) || array_key_exists("steps", $context) ? $context["steps"] : (function () { throw new RuntimeError('Variable "steps" does not exist.', 32, $this->source); })()), "select_shipping", [], "array", false, false, false, 32), "html", null, true);
            echo "\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_checkout_select_shipping");
            echo "\">
        <div class=\"steps-icon\">";
            // line 33
            echo twig_call_macro($macros["icons"], "macro_transport", [], 33, $context, $this->getSourceContext());
            echo "</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">";
            // line 35
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping"), "html", null, true);
            echo "</h6>
            <div class=\"steps-text\">";
            // line 36
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.choose_how_your_goods_will_be_delivered"), "html", null, true);
            echo "</div>
        </div>
    </a>
    ";
        }
        // line 40
        echo "    ";
        if ((isset($context["order_requires_payment"]) || array_key_exists("order_requires_payment", $context) ? $context["order_requires_payment"] : (function () { throw new RuntimeError('Variable "order_requires_payment" does not exist.', 40, $this->source); })())) {
            // line 41
            echo "    <a class=\"steps-item ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["steps"]) || array_key_exists("steps", $context) ? $context["steps"] : (function () { throw new RuntimeError('Variable "steps" does not exist.', 41, $this->source); })()), "select_payment", [], "array", false, false, false, 41), "html", null, true);
            echo "\" href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_checkout_select_payment");
            echo "\">
        <div class=\"steps-icon\">";
            // line 42
            echo twig_call_macro($macros["icons"], "macro_creditCard", [], 42, $context, $this->getSourceContext());
            echo "</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.payment"), "html", null, true);
            echo "</h6>
            <div class=\"steps-text\">";
            // line 45
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.choose_how_you_will_pay"), "html", null, true);
            echo "</div>
        </div>
    </a>
    ";
        }
        // line 49
        echo "    <a class=\"steps-item ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["steps"]) || array_key_exists("steps", $context) ? $context["steps"] : (function () { throw new RuntimeError('Variable "steps" does not exist.', 49, $this->source); })()), "complete", [], "array", false, false, false, 49), "html", null, true);
        echo "\" href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_checkout_complete");
        echo "\">
        <div class=\"steps-icon\">";
        // line 50
        echo twig_call_macro($macros["icons"], "macro_flag", [], 50, $context, $this->getSourceContext());
        echo "</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.complete"), "html", null, true);
        echo "</h6>
            <div class=\"steps-text\">";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.review_and_confirm_your_order"), "html", null, true);
        echo "</div>
        </div>
    </a>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Checkout:_steps.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 53,  179 => 52,  174 => 50,  167 => 49,  160 => 45,  156 => 44,  151 => 42,  144 => 41,  141 => 40,  134 => 36,  130 => 35,  125 => 33,  118 => 32,  116 => 31,  110 => 28,  106 => 27,  101 => 25,  95 => 24,  91 => 23,  88 => 22,  84 => 20,  82 => 19,  79 => 18,  77 => 17,  75 => 16,  72 => 15,  70 => 14,  68 => 13,  65 => 12,  61 => 10,  57 => 8,  55 => 7,  52 => 6,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% if active is not defined or active == 'address' %}
    {% set steps = {'address': 'active', 'select_shipping': 'disabled', 'select_payment': 'disabled', 'complete': 'disabled'} %}
{% elseif active == 'select_shipping' %}
    {% set steps = {'address': 'completed', 'select_shipping': 'active', 'select_payment': 'disabled', 'complete': 'disabled'} %}
{% elseif active == 'select_payment' %}
    {% set steps = {'address': 'completed', 'select_shipping': 'completed', 'select_payment': 'active', 'complete': 'disabled'} %}
{% else %}
    {% set steps = {'address': 'completed', 'select_shipping': 'completed', 'select_payment': 'completed', 'complete': 'active'} %}
{% endif %}

{% set order_requires_payment = sylius_is_payment_required(order) %}
{% set order_requires_shipping = sylius_is_shipping_required(order) %}

{% set steps_count = 'four' %}
{% if not order_requires_payment and not order_requires_shipping %}
    {% set steps_count = 'two' %}
{% elseif not order_requires_payment or not order_requires_shipping %}
    {% set steps_count = 'three' %}
{% endif %}

<div class=\"steps mb-5 {{ steps_count }}\">
    <a class=\"steps-item {{ steps['address'] }}\" href=\"{{ path('sylius_shop_checkout_address') }}\">
        <div class=\"steps-icon\">{{ icons.address() }}</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">{{ 'sylius.ui.address'|trans }}</h6>
            <div class=\"steps-text\">{{ 'sylius.ui.fill_in_your_billing_and_shipping_addresses'|trans }}</div>
        </div>
    </a>
    {% if order_requires_shipping %}
    <a class=\"steps-item {{ steps['select_shipping'] }}\" href=\"{{ path('sylius_shop_checkout_select_shipping') }}\">
        <div class=\"steps-icon\">{{ icons.transport() }}</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">{{ 'sylius.ui.shipping'|trans }}</h6>
            <div class=\"steps-text\">{{ 'sylius.ui.choose_how_your_goods_will_be_delivered'|trans }}</div>
        </div>
    </a>
    {% endif %}
    {% if order_requires_payment %}
    <a class=\"steps-item {{ steps['select_payment'] }}\" href=\"{{ path('sylius_shop_checkout_select_payment') }}\">
        <div class=\"steps-icon\">{{ icons.creditCard() }}</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">{{ 'sylius.ui.payment'|trans }}</h6>
            <div class=\"steps-text\">{{ 'sylius.ui.choose_how_you_will_pay'|trans }}</div>
        </div>
    </a>
    {% endif %}
    <a class=\"steps-item {{ steps['complete'] }}\" href=\"{{ path('sylius_shop_checkout_complete') }}\">
        <div class=\"steps-icon\">{{ icons.flag() }}</div>
        <div class=\"steps-content\">
            <h6 class=\"steps-title\">{{ 'sylius.ui.complete'|trans }}</h6>
            <div class=\"steps-text\">{{ 'sylius.ui.review_and_confirm_your_order'|trans }}</div>
        </div>
    </a>
</div>
", "SyliusShopBundle:Checkout:_steps.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Checkout/_steps.html.twig");
    }
}
