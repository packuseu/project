<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusCmsPlugin::_javascripts.html.twig */
class __TwigTemplate_03deefe5d8d36bf71a3a1c3b430f800fd451b87e620fa4c6e0969203260ca529 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusCmsPlugin::_javascripts.html.twig"));

        // line 1
        if (twig_in_filter("omni_sylius_admin_node", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1))) {
            // line 2
            echo "    ";
            // line 3
            echo "    <script src=\"https://cdn.ckeditor.com/4.11.1/full/ckeditor.js\"></script>

    <script type=\"text/javascript\">
        CKEDITOR.config.removeButtons = 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,' +
            'PasteFromWord,Redo,Undo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,' +
            'Button,HiddenField,ImageButton,CopyFormatting,Outdent,Indent,Blockquote,CreateDiv,JustifyLeft,' +
            'JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Link,Unlink,Anchor,Flash,Table,' +
            'HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Maximize,ShowBlocks,About';

        CKEDITOR.config.entities_latin = false;

        var editors = document.querySelectorAll('.ckeditor');

        for (i = 0; i < editors.length; i++) {
            CKEDITOR.replace(editors[i].getAttribute('id'), {
                ";
            // line 18
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 18, $this->source); })()), "request", [], "any", false, false, false, 18), "get", [0 => "browseUrl"], "method", false, false, false, 18) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 18, $this->source); })()), "request", [], "any", false, false, false, 18), "get", [0 => "uploadUrl"], "method", false, false, false, 18))) {
                // line 19
                echo "                filebrowserBrowseUrl: \"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 19, $this->source); })()), "request", [], "any", false, false, false, 19), "get", [0 => "browseUrl"], "method", false, false, false, 19), "html", null, true);
                echo "\",
                filebrowserUploadUrl: \"";
                // line 20
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 20, $this->source); })()), "request", [], "any", false, false, false, 20), "get", [0 => "uploadUrl"], "method", false, false, false, 20), "html", null, true);
                echo "\",
                ";
            }
            // line 22
            echo "                allowedContent: true,
                filebrowserImageUploadUrl: \"";
            // line 23
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_upload_image");
            echo "\",
            });
        }


        function resetDropdown(\$relationTypeSelector) {
            var \$relationSelector = \$relationTypeSelector.parent().parent().find('.sylius-autocomplete');
            var resource = \$relationTypeSelector.find('option:selected').val();
            var url = \$relationSelector.data('url').substr(0, \$relationSelector.data('url').indexOf('?')) + '?type=' + resource;
            \$relationSelector.attr('data-url', url);
            \$relationSelector.data('url', url);
            \$relationSelector.dropdown();

            \$relationSelector.dropdown({
                delay: {
                    search: 250,
                },
                forceSelection: false,
                apiSettings: {
                    url: url,
                    dataType: 'JSON',
                    cache: false,
                    beforeSend: function(settings) {
                        /* eslint-disable-next-line no-param-reassign */
                        settings.data[\$relationSelector.data('criteria-name')] = settings.urlData.query;

                        return settings;
                    },
                    onResponse: function(response) {
                        return {
                            success: true,
                            results: response.map(function(item) {
                                return {
                                    name: item[\$relationSelector.data('choice-name')],
                                    value: item[\$relationSelector.data('choice-value')],
                                }
                            }),
                        };
                    },
                },
            });
        }
    </script>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusCmsPlugin::_javascripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 23,  73 => 22,  68 => 20,  63 => 19,  61 => 18,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if 'omni_sylius_admin_node' in app.request.get('_route') %}
    {# This javascript is used to remove unnecessary CKEditor tools from https://cdn.ckeditor.com/4.11.1/full/ckeditor.js #}
    <script src=\"https://cdn.ckeditor.com/4.11.1/full/ckeditor.js\"></script>

    <script type=\"text/javascript\">
        CKEDITOR.config.removeButtons = 'Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,' +
            'PasteFromWord,Redo,Undo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,' +
            'Button,HiddenField,ImageButton,CopyFormatting,Outdent,Indent,Blockquote,CreateDiv,JustifyLeft,' +
            'JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Link,Unlink,Anchor,Flash,Table,' +
            'HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Maximize,ShowBlocks,About';

        CKEDITOR.config.entities_latin = false;

        var editors = document.querySelectorAll('.ckeditor');

        for (i = 0; i < editors.length; i++) {
            CKEDITOR.replace(editors[i].getAttribute('id'), {
                {% if app.request.get('browseUrl') and app.request.get('uploadUrl') %}
                filebrowserBrowseUrl: \"{{ app.request.get('browseUrl') }}\",
                filebrowserUploadUrl: \"{{ app.request.get('uploadUrl') }}\",
                {% endif %}
                allowedContent: true,
                filebrowserImageUploadUrl: \"{{ path('omni_sylius_admin_upload_image') }}\",
            });
        }


        function resetDropdown(\$relationTypeSelector) {
            var \$relationSelector = \$relationTypeSelector.parent().parent().find('.sylius-autocomplete');
            var resource = \$relationTypeSelector.find('option:selected').val();
            var url = \$relationSelector.data('url').substr(0, \$relationSelector.data('url').indexOf('?')) + '?type=' + resource;
            \$relationSelector.attr('data-url', url);
            \$relationSelector.data('url', url);
            \$relationSelector.dropdown();

            \$relationSelector.dropdown({
                delay: {
                    search: 250,
                },
                forceSelection: false,
                apiSettings: {
                    url: url,
                    dataType: 'JSON',
                    cache: false,
                    beforeSend: function(settings) {
                        /* eslint-disable-next-line no-param-reassign */
                        settings.data[\$relationSelector.data('criteria-name')] = settings.urlData.query;

                        return settings;
                    },
                    onResponse: function(response) {
                        return {
                            success: true,
                            results: response.map(function(item) {
                                return {
                                    name: item[\$relationSelector.data('choice-name')],
                                    value: item[\$relationSelector.data('choice-value')],
                                }
                            }),
                        };
                    },
                },
            });
        }
    </script>
{% endif %}
", "OmniSyliusCmsPlugin::_javascripts.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/_javascripts.html.twig");
    }
}
