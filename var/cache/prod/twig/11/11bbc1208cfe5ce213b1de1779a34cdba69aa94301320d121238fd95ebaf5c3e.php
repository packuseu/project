<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @FOSSyliusImportExportPlugin/Crud/import_form.html.twig */
class __TwigTemplate_d8929ae652ea2c9118f466ce5b28771de46d0947f457daf2f92e16fe377282cb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@FOSSyliusImportExportPlugin/Crud/import_form.html.twig"));

        // line 1
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), [0 => "@FOSSyliusImportExportPlugin/Form/theme.html.twig"], true);
        // line 2
        $context["header"] = ("sylius.ui." . (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 2, $this->source); })()));
        // line 3
        echo "
<div class=\"ui styled fluid accordion\">
    <div class=\"title active\">
        <i class=\"dropdown icon\"></i>
        ";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fos.import_export.ui.import"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 7, $this->source); })())), "html", null, true);
        echo "
    </div>
    <div class=\"content active\">
        ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_import_data", ["resource" =>         // line 11
(isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 11, $this->source); })())]), "method" => "POST", "attr" => ["class" => "ui loadable form", "novalidate" => "novalidate"]]);
        // line 14
        echo "
        <div class=\"ui segment\">
            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), 'widget');
        echo "
        </div>
        <button class=\"ui icon primary button\" type=\"submit\" id=\"import-data\">
            ";
        // line 19
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 19, $this->source); })())), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("fos.import_export.ui.import"), "html", null, true);
        echo "
        </button>
        ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), 'form_end');
        echo "
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@FOSSyliusImportExportPlugin/Crud/import_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 21,  71 => 19,  65 => 16,  61 => 14,  59 => 11,  58 => 10,  50 => 7,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% form_theme form '@FOSSyliusImportExportPlugin/Form/theme.html.twig' %}
{% set header = 'sylius.ui.'~resource %}

<div class=\"ui styled fluid accordion\">
    <div class=\"title active\">
        <i class=\"dropdown icon\"></i>
        {{ 'fos.import_export.ui.import'|trans }} {{ header|trans }}
    </div>
    <div class=\"content active\">
        {{ form_start(form, {
            'action': path('app_import_data',  {'resource': resource}),
            'method': 'POST',
            'attr': {'class': 'ui loadable form', 'novalidate': 'novalidate'}
        }) }}
        <div class=\"ui segment\">
            {{ form_widget(form) }}
        </div>
        <button class=\"ui icon primary button\" type=\"submit\" id=\"import-data\">
            {{ resource|capitalize }} {{ 'fos.import_export.ui.import'|trans }}
        </button>
        {{ form_end(form) }}
    </div>
</div>
", "@FOSSyliusImportExportPlugin/Crud/import_form.html.twig", "/var/www/html/vendor/friendsofsylius/sylius-import-export-plugin/src/Resources/views/Crud/import_form.html.twig");
    }
}
