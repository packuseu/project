<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Brille24SyliusTierPricePlugin:Admin:_javascripts.html.twig */
class __TwigTemplate_83779c4712b10c3bd77a27ae6cecf52335348e58c7d26513759b1849bd400500 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Brille24SyliusTierPricePlugin:Admin:_javascripts.html.twig"));

        // line 2
        $this->loadTemplate("SyliusUiBundle::_javascripts.html.twig", "Brille24SyliusTierPricePlugin:Admin:_javascripts.html.twig", 2)->display(twig_array_merge($context, ["path" => "bundles/brille24syliustierpriceplugin/js/tierPrices/itemCollectionRender.js"]));
        // line 3
        $this->loadTemplate("SyliusUiBundle::_javascripts.html.twig", "Brille24SyliusTierPricePlugin:Admin:_javascripts.html.twig", 3)->display(twig_array_merge($context, ["path" => "bundles/brille24syliustierpriceplugin/js/jQueryElementSort.js"]));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Brille24SyliusTierPricePlugin:Admin:_javascripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 3,  40 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# Including javascript #}
{% include 'SyliusUiBundle::_javascripts.html.twig' with {path: 'bundles/brille24syliustierpriceplugin/js/tierPrices/itemCollectionRender.js'} %}
{% include 'SyliusUiBundle::_javascripts.html.twig' with {path: 'bundles/brille24syliustierpriceplugin/js/jQueryElementSort.js'} %}
", "Brille24SyliusTierPricePlugin:Admin:_javascripts.html.twig", "/var/www/html/vendor/brille24/sylius-tierprice-plugin/src/Resources/views/Admin/_javascripts.html.twig");
    }
}
