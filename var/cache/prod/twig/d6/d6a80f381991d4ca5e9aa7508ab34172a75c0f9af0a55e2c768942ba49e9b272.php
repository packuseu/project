<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusBannerPlugin:PositionTypes:_half_screen_adoptive_grid.html.twig */
class __TwigTemplate_c335bc5d929570df07eaf0771353276c578f4d2e428e4ed184ff75a46187ebd1 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusBannerPlugin:PositionTypes:_half_screen_adoptive_grid.html.twig"));

        // line 7
        echo "
";
        // line 17
        echo "
";
        // line 41
        echo "
";
        // line 65
        echo "
";
        // line 96
        echo "
";
        // line 97
        $macros["row"] = $this->macros["row"] = $this;
        // line 98
        echo "
";
        // line 99
        $context["imagesCounter"] = 0;
        // line 100
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 100, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 100, $this->source); })()), "channel", [], "array", false, false, false, 100)], "method", false, false, false, 100)) {
                // line 101
                echo "    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 101));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 102
                    echo "        ";
                    $context["imagesCounter"] = ((isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 102, $this->source); })()) + 1);
                    // line 103
                    echo "    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 105
        echo "
";
        // line 106
        if ((isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 106, $this->source); })())) {
            // line 107
            echo "    <div class=\"col-12 col-md-6\">
        <div class=\"row\">
            ";
            // line 109
            $context["counter"] = 0;
            // line 110
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 110, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 110, $this->source); })()), "channel", [], "array", false, false, false, 110)], "method", false, false, false, 110)) {
                    // line 111
                    echo "                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 111));
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        // line 112
                        echo "                    ";
                        $context["counter"] = ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 112, $this->source); })()) + 1);
                        // line 113
                        echo "
                    ";
                        // line 114
                        if (((isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 114, $this->source); })()) == 1)) {
                            // line 115
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_one_col", [$context["image"]], 115, $context, $this->getSourceContext());
                            echo "
                    ";
                        } elseif ((                        // line 116
(isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 116, $this->source); })()) == 2)) {
                            // line 117
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_two_cols", [$context["image"], false], 117, $context, $this->getSourceContext());
                            echo "
                    ";
                        } elseif ((                        // line 118
(isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 118, $this->source); })()) == 3)) {
                            // line 119
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_three_cols", [$context["image"], (isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 119, $this->source); })())], 119, $context, $this->getSourceContext());
                            echo "
                    ";
                        } elseif (((                        // line 120
(isset($context["imagesCounter"]) || array_key_exists("imagesCounter", $context) ? $context["imagesCounter"] : (function () { throw new RuntimeError('Variable "imagesCounter" does not exist.', 120, $this->source); })()) > 3) && ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 120, $this->source); })()) < 5))) {
                            // line 121
                            echo "                        ";
                            echo twig_call_macro($macros["row"], "macro_two_cols", [$context["image"], true], 121, $context, $this->getSourceContext());
                            echo "
                    ";
                        }
                        // line 123
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 124
                    echo "            ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 125
            echo "        </div>
    </div>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function macro_card_img($__imgPath__ = null, $__small__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "imgPath" => $__imgPath__,
            "small" => $__small__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "card_img"));

            // line 3
            echo "    <div class=\"card-bg-img ";
            if ((isset($context["small"]) || array_key_exists("small", $context) ? $context["small"] : (function () { throw new RuntimeError('Variable "small" does not exist.', 3, $this->source); })())) {
                echo "bg--small";
            }
            echo "\"
         style=\"background-image: url(";
            // line 4
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter((isset($context["imgPath"]) || array_key_exists("imgPath", $context) ? $context["imgPath"] : (function () { throw new RuntimeError('Variable "imgPath" does not exist.', 4, $this->source); })()), "omni_sylius_banner"), "html", null, true);
            echo ");\"
    ></div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 9
    public function macro_card_body($__content__ = null, $__contentBackground__ = null, $__size__ = null, $__contentPosition__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "content" => $__content__,
            "contentBackground" => $__contentBackground__,
            "size" => $__size__,
            "contentPosition" => $__contentPosition__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "card_body"));

            // line 10
            echo "    <div class=\"card-body body--";
            echo twig_escape_filter($this->env, (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 10, $this->source); })()), "html", null, true);
            echo "\"
         style=\"";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 11, $this->source); })()), "html", null, true);
            echo " background-color: ";
            echo twig_escape_filter($this->env, (isset($context["contentBackground"]) || array_key_exists("contentBackground", $context) ? $context["contentBackground"] : (function () { throw new RuntimeError('Variable "contentBackground" does not exist.', 11, $this->source); })()), "html", null, true);
            echo "\"
    >
        ";
            // line 14
            echo "        ";
            echo (isset($context["content"]) || array_key_exists("content", $context) ? $context["content"] : (function () { throw new RuntimeError('Variable "content" does not exist.', 14, $this->source); })());
            echo "
    </div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 19
    public function macro_one_col($__image__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "image" => $__image__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "one_col"));

            // line 20
            echo "    ";
            $macros["card"] = $this;
            // line 21
            echo "
    <div class=\"col-12\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"";
            // line 24
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 24), "link", [], "any", true, true, false, 24)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 24), "link", [], "any", false, false, false, 24), "javascript:;")) : ("javascript:;")), "html", null, true);
            echo "\">
                ";
            // line 25
            echo twig_call_macro($macros["card"], "macro_card_img", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 25, $this->source); })()), "path", [], "any", false, false, false, 25), false], 25, $context, $this->getSourceContext());
            echo "

                ";
            // line 28
            echo "                ";
            $context["size"] = (("height: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 28, $this->source); })()), "contentSpace", [], "any", false, false, false, 28))) . "%;");
            // line 29
            echo "                ";
            $context["contentPosition"] = twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 29, $this->source); })()), "contentPosition", [], "any", false, false, false, 29);
            // line 30
            echo "
                ";
            // line 32
            echo "                ";
            if ((((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 32, $this->source); })()) == "left") || ((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 32, $this->source); })()) == "right"))) {
                // line 33
                echo "                    ";
                $context["size"] = (("width: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 33, $this->source); })()), "contentSpace", [], "any", false, false, false, 33))) . "%;");
                // line 34
                echo "                ";
            }
            // line 35
            echo "
                ";
            // line 36
            echo twig_call_macro($macros["card"], "macro_card_body", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 36, $this->source); })()), "content", [], "any", false, false, false, 36), twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 36, $this->source); })()), "contentBackground", [], "any", false, false, false, 36), (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 36, $this->source); })()), (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 36, $this->source); })())], 36, $context, $this->getSourceContext());
            echo "
            </a>
        </div>
    </div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 43
    public function macro_two_cols($__image__ = null, $__isSmallImg__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "image" => $__image__,
            "isSmallImg" => $__isSmallImg__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "two_cols"));

            // line 44
            echo "    ";
            $macros["card"] = $this;
            // line 45
            echo "
    <div class=\"col-6\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"";
            // line 48
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 48), "link", [], "any", true, true, false, 48)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 48), "link", [], "any", false, false, false, 48), "javascript:;")) : ("javascript:;")), "html", null, true);
            echo "\">
                ";
            // line 49
            echo twig_call_macro($macros["card"], "macro_card_img", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 49, $this->source); })()), "path", [], "any", false, false, false, 49), (isset($context["isSmallImg"]) || array_key_exists("isSmallImg", $context) ? $context["isSmallImg"] : (function () { throw new RuntimeError('Variable "isSmallImg" does not exist.', 49, $this->source); })())], 49, $context, $this->getSourceContext());
            echo "

                ";
            // line 52
            echo "                ";
            $context["size"] = (("height: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 52, $this->source); })()), "contentSpace", [], "any", false, false, false, 52))) . "%;");
            // line 53
            echo "                ";
            $context["contentPosition"] = twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 53, $this->source); })()), "contentPosition", [], "any", false, false, false, 53);
            // line 54
            echo "
                ";
            // line 56
            echo "                ";
            if ((((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 56, $this->source); })()) == "left") || ((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 56, $this->source); })()) == "right"))) {
                // line 57
                echo "                    ";
                $context["contentPosition"] = "bottom";
                // line 58
                echo "                ";
            }
            // line 59
            echo "
                ";
            // line 60
            echo twig_call_macro($macros["card"], "macro_card_body", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 60, $this->source); })()), "content", [], "any", false, false, false, 60), twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 60, $this->source); })()), "contentBackground", [], "any", false, false, false, 60), (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 60, $this->source); })()), (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 60, $this->source); })())], 60, $context, $this->getSourceContext());
            echo "
            </a>
        </div>
    </div>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 67
    public function macro_three_cols($__image__ = null, $__counter__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "image" => $__image__,
            "counter" => $__counter__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "three_cols"));

            // line 68
            echo "    ";
            $macros["card"] = $this;
            // line 69
            echo "
    ";
            // line 70
            if ((((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 70, $this->source); })()) == 1) || ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 70, $this->source); })()) == 2))) {
                // line 71
                echo "        <div class=\"col-6\">
    ";
            }
            // line 73
            echo "
    <div class=\"card\">
        <a class=\"card-link\" href=\"";
            // line 75
            echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 75), "link", [], "any", true, true, false, 75)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["image"] ?? null), "translation", [], "any", false, true, false, 75), "link", [], "any", false, false, false, 75), "javascript:;")) : ("javascript:;")), "html", null, true);
            echo "\">
            ";
            // line 76
            $context["isSmall"] = (((((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 76, $this->source); })()) == 2) || ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 76, $this->source); })()) == 3))) ? (true) : (false));
            // line 77
            echo "            ";
            echo twig_call_macro($macros["card"], "macro_card_img", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 77, $this->source); })()), "path", [], "any", false, false, false, 77), (isset($context["isSmall"]) || array_key_exists("isSmall", $context) ? $context["isSmall"] : (function () { throw new RuntimeError('Variable "isSmall" does not exist.', 77, $this->source); })())], 77, $context, $this->getSourceContext());
            echo "

            ";
            // line 80
            echo "            ";
            $context["size"] = (("height: " . twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 80, $this->source); })()), "contentSpace", [], "any", false, false, false, 80))) . "%;");
            // line 81
            echo "            ";
            $context["contentPosition"] = twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 81, $this->source); })()), "contentPosition", [], "any", false, false, false, 81);
            // line 82
            echo "
            ";
            // line 84
            echo "            ";
            if ((((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 84, $this->source); })()) == "left") || ((isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 84, $this->source); })()) == "right"))) {
                // line 85
                echo "                ";
                $context["contentPosition"] = "bottom";
                // line 86
                echo "            ";
            }
            // line 87
            echo "
            ";
            // line 88
            echo twig_call_macro($macros["card"], "macro_card_body", [twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 88, $this->source); })()), "content", [], "any", false, false, false, 88), twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 88, $this->source); })()), "contentBackground", [], "any", false, false, false, 88), (isset($context["size"]) || array_key_exists("size", $context) ? $context["size"] : (function () { throw new RuntimeError('Variable "size" does not exist.', 88, $this->source); })()), (isset($context["contentPosition"]) || array_key_exists("contentPosition", $context) ? $context["contentPosition"] : (function () { throw new RuntimeError('Variable "contentPosition" does not exist.', 88, $this->source); })())], 88, $context, $this->getSourceContext());
            echo "
        </a>
    </div>

    ";
            // line 92
            if ((((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 92, $this->source); })()) == 1) || ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 92, $this->source); })()) == 3))) {
                // line 93
                echo "        </div>
    ";
            }
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "OmniSyliusBannerPlugin:PositionTypes:_half_screen_adoptive_grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  470 => 93,  468 => 92,  461 => 88,  458 => 87,  455 => 86,  452 => 85,  449 => 84,  446 => 82,  443 => 81,  440 => 80,  434 => 77,  432 => 76,  428 => 75,  424 => 73,  420 => 71,  418 => 70,  415 => 69,  412 => 68,  395 => 67,  378 => 60,  375 => 59,  372 => 58,  369 => 57,  366 => 56,  363 => 54,  360 => 53,  357 => 52,  352 => 49,  348 => 48,  343 => 45,  340 => 44,  323 => 43,  306 => 36,  303 => 35,  300 => 34,  297 => 33,  294 => 32,  291 => 30,  288 => 29,  285 => 28,  280 => 25,  276 => 24,  271 => 21,  268 => 20,  252 => 19,  236 => 14,  229 => 11,  224 => 10,  205 => 9,  190 => 4,  183 => 3,  166 => 2,  156 => 125,  149 => 124,  143 => 123,  137 => 121,  135 => 120,  130 => 119,  128 => 118,  123 => 117,  121 => 116,  116 => 115,  114 => 114,  111 => 113,  108 => 112,  103 => 111,  97 => 110,  95 => 109,  91 => 107,  89 => 106,  86 => 105,  75 => 103,  72 => 102,  67 => 101,  62 => 100,  60 => 99,  57 => 98,  55 => 97,  52 => 96,  49 => 65,  46 => 41,  43 => 17,  40 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("{# render card image #}
{% macro card_img(imgPath, small) %}
    <div class=\"card-bg-img {% if small %}bg--small{% endif %}\"
         style=\"background-image: url({{ imgPath|imagine_filter('omni_sylius_banner') }});\"
    ></div>
{% endmacro %}

{# render card body #}
{% macro card_body(content, contentBackground, size, contentPosition) %}
    <div class=\"card-body body--{{ contentPosition }}\"
         style=\"{{ size }} background-color: {{ contentBackground }}\"
    >
        {#<h5 class=\"card-title\">{{ content|raw }}</h5>#}
        {{ content|raw }}
    </div>
{% endmacro %}

{# render grid col-12 #}
{% macro one_col(image) %}
    {% import _self as card %}

    <div class=\"col-12\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                {{ card.card_img(image.path, false) }}

                {# if position is top or bottom, contentSpace is height #}
                {% set size = 'height: ' ~ image.contentSpace|number_format ~ '%;' %}
                {% set contentPosition = image.contentPosition %}

                {# if position is left or right, contentSpace is width #}
                {% if contentPosition == 'left' or contentPosition == 'right' %}
                    {% set size = 'width: ' ~ image.contentSpace|number_format ~ '%;' %}
                {% endif %}

                {{ card.card_body(image.content, image.contentBackground, size, contentPosition) }}
            </a>
        </div>
    </div>
{% endmacro %}

{# render grid col-6 #}
{% macro two_cols(image, isSmallImg) %}
    {% import _self as card %}

    <div class=\"col-6\">
        <div class=\"card\">
            <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                {{ card.card_img(image.path, isSmallImg) }}

                {# contentSpace is height #}
                {% set size = 'height: ' ~ image.contentSpace|number_format ~ '%;' %}
                {% set contentPosition = image.contentPosition %}

                {# if position is left or right, set it to bottom #}
                {% if contentPosition == 'left' or contentPosition == 'right' %}
                    {% set contentPosition = 'bottom' %}
                {% endif %}

                {{ card.card_body(image.content, image.contentBackground, size, contentPosition) }}
            </a>
        </div>
    </div>
{% endmacro %}

{# render grid special layout for 3 columns #}
{% macro three_cols(image, counter) %}
    {% import _self as card %}

    {% if counter == 1 or counter == 2 %}
        <div class=\"col-6\">
    {% endif %}

    <div class=\"card\">
        <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
            {% set isSmall = (counter == 2 or counter == 3) ? true : false %}
            {{ card.card_img(image.path, isSmall) }}

            {# contentSpace is height #}
            {% set size = 'height: ' ~ image.contentSpace|number_format ~ '%;' %}
            {% set contentPosition = image.contentPosition %}

            {# if position is left or right, set it to bottom #}
            {% if contentPosition == 'left' or contentPosition == 'right' %}
                {% set contentPosition = 'bottom' %}
            {% endif %}

            {{ card.card_body(image.content, image.contentBackground, size, contentPosition) }}
        </a>
    </div>

    {% if counter == 1 or counter == 3 %}
        </div>
    {% endif %}
{% endmacro %}

{% import _self as row %}

{% set imagesCounter = 0 %}
{% for banner in banners if banner.hasChannelCode(options['channel']) %}
    {% for image in banner.images %}
        {% set imagesCounter = imagesCounter + 1 %}
    {% endfor %}
{% endfor %}

{% if imagesCounter %}
    <div class=\"col-12 col-md-6\">
        <div class=\"row\">
            {% set counter = 0 %}
            {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                {% for image in banner.images %}
                    {% set counter = counter + 1 %}

                    {% if imagesCounter == 1 %}
                        {{ row.one_col(image) }}
                    {% elseif imagesCounter == 2 %}
                        {{ row.two_cols(image, false) }}
                    {% elseif imagesCounter == 3 %}
                        {{ row.three_cols(image, counter) }}
                    {% elseif imagesCounter > 3 and counter < 5 %}
                        {{ row.two_cols(image, true) }}
                    {% endif %}
                {% endfor %}
            {% endfor %}
        </div>
    </div>
{% endif %}
", "OmniSyliusBannerPlugin:PositionTypes:_half_screen_adoptive_grid.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/PositionTypes/_half_screen_adoptive_grid.html.twig");
    }
}
