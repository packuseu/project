<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Common/Order:_summary.html.twig */
class __TwigTemplate_26feb8d50f5752a3998e632b427b7292313ac0cbe9099a84e9690b3e301e9a4d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Common/Order:_summary.html.twig"));

        // line 1
        echo "<div class=\"mb-4\">
    ";
        // line 2
        $this->loadTemplate("@SyliusShop/Common/Order/_addresses.html.twig", "SyliusShopBundle:Common/Order:_summary.html.twig", 2)->display($context);
        // line 3
        echo "</div>

<div class=\"mb-4\">
    ";
        // line 6
        $this->loadTemplate("@SyliusShop/Common/Order/_table.html.twig", "SyliusShopBundle:Common/Order:_summary.html.twig", 6)->display($context);
        // line 7
        echo "</div>

<div class=\"row mb-4\">
    <div class=\"col\">
        ";
        // line 11
        $this->loadTemplate("@SyliusShop/Common/Order/_payments.html.twig", "SyliusShopBundle:Common/Order:_summary.html.twig", 11)->display($context);
        // line 12
        echo "    </div>
    <div class=\"col\">
        ";
        // line 14
        $this->loadTemplate("@SyliusShop/Common/Order/_shipments.html.twig", "SyliusShopBundle:Common/Order:_summary.html.twig", 14)->display($context);
        // line 15
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Common/Order:_summary.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 15,  64 => 14,  60 => 12,  58 => 11,  52 => 7,  50 => 6,  45 => 3,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"mb-4\">
    {% include '@SyliusShop/Common/Order/_addresses.html.twig' %}
</div>

<div class=\"mb-4\">
    {% include '@SyliusShop/Common/Order/_table.html.twig' %}
</div>

<div class=\"row mb-4\">
    <div class=\"col\">
        {% include '@SyliusShop/Common/Order/_payments.html.twig' %}
    </div>
    <div class=\"col\">
        {% include '@SyliusShop/Common/Order/_shipments.html.twig' %}
    </div>
</div>
", "SyliusShopBundle:Common/Order:_summary.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Common/Order/_summary.html.twig");
    }
}
