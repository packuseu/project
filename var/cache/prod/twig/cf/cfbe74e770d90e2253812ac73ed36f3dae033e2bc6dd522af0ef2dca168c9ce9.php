<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusShippingPlugin/Admin/_stylesheets.html.twig */
class __TwigTemplate_15b092e81b49561f6f0484b692665597eeb7c67d72b9ae10ae58808056263a17 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusShippingPlugin/Admin/_stylesheets.html.twig"));

        // line 1
        $this->loadTemplate("SyliusUiBundle::_stylesheets.html.twig", "@OmniSyliusShippingPlugin/Admin/_stylesheets.html.twig", 1)->display(twig_array_merge($context, ["path" => "bundles/omnisyliusshippingplugin/css/admin/shipperConfigsCollection.css"]));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusShippingPlugin/Admin/_stylesheets.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include 'SyliusUiBundle::_stylesheets.html.twig' with {'path': 'bundles/omnisyliusshippingplugin/css/admin/shipperConfigsCollection.css'} %}
", "@OmniSyliusShippingPlugin/Admin/_stylesheets.html.twig", "/var/www/html/vendor/omni/sylius-shipping-plugin/src/Resources/views/Admin/_stylesheets.html.twig");
    }
}
