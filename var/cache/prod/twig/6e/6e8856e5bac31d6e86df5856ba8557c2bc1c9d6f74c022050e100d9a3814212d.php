<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/SyliusAdminBundle/Common/Label/shipmentState.html.twig */
class __TwigTemplate_4e92455cf06b90bbe1ce1b651b87f83055710b3c2af4df91cd7ca9434ba87b93 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/SyliusAdminBundle/Common/Label/shipmentState.html.twig"));

        // line 2
        $context["viewOptions"] = ["cancelled" => ["icon" => "ban", "color" => "red"], "cart" => ["icon" => "adjust", "color" => "grey"], "ready" => ["icon" => "clock", "color" => "blue"], "new" => ["icon" => "clock", "color" => "blue"], "exported" => ["icon" => "clock", "color" => "purple"], "courier_ready" => ["icon" => "clock", "color" => "orange"], "packed" => ["icon" => "box", "color" => "yellow"], "shipped" => ["icon" => "plane", "color" => "green"]];
        // line 13
        echo "
";
        // line 14
        $context["value"] = ("sylius.ui." . (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 14, $this->source); })()));
        // line 15
        echo "
<span class=\"ui ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["viewOptions"]) || array_key_exists("viewOptions", $context) ? $context["viewOptions"] : (function () { throw new RuntimeError('Variable "viewOptions" does not exist.', 16, $this->source); })()), (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 16, $this->source); })()), [], "array", false, false, false, 16), "color", [], "array", false, false, false, 16), "html", null, true);
        echo " label\">
    <i class=\"";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["viewOptions"]) || array_key_exists("viewOptions", $context) ? $context["viewOptions"] : (function () { throw new RuntimeError('Variable "viewOptions" does not exist.', 17, $this->source); })()), (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 17, $this->source); })()), [], "array", false, false, false, 17), "icon", [], "array", false, false, false, 17), "html", null, true);
        echo " icon\"></i>
    ";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["value"]) || array_key_exists("value", $context) ? $context["value"] : (function () { throw new RuntimeError('Variable "value" does not exist.', 18, $this->source); })())), "html", null, true);
        echo "
</span>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/SyliusAdminBundle/Common/Label/shipmentState.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 18,  54 => 17,  50 => 16,  47 => 15,  45 => 14,  42 => 13,  40 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{%
    set viewOptions = {
        cancelled: { icon: 'ban', color: 'red' },
        cart: { icon: 'adjust', color: 'grey' },
        ready: { icon: 'clock', color: 'blue' },
        new: { icon: 'clock', color: 'blue' },
        exported: { icon: 'clock', color: 'purple' },
        courier_ready: { icon: 'clock', color: 'orange' },
        packed: { icon: 'box', color: 'yellow' },
        shipped: { icon: 'plane', color: 'green' },
    }
%}

{% set value = 'sylius.ui.' ~ data %}

<span class=\"ui {{ viewOptions[data]['color'] }} label\">
    <i class=\"{{ viewOptions[data]['icon'] }} icon\"></i>
    {{ value|trans }}
</span>
", "bundles/SyliusAdminBundle/Common/Label/shipmentState.html.twig", "/var/www/html/templates/bundles/SyliusAdminBundle/Common/Label/shipmentState.html.twig");
    }
}
