<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Common/Order:_addresses.html.twig */
class __TwigTemplate_4039cc7d2e010c061b88a9a16a7553cc0ef20ae62ed8e72a8dd2c5ba82e1c26c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Common/Order:_addresses.html.twig"));

        // line 1
        echo "<div class=\"row\">
    <div class=\"col\">
        <div class=\"card\">
            <div class=\"card-header\">
                ";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_address"), "html", null, true);
        echo "
            </div>
            <div class=\"card-body\">
                ";
        // line 8
        $this->loadTemplate("@SyliusShop/Common/_address.html.twig", "SyliusShopBundle:Common/Order:_addresses.html.twig", 8)->display(twig_array_merge($context, ["address" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 8, $this->source); })()), "shippingAddress", [], "any", false, false, false, 8)]));
        // line 9
        echo "            </div>
        </div>
    </div>
    <div class=\"col\">
        <div class=\"card\">
            <div class=\"card-header\">
                ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.billing_address"), "html", null, true);
        echo "
            </div>
            <div class=\"card-body\">
                ";
        // line 18
        $this->loadTemplate("@SyliusShop/Common/_address.html.twig", "SyliusShopBundle:Common/Order:_addresses.html.twig", 18)->display(twig_array_merge($context, ["address" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 18, $this->source); })()), "billingAddress", [], "any", false, false, false, 18)]));
        // line 19
        echo "            </div>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Common/Order:_addresses.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 19,  68 => 18,  62 => 15,  54 => 9,  52 => 8,  46 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"row\">
    <div class=\"col\">
        <div class=\"card\">
            <div class=\"card-header\">
                {{ 'sylius.ui.shipping_address'|trans }}
            </div>
            <div class=\"card-body\">
                {% include '@SyliusShop/Common/_address.html.twig' with {'address': order.shippingAddress} %}
            </div>
        </div>
    </div>
    <div class=\"col\">
        <div class=\"card\">
            <div class=\"card-header\">
                {{ 'sylius.ui.billing_address'|trans }}
            </div>
            <div class=\"card-body\">
                {% include '@SyliusShop/Common/_address.html.twig' with {'address': order.billingAddress} %}
            </div>
        </div>
    </div>
</div>
", "SyliusShopBundle:Common/Order:_addresses.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Common/Order/_addresses.html.twig");
    }
}
