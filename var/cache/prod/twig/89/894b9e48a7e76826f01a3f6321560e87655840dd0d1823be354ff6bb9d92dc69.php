<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Taxon:_horizontalMenu.html.twig */
class __TwigTemplate_892886fd42586179346ccbf6328044ac920fca83018e1c7c3c8be26ae6ba96fe extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Taxon:_horizontalMenu.html.twig"));

        // line 31
        echo "
";
        // line 32
        $macros["macros"] = $this->macros["macros"] = $this;
        // line 33
        echo "
<nav class=\"navbar navbar-expand-lg bottom-navbar\">
    <div class=\"collapse navbar-collapse justify-content-center\" id=\"mainNavbar\">
        <form action=\"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_search");
        echo "\" class=\"input-group pt-2\" method=\"get\">
            <div class=\"ui inverted transparent icon input input-group\">
                <div class=\"input-group-prepend\">
                    <button class=\"input-group-text\"><i class=\"fa fa-search\"></i></button>
                </div>
                <input type=\"text\" id=\"search\" class=\"form-control\" placeholder=";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.search"), "html", null, true);
        echo " name=\"q\" value=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("searchTerm", $context)) ? (_twig_default_filter((isset($context["searchTerm"]) || array_key_exists("searchTerm", $context) ? $context["searchTerm"] : (function () { throw new RuntimeError('Variable "searchTerm" does not exist.', 41, $this->source); })()), "")) : ("")), "html", null, true);
        echo "\">
            </div>
        </form>
        <div class=\"navbar-nav py-1\">
            ";
        // line 45
        $context["headerNode"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeByType("main_menu");
        // line 46
        echo "
            ";
        // line 47
        if ((null != (isset($context["headerNode"]) || array_key_exists("headerNode", $context) ? $context["headerNode"] : (function () { throw new RuntimeError('Variable "headerNode" does not exist.', 47, $this->source); })()))) {
            // line 48
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["headerNode"]) || array_key_exists("headerNode", $context) ? $context["headerNode"] : (function () { throw new RuntimeError('Variable "headerNode" does not exist.', 48, $this->source); })()), "children", [], "any", false, false, false, 48));
            foreach ($context['_seq'] as $context["_key"] => $context["node"]) {
                // line 49
                echo "                    ";
                if (twig_get_attribute($this->env, $this->source, $context["node"], "enabled", [], "any", false, false, false, 49)) {
                    // line 50
                    echo "                        ";
                    echo twig_call_macro($macros["macros"], "macro_node_item", [$context["node"]], 50, $context, $this->getSourceContext());
                    echo "
                    ";
                }
                // line 52
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['node'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "            ";
        }
        // line 54
        echo "        </div>
        <div class=\"d-md-none py-3 border-top\">
            ";
        // line 56
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.before_security_widget"]);
        echo "

            ";
        // line 58
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("sylius.controller.shop.security_widget:renderAction"));
        echo "

            ";
        // line 60
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.after_security_widget"]);
        echo "
        </div>
    </div>
</nav>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 1
    public function macro_node_item($__node__ = null, $__depth__ = 0, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "depth" => $__depth__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "node_item"));

            // line 2
            echo "    ";
            $macros["macros"] = $this;
            // line 3
            echo "    ";
            $context["detatchment"] = "";
            // line 4
            echo "
    ";
            // line 5
            if ((isset($context["depth"]) || array_key_exists("depth", $context) ? $context["depth"] : (function () { throw new RuntimeError('Variable "depth" does not exist.', 5, $this->source); })())) {
                // line 6
                echo "        ";
                $context["detatchment"] = ("detatched-left-" . ((isset($context["depth"]) || array_key_exists("depth", $context) ? $context["depth"] : (function () { throw new RuntimeError('Variable "depth" does not exist.', 6, $this->source); })()) * 20));
                // line 7
                echo "    ";
            }
            // line 8
            echo "
    ";
            // line 9
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 9, $this->source); })()), "children", [], "any", false, false, false, 9)) > 0)) {
                // line 10
                echo "        <span class=\"nav-item dropdown\">
            <a class=\"nav-link dropdown-toggle ";
                // line 11
                echo twig_escape_filter($this->env, (isset($context["detatchment"]) || array_key_exists("detatchment", $context) ? $context["detatchment"] : (function () { throw new RuntimeError('Variable "detatchment" does not exist.', 11, $this->source); })()), "html", null, true);
                echo "\" href=\"#\" role=\"button\" data-toggle=\"dropdown\">
                ";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 12, $this->source); })()), "translation", [], "any", false, false, false, 12), "title", [], "any", false, false, false, 12), "html", null, true);
                echo "
            </a>
            <div class=\"dropdown-menu simple-dropdown ";
                // line 14
                echo twig_escape_filter($this->env, (isset($context["detatchment"]) || array_key_exists("detatchment", $context) ? $context["detatchment"] : (function () { throw new RuntimeError('Variable "detatchment" does not exist.', 14, $this->source); })()), "html", null, true);
                echo "\">
                ";
                // line 15
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 15, $this->source); })()), "children", [], "any", false, false, false, 15));
                foreach ($context['_seq'] as $context["_key"] => $context["subnode"]) {
                    // line 16
                    echo "                    ";
                    echo twig_call_macro($macros["macros"], "macro_node_item", [$context["subnode"], ((isset($context["depth"]) || array_key_exists("depth", $context) ? $context["depth"] : (function () { throw new RuntimeError('Variable "depth" does not exist.', 16, $this->source); })()) + 1)], 16, $context, $this->getSourceContext());
                    echo "
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subnode'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 18
                echo "            </div>
        </span>
    ";
            } else {
                // line 21
                echo "        ";
                if ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeLinkable((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 21, $this->source); })()))) {
                    // line 22
                    echo "            <a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 22, $this->source); })()), "link", [], "any", false, false, false, 22), "html", null, true);
                    echo "\" class=\"nav-item nav-link ";
                    echo twig_escape_filter($this->env, (isset($context["detatchment"]) || array_key_exists("detatchment", $context) ? $context["detatchment"] : (function () { throw new RuntimeError('Variable "detatchment" does not exist.', 22, $this->source); })()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 22, $this->source); })()), "translation", [], "any", false, false, false, 22), "title", [], "any", false, false, false, 22), "html", null, true);
                    echo "</a>
        ";
                } elseif ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeRelated(                // line 23
(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 23, $this->source); })()))) {
                    // line 24
                    echo "            ";
                    $context["relation"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getRelation((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 24, $this->source); })()));
                    // line 25
                    echo "            <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_index", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["relation"]) || array_key_exists("relation", $context) ? $context["relation"] : (function () { throw new RuntimeError('Variable "relation" does not exist.', 25, $this->source); })()), "slug", [], "any", false, false, false, 25), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["relation"]) || array_key_exists("relation", $context) ? $context["relation"] : (function () { throw new RuntimeError('Variable "relation" does not exist.', 25, $this->source); })()), "translation", [], "any", false, false, false, 25), "locale", [], "any", false, false, false, 25)]), "html", null, true);
                    echo "\" class=\"nav-item nav-link ";
                    echo twig_escape_filter($this->env, (isset($context["detatchment"]) || array_key_exists("detatchment", $context) ? $context["detatchment"] : (function () { throw new RuntimeError('Variable "detatchment" does not exist.', 25, $this->source); })()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getRelationLabel((isset($context["relation"]) || array_key_exists("relation", $context) ? $context["relation"] : (function () { throw new RuntimeError('Variable "relation" does not exist.', 25, $this->source); })())), "html", null, true);
                    echo "</a>
        ";
                } else {
                    // line 27
                    echo "            <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_cms_frontend_show", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 27, $this->source); })()), "translation", [], "any", false, false, false, 27), "slug", [], "any", false, false, false, 27), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 27, $this->source); })()), "translation", [], "any", false, false, false, 27), "locale", [], "any", false, false, false, 27)]), "html", null, true);
                    echo "\" class=\"nav-item nav-link ";
                    echo twig_escape_filter($this->env, (isset($context["detatchment"]) || array_key_exists("detatchment", $context) ? $context["detatchment"] : (function () { throw new RuntimeError('Variable "detatchment" does not exist.', 27, $this->source); })()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 27, $this->source); })()), "translation", [], "any", false, false, false, 27), "title", [], "any", false, false, false, 27), "html", null, true);
                    echo "</a>
        ";
                }
                // line 29
                echo "    ";
            }
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Taxon:_horizontalMenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 29,  223 => 27,  213 => 25,  210 => 24,  208 => 23,  199 => 22,  196 => 21,  191 => 18,  182 => 16,  178 => 15,  174 => 14,  169 => 12,  165 => 11,  162 => 10,  160 => 9,  157 => 8,  154 => 7,  151 => 6,  149 => 5,  146 => 4,  143 => 3,  140 => 2,  123 => 1,  111 => 60,  106 => 58,  101 => 56,  97 => 54,  94 => 53,  88 => 52,  82 => 50,  79 => 49,  74 => 48,  72 => 47,  69 => 46,  67 => 45,  58 => 41,  50 => 36,  45 => 33,  43 => 32,  40 => 31,);
    }

    public function getSourceContext()
    {
        return new Source("{% macro node_item(node, depth = 0) %}
    {% import _self as macros %}
    {% set detatchment = '' %}

    {% if depth %}
        {% set detatchment = 'detatched-left-' ~ depth * 20 %}
    {% endif %}

    {% if node.children|length > 0 %}
        <span class=\"nav-item dropdown\">
            <a class=\"nav-link dropdown-toggle {{ detatchment }}\" href=\"#\" role=\"button\" data-toggle=\"dropdown\">
                {{ node.translation.title }}
            </a>
            <div class=\"dropdown-menu simple-dropdown {{ detatchment }}\">
                {% for subnode in node.children %}
                    {{ macros.node_item(subnode, depth + 1) }}
                {% endfor %}
            </div>
        </span>
    {% else %}
        {% if omni_sylius_is_node_linkable(node) %}
            <a href=\"{{ node.link }}\" class=\"nav-item nav-link {{ detatchment }}\">{{ node.translation.title }}</a>
        {% elseif omni_sylius_is_node_related(node) %}
            {% set relation = omni_sylius_get_relation(node) %}
            <a href=\"{{ path('sylius_shop_product_index', {'slug': relation.slug, '_locale': relation.translation.locale}) }}\" class=\"nav-item nav-link {{ detatchment }}\">{{ omni_sylius_get_relation_label(relation) }}</a>
        {% else %}
            <a href=\"{{ path('omni_sylius_cms_frontend_show', {'slug': node.translation.slug, '_locale': node.translation.locale}) }}\" class=\"nav-item nav-link {{ detatchment }}\">{{ node.translation.title }}</a>
        {% endif %}
    {% endif %}
{% endmacro %}

{% import _self as macros %}

<nav class=\"navbar navbar-expand-lg bottom-navbar\">
    <div class=\"collapse navbar-collapse justify-content-center\" id=\"mainNavbar\">
        <form action=\"{{ path('omni_sylius_search') }}\" class=\"input-group pt-2\" method=\"get\">
            <div class=\"ui inverted transparent icon input input-group\">
                <div class=\"input-group-prepend\">
                    <button class=\"input-group-text\"><i class=\"fa fa-search\"></i></button>
                </div>
                <input type=\"text\" id=\"search\" class=\"form-control\" placeholder={{ 'sylius.ui.search'|trans }} name=\"q\" value=\"{{ searchTerm|default('') }}\">
            </div>
        </form>
        <div class=\"navbar-nav py-1\">
            {% set headerNode = omni_sylius_get_node_by_type('main_menu') %}

            {% if null != headerNode %}
                {% for node in headerNode.children %}
                    {% if node.enabled%}
                        {{ macros.node_item(node) }}
                    {% endif %}
                {% endfor %}
            {% endif %}
        </div>
        <div class=\"d-md-none py-3 border-top\">
            {{ sonata_block_render_event('sylius.shop.layout.before_security_widget') }}

            {{ render(controller('sylius.controller.shop.security_widget:renderAction')) }}

            {{ sonata_block_render_event('sylius.shop.layout.after_security_widget') }}
        </div>
    </div>
</nav>
", "SyliusShopBundle:Taxon:_horizontalMenu.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Taxon/_horizontalMenu.html.twig");
    }
}
