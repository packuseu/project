<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* LeadCatcher/lead_catcher.html.twig */
class __TwigTemplate_606832cb0ed8798b4f12d525522ff2424b4018f7cedfd210b81700c487de7404 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "LeadCatcher/lead_catcher.html.twig"));

        // line 1
        echo "<div class=\"modal fade\" id=\"lead-catcher\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
                <h4 class=\"h2\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.lead_catcher.header"), "html", null, true);
        echo "</h4>
                <div class=\"note\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.lead_catcher.tell_more"), "html", null, true);
        echo "</div>
                <div class=\"row\">
                    <form action=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("quote", ["locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 11, $this->source); })()), "request", [], "any", false, false, false, 11), "locale", [], "any", false, false, false, 11)]), "html", null, true);
        echo "\" method=\"get\">
                        <div class=\"col-12\">
                            <div class=\"form-group quantity-slider\">
                                <label for=\"leadQuantity\" class=\"catcher-label\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.lead_catcher.quantity"), "html", null, true);
        echo "</label>
                                <input type=\"text\" class=\"custom-range\" id=\"leadQuantity\" name=\"quantity\" data-slider-value=\"50\">
                            </div>
                            <div class=\"slider-label-wrapper\">
                                <div class=\"slider-tick-label-container row\">
                                    <div class=\"slider-tick-label col-3 quantites\">0</div>
                                    <div class=\"slider-tick-label col-3 quantites\">180</div>
                                    <div class=\"slider-tick-label col-3 quantites\">20k</div>
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"leadQueantity\" class=\"catcher-label\">";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.lead_catcher.deadline"), "html", null, true);
        echo "</label>
                                <input type=\"date\" name=\"deadline\" max=\"2300-12-31\" required
                                       min=\"2000-01-01\" class=\"form-control\">
                            </div>
                            <div class=\"form-group\">
                                <button class=\"btn btn-primary mx-auto\" type=\"submit\">
                                    ";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.lead_catcher.continue"), "html", null, true);
        echo "
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "LeadCatcher/lead_catcher.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 31,  78 => 25,  64 => 14,  58 => 11,  53 => 9,  49 => 8,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"modal fade\" id=\"lead-catcher\" tabindex=\"-1\" role=\"dialog\">
    <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
            <div class=\"modal-body\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
                <h4 class=\"h2\">{{ 'app.lead_catcher.header'|trans }}</h4>
                <div class=\"note\">{{ 'app.lead_catcher.tell_more'|trans }}</div>
                <div class=\"row\">
                    <form action=\"{{ path('quote', {locale: app.request.locale}) }}\" method=\"get\">
                        <div class=\"col-12\">
                            <div class=\"form-group quantity-slider\">
                                <label for=\"leadQuantity\" class=\"catcher-label\">{{ 'app.lead_catcher.quantity'|trans }}</label>
                                <input type=\"text\" class=\"custom-range\" id=\"leadQuantity\" name=\"quantity\" data-slider-value=\"50\">
                            </div>
                            <div class=\"slider-label-wrapper\">
                                <div class=\"slider-tick-label-container row\">
                                    <div class=\"slider-tick-label col-3 quantites\">0</div>
                                    <div class=\"slider-tick-label col-3 quantites\">180</div>
                                    <div class=\"slider-tick-label col-3 quantites\">20k</div>
                                </div>
                            </div>
                            <div class=\"form-group\">
                                <label for=\"leadQueantity\" class=\"catcher-label\">{{ 'app.lead_catcher.deadline'|trans }}</label>
                                <input type=\"date\" name=\"deadline\" max=\"2300-12-31\" required
                                       min=\"2000-01-01\" class=\"form-control\">
                            </div>
                            <div class=\"form-group\">
                                <button class=\"btn btn-primary mx-auto\" type=\"submit\">
                                    {{ 'app.lead_catcher.continue'|trans }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
", "LeadCatcher/lead_catcher.html.twig", "/var/www/html/templates/LeadCatcher/lead_catcher.html.twig");
    }
}
