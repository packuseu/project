<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusSearchPlugin/search_form.html.twig */
class __TwigTemplate_39682eca45cb52db2a67c2ac344d40423f783d099069217db330693d7247fe70 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusSearchPlugin/search_form.html.twig"));

        // line 1
        echo " <div class=\"ui right pointing inverted item\">
    <form action=\"";
        // line 2
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_search");
        echo "\" class=\"input-group\" method=\"get\">

        <div class=\"ui inverted transparent icon input\">
            <input type=\"text\" id=\"search\" placeholder=\"Search term...\" name=\"q\" value=\"";
        // line 5
        echo twig_escape_filter($this->env, ((array_key_exists("searchTerm", $context)) ? (_twig_default_filter((isset($context["searchTerm"]) || array_key_exists("searchTerm", $context) ? $context["searchTerm"] : (function () { throw new RuntimeError('Variable "searchTerm" does not exist.', 5, $this->source); })()), "")) : ("")), "html", null, true);
        echo "\">
            <i class=\"search link icon\"></i>
        </div>
    </form>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusSearchPlugin/search_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 5,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source(" <div class=\"ui right pointing inverted item\">
    <form action=\"{{ path('omni_sylius_search') }}\" class=\"input-group\" method=\"get\">

        <div class=\"ui inverted transparent icon input\">
            <input type=\"text\" id=\"search\" placeholder=\"Search term...\" name=\"q\" value=\"{{ searchTerm|default('') }}\">
            <i class=\"search link icon\"></i>
        </div>
    </form>
</div>
", "@OmniSyliusSearchPlugin/search_form.html.twig", "/var/www/html/vendor/omni/sylius-search-plugin/src/Resources/views/search_form.html.twig");
    }
}
