<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Brille24SyliusTierPricePlugin/Shop/Product/Show/_tier_price_promo.html.twig */
class __TwigTemplate_9301fbacdd02487b338f1062b1acccdc4b1c9abe5b6510f88da3f01ed91c57cc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Brille24SyliusTierPricePlugin/Shop/Product/Show/_tier_price_promo.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "@Brille24SyliusTierPricePlugin/Shop/Product/Show/_tier_price_promo.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["product"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["block"]) || array_key_exists("block", $context) ? $context["block"] : (function () { throw new RuntimeError('Variable "block" does not exist.', 3, $this->source); })()), "settings", [], "any", false, false, false, 3), "product", [], "any", false, false, false, 3);
        // line 4
        echo "
<div id=\"tier_prices_tables\">
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "variants", [], "any", false, false, false, 6));
        foreach ($context['_seq'] as $context["_key"] => $context["product_variant"]) {
            // line 7
            echo "        ";
            $context["tier_prices"] = twig_get_attribute($this->env, $this->source, $context["product_variant"], "getTierPricesForChannel", [0 => twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 7, $this->source); })()), "channel", [], "any", false, false, false, 7), 1 => twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 7, $this->source); })()), "customer", [], "any", false, false, false, 7)], "method", false, false, false, 7);
            // line 8
            echo "
        ";
            // line 9
            $context["tier_prices_table_id"] = (twig_get_attribute($this->env, $this->source, $context["product_variant"], "code", [], "any", false, false, false, 9) . "_table");
            // line 10
            echo "
        ";
            // line 11
            if ((twig_length_filter($this->env, (isset($context["tier_prices"]) || array_key_exists("tier_prices", $context) ? $context["tier_prices"] : (function () { throw new RuntimeError('Variable "tier_prices" does not exist.', 11, $this->source); })())) > 0)) {
                // line 12
                echo "            <div id=\"";
                echo twig_escape_filter($this->env, (isset($context["tier_prices_table_id"]) || array_key_exists("tier_prices_table_id", $context) ? $context["tier_prices_table_id"] : (function () { throw new RuntimeError('Variable "tier_prices_table_id" does not exist.', 12, $this->source); })()), "html", null, true);
                echo "\">
                <span style=\"font-weight: bold;\">";
                // line 13
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("brille24_tier_price.ui.tier_prices"), "html", null, true);
                echo "</span>

                <table class=\"ui stackable celled table\">
                    <thead>
                    <tr>
                        <th>";
                // line 18
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.quantity"), "html", null, true);
                echo "</th>
                        <th>";
                // line 19
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.unit_price"), "html", null, true);
                echo "</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style=\"display: none\">
                        <td>";
                // line 24
                echo twig_escape_filter($this->env, twig_number_format_filter($this->env, 1), "html", null, true);
                echo "</td>
                        <td>";
                // line 25
                echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["product_variant"], "channelPricings", [], "any", false, false, false, 25), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 25, $this->source); })()), "channel", [], "any", false, false, false, 25), "code", [], "any", false, false, false, 25), [], "array", false, false, false, 25), "price", [], "any", false, false, false, 25)], 25, $context, $this->getSourceContext());
                echo "</td>
                    </tr>
                    ";
                // line 27
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["tier_prices"]) || array_key_exists("tier_prices", $context) ? $context["tier_prices"] : (function () { throw new RuntimeError('Variable "tier_prices" does not exist.', 27, $this->source); })()));
                foreach ($context['_seq'] as $context["_key"] => $context["tierPrice"]) {
                    // line 28
                    echo "                        <tr>
                            <td>";
                    // line 29
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["tierPrice"], "qty", [], "any", false, false, false, 29)), "html", null, true);
                    echo "</td>
                            <td>";
                    // line 30
                    echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, $context["tierPrice"], "price", [], "any", false, false, false, 30), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 30, $this->source); })()), "channel", [], "any", false, false, false, 30), "baseCurrency", [], "any", false, false, false, 30)], 30, $context, $this->getSourceContext());
                    echo "</td>
                        </tr>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tierPrice'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 33
                echo "                    </tbody>
                </table>

                <br />
            </div>
        ";
            }
            // line 39
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product_variant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@Brille24SyliusTierPricePlugin/Shop/Product/Show/_tier_price_promo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 40,  130 => 39,  122 => 33,  113 => 30,  109 => 29,  106 => 28,  102 => 27,  97 => 25,  93 => 24,  85 => 19,  81 => 18,  73 => 13,  68 => 12,  66 => 11,  63 => 10,  61 => 9,  58 => 8,  55 => 7,  51 => 6,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}

{% set product = block.settings.product %}

<div id=\"tier_prices_tables\">
    {% for product_variant in product.variants %}
        {% set tier_prices = product_variant.getTierPricesForChannel(sylius.channel,sylius.customer) %}

        {% set tier_prices_table_id = product_variant.code~'_table' %}

        {% if tier_prices|length > 0 %}
            <div id=\"{{ tier_prices_table_id }}\">
                <span style=\"font-weight: bold;\">{{ 'brille24_tier_price.ui.tier_prices'|trans }}</span>

                <table class=\"ui stackable celled table\">
                    <thead>
                    <tr>
                        <th>{{ 'sylius.ui.quantity'|trans }}</th>
                        <th>{{ 'sylius.ui.unit_price'|trans }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr style=\"display: none\">
                        <td>{{ 1|number_format }}</td>
                        <td>{{ money.convertAndFormat(product_variant.channelPricings[sylius.channel.code].price) }}</td>
                    </tr>
                    {% for tierPrice in tier_prices %}
                        <tr>
                            <td>{{ tierPrice.qty|number_format }}</td>
                            <td>{{ money.convertAndFormat(tierPrice.price, sylius.channel.baseCurrency) }}</td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>

                <br />
            </div>
        {% endif %}
    {% endfor %}
</div>
", "@Brille24SyliusTierPricePlugin/Shop/Product/Show/_tier_price_promo.html.twig", "/var/www/html/vendor/brille24/sylius-tierprice-plugin/src/Resources/views/Shop/Product/Show/_tier_price_promo.html.twig");
    }
}
