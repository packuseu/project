<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Grid:_default.html.twig */
class __TwigTemplate_7725c10a560f570d277fd2e15762a1bdcc875cb71fd635bb674e7a270d22b848 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Grid:_default.html.twig"));

        // line 1
        $macros["pagination"] = $this->macros["pagination"] = $this->loadTemplate("@SyliusShop/Common/Macro/pagination.html.twig", "SyliusShopBundle:Grid:_default.html.twig", 1)->unwrap();
        // line 2
        $macros["messages"] = $this->macros["messages"] = $this->loadTemplate("@SyliusShop/Common/Macro/messages.html.twig", "SyliusShopBundle:Grid:_default.html.twig", 2)->unwrap();
        // line 3
        $macros["table"] = $this->macros["table"] = $this->loadTemplate("@SyliusShop/Common/Macro/table.html.twig", "SyliusShopBundle:Grid:_default.html.twig", 3)->unwrap();
        // line 4
        echo "
";
        // line 5
        $context["definition"] = twig_get_attribute($this->env, $this->source, (isset($context["grid"]) || array_key_exists("grid", $context) ? $context["grid"] : (function () { throw new RuntimeError('Variable "grid" does not exist.', 5, $this->source); })()), "definition", [], "any", false, false, false, 5);
        // line 6
        $context["data"] = twig_get_attribute($this->env, $this->source, (isset($context["grid"]) || array_key_exists("grid", $context) ? $context["grid"] : (function () { throw new RuntimeError('Variable "grid" does not exist.', 6, $this->source); })()), "data", [], "any", false, false, false, 6);
        // line 7
        echo "
";
        // line 8
        $context["path"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "attributes", [], "any", false, false, false, 8), "get", [0 => "_route"], "method", false, false, false, 8), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 8, $this->source); })()), "request", [], "any", false, false, false, 8), "attributes", [], "any", false, false, false, 8), "get", [0 => "_route_params"], "method", false, false, false, 8));
        // line 9
        echo "
";
        // line 10
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 10, $this->source); })()), "enabledFilters", [], "any", false, false, false, 10)) > 0)) {
            // line 11
            echo "    <div class=\"\">
        <div class=\"\">
            ";
            // line 13
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.filters"), "html", null, true);
            echo "
        </div>
        <div class=\"\">
            <form method=\"get\" action=\"";
            // line 16
            echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 16, $this->source); })()), "html", null, true);
            echo "\" class=\"loadable\">
                <div class=\"\">
                ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->extensions['Sylius\Bundle\UiBundle\Twig\SortByExtension']->sortBy(twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 18, $this->source); })()), "enabledFilters", [], "any", false, false, false, 18), "position"));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["_key"] => $context["filter"]) {
                if (twig_get_attribute($this->env, $this->source, $context["filter"], "enabled", [], "any", false, false, false, 18)) {
                    // line 19
                    echo "                    ";
                    echo call_user_func_array($this->env->getFunction('sylius_grid_render_filter')->getCallable(), [(isset($context["grid"]) || array_key_exists("grid", $context) ? $context["grid"] : (function () { throw new RuntimeError('Variable "grid" does not exist.', 19, $this->source); })()), $context["filter"]]);
                    echo "

                    ";
                    // line 21
                    if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, false, 21) % 2)) {
                        // line 22
                        echo "                        </div>
                        <div class=\"\">
                    ";
                    }
                    // line 25
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "                </div>
                <button class=\"btn btn-primary\" type=\"submit\">
                    ";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.filter"), "html", null, true);
            echo "
                </button>
                <a class=\"btn btn-outline-primary\" href=\"";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 30, $this->source); })()), "html", null, true);
            echo "\">
                    ";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.clear_filters"), "html", null, true);
            echo "
                </a>
            </form>
        </div>
    </div>
";
        }
        // line 37
        echo "
";
        // line 38
        if ((((twig_length_filter($this->env, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 38, $this->source); })())) > 0) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["definition"] ?? null), "actionGroups", [], "any", false, true, false, 38), "bulk", [], "any", true, true, false, 38)) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 38, $this->source); })()), "getEnabledActions", [0 => "bulk"], "method", false, false, false, 38)) > 0))) {
            // line 39
            echo "    <div class=\"\">
        <div class=\"\">
            ";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.bulk_actions"), "html", null, true);
            echo "
        </div>
        <div class=\"\">
            ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 44, $this->source); })()), "getEnabledActions", [0 => "bulk"], "method", false, false, false, 44));
            foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
                // line 45
                echo "                ";
                echo call_user_func_array($this->env->getFunction('sylius_grid_render_bulk_action')->getCallable(), [(isset($context["grid"]) || array_key_exists("grid", $context) ? $context["grid"] : (function () { throw new RuntimeError('Variable "grid" does not exist.', 45, $this->source); })()), $context["action"], null]);
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "        </div>
    </div>
";
        }
        // line 50
        echo "
<div class=\"\">
    ";
        // line 52
        if (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 52, $this->source); })()), "limits", [], "any", false, false, false, 52)) > 1) && (twig_length_filter($this->env, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 52, $this->source); })())) > min(twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 52, $this->source); })()), "limits", [], "any", false, false, false, 52))))) {
            // line 53
            echo "    <div class=\"\">
        <div class=\"\">
            ";
            // line 55
            echo twig_call_macro($macros["pagination"], "macro_simple", [(isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 55, $this->source); })())], 55, $context, $this->getSourceContext());
            echo "
        </div>
        <div class=\"\">
            <div class=\"\">
                ";
            // line 59
            echo twig_call_macro($macros["pagination"], "macro_perPage", [(isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 59, $this->source); })()), twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 59, $this->source); })()), "limits", [], "any", false, false, false, 59)], 59, $context, $this->getSourceContext());
            echo "
            </div>
        </div>
    </div>
    ";
        } else {
            // line 64
            echo "        ";
            echo twig_call_macro($macros["pagination"], "macro_simple", [(isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 64, $this->source); })())], 64, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 66
        echo "
    ";
        // line 67
        if ((twig_length_filter($this->env, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 67, $this->source); })())) > 0)) {
            // line 68
            echo "        <table class=\"table\">
            <thead>
            <tr>
                ";
            // line 71
            echo twig_call_macro($macros["table"], "macro_headers", [(isset($context["grid"]) || array_key_exists("grid", $context) ? $context["grid"] : (function () { throw new RuntimeError('Variable "grid" does not exist.', 71, $this->source); })()), (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 71, $this->source); })()), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 71, $this->source); })()), "request", [], "any", false, false, false, 71), "attributes", [], "any", false, false, false, 71)], 71, $context, $this->getSourceContext());
            echo "
            </tr>
            </thead>
            <tbody>
            ";
            // line 75
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 75, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 76
                echo "                ";
                echo twig_call_macro($macros["table"], "macro_row", [(isset($context["grid"]) || array_key_exists("grid", $context) ? $context["grid"] : (function () { throw new RuntimeError('Variable "grid" does not exist.', 76, $this->source); })()), (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 76, $this->source); })()), $context["row"]], 76, $context, $this->getSourceContext());
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "            </tbody>
        </table>
    ";
        } else {
            // line 81
            echo "        ";
            echo twig_call_macro($macros["messages"], "macro_info", ["sylius.ui.no_results_to_display"], 81, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 83
        echo "    ";
        echo twig_call_macro($macros["pagination"], "macro_simple", [(isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 83, $this->source); })())], 83, $context, $this->getSourceContext());
        echo "
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Grid:_default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 83,  233 => 81,  228 => 78,  219 => 76,  215 => 75,  208 => 71,  203 => 68,  201 => 67,  198 => 66,  192 => 64,  184 => 59,  177 => 55,  173 => 53,  171 => 52,  167 => 50,  162 => 47,  153 => 45,  149 => 44,  143 => 41,  139 => 39,  137 => 38,  134 => 37,  125 => 31,  121 => 30,  116 => 28,  112 => 26,  102 => 25,  97 => 22,  95 => 21,  89 => 19,  78 => 18,  73 => 16,  67 => 13,  63 => 11,  61 => 10,  58 => 9,  56 => 8,  53 => 7,  51 => 6,  49 => 5,  46 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusShop/Common/Macro/pagination.html.twig' as pagination %}
{% import '@SyliusShop/Common/Macro/messages.html.twig' as messages %}
{% import '@SyliusShop/Common/Macro/table.html.twig' as table %}

{% set definition = grid.definition %}
{% set data = grid.data %}

{% set path = path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')) %}

{% if definition.enabledFilters|length > 0 %}
    <div class=\"\">
        <div class=\"\">
            {{ 'sylius.ui.filters'|trans }}
        </div>
        <div class=\"\">
            <form method=\"get\" action=\"{{ path }}\" class=\"loadable\">
                <div class=\"\">
                {% for filter in definition.enabledFilters|sort_by('position') if filter.enabled %}
                    {{ sylius_grid_render_filter(grid, filter) }}

                    {% if loop.index0 % 2 %}
                        </div>
                        <div class=\"\">
                    {% endif %}
                {% endfor %}
                </div>
                <button class=\"btn btn-primary\" type=\"submit\">
                    {{ 'sylius.ui.filter'|trans }}
                </button>
                <a class=\"btn btn-outline-primary\" href=\"{{ path }}\">
                    {{ 'sylius.ui.clear_filters'|trans }}
                </a>
            </form>
        </div>
    </div>
{% endif %}

{% if data|length > 0 and definition.actionGroups.bulk is defined and definition.getEnabledActions('bulk')|length > 0 %}
    <div class=\"\">
        <div class=\"\">
            {{ 'sylius.ui.bulk_actions'|trans }}
        </div>
        <div class=\"\">
            {% for action in definition.getEnabledActions('bulk') %}
                {{ sylius_grid_render_bulk_action(grid, action, null) }}
            {% endfor %}
        </div>
    </div>
{% endif %}

<div class=\"\">
    {% if definition.limits|length > 1 and data|length > min(definition.limits) %}
    <div class=\"\">
        <div class=\"\">
            {{ pagination.simple(data) }}
        </div>
        <div class=\"\">
            <div class=\"\">
                {{ pagination.perPage(data, definition.limits) }}
            </div>
        </div>
    </div>
    {% else %}
        {{ pagination.simple(data) }}
    {% endif %}

    {% if data|length > 0 %}
        <table class=\"table\">
            <thead>
            <tr>
                {{ table.headers(grid, definition, app.request.attributes) }}
            </tr>
            </thead>
            <tbody>
            {% for row in data %}
                {{ table.row(grid, definition, row) }}
            {% endfor %}
            </tbody>
        </table>
    {% else %}
        {{ messages.info('sylius.ui.no_results_to_display') }}
    {% endif %}
    {{ pagination.simple(data) }}
</div>
", "SyliusShopBundle:Grid:_default.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Grid/_default.html.twig");
    }
}
