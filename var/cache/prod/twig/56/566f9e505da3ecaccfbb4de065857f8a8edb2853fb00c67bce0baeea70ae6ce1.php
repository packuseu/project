<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Banner/PositionType/directional.html.twig */
class __TwigTemplate_1c1f5b5ec1955acaca937a09572b378987163689153f262a01baeaeb8938d2fd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Banner/PositionType/directional.html.twig"));

        // line 1
        echo "<div class=\"container\">
    <div class=\"banners-two-columns\">
        <div class=\"row pb-4\">
            ";
        // line 4
        $context["counter"] = 1;
        // line 5
        echo "
            ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 6, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 6, $this->source); })()), "channel", [], "array", false, false, false, 6)], "method", false, false, false, 6)) {
                // line 7
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 7));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 8
                    echo "                    <div class=\"col-12 col-md-6 ";
                    if (((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 8, $this->source); })()) % 2)) {
                        echo "pl-0 pr-2";
                    } else {
                        echo "pr-0 pl-2";
                    }
                    echo "\">
                        <a class=\"card-link\" href=\"";
                    // line 9
                    echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 9), "link", [], "any", true, true, false, 9)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 9), "link", [], "any", false, false, false, 9), "javascript:;")) : ("javascript:;")), "html", null, true);
                    echo "\">
                            <div style=\"width: 100%; background-image: url(";
                    // line 10
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 10), "omni_sylius_banner"), "html", null, true);
                    echo "); background-repeat: no-repeat; background-size: cover; height: 300px\">
                                <div style=\"position: absolute; bottom: 40px; left: 40px; z-index: 4\">
                                    ";
                    // line 12
                    echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 12), "content", [], "any", false, false, false, 12);
                    echo "
                                </div>
                                ";
                    // line 14
                    if (twig_get_attribute($this->env, $this->source, $context["image"], "contentBackground", [], "any", false, false, false, 14)) {
                        // line 15
                        echo "                                    <div style=\"height: 100%; z-index: 2; position: absolute; top: 0; width: 50%; background: linear-gradient(to bottom left, transparent 0%, transparent 50%, ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentBackground", [], "any", false, false, false, 15), "html", null, true);
                        echo " 50%, ";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentBackground", [], "any", false, false, false, 15), "html", null, true);
                        echo " 100%);\"></div>
                                ";
                    }
                    // line 17
                    echo "                            </div>
                        </a>
                    </div>

                    ";
                    // line 21
                    $context["counter"] = ((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 21, $this->source); })()) + 1);
                    // line 22
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "            ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Banner/PositionType/directional.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  114 => 24,  107 => 23,  101 => 22,  99 => 21,  93 => 17,  85 => 15,  83 => 14,  78 => 12,  73 => 10,  69 => 9,  60 => 8,  55 => 7,  50 => 6,  47 => 5,  45 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container\">
    <div class=\"banners-two-columns\">
        <div class=\"row pb-4\">
            {% set counter = 1 %}

            {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                {% for image in banner.images %}
                    <div class=\"col-12 col-md-6 {% if counter % 2 %}pl-0 pr-2{% else %}pr-0 pl-2{% endif %}\">
                        <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                            <div style=\"width: 100%; background-image: url({{ image.path|imagine_filter('omni_sylius_banner') }}); background-repeat: no-repeat; background-size: cover; height: 300px\">
                                <div style=\"position: absolute; bottom: 40px; left: 40px; z-index: 4\">
                                    {{ image.translation.content|raw }}
                                </div>
                                {% if image.contentBackground %}
                                    <div style=\"height: 100%; z-index: 2; position: absolute; top: 0; width: 50%; background: linear-gradient(to bottom left, transparent 0%, transparent 50%, {{ image.contentBackground }} 50%, {{ image.contentBackground }} 100%);\"></div>
                                {% endif %}
                            </div>
                        </a>
                    </div>

                    {% set counter = counter + 1 %}
                {% endfor %}
            {% endfor %}
        </div>
    </div>
</div>
", "Banner/PositionType/directional.html.twig", "/var/www/html/templates/Banner/PositionType/directional.html.twig");
    }
}
