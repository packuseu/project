<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle::_footer.html.twig */
class __TwigTemplate_c0302460865996fd7b10e49337fe558f588de0159a5806aaf6cf899417c10b42 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle::_footer.html.twig"));

        // line 1
        echo "<footer id=\"footer\" class=\"ui inverted vertical footer segment pt-5\">
    <div class=\"ui container\">
        <div class=\"footer__grid row\">
            ";
        // line 4
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.before_footer"]);
        echo "

            ";
        // line 6
        $context["footerNode"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeWithChildrenByType("footer_menu", 2);
        // line 7
        echo "
            ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(((twig_get_attribute($this->env, $this->source, ($context["footerNode"] ?? null), "children", [], "any", true, true, false, 8)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["footerNode"] ?? null), "children", [], "any", false, false, false, 8), [])) : ([])));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        foreach ($context['_seq'] as $context["_key"] => $context["placeholder"]) {
            if (twig_get_attribute($this->env, $this->source, $context["placeholder"], "enabled", [], "any", false, false, false, 8)) {
                // line 9
                echo "                ";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 9) == true)) {
                    // line 10
                    echo "                    <div class=\"three wide column col-12 col-lg-6\">
                        <h4 class=\"ui inverted header\">";
                    // line 11
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["placeholder"], "translation", [], "any", false, false, false, 11), "title", [], "any", false, false, false, 11)), "html", null, true);
                    echo "</h4>
                        <div class=\"row ui inverted link\">
                            <div class=\"col-12 col-lg-6\">
                                ";
                    // line 14
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["placeholder"], "children", [], "any", false, false, false, 14));
                    foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                        if ((twig_get_attribute($this->env, $this->source, $context["item"], "enabled", [], "any", false, false, false, 14) && ($context["key"] % 2 == 0))) {
                            // line 15
                            echo "                                    ";
                            $context["url"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeUrl($context["item"]);
                            // line 16
                            echo "                                    <a class=\"item\"";
                            if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 16, $this->source); })()), "url", [], "any", false, false, false, 16)) {
                                echo " href=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 16, $this->source); })()), "url", [], "any", false, false, false, 16), "html", null, true);
                                echo "\"";
                                if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 16, $this->source); })()), "target", [], "any", false, false, false, 16)) {
                                    echo " target=\"";
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 16, $this->source); })()), "target", [], "any", false, false, false, 16), "html", null, true);
                                    echo "\"";
                                }
                            }
                            echo ">";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "translation", [], "any", false, false, false, 16), "title", [], "any", false, false, false, 16), "html", null, true);
                            echo "</a>
                                ";
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 18
                    echo "                            </div>
                            <div class=\"col-12 col-lg-6\">
                                ";
                    // line 20
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["placeholder"], "children", [], "any", false, false, false, 20));
                    foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                        if ((twig_get_attribute($this->env, $this->source, $context["item"], "enabled", [], "any", false, false, false, 20) && ($context["key"] % 2 != 0))) {
                            // line 21
                            echo "                                    ";
                            $context["url"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeUrl($context["item"]);
                            // line 22
                            echo "                                    <a class=\"item\"";
                            if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 22, $this->source); })()), "url", [], "any", false, false, false, 22)) {
                                echo " href=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 22, $this->source); })()), "url", [], "any", false, false, false, 22), "html", null, true);
                                echo "\"";
                                if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 22, $this->source); })()), "target", [], "any", false, false, false, 22)) {
                                    echo " target=\"";
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 22, $this->source); })()), "target", [], "any", false, false, false, 22), "html", null, true);
                                    echo "\"";
                                }
                            }
                            echo ">";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "translation", [], "any", false, false, false, 22), "title", [], "any", false, false, false, 22), "html", null, true);
                            echo "</a>
                                ";
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 24
                    echo "                            </div>
                        </div>
                    </div>
                    ";
                } else {
                    // line 28
                    echo "                        <div class=\"three wide column col-12 col-lg-3\">
                            <h4 class=\"ui inverted header\">";
                    // line 29
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["placeholder"], "translation", [], "any", false, false, false, 29), "title", [], "any", false, false, false, 29)), "html", null, true);
                    echo "</h4>
                            <div class=\"ui inverted link\">
                                ";
                    // line 31
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["placeholder"], "children", [], "any", false, false, false, 31));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        if (twig_get_attribute($this->env, $this->source, $context["item"], "enabled", [], "any", false, false, false, 31)) {
                            // line 32
                            echo "                                    ";
                            $context["url"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeUrl($context["item"]);
                            // line 33
                            echo "                                    <a class=\"item\"";
                            if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 33, $this->source); })()), "url", [], "any", false, false, false, 33)) {
                                echo " href=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 33, $this->source); })()), "url", [], "any", false, false, false, 33), "html", null, true);
                                echo "\"";
                                if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 33, $this->source); })()), "target", [], "any", false, false, false, 33)) {
                                    echo " target=\"";
                                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 33, $this->source); })()), "target", [], "any", false, false, false, 33), "html", null, true);
                                    echo "\"";
                                }
                            }
                            echo ">";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "translation", [], "any", false, false, false, 33), "title", [], "any", false, false, false, 33), "html", null, true);
                            echo "</a>
                                ";
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 35
                    echo "                            </div>
                        </div>
                ";
                }
                // line 38
                echo "
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['placeholder'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "
            <div class=\"footer__grid__col\">
                <h6 class=\"footer__grid__heading footer__grid__heading--white copyright\">&copy; ";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 42, $this->source); })()), "channel", [], "any", false, false, false, 42), "name", [], "any", false, false, false, 42), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo ". ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("shop24.ui.footer"), "html", null, true);
        echo "</h6>
            </div>

            ";
        // line 45
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.after_footer"]);
        echo "
        </div>
    </div>
</footer>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle::_footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 45,  203 => 42,  199 => 40,  188 => 38,  183 => 35,  162 => 33,  159 => 32,  154 => 31,  149 => 29,  146 => 28,  140 => 24,  119 => 22,  116 => 21,  111 => 20,  107 => 18,  86 => 16,  83 => 15,  78 => 14,  72 => 11,  69 => 10,  66 => 9,  55 => 8,  52 => 7,  50 => 6,  45 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer id=\"footer\" class=\"ui inverted vertical footer segment pt-5\">
    <div class=\"ui container\">
        <div class=\"footer__grid row\">
            {{ sonata_block_render_event('sylius.shop.layout.before_footer') }}

            {% set footerNode = omni_sylius_get_node_with_children_by_type('footer_menu', 2) %}

            {% for placeholder in footerNode.children|default([]) if placeholder.enabled %}
                {% if loop.first == true %}
                    <div class=\"three wide column col-12 col-lg-6\">
                        <h4 class=\"ui inverted header\">{{ placeholder.translation.title|trans }}</h4>
                        <div class=\"row ui inverted link\">
                            <div class=\"col-12 col-lg-6\">
                                {% for key, item in placeholder.children if item.enabled and key is even %}
                                    {% set url = omni_sylius_get_node_url(item) %}
                                    <a class=\"item\"{% if url.url %} href=\"{{ url.url }}\"{% if url.target %} target=\"{{ url.target }}\"{% endif %}{% endif %}>{{ item.translation.title }}</a>
                                {% endfor %}
                            </div>
                            <div class=\"col-12 col-lg-6\">
                                {% for key, item in placeholder.children if item.enabled and key is odd %}
                                    {% set url = omni_sylius_get_node_url(item) %}
                                    <a class=\"item\"{% if url.url %} href=\"{{ url.url }}\"{% if url.target %} target=\"{{ url.target }}\"{% endif %}{% endif %}>{{ item.translation.title }}</a>
                                {% endfor %}
                            </div>
                        </div>
                    </div>
                    {% else %}
                        <div class=\"three wide column col-12 col-lg-3\">
                            <h4 class=\"ui inverted header\">{{ placeholder.translation.title|trans }}</h4>
                            <div class=\"ui inverted link\">
                                {% for item in placeholder.children if item.enabled %}
                                    {% set url = omni_sylius_get_node_url(item) %}
                                    <a class=\"item\"{% if url.url %} href=\"{{ url.url }}\"{% if url.target %} target=\"{{ url.target }}\"{% endif %}{% endif %}>{{ item.translation.title }}</a>
                                {% endfor %}
                            </div>
                        </div>
                {% endif %}

            {% endfor %}

            <div class=\"footer__grid__col\">
                <h6 class=\"footer__grid__heading footer__grid__heading--white copyright\">&copy; {{sylius.channel.name}} {{ 'now' | date('Y') }}. {{ 'shop24.ui.footer'|trans }}</h6>
            </div>

            {{ sonata_block_render_event('sylius.shop.layout.after_footer') }}
        </div>
    </div>
</footer>
", "SyliusShopBundle::_footer.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/_footer.html.twig");
    }
}
