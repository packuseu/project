<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Banner/PositionType/three_columns.html.twig */
class __TwigTemplate_604fc4c45a404d1ae59da26bc7191a56251aeed18a3181c274c53121daf41987 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Banner/PositionType/three_columns.html.twig"));

        // line 1
        echo "<div class=\"container\">
    <div class=\"banners-three-columns\">
        <div class=\"row\">
            ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 4, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 4, $this->source); })()), "channel", [], "array", false, false, false, 4)], "method", false, false, false, 4)) {
                // line 5
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 6
                    echo "                    <div class=\"col-12 col-md-4\">
                        <a class=\"card-link\" href=\"";
                    // line 7
                    echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 7), "link", [], "any", true, true, false, 7)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 7), "link", [], "any", false, false, false, 7), "javascript:;")) : ("javascript:;")), "html", null, true);
                    echo "\">
                            <img class=\"three-column-card-img\" src=\"";
                    // line 8
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 8), "omni_sylius_banner"), "html", null, true);
                    echo "\">
                        </a>
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 12
                echo "            ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Banner/PositionType/three_columns.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 13,  72 => 12,  62 => 8,  58 => 7,  55 => 6,  50 => 5,  45 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"container\">
    <div class=\"banners-three-columns\">
        <div class=\"row\">
            {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                {% for image in banner.images %}
                    <div class=\"col-12 col-md-4\">
                        <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                            <img class=\"three-column-card-img\" src=\"{{ image.path|imagine_filter('omni_sylius_banner') }}\">
                        </a>
                    </div>
                {% endfor %}
            {% endfor %}
        </div>
    </div>
</div>
", "Banner/PositionType/three_columns.html.twig", "/var/www/html/templates/Banner/PositionType/three_columns.html.twig");
    }
}
