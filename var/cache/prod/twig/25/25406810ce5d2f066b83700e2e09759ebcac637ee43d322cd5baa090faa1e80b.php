<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig */
class __TwigTemplate_2cee246d965d44761190d1dae35f799a3e0fa4769c4d4fbbffbc196750af6466 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig"));

        // line 1
        $macros["buttons"] = $this->macros["buttons"] = $this->loadTemplate("@SyliusUi/Macro/buttons.html.twig", "@OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $macros["tree"] = $this->macros["tree"] = $this;
        // line 4
        echo "
";
        // line 46
        echo "
<style>
    .node-tree-show,
    .node-tree-hide {
        cursor: pointer
    }
</style>

<a href=\"";
        // line 54
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_node_create");
        echo "\" class=\"ui large fluid labeled icon primary button\">
    <i class=\"plus icon\"></i>
    ";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.create"), "html", null, true);
        echo "
</a>

<div class=\"ui segment\">
    <div class=\"ui list sylius-sortable-list\" id=\"node-tree\">
        ";
        // line 61
        echo twig_call_macro($macros["tree"], "macro_render", [(isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 61, $this->source); })())], 61, $context, $this->getSourceContext());
        echo "
    </div>
</div>

<script type=\"text/javascript\">
  const nodeTree = document.getElementById('node-tree');

  // Show tree to current editing item.
  let itemCurrent = document.getElementById('node-tree-current');
  if (itemCurrent) {
    itemCurrent = itemCurrent.innerText;

    let itemCurrentDiv = nodeTree.querySelector('.item[data-id=\"'+itemCurrent+'\"]');
    let parent = itemCurrentDiv.parentNode;
    let nodeTreeRoot = false;

    while(nodeTreeRoot === false)
    {
      if (parent.className === 'list') {
        toggleElement(parent);
      }

      if (parent.className === 'item') {
        let opendIcon = parent.querySelector('.node-tree-show');
        let closeIcon = parent.querySelector('.node-tree-hide');

        toggleElement(opendIcon);
        toggleElement(closeIcon);
      }

      if (parent.id === 'node-tree') {
        nodeTreeRoot = true;
      }

      parent = parent.parentNode;
    }
  }

  function showSubmenuClick(element) {
    let parent = element.parentNode;
    let closeIcon = parent.querySelector('.node-tree-hide');
    let firstList = parent.querySelector('.list');

    toggleElement(element);
    toggleElement(closeIcon);
    toggleElement(firstList);
  }

  function hideSubmenuClick(element) {
    let parent = element.parentNode;
    let showIcon = parent.querySelector('.node-tree-show');
    let firstList = parent.querySelector('.list');

    toggleElement(element);
    toggleElement(showIcon);
    toggleElement(firstList);
  }

  function toggleElement(element) {
    if (element.style.display === 'none') {
      element.style.display = 'table-cell';
    } else {
      element.style.display = 'none';
    }
  }
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function macro_render($__resources__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "resources" => $__resources__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "render"));

            // line 6
            echo "    ";
            $macros["buttons"] = $this->loadTemplate("@SyliusUi/Macro/buttons.html.twig", "@OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig", 6)->unwrap();
            // line 7
            echo "    ";
            $macros["tree"] = $this;
            // line 8
            echo "
    ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 9, $this->source); })()));
            foreach ($context['_seq'] as $context["_key"] => $context["node"]) {
                if ((twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 9) != null)) {
                    // line 10
                    echo "        <div class=\"item\" data-id=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 10), "html", null, true);
                    echo "\">
            ";
                    // line 11
                    if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["node"], "children", [], "any", false, false, false, 11))) {
                        // line 12
                        echo "                <i class=\"plus circle icon node-tree-show\" onClick=\"showSubmenuClick(this);\"></i>
                <i class=\"minus square icon node-tree-hide\" onClick=\"hideSubmenuClick(this);\" style=\"display: none;\"></i>
            ";
                    } else {
                        // line 15
                        echo "                ";
                        // line 16
                        echo "                <i class=\"icon\"></i>
            ";
                    }
                    // line 18
                    echo "            <i class=\"folder icon\"></i>
            <div class=\"content\">
                <div class=\"header\">
                    <span>";
                    // line 21
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["node"], "translation", [], "any", false, false, false, 21), "title", [], "any", false, false, false, 21), "html", null, true);
                    echo "</span>
                    <br>
                    <div class=\"ui mini buttons\" style=\"margin-top: 5px;\">
                        ";
                    // line 24
                    if ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeNestable($context["node"])) {
                        // line 25
                        echo "                            ";
                        echo twig_call_macro($macros["buttons"], "macro_create", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_node_create_for_parent", ["id" => twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 25)]), "sylius.ui.add"], 25, $context, $this->getSourceContext());
                        echo "
                        ";
                    } else {
                        // line 27
                        echo "                            ";
                        echo twig_call_macro($macros["buttons"], "macro_default", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_node_create_for_parent", ["id" => twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 27)]), "sylius.ui.add", "create_node", "plus", "disabled"], 27, $context, $this->getSourceContext());
                        echo "
                        ";
                    }
                    // line 29
                    echo "                        ";
                    echo twig_call_macro($macros["buttons"], "macro_edit", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_node_update", ["id" => twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 29)]), null, null, false], 29, $context, $this->getSourceContext());
                    echo "
                        ";
                    // line 30
                    echo twig_call_macro($macros["buttons"], "macro_delete", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_node_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 30)]), null, false, twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 30)], 30, $context, $this->getSourceContext());
                    echo "
                        <a class=\"ui icon button sylius-taxon-move-up\" data-url=\"";
                    // line 31
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_node_move_up", ["id" => twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 31)]), "html", null, true);
                    echo "\">
                            <i class=\"icon arrow up\"></i>
                        </a>
                        <a class=\"ui icon button sylius-taxon-move-down\" data-url=\"";
                    // line 34
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_node_move_down", ["id" => twig_get_attribute($this->env, $this->source, $context["node"], "id", [], "any", false, false, false, 34)]), "html", null, true);
                    echo "\">
                            <i class=\"icon arrow down\"></i>
                        </a>
                    </div>
                </div>
                <div class=\"list\" style=\"display: none;\">
                    ";
                    // line 40
                    echo twig_call_macro($macros["tree"], "macro_render", [twig_get_attribute($this->env, $this->source, $context["node"], "children", [], "any", false, false, false, 40)], 40, $context, $this->getSourceContext());
                    echo "
                </div>
            </div>
        </div>
    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['node'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 40,  235 => 34,  229 => 31,  225 => 30,  220 => 29,  214 => 27,  208 => 25,  206 => 24,  200 => 21,  195 => 18,  191 => 16,  189 => 15,  184 => 12,  182 => 11,  177 => 10,  172 => 9,  169 => 8,  166 => 7,  163 => 6,  147 => 5,  73 => 61,  65 => 56,  60 => 54,  50 => 46,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusUi/Macro/buttons.html.twig' as buttons %}

{% import _self as tree %}

{% macro render(resources) %}
    {% import '@SyliusUi/Macro/buttons.html.twig' as buttons %}
    {% import _self as tree %}

    {% for node in resources if node.id != null %}
        <div class=\"item\" data-id=\"{{ node.id }}\">
            {% if node.children|length %}
                <i class=\"plus circle icon node-tree-show\" onClick=\"showSubmenuClick(this);\"></i>
                <i class=\"minus square icon node-tree-hide\" onClick=\"hideSubmenuClick(this);\" style=\"display: none;\"></i>
            {% else %}
                {# empty icon to take space #}
                <i class=\"icon\"></i>
            {% endif %}
            <i class=\"folder icon\"></i>
            <div class=\"content\">
                <div class=\"header\">
                    <span>{{ node.translation.title }}</span>
                    <br>
                    <div class=\"ui mini buttons\" style=\"margin-top: 5px;\">
                        {% if omni_sylius_is_node_nestable(node) %}
                            {{ buttons.create(path('omni_sylius_admin_node_create_for_parent', { 'id': node.id }), 'sylius.ui.add') }}
                        {% else %}
                            {{ buttons.default(path('omni_sylius_admin_node_create_for_parent', { 'id': node.id }), 'sylius.ui.add', 'create_node', 'plus', 'disabled') }}
                        {% endif %}
                        {{ buttons.edit(path('omni_sylius_admin_node_update', { 'id': node.id }), null, null, false) }}
                        {{ buttons.delete(path('omni_sylius_admin_node_delete', { 'id': node.id }), null, false, node.id) }}
                        <a class=\"ui icon button sylius-taxon-move-up\" data-url=\"{{ path('omni_sylius_admin_node_move_up', { id: node.id }) }}\">
                            <i class=\"icon arrow up\"></i>
                        </a>
                        <a class=\"ui icon button sylius-taxon-move-down\" data-url=\"{{ path('omni_sylius_admin_node_move_down', { id: node.id }) }}\">
                            <i class=\"icon arrow down\"></i>
                        </a>
                    </div>
                </div>
                <div class=\"list\" style=\"display: none;\">
                    {{ tree.render(node.children) }}
                </div>
            </div>
        </div>
    {% endfor %}
{% endmacro %}

<style>
    .node-tree-show,
    .node-tree-hide {
        cursor: pointer
    }
</style>

<a href=\"{{ path('omni_sylius_admin_node_create') }}\" class=\"ui large fluid labeled icon primary button\">
    <i class=\"plus icon\"></i>
    {{ 'sylius.ui.create'|trans }}
</a>

<div class=\"ui segment\">
    <div class=\"ui list sylius-sortable-list\" id=\"node-tree\">
        {{ tree.render(resources) }}
    </div>
</div>

<script type=\"text/javascript\">
  const nodeTree = document.getElementById('node-tree');

  // Show tree to current editing item.
  let itemCurrent = document.getElementById('node-tree-current');
  if (itemCurrent) {
    itemCurrent = itemCurrent.innerText;

    let itemCurrentDiv = nodeTree.querySelector('.item[data-id=\"'+itemCurrent+'\"]');
    let parent = itemCurrentDiv.parentNode;
    let nodeTreeRoot = false;

    while(nodeTreeRoot === false)
    {
      if (parent.className === 'list') {
        toggleElement(parent);
      }

      if (parent.className === 'item') {
        let opendIcon = parent.querySelector('.node-tree-show');
        let closeIcon = parent.querySelector('.node-tree-hide');

        toggleElement(opendIcon);
        toggleElement(closeIcon);
      }

      if (parent.id === 'node-tree') {
        nodeTreeRoot = true;
      }

      parent = parent.parentNode;
    }
  }

  function showSubmenuClick(element) {
    let parent = element.parentNode;
    let closeIcon = parent.querySelector('.node-tree-hide');
    let firstList = parent.querySelector('.list');

    toggleElement(element);
    toggleElement(closeIcon);
    toggleElement(firstList);
  }

  function hideSubmenuClick(element) {
    let parent = element.parentNode;
    let showIcon = parent.querySelector('.node-tree-show');
    let firstList = parent.querySelector('.list');

    toggleElement(element);
    toggleElement(showIcon);
    toggleElement(firstList);
  }

  function toggleElement(element) {
    if (element.style.display === 'none') {
      element.style.display = 'table-cell';
    } else {
      element.style.display = 'none';
    }
  }
</script>
", "@OmniSyliusCmsPlugin/Node/_treeWithButtons.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Node/_treeWithButtons.html.twig");
    }
}
