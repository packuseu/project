<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Show:_addToCart.html.twig */
class __TwigTemplate_f60293b2fcb380663ffc2f0c6c1f40c18b78507f7bb543a988c230c89583e492 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Show:_addToCart.html.twig"));

        // line 1
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Product/Show:_addToCart.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["product"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["order_item"]) || array_key_exists("order_item", $context) ? $context["order_item"] : (function () { throw new RuntimeError('Variable "order_item" does not exist.', 3, $this->source); })()), "variant", [], "any", false, false, false, 3), "product", [], "any", false, false, false, 3);
        // line 4
        echo "
";
        // line 5
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), [0 => "@SyliusShop/Form/theme.html.twig"], true);
        // line 6
        echo "
";
        // line 7
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.before_add_to_cart", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 7, $this->source); })()), "order_item" => (isset($context["order_item"]) || array_key_exists("order_item", $context) ? $context["order_item"] : (function () { throw new RuntimeError('Variable "order_item" does not exist.', 7, $this->source); })())]]);
        echo "
<div class=\"description-padding\">
";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_ajax_cart_add_item", ["productId" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 9, $this->source); })()), "id", [], "any", false, false, false, 9)]), "attr" => ["id" => "sylius-product-adding-to-cart", "data-js-add-to-cart" => "form", "class" => "loadable", "novalidate" => "novalidate", "data-redirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_show", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 9, $this->source); })()), "slug", [], "any", false, false, false, 9)])]]);
        echo "
    ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), 'errors');
        echo "
    <div class=\"alert alert-danger d-none\" data-js-add-to-cart=\"error\"></div>
    ";
        // line 12
        if ( !twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 12, $this->source); })()), "simple", [], "any", false, false, false, 12)) {
            // line 13
            echo "        ";
            if (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 13, $this->source); })()), "variantSelectionMethodChoice", [], "any", false, false, false, 13)) {
                // line 14
                echo "            ";
                $this->loadTemplate("@SyliusShop/Product/Show/_variants.html.twig", "SyliusShopBundle:Product/Show:_addToCart.html.twig", 14)->display($context);
                // line 15
                echo "        ";
            } else {
                // line 16
                echo "            ";
                $this->loadTemplate("@SyliusShop/Product/Show/_options.html.twig", "SyliusShopBundle:Product/Show:_addToCart.html.twig", 16)->display($context);
                // line 17
                echo "            <div class=\"col-12 text-right\">
                <a id=\"cartResetOptions\" href=\"#\">
                    <span class=\"btn-icon\">";
                // line 19
                echo twig_call_macro($macros["icons"], "macro_reload", [], 19, $context, $this->getSourceContext());
                echo "</span>
                    ";
                // line 20
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.refresh_variants"), "html", null, true);
                echo "
                </a>
                <br/>
                <br/>
            </div>
        ";
            }
            // line 26
            echo "    ";
        }
        // line 27
        echo "
    <div class=\"row align-items-end\">
        <div class=\"col-4\">
            ";
        // line 30
        $context["step"] = 1;
        // line 31
        echo "
            ";
        // line 32
        if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 32, $this->source); })()), "quantityStep", [], "any", false, false, false, 32)) && (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 32, $this->source); })()), "quantityStep", [], "any", false, false, false, 32) > 1))) {
            // line 33
            echo "                ";
            $context["step"] = twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 33, $this->source); })()), "quantityStep", [], "any", false, false, false, 33);
            // line 34
            echo "            ";
        }
        // line 35
        echo "
            <div class=\"form-group\">
                <label for=\"sylius_add_to_cart_cartItem_quantity\" class=\"required\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.form.quantity"), "html", null, true);
        echo "</label>
                <input type=\"number\" id=\"sylius_add_to_cart_cartItem_quantity\" name=\"sylius_add_to_cart[cartItem][quantity]\" required=\"required\" min=\"";
        // line 38
        echo twig_escape_filter($this->env, (isset($context["step"]) || array_key_exists("step", $context) ? $context["step"] : (function () { throw new RuntimeError('Variable "step" does not exist.', 38, $this->source); })()), "html", null, true);
        echo "\" class=\"form-control js-quantity-step\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["step"]) || array_key_exists("step", $context) ? $context["step"] : (function () { throw new RuntimeError('Variable "step" does not exist.', 38, $this->source); })()), "html", null, true);
        echo "\" step=\"";
        echo twig_escape_filter($this->env, (isset($context["step"]) || array_key_exists("step", $context) ? $context["step"] : (function () { throw new RuntimeError('Variable "step" does not exist.', 38, $this->source); })()), "html", null, true);
        echo "\" data-step=\"";
        echo twig_escape_filter($this->env, (isset($context["step"]) || array_key_exists("step", $context) ? $context["step"] : (function () { throw new RuntimeError('Variable "step" does not exist.', 38, $this->source); })()), "html", null, true);
        echo "\">
            </div>

            ";
        // line 41
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.add_to_cart_form", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 41, $this->source); })()), "order_item" => (isset($context["order_item"]) || array_key_exists("order_item", $context) ? $context["order_item"] : (function () { throw new RuntimeError('Variable "order_item" does not exist.', 41, $this->source); })())]]);
        echo "
        </div>
        <div class=\"col\">
            <div class=\"form-group\">
                <button type=\"submit\" class=\"btn btn-block btn-primary\" id=\"addToCartButton\">
                    <span class=\"btn-icon\">";
        // line 46
        echo twig_call_macro($macros["icons"], "macro_cart", [], 46, $context, $this->getSourceContext());
        echo "</span>
                    ";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.add_to_cart"), "html", null, true);
        echo "
                </button>
                <div class=\"btn btn-block btn-primary d-none\" id=\"quote-button\" data-toggle=\"modal\" data-target=\"#quote-modal\">
                    <span class=\"btn-icon\">";
        // line 50
        echo twig_call_macro($macros["icons"], "macro_clipboardCheck", [], 50, $context, $this->getSourceContext());
        echo "</span>
                    ";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.quote.get_a_quote"), "html", null, true);
        echo "
                </div>
            </div>
        </div>
        <div class=\"col-12 text-center d-none\" id=\"cannotAddToCartMessage\">
            ";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.add_to_cart.not_available"), "html", null, true);
        echo "
        </div>
    </div>
    ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 59, $this->source); })()), "_token", [], "any", false, false, false, 59), 'row');
        echo "
";
        // line 60
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 60, $this->source); })()), 'form_end', ["render_rest" => false]);
        echo "
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Show:_addToCart.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 60,  177 => 59,  171 => 56,  163 => 51,  159 => 50,  153 => 47,  149 => 46,  141 => 41,  129 => 38,  125 => 37,  121 => 35,  118 => 34,  115 => 33,  113 => 32,  110 => 31,  108 => 30,  103 => 27,  100 => 26,  91 => 20,  87 => 19,  83 => 17,  80 => 16,  77 => 15,  74 => 14,  71 => 13,  69 => 12,  64 => 10,  60 => 9,  55 => 7,  52 => 6,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% set product = order_item.variant.product %}

{% form_theme form '@SyliusShop/Form/theme.html.twig' %}

{{ sonata_block_render_event('sylius.shop.product.show.before_add_to_cart', {'product': product, 'order_item': order_item}) }}
<div class=\"description-padding\">
{{ form_start(form, {'action': path('sylius_shop_ajax_cart_add_item', {'productId': product.id}), 'attr': {'id': 'sylius-product-adding-to-cart', 'data-js-add-to-cart': 'form', 'class': 'loadable', 'novalidate': 'novalidate', 'data-redirect': path('sylius_shop_product_show', {slug: product.slug})}}) }}
    {{ form_errors(form) }}
    <div class=\"alert alert-danger d-none\" data-js-add-to-cart=\"error\"></div>
    {% if not product.simple %}
        {% if product.variantSelectionMethodChoice %}
            {% include '@SyliusShop/Product/Show/_variants.html.twig' %}
        {% else %}
            {% include '@SyliusShop/Product/Show/_options.html.twig' %}
            <div class=\"col-12 text-right\">
                <a id=\"cartResetOptions\" href=\"#\">
                    <span class=\"btn-icon\">{{ icons.reload() }}</span>
                    {{ 'sylius.ui.refresh_variants'|trans }}
                </a>
                <br/>
                <br/>
            </div>
        {% endif %}
    {% endif %}

    <div class=\"row align-items-end\">
        <div class=\"col-4\">
            {% set step = 1 %}

            {% if product.quantityStep is not null and product.quantityStep > 1 %}
                {% set step = product.quantityStep %}
            {% endif %}

            <div class=\"form-group\">
                <label for=\"sylius_add_to_cart_cartItem_quantity\" class=\"required\">{{ 'app.form.quantity'|trans }}</label>
                <input type=\"number\" id=\"sylius_add_to_cart_cartItem_quantity\" name=\"sylius_add_to_cart[cartItem][quantity]\" required=\"required\" min=\"{{ step }}\" class=\"form-control js-quantity-step\" value=\"{{ step }}\" step=\"{{ step }}\" data-step=\"{{ step }}\">
            </div>

            {{ sonata_block_render_event('sylius.shop.product.show.add_to_cart_form', {'product': product, 'order_item': order_item}) }}
        </div>
        <div class=\"col\">
            <div class=\"form-group\">
                <button type=\"submit\" class=\"btn btn-block btn-primary\" id=\"addToCartButton\">
                    <span class=\"btn-icon\">{{ icons.cart() }}</span>
                    {{ 'sylius.ui.add_to_cart'|trans }}
                </button>
                <div class=\"btn btn-block btn-primary d-none\" id=\"quote-button\" data-toggle=\"modal\" data-target=\"#quote-modal\">
                    <span class=\"btn-icon\">{{ icons.clipboardCheck() }}</span>
                    {{ 'app.quote.get_a_quote'|trans }}
                </div>
            </div>
        </div>
        <div class=\"col-12 text-center d-none\" id=\"cannotAddToCartMessage\">
            {{ 'app.ui.add_to_cart.not_available'|trans }}
        </div>
    </div>
    {{ form_row(form._token) }}
{{ form_end(form, {'render_rest': false}) }}
</div>
", "SyliusShopBundle:Product/Show:_addToCart.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Product/Show/_addToCart.html.twig");
    }
}
