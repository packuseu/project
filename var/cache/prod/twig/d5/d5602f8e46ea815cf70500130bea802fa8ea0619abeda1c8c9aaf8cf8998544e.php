<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Common/Order:_payments.html.twig */
class __TwigTemplate_99dd02b1fe51490592d45dfe34df9ff923d822bdc17f7c590663a604826eb024 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Common/Order:_payments.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Common/Order:_payments.html.twig", 1)->unwrap();
        // line 2
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Common/Order:_payments.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 4, $this->source); })()), "payments", [], "any", false, false, false, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["payment"]) {
            // line 5
            echo "<div class=\"card bg-light\">
    <div class=\"card-body\">
        <div class=\"d-flex align-items-center\">
            <div class=\"pr-3\" style=\"font-size:2rem;\">";
            // line 8
            echo twig_call_macro($macros["icons"], "macro_creditCard", [], 8, $context, $this->getSourceContext());
            echo "</div>
            <div>
                <div><strong>";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["payment"], "method", [], "any", false, false, false, 10), "html", null, true);
            echo "</strong></div>
                <div>";
            // line 11
            echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, $context["payment"], "amount", [], "any", false, false, false, 11), twig_get_attribute($this->env, $this->source, $context["payment"], "currencyCode", [], "any", false, false, false, 11)], 11, $context, $this->getSourceContext());
            echo "</div>
            </div>
        </div>
    </div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Common/Order:_payments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 11,  61 => 10,  56 => 8,  51 => 5,  47 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}
{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% for payment in order.payments %}
<div class=\"card bg-light\">
    <div class=\"card-body\">
        <div class=\"d-flex align-items-center\">
            <div class=\"pr-3\" style=\"font-size:2rem;\">{{ icons.creditCard() }}</div>
            <div>
                <div><strong>{{ payment.method }}</strong></div>
                <div>{{ money.format(payment.amount, payment.currencyCode) }}</div>
            </div>
        </div>
    </div>
</div>
{% endfor %}
", "SyliusShopBundle:Common/Order:_payments.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Common/Order/_payments.html.twig");
    }
}
