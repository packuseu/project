<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusBannerPlugin:Banner:banner.html.twig */
class __TwigTemplate_8ce65e43966cc0dae899934f2aad8746cac1667b9c14db8f55196fae057df716 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'banner' => [$this, 'block_banner'],
            'banner_link_start' => [$this, 'block_banner_link_start'],
            'image' => [$this, 'block_image'],
            'banner_link_end' => [$this, 'block_banner_link_end'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusBannerPlugin:Banner:banner.html.twig"));

        // line 1
        echo "

";
        // line 3
        if ((twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "image", [], "any", true, true, false, 3) &&  !(null === twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 3, $this->source); })()), "image", [], "any", false, false, false, 3)))) {
            // line 4
            echo "    ";
            $context["image"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 4, $this->source); })()), "getImagesByType", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 4, $this->source); })()), "image", [], "any", false, false, false, 4)], "method", false, false, false, 4), 0, [], "array", false, false, false, 4);
        } else {
            // line 6
            echo "    ";
            $context["image"] = twig_get_attribute($this->env, $this->source, (isset($context["banner"]) || array_key_exists("banner", $context) ? $context["banner"] : (function () { throw new RuntimeError('Variable "banner" does not exist.', 6, $this->source); })()), "image", [], "any", false, false, false, 6);
        }
        // line 9
        if (((array_key_exists("image", $context) &&  !(null === (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 9, $this->source); })()))) && twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 9, $this->source); })()), "path", [], "any", false, false, false, 9))) {
            // line 10
            $context["path"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 10, $this->source); })()), "path", [], "any", false, false, false, 10), "omni_sylius_banner");
        }
        // line 13
        $this->displayBlock('banner', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_banner($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner"));

        // line 14
        echo "    ";
        if (((array_key_exists("image", $context) &&  !(null === (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()))) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 14, $this->source); })()), "translation", [], "any", false, false, false, 14), "link", [], "any", false, false, false, 14)))) {
            // line 15
            echo "        ";
            $this->displayBlock('banner_link_start', $context, $blocks);
            // line 18
            echo "    ";
        }
        // line 19
        echo "    ";
        if (array_key_exists("path", $context)) {
            // line 20
            echo "        ";
            $this->displayBlock('image', $context, $blocks);
            // line 23
            echo "    ";
        }
        // line 24
        echo "    ";
        if (((array_key_exists("image", $context) &&  !(null === (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 24, $this->source); })()))) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 24, $this->source); })()), "translation", [], "any", false, false, false, 24), "link", [], "any", false, false, false, 24)))) {
            // line 25
            echo "        ";
            $this->displayBlock('banner_link_end', $context, $blocks);
            // line 28
            echo "    ";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 15
    public function block_banner_link_start($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner_link_start"));

        // line 16
        echo "            <a href=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["image"]) || array_key_exists("image", $context) ? $context["image"] : (function () { throw new RuntimeError('Variable "image" does not exist.', 16, $this->source); })()), "translation", [], "any", false, false, false, 16), "link", [], "any", false, false, false, 16), "html", null, true);
        echo "\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 20
    public function block_image($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "image"));

        // line 21
        echo "            <img src=\"";
        echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 21, $this->source); })()), "html", null, true);
        echo "\">
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 25
    public function block_banner_link_end($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "banner_link_end"));

        // line 26
        echo "            </a>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusBannerPlugin:Banner:banner.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 26,  142 => 25,  132 => 21,  125 => 20,  115 => 16,  108 => 15,  100 => 28,  97 => 25,  94 => 24,  91 => 23,  88 => 20,  85 => 19,  82 => 18,  79 => 15,  76 => 14,  63 => 13,  60 => 10,  58 => 9,  54 => 6,  50 => 4,  48 => 3,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("

{% if options.image is defined and options.image is not null %}
    {% set image = banner.getImagesByType(options.image)[0] %}
{% else %}
    {% set image = banner.image %}
{% endif %}

{%- if image is defined and image is not null and image.path -%}
    {% set path = image.path|imagine_filter('omni_sylius_banner') %}
{%- endif -%}

{% block banner %}
    {% if image is defined and image is not null and image.translation.link is not null %}
        {% block banner_link_start %}
            <a href=\"{{ image.translation.link }}\">
        {% endblock %}
    {% endif %}
    {% if path is defined %}
        {% block image %}
            <img src=\"{{ path }}\">
        {% endblock %}
    {% endif %}
    {% if image is defined and image is not null and image.translation.link is not null %}
        {% block banner_link_end %}
            </a>
        {% endblock %}
    {% endif %}
{% endblock %}
", "OmniSyliusBannerPlugin:Banner:banner.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/Banner/banner.html.twig");
    }
}
