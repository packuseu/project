<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/SyliusAdminBundle/Order/Show/_shipment.html.twig */
class __TwigTemplate_fba4dce4a6d63f349b1c266e8dd6fea151818dc041fad13124762b356694241f extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/SyliusAdminBundle/Order/Show/_shipment.html.twig"));

        // line 1
        $macros["label"] = $this->macros["label"] = $this->loadTemplate("@SyliusUi/Macro/labels.html.twig", "bundles/SyliusAdminBundle/Order/Show/_shipment.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"item\">
    <div class=\"right floated content\">
        ";
        // line 5
        $this->loadTemplate("@SyliusAdmin/Common/Label/shipmentState.html.twig", "bundles/SyliusAdminBundle/Order/Show/_shipment.html.twig", 5)->display(twig_array_merge($context, ["data" => twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 5, $this->source); })()), "state", [], "any", false, false, false, 5)]));
        // line 6
        echo "    </div>
    <i class=\"large truck icon\"></i>
    <div class=\"content\">
        <div class=\"header\">
            ";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 10, $this->source); })()), "method", [], "any", false, false, false, 10), "html", null, true);
        echo "
        </div>
        <div class=\"description\">
            <i class=\"globe icon\"></i>";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 13, $this->source); })()), "method", [], "any", false, false, false, 13), "zone", [], "any", false, false, false, 13), "html", null, true);
        echo "
        </div>
    </div>
    ";
        // line 16
        if (($this->extensions['SM\Extension\Twig\SMExtension']->can((isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 16, $this->source); })()), "pack", "sylius_shipment") && !twig_in_filter("dpd", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 16, $this->source); })()), "method", [], "any", false, false, false, 16), "code", [], "any", false, false, false, 16)))) {
            // line 17
            echo "        <div class=\"ui segment\">
            <form name=\"sylius_shipment_ship\" method=\"post\" action=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("pack", ["orderId" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 18, $this->source); })()), "id", [], "any", false, false, false, 18), "shipmentId" => twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 18, $this->source); })()), "id", [], "any", false, false, false, 18)]), "html", null, true);
            echo "\" class=\"ui loadable form\" novalidate=\"novalidate\">
                <div class=\"ui fluid action input\">
                    <button type=\"submit\" class=\"ui labeled icon teal button\"><i class=\"box icon\"></i>";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.shipment.pack"), "html", null, true);
            echo "</button>
                </div>
            </form>
        </div>
    ";
        }
        // line 25
        echo "    ";
        if ((($this->extensions['SM\Extension\Twig\SMExtension']->can((isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 25, $this->source); })()), "export", "sylius_shipment") && twig_in_filter("dpd", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 25, $this->source); })()), "method", [], "any", false, false, false, 25), "code", [], "any", false, false, false, 25))) && (null === twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 25, $this->source); })()), "tracking", [], "any", false, false, false, 25)))) {
            // line 26
            echo "        <div class=\"ui segment\">
            <form action=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("export_single_shipment", ["orderId" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 27, $this->source); })()), "id", [], "any", false, false, false, 27), "shipmentId" => twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 27, $this->source); })()), "id", [], "any", false, false, false, 27)]), "html", null, true);
            echo "\" method=\"post\" novalidate=\"\">
                <label for=\"number_of_parcels\" style=\"display: block\">";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.number_of_parcels"), "html", null, true);
            echo "</label>
                <div class=\"ui fluid action input\" style=\"margin-top: 5px; margin-bottom: 10px\">
                    <input type=\"number\" style=\"display: block\" id=\"number_of_parcels\" name=\"number_of_parcels\" value=\"1\">
                </div>
                <button type=\"submit\" class=\"ui icon labeled tiny blue fluid loadable button\"><i class=\"reply all icon\"></i>";
            // line 32
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.order.export_shipment"), "html", null, true);
            echo "</button>
            </form>
        </div>
    ";
        }
        // line 36
        echo "    ";
        if ($this->extensions['SM\Extension\Twig\SMExtension']->can((isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 36, $this->source); })()), "ship", "sylius_shipment")) {
            // line 37
            echo "        ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_admin_partial_shipment_ship", ["orderId" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 37, $this->source); })()), "id", [], "any", false, false, false, 37), "id" => twig_get_attribute($this->env, $this->source, (isset($context["shipment"]) || array_key_exists("shipment", $context) ? $context["shipment"] : (function () { throw new RuntimeError('Variable "shipment" does not exist.', 37, $this->source); })()), "id", [], "any", false, false, false, 37)]));
            echo "
    ";
        }
        // line 39
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/SyliusAdminBundle/Order/Show/_shipment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 39,  112 => 37,  109 => 36,  102 => 32,  95 => 28,  91 => 27,  88 => 26,  85 => 25,  77 => 20,  72 => 18,  69 => 17,  67 => 16,  61 => 13,  55 => 10,  49 => 6,  47 => 5,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusUi/Macro/labels.html.twig' as label %}

<div class=\"item\">
    <div class=\"right floated content\">
        {% include '@SyliusAdmin/Common/Label/shipmentState.html.twig' with {'data': shipment.state} %}
    </div>
    <i class=\"large truck icon\"></i>
    <div class=\"content\">
        <div class=\"header\">
            {{ shipment.method }}
        </div>
        <div class=\"description\">
            <i class=\"globe icon\"></i>{{ shipment.method.zone }}
        </div>
    </div>
    {% if sm_can(shipment, 'pack', 'sylius_shipment') and 'dpd' not in shipment.method.code %}
        <div class=\"ui segment\">
            <form name=\"sylius_shipment_ship\" method=\"post\" action=\"{{ path('pack', {orderId: order.id, shipmentId: shipment.id}) }}\" class=\"ui loadable form\" novalidate=\"novalidate\">
                <div class=\"ui fluid action input\">
                    <button type=\"submit\" class=\"ui labeled icon teal button\"><i class=\"box icon\"></i>{{ 'app.shipment.pack'|trans }}</button>
                </div>
            </form>
        </div>
    {% endif %}
    {% if sm_can(shipment, 'export', 'sylius_shipment') and 'dpd' in shipment.method.code and shipment.tracking is null %}
        <div class=\"ui segment\">
            <form action=\"{{ path('export_single_shipment', {orderId: order.id, shipmentId: shipment.id}) }}\" method=\"post\" novalidate=\"\">
                <label for=\"number_of_parcels\" style=\"display: block\">{{ 'app.number_of_parcels'|trans }}</label>
                <div class=\"ui fluid action input\" style=\"margin-top: 5px; margin-bottom: 10px\">
                    <input type=\"number\" style=\"display: block\" id=\"number_of_parcels\" name=\"number_of_parcels\" value=\"1\">
                </div>
                <button type=\"submit\" class=\"ui icon labeled tiny blue fluid loadable button\"><i class=\"reply all icon\"></i>{{ 'app.order.export_shipment'|trans }}</button>
            </form>
        </div>
    {% endif %}
    {% if sm_can(shipment, 'ship', 'sylius_shipment') %}
        {{ render(path('sylius_admin_partial_shipment_ship', {'orderId': order.id, 'id': shipment.id})) }}
    {% endif %}
</div>
", "bundles/SyliusAdminBundle/Order/Show/_shipment.html.twig", "/var/www/html/templates/bundles/SyliusAdminBundle/Order/Show/_shipment.html.twig");
    }
}
