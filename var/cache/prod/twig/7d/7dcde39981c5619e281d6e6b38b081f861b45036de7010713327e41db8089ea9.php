<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Show:_variants.html.twig */
class __TwigTemplate_61b4726927ade63ac23fd6b97d64c4a2f844e7af415f07a4fd55775d1adb4b04 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Show:_variants.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Product/Show:_variants.html.twig", 1)->unwrap();
        // line 2
        echo "
<table class=\"table\" id=\"sylius-product-variants\">
    <thead class=\"\">
        <tr>
            <th class=\"border-0 h6\">";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.variant"), "html", null, true);
        echo "</th>
            <th class=\"border-0 h6\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.price"), "html", null, true);
        echo "</th>
            <th class=\"border-0 h6\"></th>
        </tr>
    </thead>
    <tbody>
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 12, $this->source); })()), "variants", [], "any", false, false, false, 12));
        foreach ($context['_seq'] as $context["key"] => $context["variant"]) {
            // line 13
            echo "        <tr>
            <td>
                ";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["variant"], "name", [], "any", false, false, false, 15), "html", null, true);
            echo "
                ";
            // line 16
            if (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 16, $this->source); })()), "hasOptions", [], "method", false, false, false, 16)) {
                // line 17
                echo "                    <span class=\"text-muted\">|</span>
                    ";
                // line 18
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["variant"], "optionValues", [], "any", false, false, false, 18));
                foreach ($context['_seq'] as $context["_key"] => $context["optionValue"]) {
                    // line 19
                    echo "                        <span class=\"pr-1\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["optionValue"], "value", [], "any", false, false, false, 19), "html", null, true);
                    echo "</span>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['optionValue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 21
                echo "                ";
            }
            // line 22
            echo "            </td>
            <td data-js-product-variant-price>";
            // line 23
            echo twig_call_macro($macros["money"], "macro_calculatePrice", [$context["variant"]], 23, $context, $this->getSourceContext());
            echo "</td>
            <td class=\"text-right\">";
            // line 24
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), "cartItem", [], "any", false, false, false, 24), "variant", [], "any", false, false, false, 24), $context["key"], [], "array", false, false, false, 24), 'widget', ["label" => false]);
            echo "</td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['variant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "    </tbody>
</table>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Show:_variants.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 27,  100 => 24,  96 => 23,  93 => 22,  90 => 21,  81 => 19,  77 => 18,  74 => 17,  72 => 16,  68 => 15,  64 => 13,  60 => 12,  52 => 7,  48 => 6,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}

<table class=\"table\" id=\"sylius-product-variants\">
    <thead class=\"\">
        <tr>
            <th class=\"border-0 h6\">{{ 'sylius.ui.variant'|trans }}</th>
            <th class=\"border-0 h6\">{{ 'sylius.ui.price'|trans }}</th>
            <th class=\"border-0 h6\"></th>
        </tr>
    </thead>
    <tbody>
    {% for key, variant in product.variants %}
        <tr>
            <td>
                {{ variant.name }}
                {% if product.hasOptions() %}
                    <span class=\"text-muted\">|</span>
                    {% for optionValue in variant.optionValues %}
                        <span class=\"pr-1\">{{ optionValue.value }}</span>
                    {% endfor %}
                {% endif %}
            </td>
            <td data-js-product-variant-price>{{ money.calculatePrice(variant) }}</td>
            <td class=\"text-right\">{{ form_widget(form.cartItem.variant[key], {'label': false}) }}</td>
        </tr>
    {% endfor %}
    </tbody>
</table>
", "SyliusShopBundle:Product/Show:_variants.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Product/Show/_variants.html.twig");
    }
}
