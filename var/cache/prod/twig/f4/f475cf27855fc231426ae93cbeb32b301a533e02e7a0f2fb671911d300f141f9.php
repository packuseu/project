<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Brille24SyliusTierPricePlugin/Shop/_javascripts.html.twig */
class __TwigTemplate_92230873bc00169332b7290ca5bddd53450e847f6b3ab293c3e80045e655881a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Brille24SyliusTierPricePlugin/Shop/_javascripts.html.twig"));

        // line 1
        $this->loadTemplate("@SyliusUi/_javascripts.html.twig", "@Brille24SyliusTierPricePlugin/Shop/_javascripts.html.twig", 1)->display(twig_array_merge($context, ["path" => "bundles/brille24syliustierpriceplugin/js/updateProductVariant.js"]));
        // line 2
        $this->loadTemplate("@SyliusUi/_javascripts.html.twig", "@Brille24SyliusTierPricePlugin/Shop/_javascripts.html.twig", 2)->display(twig_array_merge($context, ["path" => "bundles/brille24syliustierpriceplugin/js/updateProduct.js"]));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@Brille24SyliusTierPricePlugin/Shop/_javascripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include '@SyliusUi/_javascripts.html.twig' with {path: 'bundles/brille24syliustierpriceplugin/js/updateProductVariant.js'} %}
{% include '@SyliusUi/_javascripts.html.twig' with {path: 'bundles/brille24syliustierpriceplugin/js/updateProduct.js'} %}
", "@Brille24SyliusTierPricePlugin/Shop/_javascripts.html.twig", "/var/www/html/vendor/brille24/sylius-tierprice-plugin/src/Resources/views/Shop/_javascripts.html.twig");
    }
}
