<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Email/layout.html.twig */
class __TwigTemplate_6d0c944206427f0bf48ad7764ef7a4c92a6212312a8c9c39f7d77250ef95ed78 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'additional_styles' => [$this, 'block_additional_styles'],
            'subject' => [$this, 'block_subject'],
            'logo' => [$this, 'block_logo'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Email/layout.html.twig"));

        // line 1
        $this->displayBlock('body', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 2
        echo "    <!doctype html>
    <html>
    <head>
        <meta name=\"viewport\" content=\"width=device-width\" />
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
        <title>Packus</title>
        <style>
            /* -------------------------------------
                GLOBAL RESETS
            ------------------------------------- */
            img {
                border: none;
                -ms-interpolation-mode: bicubic;
                max-width: 100%; }

            body {
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%; }

            table {
                border-collapse: separate;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                width: 100%; }
            table td {
                font-family: sans-serif;
                font-size: 14px;
                vertical-align: top; }

            /* -------------------------------------
                BODY & CONTAINER
            ------------------------------------- */

            .body {
                width: 100%; }

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display: block;
                Margin: 0 auto !important;
                /* makes it centered */
                max-width: 580px;
                padding: 10px;
                width: 580px; }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                box-sizing: border-box;
                display: block;
                Margin: 0 auto;
                max-width: 580px;
                padding: 10px; }

            /* -------------------------------------
                HEADER, FOOTER, MAIN
            ------------------------------------- */
            .main {
                border-radius: 3px;
                width: 100%; }

            .wrapper {
                box-sizing: border-box;
                padding: 20px; }

            .content-block {
                padding-bottom: 10px;
                padding-top: 10px;
            }

            .footer {
                clear: both;
                Margin-top: 30px;
                text-align: center;
                width: 100%; }
            .footer td,
            .footer p,
            .footer span,
            .footer a {
                color: #999999;
                font-size: 12px;
                text-align: center; }

            /* -------------------------------------
                TYPOGRAPHY
            ------------------------------------- */
            h1,
            h2,
            h3,
            h4 {
                color: #000000;
                font-family: sans-serif;
                font-weight: 400;
                line-height: 1.4;
                margin: 0;
                Margin-bottom: 30px; }

            h1 {
                font-size: 28px;
                font-weight: bold;}

            p,
            ul,
            ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                Margin-bottom: 15px; }
            p li,
            ul li,
            ol li {
                list-style-position: inside;
                margin-left: 5px; }

            a {
                color: #3498db;
                text-decoration: underline; }

            /* -------------------------------------
                BUTTONS
            ------------------------------------- */
            .btn {
                width: 100%; }
            .btn table {
                width: auto; }
            .btn table td {
                text-align: center; }
            .btn a {
                color: #ffffff;
                text-transform: uppercase;
                text-decoration: none;
            }

            /* -------------------------------------
                OTHER STYLES THAT MIGHT BE USEFUL
            ------------------------------------- */
            .last {
                margin-bottom: 0; }

            .first {
                margin-top: 0; }

            .align-center {
                text-align: center; }

            .align-right {
                text-align: right; }

            .align-left {
                text-align: left; }

            .clear {
                clear: both; }

            .mt0 {
                margin-top: 0; }

            .mb0 {
                margin-bottom: 0; }

            .preheader {
                color: transparent;
                display: none;
                height: 0;
                max-height: 0;
                max-width: 0;
                opacity: 0;
                overflow: hidden;
                mso-hide: all;
                visibility: hidden;
                width: 0; }

            .powered-by a {
                text-decoration: none; }

            hr {
                border: 0;
                border-bottom: 1px solid #f6f6f6;
                Margin: 20px 0; }

            .logo {
                margin-bottom: 30px;
                margin-right: auto;
                margin-left: auto;
            }

            .logo img {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }

            .background {
                background-image: url('";
        // line 201
        echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 201, $this->source); })()), "request", [], "any", false, false, false, 201), "getSchemeAndHttpHost", [], "method", false, false, false, 201) . $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/omni/img/bg-email.png")), "html", null, true);
        echo "');
                background-repeat: no-repeat;
                background-position: top left;
            }

            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                    text-align: center;}
                table[class=body] .logo {
                    text-align: center;}
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important; }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important; }
                table[class=body] .content {
                    padding: 0 !important; }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important; }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important; }
                table[class=body] .btn table {
                    width: 100% !important; }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important; }}

            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%; }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%; }
                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important; }
            }
            ";
        // line 263
        $this->displayBlock('additional_styles', $context, $blocks);
        // line 264
        echo "        </style>
    </head>
    <body class=\"background\">
    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\">
        <tr>
            <td>&nbsp;</td>
            <td class=\"container\">
                <div class=\"content\">
                    <span class=\"preheader\">";
        // line 272
        $this->displayBlock('subject', $context, $blocks);
        echo "</span>

                    <table class=\"main\">
                        <tr>
                            <td class=\"wrapper\">
                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                                    <tr>
                                        <td>
                                            <p class=\"logo\">
                                                ";
        // line 281
        $this->displayBlock('logo', $context, $blocks);
        // line 296
        echo "                                            </p>

                                            ";
        // line 298
        $this->displayBlock('content', $context, $blocks);
        // line 299
        echo "                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <div class=\"footer\">
                    </div>
                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </body>
    </html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 263
    public function block_additional_styles($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "additional_styles"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 272
    public function block_subject($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "subject"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 281
    public function block_logo($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "logo"));

        // line 282
        echo "                                                    ";
        $context["logo"] = null;
        // line 283
        echo "
                                                    ";
        // line 284
        if ((twig_get_attribute($this->env, $this->source, ($context["sylius"] ?? null), "channel", [], "any", true, true, false, 284) && twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 284, $this->source); })()), "channel", [], "any", false, false, false, 284))) {
            // line 285
            echo "                                                        ";
            $context["logo"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 285, $this->source); })()), "channel", [], "any", false, false, false, 285), "logos", [], "any", false, false, false, 285), "first", [], "any", false, false, false, 285);
            // line 286
            echo "                                                    ";
        }
        // line 287
        echo "
                                                    ";
        // line 288
        if ((isset($context["logo"]) || array_key_exists("logo", $context) ? $context["logo"] : (function () { throw new RuntimeError('Variable "logo" does not exist.', 288, $this->source); })())) {
            // line 289
            echo "                                                        ";
            $context["logo_path"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, (isset($context["logo"]) || array_key_exists("logo", $context) ? $context["logo"] : (function () { throw new RuntimeError('Variable "logo" does not exist.', 289, $this->source); })()), "path", [], "any", false, false, false, 289), "sylius_admin_product_original");
            // line 290
            echo "                                                    ";
        } else {
            // line 291
            echo "                                                        ";
            $context["logo_path"] = (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 291, $this->source); })()), "request", [], "any", false, false, false, 291), "getSchemeAndHttpHost", [], "method", false, false, false, 291) . $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo.png"));
            // line 292
            echo "                                                    ";
        }
        // line 293
        echo "
                                                    <img src=\"";
        // line 294
        echo twig_escape_filter($this->env, (isset($context["logo_path"]) || array_key_exists("logo_path", $context) ? $context["logo_path"] : (function () { throw new RuntimeError('Variable "logo_path" does not exist.', 294, $this->source); })()), "html", null, true);
        echo "\" alt=\"logo\" style=\"max-width: 100px\">
                                                ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 298
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Email/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  453 => 298,  444 => 294,  441 => 293,  438 => 292,  435 => 291,  432 => 290,  429 => 289,  427 => 288,  424 => 287,  421 => 286,  418 => 285,  416 => 284,  413 => 283,  410 => 282,  403 => 281,  391 => 272,  379 => 263,  356 => 299,  354 => 298,  350 => 296,  348 => 281,  336 => 272,  326 => 264,  324 => 263,  259 => 201,  58 => 2,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% block body %}
    <!doctype html>
    <html>
    <head>
        <meta name=\"viewport\" content=\"width=device-width\" />
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />
        <title>Packus</title>
        <style>
            /* -------------------------------------
                GLOBAL RESETS
            ------------------------------------- */
            img {
                border: none;
                -ms-interpolation-mode: bicubic;
                max-width: 100%; }

            body {
                font-family: sans-serif;
                -webkit-font-smoothing: antialiased;
                font-size: 14px;
                line-height: 1.4;
                margin: 0;
                padding: 0;
                -ms-text-size-adjust: 100%;
                -webkit-text-size-adjust: 100%; }

            table {
                border-collapse: separate;
                mso-table-lspace: 0pt;
                mso-table-rspace: 0pt;
                width: 100%; }
            table td {
                font-family: sans-serif;
                font-size: 14px;
                vertical-align: top; }

            /* -------------------------------------
                BODY & CONTAINER
            ------------------------------------- */

            .body {
                width: 100%; }

            /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
            .container {
                display: block;
                Margin: 0 auto !important;
                /* makes it centered */
                max-width: 580px;
                padding: 10px;
                width: 580px; }

            /* This should also be a block element, so that it will fill 100% of the .container */
            .content {
                box-sizing: border-box;
                display: block;
                Margin: 0 auto;
                max-width: 580px;
                padding: 10px; }

            /* -------------------------------------
                HEADER, FOOTER, MAIN
            ------------------------------------- */
            .main {
                border-radius: 3px;
                width: 100%; }

            .wrapper {
                box-sizing: border-box;
                padding: 20px; }

            .content-block {
                padding-bottom: 10px;
                padding-top: 10px;
            }

            .footer {
                clear: both;
                Margin-top: 30px;
                text-align: center;
                width: 100%; }
            .footer td,
            .footer p,
            .footer span,
            .footer a {
                color: #999999;
                font-size: 12px;
                text-align: center; }

            /* -------------------------------------
                TYPOGRAPHY
            ------------------------------------- */
            h1,
            h2,
            h3,
            h4 {
                color: #000000;
                font-family: sans-serif;
                font-weight: 400;
                line-height: 1.4;
                margin: 0;
                Margin-bottom: 30px; }

            h1 {
                font-size: 28px;
                font-weight: bold;}

            p,
            ul,
            ol {
                font-family: sans-serif;
                font-size: 14px;
                font-weight: normal;
                margin: 0;
                Margin-bottom: 15px; }
            p li,
            ul li,
            ol li {
                list-style-position: inside;
                margin-left: 5px; }

            a {
                color: #3498db;
                text-decoration: underline; }

            /* -------------------------------------
                BUTTONS
            ------------------------------------- */
            .btn {
                width: 100%; }
            .btn table {
                width: auto; }
            .btn table td {
                text-align: center; }
            .btn a {
                color: #ffffff;
                text-transform: uppercase;
                text-decoration: none;
            }

            /* -------------------------------------
                OTHER STYLES THAT MIGHT BE USEFUL
            ------------------------------------- */
            .last {
                margin-bottom: 0; }

            .first {
                margin-top: 0; }

            .align-center {
                text-align: center; }

            .align-right {
                text-align: right; }

            .align-left {
                text-align: left; }

            .clear {
                clear: both; }

            .mt0 {
                margin-top: 0; }

            .mb0 {
                margin-bottom: 0; }

            .preheader {
                color: transparent;
                display: none;
                height: 0;
                max-height: 0;
                max-width: 0;
                opacity: 0;
                overflow: hidden;
                mso-hide: all;
                visibility: hidden;
                width: 0; }

            .powered-by a {
                text-decoration: none; }

            hr {
                border: 0;
                border-bottom: 1px solid #f6f6f6;
                Margin: 20px 0; }

            .logo {
                margin-bottom: 30px;
                margin-right: auto;
                margin-left: auto;
            }

            .logo img {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }

            .background {
                background-image: url('{{ app.request.getSchemeAndHttpHost() ~ asset('assets/omni/img/bg-email.png') }}');
                background-repeat: no-repeat;
                background-position: top left;
            }

            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                    text-align: center;}
                table[class=body] .logo {
                    text-align: center;}
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important; }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important; }
                table[class=body] .content {
                    padding: 0 !important; }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important; }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important; }
                table[class=body] .btn table {
                    width: 100% !important; }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important; }}

            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%; }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%; }
                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important; }
            }
            {% block additional_styles %}{% endblock %}
        </style>
    </head>
    <body class=\"background\">
    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"body\">
        <tr>
            <td>&nbsp;</td>
            <td class=\"container\">
                <div class=\"content\">
                    <span class=\"preheader\">{% block subject %}{% endblock %}</span>

                    <table class=\"main\">
                        <tr>
                            <td class=\"wrapper\">
                                <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
                                    <tr>
                                        <td>
                                            <p class=\"logo\">
                                                {% block logo %}
                                                    {% set logo = null %}

                                                    {% if sylius.channel is defined and sylius.channel %}
                                                        {% set logo = sylius.channel.logos.first %}
                                                    {% endif %}

                                                    {% if logo %}
                                                        {% set logo_path = logo.path|imagine_filter('sylius_admin_product_original') %}
                                                    {% else %}
                                                        {% set logo_path = app.request.getSchemeAndHttpHost() ~ asset('img/logo.png') %}
                                                    {% endif %}

                                                    <img src=\"{{ logo_path }}\" alt=\"logo\" style=\"max-width: 100px\">
                                                {% endblock %}
                                            </p>

                                            {% block content %}{% endblock %}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <div class=\"footer\">
                    </div>
                </div>
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </body>
    </html>
{% endblock %}
", "Email/layout.html.twig", "/var/www/html/templates/Email/layout.html.twig");
    }
}
