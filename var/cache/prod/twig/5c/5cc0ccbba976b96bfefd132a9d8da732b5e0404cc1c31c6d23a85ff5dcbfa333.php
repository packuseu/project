<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Cart/Summary:_totals.html.twig */
class __TwigTemplate_ac21e2b0d2fb3df13f3871c62101f0ef6a0f4ddf3ddda888e1831716be3f7673 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Cart/Summary:_totals.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Cart/Summary:_totals.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["itemsSubtotal"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderItemsSubtotalExtension']->getSubtotal((isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 3, $this->source); })()));
        // line 4
        $context["taxIncluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getIncludedTax((isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 4, $this->source); })()));
        // line 5
        $context["taxExcluded"] = $this->extensions['Sylius\Bundle\ShopBundle\Twig\OrderTaxesTotalExtension']->getExcludedTax((isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 5, $this->source); })()));
        // line 6
        echo "
<div class=\"card mb-3\">
    <div class=\"card-header\">
        ";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.summary"), "html", null, true);
        echo "
    </div>
    ";
        // line 11
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.cart.summary.totals", ["cart" => (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 11, $this->source); })())]]);
        echo "

    <ul class=\"list-group list-group-flush\">
        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            ";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.items_total"), "html", null, true);
        echo ":
            <span>";
        // line 16
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["itemsSubtotal"]) || array_key_exists("itemsSubtotal", $context) ? $context["itemsSubtotal"] : (function () { throw new RuntimeError('Variable "itemsSubtotal" does not exist.', 16, $this->source); })())], 16, $context, $this->getSourceContext());
        echo "</span>
        </li>

        ";
        // line 19
        if (twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 19, $this->source); })()), "orderPromotionTotal", [], "any", false, false, false, 19)) {
            // line 20
            echo "        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            ";
            // line 21
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.discount"), "html", null, true);
            echo ":
            <span>";
            // line 22
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 22, $this->source); })()), "orderPromotionTotal", [], "any", false, false, false, 22)], 22, $context, $this->getSourceContext());
            echo "</span>
        </li>
        ";
        }
        // line 25
        echo "
        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            ";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.shipping_estimated_cost"), "html", null, true);
        echo ":
            <span>
                ";
        // line 29
        if ((twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 29, $this->source); })()), "getAdjustmentsTotal", [0 => "shipping"], "method", false, false, false, 29) > twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 29, $this->source); })()), "shippingTotal", [], "any", false, false, false, 29))) {
            // line 30
            echo "                    <small><s>";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 30, $this->source); })()), "getAdjustmentsTotal", [0 => "shipping"], "method", false, false, false, 30)], 30, $context, $this->getSourceContext());
            echo "</s></small>
                ";
        }
        // line 32
        echo "                ";
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 32, $this->source); })()), "shippingTotal", [], "any", false, false, false, 32)], 32, $context, $this->getSourceContext());
        echo "
            </span>
        </li>

        <li class=\"list-group-item d-flex justify-content-between align-items-center ";
        // line 36
        if (((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 36, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 36, $this->source); })()))) {
            echo "bg-light";
        }
        echo "\">
            ";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.taxes_total"), "html", null, true);
        echo ":
            ";
        // line 38
        if (( !(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 38, $this->source); })()) &&  !(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 38, $this->source); })()))) {
            // line 39
            echo "                <span>";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [0], 39, $context, $this->getSourceContext());
            echo "</span>
            ";
        }
        // line 41
        echo "            ";
        if ((isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 41, $this->source); })())) {
            // line 42
            echo "                <span>";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxExcluded"]) || array_key_exists("taxExcluded", $context) ? $context["taxExcluded"] : (function () { throw new RuntimeError('Variable "taxExcluded" does not exist.', 42, $this->source); })())], 42, $context, $this->getSourceContext());
            echo "</span>
            ";
        }
        // line 44
        echo "            ";
        if ((isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 44, $this->source); })())) {
            // line 45
            echo "                <span><small>(";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.included_in_price"), "html", null, true);
            echo ")</small> ";
            echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["taxIncluded"]) || array_key_exists("taxIncluded", $context) ? $context["taxIncluded"] : (function () { throw new RuntimeError('Variable "taxIncluded" does not exist.', 45, $this->source); })())], 45, $context, $this->getSourceContext());
            echo "</span>
            ";
        }
        // line 47
        echo "        </li>

        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            <strong>";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.order_total"), "html", null, true);
        echo ":</strong>
            <span class=\"h3\">";
        // line 51
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 51, $this->source); })()), "total", [], "any", false, false, false, 51)], 51, $context, $this->getSourceContext());
        echo "</span>
        </li>

        ";
        // line 54
        if ( !(twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 54, $this->source); })()), "currencyCode", [], "any", false, false, false, 54) === twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 54, $this->source); })()), "currencyCode", [], "any", false, false, false, 54))) {
            // line 55
            echo "        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            ";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.base_currency_order_total"), "html", null, true);
            echo ":
            <span>";
            // line 57
            echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 57, $this->source); })()), "total", [], "any", false, false, false, 57), twig_get_attribute($this->env, $this->source, (isset($context["cart"]) || array_key_exists("cart", $context) ? $context["cart"] : (function () { throw new RuntimeError('Variable "cart" does not exist.', 57, $this->source); })()), "currencyCode", [], "any", false, false, false, 57)], 57, $context, $this->getSourceContext());
            echo "</span>
        </li>
        ";
        }
        // line 60
        echo "    </ul>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Cart/Summary:_totals.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 60,  180 => 57,  176 => 56,  173 => 55,  171 => 54,  165 => 51,  161 => 50,  156 => 47,  148 => 45,  145 => 44,  139 => 42,  136 => 41,  130 => 39,  128 => 38,  124 => 37,  118 => 36,  110 => 32,  104 => 30,  102 => 29,  97 => 27,  93 => 25,  87 => 22,  83 => 21,  80 => 20,  78 => 19,  72 => 16,  68 => 15,  61 => 11,  56 => 9,  51 => 6,  49 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/money.html.twig\" as money %}

{% set itemsSubtotal = sylius_order_items_subtotal(cart) %}
{% set taxIncluded = sylius_order_tax_included(cart) %}
{% set taxExcluded = sylius_order_tax_excluded(cart) %}

<div class=\"card mb-3\">
    <div class=\"card-header\">
        {{ 'sylius.ui.summary'|trans }}
    </div>
    {{ sonata_block_render_event('sylius.shop.cart.summary.totals', {'cart': cart}) }}

    <ul class=\"list-group list-group-flush\">
        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            {{ 'sylius.ui.items_total'|trans }}:
            <span>{{ money.convertAndFormat(itemsSubtotal) }}</span>
        </li>

        {% if cart.orderPromotionTotal %}
        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            {{ 'sylius.ui.discount'|trans }}:
            <span>{{ money.convertAndFormat(cart.orderPromotionTotal) }}</span>
        </li>
        {% endif %}

        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            {{ 'sylius.ui.shipping_estimated_cost'|trans }}:
            <span>
                {% if cart.getAdjustmentsTotal('shipping') > cart.shippingTotal %}
                    <small><s>{{ money.convertAndFormat(cart.getAdjustmentsTotal('shipping')) }}</s></small>
                {% endif %}
                {{ money.convertAndFormat(cart.shippingTotal) }}
            </span>
        </li>

        <li class=\"list-group-item d-flex justify-content-between align-items-center {% if taxIncluded and not taxExcluded %}bg-light{% endif %}\">
            {{ 'sylius.ui.taxes_total'|trans }}:
            {% if not taxIncluded and not taxExcluded %}
                <span>{{ money.convertAndFormat(0) }}</span>
            {% endif %}
            {% if taxExcluded %}
                <span>{{ money.convertAndFormat(taxExcluded) }}</span>
            {% endif %}
            {% if taxIncluded %}
                <span><small>({{ 'sylius.ui.included_in_price'|trans }})</small> {{ money.convertAndFormat(taxIncluded) }}</span>
            {% endif %}
        </li>

        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            <strong>{{ 'sylius.ui.order_total'|trans }}:</strong>
            <span class=\"h3\">{{ money.convertAndFormat(cart.total) }}</span>
        </li>

        {% if cart.currencyCode is not same as(sylius.currencyCode) %}
        <li class=\"list-group-item d-flex justify-content-between align-items-center\">
            {{ 'sylius.ui.base_currency_order_total'|trans }}:
            <span>{{ money.format(cart.total, cart.currencyCode) }}</span>
        </li>
        {% endif %}
    </ul>
</div>
", "SyliusShopBundle:Cart/Summary:_totals.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Cart/Summary/_totals.html.twig");
    }
}
