<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Account/Order/Show:_header.html.twig */
class __TwigTemplate_60a623243a8aa4cb84018a22eedd5f0a4a5fca84685cf759f099bd0037dacfd5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Account/Order/Show:_header.html.twig"));

        // line 1
        $macros["flags"] = $this->macros["flags"] = $this->loadTemplate("@SyliusUi/Macro/flags.html.twig", "SyliusShopBundle:Account/Order/Show:_header.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $macros["headers"] = $this->macros["headers"] = $this->loadTemplate("@SyliusShop/Common/Macro/headers.html.twig", "SyliusShopBundle:Account/Order/Show:_header.html.twig", 3)->unwrap();
        // line 4
        echo "
";
        // line 5
        echo twig_call_macro($macros["headers"], "macro_default", [(($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.order") . " #") . twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 5, $this->source); })()), "number", [], "any", false, false, false, 5))], 5, $context, $this->getSourceContext());
        echo "

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"d-flex\">
            <div>
                ";
        // line 11
        echo $this->extensions['Sonata\IntlBundle\Twig\Extension\DateTimeExtension']->formatDate(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 11, $this->source); })()), "checkoutCompletedAt", [], "any", false, false, false, 11));
        echo "
            </div>
            <div class=\"pl-2\">
                ";
        // line 14
        $this->loadTemplate([0 => ((("@SyliusShop/Account/Order/Label/State" . "/") . twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 14, $this->source); })()), "state", [], "any", false, false, false, 14)) . ".html.twig"), 1 => "@SyliusUi/Label/_default.html.twig"], "SyliusShopBundle:Account/Order/Show:_header.html.twig", 14)->display(twig_array_merge($context, ["value" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("sylius.ui." . twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 14, $this->source); })()), "state", [], "any", false, false, false, 14)))]));
        // line 15
        echo "            </div>
            <div class=\"pl-2\">
                ";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 17, $this->source); })()), "currencyCode", [], "any", false, false, false, 17), "html", null, true);
        echo "
            </div>
            <div class=\"pl-2\">
                ";
        // line 20
        echo twig_call_macro($macros["flags"], "macro_fromLocaleCode", [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 20, $this->source); })()), "localeCode", [], "any", false, false, false, 20)], 20, $context, $this->getSourceContext());
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_locale_name')->getCallable(), [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 20, $this->source); })()), "localeCode", [], "any", false, false, false, 20)]), "html", null, true);
        echo "
            </div>
        </div>
    </div>
</div>


";
        // line 27
        if (twig_in_filter(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 27, $this->source); })()), "paymentState", [], "any", false, false, false, 27), [0 => "awaiting_payment"])) {
            // line 28
            echo "    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_order_show", ["tokenValue" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 28, $this->source); })()), "tokenValue", [], "any", false, false, false, 28)]), "html", null, true);
            echo "\" class=\"btn btn-primary my-3\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.pay"), "html", null, true);
            echo "</a>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Account/Order/Show:_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 28,  88 => 27,  77 => 20,  71 => 17,  67 => 15,  65 => 14,  59 => 11,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusUi/Macro/flags.html.twig' as flags %}

{% import '@SyliusShop/Common/Macro/headers.html.twig' as headers %}

{{ headers.default('sylius.ui.order'|trans ~ ' #' ~ order.number) }}

<div class=\"card\">
    <div class=\"card-body\">
        <div class=\"d-flex\">
            <div>
                {{ order.checkoutCompletedAt|format_date }}
            </div>
            <div class=\"pl-2\">
                {% include [('@SyliusShop/Account/Order/Label/State' ~ '/' ~ order.state ~ '.html.twig'), '@SyliusUi/Label/_default.html.twig'] with {'value': ('sylius.ui.' ~ order.state)|trans} %}
            </div>
            <div class=\"pl-2\">
                {{ order.currencyCode }}
            </div>
            <div class=\"pl-2\">
                {{ flags.fromLocaleCode(order.localeCode) }}{{ order.localeCode|sylius_locale_name }}
            </div>
        </div>
    </div>
</div>


{% if order.paymentState in ['awaiting_payment'] %}
    <a href=\"{{ path('sylius_shop_order_show', {'tokenValue': order.tokenValue}) }}\" class=\"btn btn-primary my-3\">{{ 'sylius.ui.pay'|trans }}</a>
{% endif %}
", "SyliusShopBundle:Account/Order/Show:_header.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Account/Order/Show/_header.html.twig");
    }
}
