<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Show/Tabs:_content.html.twig */
class __TwigTemplate_dc5a521449e98ba3d48617f43f9cc9272446c8c6425c67d2710e01cbb5e659e5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Show/Tabs:_content.html.twig"));

        // line 1
        echo "<div class=\"tab-pane fade show active\" id=\"nav-details\">
    ";
        // line 2
        $this->loadTemplate("@SyliusShop/Product/Show/Tabs/_details.html.twig", "SyliusShopBundle:Product/Show/Tabs:_content.html.twig", 2)->display($context);
        // line 3
        echo "</div>
<div class=\"tab-pane fade\" id=\"nav-attributes\">
    ";
        // line 5
        $this->loadTemplate("@SyliusShop/Product/Show/Tabs/_attributes.html.twig", "SyliusShopBundle:Product/Show/Tabs:_content.html.twig", 5)->display($context);
        // line 6
        echo "</div>
<div class=\"tab-pane fade\" id=\"nav-reviews\">
    ";
        // line 8
        $this->loadTemplate("@SyliusShop/Product/Show/Tabs/_reviews.html.twig", "SyliusShopBundle:Product/Show/Tabs:_content.html.twig", 8)->display($context);
        // line 9
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Show/Tabs:_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 9,  55 => 8,  51 => 6,  49 => 5,  45 => 3,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"tab-pane fade show active\" id=\"nav-details\">
    {% include '@SyliusShop/Product/Show/Tabs/_details.html.twig' %}
</div>
<div class=\"tab-pane fade\" id=\"nav-attributes\">
    {% include '@SyliusShop/Product/Show/Tabs/_attributes.html.twig' %}
</div>
<div class=\"tab-pane fade\" id=\"nav-reviews\">
    {% include '@SyliusShop/Product/Show/Tabs/_reviews.html.twig' %}
</div>
", "SyliusShopBundle:Product/Show/Tabs:_content.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Product/Show/Tabs/_content.html.twig");
    }
}
