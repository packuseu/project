<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Email/orderConfirmation.html.twig */
class __TwigTemplate_d003bbceb9b8ec5861e86f799ef232403312c2bd72e9b9a41ee022f65314d733 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'subject' => [$this, 'block_subject'],
            'additional_styles' => [$this, 'block_additional_styles'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "Email/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Email/orderConfirmation.html.twig"));

        $this->parent = $this->loadTemplate("Email/layout.html.twig", "Email/orderConfirmation.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_subject($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.subject"), "html", null, true);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_additional_styles($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "additional_styles"));

        // line 8
        echo "    .order-table {
        border:1px
        solid #eaeaea;
    }

    .order-table-header {
        font-size:13px;
        padding:3px 9px;
    }

    .order-table-row {
        border-bottom: 1px;
    }

    .order-table-data {
        font-size:11px;
        padding:3px 9px;
        border-bottom:1px;
    }
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 29
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 30
        echo "    ";
        $context["url"] = (( !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 30, $this->source); })()), "channel", [], "any", false, false, false, 30), "hostname", [], "any", false, false, false, 30))) ? (((((isset($context["url_scheme"]) || array_key_exists("url_scheme", $context) ? $context["url_scheme"] : (function () { throw new RuntimeError('Variable "url_scheme" does not exist.', 30, $this->source); })()) . "://") . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 30, $this->source); })()), "channel", [], "any", false, false, false, 30), "hostname", [], "any", false, false, false, 30)) . $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_order_show", ["tokenValue" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 30, $this->source); })()), "tokenValue", [], "any", false, false, false, 30), "_locale" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 30, $this->source); })()), "localeCode", [], "any", false, false, false, 30)]))) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sylius_shop_order_show", ["tokenValue" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 30, $this->source); })()), "tokenValue", [], "any", false, false, false, 30), "_locale" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 30, $this->source); })()), "localeCode", [], "any", false, false, false, 30)])));
        // line 31
        echo "    ";
        // line 32
        echo "        <p>
            ";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.your_order_placed"), "html", null, true);
        echo ": <b>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 33, $this->source); })()), "number", [], "any", false, false, false, 33), "html", null, true);
        echo "</b>
        </p>
        <p>
            ";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.view_order"), "html", null, true);
        echo " <a href=\"";
        echo (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 36, $this->source); })());
        echo "\">";
        echo (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 36, $this->source); })());
        echo "</a>
        </p>
        <table class=\"order-table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
            <thead>
            <tr bgcolor=\"#EAEAEA\">
                <th align=\"left\" class=\"order-table-header\">";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.record"), "html", null, true);
        echo "</th>
                <th align=\"left\" class=\"order-table-header\">";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.code"), "html", null, true);
        echo "</th>
                <th align=\"center\" class=\"order-table-header\">";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.quantity"), "html", null, true);
        echo "</th>
                <th align=\"right\" class=\"order-table-header\">";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.sum"), "html", null, true);
        echo "</th>
            </tr>
            </thead>
            <tbody>
                ";
        // line 48
        $context["unit_total_before_tax"] = 0;
        // line 49
        echo "
                ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 50, $this->source); })()), "items", [], "any", false, false, false, 50));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 51
            echo "                    <tr class=\"order-table-row\" style=\"border-bottom: 1px; border-color: #cccccc\">
                        <td class=\"order-table-data\"><b>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "variant", [], "any", false, false, false, 52), "name", [], "any", false, false, false, 52), "html", null, true);
            echo "</b></td>
                        <td class=\"order-table-data\">";
            // line 53
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["item"], "variant", [], "any", false, false, false, 53), "code", [], "any", false, false, false, 53), "html", null, true);
            echo "</td>
                        <td align=\"center\" class=\"order-table-data\">";
            // line 54
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 54), "html", null, true);
            echo "</td>
                        <td align=\"right\" class=\"order-table-data\">";
            // line 55
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_format_money')->getCallable(), [(twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 55) * twig_get_attribute($this->env, $this->source, $context["item"], "unitPrice", [], "any", false, false, false, 55)), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 55, $this->source); })()), "channel", [], "any", false, false, false, 55), "baseCurrency", [], "any", false, false, false, 55), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 55, $this->source); })()), "request", [], "any", false, false, false, 55), "locale", [], "any", false, false, false, 55)]), "html", null, true);
            echo "</td>
                    </tr>

                    ";
            // line 58
            $context["unit_total_before_tax"] = ((isset($context["unit_total_before_tax"]) || array_key_exists("unit_total_before_tax", $context) ? $context["unit_total_before_tax"] : (function () { throw new RuntimeError('Variable "unit_total_before_tax" does not exist.', 58, $this->source); })()) + (twig_get_attribute($this->env, $this->source, $context["item"], "quantity", [], "any", false, false, false, 58) * twig_get_attribute($this->env, $this->source, $context["item"], "unitPrice", [], "any", false, false, false, 58)));
            // line 59
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "            </tbody>
            <tbody>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\">";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.sum"), "html", null, true);
        echo "</td>
                    <td align=\"right\" class=\"order-table-data\">";
        // line 64
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_format_money')->getCallable(), [(isset($context["unit_total_before_tax"]) || array_key_exists("unit_total_before_tax", $context) ? $context["unit_total_before_tax"] : (function () { throw new RuntimeError('Variable "unit_total_before_tax" does not exist.', 64, $this->source); })()), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 64, $this->source); })()), "channel", [], "any", false, false, false, 64), "baseCurrency", [], "any", false, false, false, 64), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 64, $this->source); })()), "request", [], "any", false, false, false, 64), "locale", [], "any", false, false, false, 64)]), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\">";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.shipping"), "html", null, true);
        echo "</td>
                    ";
        // line 68
        $context["shipping_price"] = 0;
        // line 69
        echo "
                    ";
        // line 70
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 70, $this->source); })()), "shipments", [], "any", false, false, false, 70));
        foreach ($context['_seq'] as $context["_key"] => $context["shipment"]) {
            // line 71
            echo "                        ";
            $context["shipping_price"] = ((isset($context["shipping_price"]) || array_key_exists("shipping_price", $context) ? $context["shipping_price"] : (function () { throw new RuntimeError('Variable "shipping_price" does not exist.', 71, $this->source); })()) + twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["shipment"], "method", [], "any", false, false, false, 71), "getAmountForChannel", [0 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 71, $this->source); })()), "channel", [], "any", false, false, false, 71), "code", [], "any", false, false, false, 71)], "method", false, false, false, 71));
            // line 72
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "
                    <td align=\"right\" class=\"order-table-data\">";
        // line 74
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_format_money')->getCallable(), [(isset($context["shipping_price"]) || array_key_exists("shipping_price", $context) ? $context["shipping_price"] : (function () { throw new RuntimeError('Variable "shipping_price" does not exist.', 74, $this->source); })()), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 74, $this->source); })()), "channel", [], "any", false, false, false, 74), "baseCurrency", [], "any", false, false, false, 74), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 74, $this->source); })()), "request", [], "any", false, false, false, 74), "locale", [], "any", false, false, false, 74)]), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\">";
        // line 77
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.taxes_total"), "html", null, true);
        echo "</td>
                    <td align=\"right\" class=\"order-table-data\">";
        // line 78
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_format_money')->getCallable(), [((twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 78, $this->source); })()), "total", [], "any", false, false, false, 78) - (isset($context["unit_total_before_tax"]) || array_key_exists("unit_total_before_tax", $context) ? $context["unit_total_before_tax"] : (function () { throw new RuntimeError('Variable "unit_total_before_tax" does not exist.', 78, $this->source); })())) - (isset($context["shipping_price"]) || array_key_exists("shipping_price", $context) ? $context["shipping_price"] : (function () { throw new RuntimeError('Variable "shipping_price" does not exist.', 78, $this->source); })())), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 78, $this->source); })()), "channel", [], "any", false, false, false, 78), "baseCurrency", [], "any", false, false, false, 78), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 78, $this->source); })()), "request", [], "any", false, false, false, 78), "locale", [], "any", false, false, false, 78)]), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\"><b>";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.total"), "html", null, true);
        echo "</b></td>
                    <td align=\"right\" class=\"order-table-data\"><b>";
        // line 82
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_format_money')->getCallable(), [twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 82, $this->source); })()), "total", [], "any", false, false, false, 82), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 82, $this->source); })()), "channel", [], "any", false, false, false, 82), "baseCurrency", [], "any", false, false, false, 82), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 82, $this->source); })()), "request", [], "any", false, false, false, 82), "locale", [], "any", false, false, false, 82)]), "html", null, true);
        echo "</b></td>
                </tr>
            </tbody>
        </table>

        ";
        // line 87
        $context["order_payed"] = true;
        // line 88
        echo "        ";
        $context["bank_transfer"] = false;
        // line 89
        echo "        ";
        $context["method"] = null;
        // line 90
        echo "
        ";
        // line 91
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 91, $this->source); })()), "payments", [], "any", false, false, false, 91));
        foreach ($context['_seq'] as $context["_key"] => $context["payment"]) {
            // line 92
            echo "            ";
            if ((twig_get_attribute($this->env, $this->source, $context["payment"], "state", [], "any", false, false, false, 92) != "completed")) {
                // line 93
                echo "                ";
                $context["order_payed"] = false;
                // line 94
                echo "            ";
            }
            // line 95
            echo "            ";
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["payment"], "method", [], "any", false, false, false, 95), "code", [], "any", false, false, false, 95) == "bank_transfer")) {
                // line 96
                echo "                ";
                $context["method"] = twig_get_attribute($this->env, $this->source, $context["payment"], "method", [], "any", false, false, false, 96);
                // line 97
                echo "                ";
                $context["bank_transfer"] = true;
                // line 98
                echo "            ";
            }
            // line 99
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['payment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "
        ";
        // line 101
        if (((isset($context["order_payed"]) || array_key_exists("order_payed", $context) ? $context["order_payed"] : (function () { throw new RuntimeError('Variable "order_payed" does not exist.', 101, $this->source); })()) == false)) {
            // line 102
            echo "            <p style=\"margin-top: 10px\">
                ";
            // line 103
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.waiting_for_payment"), "html", null, true);
            echo ".
            </p>
        ";
        }
        // line 106
        echo "
        ";
        // line 107
        if (((isset($context["bank_transfer"]) || array_key_exists("bank_transfer", $context) ? $context["bank_transfer"] : (function () { throw new RuntimeError('Variable "bank_transfer" does not exist.', 107, $this->source); })()) &&  !(null === (isset($context["method"]) || array_key_exists("method", $context) ? $context["method"] : (function () { throw new RuntimeError('Variable "method" does not exist.', 107, $this->source); })())))) {
            // line 108
            echo "            <p style=\"margin-top: 10px\">
                ";
            // line 109
            echo twig_replace_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["method"]) || array_key_exists("method", $context) ? $context["method"] : (function () { throw new RuntimeError('Variable "method" does not exist.', 109, $this->source); })()), "translation", [], "any", false, false, false, 109), "instructions", [], "any", false, false, false, 109), ["{order_number}" => twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 109, $this->source); })()), "number", [], "any", false, false, false, 109), "
" => "<br/>"]);
            echo "
            </p>
        ";
        }
        // line 112
        echo "
        <p>";
        // line 113
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.products_will_be_ready"), "html", null, true);
        echo "</p>

        <p style=\"margin-top: 20px\">
            ";
        // line 116
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.email.order_confirm.thank_you"), "html", null, true);
        echo "
        </p>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Email/orderConfirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  344 => 116,  338 => 113,  335 => 112,  328 => 109,  325 => 108,  323 => 107,  320 => 106,  314 => 103,  311 => 102,  309 => 101,  306 => 100,  300 => 99,  297 => 98,  294 => 97,  291 => 96,  288 => 95,  285 => 94,  282 => 93,  279 => 92,  275 => 91,  272 => 90,  269 => 89,  266 => 88,  264 => 87,  256 => 82,  252 => 81,  246 => 78,  242 => 77,  236 => 74,  233 => 73,  227 => 72,  224 => 71,  220 => 70,  217 => 69,  215 => 68,  211 => 67,  205 => 64,  201 => 63,  196 => 60,  190 => 59,  188 => 58,  182 => 55,  178 => 54,  174 => 53,  170 => 52,  167 => 51,  163 => 50,  160 => 49,  158 => 48,  151 => 44,  147 => 43,  143 => 42,  139 => 41,  127 => 36,  119 => 33,  116 => 32,  114 => 31,  111 => 30,  104 => 29,  78 => 8,  71 => 7,  61 => 4,  54 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'Email/layout.html.twig' %}

{% block subject %}
    {{ 'app.email.order_confirm.subject'|trans }}
{% endblock %}

{% block additional_styles %}
    .order-table {
        border:1px
        solid #eaeaea;
    }

    .order-table-header {
        font-size:13px;
        padding:3px 9px;
    }

    .order-table-row {
        border-bottom: 1px;
    }

    .order-table-data {
        font-size:11px;
        padding:3px 9px;
        border-bottom:1px;
    }
{% endblock %}

{% block content %}
    {% set url = order.channel.hostname is not null ? url_scheme ~ '://' ~ order.channel.hostname ~ path('sylius_shop_order_show', {'tokenValue': order.tokenValue, '_locale': order.localeCode}) : url('sylius_shop_order_show', {'tokenValue': order.tokenValue, '_locale': order.localeCode}) %}
    {% autoescape %}
        <p>
            {{ 'app.email.order_confirm.your_order_placed'|trans }}: <b>{{ order.number }}</b>
        </p>
        <p>
            {{ 'app.email.order_confirm.view_order'|trans }} <a href=\"{{ url|raw }}\">{{ url|raw }}</a>
        </p>
        <table class=\"order-table\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
            <thead>
            <tr bgcolor=\"#EAEAEA\">
                <th align=\"left\" class=\"order-table-header\">{{ 'app.email.order_confirm.record'|trans }}</th>
                <th align=\"left\" class=\"order-table-header\">{{ 'app.email.order_confirm.code'|trans }}</th>
                <th align=\"center\" class=\"order-table-header\">{{ 'app.email.order_confirm.quantity'|trans }}</th>
                <th align=\"right\" class=\"order-table-header\">{{ 'app.email.order_confirm.sum'|trans }}</th>
            </tr>
            </thead>
            <tbody>
                {% set unit_total_before_tax = 0 %}

                {% for item in order.items %}
                    <tr class=\"order-table-row\" style=\"border-bottom: 1px; border-color: #cccccc\">
                        <td class=\"order-table-data\"><b>{{ item.variant.name }}</b></td>
                        <td class=\"order-table-data\">{{ item.variant.code }}</td>
                        <td align=\"center\" class=\"order-table-data\">{{ item.quantity }}</td>
                        <td align=\"right\" class=\"order-table-data\">{{ (item.quantity * item.unitPrice)|sylius_format_money(sylius.channel.baseCurrency, app.request.locale) }}</td>
                    </tr>

                    {% set unit_total_before_tax = unit_total_before_tax + item.quantity * item.unitPrice %}
                {% endfor %}
            </tbody>
            <tbody>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\">{{ 'app.email.order_confirm.sum'|trans }}</td>
                    <td align=\"right\" class=\"order-table-data\">{{ unit_total_before_tax|sylius_format_money(sylius.channel.baseCurrency, app.request.locale) }}</td>
                </tr>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\">{{ 'app.email.order_confirm.shipping'|trans }}</td>
                    {% set shipping_price = 0 %}

                    {% for shipment in order.shipments %}
                        {% set shipping_price = shipping_price + shipment.method.getAmountForChannel(sylius.channel.code) %}
                    {% endfor %}

                    <td align=\"right\" class=\"order-table-data\">{{ shipping_price|sylius_format_money(sylius.channel.baseCurrency, app.request.locale) }}</td>
                </tr>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\">{{ 'sylius.ui.taxes_total'|trans }}</td>
                    <td align=\"right\" class=\"order-table-data\">{{ (order.total - unit_total_before_tax - shipping_price)|sylius_format_money(sylius.channel.baseCurrency, app.request.locale) }}</td>
                </tr>
                <tr>
                    <td colspan=\"3\" align=\"right\" class=\"order-table-data\"><b>{{ 'app.email.order_confirm.total'|trans }}</b></td>
                    <td align=\"right\" class=\"order-table-data\"><b>{{ order.total|sylius_format_money(sylius.channel.baseCurrency, app.request.locale) }}</b></td>
                </tr>
            </tbody>
        </table>

        {% set order_payed = true %}
        {% set bank_transfer = false %}
        {% set method = null %}

        {% for payment in order.payments %}
            {% if payment.state != 'completed' %}
                {% set order_payed = false %}
            {% endif %}
            {% if payment.method.code == 'bank_transfer' %}
                {% set method = payment.method %}
                {% set bank_transfer = true %}
            {% endif %}
        {% endfor %}

        {% if order_payed == false %}
            <p style=\"margin-top: 10px\">
                {{ 'app.email.order_confirm.waiting_for_payment'|trans }}.
            </p>
        {% endif %}

        {% if bank_transfer and method is not null %}
            <p style=\"margin-top: 10px\">
                {{ method.translation.instructions|replace({'{order_number}': order.number, '\\n': '<br/>'})|raw }}
            </p>
        {% endif %}

        <p>{{ 'app.email.products_will_be_ready'|trans }}</p>

        <p style=\"margin-top: 20px\">
            {{ 'app.email.order_confirm.thank_you'|trans }}
        </p>
    {% endautoescape %}
{% endblock %}
", "Email/orderConfirmation.html.twig", "/var/www/html/templates/Email/orderConfirmation.html.twig");
    }
}
