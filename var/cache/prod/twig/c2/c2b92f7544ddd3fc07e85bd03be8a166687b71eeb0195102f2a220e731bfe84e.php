<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* bundles/SyliusAdminBundle/Channel/_form.html.twig */
class __TwigTemplate_c9f9141b66226e5aabcd415290ed878dd7976ba7d56c3426cc5796f309e5975b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "bundles/SyliusAdminBundle/Channel/_form.html.twig"));

        // line 1
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'errors');
        echo "
<div class=\"ui two column stackable grid\">
    <div class=\"column\">
        <div class=\"ui segment\">
            ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), 'errors');
        echo "
            <div class=\"two fields\">
                ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), "code", [], "any", false, false, false, 7), 'row');
        echo "
                ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "name", [], "any", false, false, false, 8), 'row');
        echo "
            </div>
            ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), "description", [], "any", false, false, false, 10), 'row');
        echo "
            ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "enabled", [], "any", false, false, false, 11), 'row');
        echo "
            <div class=\"two fields\">
                <div class=\"field\">
                    ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "hostname", [], "any", false, false, false, 14), 'label');
        echo "
                    <div class=\"ui labeled input\">
                        <div class=\"ui label\">http://</div>
                        ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 17, $this->source); })()), "hostname", [], "any", false, false, false, 17), 'widget');
        echo "
                    </div>
                    ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 19, $this->source); })()), "hostname", [], "any", false, false, false, 19), 'errors');
        echo "
                </div>
                ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "contactEmail", [], "any", false, false, false, 21), 'row');
        echo "
            </div>
            <div class=\"two fields\">
                ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), "color", [], "any", false, false, false, 24), 'row');
        echo "
                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), "themeName", [], "any", false, false, false, 25), 'row');
        echo "
            </div>
        </div>
        <div class=\"ui segment\">
            <h4 class=\"ui dividing header\">";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), "shopBillingData", [], "any", false, false, false, 29), 'label');
        echo "</h4>
            <div class=\"two fields\">
                ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "shopBillingData", [], "any", false, false, false, 31), "company", [], "any", false, false, false, 31), 'row');
        echo "
                ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 32, $this->source); })()), "shopBillingData", [], "any", false, false, false, 32), "taxId", [], "any", false, false, false, 32), 'row');
        echo "
            </div>
            <div class=\"two fields\">
                ";
        // line 35
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 35, $this->source); })()), "shopBillingData", [], "any", false, false, false, 35), "countryCode", [], "any", false, false, false, 35), 'row');
        echo "
                ";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 36, $this->source); })()), "shopBillingData", [], "any", false, false, false, 36), "street", [], "any", false, false, false, 36), 'row');
        echo "
            </div>
            <div class=\"two fields\">
                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 39, $this->source); })()), "shopBillingData", [], "any", false, false, false, 39), "city", [], "any", false, false, false, 39), 'row');
        echo "
                ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 40, $this->source); })()), "shopBillingData", [], "any", false, false, false, 40), "postcode", [], "any", false, false, false, 40), 'row');
        echo "
            </div>
        </div>

        ";
        // line 45
        echo "        <div class=\"ui segment\">
            <h4 class=\"ui dividing header\">";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("shop24.ui.courier_credentials"), "html", null, true);
        echo "</h4>
            <input type=\"hidden\" id=\"sylius_channel_address_firstName\" name=\"sylius_channel[address][firstName]\" value=\"-\">
            <input type=\"hidden\" id=\"sylius_channel_address_lastName\" name=\"sylius_channel[address][lastName]\" value=\"-\">
            <div class=\"two fields\">
                ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 50, $this->source); })()), "address", [], "any", false, false, false, 50), "company", [], "any", false, false, false, 50), 'row');
        echo "
                ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 51, $this->source); })()), "address", [], "any", false, false, false, 51), "phoneNumber", [], "any", false, false, false, 51), 'row');
        echo "
            </div>
            <div class=\"two fields\">
                ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 54, $this->source); })()), "address", [], "any", false, false, false, 54), "countryCode", [], "any", false, false, false, 54), 'row');
        echo "
                ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 55, $this->source); })()), "address", [], "any", false, false, false, 55), "city", [], "any", false, false, false, 55), 'row');
        echo "
            </div>
            <div class=\"two fields\">
                ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 58, $this->source); })()), "address", [], "any", false, false, false, 58), "street", [], "any", false, false, false, 58), 'row');
        echo "
                ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 59, $this->source); })()), "address", [], "any", false, false, false, 59), "postcode", [], "any", false, false, false, 59), 'row');
        echo "
            </div>
            <div class=\"two fields\">
                ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 62, $this->source); })()), "address", [], "any", false, false, false, 62), "houseNumber", [], "any", false, false, false, 62), 'row');
        echo "
            </div>
            ";
        // line 64
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 64, $this->source); })()), "shipperConfigs", [], "any", false, false, false, 64), 'row');
        echo "
        </div>
        ";
        // line 67
        echo "    </div>
    <div class=\"column\">
        <div class=\"ui segment\">
            ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 70, $this->source); })()), "locales", [], "any", false, false, false, 70), 'row');
        echo "

            ";
        // line 73
        echo "            ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 73, $this->source); })()), "countries", [], "any", false, false, false, 73), 'row');
        echo "
            ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 74, $this->source); })()), "logos", [], "any", false, false, false, 74), 'row');
        echo "
            ";
        // line 75
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 75, $this->source); })()), "images", [], "any", false, false, false, 75), 'row');
        echo "
            ";
        // line 77
        echo "
            ";
        // line 78
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 78, $this->source); })()), "defaultLocale", [], "any", false, false, false, 78), 'row');
        echo "
            ";
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 79, $this->source); })()), "currencies", [], "any", false, false, false, 79), 'row');
        echo "
            ";
        // line 80
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 80, $this->source); })()), "baseCurrency", [], "any", false, false, false, 80), 'row');
        echo "
            ";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 81, $this->source); })()), "defaultTaxZone", [], "any", false, false, false, 81), 'row');
        echo "
            ";
        // line 82
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 82, $this->source); })()), "taxCalculationStrategy", [], "any", false, false, false, 82), 'row');
        echo "
            ";
        // line 83
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 83, $this->source); })()), "skippingShippingStepAllowed", [], "any", false, false, false, 83), 'row');
        echo "
            ";
        // line 84
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 84, $this->source); })()), "skippingPaymentStepAllowed", [], "any", false, false, false, 84), 'row');
        echo "
            ";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 85, $this->source); })()), "accountVerificationRequired", [], "any", false, false, false, 85), 'row');
        echo "

            ";
        // line 87
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 87, $this->source); })()), "minimalCartSize", [], "any", false, false, false, 87), 'row');
        echo "
        </div>
    </div>

</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "bundles/SyliusAdminBundle/Channel/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 87,  244 => 85,  240 => 84,  236 => 83,  232 => 82,  228 => 81,  224 => 80,  220 => 79,  216 => 78,  213 => 77,  209 => 75,  205 => 74,  200 => 73,  195 => 70,  190 => 67,  185 => 64,  180 => 62,  174 => 59,  170 => 58,  164 => 55,  160 => 54,  154 => 51,  150 => 50,  143 => 46,  140 => 45,  133 => 40,  129 => 39,  123 => 36,  119 => 35,  113 => 32,  109 => 31,  104 => 29,  97 => 25,  93 => 24,  87 => 21,  82 => 19,  77 => 17,  71 => 14,  65 => 11,  61 => 10,  56 => 8,  52 => 7,  47 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_errors(form) }}
<div class=\"ui two column stackable grid\">
    <div class=\"column\">
        <div class=\"ui segment\">
            {{ form_errors(form) }}
            <div class=\"two fields\">
                {{ form_row(form.code) }}
                {{ form_row(form.name) }}
            </div>
            {{ form_row(form.description) }}
            {{ form_row(form.enabled) }}
            <div class=\"two fields\">
                <div class=\"field\">
                    {{ form_label(form.hostname) }}
                    <div class=\"ui labeled input\">
                        <div class=\"ui label\">http://</div>
                        {{ form_widget(form.hostname) }}
                    </div>
                    {{ form_errors(form.hostname) }}
                </div>
                {{ form_row(form.contactEmail) }}
            </div>
            <div class=\"two fields\">
                {{ form_row(form.color) }}
                {{ form_row(form.themeName) }}
            </div>
        </div>
        <div class=\"ui segment\">
            <h4 class=\"ui dividing header\">{{ form_label(form.shopBillingData) }}</h4>
            <div class=\"two fields\">
                {{ form_row(form.shopBillingData.company) }}
                {{ form_row(form.shopBillingData.taxId) }}
            </div>
            <div class=\"two fields\">
                {{ form_row(form.shopBillingData.countryCode) }}
                {{ form_row(form.shopBillingData.street) }}
            </div>
            <div class=\"two fields\">
                {{ form_row(form.shopBillingData.city) }}
                {{ form_row(form.shopBillingData.postcode) }}
            </div>
        </div>

        {# START: OmniSyliusShippingPlugin #}
        <div class=\"ui segment\">
            <h4 class=\"ui dividing header\">{{ 'shop24.ui.courier_credentials'|trans }}</h4>
            <input type=\"hidden\" id=\"sylius_channel_address_firstName\" name=\"sylius_channel[address][firstName]\" value=\"-\">
            <input type=\"hidden\" id=\"sylius_channel_address_lastName\" name=\"sylius_channel[address][lastName]\" value=\"-\">
            <div class=\"two fields\">
                {{ form_row(form.address.company) }}
                {{ form_row(form.address.phoneNumber) }}
            </div>
            <div class=\"two fields\">
                {{ form_row(form.address.countryCode) }}
                {{ form_row(form.address.city) }}
            </div>
            <div class=\"two fields\">
                {{ form_row(form.address.street) }}
                {{ form_row(form.address.postcode) }}
            </div>
            <div class=\"two fields\">
                {{ form_row(form.address.houseNumber) }}
            </div>
            {{ form_row(form.shipperConfigs) }}
        </div>
        {# END: OmniSyliusShippingPlugin #}
    </div>
    <div class=\"column\">
        <div class=\"ui segment\">
            {{ form_row(form.locales) }}

            {# START: OmniSyliusCorePlugin #}
            {{ form_row(form.countries) }}
            {{ form_row(form.logos) }}
            {{ form_row(form.images) }}
            {# END: OmniSyliusCorePlugin #}

            {{ form_row(form.defaultLocale) }}
            {{ form_row(form.currencies) }}
            {{ form_row(form.baseCurrency) }}
            {{ form_row(form.defaultTaxZone) }}
            {{ form_row(form.taxCalculationStrategy) }}
            {{ form_row(form.skippingShippingStepAllowed) }}
            {{ form_row(form.skippingPaymentStepAllowed) }}
            {{ form_row(form.accountVerificationRequired) }}

            {{ form_row(form.minimalCartSize) }}
        </div>
    </div>

</div>
", "bundles/SyliusAdminBundle/Channel/_form.html.twig", "/var/www/html/templates/bundles/SyliusAdminBundle/Channel/_form.html.twig");
    }
}
