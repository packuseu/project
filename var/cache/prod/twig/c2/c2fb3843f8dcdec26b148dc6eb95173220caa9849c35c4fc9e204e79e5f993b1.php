<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusCmsPlugin/Frontend/_footer.html.twig */
class __TwigTemplate_ecdcc8197c9ea397b80324ec8495969c4087f39cb99a6991b0beef811f7878ec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusCmsPlugin/Frontend/_footer.html.twig"));

        // line 1
        $context["footerNode"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeWithChildrenByType("footer_menu", 2);
        // line 2
        echo "
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(((twig_get_attribute($this->env, $this->source, ($context["footerNode"] ?? null), "children", [], "any", true, true, false, 3)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["footerNode"] ?? null), "children", [], "any", false, false, false, 3), [])) : ([])));
        foreach ($context['_seq'] as $context["_key"] => $context["placeholder"]) {
            if (twig_get_attribute($this->env, $this->source, $context["placeholder"], "enabled", [], "any", false, false, false, 3)) {
                // line 4
                echo "    <div class=\"three wide column\">
        <h4 class=\"ui inverted header\">";
                // line 5
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["placeholder"], "translation", [], "any", false, false, false, 5), "title", [], "any", false, false, false, 5)), "html", null, true);
                echo "</h4>
        <div class=\"ui inverted link list\">
            ";
                // line 7
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["placeholder"], "children", [], "any", false, false, false, 7));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    if (twig_get_attribute($this->env, $this->source, $context["item"], "enabled", [], "any", false, false, false, 7)) {
                        // line 8
                        echo "                ";
                        $context["url"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeUrl((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 8, $this->source); })()));
                        // line 9
                        echo "                ";
                        if (twig_test_empty(twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 9, $this->source); })()), "url", [], "any", false, false, false, 9))) {
                            echo " {
                    <span class=\"item\">";
                            // line 10
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 10, $this->source); })()), "translation", [], "any", false, false, false, 10), "title", [], "any", false, false, false, 10), "html", null, true);
                            echo "</span>
                ";
                        } else {
                            // line 12
                            echo "                    <a href=\"";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 12, $this->source); })()), "url", [], "any", false, false, false, 12), "html", null, true);
                            echo "\"";
                            if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 12, $this->source); })()), "target", [], "any", false, false, false, 12)) {
                                echo " target=\"";
                                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 12, $this->source); })()), "target", [], "any", false, false, false, 12), "html", null, true);
                                echo "\"";
                            }
                            echo " class=\"item\">";
                            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 12, $this->source); })()), "translation", [], "any", false, false, false, 12), "title", [], "any", false, false, false, 12), "html", null, true);
                            echo "</a>
                ";
                        }
                        // line 14
                        echo "            ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 15
                echo "        </div>
    </div>
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['placeholder'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusCmsPlugin/Frontend/_footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 15,  90 => 14,  76 => 12,  71 => 10,  66 => 9,  63 => 8,  58 => 7,  53 => 5,  50 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set footerNode = omni_sylius_get_node_with_children_by_type('footer_menu', 2) %}

{% for placeholder in footerNode.children|default([]) if placeholder.enabled %}
    <div class=\"three wide column\">
        <h4 class=\"ui inverted header\">{{ placeholder.translation.title|trans }}</h4>
        <div class=\"ui inverted link list\">
            {% for item in placeholder.children if item.enabled %}
                {% set url = omni_sylius_get_node_url(node) %}
                {% if url.url is empty %} {
                    <span class=\"item\">{{ node.translation.title }}</span>
                {% else %}
                    <a href=\"{{ url.url }}\"{% if url.target %} target=\"{{ url.target }}\"{% endif %} class=\"item\">{{ node.translation.title }}</a>
                {% endif %}
            {% endfor %}
        </div>
    </div>
{% endfor %}
", "@OmniSyliusCmsPlugin/Frontend/_footer.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Frontend/_footer.html.twig");
    }
}
