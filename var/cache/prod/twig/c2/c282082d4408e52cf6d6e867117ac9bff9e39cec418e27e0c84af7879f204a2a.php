<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusAdminBundle:Product/Show:_pricing.html.twig */
class __TwigTemplate_1f6b49cdef6aab7cf756232dd86833f9997d88dabb71fb0df7cbe108d4804fad extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusAdminBundle:Product/Show:_pricing.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusAdmin/Common/Macro/money.html.twig", "SyliusAdminBundle:Product/Show:_pricing.html.twig", 1)->unwrap();
        // line 2
        echo "<div id=\"pricing\">
    <h4 class=\"ui top attached large header\">";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.pricing"), "html", null, true);
        echo "</h4>
    <div class=\"ui attached segment\">
        <table id=\"pricing\" class=\"ui very basic celled table\">
            <thead>
                <tr>
                    <th><strong class=\"gray text\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.channels"), "html", null, true);
        echo "</strong></th>
                    <th><strong class=\"gray text\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.price"), "html", null, true);
        echo "</strong></th>
                    <th><strong class=\"gray text\">";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.original_price"), "html", null, true);
        echo "</strong></th>
                </tr>
            </thead>
            <tbody>
            ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 14, $this->source); })()), "variants", [], "any", false, false, false, 14), "first", [], "any", false, false, false, 14), "channelPricings", [], "any", false, false, false, 14));
        foreach ($context['_seq'] as $context["_key"] => $context["channelPricing"]) {
            // line 15
            echo "                <tr>
                    <td class=\"five wide gray text\">
                        <strong>";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Sylius\Bundle\AdminBundle\Twig\ChannelNameExtension']->getChannelNameByCode(twig_get_attribute($this->env, $this->source, $context["channelPricing"], "channelCode", [], "any", false, false, false, 17)), "html", null, true);
            echo " </strong>
                    </td>
                    <td>";
            // line 19
            echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, $context["channelPricing"], "price", [], "any", false, false, false, 19), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 19, $this->source); })()), "channels", [], "any", false, false, false, 19), "first", [], "any", false, false, false, 19), "baseCurrency", [], "any", false, false, false, 19)], 19, $context, $this->getSourceContext());
            echo "</td>
                    ";
            // line 20
            if ((twig_get_attribute($this->env, $this->source, $context["channelPricing"], "originalPrice", [], "any", false, false, false, 20) != null)) {
                // line 21
                echo "                        <td>";
                echo twig_call_macro($macros["money"], "macro_format", [twig_get_attribute($this->env, $this->source, $context["channelPricing"], "originalPrice", [], "any", false, false, false, 21), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 21, $this->source); })()), "channels", [], "any", false, false, false, 21), "first", [], "any", false, false, false, 21), "baseCurrency", [], "any", false, false, false, 21)], 21, $context, $this->getSourceContext());
                echo "</td>
                    ";
            } else {
                // line 23
                echo "                        <td>-</td>
                    ";
            }
            // line 25
            echo "                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channelPricing'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "            </tbody>
        </table>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusAdminBundle:Product/Show:_pricing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 27,  97 => 25,  93 => 23,  87 => 21,  85 => 20,  81 => 19,  76 => 17,  72 => 15,  68 => 14,  61 => 10,  57 => 9,  53 => 8,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusAdmin/Common/Macro/money.html.twig\" as money %}
<div id=\"pricing\">
    <h4 class=\"ui top attached large header\">{{ 'sylius.ui.pricing'|trans }}</h4>
    <div class=\"ui attached segment\">
        <table id=\"pricing\" class=\"ui very basic celled table\">
            <thead>
                <tr>
                    <th><strong class=\"gray text\">{{ 'sylius.ui.channels'|trans }}</strong></th>
                    <th><strong class=\"gray text\">{{ 'sylius.ui.price'|trans }}</strong></th>
                    <th><strong class=\"gray text\">{{ 'sylius.ui.original_price'|trans }}</strong></th>
                </tr>
            </thead>
            <tbody>
            {% for channelPricing in product.variants.first.channelPricings %}
                <tr>
                    <td class=\"five wide gray text\">
                        <strong>{{ channelPricing.channelCode|sylius_channel_name }} </strong>
                    </td>
                    <td>{{ money.format(channelPricing.price, product.channels.first.baseCurrency) }}</td>
                    {% if channelPricing.originalPrice != null %}
                        <td>{{ money.format(channelPricing.originalPrice, product.channels.first.baseCurrency) }}</td>
                    {% else %}
                        <td>-</td>
                    {% endif %}
                </tr>
            {% endfor %}
            </tbody>
        </table>
    </div>
</div>
", "SyliusAdminBundle:Product/Show:_pricing.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/Product/Show/_pricing.html.twig");
    }
}
