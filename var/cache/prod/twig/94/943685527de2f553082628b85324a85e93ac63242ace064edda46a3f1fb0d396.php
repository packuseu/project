<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Banner/PositionType/_cta.html.twig */
class __TwigTemplate_468835ce67a2fd56ded6e27b0e31ddfcd947101f90d647d31fe5a6dc6e2131fc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Banner/PositionType/_cta.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["banners"] ?? null), 0, [], "array", false, true, false, 1), "images", [], "any", false, true, false, 1), 0, [], "array", true, true, false, 1)) {
            // line 2
            echo "    ";
            $context["imagePath"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 2, $this->source); })()), 0, [], "array", false, false, false, 2), "images", [], "any", false, false, false, 2), 0, [], "array", false, false, false, 2), "path", [], "any", false, false, false, 2), "omni_sylius_banner");
            // line 3
            echo "
    <section class=\"cta-section\">
        <div class=\"row\">
            <div class=\"col-12 col-lg-6 cta-section__text\">
                <h3>
                    ";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 8, $this->source); })()), "translation", [], "any", false, false, false, 8), "title", [], "any", false, false, false, 8), "html", null, true);
            echo "
                </h3>
                <p>
                    ";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 11, $this->source); })()), "translation", [], "any", false, false, false, 11), "description", [], "any", false, false, false, 11), "html", null, true);
            echo "
                </p>
            </div>
            <div class=\"col-12 col-lg-6 d-flex align-items-center justify-content-center cta-section__logo\">
                <img class=\"logo\" src=\"";
            // line 15
            echo twig_escape_filter($this->env, (isset($context["imagePath"]) || array_key_exists("imagePath", $context) ? $context["imagePath"] : (function () { throw new RuntimeError('Variable "imagePath" does not exist.', 15, $this->source); })()), "html", null, true);
            echo "\" alt=\"\" />
            </div>
        </div>
    </section>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Banner/PositionType/_cta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 15,  58 => 11,  52 => 8,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if banners[0].images[0] is defined %}
    {% set imagePath = banners[0].images[0].path|imagine_filter('omni_sylius_banner') %}

    <section class=\"cta-section\">
        <div class=\"row\">
            <div class=\"col-12 col-lg-6 cta-section__text\">
                <h3>
                    {{ position.translation.title }}
                </h3>
                <p>
                    {{ position.translation.description }}
                </p>
            </div>
            <div class=\"col-12 col-lg-6 d-flex align-items-center justify-content-center cta-section__logo\">
                <img class=\"logo\" src=\"{{ imagePath }}\" alt=\"\" />
            </div>
        </div>
    </section>
{% endif %}
", "Banner/PositionType/_cta.html.twig", "/var/www/html/templates/Banner/PositionType/_cta.html.twig");
    }
}
