<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @SyliusAdmin/Product/Show/_simpleProduct.html.twig */
class __TwigTemplate_a2240d88d4b9097f9e7b02eae2830010ec5be9ed45c4080667d50c4c3d6fd918 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig"));

        // line 1
        $this->loadTemplate("@SyliusAdmin/Product/Show/_header.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 1)->display($context);
        // line 2
        echo "
<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        ";
        // line 5
        $this->loadTemplate("@SyliusAdmin/Product/Show/_details.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 5)->display($context);
        // line 6
        echo "        <div class=\"ui hidden divider\"></div>
        ";
        // line 7
        $this->loadTemplate("@SyliusAdmin/Product/Show/_taxonomy.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 7)->display($context);
        // line 8
        echo "    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        ";
        // line 10
        $this->loadTemplate("@SyliusAdmin/Product/Show/_pricing.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 10)->display($context);
        // line 11
        echo "        <div class=\"ui hidden divider\"></div>
        ";
        // line 12
        $this->loadTemplate("@SyliusAdmin/Product/Show/_shipping.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 12)->display($context);
        // line 13
        echo "    </div>
</div>
<div class=\"ui hidden divider\"></div>
";
        // line 16
        $this->loadTemplate("@SyliusAdmin/Product/Show/_media.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 16)->display($context);
        // line 17
        echo "<div class=\"ui hidden divider\"></div>
";
        // line 18
        $this->loadTemplate("@SyliusAdmin/Product/Show/_moreDetails.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 18)->display($context);
        // line 19
        echo "<div class=\"ui hidden divider\"></div>
<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        ";
        // line 22
        $this->loadTemplate("@SyliusAdmin/Product/Show/_attributes.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 22)->display($context);
        // line 23
        echo "    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        ";
        // line 25
        $this->loadTemplate("@SyliusAdmin/Product/Show/_associations.html.twig", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", 25)->display($context);
        // line 26
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@SyliusAdmin/Product/Show/_simpleProduct.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 26,  88 => 25,  84 => 23,  82 => 22,  77 => 19,  75 => 18,  72 => 17,  70 => 16,  65 => 13,  63 => 12,  60 => 11,  58 => 10,  54 => 8,  52 => 7,  49 => 6,  47 => 5,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include '@SyliusAdmin/Product/Show/_header.html.twig' %}

<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_details.html.twig' %}
        <div class=\"ui hidden divider\"></div>
        {% include '@SyliusAdmin/Product/Show/_taxonomy.html.twig' %}
    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_pricing.html.twig' %}
        <div class=\"ui hidden divider\"></div>
        {% include '@SyliusAdmin/Product/Show/_shipping.html.twig' %}
    </div>
</div>
<div class=\"ui hidden divider\"></div>
{% include '@SyliusAdmin/Product/Show/_media.html.twig' %}
<div class=\"ui hidden divider\"></div>
{% include '@SyliusAdmin/Product/Show/_moreDetails.html.twig' %}
<div class=\"ui hidden divider\"></div>
<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_attributes.html.twig' %}
    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_associations.html.twig' %}
    </div>
</div>
", "@SyliusAdmin/Product/Show/_simpleProduct.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/Product/Show/_simpleProduct.html.twig");
    }
}
