<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product/Index:_sidebar.html.twig */
class __TwigTemplate_0ba921fbca432745a64edbbd243c1e19c179794d0954638979a450c5c1d8f3bf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product/Index:_sidebar.html.twig"));

        // line 1
        $context["path"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "attributes", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1), twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "attributes", [], "any", false, false, false, 1), "get", [0 => "_route_params"], "method", false, false, false, 1));
        // line 2
        $context["definition"] = twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 2, $this->source); })()), "definition", [], "any", false, false, false, 2);
        // line 3
        $context["data"] = twig_get_attribute($this->env, $this->source, (isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 3, $this->source); })()), "data", [], "any", false, false, false, 3);
        // line 4
        echo "
";
        // line 5
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 5, $this->source); })()), "enabledFilters", [], "any", false, false, false, 5)) > 0)) {
            // line 6
            echo "    <div class=\"filter-container mb-4\">
        <form method=\"get\" action=\"";
            // line 7
            echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 7, $this->source); })()), "html", null, true);
            echo "\" class=\"loadable\">
            <div class=\"\">
                ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->extensions['Sylius\Bundle\UiBundle\Twig\SortByExtension']->sortBy(twig_get_attribute($this->env, $this->source, (isset($context["definition"]) || array_key_exists("definition", $context) ? $context["definition"] : (function () { throw new RuntimeError('Variable "definition" does not exist.', 9, $this->source); })()), "enabledFilters", [], "any", false, false, false, 9), "position"));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            foreach ($context['_seq'] as $context["_key"] => $context["filter"]) {
                if (twig_get_attribute($this->env, $this->source, $context["filter"], "enabled", [], "any", false, false, false, 9)) {
                    // line 10
                    echo "                    ";
                    if ((twig_get_attribute($this->env, $this->source, $context["filter"], "name", [], "any", false, false, false, 10) != "price")) {
                        // line 11
                        echo "                        ";
                        echo call_user_func_array($this->env->getFunction('sylius_grid_render_filter')->getCallable(), [(isset($context["resources"]) || array_key_exists("resources", $context) ? $context["resources"] : (function () { throw new RuntimeError('Variable "resources" does not exist.', 11, $this->source); })()), $context["filter"]]);
                        echo "
                        ";
                        // line 12
                        if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index0", [], "any", false, false, false, 12) % 2)) {
                            // line 13
                            echo "                            </div>
                            <div class=\"\">
                        ";
                        }
                        // line 16
                        echo "                    ";
                    }
                    // line 17
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filter'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "            </div>
            <div class='mt-2'>
                <button class=\"btn btn-primary\" type=\"submit\">
                    ";
            // line 21
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.filter"), "html", null, true);
            echo "
                </button>
                <a class=\"btn btn-outline-primary\" href=\"";
            // line 23
            echo twig_escape_filter($this->env, (isset($context["path"]) || array_key_exists("path", $context) ? $context["path"] : (function () { throw new RuntimeError('Variable "path" does not exist.', 23, $this->source); })()), "html", null, true);
            echo "\">
                    ";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.clear_filters"), "html", null, true);
            echo "
                </a>
            </div>
        </form>
    </div>
";
        }
        // line 30
        echo "
";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sylius_shop_partial_taxon_show_by_slug", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 32
(isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 32, $this->source); })()), "request", [], "any", false, false, false, 32), "attributes", [], "any", false, false, false, 32), "get", [0 => "slug"], "method", false, false, false, 32), "template" => "@SyliusShop/Taxon/_verticalMenu.html.twig"]));
        // line 34
        echo "

";
        // line 36
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "sidebar-bottom-zone");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product/Index:_sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 36,  127 => 34,  125 => 32,  124 => 31,  121 => 30,  112 => 24,  108 => 23,  103 => 21,  98 => 18,  88 => 17,  85 => 16,  80 => 13,  78 => 12,  73 => 11,  70 => 10,  59 => 9,  54 => 7,  51 => 6,  49 => 5,  46 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set path = path(app.request.attributes.get('_route'), app.request.attributes.get('_route_params')) %}
{% set definition = resources.definition %}
{% set data = resources.data %}

{% if definition.enabledFilters|length > 0 %}
    <div class=\"filter-container mb-4\">
        <form method=\"get\" action=\"{{ path }}\" class=\"loadable\">
            <div class=\"\">
                {% for filter in definition.enabledFilters|sort_by('position') if filter.enabled %}
                    {% if filter.name != 'price' %}
                        {{ sylius_grid_render_filter(resources, filter) }}
                        {% if loop.index0 % 2 %}
                            </div>
                            <div class=\"\">
                        {% endif %}
                    {% endif %}
                {% endfor %}
            </div>
            <div class='mt-2'>
                <button class=\"btn btn-primary\" type=\"submit\">
                    {{ 'sylius.ui.filter'|trans }}
                </button>
                <a class=\"btn btn-outline-primary\" href=\"{{ path }}\">
                    {{ 'sylius.ui.clear_filters'|trans }}
                </a>
            </div>
        </form>
    </div>
{% endif %}

{{ render(url('sylius_shop_partial_taxon_show_by_slug', {
    'slug': app.request.attributes.get('slug'),
    'template': '@SyliusShop/Taxon/_verticalMenu.html.twig'
})) }}

{{ omni_sylius_banner_zone('sidebar-bottom-zone') }}
", "SyliusShopBundle:Product/Index:_sidebar.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Product/Index/_sidebar.html.twig");
    }
}
