<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Account/AddressBook:_defaultAddress.html.twig */
class __TwigTemplate_543a9fcb988e7a06df18064e08e017de6ccb89d716b459bf9b28fdb2cd2de3e7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Account/AddressBook:_defaultAddress.html.twig"));

        // line 1
        echo "<div class=\"card border-primary\">
    <div class=\"card-body\">
        ";
        // line 3
        $this->loadTemplate("@SyliusShop/Common/_address.html.twig", "SyliusShopBundle:Account/AddressBook:_defaultAddress.html.twig", 3)->display(twig_array_merge($context, ["address" => (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 3, $this->source); })())]));
        // line 4
        echo "        <div class=\"d-flex justify-content-between\">
            <button class=\"btn btn-outline-primary\" type=\"button\" disabled>";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.your_default_address"), "html", null, true);
        echo "</button>
            <div class=\"btn-group\">
                <a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_account_address_book_update", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 7, $this->source); })()), "id", [], "any", false, false, false, 7)]), "html", null, true);
        echo "\" class=\"btn btn-link\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.edit"), "html", null, true);
        echo "</a>

                <form action=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_account_address_book_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 9, $this->source); })()), "id", [], "any", false, false, false, 9)]), "html", null, true);
        echo "\" method=\"post\" onsubmit=\"return confirm('";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.are_your_sure_you_want_to_perform_this_action"), "html", null, true);
        echo "');\">
                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                    <button class=\"btn btn-link text-muted\" type=\"submit\">
                        ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.delete"), "html", null, true);
        echo "
                    </button>
                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(twig_get_attribute($this->env, $this->source, (isset($context["address"]) || array_key_exists("address", $context) ? $context["address"] : (function () { throw new RuntimeError('Variable "address" does not exist.', 14, $this->source); })()), "id", [], "any", false, false, false, 14)), "html", null, true);
        echo "\" />
                </form>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Account/AddressBook:_defaultAddress.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  69 => 12,  61 => 9,  54 => 7,  49 => 5,  46 => 4,  44 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"card border-primary\">
    <div class=\"card-body\">
        {% include '@SyliusShop/Common/_address.html.twig' with {'address': address} %}
        <div class=\"d-flex justify-content-between\">
            <button class=\"btn btn-outline-primary\" type=\"button\" disabled>{{ 'sylius.ui.your_default_address'|trans }}</button>
            <div class=\"btn-group\">
                <a href=\"{{ path('sylius_shop_account_address_book_update', {'id': address.id}) }}\" class=\"btn btn-link\">{{ 'sylius.ui.edit'|trans }}</a>

                <form action=\"{{ path('sylius_shop_account_address_book_delete', {'id': address.id}) }}\" method=\"post\" onsubmit=\"return confirm('{{ \"sylius.ui.are_your_sure_you_want_to_perform_this_action\"|trans }}');\">
                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                    <button class=\"btn btn-link text-muted\" type=\"submit\">
                        {{ 'sylius.ui.delete'|trans }}
                    </button>
                    <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token(address.id) }}\" />
                </form>
            </div>
        </div>
    </div>
</div>
", "SyliusShopBundle:Account/AddressBook:_defaultAddress.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Account/AddressBook/_defaultAddress.html.twig");
    }
}
