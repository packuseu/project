<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @Brille24SyliusTierPricePlugin/Admin/ProductVariant/Tab/_tierprice.html.twig */
class __TwigTemplate_126bcde92db7889f83f36962c4fde897b699849da1a4ef4a5ade7b77d93a946d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@Brille24SyliusTierPricePlugin/Admin/ProductVariant/Tab/_tierprice.html.twig"));

        // line 31
        echo "
";
        // line 33
        $this->displayBlock('content', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 34
        echo "    ";
        $macros["row"] = $this;
        // line 35
        echo "    <div class=\"ui tab\" data-tab=\"tierprice\">
        ";
        // line 36
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "tierPrices", [], "any", true, true, false, 36)) {
            // line 37
            echo "            ";
            $context["variantForm"] = (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 37, $this->source); })());
            // line 38
            echo "        ";
        } else {
            // line 39
            echo "            ";
            $context["variantForm"] = twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 39, $this->source); })()), "variant", [], "any", false, false, false, 39);
            // line 40
            echo "        ";
        }
        // line 41
        echo "        ";
        // line 42
        echo "        <script>
            var tierPriceIndex = ";
        // line 43
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["variantForm"]) || array_key_exists("variantForm", $context) ? $context["variantForm"] : (function () { throw new RuntimeError('Variable "variantForm" does not exist.', 43, $this->source); })()), "tierPrices", [], "any", false, false, false, 43)), "html", null, true);
        echo ";
        </script>

        ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["variantForm"]) || array_key_exists("variantForm", $context) ? $context["variantForm"] : (function () { throw new RuntimeError('Variable "variantForm" does not exist.', 46, $this->source); })()), 'errors');
        echo "

        ";
        // line 49
        echo "        <span data-prototype=\"";
        echo twig_escape_filter($this->env, twig_call_macro($macros["row"], "macro_tierPriceRow", [twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["variantForm"]) || array_key_exists("variantForm", $context) ? $context["variantForm"] : (function () { throw new RuntimeError('Variable "variantForm" does not exist.', 49, $this->source); })()), "tierPrices", [], "any", false, false, false, 49), "vars", [], "any", false, false, false, 49), "prototype", [], "any", false, false, false, 49), "__name__"], 49, $context, $this->getSourceContext()), "html_attr");
        echo "\"
              id=\"prototype_holder\">
        </span>

        ";
        // line 54
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["variantForm"]) || array_key_exists("variantForm", $context) ? $context["variantForm"] : (function () { throw new RuntimeError('Variable "variantForm" does not exist.', 54, $this->source); })()), "vars", [], "any", false, false, false, 54), "data", [], "any", false, false, false, 54), "product", [], "any", false, false, false, 54), "channels", [], "any", false, false, false, 54));
        foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
            // line 55
            echo "            <h3 class=\"ui dividing header\"> ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "name", [], "any", false, false, false, 55), "html", null, true);
            echo "</h3>

            ";
            // line 57
            $context["bodyId"] = ("tierPricesTable_" . twig_get_attribute($this->env, $this->source, $context["channel"], "id", [], "any", false, false, false, 57));
            // line 58
            echo "
            <table class=\"ui stackable celled table\" id=\"";
            // line 59
            echo twig_escape_filter($this->env, (isset($context["bodyId"]) || array_key_exists("bodyId", $context) ? $context["bodyId"] : (function () { throw new RuntimeError('Variable "bodyId" does not exist.', 59, $this->source); })()), "html", null, true);
            echo "_table\"
                   ";
            // line 61
            echo "                   data-prototype=\"";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('sylius_currency_symbol')->getCallable(), [twig_get_attribute($this->env, $this->source, $context["channel"], "baseCurrency", [], "any", false, false, false, 61)]), "html", null, true);
            echo "\">
                <thead>
                <tr>
                    <th class=\"table-column-quantity\">";
            // line 64
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.quantity"), "html", null, true);
            echo "</th>
                    <th>";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.unit_price"), "html", null, true);
            echo " </th>
                    <th>";
            // line 66
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("brille24_tier_price.ui.customer_group"), "html", null, true);
            echo " </th>
                    <th>";
            // line 67
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.delete"), "html", null, true);
            echo "</th>
                </tr>
                </thead>
                <tbody id=\"";
            // line 70
            echo twig_escape_filter($this->env, (isset($context["bodyId"]) || array_key_exists("bodyId", $context) ? $context["bodyId"] : (function () { throw new RuntimeError('Variable "bodyId" does not exist.', 70, $this->source); })()), "html", null, true);
            echo "\">
                ";
            // line 72
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["variantForm"]) || array_key_exists("variantForm", $context) ? $context["variantForm"] : (function () { throw new RuntimeError('Variable "variantForm" does not exist.', 72, $this->source); })()), "tierPrices", [], "any", false, false, false, 72));
            foreach ($context['_seq'] as $context["i"] => $context["tierprice"]) {
                // line 73
                echo "                    ";
                if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["tierprice"], "channel", [], "any", false, false, false, 73), "vars", [], "any", false, false, false, 73), "value", [], "array", false, false, false, 73) == twig_get_attribute($this->env, $this->source, $context["channel"], "code", [], "any", false, false, false, 73))) {
                    // line 74
                    echo "                        ";
                    echo twig_call_macro($macros["row"], "macro_tierPriceRow", [$context["tierprice"], $context["i"], $context["channel"]], 74, $context, $this->getSourceContext());
                    echo "
                    ";
                }
                // line 76
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['tierprice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 77
            echo "                </tbody>
            </table>

            ";
            // line 81
            echo "            <div class=\"ui labeled icon primary button\" onclick=\"tierPriceTableAdd('";
            echo twig_escape_filter($this->env, (isset($context["bodyId"]) || array_key_exists("bodyId", $context) ? $context["bodyId"] : (function () { throw new RuntimeError('Variable "bodyId" does not exist.', 81, $this->source); })()), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["channel"], "code", [], "any", false, false, false, 81), "html", null, true);
            echo "')\">
                <i class=\"add icon\"></i> ";
            // line 82
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.add"), "html", null, true);
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "
        ";
        // line 86
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [(("sylius.admin.product_variant." . (isset($context["action"]) || array_key_exists("action", $context) ? $context["action"] : (function () { throw new RuntimeError('Variable "action" does not exist.', 86, $this->source); })())) . ".tab_tierprice"), ["form" => (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 86, $this->source); })())]]);
        echo "
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function macro_tierPriceRow($__tierprice__ = null, $__index__ = null, $__channel__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "tierprice" => $__tierprice__,
            "index" => $__index__,
            "channel" => $__channel__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "tierPriceRow"));

            // line 3
            echo "    <tr class=\"item\">
        <td>
            <div class=\"field\">
                ";
            // line 6
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 6, $this->source); })()), "qty", [], "any", false, false, false, 6), 'widget', ["attr" => ["class" => "TIERPRICE_SORTING_CHANGED"]]);
            echo "
                ";
            // line 7
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 7, $this->source); })()), "qty", [], "any", false, false, false, 7), 'errors');
            echo "
            </div>
        </td>
        <td>
            ";
            // line 11
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 11, $this->source); })()), "channel", [], "any", false, false, false, 11), 'widget');
            echo "
            <div class=\"field priceField\">
                ";
            // line 13
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 13, $this->source); })()), "price", [], "any", false, false, false, 13), 'widget', ["currency" => ((twig_get_attribute($this->env, $this->source, ($context["channel"] ?? null), "baseCurrency", [], "any", true, true, false, 13)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["channel"] ?? null), "baseCurrency", [], "any", false, false, false, 13), "USD")) : ("USD"))]);
            echo "
                ";
            // line 14
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 14, $this->source); })()), "price", [], "any", false, false, false, 14), 'errors');
            echo "
            </div>
        </td>
        <td>
            ";
            // line 18
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 18, $this->source); })()), "channel", [], "any", false, false, false, 18), 'widget');
            echo "
            <div class=\"field\">
                ";
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 20, $this->source); })()), "customerGroup", [], "any", false, false, false, 20), 'widget', ["attr" => ["class" => "ui dropdown"]]);
            echo "
                ";
            // line 21
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["tierprice"]) || array_key_exists("tierprice", $context) ? $context["tierprice"] : (function () { throw new RuntimeError('Variable "tierprice" does not exist.', 21, $this->source); })()), "customerGroup", [], "any", false, false, false, 21), 'errors');
            echo "
            </div>
        </td>
        <td>
            <a class=\"ui red labeled icon button\" onclick=\"tierPriceTableRemove(this);\">
                <i class=\"trash icon\"></i>";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.delete"), "html", null, true);
            echo "
            </a>
        </td>
    </tr>
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@Brille24SyliusTierPricePlugin/Admin/ProductVariant/Tab/_tierprice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 26,  263 => 21,  259 => 20,  254 => 18,  247 => 14,  243 => 13,  238 => 11,  231 => 7,  227 => 6,  222 => 3,  204 => 2,  194 => 86,  191 => 85,  182 => 82,  175 => 81,  170 => 77,  164 => 76,  158 => 74,  155 => 73,  150 => 72,  146 => 70,  140 => 67,  136 => 66,  132 => 65,  128 => 64,  121 => 61,  117 => 59,  114 => 58,  112 => 57,  106 => 55,  101 => 54,  93 => 49,  88 => 46,  82 => 43,  79 => 42,  77 => 41,  74 => 40,  71 => 39,  68 => 38,  65 => 37,  63 => 36,  60 => 35,  57 => 34,  44 => 33,  41 => 31,);
    }

    public function getSourceContext()
    {
        return new Source("{# A macro rendering a row of the tier price table#}
{% macro tierPriceRow(tierprice, index, channel) %}
    <tr class=\"item\">
        <td>
            <div class=\"field\">
                {{ form_widget(tierprice.qty, {'attr' : {'class': 'TIERPRICE_SORTING_CHANGED'}}) }}
                {{ form_errors(tierprice.qty) }}
            </div>
        </td>
        <td>
            {{ form_widget(tierprice.channel) }}
            <div class=\"field priceField\">
                {{ form_widget(tierprice.price, {currency: channel.baseCurrency|default('USD')}) }}
                {{ form_errors(tierprice.price) }}
            </div>
        </td>
        <td>
            {{ form_widget(tierprice.channel) }}
            <div class=\"field\">
                {{ form_widget(tierprice.customerGroup, {'attr': {'class': 'ui dropdown'}}) }}
                {{ form_errors(tierprice.customerGroup) }}
            </div>
        </td>
        <td>
            <a class=\"ui red labeled icon button\" onclick=\"tierPriceTableRemove(this);\">
                <i class=\"trash icon\"></i>{{ 'sylius.ui.delete'|trans }}
            </a>
        </td>
    </tr>
{% endmacro %}

{# The tierprice tab #}
{% block content %}
    {% import _self as row %}
    <div class=\"ui tab\" data-tab=\"tierprice\">
        {% if form.tierPrices is defined %}
            {% set variantForm = form %}
        {% else %}
            {% set variantForm = form.variant %}
        {% endif %}
        {# Setting the tierPriceIndex for javascript, to generate incremental ids #}
        <script>
            var tierPriceIndex = {{ variantForm.tierPrices|length }};
        </script>

        {{ form_errors(variantForm) }}

        {# Generating the new row prototype#}
        <span data-prototype=\"{{ row.tierPriceRow(variantForm.tierPrices.vars.prototype, '__name__')|e('html_attr') }}\"
              id=\"prototype_holder\">
        </span>

        {# Itterating over the channels #}
        {% for channel in variantForm.vars.data.product.channels %}
            <h3 class=\"ui dividing header\"> {{ channel.name }}</h3>

            {% set bodyId = \"tierPricesTable_\" ~ channel.id %}

            <table class=\"ui stackable celled table\" id=\"{{ bodyId }}_table\"
                   {# Setting the default currency for this table (channel) #}
                   data-prototype=\"{{ channel.baseCurrency|sylius_currency_symbol }}\">
                <thead>
                <tr>
                    <th class=\"table-column-quantity\">{{ 'sylius.ui.quantity'|trans }}</th>
                    <th>{{ 'sylius.ui.unit_price'|trans }} </th>
                    <th>{{ 'brille24_tier_price.ui.customer_group'|trans }} </th>
                    <th>{{ 'sylius.ui.delete'|trans }}</th>
                </tr>
                </thead>
                <tbody id=\"{{ bodyId }}\">
                {# Rendering the table body #}
                {% for i, tierprice in variantForm.tierPrices %}
                    {% if tierprice.channel.vars['value'] == channel.code %}
                        {{ row.tierPriceRow(tierprice, i, channel) }}
                    {% endif %}
                {% endfor %}
                </tbody>
            </table>

            {# The add button for every table #}
            <div class=\"ui labeled icon primary button\" onclick=\"tierPriceTableAdd('{{ bodyId }}', '{{ channel.code }}')\">
                <i class=\"add icon\"></i> {{ 'sylius.ui.add'|trans }}
            </div>
        {% endfor %}

        {{ sonata_block_render_event('sylius.admin.product_variant.' ~ action ~ '.tab_tierprice', {'form': form }) }}
    </div>
{% endblock %}
", "@Brille24SyliusTierPricePlugin/Admin/ProductVariant/Tab/_tierprice.html.twig", "/var/www/html/vendor/brille24/sylius-tierprice-plugin/src/Resources/views/Admin/ProductVariant/Tab/_tierprice.html.twig");
    }
}
