<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusBannerPlugin:Admin:_breadcrumbs.html.twig */
class __TwigTemplate_2b250c6823c0139209b2d683e51777d2ac6a16b522c2612dfd7f0d7f353d7076 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusBannerPlugin:Admin:_breadcrumbs.html.twig"));

        // line 1
        $macros["breadcrumb"] = $this->macros["breadcrumb"] = $this->loadTemplate("@SyliusAdmin/Macro/breadcrumb.html.twig", "OmniSyliusBannerPlugin:Admin:_breadcrumbs.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["breadcrumbs"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 3), "breadcrumbs", [], "any", true, true, false, 3)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 3), "breadcrumbs", [], "any", false, false, false, 3), [])) : ([]));
        // line 4
        $context["processed_bredcrumbs"] = [];
        // line 5
        echo "
";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) || array_key_exists("breadcrumbs", $context) ? $context["breadcrumbs"] : (function () { throw new RuntimeError('Variable "breadcrumbs" does not exist.', 6, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb_config"]) {
            // line 7
            echo "    ";
            $context["current_breadcrumb"] = ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "label", [], "any", false, false, false, 7))];
            // line 8
            echo "
    ";
            // line 9
            if (twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "route", [], "any", true, true, false, 9)) {
                // line 10
                echo "        ";
                $context["should_fallback"] = false;
                // line 11
                echo "
        ";
                // line 12
                if (twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "required_params", [], "any", true, true, false, 12)) {
                    // line 13
                    echo "            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "required_params", [], "any", false, false, false, 13));
                    foreach ($context['_seq'] as $context["_key"] => $context["required"]) {
                        // line 14
                        echo "                ";
                        if (( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "params", [], "any", false, true, false, 14), $context["required"], [], "array", true, true, false, 14) || twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "params", [], "any", false, false, false, 14), $context["required"], [], "array", false, false, false, 14)))) {
                            // line 15
                            echo "                    ";
                            $context["should_fallback"] = true;
                            // line 16
                            echo "                ";
                        }
                        // line 17
                        echo "            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['required'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 18
                    echo "        ";
                }
                // line 19
                echo "
        ";
                // line 20
                if ((isset($context["should_fallback"]) || array_key_exists("should_fallback", $context) ? $context["should_fallback"] : (function () { throw new RuntimeError('Variable "should_fallback" does not exist.', 20, $this->source); })())) {
                    // line 21
                    echo "            ";
                    $context["current_breadcrumb"] = twig_array_merge((isset($context["current_breadcrumb"]) || array_key_exists("current_breadcrumb", $context) ? $context["current_breadcrumb"] : (function () { throw new RuntimeError('Variable "current_breadcrumb" does not exist.', 21, $this->source); })()), ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "fallback_route", [], "any", false, false, false, 21), ((twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "params", [], "any", true, true, false, 21)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "params", [], "any", false, false, false, 21), [])) : ([])))]);
                    // line 22
                    echo "        ";
                } else {
                    // line 23
                    echo "            ";
                    $context["current_breadcrumb"] = twig_array_merge((isset($context["current_breadcrumb"]) || array_key_exists("current_breadcrumb", $context) ? $context["current_breadcrumb"] : (function () { throw new RuntimeError('Variable "current_breadcrumb" does not exist.', 23, $this->source); })()), ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "route", [], "any", false, false, false, 23), ((twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "params", [], "any", true, true, false, 23)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, $context["breadcrumb_config"], "params", [], "any", false, false, false, 23), [])) : ([])))]);
                    // line 24
                    echo "        ";
                }
                // line 25
                echo "    ";
            }
            // line 26
            echo "
    ";
            // line 27
            $context["processed_bredcrumbs"] = twig_array_merge((isset($context["processed_bredcrumbs"]) || array_key_exists("processed_bredcrumbs", $context) ? $context["processed_bredcrumbs"] : (function () { throw new RuntimeError('Variable "processed_bredcrumbs" does not exist.', 27, $this->source); })()), [0 => (isset($context["current_breadcrumb"]) || array_key_exists("current_breadcrumb", $context) ? $context["current_breadcrumb"] : (function () { throw new RuntimeError('Variable "current_breadcrumb" does not exist.', 27, $this->source); })())]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb_config'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "
";
        // line 30
        echo twig_call_macro($macros["breadcrumb"], "macro_crumble", [(isset($context["processed_bredcrumbs"]) || array_key_exists("processed_bredcrumbs", $context) ? $context["processed_bredcrumbs"] : (function () { throw new RuntimeError('Variable "processed_bredcrumbs" does not exist.', 30, $this->source); })())], 30, $context, $this->getSourceContext());
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusBannerPlugin:Admin:_breadcrumbs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 30,  124 => 29,  118 => 27,  115 => 26,  112 => 25,  109 => 24,  106 => 23,  103 => 22,  100 => 21,  98 => 20,  95 => 19,  92 => 18,  86 => 17,  83 => 16,  80 => 15,  77 => 14,  72 => 13,  70 => 12,  67 => 11,  64 => 10,  62 => 9,  59 => 8,  56 => 7,  52 => 6,  49 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusAdmin/Macro/breadcrumb.html.twig' as breadcrumb %}

{% set breadcrumbs = configuration.vars.breadcrumbs|default([]) %}
{% set processed_bredcrumbs = [] %}

{% for breadcrumb_config in breadcrumbs %}
    {% set current_breadcrumb = { label: breadcrumb_config.label|trans } %}

    {% if breadcrumb_config.route is defined %}
        {% set should_fallback = false %}

        {% if breadcrumb_config.required_params is defined %}
            {% for required in breadcrumb_config.required_params %}
                {% if breadcrumb_config.params[required] is not defined or breadcrumb_config.params[required] is empty %}
                    {% set should_fallback = true %}
                {% endif %}
            {% endfor %}
        {% endif %}

        {% if should_fallback  %}
            {% set current_breadcrumb = current_breadcrumb|merge({ url: path(breadcrumb_config.fallback_route, breadcrumb_config.params|default([])) }) %}
        {% else %}
            {% set current_breadcrumb = current_breadcrumb|merge({ url: path(breadcrumb_config.route, breadcrumb_config.params|default([])) }) %}
        {% endif %}
    {% endif %}

    {% set processed_bredcrumbs = processed_bredcrumbs|merge([current_breadcrumb]) %}
{% endfor %}

{{ breadcrumb.crumble(processed_bredcrumbs) }}
", "OmniSyliusBannerPlugin:Admin:_breadcrumbs.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/Admin/_breadcrumbs.html.twig");
    }
}
