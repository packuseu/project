<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product:index.html.twig */
class __TwigTemplate_e32f5e182cfbd149ec8253db3e02378d8355c285611d78f1c90765dc98e55a22 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'lead_catcher' => [$this, 'block_lead_catcher'],
            'head_extra' => [$this, 'block_head_extra'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product:index.html.twig"));

        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "SyliusShopBundle:Product:index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        $context["taxon"] = $this->extensions['App\Twig\AppExtension']->getTaxonBySlug(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 4, $this->source); })()), "request", [], "any", false, false, false, 4), "attributes", [], "any", false, false, false, 4), "get", [0 => "slug"], "method", false, false, false, 4));
        // line 5
        $this->loadTemplate("@SyliusShop/Product/Index/_header.html.twig", "SyliusShopBundle:Product:index.html.twig", 5)->display($context);
        // line 6
        echo "
<div class=\"row\">
    <div class=\"col-12 col-md-4 col-lg-3 filter-aside mt-0\">
        ";
        // line 9
        $this->loadTemplate("@SyliusShop/Product/Index/_sidebar.html.twig", "SyliusShopBundle:Product:index.html.twig", 9)->display($context);
        // line 10
        echo "    </div>
    <div class=\"col-12 col-md-8 col-lg-9\">
        ";
        // line 12
        $macros["headers"] = $this->loadTemplate("@SyliusShop/Common/Macro/headers.html.twig", "SyliusShopBundle:Product:index.html.twig", 12)->unwrap();
        // line 13
        echo "
        ";
        // line 14
        echo twig_call_macro($macros["headers"], "macro_default", [twig_get_attribute($this->env, $this->source, (isset($context["taxon"]) || array_key_exists("taxon", $context) ? $context["taxon"] : (function () { throw new RuntimeError('Variable "taxon" does not exist.', 14, $this->source); })()), "name", [], "any", false, false, false, 14), "", twig_get_attribute($this->env, $this->source, (isset($context["taxon"]) || array_key_exists("taxon", $context) ? $context["taxon"] : (function () { throw new RuntimeError('Variable "taxon" does not exist.', 14, $this->source); })()), "description", [], "any", false, false, false, 14)], 14, $context, $this->getSourceContext());
        echo "

        ";
        // line 16
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "body-top-zone");
        echo "<br />

        ";
        // line 18
        $this->loadTemplate("@SyliusShop/Product/Index/_main.html.twig", "SyliusShopBundle:Product:index.html.twig", 18)->display($context);
        // line 19
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 23
    public function block_lead_catcher($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lead_catcher"));

        $this->loadTemplate("LeadCatcher/lead_catcher.html.twig", "SyliusShopBundle:Product:index.html.twig", 23)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 25
    public function block_head_extra($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head_extra"));

        // line 26
        echo "    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 29
    public function block_extra_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        // line 30
        echo "    ";
        $this->loadTemplate("LeadCatcher/_scripts.html.twig", "SyliusShopBundle:Product:index.html.twig", 30)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 30,  131 => 29,  123 => 26,  116 => 25,  103 => 23,  94 => 19,  92 => 18,  87 => 16,  82 => 14,  79 => 13,  77 => 12,  73 => 10,  71 => 9,  66 => 6,  64 => 5,  62 => 4,  55 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% block content %}
{% set taxon = app_get_taxon_by_slug(app.request.attributes.get('slug')) %}
{% include '@SyliusShop/Product/Index/_header.html.twig' %}

<div class=\"row\">
    <div class=\"col-12 col-md-4 col-lg-3 filter-aside mt-0\">
        {% include '@SyliusShop/Product/Index/_sidebar.html.twig' %}
    </div>
    <div class=\"col-12 col-md-8 col-lg-9\">
        {% import '@SyliusShop/Common/Macro/headers.html.twig' as headers %}

        {{ headers.default(taxon.name, '', taxon.description) }}

        {{ omni_sylius_banner_zone('body-top-zone') }}<br />

        {% include '@SyliusShop/Product/Index/_main.html.twig' %}
    </div>
</div>
{% endblock %}

{% block lead_catcher %}{% include 'LeadCatcher/lead_catcher.html.twig' %}{% endblock %}

{% block head_extra %}
    <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css\">
{% endblock %}

{% block extra_javascripts %}
    {% include 'LeadCatcher/_scripts.html.twig' %}
{% endblock %}
", "SyliusShopBundle:Product:index.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Product/index.html.twig");
    }
}
