<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:shippingExportLabel.html.twig */
class __TwigTemplate_92f226308ac66d5a984229512453f09780ccd1acd7b5e774f5fa7512f60cb814 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:shippingExportLabel.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->source, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 1, $this->source); })()), "labelPath", [], "any", false, false, false, 1)) {
            // line 2
            echo "    ";
            $this->loadTemplate("@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_downloadShippingLabel.html.twig", "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:shippingExportLabel.html.twig", 2)->display($context);
        } else {
            // line 4
            echo "    <span class=\"ui label\">
        ";
            // line 5
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("bitbag.ui.no_shipping_label"), "html", null, true);
            echo "
    </span>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:shippingExportLabel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 5,  46 => 4,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if data.labelPath %}
    {% include '@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_downloadShippingLabel.html.twig' %}
{% else %}
    <span class=\"ui label\">
        {{ 'bitbag.ui.no_shipping_label'|trans }}
    </span>
{% endif %}
", "BitBagSyliusShippingExportPlugin:ShippingExport/Grid/Field:shippingExportLabel.html.twig", "/var/www/html/vendor/bitbag/shipping-export-plugin/src/Resources/views/ShippingExport/Grid/Field/shippingExportLabel.html.twig");
    }
}
