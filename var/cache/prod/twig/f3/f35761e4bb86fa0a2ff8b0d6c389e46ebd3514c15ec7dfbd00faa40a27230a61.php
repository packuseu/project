<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle::login.html.twig */
class __TwigTemplate_1e8fd6ff00933018b52538927b10dfb5d2ac8b00cc4f90a67379b0256877c67c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle::login.html.twig"));

        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 3, $this->source); })()), [0 => "@SyliusShop/Form/theme.html.twig"], true);
        // line 1
        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "SyliusShopBundle::login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    ";
        $this->loadTemplate("@SyliusShop/Login/_header.html.twig", "SyliusShopBundle::login.html.twig", 6)->display($context);
        // line 7
        echo "
    ";
        // line 8
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.login.after_content_header"]);
        echo "

    <div class=\"row\">
        <div class=\"col-12 col-md-6 mb-3\">
            ";
        // line 12
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.login.before_login"]);
        echo "

            <div class=\"card\">
                <div class=\"card-body\">
                    <h4>";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.registration.social_logins"), "html", null, true);
        echo "</h4>

                    <div class=\"text-center\">
                        <a href=\"";
        // line 19
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("hwi_oauth_service_redirect", ["service" => "facebook"]);
        echo "\" class=\"btn btn-primary fb-login\">
                            <img class=\"soc-icon\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/fb.png"), "html", null, true);
        echo "\" alt=\"facebook login\">
                            Facebook
                        </a>
                        <a href=\"";
        // line 23
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("hwi_oauth_service_redirect", ["service" => "google"]);
        echo "\" class=\"btn btn-primary google-login\">
                            <img class=\"soc-icon\" src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/g.png"), "html", null, true);
        echo "\" alt=\"google login\">
                            Google
                        </a>
                    </div>
                    <div class=\"text-divider\">
                        <div class=\"text-divider__info\">";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.registration.or"), "html", null, true);
        echo "</div>
                        <div class=\"text-divider__line\"></div>
                    </div>
                    ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 32, $this->source); })()), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_login_check"), "attr" => ["class" => "loadable", "novalidate" => "novalidate"]]);
        echo "
                        ";
        // line 33
        $this->loadTemplate("@SyliusShop/Login/_form.html.twig", "SyliusShopBundle::login.html.twig", 33)->display($context);
        // line 34
        echo "
                        ";
        // line 35
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.login.form", ["form" => (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 35, $this->source); })())]]);
        echo "

                        <button type=\"submit\" class=\"btn btn-primary\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.login"), "html", null, true);
        echo "</button>
                        <a href=\"";
        // line 38
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_request_password_reset_token");
        echo "\" class=\"btn btn-link\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.forgot_password"), "html", null, true);
        echo "</a>
                        <input type=\"hidden\" name=\"_csrf_shop_security_token\" value=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("shop_authenticate"), "html", null, true);
        echo "\">
                    ";
        // line 40
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 40, $this->source); })()), 'form_end', ["render_rest" => false]);
        echo "
                </div>
            </div>

            ";
        // line 44
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.login.after_login"]);
        echo "
        </div>

        <div class=\"col-12 col-md-6 mb-3\">
            ";
        // line 48
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.login.before_register"]);
        echo "

            <div class=\"card\">
                <div class=\"card-body\">
                    ";
        // line 52
        $this->loadTemplate("@SyliusShop/Login/_register.html.twig", "SyliusShopBundle::login.html.twig", 52)->display($context);
        // line 53
        echo "                </div>
            </div>

            ";
        // line 56
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.login.after_register"]);
        echo "
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle::login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  172 => 56,  167 => 53,  165 => 52,  158 => 48,  151 => 44,  144 => 40,  140 => 39,  134 => 38,  130 => 37,  125 => 35,  122 => 34,  120 => 33,  116 => 32,  110 => 29,  102 => 24,  98 => 23,  92 => 20,  88 => 19,  82 => 16,  75 => 12,  68 => 8,  65 => 7,  62 => 6,  55 => 5,  47 => 1,  45 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% form_theme form '@SyliusShop/Form/theme.html.twig' %}

{% block content %}
    {% include '@SyliusShop/Login/_header.html.twig' %}

    {{ sonata_block_render_event('sylius.shop.login.after_content_header') }}

    <div class=\"row\">
        <div class=\"col-12 col-md-6 mb-3\">
            {{ sonata_block_render_event('sylius.shop.login.before_login') }}

            <div class=\"card\">
                <div class=\"card-body\">
                    <h4>{{ 'app.registration.social_logins'|trans }}</h4>

                    <div class=\"text-center\">
                        <a href=\"{{ path('hwi_oauth_service_redirect', {'service': 'facebook' }) }}\" class=\"btn btn-primary fb-login\">
                            <img class=\"soc-icon\" src=\"{{ asset('img/fb.png') }}\" alt=\"facebook login\">
                            Facebook
                        </a>
                        <a href=\"{{ path('hwi_oauth_service_redirect', {'service': 'google' }) }}\" class=\"btn btn-primary google-login\">
                            <img class=\"soc-icon\" src=\"{{ asset('img/g.png') }}\" alt=\"google login\">
                            Google
                        </a>
                    </div>
                    <div class=\"text-divider\">
                        <div class=\"text-divider__info\">{{ 'app.registration.or'|trans }}</div>
                        <div class=\"text-divider__line\"></div>
                    </div>
                    {{ form_start(form, {'action': path('sylius_shop_login_check'), 'attr': {'class': 'loadable', 'novalidate': 'novalidate'}}) }}
                        {% include '@SyliusShop/Login/_form.html.twig' %}

                        {{ sonata_block_render_event('sylius.shop.login.form', {'form': form}) }}

                        <button type=\"submit\" class=\"btn btn-primary\">{{ 'sylius.ui.login'|trans }}</button>
                        <a href=\"{{ path('sylius_shop_request_password_reset_token') }}\" class=\"btn btn-link\">{{ 'sylius.ui.forgot_password'|trans }}</a>
                        <input type=\"hidden\" name=\"_csrf_shop_security_token\" value=\"{{ csrf_token('shop_authenticate') }}\">
                    {{ form_end(form, {'render_rest': false}) }}
                </div>
            </div>

            {{ sonata_block_render_event('sylius.shop.login.after_login') }}
        </div>

        <div class=\"col-12 col-md-6 mb-3\">
            {{ sonata_block_render_event('sylius.shop.login.before_register') }}

            <div class=\"card\">
                <div class=\"card-body\">
                    {% include '@SyliusShop/Login/_register.html.twig' %}
                </div>
            </div>

            {{ sonata_block_render_event('sylius.shop.login.after_register') }}
        </div>
    </div>
{% endblock %}
", "SyliusShopBundle::login.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/login.html.twig");
    }
}
