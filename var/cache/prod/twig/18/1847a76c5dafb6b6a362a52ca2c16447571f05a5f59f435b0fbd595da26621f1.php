<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @LexikTranslationBundle/Translation/_gridToolbar.html.twig */
class __TwigTemplate_7183706f805cc2a20bd0c778364f9fc2c1a94d7370ea180ebc6edd7a8f0157e6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@LexikTranslationBundle/Translation/_gridToolbar.html.twig"));

        // line 1
        echo "<div class=\"page-header\">
    <h1>
        ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.page_title", [], "LexikTranslationBundle"), "html", null, true);
        echo "
        <div class=\"pull-right\">
            <a href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_new");
        echo "\" role=\"button\" class=\"btn btn-success\">
                <span class=\"glyphicon glyphicon-plus\"></span>
                ";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("translations.new_translation", [], "LexikTranslationBundle"), "html", null, true);
        echo "
            </a>
            <a href=\"";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("lexik_translation_overview");
        echo "\" role=\"button\" class=\"btn btn-primary\">
                <span class=\"glyphicon glyphicon-tasks\"></span>
                ";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("overview.page_title", [], "LexikTranslationBundle"), "html", null, true);
        echo "
            </a>
        </div>
    </h1>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@LexikTranslationBundle/Translation/_gridToolbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 11,  59 => 9,  54 => 7,  49 => 5,  44 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"page-header\">
    <h1>
        {{ 'translations.page_title'|trans({}, 'LexikTranslationBundle') }}
        <div class=\"pull-right\">
            <a href=\"{{ path('lexik_translation_new') }}\" role=\"button\" class=\"btn btn-success\">
                <span class=\"glyphicon glyphicon-plus\"></span>
                {{ 'translations.new_translation'|trans({}, 'LexikTranslationBundle') }}
            </a>
            <a href=\"{{ path('lexik_translation_overview') }}\" role=\"button\" class=\"btn btn-primary\">
                <span class=\"glyphicon glyphicon-tasks\"></span>
                {{ 'overview.page_title'|trans({}, 'LexikTranslationBundle') }}
            </a>
        </div>
    </h1>
</div>
", "@LexikTranslationBundle/Translation/_gridToolbar.html.twig", "/var/www/html/vendor/lexik/translation-bundle/Resources/views/Translation/_gridToolbar.html.twig");
    }
}
