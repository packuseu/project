<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Product:show.html.twig */
class __TwigTemplate_d2d0dd3b97238c7db5eed7a6e6b10ed8cc57b087e98f9167e29b8e26dacf106e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'title' => [$this, 'block_title'],
            'metatags' => [$this, 'block_metatags'],
            'lead_catcher' => [$this, 'block_lead_catcher'],
            'head_extra' => [$this, 'block_head_extra'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusShop/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Product:show.html.twig"));

        $this->parent = $this->loadTemplate("@SyliusShop/layout.html.twig", "SyliusShopBundle:Product:show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 4
        $this->loadTemplate("@SyliusShop/Product/Show/_breadcrumb.html.twig", "SyliusShopBundle:Product:show.html.twig", 4)->display($context);
        // line 5
        echo "
";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('metatags', $context, $blocks);
        // line 12
        echo "
<div class=\"row\">
    <div class=\"col-12 col-md-6 mb-5\">
        <div class=\"pr-3\">
            ";
        // line 16
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.before_images", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 16, $this->source); })())]]);
        echo "

            ";
        // line 18
        $this->loadTemplate("@SyliusShop/Product/Show/_images.html.twig", "SyliusShopBundle:Product:show.html.twig", 18)->display($context);
        // line 19
        echo "
            ";
        // line 20
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.after_images", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 20, $this->source); })())]]);
        echo "
        </div>
    </div>
    <div class=\"col-12 col-md-6 mb-5 mt-4\">
        ";
        // line 24
        $this->loadTemplate("@SyliusShop/Product/Show/_header.html.twig", "SyliusShopBundle:Product:show.html.twig", 24)->display($context);
        // line 25
        echo "
        ";
        // line 26
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.after_product_header", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 26, $this->source); })())]]);
        echo "

        <div class=\"d-flex justify-content-between align-items-center mb-2 pb-3\">
            ";
        // line 29
        if ( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 29, $this->source); })()), "variants", [], "any", false, false, false, 29), "empty", [], "method", false, false, false, 29)) {
            // line 30
            echo "                ";
            $this->loadTemplate("@SyliusShop/Product/Show/_price.html.twig", "SyliusShopBundle:Product:show.html.twig", 30)->display($context);
            // line 31
            echo "            ";
        }
        // line 32
        echo "            ";
        $this->loadTemplate("@SyliusShop/Product/Show/_reviews.html.twig", "SyliusShopBundle:Product:show.html.twig", 32)->display($context);
        // line 33
        echo "
            ";
        // line 34
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.after_reviews", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 34, $this->source); })())]]);
        echo "
        </div>
        <div class=\"m-12\">
            <div class=\"custom-control custom-switch\">
                <input type=\"checkbox\" class=\"custom-control-input\" id=\"price-display-switch\">
                <label class=\"custom-control-label\" for=\"price-display-switch\">";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.prices.show_with_tax"), "html", null, true);
        echo "</label>
            </div>
        </div>

        ";
        // line 43
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.after_price", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 43, $this->source); })())]]);
        echo "
        <div class=\"my-2 description-padding\">
            <p class=\"pb-2 product-description\">";
        // line 45
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 45, $this->source); })()), "shortDescription", [], "any", false, false, false, 45), "html", null, true);
        echo "</p>
        </div>

        <div class=\"my-2 description-padding\">
            <p class=\"pb-2 product-description\" id=\"minimal-amount-info\">";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.product.minimal_amount"), "html", null, true);
        echo ": <span></span></p>
        </div>

        ";
        // line 52
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.after_description", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 52, $this->source); })())]]);
        echo "

        ";
        // line 54
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "product-after-description-zone");
        echo "

        ";
        // line 56
        $this->loadTemplate("@SyliusShop/Product/Show/_attributes.html.twig", "SyliusShopBundle:Product:show.html.twig", 56)->display($context);
        // line 57
        echo "
        ";
        // line 58
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.after_attributes", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 58, $this->source); })())]]);
        echo "

        ";
        // line 60
        if (((twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 60, $this->source); })()), "isConfigurable", [], "method", false, false, false, 60) && (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 60, $this->source); })()), "getVariantSelectionMethod", [], "method", false, false, false, 60) == "match")) &&  !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 60, $this->source); })()), "variants", [], "any", false, false, false, 60), "empty", [], "method", false, false, false, 60))) {
            // line 61
            echo "            ";
            $this->loadTemplate("@SyliusShop/Product/Show/_variantsPricing.html.twig", "SyliusShopBundle:Product:show.html.twig", 61)->display(twig_array_merge($context, ["pricing" => call_user_func_array($this->env->getFunction('sylius_product_variant_prices')->getCallable(), [(isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 61, $this->source); })()), twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 61, $this->source); })()), "channel", [], "any", false, false, false, 61)])]));
            // line 62
            echo "        ";
        }
        // line 63
        echo "        ";
        $this->loadTemplate("@SyliusShop/Product/Show/_inventory.html.twig", "SyliusShopBundle:Product:show.html.twig", 63)->display($context);
        // line 64
        echo "
        ";
        // line 65
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.product.show.after_add_to_cart", ["product" => (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 65, $this->source); })())]]);
        echo "
    </div>
</div>

<div class=\"container\">
    ";
        // line 70
        $this->loadTemplate("@SyliusShop/Product/Show/_details.html.twig", "SyliusShopBundle:Product:show.html.twig", 70)->display($context);
        // line 71
        echo "</div>

<div class=\"mb-5 pb-5 sm-container\">
    ";
        // line 74
        $this->loadTemplate("@SyliusShop/Product/Show/_reviewsList.html.twig", "SyliusShopBundle:Product:show.html.twig", 74)->display($context);
        // line 75
        echo "</div>

";
        // line 77
        $this->loadTemplate("@SyliusShop/Product/Show/_associations.html.twig", "SyliusShopBundle:Product:show.html.twig", 77)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 6, $this->source); })()), "name", [], "any", false, false, false, 6), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_metatags($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "metatags"));

        // line 9
        echo "    <meta name=\"title\" content=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 9, $this->source); })()), "name", [], "any", false, false, false, 9), "html", null, true);
        echo "\">
    <meta name=\"description\" content=\"";
        // line 10
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translation", [], "any", false, true, false, 10), "metaDescription", [], "any", true, true, false, 10)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["product"] ?? null), "translation", [], "any", false, true, false, 10), "metaDescription", [], "any", false, false, false, 10), (((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 10, $this->source); })()), "shortDescription", [], "any", false, false, false, 10)) > 50)) ? (twig_slice($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 10, $this->source); })()), "shortDescription", [], "any", false, false, false, 10), 0, 254)) : (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 10, $this->source); })()), "shortDescription", [], "any", false, false, false, 10))))) : ((((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 10, $this->source); })()), "shortDescription", [], "any", false, false, false, 10)) > 50)) ? (twig_slice($this->env, twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 10, $this->source); })()), "shortDescription", [], "any", false, false, false, 10), 0, 254)) : (twig_get_attribute($this->env, $this->source, (isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 10, $this->source); })()), "shortDescription", [], "any", false, false, false, 10))))), "html", null, true);
        echo "\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 80
    public function block_lead_catcher($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lead_catcher"));

        $this->loadTemplate("LeadCatcher/lead_catcher.html.twig", "SyliusShopBundle:Product:show.html.twig", 80)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 82
    public function block_head_extra($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head_extra"));

        // line 83
        echo "    <link rel=\"stylesheet\"  href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css\"></link>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 87
    public function block_extra_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        // line 88
        echo "    <script type=\"text/javascript\">
        let priceData = JSON.parse('";
        // line 89
        echo $this->extensions['App\Twig\AppExtension']->getProductPricingData((isset($context["product"]) || array_key_exists("product", $context) ? $context["product"] : (function () { throw new RuntimeError('Variable "product" does not exist.', 89, $this->source); })()));
        echo "');
        let optionMap = generateOptionMap(priceData);
        let variantJustChanged1 = false;
        let variantJustChanged2 = false;
        let \$addToCart = \$('#addToCartButton');
        let \$cannotAddToCartMessage = \$('#cannotAddToCartMessage');
        let \$quantityBox = \$('#sylius_add_to_cart_cartItem_quantity');
        let \$quoteButton = \$('#quote-button');
        let \$cartResetOptions = \$('#cartResetOptions');

        let selectedVariant = getSelectedVariant(priceData);
        let selectedVariantObject = priceData[selectedVariant];

        // Reset variant options
        \$cartResetOptions.click(function () {
            \$('#sylius-product-adding-to-cart select').each(function () {
                return \$(this).prop('selectedIndex', 0).change();
            });

            \$('#sylius-product-adding-to-cart select').find('option').each(function () {
                \$(this).show();
            });
            \$('.product-price').html('');
            recalculateDisplayedPrice(\$quantityBox, \$('#price-display-switch').is(':checked'));
            \$addToCart.attr('disabled','disabled');
        });

        // We show the tooltip with the tier prices of the current selected variant
        \$('#price-map-' + selectedVariant).show();
        handleAddToCartDisplay(selectedVariant);

        // The form select of the product variants is made dynamically to avoid the
        formProductSelectOfQuoteForm(priceData);

        updateMinimalAmountInfo(selectedVariantObject);
        updateOptionsForVariant(selectedVariantObject);

        // Show prices with / without tax
        \$('#price-display-switch').on('change', function () {
            var checked = \$(this).is(\":checked\");

            \$.each(\$('.price-map'), function () {
                if (checked) {
                    \$(this).attr('title', \$(this).data('full_price_list'));
                } else {
                    \$(this).attr('title', \$(this).data('price_list'));
                }
            });

            recalculateDisplayedPrice(\$quantityBox, checked);
        });

        // When the quote form modal is being called, we must change the product in the selection
        // to be the same as the one that is currently active and update the quantity in the form
        \$('#quote-button').on('click', function () {
            \$('#quote_quantity').val(\$quantityBox.val());

            var selectedVariantCode = getSelectedVariant(priceData);

            if (selectedVariantCode) {
                \$('#quote_product').val(selectedVariantCode);
            }
        });

        // On every quantity change the price that is being displayed must be
        // recalculated to display the discounts that are based on price
        \$quantityBox.on('change', function () {
            recalculateDisplayedPrice(\$(this), \$('#price-display-switch').is(\":checked\"));
            handleAddToCartDisplay(getSelectedVariant(priceData));
        });

        // We submit the form asynchrniously
        \$('#quote-form').submit(function(e) {
            e.preventDefault();

            var form = \$(this);
            var url = form.attr('action');

            \$.ajax({
                type: \"POST\",
                url: url,
                data: form.serialize(),
                beforeSend: function() {
                    \$('#quote-spinner').show();
                },
                success: function(data) {
                    var \$success = \$('#quote-success');
                    \$('#quote-spinner').hide();
                    \$success.show();
                    \$success.fadeOut(3000);
                },
                error: function (data) {
                    var \$error = \$('#quote-error');
                    \$('#quote-spinner').hide();
                    \$error.show();
                    \$error.fadeOut(3000);
                }
            });
        });

        // Reset the quantities in order to reset the price discount information in the price info
        // on variant change. Also the price table info needs to be changed as well
        \$('select').on('change', function () {
            if (\$(this).data('option')) {
                var variantCode = getSelectedVariant(priceData);

                if (variantCode === null) {
                    let selectedOpt = \$(this).attr('data-option');
                    let selectedVal = \$(this).val();
                    if (typeof optionMap[selectedOpt] !== 'undefined'
                        && typeof optionMap[selectedOpt][selectedVal] !== 'undefined'
                    ) {
                        let allowedOptions = optionMap[selectedOpt][selectedVal];
                        \$.each(allowedOptions, function (dataOptionName, allowedValues) {
                            \$('select[data-option=\"' + dataOptionName + '\"] option').each(function() {
                                if (\$.inArray(\$(this).val(), allowedValues) === -1) {
                                    \$(this).hide();
                                } else {
                                    \$(this).show();
                                }
                            });
                        });
                    }
                }

                updateMinimalAmountInfo(priceData[variantCode]);
                updateOptionsForVariant(priceData[variantCode]);
                // nullify the quantities
                \$quantityBox.val(1);
                variantJustChanged1 = true;
                handleAddToCartDisplay(variantCode);

                // first we hide all the current price map tooltips
                \$('.price-map').hide();

                // then we attempt to unhide the tooltip for the current variant if there is one
                \$('#price-map-' + variantCode).show();
            }
        });

        // elaborate way to make sure that the price is recalculated correctly after the
        // change of the selected variant. The 2 variables are needed to protect against
        // subsequent editing of the span.
        \$('.product-price').on('DOMSubtreeModified', function () {
            if (!variantJustChanged1) {
                return;
            }

            if (!variantJustChanged2) {
                variantJustChanged2 = true;

                return;
            }

            variantJustChanged1 = false;
            variantJustChanged2 = false;
            recalculateDisplayedPrice(\$quantityBox, \$('#price-display-switch').is(\":checked\"));
        });

        function updateMinimalAmountInfo(variant) {
            var \$display = \$('#minimal-amount-info');

            if (typeof variant === 'undefined') {
                \$display.hide();
                \$display.find('span').html('');
                return;
            }

            if (variant.min_sellable > 0) {
                \$display.show();
                \$display.find('span').html(variant.min_sellable);
            } else {
                \$display.hide();
                \$display.find('span').html('');
            }
        }

        // Parses variant option information so it can be easily used for hiding options
        // that cannot result in valid variant selections.
        // For a product that has 3 variants with options [A = 1, B = 2], [A = 1, B = 1], [A = 2, B = 3]
        // This function would return a response such as this:
        // [ A = [1 = [B = [1, 2]], 2 = [B = [3]]], B = [1 = [A = [1]], 2 = [A = [1]], 3 = [A = [2]]]]
        function generateOptionMap(priceData) {
            let map = [];

            \$.each(priceData, function (index, variantData) {
                let variantOptionData = {};

                \$.each(variantData.options, function (opIndex, option) {
                    variantOptionData[option.option] = option.value;
                });

                \$.each(variantData.options, function (opIndex, option) {
                    if (!map.hasOwnProperty(option.option)) {
                        map[option.option] = {};
                    }

                    if (!map[option.option].hasOwnProperty(option.value)) {
                        map[option.option][option.value] = {};
                    }

                    \$.each(variantOptionData, function(code, value) {
                        if (code == option.option) {
                            return true;
                        }

                        if (!map[option.option][option.value].hasOwnProperty(code)) {
                            map[option.option][option.value][code] = [];
                        }

                        if (!map[option.option][option.value][code].includes(value)) {
                            map[option.option][option.value][code].push(value);
                        }
                    });
                });
            });

            return map;
        }

        // Hides options that do not have corresponding variants
        // Attatched to their combination
        function updateOptionsForVariant(variant) {
            if (typeof variant === 'undefined') {
                return;
            }

            \$.each(variant.options, function (index, option) {
                \$.each(\$('select'), function () {
                    if (\$(this).data('option') && \$(this).data('option') != option.option) {
                        let selectOption = \$(this).data('option');

                        \$(this).children().each(function () {
                            if (optionMap[option.option][option.value][selectOption].includes(\$(this).val())) {
                                \$(this).show();
                            } else {
                                \$(this).hide();
                            }
                        });
                    }
                });
            });
        }

        function handleAddToCartDisplay(variantCode) {
            if (!variantCode) {
                return;
            }

            var variant = priceData[variantCode];

            if (!variant || variant.sellable == false) {
                \$addToCart.hide();
                \$cannotAddToCartMessage.removeClass('d-none');
                \$quoteButton.removeClass('d-none');

                return;
            }

            var quantity = parseInt(\$quantityBox.val());

            if (variant.max_sellable && quantity >= variant.max_sellable) {
                \$addToCart.hide();
                \$cannotAddToCartMessage.removeClass('d-none');
                \$quoteButton.removeClass('d-none');

                return;
            }

            \$addToCart.show();
            \$cannotAddToCartMessage.addClass('d-none');
            \$quoteButton.addClass('d-none');
        }

        function recalculateDisplayedPrice(\$qDiv, withTax = false) {
            var quantity = parseInt(\$qDiv.val());
            var variantCode = getSelectedVariant(priceData);

            if (variantCode === null) {
                return;
            }

            var variant = priceData[variantCode];
            var lowestBound = 99999999999999;
            var priceChanged = false;
            var newPrice = null;

            if (withTax) {
                newPrice = variant.full_price;
            } else {
                newPrice = variant.price;
            }

            var oldPrice = newPrice;

            \$.each(variant.tier_prices, function (index, tier_price) {
                if (tier_price.quantity < lowestBound) {
                    lowestBound = tier_price.quantity;
                }

                if (tier_price.quantity <= quantity) {
                    priceChanged = true;

                    if (withTax) {
                        newPrice = tier_price.full_price;
                    } else {
                        newPrice = tier_price.price;
                    }
                }
            });

            if (priceChanged) {
                \$('.product-price').html('<del>' + oldPrice + '</del>' + '&nbsp' + newPrice);
            } else {
                if (quantity > 0 && quantity < lowestBound) {
                    \$('.product-price').html(newPrice);
                }
            }
        }

        function formProductSelectOfQuoteForm(data) {
            var selectedOptions = getSelectedOptions();
            var \$quoteProductSelect = \$('#quote_product');

            \$.each(data, function (code, variantData) {
                var selected = isVariantSelected(selectedOptions, variantData.options);
                var optionHtml = '<option value=\"' + code +'\" ';

                if (selected) {
                    optionHtml += 'selected ';
                }

                optionHtml += '>' + variantData.name + '</option>';

                \$quoteProductSelect.append(optionHtml);
            });
        }

        function getSelectedVariant(data) {
            let returnValue = null;
            let selectedOptions = getSelectedOptions();

            \$.each(data, function (code, variant) {
                if (isVariantSelected(selectedOptions, variant.options)) {
                    returnValue = code;
                }
            });

            return returnValue;
        }

        function getSelectedOptions() {
            var selectedOptions = [];

            \$.each(\$('select'), function () {
                if (\$(this).data('option')) {
                    selectedOptions.push(
                        {
                            option: \$(this).data('option'),
                            value: \$(this).children('option:selected').val()
                        }
                    )
                }
            });

            return selectedOptions;
        }

        function isVariantSelected(selectedOptions, options) {
            var optionCount = selectedOptions.length;
            var countOverlaping = 0;

            if (optionCount == 0) {
                return true
            }

            \$.each(selectedOptions, function (index, selectedOption) {
                \$.each(options, function (index, option) {
                    if (option.option == selectedOption.option && option.value == selectedOption.value) {
                        countOverlaping++;
                    }
                });
            });

            return countOverlaping == optionCount;
        }
    </script>

    ";
        // line 477
        $this->loadTemplate("LeadCatcher/_scripts.html.twig", "SyliusShopBundle:Product:show.html.twig", 477)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Product:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  683 => 477,  292 => 89,  289 => 88,  282 => 87,  273 => 83,  266 => 82,  253 => 80,  244 => 10,  239 => 9,  232 => 8,  219 => 6,  212 => 77,  208 => 75,  206 => 74,  201 => 71,  199 => 70,  191 => 65,  188 => 64,  185 => 63,  182 => 62,  179 => 61,  177 => 60,  172 => 58,  169 => 57,  167 => 56,  162 => 54,  157 => 52,  151 => 49,  144 => 45,  139 => 43,  132 => 39,  124 => 34,  121 => 33,  118 => 32,  115 => 31,  112 => 30,  110 => 29,  104 => 26,  101 => 25,  99 => 24,  92 => 20,  89 => 19,  87 => 18,  82 => 16,  76 => 12,  74 => 8,  71 => 7,  69 => 6,  66 => 5,  64 => 4,  57 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusShop/layout.html.twig' %}

{% block content %}
{% include '@SyliusShop/Product/Show/_breadcrumb.html.twig' %}

{% block title %}{{ product.name }}{% endblock %}

{% block metatags %}
    <meta name=\"title\" content=\"{{ product.name }}\">
    <meta name=\"description\" content=\"{{ product.translation.metaDescription|default(product.shortDescription|length > 50 ? product.shortDescription|slice(0, 254) : product.shortDescription) }}\">
{% endblock %}

<div class=\"row\">
    <div class=\"col-12 col-md-6 mb-5\">
        <div class=\"pr-3\">
            {{ sonata_block_render_event('sylius.shop.product.show.before_images', {'product': product}) }}

            {% include '@SyliusShop/Product/Show/_images.html.twig' %}

            {{ sonata_block_render_event('sylius.shop.product.show.after_images', {'product': product}) }}
        </div>
    </div>
    <div class=\"col-12 col-md-6 mb-5 mt-4\">
        {% include '@SyliusShop/Product/Show/_header.html.twig' %}

        {{ sonata_block_render_event('sylius.shop.product.show.after_product_header', {'product': product}) }}

        <div class=\"d-flex justify-content-between align-items-center mb-2 pb-3\">
            {% if not product.variants.empty() %}
                {% include '@SyliusShop/Product/Show/_price.html.twig' %}
            {% endif %}
            {% include '@SyliusShop/Product/Show/_reviews.html.twig' %}

            {{ sonata_block_render_event('sylius.shop.product.show.after_reviews', {'product': product}) }}
        </div>
        <div class=\"m-12\">
            <div class=\"custom-control custom-switch\">
                <input type=\"checkbox\" class=\"custom-control-input\" id=\"price-display-switch\">
                <label class=\"custom-control-label\" for=\"price-display-switch\">{{ 'app.prices.show_with_tax'|trans }}</label>
            </div>
        </div>

        {{ sonata_block_render_event('sylius.shop.product.show.after_price', {'product': product}) }}
        <div class=\"my-2 description-padding\">
            <p class=\"pb-2 product-description\">{{ product.shortDescription }}</p>
        </div>

        <div class=\"my-2 description-padding\">
            <p class=\"pb-2 product-description\" id=\"minimal-amount-info\">{{ 'app.product.minimal_amount'|trans }}: <span></span></p>
        </div>

        {{ sonata_block_render_event('sylius.shop.product.show.after_description', {'product': product}) }}

        {{ omni_sylius_banner_zone('product-after-description-zone') }}

        {% include '@SyliusShop/Product/Show/_attributes.html.twig' %}

        {{ sonata_block_render_event('sylius.shop.product.show.after_attributes', {'product': product}) }}

        {% if product.isConfigurable() and product.getVariantSelectionMethod() == 'match' and not product.variants.empty() %}
            {% include '@SyliusShop/Product/Show/_variantsPricing.html.twig' with {'pricing': sylius_product_variant_prices(product, sylius.channel)} %}
        {% endif %}
        {% include '@SyliusShop/Product/Show/_inventory.html.twig' %}

        {{ sonata_block_render_event('sylius.shop.product.show.after_add_to_cart', {'product': product}) }}
    </div>
</div>

<div class=\"container\">
    {% include '@SyliusShop/Product/Show/_details.html.twig' %}
</div>

<div class=\"mb-5 pb-5 sm-container\">
    {% include '@SyliusShop/Product/Show/_reviewsList.html.twig' %}
</div>

{% include '@SyliusShop/Product/Show/_associations.html.twig' %}
{% endblock %}

{% block lead_catcher %}{% include 'LeadCatcher/lead_catcher.html.twig' %}{% endblock %}

{% block head_extra %}
    <link rel=\"stylesheet\"  href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/11.0.2/css/bootstrap-slider.min.css\"></link>
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
{% endblock %}

{% block extra_javascripts %}
    <script type=\"text/javascript\">
        let priceData = JSON.parse('{{ app_get_product_pricing_data(product)|raw }}');
        let optionMap = generateOptionMap(priceData);
        let variantJustChanged1 = false;
        let variantJustChanged2 = false;
        let \$addToCart = \$('#addToCartButton');
        let \$cannotAddToCartMessage = \$('#cannotAddToCartMessage');
        let \$quantityBox = \$('#sylius_add_to_cart_cartItem_quantity');
        let \$quoteButton = \$('#quote-button');
        let \$cartResetOptions = \$('#cartResetOptions');

        let selectedVariant = getSelectedVariant(priceData);
        let selectedVariantObject = priceData[selectedVariant];

        // Reset variant options
        \$cartResetOptions.click(function () {
            \$('#sylius-product-adding-to-cart select').each(function () {
                return \$(this).prop('selectedIndex', 0).change();
            });

            \$('#sylius-product-adding-to-cart select').find('option').each(function () {
                \$(this).show();
            });
            \$('.product-price').html('');
            recalculateDisplayedPrice(\$quantityBox, \$('#price-display-switch').is(':checked'));
            \$addToCart.attr('disabled','disabled');
        });

        // We show the tooltip with the tier prices of the current selected variant
        \$('#price-map-' + selectedVariant).show();
        handleAddToCartDisplay(selectedVariant);

        // The form select of the product variants is made dynamically to avoid the
        formProductSelectOfQuoteForm(priceData);

        updateMinimalAmountInfo(selectedVariantObject);
        updateOptionsForVariant(selectedVariantObject);

        // Show prices with / without tax
        \$('#price-display-switch').on('change', function () {
            var checked = \$(this).is(\":checked\");

            \$.each(\$('.price-map'), function () {
                if (checked) {
                    \$(this).attr('title', \$(this).data('full_price_list'));
                } else {
                    \$(this).attr('title', \$(this).data('price_list'));
                }
            });

            recalculateDisplayedPrice(\$quantityBox, checked);
        });

        // When the quote form modal is being called, we must change the product in the selection
        // to be the same as the one that is currently active and update the quantity in the form
        \$('#quote-button').on('click', function () {
            \$('#quote_quantity').val(\$quantityBox.val());

            var selectedVariantCode = getSelectedVariant(priceData);

            if (selectedVariantCode) {
                \$('#quote_product').val(selectedVariantCode);
            }
        });

        // On every quantity change the price that is being displayed must be
        // recalculated to display the discounts that are based on price
        \$quantityBox.on('change', function () {
            recalculateDisplayedPrice(\$(this), \$('#price-display-switch').is(\":checked\"));
            handleAddToCartDisplay(getSelectedVariant(priceData));
        });

        // We submit the form asynchrniously
        \$('#quote-form').submit(function(e) {
            e.preventDefault();

            var form = \$(this);
            var url = form.attr('action');

            \$.ajax({
                type: \"POST\",
                url: url,
                data: form.serialize(),
                beforeSend: function() {
                    \$('#quote-spinner').show();
                },
                success: function(data) {
                    var \$success = \$('#quote-success');
                    \$('#quote-spinner').hide();
                    \$success.show();
                    \$success.fadeOut(3000);
                },
                error: function (data) {
                    var \$error = \$('#quote-error');
                    \$('#quote-spinner').hide();
                    \$error.show();
                    \$error.fadeOut(3000);
                }
            });
        });

        // Reset the quantities in order to reset the price discount information in the price info
        // on variant change. Also the price table info needs to be changed as well
        \$('select').on('change', function () {
            if (\$(this).data('option')) {
                var variantCode = getSelectedVariant(priceData);

                if (variantCode === null) {
                    let selectedOpt = \$(this).attr('data-option');
                    let selectedVal = \$(this).val();
                    if (typeof optionMap[selectedOpt] !== 'undefined'
                        && typeof optionMap[selectedOpt][selectedVal] !== 'undefined'
                    ) {
                        let allowedOptions = optionMap[selectedOpt][selectedVal];
                        \$.each(allowedOptions, function (dataOptionName, allowedValues) {
                            \$('select[data-option=\"' + dataOptionName + '\"] option').each(function() {
                                if (\$.inArray(\$(this).val(), allowedValues) === -1) {
                                    \$(this).hide();
                                } else {
                                    \$(this).show();
                                }
                            });
                        });
                    }
                }

                updateMinimalAmountInfo(priceData[variantCode]);
                updateOptionsForVariant(priceData[variantCode]);
                // nullify the quantities
                \$quantityBox.val(1);
                variantJustChanged1 = true;
                handleAddToCartDisplay(variantCode);

                // first we hide all the current price map tooltips
                \$('.price-map').hide();

                // then we attempt to unhide the tooltip for the current variant if there is one
                \$('#price-map-' + variantCode).show();
            }
        });

        // elaborate way to make sure that the price is recalculated correctly after the
        // change of the selected variant. The 2 variables are needed to protect against
        // subsequent editing of the span.
        \$('.product-price').on('DOMSubtreeModified', function () {
            if (!variantJustChanged1) {
                return;
            }

            if (!variantJustChanged2) {
                variantJustChanged2 = true;

                return;
            }

            variantJustChanged1 = false;
            variantJustChanged2 = false;
            recalculateDisplayedPrice(\$quantityBox, \$('#price-display-switch').is(\":checked\"));
        });

        function updateMinimalAmountInfo(variant) {
            var \$display = \$('#minimal-amount-info');

            if (typeof variant === 'undefined') {
                \$display.hide();
                \$display.find('span').html('');
                return;
            }

            if (variant.min_sellable > 0) {
                \$display.show();
                \$display.find('span').html(variant.min_sellable);
            } else {
                \$display.hide();
                \$display.find('span').html('');
            }
        }

        // Parses variant option information so it can be easily used for hiding options
        // that cannot result in valid variant selections.
        // For a product that has 3 variants with options [A = 1, B = 2], [A = 1, B = 1], [A = 2, B = 3]
        // This function would return a response such as this:
        // [ A = [1 = [B = [1, 2]], 2 = [B = [3]]], B = [1 = [A = [1]], 2 = [A = [1]], 3 = [A = [2]]]]
        function generateOptionMap(priceData) {
            let map = [];

            \$.each(priceData, function (index, variantData) {
                let variantOptionData = {};

                \$.each(variantData.options, function (opIndex, option) {
                    variantOptionData[option.option] = option.value;
                });

                \$.each(variantData.options, function (opIndex, option) {
                    if (!map.hasOwnProperty(option.option)) {
                        map[option.option] = {};
                    }

                    if (!map[option.option].hasOwnProperty(option.value)) {
                        map[option.option][option.value] = {};
                    }

                    \$.each(variantOptionData, function(code, value) {
                        if (code == option.option) {
                            return true;
                        }

                        if (!map[option.option][option.value].hasOwnProperty(code)) {
                            map[option.option][option.value][code] = [];
                        }

                        if (!map[option.option][option.value][code].includes(value)) {
                            map[option.option][option.value][code].push(value);
                        }
                    });
                });
            });

            return map;
        }

        // Hides options that do not have corresponding variants
        // Attatched to their combination
        function updateOptionsForVariant(variant) {
            if (typeof variant === 'undefined') {
                return;
            }

            \$.each(variant.options, function (index, option) {
                \$.each(\$('select'), function () {
                    if (\$(this).data('option') && \$(this).data('option') != option.option) {
                        let selectOption = \$(this).data('option');

                        \$(this).children().each(function () {
                            if (optionMap[option.option][option.value][selectOption].includes(\$(this).val())) {
                                \$(this).show();
                            } else {
                                \$(this).hide();
                            }
                        });
                    }
                });
            });
        }

        function handleAddToCartDisplay(variantCode) {
            if (!variantCode) {
                return;
            }

            var variant = priceData[variantCode];

            if (!variant || variant.sellable == false) {
                \$addToCart.hide();
                \$cannotAddToCartMessage.removeClass('d-none');
                \$quoteButton.removeClass('d-none');

                return;
            }

            var quantity = parseInt(\$quantityBox.val());

            if (variant.max_sellable && quantity >= variant.max_sellable) {
                \$addToCart.hide();
                \$cannotAddToCartMessage.removeClass('d-none');
                \$quoteButton.removeClass('d-none');

                return;
            }

            \$addToCart.show();
            \$cannotAddToCartMessage.addClass('d-none');
            \$quoteButton.addClass('d-none');
        }

        function recalculateDisplayedPrice(\$qDiv, withTax = false) {
            var quantity = parseInt(\$qDiv.val());
            var variantCode = getSelectedVariant(priceData);

            if (variantCode === null) {
                return;
            }

            var variant = priceData[variantCode];
            var lowestBound = 99999999999999;
            var priceChanged = false;
            var newPrice = null;

            if (withTax) {
                newPrice = variant.full_price;
            } else {
                newPrice = variant.price;
            }

            var oldPrice = newPrice;

            \$.each(variant.tier_prices, function (index, tier_price) {
                if (tier_price.quantity < lowestBound) {
                    lowestBound = tier_price.quantity;
                }

                if (tier_price.quantity <= quantity) {
                    priceChanged = true;

                    if (withTax) {
                        newPrice = tier_price.full_price;
                    } else {
                        newPrice = tier_price.price;
                    }
                }
            });

            if (priceChanged) {
                \$('.product-price').html('<del>' + oldPrice + '</del>' + '&nbsp' + newPrice);
            } else {
                if (quantity > 0 && quantity < lowestBound) {
                    \$('.product-price').html(newPrice);
                }
            }
        }

        function formProductSelectOfQuoteForm(data) {
            var selectedOptions = getSelectedOptions();
            var \$quoteProductSelect = \$('#quote_product');

            \$.each(data, function (code, variantData) {
                var selected = isVariantSelected(selectedOptions, variantData.options);
                var optionHtml = '<option value=\"' + code +'\" ';

                if (selected) {
                    optionHtml += 'selected ';
                }

                optionHtml += '>' + variantData.name + '</option>';

                \$quoteProductSelect.append(optionHtml);
            });
        }

        function getSelectedVariant(data) {
            let returnValue = null;
            let selectedOptions = getSelectedOptions();

            \$.each(data, function (code, variant) {
                if (isVariantSelected(selectedOptions, variant.options)) {
                    returnValue = code;
                }
            });

            return returnValue;
        }

        function getSelectedOptions() {
            var selectedOptions = [];

            \$.each(\$('select'), function () {
                if (\$(this).data('option')) {
                    selectedOptions.push(
                        {
                            option: \$(this).data('option'),
                            value: \$(this).children('option:selected').val()
                        }
                    )
                }
            });

            return selectedOptions;
        }

        function isVariantSelected(selectedOptions, options) {
            var optionCount = selectedOptions.length;
            var countOverlaping = 0;

            if (optionCount == 0) {
                return true
            }

            \$.each(selectedOptions, function (index, selectedOption) {
                \$.each(options, function (index, option) {
                    if (option.option == selectedOption.option && option.value == selectedOption.value) {
                        countOverlaping++;
                    }
                });
            });

            return countOverlaping == optionCount;
        }
    </script>

    {% include 'LeadCatcher/_scripts.html.twig' %}
{% endblock %}
", "SyliusShopBundle:Product:show.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Product/show.html.twig");
    }
}
