<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusCmsPlugin:Frontend:_breadcrumb.html.twig */
class __TwigTemplate_9ab229a3584f3e75e2992fc1da00405937a333c66ddc236a03d32dda6cecc130 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusCmsPlugin:Frontend:_breadcrumb.html.twig"));

        // line 1
        echo "<div class=\"ui breadcrumb\">
    <a href=\"";
        // line 2
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_homepage");
        echo "\" class=\"section\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.home"), "html", null, true);
        echo "</a>
    ";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getBreadcrumbs());
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 4
            echo "        <div class=\"divider\"> / </div>
        ";
            // line 5
            if (twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "current", [], "any", false, false, false, 5)) {
                // line 6
                echo "            <div class=\"active section\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "node", [], "any", false, false, false, 6), "title", [], "any", false, false, false, 6), "html", null, true);
                echo "</div>
        ";
            } else {
                // line 8
                echo "            ";
                $context["url"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeUrl(twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "node", [], "any", false, false, false, 8));
                // line 9
                echo "            ";
                if (twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 9, $this->source); })()), "url", [], "any", false, false, false, 9)) {
                    // line 10
                    echo "                <a href=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new RuntimeError('Variable "url" does not exist.', 10, $this->source); })()), "url", [], "any", false, false, false, 10), "html", null, true);
                    echo "\" class=\"section\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "node", [], "any", false, false, false, 10), "title", [], "any", false, false, false, 10), "html", null, true);
                    echo "</a>
            ";
                } else {
                    // line 12
                    echo "                <div class=\"section\">";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["breadcrumb"], "node", [], "any", false, false, false, 12), "title", [], "any", false, false, false, 12), "html", null, true);
                    echo "</div>
            ";
                }
                // line 14
                echo "        ";
            }
            // line 15
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusCmsPlugin:Frontend:_breadcrumb.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 16,  87 => 15,  84 => 14,  78 => 12,  70 => 10,  67 => 9,  64 => 8,  58 => 6,  56 => 5,  53 => 4,  49 => 3,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"ui breadcrumb\">
    <a href=\"{{ path('sylius_shop_homepage') }}\" class=\"section\">{{ 'sylius.ui.home'|trans }}</a>
    {% for breadcrumb in omni_sylius_get_breadcrumbs() %}
        <div class=\"divider\"> / </div>
        {% if breadcrumb.current %}
            <div class=\"active section\">{{ breadcrumb.node.title }}</div>
        {% else %}
            {% set url = omni_sylius_get_node_url(breadcrumb.node) %}
            {% if url.url %}
                <a href=\"{{ url.url }}\" class=\"section\">{{ breadcrumb.node.title }}</a>
            {% else %}
                <div class=\"section\">{{ breadcrumb.node.title }}</div>
            {% endif %}
        {% endif %}
    {% endfor %}
</div>
", "OmniSyliusCmsPlugin:Frontend:_breadcrumb.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Frontend/_breadcrumb.html.twig");
    }
}
