<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusCmsPlugin/Node/_form.html.twig */
class __TwigTemplate_a854f4d160ee774ede56f7dee87d4e8cc0d3478ac34ea96cb5ac924236c5e1cb extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusCmsPlugin/Node/_form.html.twig"));

        // line 2
        echo "
";
        // line 3
        $macros["__internal_parse_63"] = $this->macros["__internal_parse_63"] = $this->loadTemplate("@SyliusAdmin/Macro/translationForm.html.twig", "@OmniSyliusCmsPlugin/Node/_form.html.twig", 3)->unwrap();
        // line 4
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), [0 => "@OmniSyliusCmsPlugin/Form/imagesTheme.html.twig"], true);
        // line 5
        echo "
<div class=\"ui segment\">
    ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), 'errors');
        echo "
    <div class=\"two fields\">
        ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "code", [], "any", false, false, false, 9), 'row');
        echo "

        ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "type", [], "any", false, false, false, 11), 'row');
        echo "

        ";
        // line 13
        if ((twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 13, $this->source); })()), "id", [], "any", false, false, false, 13) != null)) {
            // line 14
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "parent", [], "any", false, false, false, 14), 'row');
            echo "

            ";
            // line 16
            if ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeRelated((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 16, $this->source); })()))) {
                // line 17
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 17, $this->source); })()), "relation", [], "any", false, false, false, 17), 'row');
                echo "
            ";
            }
            // line 19
            echo "
            ";
            // line 20
            if ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeScopable((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 20, $this->source); })()))) {
                // line 21
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "channels", [], "any", false, false, false, 21), 'row');
                echo "
            ";
            }
            // line 23
            echo "        ";
        }
        // line 24
        echo "    </div>

    ";
        // line 26
        if ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeImageAware((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 26, $this->source); })()))) {
            // line 27
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), "images", [], "any", false, false, false, 27), 'row');
            echo "
    ";
        }
        // line 29
        echo "
    <div class=\"two fields\">
        ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "enabled", [], "any", false, false, false, 31), 'row');
        echo "
        ";
        // line 32
        if (twig_get_attribute($this->env, $this->source, ($context["form"] ?? null), "slugFromRelation", [], "any", true, true, false, 32)) {
            // line 33
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), "slugFromRelation", [], "any", false, false, false, 33), 'row');
            echo "
        ";
        }
        // line 35
        echo "    </div>
</div>

";
        // line 38
        echo twig_call_macro($macros["__internal_parse_63"], "macro_translationForm", [twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 38, $this->source); })()), "translations", [], "any", false, false, false, 38)], 38, $context, $this->getSourceContext());
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusCmsPlugin/Node/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 38,  124 => 35,  118 => 33,  116 => 32,  112 => 31,  108 => 29,  102 => 27,  100 => 26,  96 => 24,  93 => 23,  87 => 21,  85 => 20,  82 => 19,  76 => 17,  74 => 16,  68 => 14,  66 => 13,  61 => 11,  56 => 9,  51 => 7,  47 => 5,  45 => 4,  43 => 3,  40 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{% trans_default_domain 'OmniSyliusCmsPlugin' %}

{% from '@SyliusAdmin/Macro/translationForm.html.twig' import translationForm %}
{% form_theme form '@OmniSyliusCmsPlugin/Form/imagesTheme.html.twig' %}

<div class=\"ui segment\">
    {{ form_errors(form) }}
    <div class=\"two fields\">
        {{ form_row(form.code) }}

        {{ form_row(form.type) }}

        {% if resource.id != null %}
            {{ form_row(form.parent) }}

            {% if omni_sylius_is_node_related(node) %}
                {{ form_row(form.relation) }}
            {% endif %}

            {% if omni_sylius_is_node_scopable(node) %}
                {{ form_row(form.channels) }}
            {% endif %}
        {% endif %}
    </div>

    {% if omni_sylius_is_node_image_aware(node) %}
        {{ form_row(form.images) }}
    {% endif %}

    <div class=\"two fields\">
        {{ form_row(form.enabled) }}
        {% if form.slugFromRelation is defined %}
            {{ form_row(form.slugFromRelation) }}
        {% endif %}
    </div>
</div>

{{ translationForm(form.translations) }}
", "@OmniSyliusCmsPlugin/Node/_form.html.twig", "/var/www/html/vendor/omni/sylius-cms-plugin/src/Resources/views/Node/_form.html.twig");
    }
}
