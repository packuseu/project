<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusBannerPlugin::_javascripts.html.twig */
class __TwigTemplate_6cac1b9171c7adc295f749f79bdb26106460ea8642660c7bf3042bf30893fc33 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusBannerPlugin::_javascripts.html.twig"));

        // line 1
        if (twig_in_filter("omni_sylius_banner", twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 1, $this->source); })()), "request", [], "any", false, false, false, 1), "get", [0 => "_route"], "method", false, false, false, 1))) {
            // line 2
            echo "    ";
            // line 3
            echo "    <script src=\"https://cdn.ckeditor.com/4.11.1/full/ckeditor.js\"></script>

    <script type=\"text/javascript\">
        CKEDITOR.config.removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,' +
            'PasteFromWord,Redo,Undo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,' +
            'Button,HiddenField,ImageButton,CopyFormatting,Outdent,Indent,Blockquote,CreateDiv,JustifyLeft,' +
            'JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Link,Unlink,Anchor,Image,Flash,Table,' +
            'HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Maximize,ShowBlocks,About';

        \$('#omni_sylius_banner_position').on('change', function () {
            var \$positionImagesContainer = \$('.position-type-images');

            \$.get({
                'url': \$positionImagesContainer.data('position-types-url').replace('__id__', this.value),
                'success': function (result) {
                    \$positionImagesContainer.find('.active').addClass('hidden').removeClass('active');
                    \$('#position-' + result.type).removeClass('hidden').addClass('active');
                }
            });
        });

        \$('.bannerForm a.ui.labeled.icon.button[data-form-collection=\"add\"]').click('click', function() {
            setTimeout(function(){
                var newEditors = document.querySelectorAll('.ckeditor');
                for (var i = 0; i < newEditors.length; i++) {
                    if (!CKEDITOR.instances[newEditors[i].id]) {
                        CKEDITOR.replace(newEditors[i]);
                    }
                }
            }, 500);
        });
    </script>
    ";
            // line 35
            $this->loadTemplate("SyliusUiBundle::_javascripts.html.twig", "OmniSyliusBannerPlugin::_javascripts.html.twig", 35)->display(twig_array_merge($context, ["path" => "bundles/omnisyliusbannerplugin/assets/admin/js/jquery-ui-sortable.min.js"]));
            // line 36
            echo "    ";
            $this->loadTemplate("SyliusUiBundle::_javascripts.html.twig", "OmniSyliusBannerPlugin::_javascripts.html.twig", 36)->display(twig_array_merge($context, ["path" => "bundles/omnisyliusbannerplugin/assets/admin/js/sortable.js"]));
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusBannerPlugin::_javascripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 36,  78 => 35,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if 'omni_sylius_banner' in app.request.get('_route') %}
    {# This javascript is used to remove unnecessary CKEditor tools from https://cdn.ckeditor.com/4.11.1/full/ckeditor.js #}
    <script src=\"https://cdn.ckeditor.com/4.11.1/full/ckeditor.js\"></script>

    <script type=\"text/javascript\">
        CKEDITOR.config.removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,' +
            'PasteFromWord,Redo,Undo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,' +
            'Button,HiddenField,ImageButton,CopyFormatting,Outdent,Indent,Blockquote,CreateDiv,JustifyLeft,' +
            'JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,Link,Unlink,Anchor,Image,Flash,Table,' +
            'HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,Styles,Maximize,ShowBlocks,About';

        \$('#omni_sylius_banner_position').on('change', function () {
            var \$positionImagesContainer = \$('.position-type-images');

            \$.get({
                'url': \$positionImagesContainer.data('position-types-url').replace('__id__', this.value),
                'success': function (result) {
                    \$positionImagesContainer.find('.active').addClass('hidden').removeClass('active');
                    \$('#position-' + result.type).removeClass('hidden').addClass('active');
                }
            });
        });

        \$('.bannerForm a.ui.labeled.icon.button[data-form-collection=\"add\"]').click('click', function() {
            setTimeout(function(){
                var newEditors = document.querySelectorAll('.ckeditor');
                for (var i = 0; i < newEditors.length; i++) {
                    if (!CKEDITOR.instances[newEditors[i].id]) {
                        CKEDITOR.replace(newEditors[i]);
                    }
                }
            }, 500);
        });
    </script>
    {% include 'SyliusUiBundle::_javascripts.html.twig' with {'path': 'bundles/omnisyliusbannerplugin/assets/admin/js/jquery-ui-sortable.min.js'} %}
    {% include 'SyliusUiBundle::_javascripts.html.twig' with {'path': 'bundles/omnisyliusbannerplugin/assets/admin/js/sortable.js'} %}
{% endif %}
", "OmniSyliusBannerPlugin::_javascripts.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/_javascripts.html.twig");
    }
}
