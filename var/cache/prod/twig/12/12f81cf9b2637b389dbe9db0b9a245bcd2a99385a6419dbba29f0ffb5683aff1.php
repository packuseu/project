<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Checkout/SelectShipping:_choice.html.twig */
class __TwigTemplate_6420bb47b8fe1c3473e7b83108e3d7de7671b45094dc9fecdaee5a8aaf5f3832 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Checkout/SelectShipping:_choice.html.twig"));

        // line 1
        $macros["money"] = $this->macros["money"] = $this->loadTemplate("@SyliusShop/Common/Macro/money.html.twig", "SyliusShopBundle:Checkout/SelectShipping:_choice.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"list-group mb-2\">
    <div class=\"list-group-item flex-column align-items-start\">
        <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">
                ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 7, $this->source); })()), 'widget', ["attr" => ["class" => "input-shipping-method"]]);
        echo "
                ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), 'label');
        echo "
            </h5>
            <small>";
        // line 10
        echo twig_call_macro($macros["money"], "macro_convertAndFormat", [(isset($context["fee"]) || array_key_exists("fee", $context) ? $context["fee"] : (function () { throw new RuntimeError('Variable "fee" does not exist.', 10, $this->source); })())], 10, $context, $this->getSourceContext());
        echo "</small>
        </div>
        ";
        // line 12
        if ( !(null === twig_get_attribute($this->env, $this->source, (isset($context["method"]) || array_key_exists("method", $context) ? $context["method"] : (function () { throw new RuntimeError('Variable "method" does not exist.', 12, $this->source); })()), "description", [], "any", false, false, false, 12))) {
            // line 13
            echo "            <p class=\"mb-1\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["method"]) || array_key_exists("method", $context) ? $context["method"] : (function () { throw new RuntimeError('Variable "method" does not exist.', 13, $this->source); })()), "description", [], "any", false, false, false, 13), "html", null, true);
            echo "</p>
        ";
        }
        // line 15
        echo "    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Checkout/SelectShipping:_choice.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 15,  65 => 13,  63 => 12,  58 => 10,  53 => 8,  49 => 7,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import '@SyliusShop/Common/Macro/money.html.twig' as money %}

<div class=\"list-group mb-2\">
    <div class=\"list-group-item flex-column align-items-start\">
        <div class=\"d-flex w-100 justify-content-between\">
            <h5 class=\"mb-1\">
                {{ form_widget(form, {'attr': {'class': 'input-shipping-method'}}) }}
                {{ form_label(form) }}
            </h5>
            <small>{{ money.convertAndFormat(fee) }}</small>
        </div>
        {% if method.description is not null %}
            <p class=\"mb-1\">{{ method.description }}</p>
        {% endif %}
    </div>
</div>
", "SyliusShopBundle:Checkout/SelectShipping:_choice.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Checkout/SelectShipping/_choice.html.twig");
    }
}
