<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusAdminBundle:Product/Show:_configurableProduct.html.twig */
class __TwigTemplate_279b8452da5910e3346cd0387672a9cdec5f91112144b61927c5b0141869815c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig"));

        // line 1
        $this->loadTemplate("@SyliusAdmin/Product/Show/_header.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 1)->display($context);
        // line 2
        echo "
<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        ";
        // line 5
        $this->loadTemplate("@SyliusAdmin/Product/Show/_taxonomy.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 5)->display($context);
        // line 6
        echo "    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        ";
        // line 8
        $this->loadTemplate("@SyliusAdmin/Product/Show/_options.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 8)->display($context);
        // line 9
        echo "    </div>
</div>
<div class=\"ui hidden divider\"></div>
";
        // line 12
        $this->loadTemplate("@SyliusAdmin/Product/Show/_media.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 12)->display($context);
        // line 13
        echo "<div class=\"ui hidden divider\"></div>
";
        // line 14
        $this->loadTemplate("@SyliusAdmin/Product/Show/_moreDetails.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 14)->display($context);
        // line 15
        echo "<div class=\"ui hidden divider\"></div>
<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        ";
        // line 18
        $this->loadTemplate("@SyliusAdmin/Product/Show/_attributes.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 18)->display($context);
        // line 19
        echo "    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        ";
        // line 21
        $this->loadTemplate("@SyliusAdmin/Product/Show/_associations.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 21)->display($context);
        // line 22
        echo "    </div>
</div>
<div class=\"ui hidden divider\"></div>
";
        // line 25
        $this->loadTemplate("@SyliusAdmin/Product/Show/_variants.html.twig", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", 25)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 25,  80 => 22,  78 => 21,  74 => 19,  72 => 18,  67 => 15,  65 => 14,  62 => 13,  60 => 12,  55 => 9,  53 => 8,  49 => 6,  47 => 5,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include '@SyliusAdmin/Product/Show/_header.html.twig' %}

<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_taxonomy.html.twig' %}
    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_options.html.twig' %}
    </div>
</div>
<div class=\"ui hidden divider\"></div>
{% include '@SyliusAdmin/Product/Show/_media.html.twig' %}
<div class=\"ui hidden divider\"></div>
{% include '@SyliusAdmin/Product/Show/_moreDetails.html.twig' %}
<div class=\"ui hidden divider\"></div>
<div class=\"ui grid\">
    <div class=\"sixteen wide mobile ten wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_attributes.html.twig' %}
    </div>
    <div class=\"sixteen wide mobile six wide computer column\">
        {% include '@SyliusAdmin/Product/Show/_associations.html.twig' %}
    </div>
</div>
<div class=\"ui hidden divider\"></div>
{% include '@SyliusAdmin/Product/Show/_variants.html.twig' %}
", "SyliusAdminBundle:Product/Show:_configurableProduct.html.twig", "/var/www/html/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/views/Product/Show/_configurableProduct.html.twig");
    }
}
