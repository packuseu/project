<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Banner/PositionType/_hero_image.html.twig */
class __TwigTemplate_dda92db0374f13bf38e42a62dbfe65c02e24a0c26c9d0b972b289e6e9903d751 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Banner/PositionType/_hero_image.html.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["banners"] ?? null), 0, [], "array", false, true, false, 1), "images", [], "any", false, true, false, 1), 0, [], "array", true, true, false, 1)) {
            // line 2
            echo "    ";
            $context["imagePath"] = $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 2, $this->source); })()), 0, [], "array", false, false, false, 2), "images", [], "any", false, false, false, 2), 0, [], "array", false, false, false, 2), "path", [], "any", false, false, false, 2), "omni_sylius_banner");
            // line 3
            echo "
    <section
        class=\"hero-image\"
        style=\"background-image: url('";
            // line 6
            echo twig_escape_filter($this->env, (isset($context["imagePath"]) || array_key_exists("imagePath", $context) ? $context["imagePath"] : (function () { throw new RuntimeError('Variable "imagePath" does not exist.', 6, $this->source); })()), "html", null, true);
            echo "')\">
        <div class=\"hero-image__content\">
            <div class=\"container\">
                <h1>
                    ";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 10, $this->source); })()), "translation", [], "any", false, false, false, 10), "title", [], "any", false, false, false, 10), "html", null, true);
            echo "
                </h1>
            </div>
            <div class=\"container container--narrow\">
                <p>
                    <i>
                        ";
            // line 16
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 16, $this->source); })()), "translation", [], "any", false, false, false, 16), "description", [], "any", false, false, false, 16), "html", null, true);
            echo "
                    </i>
                </p>
            </div>
        </div>
        <div class=\"hero-image__cover\"></div>
    </section>
";
        }
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Banner/PositionType/_hero_image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 16,  57 => 10,  50 => 6,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% if banners[0].images[0] is defined %}
    {% set imagePath = banners[0].images[0].path|imagine_filter('omni_sylius_banner') %}

    <section
        class=\"hero-image\"
        style=\"background-image: url('{{ imagePath }}')\">
        <div class=\"hero-image__content\">
            <div class=\"container\">
                <h1>
                    {{ position.translation.title }}
                </h1>
            </div>
            <div class=\"container container--narrow\">
                <p>
                    <i>
                        {{ position.translation.description }}
                    </i>
                </p>
            </div>
        </div>
        <div class=\"hero-image__cover\"></div>
    </section>
{% endif %}
", "Banner/PositionType/_hero_image.html.twig", "/var/www/html/templates/Banner/PositionType/_hero_image.html.twig");
    }
}
