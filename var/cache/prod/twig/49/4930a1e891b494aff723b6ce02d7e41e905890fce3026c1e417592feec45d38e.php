<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle::_header.html.twig */
class __TwigTemplate_5ce9da188b9c511ad09db10b8085b3aa1fb425818f2304bf0b0864e855bb4556 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle::_header.html.twig"));

        // line 1
        echo "<div class=\"row align-items-center no-gutters\">
        ";
        // line 2
        $context["logo"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 2, $this->source); })()), "channel", [], "any", false, false, false, 2), "logos", [], "any", false, false, false, 2), "first", [], "any", false, false, false, 2);
        // line 3
        echo "        <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_homepage");
        echo "\">
            <img class=\"logo\"
                 src=\"";
        // line 5
        if ((isset($context["logo"]) || array_key_exists("logo", $context) ? $context["logo"] : (function () { throw new RuntimeError('Variable "logo" does not exist.', 5, $this->source); })())) {
            // line 6
            echo "                            ";
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, (isset($context["logo"]) || array_key_exists("logo", $context) ? $context["logo"] : (function () { throw new RuntimeError('Variable "logo" does not exist.', 6, $this->source); })()), "path", [], "any", false, false, false, 6), "sylius_admin_product_original"), "html", null, true);
            echo "
                        ";
        } else {
            // line 8
            echo "                            ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo.svg"), "html", null, true);
            echo "
                      ";
        }
        // line 9
        echo "\"
                 alt=\"";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 10, $this->source); })()), "channel", [], "any", false, false, false, 10), "name", [], "any", false, false, false, 10), "html", null, true);
        echo " logo\"/>
        </a>

        ";
        // line 13
        $this->loadTemplate("_menu_display.html.twig", "SyliusShopBundle::_header.html.twig", 13)->display($context);
        // line 14
        echo "    <div class=\"\">
        ";
        // line 15
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.header"]);
        echo "
    </div>
    <div class=\"ml-auto mobile-header\">
        <div class=\"col-auto d-flex align-items-center\">
            <div class=\"ml-4\">
                <div class=\"header-icons-container\">
                    <div>
                        <a class=\"cart-toggle\" data-toggle=\"dropdown\" href=\"#\" aria-haspopup=\"true\"  id=\"searchDropdown\">
                            <span class=\"header-icon search-toggle\" id=\"searchToggle\"><i class=\"fa fa-search\"></i></span>
                        </a>
                        <div class=\"dropdown-menu dropdown-menu-right\" style=\"width:300px;\" aria-labelledby=\"searchDropdown\">
                            <form action=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_search");
        echo "\" class=\"input-group\" method=\"get\" style=\"padding: 1.25rem; margin: auto\">
                                <div class=\"ui inverted transparent icon input input-group\">
                                    <div class=\"input-group-prepend\">
                                        <button class=\"input-group-text\"><i class=\"fa fa-search\"></i></button>
                                    </div>
                                    <input type=\"text\" id=\"search\" class=\"form-control\" placeholder=";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.search"), "html", null, true);
        echo " name=\"q\" value=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("searchTerm", $context)) ? (_twig_default_filter((isset($context["searchTerm"]) || array_key_exists("searchTerm", $context) ? $context["searchTerm"] : (function () { throw new RuntimeError('Variable "searchTerm" does not exist.', 31, $this->source); })()), "")) : ("")), "html", null, true);
        echo "\">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div>
                        <a class=\"cart-toggle\" data-toggle=\"dropdown\" href=\"#\" aria-haspopup=\"true\"  id=\"userDropdown\">
                            <span class=\"header-icon\"><i class=\"fa fa-user-circle\"></i></span>
                        </a>
                        <div class=\"dropdown-menu dropdown-menu-right\" style=\"width:300px;\" aria-labelledby=\"userDropdown\">
                            ";
        // line 41
        $this->loadTemplate("@SyliusShop/Menu/_security.html.twig", "SyliusShopBundle::_header.html.twig", 41)->display($context);
        // line 42
        echo "                        </div>
                    </div>
                </div>
                ";
        // line 45
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sylius_shop_partial_cart_summary", ["template" => "@SyliusShop/Cart/_widget.html.twig"]));
        echo "
            </div>
            <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("quote", ["locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 47, $this->source); })()), "request", [], "any", false, false, false, 47), "locale", [], "any", false, false, false, 47)]), "html", null, true);
        echo "\" class=\"primary-button detatched-left\" id=\"getQuoteHeader\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.get_a_quote"), "html", null, true);
        echo "</a>
        </div>
        <div class=\"col-auto d-lg-none ml-2\">
            <div class=\"navbar-light\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#mainNavbar\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle::_header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 47,  123 => 45,  118 => 42,  116 => 41,  101 => 31,  93 => 26,  79 => 15,  76 => 14,  74 => 13,  68 => 10,  65 => 9,  59 => 8,  53 => 6,  51 => 5,  45 => 3,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"row align-items-center no-gutters\">
        {% set logo = sylius.channel.logos.first %}
        <a href=\"{{ path('sylius_shop_homepage') }}\">
            <img class=\"logo\"
                 src=\"{% if logo %}
                            {{ logo.path|imagine_filter('sylius_admin_product_original') }}
                        {% else %}
                            {{ asset('img/logo.svg') }}
                      {% endif %}\"
                 alt=\"{{sylius.channel.name}} logo\"/>
        </a>

        {% include '_menu_display.html.twig' %}
    <div class=\"\">
        {{ sonata_block_render_event('sylius.shop.layout.header') }}
    </div>
    <div class=\"ml-auto mobile-header\">
        <div class=\"col-auto d-flex align-items-center\">
            <div class=\"ml-4\">
                <div class=\"header-icons-container\">
                    <div>
                        <a class=\"cart-toggle\" data-toggle=\"dropdown\" href=\"#\" aria-haspopup=\"true\"  id=\"searchDropdown\">
                            <span class=\"header-icon search-toggle\" id=\"searchToggle\"><i class=\"fa fa-search\"></i></span>
                        </a>
                        <div class=\"dropdown-menu dropdown-menu-right\" style=\"width:300px;\" aria-labelledby=\"searchDropdown\">
                            <form action=\"{{ path('omni_sylius_search') }}\" class=\"input-group\" method=\"get\" style=\"padding: 1.25rem; margin: auto\">
                                <div class=\"ui inverted transparent icon input input-group\">
                                    <div class=\"input-group-prepend\">
                                        <button class=\"input-group-text\"><i class=\"fa fa-search\"></i></button>
                                    </div>
                                    <input type=\"text\" id=\"search\" class=\"form-control\" placeholder={{ 'sylius.ui.search'|trans }} name=\"q\" value=\"{{ searchTerm|default('') }}\">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div>
                        <a class=\"cart-toggle\" data-toggle=\"dropdown\" href=\"#\" aria-haspopup=\"true\"  id=\"userDropdown\">
                            <span class=\"header-icon\"><i class=\"fa fa-user-circle\"></i></span>
                        </a>
                        <div class=\"dropdown-menu dropdown-menu-right\" style=\"width:300px;\" aria-labelledby=\"userDropdown\">
                            {% include '@SyliusShop/Menu/_security.html.twig' %}
                        </div>
                    </div>
                </div>
                {{ render(url('sylius_shop_partial_cart_summary', {'template': '@SyliusShop/Cart/_widget.html.twig'})) }}
            </div>
            <a href=\"{{ path('quote', {locale: app.request.locale}) }}\" class=\"primary-button detatched-left\" id=\"getQuoteHeader\">{{ 'app.ui.get_a_quote'|trans }}</a>
        </div>
        <div class=\"col-auto d-lg-none ml-2\">
            <div class=\"navbar-light\">
                <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#mainNavbar\">
                    <span class=\"navbar-toggler-icon\"></span>
                </button>
            </div>
        </div>
    </div>
</div>
", "SyliusShopBundle::_header.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/_header.html.twig");
    }
}
