<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusShippingPlugin:Admin:_javascripts.html.twig */
class __TwigTemplate_6b99cd38b16a7418ac5f1359e64f3c465a2d33f33e6ea4830019cb9e197c6cea extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusShippingPlugin:Admin:_javascripts.html.twig"));

        // line 1
        $this->loadTemplate("SyliusUiBundle::_javascripts.html.twig", "OmniSyliusShippingPlugin:Admin:_javascripts.html.twig", 1)->display(twig_array_merge($context, ["path" => "bundles/omnisyliusshippingplugin/js/admin/shipperConfigsCollection.js"]));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusShippingPlugin:Admin:_javascripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include 'SyliusUiBundle::_javascripts.html.twig' with {'path': 'bundles/omnisyliusshippingplugin/js/admin/shipperConfigsCollection.js'} %}
", "OmniSyliusShippingPlugin:Admin:_javascripts.html.twig", "/var/www/html/vendor/omni/sylius-shipping-plugin/src/Resources/views/Admin/_javascripts.html.twig");
    }
}
