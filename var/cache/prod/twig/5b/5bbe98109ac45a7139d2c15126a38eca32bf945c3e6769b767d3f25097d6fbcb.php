<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* OmniSyliusBannerPlugin:PositionTypes:_unified_descriptive_grid.html.twig */
class __TwigTemplate_fa7baa5192acb8088fe3fe377ffc52479099f1bdaff27d51e7718cc3913ad460 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "OmniSyliusBannerPlugin:PositionTypes:_unified_descriptive_grid.html.twig"));

        // line 2
        echo "
<div style=\"background-color: ";
        // line 3
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "background_color", [], "any", true, true, false, 3)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, ($context["options"] ?? null), "background_color", [], "any", false, false, false, 3), "#EE2E4F")) : ("#EE2E4F")), "html", null, true);
        echo ";\">
    <div class=\"container\">
        <div class=\"banners-grid\">
            ";
        // line 6
        if (( !(null === twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 6, $this->source); })()), "title", [], "any", false, false, false, 6)) && twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 6, $this->source); })()), "descriptive", [], "any", false, false, false, 6))) {
            // line 7
            echo "                <h4 class=\"heading heading--banners-grid\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 7, $this->source); })()), "title", [], "any", false, false, false, 7), "html", null, true);
            echo "</h4>
            ";
        }
        // line 9
        echo "
            <div class=\"row\">
                ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) || array_key_exists("banners", $context) ? $context["banners"] : (function () { throw new RuntimeError('Variable "banners" does not exist.', 11, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "hasChannelCode", [0 => twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 11, $this->source); })()), "channel", [], "array", false, false, false, 11)], "method", false, false, false, 11)) {
                // line 12
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["banner"], "images", [], "any", false, false, false, 12));
                foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                    // line 13
                    echo "                        <div class=\"col-12 col-md-6 col-xl-4\">
                            <div class=\"card\">
                                <a class=\"card-link\" href=\"";
                    // line 15
                    echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 15), "link", [], "any", true, true, false, 15)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, true, false, 15), "link", [], "any", false, false, false, 15), "javascript:;")) : ("javascript:;")), "html", null, true);
                    echo "\">
                                    ";
                    // line 17
                    echo "                                    <div class=\"card-bg-img\"
                                         style=\"background-image: url(";
                    // line 18
                    echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, $context["image"], "path", [], "any", false, false, false, 18), "omni_sylius_banner"), "html", null, true);
                    echo ");\"
                                    ></div>

                                    <div class=\"card-body\" style=\"height: ";
                    // line 21
                    echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentSpace", [], "any", false, false, false, 21)), "html", null, true);
                    echo "%;
                                                                  background-color: ";
                    // line 22
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["image"], "contentBackground", [], "any", false, false, false, 22), "html", null, true);
                    echo "\"
                                    >
                                        <span class=\"card-title\">";
                    // line 24
                    echo twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["image"], "translation", [], "any", false, false, false, 24), "content", [], "any", false, false, false, 24);
                    echo "</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 30
                echo "                ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "            </div>

            ";
        // line 33
        if ((twig_get_attribute($this->env, $this->source, (isset($context["options"]) || array_key_exists("options", $context) ? $context["options"] : (function () { throw new RuntimeError('Variable "options" does not exist.', 33, $this->source); })()), "descriptive", [], "any", false, false, false, 33) &&  !(null === twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 33, $this->source); })()), "translation", [], "any", false, false, false, 33), "link", [], "any", false, false, false, 33)))) {
            // line 34
            echo "                <div class=\"text-center\">
                    <a href=\"";
            // line 35
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 35, $this->source); })()), "translation", [], "any", false, false, false, 35), "link", [], "any", false, false, false, 35), "html", null, true);
            echo "\" class=\"btn banners-grid__btn\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["position"]) || array_key_exists("position", $context) ? $context["position"] : (function () { throw new RuntimeError('Variable "position" does not exist.', 35, $this->source); })()), "translation", [], "any", false, false, false, 35), "description", [], "any", false, false, false, 35), "html", null, true);
            echo "</a>
                </div>
            ";
        }
        // line 38
        echo "        </div>
    </div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "OmniSyliusBannerPlugin:PositionTypes:_unified_descriptive_grid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 38,  125 => 35,  122 => 34,  120 => 33,  116 => 31,  109 => 30,  97 => 24,  92 => 22,  88 => 21,  82 => 18,  79 => 17,  75 => 15,  71 => 13,  66 => 12,  61 => 11,  57 => 9,  51 => 7,  49 => 6,  43 => 3,  40 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("{# this demo is made with Bootstrap 4 #}

<div style=\"background-color: {{ options.background_color|default('#EE2E4F') }};\">
    <div class=\"container\">
        <div class=\"banners-grid\">
            {% if position.title is not null and options.descriptive %}
                <h4 class=\"heading heading--banners-grid\">{{ position.title }}</h4>
            {% endif %}

            <div class=\"row\">
                {% for banner in banners if banner.hasChannelCode(options['channel']) %}
                    {% for image in banner.images %}
                        <div class=\"col-12 col-md-6 col-xl-4\">
                            <div class=\"card\">
                                <a class=\"card-link\" href=\"{{ image.translation.link|default('javascript:;') }}\">
                                    {# ideal img size 350x400#}
                                    <div class=\"card-bg-img\"
                                         style=\"background-image: url({{ image.path|imagine_filter('omni_sylius_banner') }});\"
                                    ></div>

                                    <div class=\"card-body\" style=\"height: {{ image.contentSpace|number_format }}%;
                                                                  background-color: {{ image.contentBackground }}\"
                                    >
                                        <span class=\"card-title\">{{ image.translation.content|raw }}</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    {% endfor %}
                {% endfor %}
            </div>

            {% if options.descriptive and position.translation.link is not null %}
                <div class=\"text-center\">
                    <a href=\"{{ position.translation.link }}\" class=\"btn banners-grid__btn\">{{ position.translation.description }}</a>
                </div>
            {% endif %}
        </div>
    </div>
</div>
", "OmniSyliusBannerPlugin:PositionTypes:_unified_descriptive_grid.html.twig", "/var/www/html/vendor/omni/sylius-banner-plugin/src/Resources/views/PositionTypes/_unified_descriptive_grid.html.twig");
    }
}
