<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OmniSyliusImportPlugin/Admin/Crud/show.html.twig */
class __TwigTemplate_0251dafa3a8c6d5d6d1b1218fe718308a6a31ff73e5b7020b17ef590d213799b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@SyliusAdmin/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@OmniSyliusImportPlugin/Admin/Crud/show.html.twig"));

        // line 3
        $context["header"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 3), "header", [], "any", true, true, false, 3)) ? (_twig_default_filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["configuration"] ?? null), "vars", [], "any", false, true, false, 3), "header", [], "any", false, false, false, 3), ((twig_get_attribute($this->env, $this->source, (isset($context["metadata"]) || array_key_exists("metadata", $context) ? $context["metadata"] : (function () { throw new RuntimeError('Variable "metadata" does not exist.', 3, $this->source); })()), "applicationName", [], "any", false, false, false, 3) . ".ui.edit_") . twig_get_attribute($this->env, $this->source, (isset($context["metadata"]) || array_key_exists("metadata", $context) ? $context["metadata"] : (function () { throw new RuntimeError('Variable "metadata" does not exist.', 3, $this->source); })()), "name", [], "any", false, false, false, 3)))) : (((twig_get_attribute($this->env, $this->source, (isset($context["metadata"]) || array_key_exists("metadata", $context) ? $context["metadata"] : (function () { throw new RuntimeError('Variable "metadata" does not exist.', 3, $this->source); })()), "applicationName", [], "any", false, false, false, 3) . ".ui.edit_") . twig_get_attribute($this->env, $this->source, (isset($context["metadata"]) || array_key_exists("metadata", $context) ? $context["metadata"] : (function () { throw new RuntimeError('Variable "metadata" does not exist.', 3, $this->source); })()), "name", [], "any", false, false, false, 3))));
        // line 4
        $context["event_prefix"] = (((twig_get_attribute($this->env, $this->source, (isset($context["metadata"]) || array_key_exists("metadata", $context) ? $context["metadata"] : (function () { throw new RuntimeError('Variable "metadata" does not exist.', 4, $this->source); })()), "applicationName", [], "any", false, false, false, 4) . ".admin.") . twig_get_attribute($this->env, $this->source, (isset($context["metadata"]) || array_key_exists("metadata", $context) ? $context["metadata"] : (function () { throw new RuntimeError('Variable "metadata" does not exist.', 4, $this->source); })()), "name", [], "any", false, false, false, 4)) . ".update");
        // line 1
        $this->parent = $this->loadTemplate("@SyliusAdmin/layout.html.twig", "@OmniSyliusImportPlugin/Admin/Crud/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((isset($context["header"]) || array_key_exists("header", $context) ? $context["header"] : (function () { throw new RuntimeError('Variable "header" does not exist.', 6, $this->source); })())), "html", null, true);
        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 9
        echo "    ";
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 9, $this->source); })()) . ".before_header"), ["resource" => (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 9, $this->source); })())]]);
        echo "

    ";
        // line 11
        $this->loadTemplate("@SyliusAdmin/Crud/Update/_header.html.twig", "@OmniSyliusImportPlugin/Admin/Crud/show.html.twig", 11)->display($context);
        // line 12
        echo "
    ";
        // line 13
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 13, $this->source); })()) . ".after_header"), ["resource" => (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 13, $this->source); })())]]);
        echo "

    <div class=\"ui grid\">
        <div class=\"sixteen wide mobile ten wide computer column\">
            <div id=\"details\">
                <h4 class=\"ui top attached large header\">Details</h4>
                <div class=\"ui attached segment\">
                    <div>
                        ";
        // line 21
        $context["color"] = "teal";
        // line 22
        echo "                        ";
        $context["icon"] = "clock";
        // line 23
        echo "
                        ";
        // line 24
        if ((twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 24, $this->source); })()), "status", [], "any", false, false, false, 24) == "error")) {
            // line 25
            echo "                            ";
            $context["color"] = "red";
            // line 26
            echo "                            ";
            $context["icon"] = "remove";
            // line 27
            echo "                        ";
        } elseif ((twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 27, $this->source); })()), "status", [], "any", false, false, false, 27) == "new")) {
            // line 28
            echo "                            ";
            $context["color"] = "yellow";
            // line 29
            echo "                        ";
        } elseif ((twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 29, $this->source); })()), "status", [], "any", false, false, false, 29) == "done")) {
            // line 30
            echo "                            ";
            $context["color"] = "green";
            // line 31
            echo "                            ";
            $context["icon"] = "checkmark";
            // line 32
            echo "                        ";
        }
        // line 33
        echo "
                        Status:
                        <span class=\"ui ";
        // line 35
        echo twig_escape_filter($this->env, (isset($context["color"]) || array_key_exists("color", $context) ? $context["color"] : (function () { throw new RuntimeError('Variable "color" does not exist.', 35, $this->source); })()), "html", null, true);
        echo " label\" style=\"margin-left: 20px\"><i class=\"";
        echo twig_escape_filter($this->env, (isset($context["icon"]) || array_key_exists("icon", $context) ? $context["icon"] : (function () { throw new RuntimeError('Variable "icon" does not exist.', 35, $this->source); })()), "html", null, true);
        echo " icon\"></i>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 35, $this->source); })()), "status", [], "any", false, false, false, 35), "html", null, true);
        echo "</span>

                        <table class=\"ui very basic celled table\">
                            <tbody>
                            ";
        // line 39
        if (twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "importer", [], "any", true, true, false, 39)) {
            // line 40
            echo "                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.importer.form.importer"), "html", null, true);
            echo "</strong></td>
                                    <td>";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 42, $this->source); })()), "importer", [], "any", false, false, false, 42), "html", null, true);
            echo "</td>
                                </tr>
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">";
            // line 45
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.importer.form.comments"), "html", null, true);
            echo "</strong></td>
                                    <td>";
            // line 46
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 46, $this->source); })()), "comments", [], "any", false, false, false, 46), "html", null, true);
            echo "</td>
                                </tr>
                            ";
        }
        // line 49
        echo "                            ";
        if (twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "exporter", [], "any", true, true, false, 49)) {
            // line 50
            echo "                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">";
            // line 51
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.exporter"), "html", null, true);
            echo "</strong></td>
                                    <td>";
            // line 52
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 52, $this->source); })()), "exporter", [], "any", false, false, false, 52), "html", null, true);
            echo "</td>
                                </tr>
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">";
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.query_string"), "html", null, true);
            echo "</strong></td>
                                    <td><textarea disabled readonly style=\"width: 100%;\">";
            // line 56
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 56, $this->source); })()), "queryString", [], "any", false, false, false, 56), "html", null, true);
            echo "</textarea></td>
                                </tr>
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">";
            // line 59
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.comments"), "html", null, true);
            echo "</strong></td>
                                    <td>";
            // line 60
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 60, $this->source); })()), "comments", [], "any", false, false, false, 60), "html", null, true);
            echo "</td>
                                </tr>
                            ";
        }
        // line 63
        echo "                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"sixteen wide mobile six wide computer column\">
            <div id=\"pricing\">
                <h4 class=\"ui top attached large header\">Time</h4>
                <div class=\"ui attached segment\">
                    <table id=\"pricing\" class=\"ui very basic celled table\">
                        <tbody>
                        ";
        // line 75
        if (twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "importer", [], "any", true, true, false, 75)) {
            // line 76
            echo "                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>";
            // line 78
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.importer.form.created_at"), "html", null, true);
            echo "</strong>
                                </td>
                                <td>";
            // line 80
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 80, $this->source); })()), "createdAt", [], "any", false, false, false, 80), "Y-m-d  H:i:s"), "html", null, true);
            echo "</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>";
            // line 84
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.importer.form.processing_started_at"), "html", null, true);
            echo "</strong>
                                </td>
                                <td>";
            // line 86
            if (twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 86, $this->source); })()), "processingStartedAt", [], "any", false, false, false, 86)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 86, $this->source); })()), "processingStartedAt", [], "any", false, false, false, 86), "Y-m-d  H:i:s"), "html", null, true);
                echo " ";
            } else {
                echo " - ";
            }
            echo "</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>";
            // line 90
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.importer.form.processed_at"), "html", null, true);
            echo "</strong>
                                </td>
                                <td>";
            // line 92
            if (twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 92, $this->source); })()), "processedAt", [], "any", false, false, false, 92)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 92, $this->source); })()), "processedAt", [], "any", false, false, false, 92), "Y-m-d  H:i:s"), "html", null, true);
                echo " ";
            } else {
                echo " - ";
            }
            echo "</td>
                            </tr>
                        ";
        }
        // line 95
        echo "                        ";
        if (twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "exporter", [], "any", true, true, false, 95)) {
            // line 96
            echo "                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>";
            // line 98
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.created_at"), "html", null, true);
            echo "</strong>
                                </td>
                                <td>";
            // line 100
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 100, $this->source); })()), "createdAt", [], "any", false, false, false, 100), "Y-m-d  H:i:s"), "html", null, true);
            echo "</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>";
            // line 104
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.processing_started_at"), "html", null, true);
            echo "</strong>
                                </td>
                                <td>";
            // line 106
            if (twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 106, $this->source); })()), "processingStartedAt", [], "any", false, false, false, 106)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 106, $this->source); })()), "processingStartedAt", [], "any", false, false, false, 106), "Y-m-d  H:i:s"), "html", null, true);
                echo " ";
            } else {
                echo " - ";
            }
            echo "</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>";
            // line 110
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.processed_at"), "html", null, true);
            echo "</strong>
                                </td>
                                <td>";
            // line 112
            if (twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 112, $this->source); })()), "processedAt", [], "any", false, false, false, 112)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 112, $this->source); })()), "processedAt", [], "any", false, false, false, 112), "Y-m-d  H:i:s"), "html", null, true);
                echo " ";
            } else {
                echo " - ";
            }
            echo "</td>
                            </tr>
                        ";
        }
        // line 115
        echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        ";
        // line 120
        if ((twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "exporter", [], "any", true, true, false, 120) && twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 120, $this->source); })()), "exportFile", [], "any", false, false, false, 120))) {
            // line 121
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_export_job_download", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 121, $this->source); })()), "id", [], "any", false, false, false, 121)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.download"), "html", null, true);
            echo "</a>
        ";
        }
        // line 123
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["resource"] ?? null), "exporter", [], "any", true, true, false, 123)) {
            // line 124
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_admin_export_job_delete", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 124, $this->source); })()), "id", [], "any", false, false, false, 124)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("omni_sylius.exporter.form.delete"), "html", null, true);
            echo "</a>
        ";
        }
        // line 126
        echo "    </div>

    ";
        // line 128
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 128, $this->source); })()) . ".after_content"), ["resource" => (isset($context["resource"]) || array_key_exists("resource", $context) ? $context["resource"] : (function () { throw new RuntimeError('Variable "resource" does not exist.', 128, $this->source); })())]]);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 131
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 132
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

    ";
        // line 134
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 134, $this->source); })()) . ".stylesheets")]);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 137
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 138
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "

    ";
        // line 140
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), [((isset($context["event_prefix"]) || array_key_exists("event_prefix", $context) ? $context["event_prefix"] : (function () { throw new RuntimeError('Variable "event_prefix" does not exist.', 140, $this->source); })()) . ".javascripts")]);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@OmniSyliusImportPlugin/Admin/Crud/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  407 => 140,  401 => 138,  394 => 137,  385 => 134,  379 => 132,  372 => 131,  363 => 128,  359 => 126,  351 => 124,  348 => 123,  340 => 121,  338 => 120,  331 => 115,  320 => 112,  315 => 110,  303 => 106,  298 => 104,  291 => 100,  286 => 98,  282 => 96,  279 => 95,  268 => 92,  263 => 90,  251 => 86,  246 => 84,  239 => 80,  234 => 78,  230 => 76,  228 => 75,  214 => 63,  208 => 60,  204 => 59,  198 => 56,  194 => 55,  188 => 52,  184 => 51,  181 => 50,  178 => 49,  172 => 46,  168 => 45,  162 => 42,  158 => 41,  155 => 40,  153 => 39,  142 => 35,  138 => 33,  135 => 32,  132 => 31,  129 => 30,  126 => 29,  123 => 28,  120 => 27,  117 => 26,  114 => 25,  112 => 24,  109 => 23,  106 => 22,  104 => 21,  93 => 13,  90 => 12,  88 => 11,  82 => 9,  75 => 8,  60 => 6,  52 => 1,  50 => 4,  48 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends '@SyliusAdmin/layout.html.twig' %}

{% set header = configuration.vars.header|default(metadata.applicationName~'.ui.edit_'~metadata.name) %}
{% set event_prefix = metadata.applicationName ~ '.admin.' ~ metadata.name ~ '.update' %}

{% block title %}{{ header|trans }} {{ parent() }}{% endblock %}

{% block content %}
    {{ sonata_block_render_event(event_prefix ~ '.before_header', {'resource': resource}) }}

    {% include '@SyliusAdmin/Crud/Update/_header.html.twig' %}

    {{ sonata_block_render_event(event_prefix ~ '.after_header', {'resource': resource}) }}

    <div class=\"ui grid\">
        <div class=\"sixteen wide mobile ten wide computer column\">
            <div id=\"details\">
                <h4 class=\"ui top attached large header\">Details</h4>
                <div class=\"ui attached segment\">
                    <div>
                        {% set color = 'teal' %}
                        {% set icon = 'clock' %}

                        {% if resource.status == 'error' %}
                            {% set color = 'red' %}
                            {% set icon = 'remove' %}
                        {% elseif resource.status == 'new' %}
                            {% set color = 'yellow' %}
                        {% elseif resource.status == 'done' %}
                            {% set color = 'green' %}
                            {% set icon = 'checkmark' %}
                        {% endif %}

                        Status:
                        <span class=\"ui {{ color }} label\" style=\"margin-left: 20px\"><i class=\"{{icon}} icon\"></i>{{ resource.status }}</span>

                        <table class=\"ui very basic celled table\">
                            <tbody>
                            {% if resource.importer is defined %}
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">{{ 'omni_sylius.importer.form.importer'|trans }}</strong></td>
                                    <td>{{resource.importer}}</td>
                                </tr>
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">{{ 'omni_sylius.importer.form.comments'|trans }}</strong></td>
                                    <td>{{resource.comments}}</td>
                                </tr>
                            {% endif %}
                            {% if resource.exporter is defined %}
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">{{ 'omni_sylius.exporter.form.exporter'|trans }}</strong></td>
                                    <td>{{resource.exporter}}</td>
                                </tr>
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">{{ 'omni_sylius.exporter.form.query_string'|trans }}</strong></td>
                                    <td><textarea disabled readonly style=\"width: 100%;\">{{resource.queryString}}</textarea></td>
                                </tr>
                                <tr>
                                    <td class=\"five wide\"><strong class=\"gray text\">{{ 'omni_sylius.exporter.form.comments'|trans }}</strong></td>
                                    <td>{{resource.comments}}</td>
                                </tr>
                            {% endif %}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"sixteen wide mobile six wide computer column\">
            <div id=\"pricing\">
                <h4 class=\"ui top attached large header\">Time</h4>
                <div class=\"ui attached segment\">
                    <table id=\"pricing\" class=\"ui very basic celled table\">
                        <tbody>
                        {% if resource.importer is defined %}
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>{{ 'omni_sylius.importer.form.created_at'|trans }}</strong>
                                </td>
                                <td>{{ resource.createdAt|date('Y-m-d  H:i:s') }}</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>{{ 'omni_sylius.importer.form.processing_started_at'|trans }}</strong>
                                </td>
                                <td>{% if resource.processingStartedAt %}{{ resource.processingStartedAt|date('Y-m-d  H:i:s') }} {% else %} - {% endif %}</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>{{ 'omni_sylius.importer.form.processed_at'|trans }}</strong>
                                </td>
                                <td>{% if resource.processedAt %}{{ resource.processedAt|date('Y-m-d  H:i:s') }} {% else %} - {% endif %}</td>
                            </tr>
                        {% endif %}
                        {% if resource.exporter is defined %}
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>{{ 'omni_sylius.exporter.form.created_at'|trans }}</strong>
                                </td>
                                <td>{{ resource.createdAt|date('Y-m-d  H:i:s') }}</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>{{ 'omni_sylius.exporter.form.processing_started_at'|trans }}</strong>
                                </td>
                                <td>{% if resource.processingStartedAt %}{{ resource.processingStartedAt|date('Y-m-d  H:i:s') }} {% else %} - {% endif %}</td>
                            </tr>
                            <tr>
                                <td class=\"five wide gray text\">
                                    <strong>{{ 'omni_sylius.exporter.form.processed_at'|trans }}</strong>
                                </td>
                                <td>{% if resource.processedAt %}{{ resource.processedAt|date('Y-m-d  H:i:s') }} {% else %} - {% endif %}</td>
                            </tr>
                        {% endif %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {% if resource.exporter is defined and resource.exportFile %}
            <a href=\"{{path('omni_sylius_admin_export_job_download', { 'id': resource.id })}}\">{{ 'omni_sylius.exporter.form.download'|trans }}</a>
        {% endif %}
        {% if resource.exporter is defined %}
            <a href=\"{{path('omni_sylius_admin_export_job_delete', { 'id': resource.id })}}\">{{ 'omni_sylius.exporter.form.delete'|trans }}</a>
        {% endif %}
    </div>

    {{ sonata_block_render_event(event_prefix ~ '.after_content', {'resource': resource}) }}
{% endblock %}

{% block stylesheets %}
    {{ parent() }}

    {{ sonata_block_render_event(event_prefix ~ '.stylesheets') }}
{% endblock %}

{% block javascripts %}
    {{ parent() }}

    {{ sonata_block_render_event(event_prefix ~ '.javascripts') }}
{% endblock %}
", "@OmniSyliusImportPlugin/Admin/Crud/show.html.twig", "/var/www/html/vendor/omni/sylius-import-plugin/src/Resources/views/Admin/Crud/show.html.twig");
    }
}
