<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* Form/_quote_form.html.twig */
class __TwigTemplate_8d41ce3d059319be0bb652cb1760b7165d44acdbd152c99068b918c9bd66d94d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "Form/_quote_form.html.twig"));

        // line 1
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), [0 => "@SyliusShop/Form/theme.html.twig"], true);
        // line 2
        echo "<h2 class=\"h1\">
    ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.quote.title"), "html", null, true);
        echo "
</h2>
<form method=\"post\" action=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("quote_submit", ["locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "request", [], "any", false, false, false, 5), "locale", [], "any", false, false, false, 5)]), "html", null, true);
        echo "\" id=\"quote-form\">
    <div class=\"row\">
        <div class=\"col-xs-12 col-md-8 mx-auto form-inner d-flex align-items-center flex-wrap flex-column\">
            <input type=\"hidden\" name=\"quote[_token]\" value=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("quote_submit"), "html", null, true);
        echo "\"/>
            ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "email", [], "any", false, false, false, 9), 'row');
        echo "
            ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), "phone", [], "any", false, false, false, 10), 'row');
        echo "
            <div class=\"note\">
                ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.quote.phone_note"), "html", null, true);
        echo "
            </div>
            ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "name", [], "any", false, false, false, 14), 'row');
        echo "

            ";
        // line 16
        if (!twig_in_filter("product", (isset($context["disable"]) || array_key_exists("disable", $context) ? $context["disable"] : (function () { throw new RuntimeError('Variable "disable" does not exist.', 16, $this->source); })()))) {
            // line 17
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 17, $this->source); })()), "product", [], "any", false, false, false, 17), 'row');
            echo "
            ";
        }
        // line 19
        echo "
            <div class=\"form-group-narrow\">
            ";
        // line 21
        if (!twig_in_filter("deadline", (isset($context["disable"]) || array_key_exists("disable", $context) ? $context["disable"] : (function () { throw new RuntimeError('Variable "disable" does not exist.', 21, $this->source); })()))) {
            // line 22
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 22, $this->source); })()), "deadline", [], "any", false, false, false, 22), 'row');
            echo "
            ";
        }
        // line 24
        echo "            </div>
            <div class=\"form-group-narrow\">
            ";
        // line 26
        if (!twig_in_filter("quantity", (isset($context["disable"]) || array_key_exists("disable", $context) ? $context["disable"] : (function () { throw new RuntimeError('Variable "disable" does not exist.', 26, $this->source); })()))) {
            // line 27
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), "quantity", [], "any", false, false, false, 27), 'row');
            echo "
            ";
        }
        // line 29
        echo "            </div>

            ";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "note", [], "any", false, false, false, 31), 'row');
        echo "

            <div class=\"button-wrapper\">
                <button type=\"submit\" class=\"btn btn-primary\" id=\"quote-submit\">
                    ";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.get_a_quote"), "html", null, true);
        echo "
                </button>
            </div>
        </div>
    </div>
</form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "Form/_quote_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 35,  115 => 31,  111 => 29,  105 => 27,  103 => 26,  99 => 24,  93 => 22,  91 => 21,  87 => 19,  81 => 17,  79 => 16,  74 => 14,  69 => 12,  64 => 10,  60 => 9,  56 => 8,  50 => 5,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% form_theme form '@SyliusShop/Form/theme.html.twig' %}
<h2 class=\"h1\">
    {{ 'app.quote.title'|trans }}
</h2>
<form method=\"post\" action=\"{{ path('quote_submit', {locale: app.request.locale}) }}\" id=\"quote-form\">
    <div class=\"row\">
        <div class=\"col-xs-12 col-md-8 mx-auto form-inner d-flex align-items-center flex-wrap flex-column\">
            <input type=\"hidden\" name=\"quote[_token]\" value=\"{{ csrf_token('quote_submit') }}\"/>
            {{ form_row(form.email) }}
            {{ form_row(form.phone) }}
            <div class=\"note\">
                {{ 'app.quote.phone_note'|trans }}
            </div>
            {{ form_row(form.name) }}

            {% if 'product' not in disable %}
                {{ form_row(form.product) }}
            {% endif %}

            <div class=\"form-group-narrow\">
            {% if 'deadline' not in disable %}
                {{ form_row(form.deadline) }}
            {% endif %}
            </div>
            <div class=\"form-group-narrow\">
            {% if 'quantity' not in disable %}
                {{ form_row(form.quantity) }}
            {% endif %}
            </div>

            {{ form_row(form.note) }}

            <div class=\"button-wrapper\">
                <button type=\"submit\" class=\"btn btn-primary\" id=\"quote-submit\">
                    {{ 'app.ui.get_a_quote'|trans }}
                </button>
            </div>
        </div>
    </div>
</form>
", "Form/_quote_form.html.twig", "/var/www/html/templates/Form/_quote_form.html.twig");
    }
}
