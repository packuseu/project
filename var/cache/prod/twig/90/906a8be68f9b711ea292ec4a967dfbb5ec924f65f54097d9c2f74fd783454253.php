<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Register:_form.html.twig */
class __TwigTemplate_820d5e20da2fc896900737bc3ce832c556eb61035f5bfa1431a680a772c10a81 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Register:_form.html.twig"));

        // line 1
        echo "<h4>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.registration.social_logins"), "html", null, true);
        echo "</h4>

<div class=\"text-center\">
    <a href=\"";
        // line 4
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("hwi_oauth_service_redirect", ["service" => "facebook"]);
        echo "\" class=\"btn btn-primary fb-login\">
        <img class=\"soc-icon\" src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/fb.png"), "html", null, true);
        echo "\" alt=\"facebook login\">
        Facebook
    </a>
    <a href=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("hwi_oauth_service_redirect", ["service" => "google"]);
        echo "\" class=\"btn btn-primary google-login\">
        <img class=\"soc-icon\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/g.png"), "html", null, true);
        echo "\" alt=\"google login\">
        Google
    </a>
</div>
<div class=\"text-divider\">
    <div class=\"text-divider__info\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.registration.or"), "html", null, true);
        echo "</div>
    <div class=\"text-divider__line\"></div>
</div>

<h4>";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.personal_information"), "html", null, true);
        echo "</h4>

<div class=\"row\">
    <div class=\"col\">";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "firstName", [], "any", false, false, false, 21), 'row');
        echo "</div>
    <div class=\"col\">";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 22, $this->source); })()), "lastName", [], "any", false, false, false, 22), 'row');
        echo "</div>
</div>

";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), "email", [], "any", false, false, false, 25), 'row');
        echo "
";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 26, $this->source); })()), "phoneNumber", [], "any", false, false, false, 26), 'row');
        echo "

<h4 class=\"mt-5\">";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("sylius.ui.account_credentials"), "html", null, true);
        echo "</h4>

";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 30, $this->source); })()), "user", [], "any", false, false, false, 30), "plainPassword", [], "any", false, false, false, 30), "first", [], "any", false, false, false, 30), 'row');
        echo "
";
        // line 31
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "user", [], "any", false, false, false, 31), "plainPassword", [], "any", false, false, false, 31), "second", [], "any", false, false, false, 31), 'row');
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Register:_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 31,  106 => 30,  101 => 28,  96 => 26,  92 => 25,  86 => 22,  82 => 21,  76 => 18,  69 => 14,  61 => 9,  57 => 8,  51 => 5,  47 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<h4>{{ 'app.registration.social_logins'|trans }}</h4>

<div class=\"text-center\">
    <a href=\"{{ path('hwi_oauth_service_redirect', {'service': 'facebook' }) }}\" class=\"btn btn-primary fb-login\">
        <img class=\"soc-icon\" src=\"{{ asset('img/fb.png') }}\" alt=\"facebook login\">
        Facebook
    </a>
    <a href=\"{{ path('hwi_oauth_service_redirect', {'service': 'google' }) }}\" class=\"btn btn-primary google-login\">
        <img class=\"soc-icon\" src=\"{{ asset('img/g.png') }}\" alt=\"google login\">
        Google
    </a>
</div>
<div class=\"text-divider\">
    <div class=\"text-divider__info\">{{ 'app.registration.or'|trans }}</div>
    <div class=\"text-divider__line\"></div>
</div>

<h4>{{ 'sylius.ui.personal_information'|trans }}</h4>

<div class=\"row\">
    <div class=\"col\">{{ form_row(form.firstName) }}</div>
    <div class=\"col\">{{ form_row(form.lastName) }}</div>
</div>

{{ form_row(form.email) }}
{{ form_row(form.phoneNumber) }}

<h4 class=\"mt-5\">{{ 'sylius.ui.account_credentials'|trans }}</h4>

{{ form_row(form.user.plainPassword.first) }}
{{ form_row(form.user.plainPassword.second) }}
", "SyliusShopBundle:Register:_form.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/Register/_form.html.twig");
    }
}
