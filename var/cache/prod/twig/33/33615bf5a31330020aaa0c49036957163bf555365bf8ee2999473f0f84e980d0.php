<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* BitBagSyliusShippingExportPlugin:ShippingExport/Action:_exportShipments.html.twig */
class __TwigTemplate_2979726d52c0ed9ef3919ddff720ed78ff2ec639f2c5d65a3c630379897ee038 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "BitBagSyliusShippingExportPlugin:ShippingExport/Action:_exportShipments.html.twig"));

        // line 1
        echo "<form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("bitbag_admin_export_all_new_shipments");
        echo "\" method=\"POST\">
    <input type=\"hidden\" name=\"_method\" value=\"PUT\">
    <button type=\"submit\" class=\"ui labeled icon primary button export-all-new-shipments\">
        <i class=\"arrow up icon\"></i>
        ";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("bitbag.ui.export_all_new_shipments"), "html", null, true);
        echo "
    </button>
</form>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "BitBagSyliusShippingExportPlugin:ShippingExport/Action:_exportShipments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 5,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<form action=\"{{ path('bitbag_admin_export_all_new_shipments') }}\" method=\"POST\">
    <input type=\"hidden\" name=\"_method\" value=\"PUT\">
    <button type=\"submit\" class=\"ui labeled icon primary button export-all-new-shipments\">
        <i class=\"arrow up icon\"></i>
        {{ 'bitbag.ui.export_all_new_shipments'|trans }}
    </button>
</form>
", "BitBagSyliusShippingExportPlugin:ShippingExport/Action:_exportShipments.html.twig", "/var/www/html/vendor/bitbag/shipping-export-plugin/src/Resources/views/ShippingExport/Action/_exportShipments.html.twig");
    }
}
