<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:ProductReview:_single.html.twig */
class __TwigTemplate_5750e0994e6c34692ae5dcaf1f1c8e99a00f6afb4f3d73e2175caa18ddd6d478 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:ProductReview:_single.html.twig"));

        // line 1
        echo "<div class=\"pb-2 mb-2\">
    <div class=\"mr-3 customer-reviews-stars\">";
        // line 2
        $this->loadTemplate("@SyliusShop/Common/_rating.html.twig", "SyliusShopBundle:ProductReview:_single.html.twig", 2)->display(twig_array_merge($context, ["average" => twig_get_attribute($this->env, $this->source, (isset($context["review"]) || array_key_exists("review", $context) ? $context["review"] : (function () { throw new RuntimeError('Variable "review" does not exist.', 2, $this->source); })()), "rating", [], "any", false, false, false, 2), "viewonly" => "true"]));
        echo "</div>
    <div class=\"text-muted mt-2 mb-2\">
        By <span class=\"text-black-50\">";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["review"]) || array_key_exists("review", $context) ? $context["review"] : (function () { throw new RuntimeError('Variable "review" does not exist.', 4, $this->source); })()), "author", [], "any", false, false, false, 4), "firstName", [], "any", false, false, false, 4), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["review"]) || array_key_exists("review", $context) ? $context["review"] : (function () { throw new RuntimeError('Variable "review" does not exist.', 4, $this->source); })()), "author", [], "any", false, false, false, 4), "firstName", [], "any", false, false, false, 4), "html", null, true);
        echo "</span> on ";
        echo $this->extensions['Sonata\IntlBundle\Twig\Extension\DateTimeExtension']->formatDate(twig_get_attribute($this->env, $this->source, (isset($context["review"]) || array_key_exists("review", $context) ? $context["review"] : (function () { throw new RuntimeError('Variable "review" does not exist.', 4, $this->source); })()), "createdAt", [], "any", false, false, false, 4));
        echo "
    </div>
    <div>";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["review"]) || array_key_exists("review", $context) ? $context["review"] : (function () { throw new RuntimeError('Variable "review" does not exist.', 6, $this->source); })()), "comment", [], "any", false, false, false, 6), "html", null, true);
        echo "</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:ProductReview:_single.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 6,  48 => 4,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"pb-2 mb-2\">
    <div class=\"mr-3 customer-reviews-stars\">{% include '@SyliusShop/Common/_rating.html.twig' with {'average': review.rating, 'viewonly': 'true'} %}</div>
    <div class=\"text-muted mt-2 mb-2\">
        By <span class=\"text-black-50\">{{ review.author.firstName }} {{ review.author.firstName }}</span> on {{ review.createdAt|format_date }}
    </div>
    <div>{{ review.comment }}</div>
</div>
", "SyliusShopBundle:ProductReview:_single.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/ProductReview/_single.html.twig");
    }
}
