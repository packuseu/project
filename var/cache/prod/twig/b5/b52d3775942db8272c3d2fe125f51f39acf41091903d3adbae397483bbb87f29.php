<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle::layout.html.twig */
class __TwigTemplate_117d8df6a0b2f858afa0b9bbb3877be43e57952a49c6efdfb678fef89093b512 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'metatags' => [$this, 'block_metatags'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'head_extra' => [$this, 'block_head_extra'],
            'top' => [$this, 'block_top'],
            'header' => [$this, 'block_header'],
            'content' => [$this, 'block_content'],
            'lead_catcher' => [$this, 'block_lead_catcher'],
            'footer' => [$this, 'block_footer'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>

<html lang=\"";
        // line 3
        echo twig_escape_filter($this->env, twig_slice($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 3, $this->source); })()), "request", [], "any", false, false, false, 3), "locale", [], "any", false, false, false, 3), 0, 2), "html", null, true);
        echo "\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=\"https://www.googletagmanager.com/gtag/js?id=G-4F9JMMRKSK\"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-389456798');
    </script>
    <meta name=\"google-site-verification\" content=\"BSPrD29_ZZeZ5SPZqa_NnZShwPMZKMWKPKrRIx_3kO8\" />
    <title>";
        // line 18
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">

    <meta name=\"verify-paysera\" content=\"8b2d931475c9052983e227ff8a8b58c1\">

    ";
        // line 24
        $this->displayBlock('metatags', $context, $blocks);
        // line 26
        echo "
    ";
        // line 27
        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 27, $this->source); })()), "channel", [], "any", false, false, false, 27), "logos", [], "any", false, false, false, 27)) > 0)) {
            // line 28
            echo "        <link rel=\"icon\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("/media/image/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["sylius"]) || array_key_exists("sylius", $context) ? $context["sylius"] : (function () { throw new RuntimeError('Variable "sylius" does not exist.', 28, $this->source); })()), "channel", [], "any", false, false, false, 28), "logos", [], "any", false, false, false, 28), 0, [], "array", false, false, false, 28), "path", [], "any", false, false, false, 28))), "html", null, true);
            echo "\">
    ";
        }
        // line 30
        echo "
    <link href=\"https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;600&display=swap\" rel=\"stylesheet\">
    <link href=\"https://fonts.googleapis.com/css2?family=Montserrat&display=swap\" rel=\"stylesheet\">

    ";
        // line 34
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 41
        echo "    ";
        $this->displayBlock('head_extra', $context, $blocks);
        // line 42
        echo "
    ";
        // line 43
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.head"]);
        echo "
</head>

<body>
<div class=\"top-banner-wrapper\">
    ";
        // line 48
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "before-header-zone");
        echo "
</div>
";
        // line 50
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.before_body"]);
        echo "

<div id=\"cookie-holder\" data-notice=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.cookies.notice"), "html", null, true);
        echo " <a href='#'>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.cookies.policy"), "html", null, true);
        echo "</a>.\" data-submit=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.ui.cookies.submit"), "html", null, true);
        echo "\"></div>

";
        // line 54
        $this->displayBlock('top', $context, $blocks);
        // line 67
        echo "
";
        // line 68
        $this->displayBlock('header', $context, $blocks);
        // line 85
        echo "
<div class=\"main-content container\">
    ";
        // line 87
        $this->loadTemplate("@SyliusShop/_flashes.html.twig", "SyliusShopBundle::layout.html.twig", 87)->display($context);
        // line 88
        echo "
    ";
        // line 89
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.before_content"]);
        echo "

    ";
        // line 91
        $this->displayBlock('content', $context, $blocks);
        // line 93
        echo "
    ";
        // line 94
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.after_content"]);
        echo "

    ";
        // line 96
        echo $this->extensions['Omni\Sylius\BannerPlugin\Twig\RenderExtension']->displayZone($this->env, "before-footer-zone");
        echo "
</div>

";
        // line 99
        $this->displayBlock('lead_catcher', $context, $blocks);
        // line 100
        echo "
";
        // line 101
        $this->displayBlock('footer', $context, $blocks);
        // line 108
        echo "
";
        // line 109
        $this->displayBlock('javascripts', $context, $blocks);
        // line 116
        echo "
";
        // line 117
        $this->displayBlock('extra_javascripts', $context, $blocks);
        // line 118
        echo "
";
        // line 119
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.after_body"]);
        echo "

<div class=\"loading-overlay\" data-js-loading-overlay>
    <div class=\"spinner-border\" role=\"status\">
        <span class=\"sr-only\">Loading...</span>
    </div>
</div>

</body>
</html>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 18
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("app.title"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 24
    public function block_metatags($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "metatags"));

        // line 25
        echo "    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 34
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 35
        echo "        ";
        // line 36
        echo "
        ";
        // line 37
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("appProductV3", null, "bootstrapThemeProductV3");
        echo "

        ";
        // line 39
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.stylesheets"]);
        echo "
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 41
    public function block_head_extra($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "head_extra"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 54
    public function block_top($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "top"));

        // line 55
        echo "    <div class=\"bg-dark\">
        <div class=\"container\">
            <div class=\"d-flex\">
                ";
        // line 58
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.before_currency_switcher"]);
        echo "

                ";
        // line 60
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("sylius.controller.shop.currency_switch:renderAction"));
        echo "

                ";
        // line 62
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Symfony\Bridge\Twig\Extension\HttpKernelExtension::controller("sylius.controller.shop.locale_switch:renderAction"));
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 68
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "header"));

        // line 69
        echo "    <div class=\"mb-2\">
        <div class=\"container\">
            <header class=\"my-4\">
                ";
        // line 72
        $this->loadTemplate("@SyliusShop/_header.html.twig", "SyliusShopBundle::layout.html.twig", 72)->display($context);
        // line 73
        echo "
                ";
        // line 74
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.after_header"]);
        echo "
            </header>
        </div>

        <div class=\"bg-light\">
            <div class=\"container\">
                ";
        // line 80
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("sylius_shop_partial_taxon_index_by_code", ["code" => "category", "template" => "@SyliusShop/Taxon/_horizontalMenu.html.twig"]));
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 91
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "content"));

        // line 92
        echo "    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 99
    public function block_lead_catcher($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "lead_catcher"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 101
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "footer"));

        // line 102
        echo "    <div class=\"bg-light border-top py-5 mt-5\">
        <div class=\"container\">
            ";
        // line 104
        $this->loadTemplate("@SyliusShop/_footer.html.twig", "SyliusShopBundle::layout.html.twig", 104)->display($context);
        // line 105
        echo "        </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 109
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 110
        echo "    ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app", null, "bootstrapTheme");
        echo "

    ";
        // line 112
        echo call_user_func_array($this->env->getFunction('sonata_block_render_event')->getCallable(), ["sylius.shop.layout.javascripts"]);
        echo "

    ";
        // line 114
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("appProductV3", null, "bootstrapThemeProductV3");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 117
    public function block_extra_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "extra_javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  423 => 117,  414 => 114,  409 => 112,  403 => 110,  396 => 109,  387 => 105,  385 => 104,  381 => 102,  374 => 101,  362 => 99,  355 => 92,  348 => 91,  336 => 80,  327 => 74,  324 => 73,  322 => 72,  317 => 69,  310 => 68,  298 => 62,  293 => 60,  288 => 58,  283 => 55,  276 => 54,  264 => 41,  255 => 39,  250 => 37,  247 => 36,  245 => 35,  238 => 34,  231 => 25,  224 => 24,  211 => 18,  193 => 119,  190 => 118,  188 => 117,  185 => 116,  183 => 109,  180 => 108,  178 => 101,  175 => 100,  173 => 99,  167 => 96,  162 => 94,  159 => 93,  157 => 91,  152 => 89,  149 => 88,  147 => 87,  143 => 85,  141 => 68,  138 => 67,  136 => 54,  127 => 52,  122 => 50,  117 => 48,  109 => 43,  106 => 42,  103 => 41,  101 => 34,  95 => 30,  89 => 28,  87 => 27,  84 => 26,  82 => 24,  73 => 18,  55 => 3,  51 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>

<html lang=\"{{ app.request.locale|slice(0, 2) }}\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=\"https://www.googletagmanager.com/gtag/js?id=G-4F9JMMRKSK\"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-389456798');
    </script>
    <meta name=\"google-site-verification\" content=\"BSPrD29_ZZeZ5SPZqa_NnZShwPMZKMWKPKrRIx_3kO8\" />
    <title>{% block title %}{{ 'app.title'|trans }}{% endblock %}</title>

    <meta content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\" name=\"viewport\">

    <meta name=\"verify-paysera\" content=\"8b2d931475c9052983e227ff8a8b58c1\">

    {% block metatags %}
    {% endblock %}

    {% if sylius.channel.logos|length > 0 %}
        <link rel=\"icon\" href=\"{{ asset('/media/image/' ~ sylius.channel.logos[0].path) }}\">
    {% endif %}

    <link href=\"https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;600&display=swap\" rel=\"stylesheet\">
    <link href=\"https://fonts.googleapis.com/css2?family=Montserrat&display=swap\" rel=\"stylesheet\">

    {% block stylesheets %}
        {#{{ encore_entry_link_tags('app', null, 'bootstrapThemeProductV3') }}#}

        {{ encore_entry_link_tags('appProductV3', null, 'bootstrapThemeProductV3') }}

        {{ sonata_block_render_event('sylius.shop.layout.stylesheets') }}
    {% endblock %}
    {% block head_extra %}{% endblock %}

    {{ sonata_block_render_event('sylius.shop.layout.head') }}
</head>

<body>
<div class=\"top-banner-wrapper\">
    {{ omni_sylius_banner_zone('before-header-zone') }}
</div>
{{ sonata_block_render_event('sylius.shop.layout.before_body') }}

<div id=\"cookie-holder\" data-notice=\"{{ 'app.ui.cookies.notice'|trans }} <a href='#'>{{ 'app.ui.cookies.policy'|trans }}</a>.\" data-submit=\"{{ 'app.ui.cookies.submit'|trans }}\"></div>

{% block top %}
    <div class=\"bg-dark\">
        <div class=\"container\">
            <div class=\"d-flex\">
                {{ sonata_block_render_event('sylius.shop.layout.before_currency_switcher') }}

                {{ render(controller('sylius.controller.shop.currency_switch:renderAction')) }}

                {{ render(controller('sylius.controller.shop.locale_switch:renderAction')) }}
            </div>
        </div>
    </div>
{% endblock %}

{% block header %}
    <div class=\"mb-2\">
        <div class=\"container\">
            <header class=\"my-4\">
                {% include '@SyliusShop/_header.html.twig' %}

                {{ sonata_block_render_event('sylius.shop.layout.after_header') }}
            </header>
        </div>

        <div class=\"bg-light\">
            <div class=\"container\">
                {{ render(url('sylius_shop_partial_taxon_index_by_code', {'code': 'category', 'template': '@SyliusShop/Taxon/_horizontalMenu.html.twig'})) }}
            </div>
        </div>
    </div>
{% endblock %}

<div class=\"main-content container\">
    {% include '@SyliusShop/_flashes.html.twig' %}

    {{ sonata_block_render_event('sylius.shop.layout.before_content') }}

    {% block content %}
    {% endblock %}

    {{ sonata_block_render_event('sylius.shop.layout.after_content') }}

    {{ omni_sylius_banner_zone('before-footer-zone') }}
</div>

{% block lead_catcher %}{% endblock %}

{% block footer %}
    <div class=\"bg-light border-top py-5 mt-5\">
        <div class=\"container\">
            {% include '@SyliusShop/_footer.html.twig' %}
        </div>
    </div>
{% endblock %}

{% block javascripts %}
    {{ encore_entry_script_tags('app', null, 'bootstrapTheme') }}

    {{ sonata_block_render_event('sylius.shop.layout.javascripts') }}

    {{ encore_entry_script_tags('appProductV3', null, 'bootstrapThemeProductV3') }}
{% endblock %}

{% block extra_javascripts %}{% endblock %}

{{ sonata_block_render_event('sylius.shop.layout.after_body') }}

<div class=\"loading-overlay\" data-js-loading-overlay>
    <div class=\"spinner-border\" role=\"status\">
        <span class=\"sr-only\">Loading...</span>
    </div>
</div>

</body>
</html>
", "SyliusShopBundle::layout.html.twig", "/var/www/html/themes/BootstrapThemeProductV3/SyliusShopBundle/views/layout.html.twig");
    }
}
