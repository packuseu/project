<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* SyliusShopBundle:Common/Order:_shipments.html.twig */
class __TwigTemplate_4511a58ef3395e109231883165ea44667b0c8f30ad1160604576945cbcec2d05 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "SyliusShopBundle:Common/Order:_shipments.html.twig"));

        // line 1
        $macros["icons"] = $this->macros["icons"] = $this->loadTemplate("@SyliusShop/Common/Macro/icons.html.twig", "SyliusShopBundle:Common/Order:_shipments.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["order"]) || array_key_exists("order", $context) ? $context["order"] : (function () { throw new RuntimeError('Variable "order" does not exist.', 3, $this->source); })()), "shipments", [], "any", false, false, false, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["shipment"]) {
            // line 4
            echo "    <div class=\"card bg-light\">
        <div class=\"card-body\">
            <div class=\"d-flex align-items-center\">
                <div class=\"pr-3\" style=\"font-size:2rem;\">";
            // line 7
            echo twig_call_macro($macros["icons"], "macro_transport", [], 7, $context, $this->getSourceContext());
            echo "</div>
                <div><strong>";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["shipment"], "method", [], "any", false, false, false, 8), "html", null, true);
            echo "</strong></div>
            </div>
        </div>
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['shipment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "SyliusShopBundle:Common/Order:_shipments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 8,  54 => 7,  49 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% import \"@SyliusShop/Common/Macro/icons.html.twig\" as icons %}

{% for shipment in order.shipments %}
    <div class=\"card bg-light\">
        <div class=\"card-body\">
            <div class=\"d-flex align-items-center\">
                <div class=\"pr-3\" style=\"font-size:2rem;\">{{ icons.transport() }}</div>
                <div><strong>{{ shipment.method }}</strong></div>
            </div>
        </div>
    </div>
{% endfor %}
", "SyliusShopBundle:Common/Order:_shipments.html.twig", "/var/www/html/themes/BootstrapTheme/SyliusShopBundle/views/Common/Order/_shipments.html.twig");
    }
}
