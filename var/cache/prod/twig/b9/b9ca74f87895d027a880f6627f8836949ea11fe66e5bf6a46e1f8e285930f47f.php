<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* _menu_display.html.twig */
class __TwigTemplate_12621cf2c2ab59fed50af3c2090ba00fe44c0aa5b4d1414bc3acb891293383a9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "_menu_display.html.twig"));

        // line 91
        echo "
";
        // line 97
        echo "
";
        // line 103
        echo "
";
        // line 114
        echo "
";
        // line 115
        $macros["macros"] = $this->macros["macros"] = $this;
        // line 116
        echo "

<nav class=\"detatched-left top-navbar\">
    <div class=\"\">
        ";
        // line 120
        $context["headerNode"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getNodeByType("main_menu");
        // line 121
        echo "        ";
        echo twig_escape_filter($this->env, $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->instantiateNodeRelations((isset($context["headerNode"]) || array_key_exists("headerNode", $context) ? $context["headerNode"] : (function () { throw new RuntimeError('Variable "headerNode" does not exist.', 121, $this->source); })())), "html", null, true);
        echo "

        ";
        // line 123
        if ((null != (isset($context["headerNode"]) || array_key_exists("headerNode", $context) ? $context["headerNode"] : (function () { throw new RuntimeError('Variable "headerNode" does not exist.', 123, $this->source); })()))) {
            // line 124
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["headerNode"]) || array_key_exists("headerNode", $context) ? $context["headerNode"] : (function () { throw new RuntimeError('Variable "headerNode" does not exist.', 124, $this->source); })()), "children", [], "any", false, false, false, 124));
            foreach ($context['_seq'] as $context["_key"] => $context["node"]) {
                // line 125
                echo "                ";
                if (twig_get_attribute($this->env, $this->source, $context["node"], "enabled", [], "any", false, false, false, 125)) {
                    // line 126
                    echo "                    ";
                    echo twig_call_macro($macros["macros"], "macro_node_item", [$context["node"]], 126, $context, $this->getSourceContext());
                    echo "
                ";
                }
                // line 128
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['node'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "        ";
        }
        // line 130
        echo "    </div>
</nav>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 1
    public function macro_node_item($__node__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "node_item"));

            // line 2
            echo "    ";
            $macros["macros"] = $this;
            // line 3
            echo "
    ";
            // line 4
            echo twig_escape_filter($this->env, $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->instantiateNodeRelations((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 4, $this->source); })())), "html", null, true);
            echo "

    ";
            // line 6
            if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 6, $this->source); })()), "children", [], "any", false, false, false, 6)) > 0)) {
                // line 7
                echo "        <span class=\"dropdown dropdown--position\">
            <a class=\"header-primary\" href=\"#\" role=\"button\" data-toggle=\"dropdown\">
                ";
                // line 9
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 9, $this->source); })()), "translation", [], "any", false, false, false, 9), "title", [], "any", false, false, false, 9), "html", null, true);
                echo "
            </a>
            <div class=\"dropdown-wrapper\">
                <div class=\"container dropdown-menu top-main-nav-dropdown\">
                    <div class=\"row\">
                        <div class=\"col-6\">
                            <div class=\"row nav-items-wrapper\">
                                <div class=\"top-main-nav-dropdown-top pr-0 col-6\">
                                    ";
                // line 17
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 17, $this->source); })()), "children", [], "any", false, false, false, 17));
                foreach ($context['_seq'] as $context["key"] => $context["subnode"]) {
                    if (($context["key"] % 2 == 0)) {
                        // line 18
                        echo "                                        ";
                        $context["link"] = "#";
                        // line 19
                        echo "
                                        ";
                        // line 20
                        if (twig_get_attribute($this->env, $this->source, $context["subnode"], "link", [], "any", false, false, false, 20)) {
                            // line 21
                            echo "                                            ";
                            $context["link"] = twig_get_attribute($this->env, $this->source, $context["subnode"], "link", [], "any", false, false, false, 21);
                            // line 22
                            echo "                                        ";
                        } elseif ((twig_get_attribute($this->env, $this->source, $context["subnode"], "relation", [], "any", false, false, false, 22) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "relation", [], "any", false, false, false, 22), "slug", [], "any", false, false, false, 22))) {
                            // line 23
                            echo "                                            ";
                            if ((twig_get_attribute($this->env, $this->source, $context["subnode"], "relationType", [], "any", false, false, false, 23) == "sylius.taxon")) {
                                // line 24
                                echo "                                                ";
                                $context["link"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_index", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "relation", [], "any", false, false, false, 24), "slug", [], "any", false, false, false, 24)]);
                                // line 25
                                echo "                                            ";
                            }
                            // line 26
                            echo "                                        ";
                        }
                        // line 27
                        echo "
                                        <div class=\"image-src\"
                                             data-img=\"
                                            ";
                        // line 30
                        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subnode"], "images", [], "any", false, false, false, 30)) > 0)) {
                            // line 31
                            echo "                                                ";
                            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "images", [], "any", false, false, false, 31), "first", [], "any", false, false, false, 31), "path", [], "any", false, false, false, 31), "omni_sylius_banner"), "html", null, true);
                            echo "
                                            ";
                        }
                        // line 32
                        echo "\"
                                        >
                                            <a href=\"";
                        // line 34
                        echo twig_escape_filter($this->env, (isset($context["link"]) || array_key_exists("link", $context) ? $context["link"] : (function () { throw new RuntimeError('Variable "link" does not exist.', 34, $this->source); })()), "html", null, true);
                        echo "\" class=\"top-main-nav-item js-show-image\" data-node=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subnode"], "code", [], "any", false, false, false, 34), "html", null, true);
                        echo "\">
                                                ";
                        // line 35
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "translation", [], "any", false, false, false, 35), "title", [], "any", false, false, false, 35), "html", null, true);
                        echo "
                                            </a>
                                        </div>
                                    ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['subnode'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 39
                echo "                                </div>
                                <div class=\"top-main-nav-dropdown-top pr-0 col-6\">
                                    ";
                // line 41
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 41, $this->source); })()), "children", [], "any", false, false, false, 41));
                foreach ($context['_seq'] as $context["key"] => $context["subnode"]) {
                    if (($context["key"] % 2 != 0)) {
                        // line 42
                        echo "                                        ";
                        $context["link"] = "#";
                        // line 43
                        echo "
                                        ";
                        // line 44
                        if (twig_get_attribute($this->env, $this->source, $context["subnode"], "link", [], "any", false, false, false, 44)) {
                            // line 45
                            echo "                                            ";
                            $context["link"] = twig_get_attribute($this->env, $this->source, $context["subnode"], "link", [], "any", false, false, false, 45);
                            // line 46
                            echo "                                        ";
                        } elseif ((twig_get_attribute($this->env, $this->source, $context["subnode"], "relation", [], "any", false, false, false, 46) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "relation", [], "any", false, false, false, 46), "slug", [], "any", false, false, false, 46))) {
                            // line 47
                            echo "                                            ";
                            if ((twig_get_attribute($this->env, $this->source, $context["subnode"], "relationType", [], "any", false, false, false, 47) == "sylius.taxon")) {
                                // line 48
                                echo "                                                ";
                                $context["link"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_index", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "relation", [], "any", false, false, false, 48), "slug", [], "any", false, false, false, 48)]);
                                // line 49
                                echo "                                            ";
                            }
                            // line 50
                            echo "                                        ";
                        }
                        // line 51
                        echo "
                                        <div class=\"image-src\"
                                             data-img=\"
                                            ";
                        // line 54
                        if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "images", [], "any", false, false, false, 54), "first", [], "any", false, false, false, 54)) > 0)) {
                            // line 55
                            echo "                                                ";
                            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "images", [], "any", false, false, false, 55), "first", [], "any", false, false, false, 55), "path", [], "any", false, false, false, 55), "omni_sylius_banner"), "html", null, true);
                            echo "
                                            ";
                        }
                        // line 56
                        echo "\"
                                        >
                                            <a href=\"";
                        // line 58
                        echo twig_escape_filter($this->env, (isset($context["link"]) || array_key_exists("link", $context) ? $context["link"] : (function () { throw new RuntimeError('Variable "link" does not exist.', 58, $this->source); })()), "html", null, true);
                        echo "\" class=\"top-main-nav-item js-show-image\" data-node=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["subnode"], "code", [], "any", false, false, false, 58), "html", null, true);
                        echo "\">
                                                ";
                        // line 59
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["subnode"], "translation", [], "any", false, false, false, 59), "title", [], "any", false, false, false, 59), "html", null, true);
                        echo "
                                            </a>
                                        </div>
                                    ";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['subnode'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 63
                echo "                                </div>
                                <div class=\"top-main-nav-dropdown-inner col-6 pl-0 pr-0\">
                                    ";
                // line 65
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 65, $this->source); })()), "children", [], "any", false, false, false, 65));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 66
                    echo "                                        ";
                    if ((twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "children", [], "any", false, false, false, 66)) > 0)) {
                        // line 67
                        echo "                                            <div class=\"nav-right-container hidden\" data-node=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["child"], "code", [], "any", false, false, false, 67), "html", null, true);
                        echo "\">
                                                ";
                        // line 68
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["child"], "children", [], "any", false, false, false, 68));
                        foreach ($context['_seq'] as $context["_key"] => $context["subchild"]) {
                            // line 69
                            echo "                                                    ";
                            echo twig_call_macro($macros["macros"], "macro_inner_node_inner", [$context["subchild"]], 69, $context, $this->getSourceContext());
                            echo "
                                                ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subchild'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 71
                        echo "                                            </div>
                                        ";
                    }
                    // line 73
                    echo "                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 74
                echo "                                </div>

                            </div>
                        </div>
                        <div class=\"col-6\">
                            <div class=\"text-center px-0\">
                                <img src id=\"nav-item-image\" class=\"item-image\">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </span>
    ";
            } else {
                // line 88
                echo "        ";
                echo twig_call_macro($macros["macros"], "macro_render_simple_node", [(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 88, $this->source); })())], 88, $context, $this->getSourceContext());
                echo "
    ";
            }
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 92
    public function macro_inner_node_top($__node__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "inner_node_top"));

            // line 93
            echo "    ";
            $macros["macros"] = $this;
            // line 94
            echo "
    ";
            // line 95
            echo twig_call_macro($macros["macros"], "macro_render_simple_node", [(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 95, $this->source); })()), "top-main-nav-item", ("data-node=" . twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 95, $this->source); })()), "code", [], "any", false, false, false, 95))], 95, $context, $this->getSourceContext());
            echo "
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 98
    public function macro_inner_node_inner($__node__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "inner_node_inner"));

            // line 99
            echo "    ";
            $macros["macros"] = $this;
            // line 100
            echo "
    ";
            // line 101
            echo twig_call_macro($macros["macros"], "macro_render_simple_node", [(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 101, $this->source); })()), "inner-main-nav-item pl-3"], 101, $context, $this->getSourceContext());
            echo "
";
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 104
    public function macro_render_simple_node($__node__ = null, $__class__ = "", $__data__ = "", ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "node" => $__node__,
            "class" => $__class__,
            "data" => $__data__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "macro", "render_simple_node"));

            // line 105
            echo "    ";
            if ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeLinkable((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 105, $this->source); })()))) {
                // line 106
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 106, $this->source); })()), "link", [], "any", false, false, false, 106), "html", null, true);
                echo "\" class=\"";
                if ((isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new RuntimeError('Variable "class" does not exist.', 106, $this->source); })())) {
                    echo twig_escape_filter($this->env, (isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new RuntimeError('Variable "class" does not exist.', 106, $this->source); })()), "html", null, true);
                } else {
                    echo "navbar-item";
                }
                echo "\" ";
                echo twig_escape_filter($this->env, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 106, $this->source); })()), "html", null, true);
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 106, $this->source); })()), "translation", [], "any", false, false, false, 106), "title", [], "any", false, false, false, 106), "html", null, true);
                echo "</a>
    ";
            } elseif ($this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->isNodeRelated(            // line 107
(isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 107, $this->source); })()))) {
                // line 108
                echo "        ";
                $context["relation"] = $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getRelation((isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 108, $this->source); })()));
                // line 109
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("sylius_shop_product_index", ["slug" => twig_get_attribute($this->env, $this->source, (isset($context["relation"]) || array_key_exists("relation", $context) ? $context["relation"] : (function () { throw new RuntimeError('Variable "relation" does not exist.', 109, $this->source); })()), "slug", [], "any", false, false, false, 109), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["relation"]) || array_key_exists("relation", $context) ? $context["relation"] : (function () { throw new RuntimeError('Variable "relation" does not exist.', 109, $this->source); })()), "translation", [], "any", false, false, false, 109), "locale", [], "any", false, false, false, 109)]), "html", null, true);
                echo "\" class=\"";
                if ((isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new RuntimeError('Variable "class" does not exist.', 109, $this->source); })())) {
                    echo twig_escape_filter($this->env, (isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new RuntimeError('Variable "class" does not exist.', 109, $this->source); })()), "html", null, true);
                } else {
                    echo "navbar-item";
                }
                echo "\" ";
                echo twig_escape_filter($this->env, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 109, $this->source); })()), "html", null, true);
                echo ">";
                echo twig_escape_filter($this->env, $this->extensions['Omni\Sylius\CmsPlugin\Twig\NodeExtension']->getRelationLabel((isset($context["relation"]) || array_key_exists("relation", $context) ? $context["relation"] : (function () { throw new RuntimeError('Variable "relation" does not exist.', 109, $this->source); })())), "html", null, true);
                echo "</a>
    ";
            } else {
                // line 111
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("omni_sylius_cms_frontend_show", ["slug" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 111, $this->source); })()), "translation", [], "any", false, false, false, 111), "slug", [], "any", false, false, false, 111), "_locale" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 111, $this->source); })()), "translation", [], "any", false, false, false, 111), "locale", [], "any", false, false, false, 111)]), "html", null, true);
                echo "\" class=\"";
                if ((isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new RuntimeError('Variable "class" does not exist.', 111, $this->source); })())) {
                    echo twig_escape_filter($this->env, (isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new RuntimeError('Variable "class" does not exist.', 111, $this->source); })()), "html", null, true);
                } else {
                    echo "navbar-item";
                }
                echo "\" ";
                echo twig_escape_filter($this->env, (isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 111, $this->source); })()), "html", null, true);
                echo ">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["node"]) || array_key_exists("node", $context) ? $context["node"] : (function () { throw new RuntimeError('Variable "node" does not exist.', 111, $this->source); })()), "translation", [], "any", false, false, false, 111), "title", [], "any", false, false, false, 111), "html", null, true);
                echo "</a>
    ";
            }
            
            $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);


            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "_menu_display.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  480 => 111,  464 => 109,  461 => 108,  459 => 107,  444 => 106,  441 => 105,  423 => 104,  409 => 101,  406 => 100,  403 => 99,  387 => 98,  373 => 95,  370 => 94,  367 => 93,  351 => 92,  335 => 88,  319 => 74,  313 => 73,  309 => 71,  300 => 69,  296 => 68,  291 => 67,  288 => 66,  284 => 65,  280 => 63,  269 => 59,  263 => 58,  259 => 56,  253 => 55,  251 => 54,  246 => 51,  243 => 50,  240 => 49,  237 => 48,  234 => 47,  231 => 46,  228 => 45,  226 => 44,  223 => 43,  220 => 42,  215 => 41,  211 => 39,  200 => 35,  194 => 34,  190 => 32,  184 => 31,  182 => 30,  177 => 27,  174 => 26,  171 => 25,  168 => 24,  165 => 23,  162 => 22,  159 => 21,  157 => 20,  154 => 19,  151 => 18,  146 => 17,  135 => 9,  131 => 7,  129 => 6,  124 => 4,  121 => 3,  118 => 2,  102 => 1,  93 => 130,  90 => 129,  84 => 128,  78 => 126,  75 => 125,  70 => 124,  68 => 123,  62 => 121,  60 => 120,  54 => 116,  52 => 115,  49 => 114,  46 => 103,  43 => 97,  40 => 91,);
    }

    public function getSourceContext()
    {
        return new Source("{% macro node_item(node) %}
    {% import _self as macros %}

    {{ omni_sylius_instantiate_node_relations(node) }}

    {% if node.children|length > 0 %}
        <span class=\"dropdown dropdown--position\">
            <a class=\"header-primary\" href=\"#\" role=\"button\" data-toggle=\"dropdown\">
                {{ node.translation.title }}
            </a>
            <div class=\"dropdown-wrapper\">
                <div class=\"container dropdown-menu top-main-nav-dropdown\">
                    <div class=\"row\">
                        <div class=\"col-6\">
                            <div class=\"row nav-items-wrapper\">
                                <div class=\"top-main-nav-dropdown-top pr-0 col-6\">
                                    {% for key, subnode in node.children if key is even %}
                                        {% set link = '#' %}

                                        {% if subnode.link %}
                                            {% set link = subnode.link %}
                                        {% elseif subnode.relation and subnode.relation.slug %}
                                            {% if subnode.relationType == 'sylius.taxon' %}
                                                {% set link = path('sylius_shop_product_index', { slug: subnode.relation.slug }) %}
                                            {% endif %}
                                        {% endif %}

                                        <div class=\"image-src\"
                                             data-img=\"
                                            {% if subnode.images|length > 0 %}
                                                {{ subnode.images.first.path|imagine_filter('omni_sylius_banner') }}
                                            {% endif %}\"
                                        >
                                            <a href=\"{{ link }}\" class=\"top-main-nav-item js-show-image\" data-node=\"{{ subnode.code }}\">
                                                {{ subnode.translation.title }}
                                            </a>
                                        </div>
                                    {% endfor %}
                                </div>
                                <div class=\"top-main-nav-dropdown-top pr-0 col-6\">
                                    {% for key, subnode in node.children if key is odd %}
                                        {% set link = '#' %}

                                        {% if subnode.link %}
                                            {% set link = subnode.link %}
                                        {% elseif subnode.relation and subnode.relation.slug %}
                                            {% if subnode.relationType == 'sylius.taxon' %}
                                                {% set link = path('sylius_shop_product_index', { slug: subnode.relation.slug }) %}
                                            {% endif %}
                                        {% endif %}

                                        <div class=\"image-src\"
                                             data-img=\"
                                            {% if subnode.images.first|length > 0 %}
                                                {{ subnode.images.first.path|imagine_filter('omni_sylius_banner') }}
                                            {% endif %}\"
                                        >
                                            <a href=\"{{ link }}\" class=\"top-main-nav-item js-show-image\" data-node=\"{{ subnode.code }}\">
                                                {{ subnode.translation.title }}
                                            </a>
                                        </div>
                                    {% endfor %}
                                </div>
                                <div class=\"top-main-nav-dropdown-inner col-6 pl-0 pr-0\">
                                    {% for child in node.children %}
                                        {% if child.children|length > 0 %}
                                            <div class=\"nav-right-container hidden\" data-node=\"{{ child.code }}\">
                                                {% for subchild in child.children %}
                                                    {{ macros.inner_node_inner(subchild) }}
                                                {% endfor %}
                                            </div>
                                        {% endif %}
                                    {% endfor %}
                                </div>

                            </div>
                        </div>
                        <div class=\"col-6\">
                            <div class=\"text-center px-0\">
                                <img src id=\"nav-item-image\" class=\"item-image\">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </span>
    {% else %}
        {{ macros.render_simple_node(node) }}
    {% endif %}
{% endmacro %}

{% macro inner_node_top(node) %}
    {% import _self as macros %}

    {{ macros.render_simple_node(node, 'top-main-nav-item', 'data-node=' ~ node.code) }}
{% endmacro %}

{% macro inner_node_inner(node) %}
    {% import _self as macros %}

    {{ macros.render_simple_node(node, 'inner-main-nav-item pl-3') }}
{% endmacro %}

{% macro render_simple_node(node, class = '', data = '') %}
    {% if omni_sylius_is_node_linkable(node) %}
        <a href=\"{{ node.link }}\" class=\"{% if class %}{{ class }}{% else %}navbar-item{% endif %}\" {{ data }}>{{ node.translation.title }}</a>
    {% elseif omni_sylius_is_node_related(node) %}
        {% set relation = omni_sylius_get_relation(node) %}
        <a href=\"{{ path('sylius_shop_product_index', {'slug': relation.slug, '_locale': relation.translation.locale}) }}\" class=\"{% if class %}{{ class }}{% else %}navbar-item{% endif %}\" {{ data }}>{{ omni_sylius_get_relation_label(relation) }}</a>
    {% else %}
        <a href=\"{{ path('omni_sylius_cms_frontend_show', {'slug': node.translation.slug, '_locale': node.translation.locale}) }}\" class=\"{% if class %}{{ class }}{% else %}navbar-item{% endif %}\" {{ data }}>{{ node.translation.title }}</a>
    {% endif %}
{% endmacro %}

{% import _self as macros %}


<nav class=\"detatched-left top-navbar\">
    <div class=\"\">
        {% set headerNode = omni_sylius_get_node_by_type('main_menu') %}
        {{ omni_sylius_instantiate_node_relations(headerNode) }}

        {% if null != headerNode %}
            {% for node in headerNode.children %}
                {% if node.enabled%}
                    {{ macros.node_item(node) }}
                {% endif %}
            {% endfor %}
        {% endif %}
    </div>
</nav>
", "_menu_display.html.twig", "/var/www/html/templates/_menu_display.html.twig");
    }
}
