<?php
include_once \dirname(__DIR__, 4).'/vendor/sylius/grid-bundle/src/Bundle/Templating/Helper/BulkActionGridHelper.php';

class BulkActionGridHelper_d18a6a1 extends \Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera8ec9 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer1f165 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties11497 = [
        
    ];

    public function renderBulkAction(\Sylius\Component\Grid\View\GridView $gridView, \Sylius\Component\Grid\Definition\Action $bulkAction, $data = null) : string
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'renderBulkAction', array('gridView' => $gridView, 'bulkAction' => $bulkAction, 'data' => $data), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->renderBulkAction($gridView, $bulkAction, $data);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper $instance) {
            unset($instance->bulkActionGridRenderer);
        }, $instance, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper')->__invoke($instance);

        $instance->initializer1f165 = $initializer;

        return $instance;
    }

    public function __construct(\Sylius\Component\Grid\Renderer\BulkActionGridRendererInterface $bulkActionGridRenderer)
    {
        static $reflection;

        if (! $this->valueHoldera8ec9) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper');
            $this->valueHoldera8ec9 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper $instance) {
            unset($instance->bulkActionGridRenderer);
        }, $this, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper')->__invoke($this);

        }

        $this->valueHoldera8ec9->__construct($bulkActionGridRenderer);
    }

    public function & __get($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__get', ['name' => $name], $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        if (isset(self::$publicProperties11497[$name])) {
            return $this->valueHoldera8ec9->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__isset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__unset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__clone', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $this->valueHoldera8ec9 = clone $this->valueHoldera8ec9;
    }

    public function __sleep()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__sleep', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return array('valueHoldera8ec9');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Sylius\Bundle\GridBundle\Templating\Helper\BulkActionGridHelper $instance) {
            unset($instance->bulkActionGridRenderer);
        }, $this, 'Sylius\\Bundle\\GridBundle\\Templating\\Helper\\BulkActionGridHelper')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1f165 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1f165;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'initializeProxy', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera8ec9;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHoldera8ec9;
    }


}
