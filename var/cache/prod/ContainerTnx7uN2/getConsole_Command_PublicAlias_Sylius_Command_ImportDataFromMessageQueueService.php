<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'console.command.public_alias.sylius.command.import_data_from_message_queue' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/console/Command/Command.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ContainerAwareTrait.php';
include_once \dirname(__DIR__, 4).'/vendor/friendsofsylius/sylius-import-export-plugin/src/Command/ImportDataFromMessageQueueCommand.php';

$this->services['console.command.public_alias.sylius.command.import_data_from_message_queue'] = $instance = new \FriendsOfSylius\SyliusImportExportPlugin\Command\ImportDataFromMessageQueueCommand(($this->services['sylius.importers_registry'] ?? $this->load('getSylius_ImportersRegistryService.php')));

$instance->setContainer($this);

return $instance;
