<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.grid.view_factory' shared service.

include_once \dirname(__DIR__, 4).'/vendor/sylius/grid-bundle/src/Component/View/GridViewFactoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/grid-bundle/src/Component/View/GridViewFactory.php';

return $this->services['sylius.grid.view_factory'] = new \Sylius\Component\Grid\View\GridViewFactory(($this->privates['sylius.grid.data_provider'] ?? $this->load('getSylius_Grid_DataProviderService.php')));
