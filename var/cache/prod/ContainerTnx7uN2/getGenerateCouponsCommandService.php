<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Sylius\Bundle\PromotionBundle\Command\GenerateCouponsCommand' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/console/Command/Command.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Command/GenerateCouponsCommand.php';

$this->services['Sylius\\Bundle\\PromotionBundle\\Command\\GenerateCouponsCommand'] = $instance = new \Sylius\Bundle\PromotionBundle\Command\GenerateCouponsCommand(($this->services['sylius.repository.promotion'] ?? $this->load('getSylius_Repository_PromotionService.php')), ($this->services['sylius.promotion_coupon_generator'] ?? $this->load('getSylius_PromotionCouponGeneratorService.php')));

$instance->setName('sylius:promotion:generate-coupons');

return $instance;
