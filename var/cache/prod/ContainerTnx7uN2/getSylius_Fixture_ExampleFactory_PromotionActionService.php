<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.fixture.example_factory.promotion_action' shared service.

include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Fixture/Factory/ExampleFactoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Fixture/Factory/AbstractExampleFactory.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Fixture/Factory/PromotionActionExampleFactory.php';

return $this->services['sylius.fixture.example_factory.promotion_action'] = new \Sylius\Bundle\CoreBundle\Fixture\Factory\PromotionActionExampleFactory(($this->services['Sylius\\Component\\Core\\Factory\\PromotionActionFactoryInterface'] ?? $this->load('getPromotionActionFactoryInterfaceService.php')));
