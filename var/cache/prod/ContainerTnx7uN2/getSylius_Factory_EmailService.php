<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.factory.email' shared service.

include_once \dirname(__DIR__, 4).'/vendor/sylius/mailer-bundle/src/Component/Factory/EmailFactoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/mailer-bundle/src/Component/Factory/EmailFactory.php';

return $this->services['sylius.factory.email'] = new \Sylius\Component\Mailer\Factory\EmailFactory();
