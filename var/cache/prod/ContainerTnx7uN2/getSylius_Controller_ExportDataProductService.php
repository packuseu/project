<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.controller.export_data_product' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ContainerAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ContainerAwareTrait.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/ControllerTrait.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/Controller.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Controller/ResourceController.php';
include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-import-plugin/src/Controller/ExportJobController.php';

return $this->services['sylius.controller.export_data_product'] = new \Omni\Sylius\ImportPlugin\Controller\ExportJobController(($this->services['sylius.exporters_registry'] ?? $this->load('getSylius_ExportersRegistryService.php')), ($this->privates['sylius.resource_controller.request_configuration_factory'] ?? $this->load('getSylius_ResourceController_RequestConfigurationFactoryService.php')), ($this->privates['sylius.resource_controller.resources_collection_provider'] ?? $this->load('getSylius_ResourceController_ResourcesCollectionProviderService.php')), ($this->services['sylius.repository.product'] ?? $this->load('getSylius_Repository_ProductService.php')), $this->parameters['sylius.resources']);
