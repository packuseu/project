<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'monolog.logger.integrations' shared service.

$this->services['monolog.logger.integrations'] = $instance = new \Symfony\Bridge\Monolog\Logger('integrations');

$instance->pushHandler(($this->privates['monolog.handler.console'] ?? $this->getMonolog_Handler_ConsoleService()));
$instance->pushHandler(($this->privates['monolog.handler.integrations'] ?? $this->load('getMonolog_Handler_IntegrationsService.php')));

return $instance;
