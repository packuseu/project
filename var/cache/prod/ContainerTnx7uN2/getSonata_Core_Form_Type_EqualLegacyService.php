<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sonata.core.form.type.equal_legacy' shared service.

@trigger_error('The "sonata.core.form.type.equal_legacy" service is deprecated since 3.19 and will be removed in 4.0. Use "sonata.form.type.equal" instead.', E_USER_DEPRECATED);

return $this->services['sonata.core.form.type.equal_legacy'] = new \Sonata\CoreBundle\Form\Type\EqualType(($this->services['lexik_translation.translator'] ?? $this->getLexikTranslation_TranslatorService()));
