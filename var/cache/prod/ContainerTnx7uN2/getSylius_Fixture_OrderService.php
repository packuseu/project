<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.fixture.order' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/config/Definition/ConfigurationInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/fixtures-bundle/src/Fixture/FixtureInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/fixtures-bundle/src/Fixture/AbstractFixture.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Fixture/OrderFixture.php';

return $this->services['sylius.fixture.order'] = new \Sylius\Bundle\CoreBundle\Fixture\OrderFixture(($this->services['sylius.factory.order'] ?? ($this->services['sylius.factory.order'] = new \Sylius\Component\Resource\Factory\Factory('App\\Entity\\Order\\Order'))), ($this->services['sylius.factory.cart_item'] ?? $this->load('getSylius_Factory_CartItemService.php')), ($this->services['sylius.order_item_quantity_modifier.limiting'] ?? $this->load('getSylius_OrderItemQuantityModifier_LimitingService.php')), ($this->services['doctrine.orm.default_entity_manager'] ?? $this->getDoctrine_Orm_DefaultEntityManagerService()), ($this->services['sylius.repository.channel'] ?? $this->getSylius_Repository_ChannelService()), ($this->services['sylius.repository.customer'] ?? $this->load('getSylius_Repository_CustomerService.php')), ($this->services['sylius.repository.product'] ?? $this->load('getSylius_Repository_ProductService.php')), ($this->services['sylius.repository.country'] ?? $this->load('getSylius_Repository_CountryService.php')), ($this->services['sylius.repository.payment_method'] ?? $this->load('getSylius_Repository_PaymentMethodService.php')), ($this->services['sylius.repository.shipping_method'] ?? $this->load('getSylius_Repository_ShippingMethodService.php')), ($this->services['sylius.factory.address'] ?? $this->load('getSylius_Factory_AddressService.php')), ($this->services['sm.factory'] ?? $this->getSm_FactoryService()), ($this->services['sylius.checker.order_shipping_method_selection_requirement'] ?? $this->load('getSylius_Checker_OrderShippingMethodSelectionRequirementService.php')), ($this->services['sylius.checker.order_payment_method_selection_requirement'] ?? $this->load('getSylius_Checker_OrderPaymentMethodSelectionRequirementService.php')), ($this->services['sylius.fixture.example_factory.order'] ?? $this->load('getSylius_Fixture_ExampleFactory_OrderService.php')));
