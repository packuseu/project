<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'sylius.theme.translation.theme_aware_translator' shared service.

include_once \dirname(__DIR__, 4).'/vendor/sylius/theme-bundle/src/Translation/ThemeAwareTranslator.php';

return $this->privates['sylius.theme.translation.theme_aware_translator'] = new \Sylius\Bundle\ThemeBundle\Translation\ThemeAwareTranslator(($this->privates['sylius.theme.translation.theme_aware_translator.inner'] ?? $this->load('getSylius_Theme_Translation_ThemeAwareTranslator_InnerService.php')), ($this->privates['sylius.theme.context.channel_based'] ?? $this->load('getSylius_Theme_Context_ChannelBasedService.php')));
