<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'omni_sylius_cms.response_resolver.content' shared service.

include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/ResponseResolver/ResponseResolverInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/ResponseResolver/ContentResponseResolver.php';

return $this->privates['omni_sylius_cms.response_resolver.content'] = new \Omni\Sylius\CmsPlugin\ResponseResolver\ContentResponseResolver(($this->privates['omni_sylius.manager.node_type'] ?? $this->load('getOmniSylius_Manager_NodeTypeService.php')), ($this->services['router'] ?? $this->getRouterService()), ($this->services['kernel'] ?? $this->get('kernel', 1)));
