<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.registry.shipping_methods_resolver' shared service.

include_once \dirname(__DIR__, 4).'/vendor/sylius/registry/src/PrioritizedServiceRegistryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/registry/src/PrioritizedServiceRegistry.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Shipping/Resolver/ShippingMethodsResolverInterface.php';
include_once \dirname(__DIR__, 4).'/src/Shipping/ZoneAndChannelBasedShippingMethodsResolver.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Shipping/Checker/ShippingMethodEligibilityCheckerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Shipping/Checker/ShippingMethodEligibilityChecker.php';

$this->services['sylius.registry.shipping_methods_resolver'] = $instance = new \Sylius\Component\Registry\PrioritizedServiceRegistry('Sylius\\Component\\Shipping\\Resolver\\ShippingMethodsResolverInterface', 'Shipping methods resolver');

$instance->register(new \App\Shipping\ZoneAndChannelBasedShippingMethodsResolver(($this->services['sylius.repository.shipping_method'] ?? $this->load('getSylius_Repository_ShippingMethodService.php')), ($this->services['sylius.zone_matcher'] ?? $this->load('getSylius_ZoneMatcherService.php')), ($this->services['sylius.shipping_eligibility_checker'] ?? ($this->services['sylius.shipping_eligibility_checker'] = new \Sylius\Component\Shipping\Checker\ShippingMethodEligibilityChecker()))), 1);
$instance->register(($this->services['sylius.shipping_methods_resolver.default'] ?? $this->load('getSylius_ShippingMethodsResolver_DefaultService.php')), 0);

return $instance;
