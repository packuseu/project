<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.form.type.payment_method_choice' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Form/Type/PaymentMethodChoiceType.php';

return $this->services['sylius.form.type.payment_method_choice'] = new \Sylius\Bundle\PaymentBundle\Form\Type\PaymentMethodChoiceType(($this->services['sylius.payment_methods_resolver'] ?? $this->load('getSylius_PaymentMethodsResolverService.php')), ($this->services['sylius.repository.payment_method'] ?? $this->load('getSylius_Repository_PaymentMethodService.php')));
