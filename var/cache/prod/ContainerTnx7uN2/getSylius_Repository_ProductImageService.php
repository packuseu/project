<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'sylius.repository.product_image' shared service.

include_once \dirname(__DIR__, 4).'/vendor/friendsofsylius/sylius-import-export-plugin/src/Repository/ProductImageRepositoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/friendsofsylius/sylius-import-export-plugin/src/Repository/ProductImageImageRepository.php';

$a = ($this->services['doctrine.orm.default_entity_manager'] ?? $this->getDoctrine_Orm_DefaultEntityManagerService());

return $this->privates['sylius.repository.product_image'] = new \FriendsOfSylius\SyliusImportExportPlugin\Repository\ProductImageImageRepository($a, $a->getClassMetadata('Sylius\\Component\\Core\\Model\\ProductImage'));
