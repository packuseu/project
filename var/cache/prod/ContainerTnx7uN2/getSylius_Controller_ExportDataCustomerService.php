<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.controller.export_data_customer' shared service.

include_once \dirname(__DIR__, 4).'/vendor/friendsofsylius/sylius-import-export-plugin/src/Controller/ExportDataController.php';

return $this->services['sylius.controller.export_data_customer'] = new \FriendsOfSylius\SyliusImportExportPlugin\Controller\ExportDataController(($this->services['sylius.exporters_registry'] ?? $this->load('getSylius_ExportersRegistryService.php')), ($this->privates['sylius.resource_controller.request_configuration_factory'] ?? $this->load('getSylius_ResourceController_RequestConfigurationFactoryService.php')), ($this->privates['sylius.resource_controller.resources_collection_provider'] ?? $this->load('getSylius_ResourceController_ResourcesCollectionProviderService.php')), ($this->services['sylius.repository.customer'] ?? $this->load('getSylius_Repository_CustomerService.php')), $this->parameters['sylius.resources']);
