<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.errored..service_locator.VNuxZe2.Sylius\Component\Core\Model\ImageInterface' shared service.

$this->throw('Cannot autowire service ".service_locator.VNuxZe2": it references interface "Sylius\\Component\\Core\\Model\\ImageInterface" but no such service exists. Did you create a class that implements this interface?');
