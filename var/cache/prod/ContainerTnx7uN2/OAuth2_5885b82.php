<?php
include_once \dirname(__DIR__, 4).'/vendor/friendsofsymfony/oauth2-php/lib/IOAuth2.php';
include_once \dirname(__DIR__, 4).'/vendor/friendsofsymfony/oauth2-php/lib/OAuth2.php';

class OAuth2_5885b82 extends \OAuth2\OAuth2 implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \OAuth2\OAuth2|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera8ec9 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer1f165 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties11497 = [
        
    ];

    public function getVariable($name, $default = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getVariable', array('name' => $name, 'default' => $default), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getVariable($name, $default);
    }

    public function setVariable($name, $value)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'setVariable', array('name' => $name, 'value' => $value), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->setVariable($name, $value);
    }

    public function verifyAccessToken($tokenParam, $scope = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'verifyAccessToken', array('tokenParam' => $tokenParam, 'scope' => $scope), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->verifyAccessToken($tokenParam, $scope);
    }

    public function getBearerToken(?\Symfony\Component\HttpFoundation\Request $request = null, $removeFromRequest = false)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getBearerToken', array('request' => $request, 'removeFromRequest' => $removeFromRequest), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getBearerToken($request, $removeFromRequest);
    }

    public function grantAccessToken(?\Symfony\Component\HttpFoundation\Request $request = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'grantAccessToken', array('request' => $request), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->grantAccessToken($request);
    }

    public function finishClientAuthorization($isAuthorized, $data = null, ?\Symfony\Component\HttpFoundation\Request $request = null, $scope = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'finishClientAuthorization', array('isAuthorized' => $isAuthorized, 'data' => $data, 'request' => $request, 'scope' => $scope), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->finishClientAuthorization($isAuthorized, $data, $request, $scope);
    }

    public function createAccessToken(\OAuth2\Model\IOAuth2Client $client, $data, $scope = null, $access_token_lifetime = null, $issue_refresh_token = true, $refresh_token_lifetime = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'createAccessToken', array('client' => $client, 'data' => $data, 'scope' => $scope, 'access_token_lifetime' => $access_token_lifetime, 'issue_refresh_token' => $issue_refresh_token, 'refresh_token_lifetime' => $refresh_token_lifetime), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->createAccessToken($client, $data, $scope, $access_token_lifetime, $issue_refresh_token, $refresh_token_lifetime);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        unset($instance->conf, $instance->storage, $instance->oldRefreshToken, $instance->usedAuthCode);

        $instance->initializer1f165 = $initializer;

        return $instance;
    }

    public function __construct(\OAuth2\IOAuth2Storage $storage, $config = [])
    {
        static $reflection;

        if (! $this->valueHoldera8ec9) {
            $reflection = $reflection ?? new \ReflectionClass('OAuth2\\OAuth2');
            $this->valueHoldera8ec9 = $reflection->newInstanceWithoutConstructor();
        unset($this->conf, $this->storage, $this->oldRefreshToken, $this->usedAuthCode);

        }

        $this->valueHoldera8ec9->__construct($storage, $config);
    }

    public function & __get($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__get', ['name' => $name], $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        if (isset(self::$publicProperties11497[$name])) {
            return $this->valueHoldera8ec9->$name;
        }

        $realInstanceReflection = new \ReflectionClass('OAuth2\\OAuth2');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('OAuth2\\OAuth2');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__isset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('OAuth2\\OAuth2');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__unset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('OAuth2\\OAuth2');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__clone', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $this->valueHoldera8ec9 = clone $this->valueHoldera8ec9;
    }

    public function __sleep()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__sleep', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return array('valueHoldera8ec9');
    }

    public function __wakeup()
    {
        unset($this->conf, $this->storage, $this->oldRefreshToken, $this->usedAuthCode);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1f165 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1f165;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'initializeProxy', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera8ec9;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHoldera8ec9;
    }


}
