<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'App\Sitemap\Provider\NodeUrlProvider' shared autowired service.

include_once \dirname(__DIR__, 4).'/vendor/stefandoorn/sitemap-plugin/src/Provider/UrlProviderInterface.php';
include_once \dirname(__DIR__, 4).'/src/Sitemap/Provider/NodeUrlProvider.php';
include_once \dirname(__DIR__, 4).'/vendor/stefandoorn/sitemap-plugin/src/Factory/SitemapUrlFactoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/stefandoorn/sitemap-plugin/src/Factory/SitemapUrlFactory.php';

return $this->privates['App\\Sitemap\\Provider\\NodeUrlProvider'] = new \App\Sitemap\Provider\NodeUrlProvider(($this->services['omni_sylius.repository.node'] ?? $this->load('getOmniSylius_Repository_NodeService.php')), ($this->services['router'] ?? $this->getRouterService()), ($this->privates['sylius.sitemap_url_factory'] ?? ($this->privates['sylius.sitemap_url_factory'] = new \SitemapPlugin\Factory\SitemapUrlFactory())), ($this->services['Sylius\\Component\\Locale\\Context\\LocaleContextInterface'] ?? $this->getLocaleContextInterfaceService()), ($this->privates['omni_sylius_cms.url_resolver'] ?? $this->load('getOmniSyliusCms_UrlResolverService.php')));
