<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'sylius.theme.form.type.theme_name_choice' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/theme-bundle/src/Form/Type/ThemeNameChoiceType.php';

return $this->privates['sylius.theme.form.type.theme_name_choice'] = new \Sylius\Bundle\ThemeBundle\Form\Type\ThemeNameChoiceType(($this->services['sylius.repository.theme'] ?? $this->load('getSylius_Repository_ThemeService.php')));
