<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.form.type.product_attribute' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Form/Type/AbstractResourceType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AttributeBundle/Form/Type/AttributeType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle/Form/Type/ProductAttributeType.php';

return $this->services['sylius.form.type.product_attribute'] = new \Sylius\Bundle\ProductBundle\Form\Type\ProductAttributeType('App\\Entity\\Product\\ProductAttribute', $this->parameters['sylius.form.type.product_attribute.validation_groups'], 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeTranslationType', ($this->services['sylius.form_registry.attribute_type'] ?? $this->load('getSylius_FormRegistry_AttributeTypeService.php')));
