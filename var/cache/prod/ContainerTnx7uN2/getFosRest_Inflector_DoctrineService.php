<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'fos_rest.inflector.doctrine' shared service.

@trigger_error('The fos_rest.inflector.doctrine service is deprecated since FOSRestBundle 2.8.', E_USER_DEPRECATED);

return new \FOS\RestBundle\Inflector\DoctrineInflector();
