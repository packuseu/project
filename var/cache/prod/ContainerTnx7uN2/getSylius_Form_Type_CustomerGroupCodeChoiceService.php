<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.form.type.customer_group_code_choice' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Form/Type/CustomerGroupCodeChoiceType.php';

return $this->services['sylius.form.type.customer_group_code_choice'] = new \Sylius\Bundle\CustomerBundle\Form\Type\CustomerGroupCodeChoiceType(($this->services['sylius.repository.customer_group'] ?? $this->load('getSylius_Repository_CustomerGroupService.php')));
