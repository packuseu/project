<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'omni_sylius_banner.form_type.banner_position' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Form/Type/AbstractResourceType.php';
include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-banner-plugin/src/Form/Type/BannerPositionType.php';
include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-banner-plugin/src/Form/Subscriber/AddZoneSubscriber.php';

return $this->privates['omni_sylius_banner.form_type.banner_position'] = new \Omni\Sylius\BannerPlugin\Form\Type\BannerPositionType('Omni\\Sylius\\BannerPlugin\\Model\\BannerPosition', $this->parameters['omni_banner.form.type.banner_zone.validation_groups'], new \Omni\Sylius\BannerPlugin\Form\Subscriber\AddZoneSubscriber(), 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZone', ($this->privates['omni_banner.manager.position'] ?? $this->load('getOmniBanner_Manager_PositionService.php')), ($this->services['lexik_translation.translator'] ?? $this->getLexikTranslation_TranslatorService()));
