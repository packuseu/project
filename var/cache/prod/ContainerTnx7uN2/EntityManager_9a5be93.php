<?php
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera8ec9 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer1f165 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties11497 = [
        
    ];

    public function getConnection()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getConnection', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getMetadataFactory', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getExpressionBuilder', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'beginTransaction', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getCache', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getCache();
    }

    public function transactional($func)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'transactional', array('func' => $func), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->transactional($func);
    }

    public function commit()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'commit', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->commit();
    }

    public function rollback()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'rollback', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getClassMetadata', array('className' => $className), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'createQuery', array('dql' => $dql), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'createNamedQuery', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'createQueryBuilder', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'flush', array('entity' => $entity), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'clear', array('entityName' => $entityName), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->clear($entityName);
    }

    public function close()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'close', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->close();
    }

    public function persist($entity)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'persist', array('entity' => $entity), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'remove', array('entity' => $entity), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'refresh', array('entity' => $entity), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'detach', array('entity' => $entity), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'merge', array('entity' => $entity), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getRepository', array('entityName' => $entityName), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'contains', array('entity' => $entity), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getEventManager', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getConfiguration', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'isOpen', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getUnitOfWork', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getProxyFactory', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'initializeObject', array('obj' => $obj), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getFilters', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'isFiltersStateClean', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'hasFilters', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer1f165 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHoldera8ec9) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHoldera8ec9 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHoldera8ec9->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__get', ['name' => $name], $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        if (isset(self::$publicProperties11497[$name])) {
            return $this->valueHoldera8ec9->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__isset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__unset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__clone', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $this->valueHoldera8ec9 = clone $this->valueHoldera8ec9;
    }

    public function __sleep()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__sleep', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return array('valueHoldera8ec9');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1f165 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1f165;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'initializeProxy', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera8ec9;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHoldera8ec9;
    }


}
