<?php
include_once \dirname(__DIR__, 4).'/vendor/symfony/templating/Helper/HelperInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/templating/Helper/Helper.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Templating/Helper/CheckoutStepsHelper.php';

class CheckoutStepsHelper_87a88b2 extends \Sylius\Bundle\CoreBundle\Templating\Helper\CheckoutStepsHelper implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Sylius\Bundle\CoreBundle\Templating\Helper\CheckoutStepsHelper|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera8ec9 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer1f165 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties11497 = [
        
    ];

    public function isShippingRequired(\Sylius\Component\Core\Model\OrderInterface $order) : bool
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'isShippingRequired', array('order' => $order), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->isShippingRequired($order);
    }

    public function isPaymentRequired(\Sylius\Component\Core\Model\OrderInterface $order) : bool
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'isPaymentRequired', array('order' => $order), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->isPaymentRequired($order);
    }

    public function getName() : string
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getName', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getName();
    }

    public function setCharset($charset)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'setCharset', array('charset' => $charset), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->setCharset($charset);
    }

    public function getCharset()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'getCharset', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->getCharset();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        unset($instance->charset);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\Templating\Helper\CheckoutStepsHelper $instance) {
            unset($instance->orderPaymentMethodSelectionRequirementChecker, $instance->orderShippingMethodSelectionRequirementChecker);
        }, $instance, 'Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper')->__invoke($instance);

        $instance->initializer1f165 = $initializer;

        return $instance;
    }

    public function __construct(\Sylius\Component\Core\Checker\OrderPaymentMethodSelectionRequirementCheckerInterface $orderPaymentMethodSelectionRequirementChecker, \Sylius\Component\Core\Checker\OrderShippingMethodSelectionRequirementCheckerInterface $orderShippingMethodSelectionRequirementChecker)
    {
        static $reflection;

        if (! $this->valueHoldera8ec9) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper');
            $this->valueHoldera8ec9 = $reflection->newInstanceWithoutConstructor();
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\Templating\Helper\CheckoutStepsHelper $instance) {
            unset($instance->orderPaymentMethodSelectionRequirementChecker, $instance->orderShippingMethodSelectionRequirementChecker);
        }, $this, 'Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper')->__invoke($this);

        }

        $this->valueHoldera8ec9->__construct($orderPaymentMethodSelectionRequirementChecker, $orderShippingMethodSelectionRequirementChecker);
    }

    public function & __get($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__get', ['name' => $name], $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        if (isset(self::$publicProperties11497[$name])) {
            return $this->valueHoldera8ec9->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__isset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__unset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__clone', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $this->valueHoldera8ec9 = clone $this->valueHoldera8ec9;
    }

    public function __sleep()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__sleep', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return array('valueHoldera8ec9');
    }

    public function __wakeup()
    {
        unset($this->charset);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\Templating\Helper\CheckoutStepsHelper $instance) {
            unset($instance->orderPaymentMethodSelectionRequirementChecker, $instance->orderShippingMethodSelectionRequirementChecker);
        }, $this, 'Sylius\\Bundle\\CoreBundle\\Templating\\Helper\\CheckoutStepsHelper')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1f165 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1f165;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'initializeProxy', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera8ec9;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHoldera8ec9;
    }


}
