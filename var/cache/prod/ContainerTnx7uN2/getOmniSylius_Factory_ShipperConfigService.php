<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'omni_sylius.factory.shipper_config' shared service.

return $this->services['omni_sylius.factory.shipper_config'] = new \Sylius\Component\Resource\Factory\Factory('App\\Entity\\Shipping\\ShipperConfig');
