<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'sylius.form.type.checkout_payment' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Form/Type/Checkout/PaymentType.php';

return $this->services['sylius.form.type.checkout_payment'] = new \Sylius\Bundle\CoreBundle\Form\Type\Checkout\PaymentType('App\\Entity\\Payment\\Payment', $this->parameters['sylius.form.type.checkout_payment.validation_groups']);
