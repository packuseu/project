<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'omni_sylius_cms.url_resolver.resource' shared service.

include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/UrlResolver/UrlResolverInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/UrlResolver/ResourceUrlResolver.php';

return $this->privates['omni_sylius_cms.url_resolver.resource'] = new \Omni\Sylius\CmsPlugin\UrlResolver\ResourceUrlResolver(($this->privates['omni_sylius.manager.node_type'] ?? $this->load('getOmniSylius_Manager_NodeTypeService.php')), ($this->privates['omni_sylius_cms.relation_url_generator'] ?? $this->load('getOmniSyliusCms_RelationUrlGeneratorService.php')), ($this->privates['monolog.logger'] ?? $this->load('getMonolog_LoggerService.php')));
