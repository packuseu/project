<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Sylius\Component\Core\Factory\PromotionRuleFactoryInterface' shared service.

include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Factory/PromotionRuleFactoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Factory/PromotionRuleFactory.php';

return $this->services['Sylius\\Component\\Core\\Factory\\PromotionRuleFactoryInterface'] = new \Sylius\Component\Core\Factory\PromotionRuleFactory(new \Sylius\Component\Resource\Factory\Factory('App\\Entity\\Promotion\\PromotionRule'));
