<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'brille24_tier_price.form.tier_price_type' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/brille24/sylius-tierprice-plugin/src/Form/TierPriceType.php';

return $this->privates['brille24_tier_price.form.tier_price_type'] = new \Brille24\SyliusTierPricePlugin\Form\TierPriceType(($this->services['sylius.repository.channel'] ?? $this->getSylius_Repository_ChannelService()));
