<?php

namespace ContainerTnx7uN2;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\FrozenParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * This class has been auto-generated
 * by the Symfony Dependency Injection Component.
 *
 * @final
 */
class srcApp_KernelProdDebugContainer extends Container
{
    private $buildParameters;
    private $containerDir;
    private $targetDir;
    private $parameters = [];
    private $getService;

    public function __construct(array $buildParameters = [], $containerDir = __DIR__)
    {
        $this->getService = \Closure::fromCallable([$this, 'getService']);
        $this->buildParameters = $buildParameters;
        $this->containerDir = $containerDir;
        $this->targetDir = \dirname($containerDir);
        $this->parameters = $this->getDefaultParameters();

        $this->services = $this->privates = [];
        $this->syntheticIds = [
            'kernel' => true,
        ];
        $this->methodMap = [
            'Sylius\\Bundle\\ChannelBundle\\Context\\FakeChannel\\FakeChannelCodeProviderInterface' => 'getFakeChannelCodeProviderInterfaceService',
            'Sylius\\Component\\Channel\\Context\\ChannelContextInterface' => 'getChannelContextInterfaceService',
            'Sylius\\Component\\Channel\\Context\\RequestBased\\RequestResolverInterface' => 'getRequestResolverInterfaceService',
            'Sylius\\Component\\Locale\\Context\\LocaleContextInterface' => 'getLocaleContextInterfaceService',
            'doctrine.dbal.default_connection' => 'getDoctrine_Dbal_DefaultConnectionService',
            'doctrine.orm.default_entity_manager' => 'getDoctrine_Orm_DefaultEntityManagerService',
            'event_dispatcher' => 'getEventDispatcherService',
            'http_kernel' => 'getHttpKernelService',
            'lexik_translation.translator' => 'getLexikTranslation_TranslatorService',
            'request_stack' => 'getRequestStackService',
            'router' => 'getRouterService',
            'security.authorization_checker' => 'getSecurity_AuthorizationCheckerService',
            'security.token_storage' => 'getSecurity_TokenStorageService',
            'session' => 'getSessionService',
            'sm.callback_factory' => 'getSm_CallbackFactoryService',
            'sm.factory' => 'getSm_FactoryService',
            'sylius.context.cart' => 'getSylius_Context_CartService',
            'sylius.context.cart.customer_and_channel_based' => 'getSylius_Context_Cart_CustomerAndChannelBasedService',
            'sylius.context.cart.new' => 'getSylius_Context_Cart_NewService',
            'sylius.context.cart.session_and_channel_based' => 'getSylius_Context_Cart_SessionAndChannelBasedService',
            'sylius.context.channel' => 'getSylius_Context_ChannelService',
            'sylius.context.channel.fake_channel.persister' => 'getSylius_Context_Channel_FakeChannel_PersisterService',
            'sylius.context.currency.channel_aware' => 'getSylius_Context_Currency_ChannelAwareService',
            'sylius.context.currency.storage_based' => 'getSylius_Context_Currency_StorageBasedService',
            'sylius.context.customer' => 'getSylius_Context_CustomerService',
            'sylius.context.locale.admin_based' => 'getSylius_Context_Locale_AdminBasedService',
            'sylius.context.locale.provider_based' => 'getSylius_Context_Locale_ProviderBasedService',
            'sylius.context.locale.request_based' => 'getSylius_Context_Locale_RequestBasedService',
            'sylius.context.shopper' => 'getSylius_Context_ShopperService',
            'sylius.factory.order' => 'getSylius_Factory_OrderService',
            'sylius.listener.non_channel_request_locale' => 'getSylius_Listener_NonChannelRequestLocaleService',
            'sylius.listener.request_locale_setter' => 'getSylius_Listener_RequestLocaleSetterService',
            'sylius.listener.session_cart' => 'getSylius_Listener_SessionCartService',
            'sylius.locale_provider.channel_based' => 'getSylius_LocaleProvider_ChannelBasedService',
            'sylius.repository.channel' => 'getSylius_Repository_ChannelService',
            'sylius.repository.order' => 'getSylius_Repository_OrderService',
            'sylius.storage.cart_session' => 'getSylius_Storage_CartSessionService',
            'sylius.storage.cookie' => 'getSylius_Storage_CookieService',
            'sylius.storage.currency' => 'getSylius_Storage_CurrencyService',
            'sylius.translator.listener' => 'getSylius_Translator_ListenerService',
            'sylius_resource.doctrine.mapping_driver_chain' => 'getSyliusResource_Doctrine_MappingDriverChainService',
            'sonata.core.flashmessage.manager' => 'getSonata_Core_Flashmessage_ManagerService',
            'sonata.core.flashmessage.twig.extension' => 'getSonata_Core_Flashmessage_Twig_ExtensionService',
        ];
        $this->fileMap = [
            'App\\Controller\\AppController' => 'getAppControllerService.php',
            'App\\Controller\\OrderController' => 'getOrderControllerService.php',
            'App\\Controller\\OrderItemController' => 'getOrderItemControllerService.php',
            'App\\Controller\\UploadedImagePathGenerator' => 'getUploadedImagePathGeneratorService.php',
            'App\\EventListener\\CartListener' => 'getCartListenerService.php',
            'App\\EventListener\\Payment\\OrderPaymentListener' => 'getOrderPaymentListenerService.php',
            'Liip\\ImagineBundle\\Controller\\ImagineController' => 'getImagineControllerService.php',
            'Omni\\Sylius\\SwedbankSpp\\Communication\\AbstractCommunication' => 'getAbstractCommunicationService.php',
            'Omni\\Sylius\\SwedbankSpp\\Doctrine\\ORM\\PaymentRepository' => 'getPaymentRepositoryService.php',
            'Omni\\Sylius\\SwedbankSpp\\Payum\\Action\\CreditCard\\CaptureAction' => 'getCaptureActionService.php',
            'Omni\\Sylius\\SwedbankSpp\\Payum\\Action\\CreditCard\\StatusAction' => 'getStatusActionService.php',
            'Omni\\Sylius\\TranslatorPlugin\\Controller\\TranslationController' => 'getTranslationControllerService.php',
            'Payum\\Bundle\\PayumBundle\\Controller\\AuthorizeController' => 'getAuthorizeControllerService.php',
            'Payum\\Bundle\\PayumBundle\\Controller\\CancelController' => 'getCancelControllerService.php',
            'Payum\\Bundle\\PayumBundle\\Controller\\CaptureController' => 'getCaptureControllerService.php',
            'Payum\\Bundle\\PayumBundle\\Controller\\NotifyController' => 'getNotifyControllerService.php',
            'Payum\\Bundle\\PayumBundle\\Controller\\PayoutController' => 'getPayoutControllerService.php',
            'Payum\\Bundle\\PayumBundle\\Controller\\RefundController' => 'getRefundControllerService.php',
            'Payum\\Bundle\\PayumBundle\\Controller\\SyncController' => 'getSyncControllerService.php',
            'Sylius\\Bundle\\AdminApiBundle\\Command\\CreateClientCommand' => 'getCreateClientCommandService.php',
            'Sylius\\Bundle\\AdminBundle\\Action\\RemoveAvatarAction' => 'getRemoveAvatarActionService.php',
            'Sylius\\Bundle\\AdminBundle\\Twig\\ChannelNameExtension' => 'getChannelNameExtensionService.php',
            'Sylius\\Bundle\\AdminBundle\\Twig\\OrderUnitTaxesExtension' => 'getOrderUnitTaxesExtensionService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\CancelUnpaidOrdersCommand' => 'getCancelUnpaidOrdersCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\CheckRequirementsCommand' => 'getCheckRequirementsCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\InformAboutGUSCommand' => 'getInformAboutGUSCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\InstallAssetsCommand' => 'getInstallAssetsCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\InstallCommand' => 'getInstallCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\InstallDatabaseCommand' => 'getInstallDatabaseCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\InstallSampleDataCommand' => 'getInstallSampleDataCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\SetupCommand' => 'getSetupCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Command\\ShowAvailablePluginsCommand' => 'getShowAvailablePluginsCommandService.php',
            'Sylius\\Bundle\\CoreBundle\\Security\\UserImpersonatorInterface' => 'getUserImpersonatorInterfaceService.php',
            'Sylius\\Bundle\\FixturesBundle\\Listener\\ListenerRegistryInterface' => 'getListenerRegistryInterfaceService.php',
            'Sylius\\Bundle\\FixturesBundle\\Suite\\SuiteFactoryInterface' => 'getSuiteFactoryInterfaceService.php',
            'Sylius\\Bundle\\OrderBundle\\Command\\RemoveExpiredCartsCommand' => 'getRemoveExpiredCartsCommandService.php',
            'Sylius\\Bundle\\PromotionBundle\\Command\\GenerateCouponsCommand' => 'getGenerateCouponsCommandService.php',
            'Sylius\\Bundle\\ThemeBundle\\Asset\\PathResolverInterface' => 'getPathResolverInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Command\\AssetsInstallCommand' => 'getAssetsInstallCommandService.php',
            'Sylius\\Bundle\\ThemeBundle\\Command\\ListCommand' => 'getListCommandService.php',
            'Sylius\\Bundle\\ThemeBundle\\Configuration\\ConfigurationProcessorInterface' => 'getConfigurationProcessorInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Factory\\FinderFactoryInterface' => 'getFinderFactoryInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Factory\\ThemeAuthorFactoryInterface' => 'getThemeAuthorFactoryInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Factory\\ThemeFactoryInterface' => 'getThemeFactoryInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Factory\\ThemeScreenshotFactoryInterface' => 'getThemeScreenshotFactoryInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Filesystem\\FilesystemInterface' => 'getFilesystemInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\HierarchyProvider\\ThemeHierarchyProviderInterface' => 'getThemeHierarchyProviderInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Loader\\CircularDependencyCheckerInterface' => 'getCircularDependencyCheckerInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Loader\\ThemeLoaderInterface' => 'getThemeLoaderInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Locator\\ResourceLocatorInterface' => 'getResourceLocatorInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Templating\\Locator\\TemplateLocatorInterface' => 'getTemplateLocatorInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Translation\\Finder\\TranslationFilesFinderInterface' => 'getTranslationFilesFinderInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Translation\\Provider\\Loader\\TranslatorLoaderProviderInterface' => 'getTranslatorLoaderProviderInterfaceService.php',
            'Sylius\\Bundle\\ThemeBundle\\Translation\\Provider\\Resource\\TranslatorResourceProviderInterface' => 'getTranslatorResourceProviderInterfaceService.php',
            'Sylius\\Bundle\\UserBundle\\Command\\DemoteUserCommand' => 'getDemoteUserCommandService.php',
            'Sylius\\Bundle\\UserBundle\\Command\\PromoteUserCommand' => 'getPromoteUserCommandService.php',
            'Sylius\\Component\\Addressing\\Factory\\ZoneFactoryInterface' => 'getZoneFactoryInterfaceService.php',
            'Sylius\\Component\\Channel\\Factory\\ChannelFactoryInterface' => 'getChannelFactoryInterfaceService.php',
            'Sylius\\Component\\Core\\Calculator\\ProductVariantPriceCalculatorInterface' => 'getProductVariantPriceCalculatorInterfaceService.php',
            'Sylius\\Component\\Core\\Factory\\PaymentMethodFactoryInterface' => 'getPaymentMethodFactoryInterfaceService.php',
            'Sylius\\Component\\Core\\Factory\\PromotionActionFactoryInterface' => 'getPromotionActionFactoryInterfaceService.php',
            'Sylius\\Component\\Core\\Factory\\PromotionRuleFactoryInterface' => 'getPromotionRuleFactoryInterfaceService.php',
            'Sylius\\Component\\Core\\Generator\\ImagePathGeneratorInterface' => 'getImagePathGeneratorInterfaceService.php',
            'Sylius\\Component\\Core\\Inventory\\Operator\\OrderInventoryOperatorInterface' => 'getOrderInventoryOperatorInterfaceService.php',
            'Sylius\\Component\\Order\\Factory\\AdjustmentFactoryInterface' => 'getAdjustmentFactoryInterfaceService.php',
            'Sylius\\Component\\Payment\\Factory\\PaymentFactoryInterface' => 'getPaymentFactoryInterfaceService.php',
            'Sylius\\Component\\Product\\Factory\\ProductFactoryInterface' => 'getProductFactoryInterfaceService.php',
            'Sylius\\Component\\Product\\Factory\\ProductVariantFactoryInterface' => 'getProductVariantFactoryInterfaceService.php',
            'Sylius\\Component\\Promotion\\Factory\\PromotionCouponFactoryInterface' => 'getPromotionCouponFactoryInterfaceService.php',
            'Sylius\\Component\\Taxonomy\\Factory\\TaxonFactoryInterface' => 'getTaxonFactoryInterfaceService.php',
            'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController' => 'getRedirectControllerService.php',
            'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController' => 'getTemplateControllerService.php',
            'app.controller.order_report' => 'getApp_Controller_OrderReportService.php',
            'app.factory.order_report' => 'getApp_Factory_OrderReportService.php',
            'app.repository.order_report' => 'getApp_Repository_OrderReportService.php',
            'bitbag.controller.shipping_export' => 'getBitbag_Controller_ShippingExportService.php',
            'bitbag.controller.shipping_gateway' => 'getBitbag_Controller_ShippingGatewayService.php',
            'bitbag.factory.shipping_export' => 'getBitbag_Factory_ShippingExportService.php',
            'bitbag.factory.shipping_gateway' => 'getBitbag_Factory_ShippingGatewayService.php',
            'bitbag.repository.shipping_export' => 'getBitbag_Repository_ShippingExportService.php',
            'bitbag.repository.shipping_gateway' => 'getBitbag_Repository_ShippingGatewayService.php',
            'brille24.controller.tierprice' => 'getBrille24_Controller_TierpriceService.php',
            'brille24.factory.tierprice' => 'getBrille24_Factory_TierpriceService.php',
            'brille24.repository.tierprice' => 'getBrille24_Repository_TierpriceService.php',
            'cache.app' => 'getCache_AppService.php',
            'cache.app_clearer' => 'getCache_AppClearerService.php',
            'cache.global_clearer' => 'getCache_GlobalClearerService.php',
            'cache.system' => 'getCache_SystemService.php',
            'cache.system_clearer' => 'getCache_SystemClearerService.php',
            'cache_clearer' => 'getCacheClearerService.php',
            'cache_warmer' => 'getCacheWarmerService.php',
            'console.command.public_alias.Lexik\\Bundle\\TranslationBundle\\Command\\ExportTranslationsCommand' => 'getExportTranslationsCommandService.php',
            'console.command.public_alias.Lexik\\Bundle\\TranslationBundle\\Command\\ImportTranslationsCommand' => 'getImportTranslationsCommandService.php',
            'console.command.public_alias.Omni\\Sylius\\SwedbankSpp\\Command\\UpdatePaymentStatusCommand' => 'getUpdatePaymentStatusCommandService.php',
            'console.command.public_alias.Sonata\\CoreBundle\\Command\\SonataDumpDoctrineMetaCommand' => 'getSonataDumpDoctrineMetaCommandService.php',
            'console.command.public_alias.Sonata\\CoreBundle\\Command\\SonataListFormMappingCommand' => 'getSonataListFormMappingCommandService.php',
            'console.command.public_alias.Sylius\\Bundle\\FixturesBundle\\Command\\FixturesListCommand' => 'getFixturesListCommandService.php',
            'console.command.public_alias.Sylius\\Bundle\\FixturesBundle\\Command\\FixturesLoadCommand' => 'getFixturesLoadCommandService.php',
            'console.command.public_alias.doctrine_cache.contains_command' => 'getConsole_Command_PublicAlias_DoctrineCache_ContainsCommandService.php',
            'console.command.public_alias.doctrine_cache.delete_command' => 'getConsole_Command_PublicAlias_DoctrineCache_DeleteCommandService.php',
            'console.command.public_alias.doctrine_cache.flush_command' => 'getConsole_Command_PublicAlias_DoctrineCache_FlushCommandService.php',
            'console.command.public_alias.doctrine_cache.stats_command' => 'getConsole_Command_PublicAlias_DoctrineCache_StatsCommandService.php',
            'console.command.public_alias.fos_oauth_server.clean_command' => 'getConsole_Command_PublicAlias_FosOauthServer_CleanCommandService.php',
            'console.command.public_alias.fos_oauth_server.create_client_command' => 'getConsole_Command_PublicAlias_FosOauthServer_CreateClientCommandService.php',
            'console.command.public_alias.sylius.command.export_data' => 'getConsole_Command_PublicAlias_Sylius_Command_ExportDataService.php',
            'console.command.public_alias.sylius.command.export_data_to_message_queue' => 'getConsole_Command_PublicAlias_Sylius_Command_ExportDataToMessageQueueService.php',
            'console.command.public_alias.sylius.command.import_data' => 'getConsole_Command_PublicAlias_Sylius_Command_ImportDataService.php',
            'console.command.public_alias.sylius.command.import_data_from_message_queue' => 'getConsole_Command_PublicAlias_Sylius_Command_ImportDataFromMessageQueueService.php',
            'console.command_loader' => 'getConsole_CommandLoaderService.php',
            'container.env_var_processors_locator' => 'getContainer_EnvVarProcessorsLocatorService.php',
            'doctrine' => 'getDoctrineService.php',
            'error_controller' => 'getErrorControllerService.php',
            'filesystem' => 'getFilesystemService.php',
            'form.factory' => 'getForm_FactoryService.php',
            'form.type.file' => 'getForm_Type_FileService.php',
            'fos_oauth_server.controller.authorize' => 'getFosOauthServer_Controller_AuthorizeService.php',
            'fos_oauth_server.controller.token' => 'getFosOauthServer_Controller_TokenService.php',
            'fos_rest.exception.controller' => 'getFosRest_Exception_ControllerService.php',
            'fos_rest.exception.twig_controller' => 'getFosRest_Exception_TwigControllerService.php',
            'fos_rest.serializer.jms_handler_registry' => 'getFosRest_Serializer_JmsHandlerRegistryService.php',
            'fos_rest.view_handler' => 'getFosRest_ViewHandlerService.php',
            'gaufrette.sylius_image_filesystem' => 'getGaufrette_SyliusImageFilesystemService.php',
            'hateoas.event_subscriber.json' => 'getHateoas_EventSubscriber_JsonService.php',
            'hateoas.event_subscriber.xml' => 'getHateoas_EventSubscriber_XmlService.php',
            'hateoas.expression.link' => 'getHateoas_Expression_LinkService.php',
            'hateoas.generator.registry' => 'getHateoas_Generator_RegistryService.php',
            'hateoas.helper.link' => 'getHateoas_Helper_LinkService.php',
            'httplug.client' => 'getHttplug_ClientService.php',
            'httplug.message_factory' => 'getHttplug_MessageFactoryService.php',
            'httplug.psr17_request_factory' => 'getHttplug_Psr17RequestFactoryService.php',
            'httplug.psr17_response_factory' => 'getHttplug_Psr17ResponseFactoryService.php',
            'httplug.psr17_server_request_factory' => 'getHttplug_Psr17ServerRequestFactoryService.php',
            'httplug.psr17_stream_factory' => 'getHttplug_Psr17StreamFactoryService.php',
            'httplug.psr17_uploaded_file_factory' => 'getHttplug_Psr17UploadedFileFactoryService.php',
            'httplug.psr17_uri_factory' => 'getHttplug_Psr17UriFactoryService.php',
            'httplug.psr18_client' => 'getHttplug_Psr18ClientService.php',
            'httplug.stream_factory' => 'getHttplug_StreamFactoryService.php',
            'httplug.uri_factory' => 'getHttplug_UriFactoryService.php',
            'hwi_oauth.http_client' => 'getHwiOauth_HttpClientService.php',
            'hwi_oauth.resource_owner.facebook' => 'getHwiOauth_ResourceOwner_FacebookService.php',
            'hwi_oauth.resource_owner.google' => 'getHwiOauth_ResourceOwner_GoogleService.php',
            'hwi_oauth.resource_ownermap.shop' => 'getHwiOauth_ResourceOwnermap_ShopService.php',
            'hwi_oauth.security.oauth_utils' => 'getHwiOauth_Security_OauthUtilsService.php',
            'hwi_oauth.user_checker' => 'getHwiOauth_UserCheckerService.php',
            'jms_serializer' => 'getJmsSerializerService.php',
            'jms_serializer.array_collection_handler' => 'getJmsSerializer_ArrayCollectionHandlerService.php',
            'jms_serializer.constraint_violation_handler' => 'getJmsSerializer_ConstraintViolationHandlerService.php',
            'jms_serializer.datetime_handler' => 'getJmsSerializer_DatetimeHandlerService.php',
            'jms_serializer.deserialization_context_factory' => 'getJmsSerializer_DeserializationContextFactoryService.php',
            'jms_serializer.doctrine_proxy_subscriber' => 'getJmsSerializer_DoctrineProxySubscriberService.php',
            'jms_serializer.form_error_handler' => 'getJmsSerializer_FormErrorHandlerService.php',
            'jms_serializer.handler_registry' => 'getJmsSerializer_HandlerRegistryService.php',
            'jms_serializer.json_deserialization_visitor' => 'getJmsSerializer_JsonDeserializationVisitorService.php',
            'jms_serializer.json_serialization_visitor' => 'getJmsSerializer_JsonSerializationVisitorService.php',
            'jms_serializer.metadata_driver' => 'getJmsSerializer_MetadataDriverService.php',
            'jms_serializer.object_constructor' => 'getJmsSerializer_ObjectConstructorService.php',
            'jms_serializer.php_collection_handler' => 'getJmsSerializer_PhpCollectionHandlerService.php',
            'jms_serializer.serialization_context_factory' => 'getJmsSerializer_SerializationContextFactoryService.php',
            'jms_serializer.stopwatch_subscriber' => 'getJmsSerializer_StopwatchSubscriberService.php',
            'jms_serializer.twig_extension.serializer_runtime_helper' => 'getJmsSerializer_TwigExtension_SerializerRuntimeHelperService.php',
            'jms_serializer.xml_deserialization_visitor' => 'getJmsSerializer_XmlDeserializationVisitorService.php',
            'jms_serializer.xml_serialization_visitor' => 'getJmsSerializer_XmlSerializationVisitorService.php',
            'jms_serializer.yaml_serialization_visitor' => 'getJmsSerializer_YamlSerializationVisitorService.php',
            'knp_gaufrette.filesystem_map' => 'getKnpGaufrette_FilesystemMapService.php',
            'knp_menu.factory' => 'getKnpMenu_FactoryService.php',
            'knp_menu.matcher' => 'getKnpMenu_MatcherService.php',
            'knp_snappy.image' => 'getKnpSnappy_ImageService.php',
            'knp_snappy.pdf' => 'getKnpSnappy_PdfService.php',
            'lexik_translation.data_grid.formatter' => 'getLexikTranslation_DataGrid_FormatterService.php',
            'lexik_translation.data_grid.request_handler' => 'getLexikTranslation_DataGrid_RequestHandlerService.php',
            'lexik_translation.exporter_collector' => 'getLexikTranslation_ExporterCollectorService.php',
            'lexik_translation.form.handler.trans_unit' => 'getLexikTranslation_Form_Handler_TransUnitService.php',
            'lexik_translation.importer.file' => 'getLexikTranslation_Importer_FileService.php',
            'lexik_translation.locale.manager' => 'getLexikTranslation_Locale_ManagerService.php',
            'lexik_translation.overview.stats_aggregator' => 'getLexikTranslation_Overview_StatsAggregatorService.php',
            'lexik_translation.trans_unit.manager' => 'getLexikTranslation_TransUnit_ManagerService.php',
            'lexik_translation.translation_storage' => 'getLexikTranslation_TranslationStorageService.php',
            'liip_imagine.binary.loader.default' => 'getLiipImagine_Binary_Loader_DefaultService.php',
            'liip_imagine.cache.manager' => 'getLiipImagine_Cache_ManagerService.php',
            'liip_imagine.cache.resolver.default' => 'getLiipImagine_Cache_Resolver_DefaultService.php',
            'liip_imagine.cache.resolver.no_cache_web_path' => 'getLiipImagine_Cache_Resolver_NoCacheWebPathService.php',
            'liip_imagine.cache.signer' => 'getLiipImagine_Cache_SignerService.php',
            'liip_imagine.config.stack_collection' => 'getLiipImagine_Config_StackCollectionService.php',
            'liip_imagine.data.manager' => 'getLiipImagine_Data_ManagerService.php',
            'liip_imagine.filter.loader.downscale' => 'getLiipImagine_Filter_Loader_DownscaleService.php',
            'liip_imagine.filter.loader.fixed' => 'getLiipImagine_Filter_Loader_FixedService.php',
            'liip_imagine.filter.loader.flip' => 'getLiipImagine_Filter_Loader_FlipService.php',
            'liip_imagine.filter.loader.grayscale' => 'getLiipImagine_Filter_Loader_GrayscaleService.php',
            'liip_imagine.filter.loader.interlace' => 'getLiipImagine_Filter_Loader_InterlaceService.php',
            'liip_imagine.filter.loader.resample' => 'getLiipImagine_Filter_Loader_ResampleService.php',
            'liip_imagine.filter.loader.rotate' => 'getLiipImagine_Filter_Loader_RotateService.php',
            'liip_imagine.filter.manager' => 'getLiipImagine_Filter_ManagerService.php',
            'monolog.logger.integrations' => 'getMonolog_Logger_IntegrationsService.php',
            'monolog.logger.swedbank_payment' => 'getMonolog_Logger_SwedbankPaymentService.php',
            'nfq.controller.consent' => 'getNfq_Controller_ConsentService.php',
            'nfq.factory.consent' => 'getNfq_Factory_ConsentService.php',
            'nfq.repository.consent' => 'getNfq_Repository_ConsentService.php',
            'omni.controller.parcel_machine' => 'getOmni_Controller_ParcelMachineService.php',
            'omni.factory.parcel_machine' => 'getOmni_Factory_ParcelMachineService.php',
            'omni.repository.parcel_machine' => 'getOmni_Repository_ParcelMachineService.php',
            'omni_filter.grid.choices_provider' => 'getOmniFilter_Grid_ChoicesProviderService.php',
            'omni_filter.grid.filter.price' => 'getOmniFilter_Grid_Filter_PriceService.php',
            'omni_filter.grid.filter.product_attribute' => 'getOmniFilter_Grid_Filter_ProductAttributeService.php',
            'omni_search.controller.search' => 'getOmniSearch_Controller_SearchService.php',
            'omni_sylius.controller.banner' => 'getOmniSylius_Controller_BannerService.php',
            'omni_sylius.controller.banner_image' => 'getOmniSylius_Controller_BannerImageService.php',
            'omni_sylius.controller.banner_position' => 'getOmniSylius_Controller_BannerPositionService.php',
            'omni_sylius.controller.banner_zone' => 'getOmniSylius_Controller_BannerZoneService.php',
            'omni_sylius.controller.channel_logo' => 'getOmniSylius_Controller_ChannelLogoService.php',
            'omni_sylius.controller.channel_watermark' => 'getOmniSylius_Controller_ChannelWatermarkService.php',
            'omni_sylius.controller.export_job' => 'getOmniSylius_Controller_ExportJobService.php',
            'omni_sylius.controller.import_job' => 'getOmniSylius_Controller_ImportJobService.php',
            'omni_sylius.controller.manifest' => 'getOmniSylius_Controller_ManifestService.php',
            'omni_sylius.controller.node' => 'getOmniSylius_Controller_NodeService.php',
            'omni_sylius.controller.node_image' => 'getOmniSylius_Controller_NodeImageService.php',
            'omni_sylius.controller.node_image_translation' => 'getOmniSylius_Controller_NodeImageTranslationService.php',
            'omni_sylius.controller.node_translation' => 'getOmniSylius_Controller_NodeTranslationService.php',
            'omni_sylius.controller.search_index' => 'getOmniSylius_Controller_SearchIndexService.php',
            'omni_sylius.controller.shipper_config' => 'getOmniSylius_Controller_ShipperConfigService.php',
            'omni_sylius.controller.show_node' => 'getOmniSylius_Controller_ShowNodeService.php',
            'omni_sylius.factory.banner' => 'getOmniSylius_Factory_BannerService.php',
            'omni_sylius.factory.banner_image' => 'getOmniSylius_Factory_BannerImageService.php',
            'omni_sylius.factory.banner_image_translation' => 'getOmniSylius_Factory_BannerImageTranslationService.php',
            'omni_sylius.factory.banner_position' => 'getOmniSylius_Factory_BannerPositionService.php',
            'omni_sylius.factory.banner_position_translation' => 'getOmniSylius_Factory_BannerPositionTranslationService.php',
            'omni_sylius.factory.banner_zone' => 'getOmniSylius_Factory_BannerZoneService.php',
            'omni_sylius.factory.banner_zone_translation' => 'getOmniSylius_Factory_BannerZoneTranslationService.php',
            'omni_sylius.factory.channel_logo' => 'getOmniSylius_Factory_ChannelLogoService.php',
            'omni_sylius.factory.channel_watermark' => 'getOmniSylius_Factory_ChannelWatermarkService.php',
            'omni_sylius.factory.export_job' => 'getOmniSylius_Factory_ExportJobService.php',
            'omni_sylius.factory.import_job' => 'getOmniSylius_Factory_ImportJobService.php',
            'omni_sylius.factory.manifest' => 'getOmniSylius_Factory_ManifestService.php',
            'omni_sylius.factory.node' => 'getOmniSylius_Factory_NodeService.php',
            'omni_sylius.factory.node_image' => 'getOmniSylius_Factory_NodeImageService.php',
            'omni_sylius.factory.node_image_translation' => 'getOmniSylius_Factory_NodeImageTranslationService.php',
            'omni_sylius.factory.node_translation' => 'getOmniSylius_Factory_NodeTranslationService.php',
            'omni_sylius.factory.search_index' => 'getOmniSylius_Factory_SearchIndexService.php',
            'omni_sylius.factory.seo_metadata' => 'getOmniSylius_Factory_SeoMetadataService.php',
            'omni_sylius.factory.shipper_config' => 'getOmniSylius_Factory_ShipperConfigService.php',
            'omni_sylius.payum.paysera.action.convert_payment' => 'getOmniSylius_Payum_Paysera_Action_ConvertPaymentService.php',
            'omni_sylius.payum.paysera.action.notify' => 'getOmniSylius_Payum_Paysera_Action_NotifyService.php',
            'omni_sylius.repository.banner' => 'getOmniSylius_Repository_BannerService.php',
            'omni_sylius.repository.banner_image' => 'getOmniSylius_Repository_BannerImageService.php',
            'omni_sylius.repository.banner_image_translation' => 'getOmniSylius_Repository_BannerImageTranslationService.php',
            'omni_sylius.repository.banner_position' => 'getOmniSylius_Repository_BannerPositionService.php',
            'omni_sylius.repository.banner_position_translation' => 'getOmniSylius_Repository_BannerPositionTranslationService.php',
            'omni_sylius.repository.banner_zone' => 'getOmniSylius_Repository_BannerZoneService.php',
            'omni_sylius.repository.banner_zone_translation' => 'getOmniSylius_Repository_BannerZoneTranslationService.php',
            'omni_sylius.repository.channel_logo' => 'getOmniSylius_Repository_ChannelLogoService.php',
            'omni_sylius.repository.channel_watermark' => 'getOmniSylius_Repository_ChannelWatermarkService.php',
            'omni_sylius.repository.export_job' => 'getOmniSylius_Repository_ExportJobService.php',
            'omni_sylius.repository.import_job' => 'getOmniSylius_Repository_ImportJobService.php',
            'omni_sylius.repository.manifest' => 'getOmniSylius_Repository_ManifestService.php',
            'omni_sylius.repository.node' => 'getOmniSylius_Repository_NodeService.php',
            'omni_sylius.repository.node_image' => 'getOmniSylius_Repository_NodeImageService.php',
            'omni_sylius.repository.node_image_translation' => 'getOmniSylius_Repository_NodeImageTranslationService.php',
            'omni_sylius.repository.node_translation' => 'getOmniSylius_Repository_NodeTranslationService.php',
            'omni_sylius.repository.search_index' => 'getOmniSylius_Repository_SearchIndexService.php',
            'omni_sylius.repository.seo_metadata' => 'getOmniSylius_Repository_SeoMetadataService.php',
            'omni_sylius.repository.shipper_config' => 'getOmniSylius_Repository_ShipperConfigService.php',
            'omni_sylius_manifest.generator.manifest_generation_provider_picker' => 'getOmniSyliusManifest_Generator_ManifestGenerationProviderPickerService.php',
            'omni_sylius_manifest.generator.path' => 'getOmniSyliusManifest_Generator_PathService.php',
            'payum' => 'getPayumService.php',
            'payum.action.get_http_request' => 'getPayum_Action_GetHttpRequestService.php',
            'payum.action.obtain_credit_card_builder' => 'getPayum_Action_ObtainCreditCardBuilderService.php',
            'payum.builder' => 'getPayum_BuilderService.php',
            'payum.converter.reply_to_http_response' => 'getPayum_Converter_ReplyToHttpResponseService.php',
            'payum.dynamic_gateways.config_storage' => 'getPayum_DynamicGateways_ConfigStorageService.php',
            'payum.dynamic_registry' => 'getPayum_DynamicRegistryService.php',
            'payum.extension.log_executed_actions' => 'getPayum_Extension_LogExecutedActionsService.php',
            'payum.extension.logger' => 'getPayum_Extension_LoggerService.php',
            'payum.extension.storage.app_entity_order_order' => 'getPayum_Extension_Storage_AppEntityOrderOrderService.php',
            'payum.extension.storage.app_entity_payment_payment' => 'getPayum_Extension_Storage_AppEntityPaymentPaymentService.php',
            'payum.form.extension.gateway_factories_choice' => 'getPayum_Form_Extension_GatewayFactoriesChoiceService.php',
            'payum.form.type.credit_card' => 'getPayum_Form_Type_CreditCardService.php',
            'payum.form.type.credit_card_expiration_date' => 'getPayum_Form_Type_CreditCardExpirationDateService.php',
            'payum.form.type.gateway_config' => 'getPayum_Form_Type_GatewayConfigService.php',
            'payum.form.type.gateway_factories_choice' => 'getPayum_Form_Type_GatewayFactoriesChoiceService.php',
            'payum.listener.reply_to_http_response' => 'getPayum_Listener_ReplyToHttpResponseService.php',
            'payum.profiler.payum_collector' => 'getPayum_Profiler_PayumCollectorService.php',
            'payum.security.token_storage' => 'getPayum_Security_TokenStorageService.php',
            'payum.static_registry' => 'getPayum_StaticRegistryService.php',
            'payum.storage.app_entity_order_order' => 'getPayum_Storage_AppEntityOrderOrderService.php',
            'payum.storage.app_entity_payment_payment' => 'getPayum_Storage_AppEntityPaymentPaymentService.php',
            'payum.storage.sylius_bundle_payumbundle_model_gatewayconfig' => 'getPayum_Storage_SyliusBundlePayumbundleModelGatewayconfigService.php',
            'payum.storage.sylius_bundle_payumbundle_model_paymentsecuritytoken' => 'getPayum_Storage_SyliusBundlePayumbundleModelPaymentsecuritytokenService.php',
            'routing.loader' => 'getRouting_LoaderService.php',
            'security.authentication_utils' => 'getSecurity_AuthenticationUtilsService.php',
            'security.csrf.token_manager' => 'getSecurity_Csrf_TokenManagerService.php',
            'security.password_encoder' => 'getSecurity_PasswordEncoderService.php',
            'services_resetter' => 'getServicesResetterService.php',
            'sm.callback.cascade_transition' => 'getSm_Callback_CascadeTransitionService.php',
            'sonata.block.context_manager.default' => 'getSonata_Block_ContextManager_DefaultService.php',
            'sonata.block.exception.filter.debug_only' => 'getSonata_Block_Exception_Filter_DebugOnlyService.php',
            'sonata.block.exception.filter.ignore_block_exception' => 'getSonata_Block_Exception_Filter_IgnoreBlockExceptionService.php',
            'sonata.block.exception.filter.keep_all' => 'getSonata_Block_Exception_Filter_KeepAllService.php',
            'sonata.block.exception.filter.keep_none' => 'getSonata_Block_Exception_Filter_KeepNoneService.php',
            'sonata.block.exception.renderer.inline' => 'getSonata_Block_Exception_Renderer_InlineService.php',
            'sonata.block.exception.renderer.inline_debug' => 'getSonata_Block_Exception_Renderer_InlineDebugService.php',
            'sonata.block.exception.renderer.throw' => 'getSonata_Block_Exception_Renderer_ThrowService.php',
            'sonata.block.manager' => 'getSonata_Block_ManagerService.php',
            'sonata.block.menu.registry' => 'getSonata_Block_Menu_RegistryService.php',
            'sonata.block.renderer.default' => 'getSonata_Block_Renderer_DefaultService.php',
            'sonata.block.service.container' => 'getSonata_Block_Service_ContainerService.php',
            'sonata.block.service.empty' => 'getSonata_Block_Service_EmptyService.php',
            'sonata.block.service.menu' => 'getSonata_Block_Service_MenuService.php',
            'sonata.block.service.rss' => 'getSonata_Block_Service_RssService.php',
            'sonata.block.service.template' => 'getSonata_Block_Service_TemplateService.php',
            'sonata.block.service.text' => 'getSonata_Block_Service_TextService.php',
            'sonata.core.date.moment_format_converter' => 'getSonata_Core_Date_MomentFormatConverterService.php',
            'sonata.core.flashmessage.twig.runtime' => 'getSonata_Core_Flashmessage_Twig_RuntimeService.php',
            'sonata.core.form.type.array' => 'getSonata_Core_Form_Type_ArrayService.php',
            'sonata.core.form.type.array_legacy' => 'getSonata_Core_Form_Type_ArrayLegacyService.php',
            'sonata.core.form.type.boolean' => 'getSonata_Core_Form_Type_BooleanService.php',
            'sonata.core.form.type.boolean_legacy' => 'getSonata_Core_Form_Type_BooleanLegacyService.php',
            'sonata.core.form.type.collection' => 'getSonata_Core_Form_Type_CollectionService.php',
            'sonata.core.form.type.collection_legacy' => 'getSonata_Core_Form_Type_CollectionLegacyService.php',
            'sonata.core.form.type.color_legacy' => 'getSonata_Core_Form_Type_ColorLegacyService.php',
            'sonata.core.form.type.color_selector' => 'getSonata_Core_Form_Type_ColorSelectorService.php',
            'sonata.core.form.type.date_picker' => 'getSonata_Core_Form_Type_DatePickerService.php',
            'sonata.core.form.type.date_picker_legacy' => 'getSonata_Core_Form_Type_DatePickerLegacyService.php',
            'sonata.core.form.type.date_range' => 'getSonata_Core_Form_Type_DateRangeService.php',
            'sonata.core.form.type.date_range_legacy' => 'getSonata_Core_Form_Type_DateRangeLegacyService.php',
            'sonata.core.form.type.date_range_picker' => 'getSonata_Core_Form_Type_DateRangePickerService.php',
            'sonata.core.form.type.date_range_picker_legacy' => 'getSonata_Core_Form_Type_DateRangePickerLegacyService.php',
            'sonata.core.form.type.datetime_picker' => 'getSonata_Core_Form_Type_DatetimePickerService.php',
            'sonata.core.form.type.datetime_picker_legacy' => 'getSonata_Core_Form_Type_DatetimePickerLegacyService.php',
            'sonata.core.form.type.datetime_range' => 'getSonata_Core_Form_Type_DatetimeRangeService.php',
            'sonata.core.form.type.datetime_range_legacy' => 'getSonata_Core_Form_Type_DatetimeRangeLegacyService.php',
            'sonata.core.form.type.datetime_range_picker' => 'getSonata_Core_Form_Type_DatetimeRangePickerService.php',
            'sonata.core.form.type.datetime_range_picker_legacy' => 'getSonata_Core_Form_Type_DatetimeRangePickerLegacyService.php',
            'sonata.core.form.type.equal' => 'getSonata_Core_Form_Type_EqualService.php',
            'sonata.core.form.type.equal_legacy' => 'getSonata_Core_Form_Type_EqualLegacyService.php',
            'sonata.core.form.type.translatable_choice' => 'getSonata_Core_Form_Type_TranslatableChoiceService.php',
            'sonata.core.model.adapter.chain' => 'getSonata_Core_Model_Adapter_ChainService.php',
            'sonata.core.slugify.cocur' => 'getSonata_Core_Slugify_CocurService.php',
            'sonata.core.slugify.native' => 'getSonata_Core_Slugify_NativeService.php',
            'sonata.core.twig.deprecated_template_extension' => 'getSonata_Core_Twig_DeprecatedTemplateExtensionService.php',
            'sonata.core.twig.extension.wrapping' => 'getSonata_Core_Twig_Extension_WrappingService.php',
            'sonata.core.twig.status_runtime' => 'getSonata_Core_Twig_StatusRuntimeService.php',
            'sonata.core.twig.template_extension' => 'getSonata_Core_Twig_TemplateExtensionService.php',
            'sonata.intl.locale_detector.request' => 'getSonata_Intl_LocaleDetector_RequestService.php',
            'sonata.intl.locale_detector.request_stack' => 'getSonata_Intl_LocaleDetector_RequestStackService.php',
            'sonata.intl.templating.helper.datetime' => 'getSonata_Intl_Templating_Helper_DatetimeService.php',
            'sonata.intl.templating.helper.locale' => 'getSonata_Intl_Templating_Helper_LocaleService.php',
            'sonata.intl.templating.helper.number' => 'getSonata_Intl_Templating_Helper_NumberService.php',
            'sonata.intl.timezone_detector.chain' => 'getSonata_Intl_TimezoneDetector_ChainService.php',
            'sonata.intl.timezone_detector.locale' => 'getSonata_Intl_TimezoneDetector_LocaleService.php',
            'sonata.intl.timezone_detector.user' => 'getSonata_Intl_TimezoneDetector_UserService.php',
            'sonata.twig.flashmessage.manager' => 'getSonata_Twig_Flashmessage_ManagerService.php',
            'sonata.twig.flashmessage.twig.extension' => 'getSonata_Twig_Flashmessage_Twig_ExtensionService.php',
            'swiftmailer.mailer.default' => 'getSwiftmailer_Mailer_DefaultService.php',
            'swiftmailer.mailer.default.plugin.messagelogger' => 'getSwiftmailer_Mailer_Default_Plugin_MessageloggerService.php',
            'swiftmailer.transport' => 'getSwiftmailer_TransportService.php',
            'sylius.active_promotions_provider' => 'getSylius_ActivePromotionsProviderService.php',
            'sylius.address_comparator' => 'getSylius_AddressComparatorService.php',
            'sylius.adjustments_aggregator' => 'getSylius_AdjustmentsAggregatorService.php',
            'sylius.admin.menu_builder.customer.show' => 'getSylius_Admin_MenuBuilder_Customer_ShowService.php',
            'sylius.admin.menu_builder.main' => 'getSylius_Admin_MenuBuilder_MainService.php',
            'sylius.admin.menu_builder.order.show' => 'getSylius_Admin_MenuBuilder_Order_ShowService.php',
            'sylius.admin.menu_builder.product.update' => 'getSylius_Admin_MenuBuilder_Product_UpdateService.php',
            'sylius.admin.menu_builder.product_form' => 'getSylius_Admin_MenuBuilder_ProductFormService.php',
            'sylius.admin.menu_builder.product_variant_form' => 'getSylius_Admin_MenuBuilder_ProductVariantFormService.php',
            'sylius.admin.menu_builder.promotion.update' => 'getSylius_Admin_MenuBuilder_Promotion_UpdateService.php',
            'sylius.admin_user.pin_generator.password_reset' => 'getSylius_AdminUser_PinGenerator_PasswordResetService.php',
            'sylius.admin_user.token_generator.email_verification' => 'getSylius_AdminUser_TokenGenerator_EmailVerificationService.php',
            'sylius.admin_user.token_generator.password_reset' => 'getSylius_AdminUser_TokenGenerator_PasswordResetService.php',
            'sylius.attribute_type.checkbox' => 'getSylius_AttributeType_CheckboxService.php',
            'sylius.attribute_type.date' => 'getSylius_AttributeType_DateService.php',
            'sylius.attribute_type.datetime' => 'getSylius_AttributeType_DatetimeService.php',
            'sylius.attribute_type.integer' => 'getSylius_AttributeType_IntegerService.php',
            'sylius.attribute_type.percent' => 'getSylius_AttributeType_PercentService.php',
            'sylius.attribute_type.select' => 'getSylius_AttributeType_SelectService.php',
            'sylius.attribute_type.select.value.translations' => 'getSylius_AttributeType_Select_Value_TranslationsService.php',
            'sylius.attribute_type.text' => 'getSylius_AttributeType_TextService.php',
            'sylius.attribute_type.textarea' => 'getSylius_AttributeType_TextareaService.php',
            'sylius.availability_checker.default' => 'getSylius_AvailabilityChecker_DefaultService.php',
            'sylius.average_rating_calculator' => 'getSylius_AverageRatingCalculatorService.php',
            'sylius.calculator.order_items_subtotal' => 'getSylius_Calculator_OrderItemsSubtotalService.php',
            'sylius.canonicalizer' => 'getSylius_CanonicalizerService.php',
            'sylius.checker.order_payment_method_selection_requirement' => 'getSylius_Checker_OrderPaymentMethodSelectionRequirementService.php',
            'sylius.checker.order_shipping_method_selection_requirement' => 'getSylius_Checker_OrderShippingMethodSelectionRequirementService.php',
            'sylius.checker.product_variants_parity' => 'getSylius_Checker_ProductVariantsParityService.php',
            'sylius.commands_provider.database_setup' => 'getSylius_CommandsProvider_DatabaseSetupService.php',
            'sylius.console.command.resource_debug' => 'getSylius_Console_Command_ResourceDebugService.php',
            'sylius.controller.address' => 'getSylius_Controller_AddressService.php',
            'sylius.controller.address_log_entry' => 'getSylius_Controller_AddressLogEntryService.php',
            'sylius.controller.adjustment' => 'getSylius_Controller_AdjustmentService.php',
            'sylius.controller.admin.dashboard' => 'getSylius_Controller_Admin_DashboardService.php',
            'sylius.controller.admin.notification' => 'getSylius_Controller_Admin_NotificationService.php',
            'sylius.controller.admin_user' => 'getSylius_Controller_AdminUserService.php',
            'sylius.controller.api_access_token' => 'getSylius_Controller_ApiAccessTokenService.php',
            'sylius.controller.api_auth_code' => 'getSylius_Controller_ApiAuthCodeService.php',
            'sylius.controller.api_client' => 'getSylius_Controller_ApiClientService.php',
            'sylius.controller.api_refresh_token' => 'getSylius_Controller_ApiRefreshTokenService.php',
            'sylius.controller.channel' => 'getSylius_Controller_ChannelService.php',
            'sylius.controller.channel_pricing' => 'getSylius_Controller_ChannelPricingService.php',
            'sylius.controller.country' => 'getSylius_Controller_CountryService.php',
            'sylius.controller.currency' => 'getSylius_Controller_CurrencyService.php',
            'sylius.controller.customer' => 'getSylius_Controller_CustomerService.php',
            'sylius.controller.customer_group' => 'getSylius_Controller_CustomerGroupService.php',
            'sylius.controller.customer_statistics' => 'getSylius_Controller_CustomerStatisticsService.php',
            'sylius.controller.exchange_rate' => 'getSylius_Controller_ExchangeRateService.php',
            'sylius.controller.export_data_country' => 'getSylius_Controller_ExportDataCountryService.php',
            'sylius.controller.export_data_customer' => 'getSylius_Controller_ExportDataCustomerService.php',
            'sylius.controller.export_data_order' => 'getSylius_Controller_ExportDataOrderService.php',
            'sylius.controller.export_data_product' => 'getSylius_Controller_ExportDataProductService.php',
            'sylius.controller.gateway_config' => 'getSylius_Controller_GatewayConfigService.php',
            'sylius.controller.impersonate_user' => 'getSylius_Controller_ImpersonateUserService.php',
            'sylius.controller.import_data' => 'getSylius_Controller_ImportDataService.php',
            'sylius.controller.inventory_unit' => 'getSylius_Controller_InventoryUnitService.php',
            'sylius.controller.locale' => 'getSylius_Controller_LocaleService.php',
            'sylius.controller.oauth_user' => 'getSylius_Controller_OauthUserService.php',
            'sylius.controller.order' => 'getSylius_Controller_OrderService.php',
            'sylius.controller.order_item' => 'getSylius_Controller_OrderItemService.php',
            'sylius.controller.order_item_unit' => 'getSylius_Controller_OrderItemUnitService.php',
            'sylius.controller.payment' => 'getSylius_Controller_PaymentService.php',
            'sylius.controller.payment_method' => 'getSylius_Controller_PaymentMethodService.php',
            'sylius.controller.payment_method_translation' => 'getSylius_Controller_PaymentMethodTranslationService.php',
            'sylius.controller.payment_security_token' => 'getSylius_Controller_PaymentSecurityTokenService.php',
            'sylius.controller.payum' => 'getSylius_Controller_PayumService.php',
            'sylius.controller.product' => 'getSylius_Controller_ProductService.php',
            'sylius.controller.product_association' => 'getSylius_Controller_ProductAssociationService.php',
            'sylius.controller.product_association_type' => 'getSylius_Controller_ProductAssociationTypeService.php',
            'sylius.controller.product_association_type_translation' => 'getSylius_Controller_ProductAssociationTypeTranslationService.php',
            'sylius.controller.product_attribute' => 'getSylius_Controller_ProductAttributeService.php',
            'sylius.controller.product_attribute_translation' => 'getSylius_Controller_ProductAttributeTranslationService.php',
            'sylius.controller.product_attribute_value' => 'getSylius_Controller_ProductAttributeValueService.php',
            'sylius.controller.product_image' => 'getSylius_Controller_ProductImageService.php',
            'sylius.controller.product_option' => 'getSylius_Controller_ProductOptionService.php',
            'sylius.controller.product_option_translation' => 'getSylius_Controller_ProductOptionTranslationService.php',
            'sylius.controller.product_option_value' => 'getSylius_Controller_ProductOptionValueService.php',
            'sylius.controller.product_option_value_translation' => 'getSylius_Controller_ProductOptionValueTranslationService.php',
            'sylius.controller.product_review' => 'getSylius_Controller_ProductReviewService.php',
            'sylius.controller.product_slug' => 'getSylius_Controller_ProductSlugService.php',
            'sylius.controller.product_taxon' => 'getSylius_Controller_ProductTaxonService.php',
            'sylius.controller.product_translation' => 'getSylius_Controller_ProductTranslationService.php',
            'sylius.controller.product_variant' => 'getSylius_Controller_ProductVariantService.php',
            'sylius.controller.product_variant_translation' => 'getSylius_Controller_ProductVariantTranslationService.php',
            'sylius.controller.promotion' => 'getSylius_Controller_PromotionService.php',
            'sylius.controller.promotion_action' => 'getSylius_Controller_PromotionActionService.php',
            'sylius.controller.promotion_coupon' => 'getSylius_Controller_PromotionCouponService.php',
            'sylius.controller.promotion_rule' => 'getSylius_Controller_PromotionRuleService.php',
            'sylius.controller.province' => 'getSylius_Controller_ProvinceService.php',
            'sylius.controller.security' => 'getSylius_Controller_SecurityService.php',
            'sylius.controller.shipment' => 'getSylius_Controller_ShipmentService.php',
            'sylius.controller.shipment_unit' => 'getSylius_Controller_ShipmentUnitService.php',
            'sylius.controller.shipping_category' => 'getSylius_Controller_ShippingCategoryService.php',
            'sylius.controller.shipping_method' => 'getSylius_Controller_ShippingMethodService.php',
            'sylius.controller.shipping_method_translation' => 'getSylius_Controller_ShippingMethodTranslationService.php',
            'sylius.controller.shop.contact' => 'getSylius_Controller_Shop_ContactService.php',
            'sylius.controller.shop.currency_switch' => 'getSylius_Controller_Shop_CurrencySwitchService.php',
            'sylius.controller.shop.homepage' => 'getSylius_Controller_Shop_HomepageService.php',
            'sylius.controller.shop.locale_switch' => 'getSylius_Controller_Shop_LocaleSwitchService.php',
            'sylius.controller.shop.security_widget' => 'getSylius_Controller_Shop_SecurityWidgetService.php',
            'sylius.controller.shop_user' => 'getSylius_Controller_ShopUserService.php',
            'sylius.controller.show_available_payment_methods' => 'getSylius_Controller_ShowAvailablePaymentMethodsService.php',
            'sylius.controller.show_available_shipping_methods' => 'getSylius_Controller_ShowAvailableShippingMethodsService.php',
            'sylius.controller.sitemap' => 'getSylius_Controller_SitemapService.php',
            'sylius.controller.sitemap_index' => 'getSylius_Controller_SitemapIndexService.php',
            'sylius.controller.tax_category' => 'getSylius_Controller_TaxCategoryService.php',
            'sylius.controller.tax_rate' => 'getSylius_Controller_TaxRateService.php',
            'sylius.controller.taxon' => 'getSylius_Controller_TaxonService.php',
            'sylius.controller.taxon_image' => 'getSylius_Controller_TaxonImageService.php',
            'sylius.controller.taxon_slug' => 'getSylius_Controller_TaxonSlugService.php',
            'sylius.controller.taxon_translation' => 'getSylius_Controller_TaxonTranslationService.php',
            'sylius.controller.update_product_taxon_position' => 'getSylius_Controller_UpdateProductTaxonPositionService.php',
            'sylius.controller.user_security' => 'getSylius_Controller_UserSecurityService.php',
            'sylius.controller.zone' => 'getSylius_Controller_ZoneService.php',
            'sylius.controller.zone_member' => 'getSylius_Controller_ZoneMemberService.php',
            'sylius.converter.country_name' => 'getSylius_Converter_CountryNameService.php',
            'sylius.currency_converter' => 'getSylius_CurrencyConverterService.php',
            'sylius.currency_name_converter' => 'getSylius_CurrencyNameConverterService.php',
            'sylius.custom_bulk_action_grid_renderer.twig' => 'getSylius_CustomBulkActionGridRenderer_TwigService.php',
            'sylius.custom_grid_renderer.twig' => 'getSylius_CustomGridRenderer_TwigService.php',
            'sylius.customer_ip_assigner' => 'getSylius_CustomerIpAssignerService.php',
            'sylius.customer_order_addresses_saver' => 'getSylius_CustomerOrderAddressesSaverService.php',
            'sylius.customer_statistics_provider' => 'getSylius_CustomerStatisticsProviderService.php',
            'sylius.customer_unique_address_adder' => 'getSylius_CustomerUniqueAddressAdderService.php',
            'sylius.dashboard.statistics_provider' => 'getSylius_Dashboard_StatisticsProviderService.php',
            'sylius.doctrine.orm.event_subscriber.load_metadata.attribute' => 'getSylius_Doctrine_Orm_EventSubscriber_LoadMetadata_AttributeService.php',
            'sylius.doctrine.orm.event_subscriber.load_metadata.review' => 'getSylius_Doctrine_Orm_EventSubscriber_LoadMetadata_ReviewService.php',
            'sylius.email_manager.contact' => 'getSylius_EmailManager_ContactService.php',
            'sylius.email_manager.order' => 'getSylius_EmailManager_OrderService.php',
            'sylius.email_manager.shipment' => 'getSylius_EmailManager_ShipmentService.php',
            'sylius.email_provider' => 'getSylius_EmailProviderService.php',
            'sylius.email_renderer.adapter.twig' => 'getSylius_EmailRenderer_Adapter_TwigService.php',
            'sylius.email_sender' => 'getSylius_EmailSenderService.php',
            'sylius.email_sender.adapter.swiftmailer' => 'getSylius_EmailSender_Adapter_SwiftmailerService.php',
            'sylius.event_subscriber.orm_mapped_super_class' => 'getSylius_EventSubscriber_OrmMappedSuperClassService.php',
            'sylius.event_subscriber.orm_repository_class' => 'getSylius_EventSubscriber_OrmRepositoryClassService.php',
            'sylius.event_subscriber.resource_delete' => 'getSylius_EventSubscriber_ResourceDeleteService.php',
            'sylius.expired_carts_remover' => 'getSylius_ExpiredCartsRemoverService.php',
            'sylius.exporters_registry' => 'getSylius_ExportersRegistryService.php',
            'sylius.factory.add_to_cart_command' => 'getSylius_Factory_AddToCartCommandService.php',
            'sylius.factory.address' => 'getSylius_Factory_AddressService.php',
            'sylius.factory.address_log_entry' => 'getSylius_Factory_AddressLogEntryService.php',
            'sylius.factory.admin_user' => 'getSylius_Factory_AdminUserService.php',
            'sylius.factory.api_access_token' => 'getSylius_Factory_ApiAccessTokenService.php',
            'sylius.factory.api_auth_code' => 'getSylius_Factory_ApiAuthCodeService.php',
            'sylius.factory.api_client' => 'getSylius_Factory_ApiClientService.php',
            'sylius.factory.api_refresh_token' => 'getSylius_Factory_ApiRefreshTokenService.php',
            'sylius.factory.cart_item' => 'getSylius_Factory_CartItemService.php',
            'sylius.factory.channel_pricing' => 'getSylius_Factory_ChannelPricingService.php',
            'sylius.factory.country' => 'getSylius_Factory_CountryService.php',
            'sylius.factory.currency' => 'getSylius_Factory_CurrencyService.php',
            'sylius.factory.customer' => 'getSylius_Factory_CustomerService.php',
            'sylius.factory.customer_after_checkout' => 'getSylius_Factory_CustomerAfterCheckoutService.php',
            'sylius.factory.customer_group' => 'getSylius_Factory_CustomerGroupService.php',
            'sylius.factory.email' => 'getSylius_Factory_EmailService.php',
            'sylius.factory.exchange_rate' => 'getSylius_Factory_ExchangeRateService.php',
            'sylius.factory.gateway_config' => 'getSylius_Factory_GatewayConfigService.php',
            'sylius.factory.inventory_unit' => 'getSylius_Factory_InventoryUnitService.php',
            'sylius.factory.locale' => 'getSylius_Factory_LocaleService.php',
            'sylius.factory.oauth_user' => 'getSylius_Factory_OauthUserService.php',
            'sylius.factory.order_item_unit' => 'getSylius_Factory_OrderItemUnitService.php',
            'sylius.factory.order_sequence' => 'getSylius_Factory_OrderSequenceService.php',
            'sylius.factory.payment_method_translation' => 'getSylius_Factory_PaymentMethodTranslationService.php',
            'sylius.factory.payment_security_token' => 'getSylius_Factory_PaymentSecurityTokenService.php',
            'sylius.factory.payum_get_status_action' => 'getSylius_Factory_PayumGetStatusActionService.php',
            'sylius.factory.payum_resolve_next_route' => 'getSylius_Factory_PayumResolveNextRouteService.php',
            'sylius.factory.product_association' => 'getSylius_Factory_ProductAssociationService.php',
            'sylius.factory.product_association_type' => 'getSylius_Factory_ProductAssociationTypeService.php',
            'sylius.factory.product_association_type_translation' => 'getSylius_Factory_ProductAssociationTypeTranslationService.php',
            'sylius.factory.product_attribute' => 'getSylius_Factory_ProductAttributeService.php',
            'sylius.factory.product_attribute_translation' => 'getSylius_Factory_ProductAttributeTranslationService.php',
            'sylius.factory.product_attribute_value' => 'getSylius_Factory_ProductAttributeValueService.php',
            'sylius.factory.product_image' => 'getSylius_Factory_ProductImageService.php',
            'sylius.factory.product_option' => 'getSylius_Factory_ProductOptionService.php',
            'sylius.factory.product_option_translation' => 'getSylius_Factory_ProductOptionTranslationService.php',
            'sylius.factory.product_option_value' => 'getSylius_Factory_ProductOptionValueService.php',
            'sylius.factory.product_option_value_translation' => 'getSylius_Factory_ProductOptionValueTranslationService.php',
            'sylius.factory.product_review' => 'getSylius_Factory_ProductReviewService.php',
            'sylius.factory.product_reviewer' => 'getSylius_Factory_ProductReviewerService.php',
            'sylius.factory.product_taxon' => 'getSylius_Factory_ProductTaxonService.php',
            'sylius.factory.product_translation' => 'getSylius_Factory_ProductTranslationService.php',
            'sylius.factory.product_variant_translation' => 'getSylius_Factory_ProductVariantTranslationService.php',
            'sylius.factory.promotion' => 'getSylius_Factory_PromotionService.php',
            'sylius.factory.province' => 'getSylius_Factory_ProvinceService.php',
            'sylius.factory.shipment' => 'getSylius_Factory_ShipmentService.php',
            'sylius.factory.shipment_unit' => 'getSylius_Factory_ShipmentUnitService.php',
            'sylius.factory.shipping_category' => 'getSylius_Factory_ShippingCategoryService.php',
            'sylius.factory.shipping_method' => 'getSylius_Factory_ShippingMethodService.php',
            'sylius.factory.shipping_method_translation' => 'getSylius_Factory_ShippingMethodTranslationService.php',
            'sylius.factory.shop_user' => 'getSylius_Factory_ShopUserService.php',
            'sylius.factory.tax_category' => 'getSylius_Factory_TaxCategoryService.php',
            'sylius.factory.tax_rate' => 'getSylius_Factory_TaxRateService.php',
            'sylius.factory.taxon_image' => 'getSylius_Factory_TaxonImageService.php',
            'sylius.factory.taxon_translation' => 'getSylius_Factory_TaxonTranslationService.php',
            'sylius.factory.zone_member' => 'getSylius_Factory_ZoneMemberService.php',
            'sylius.fixture.address' => 'getSylius_Fixture_AddressService.php',
            'sylius.fixture.admin_user' => 'getSylius_Fixture_AdminUserService.php',
            'sylius.fixture.api_access_token' => 'getSylius_Fixture_ApiAccessTokenService.php',
            'sylius.fixture.api_client' => 'getSylius_Fixture_ApiClientService.php',
            'sylius.fixture.book_product' => 'getSylius_Fixture_BookProductService.php',
            'sylius.fixture.channel' => 'getSylius_Fixture_ChannelService.php',
            'sylius.fixture.currency' => 'getSylius_Fixture_CurrencyService.php',
            'sylius.fixture.customer_group' => 'getSylius_Fixture_CustomerGroupService.php',
            'sylius.fixture.example_factory.address' => 'getSylius_Fixture_ExampleFactory_AddressService.php',
            'sylius.fixture.example_factory.admin_user' => 'getSylius_Fixture_ExampleFactory_AdminUserService.php',
            'sylius.fixture.example_factory.api_access_token' => 'getSylius_Fixture_ExampleFactory_ApiAccessTokenService.php',
            'sylius.fixture.example_factory.api_client' => 'getSylius_Fixture_ExampleFactory_ApiClientService.php',
            'sylius.fixture.example_factory.channel' => 'getSylius_Fixture_ExampleFactory_ChannelService.php',
            'sylius.fixture.example_factory.customer_group' => 'getSylius_Fixture_ExampleFactory_CustomerGroupService.php',
            'sylius.fixture.example_factory.order' => 'getSylius_Fixture_ExampleFactory_OrderService.php',
            'sylius.fixture.example_factory.payment_method' => 'getSylius_Fixture_ExampleFactory_PaymentMethodService.php',
            'sylius.fixture.example_factory.product' => 'getSylius_Fixture_ExampleFactory_ProductService.php',
            'sylius.fixture.example_factory.product_association' => 'getSylius_Fixture_ExampleFactory_ProductAssociationService.php',
            'sylius.fixture.example_factory.product_association_type' => 'getSylius_Fixture_ExampleFactory_ProductAssociationTypeService.php',
            'sylius.fixture.example_factory.product_attribute' => 'getSylius_Fixture_ExampleFactory_ProductAttributeService.php',
            'sylius.fixture.example_factory.product_option' => 'getSylius_Fixture_ExampleFactory_ProductOptionService.php',
            'sylius.fixture.example_factory.product_review' => 'getSylius_Fixture_ExampleFactory_ProductReviewService.php',
            'sylius.fixture.example_factory.promotion' => 'getSylius_Fixture_ExampleFactory_PromotionService.php',
            'sylius.fixture.example_factory.promotion_action' => 'getSylius_Fixture_ExampleFactory_PromotionActionService.php',
            'sylius.fixture.example_factory.promotion_rule' => 'getSylius_Fixture_ExampleFactory_PromotionRuleService.php',
            'sylius.fixture.example_factory.shipping_category' => 'getSylius_Fixture_ExampleFactory_ShippingCategoryService.php',
            'sylius.fixture.example_factory.shipping_method' => 'getSylius_Fixture_ExampleFactory_ShippingMethodService.php',
            'sylius.fixture.example_factory.shop_user' => 'getSylius_Fixture_ExampleFactory_ShopUserService.php',
            'sylius.fixture.example_factory.tax_category' => 'getSylius_Fixture_ExampleFactory_TaxCategoryService.php',
            'sylius.fixture.example_factory.tax_rate' => 'getSylius_Fixture_ExampleFactory_TaxRateService.php',
            'sylius.fixture.example_factory.taxon' => 'getSylius_Fixture_ExampleFactory_TaxonService.php',
            'sylius.fixture.geographical' => 'getSylius_Fixture_GeographicalService.php',
            'sylius.fixture.mug_product' => 'getSylius_Fixture_MugProductService.php',
            'sylius.fixture.order' => 'getSylius_Fixture_OrderService.php',
            'sylius.fixture.payment_method' => 'getSylius_Fixture_PaymentMethodService.php',
            'sylius.fixture.product' => 'getSylius_Fixture_ProductService.php',
            'sylius.fixture.product_association' => 'getSylius_Fixture_ProductAssociationService.php',
            'sylius.fixture.product_association_type' => 'getSylius_Fixture_ProductAssociationTypeService.php',
            'sylius.fixture.product_attribute' => 'getSylius_Fixture_ProductAttributeService.php',
            'sylius.fixture.product_option' => 'getSylius_Fixture_ProductOptionService.php',
            'sylius.fixture.product_review' => 'getSylius_Fixture_ProductReviewService.php',
            'sylius.fixture.promotion' => 'getSylius_Fixture_PromotionService.php',
            'sylius.fixture.shipping_category' => 'getSylius_Fixture_ShippingCategoryService.php',
            'sylius.fixture.shipping_method' => 'getSylius_Fixture_ShippingMethodService.php',
            'sylius.fixture.shop_user' => 'getSylius_Fixture_ShopUserService.php',
            'sylius.fixture.similar_product_association' => 'getSylius_Fixture_SimilarProductAssociationService.php',
            'sylius.fixture.sticker_product' => 'getSylius_Fixture_StickerProductService.php',
            'sylius.fixture.tax_category' => 'getSylius_Fixture_TaxCategoryService.php',
            'sylius.fixture.tax_rate' => 'getSylius_Fixture_TaxRateService.php',
            'sylius.fixture.taxon' => 'getSylius_Fixture_TaxonService.php',
            'sylius.fixture.tshirt_product' => 'getSylius_Fixture_TshirtProductService.php',
            'sylius.form.choice_list.loader.lazy_customer_loader' => 'getSylius_Form_ChoiceList_Loader_LazyCustomerLoaderService.php',
            'sylius.form.data_mapper.order_item_quantity' => 'getSylius_Form_DataMapper_OrderItemQuantityService.php',
            'sylius.form.event_subscriber.product_variant_generator' => 'getSylius_Form_EventSubscriber_ProductVariantGeneratorService.php',
            'sylius.form.extension.type.api_order_item' => 'getSylius_Form_Extension_Type_ApiOrderItemService.php',
            'sylius.form.extension.type.cart' => 'getSylius_Form_Extension_Type_CartService.php',
            'sylius.form.extension.type.cart_item' => 'getSylius_Form_Extension_Type_CartItemService.php',
            'sylius.form.extension.type.channel' => 'getSylius_Form_Extension_Type_ChannelService.php',
            'sylius.form.extension.type.collection' => 'getSylius_Form_Extension_Type_CollectionService.php',
            'sylius.form.extension.type.country' => 'getSylius_Form_Extension_Type_CountryService.php',
            'sylius.form.extension.type.customer' => 'getSylius_Form_Extension_Type_CustomerService.php',
            'sylius.form.extension.type.gateway_config.crypted' => 'getSylius_Form_Extension_Type_GatewayConfig_CryptedService.php',
            'sylius.form.extension.type.locale' => 'getSylius_Form_Extension_Type_LocaleService.php',
            'sylius.form.extension.type.order' => 'getSylius_Form_Extension_Type_OrderService.php',
            'sylius.form.extension.type.payment_method' => 'getSylius_Form_Extension_Type_PaymentMethodService.php',
            'sylius.form.extension.type.product' => 'getSylius_Form_Extension_Type_ProductService.php',
            'sylius.form.extension.type.product_translation' => 'getSylius_Form_Extension_Type_ProductTranslationService.php',
            'sylius.form.extension.type.product_variant' => 'getSylius_Form_Extension_Type_ProductVariantService.php',
            'sylius.form.extension.type.product_variant_generation' => 'getSylius_Form_Extension_Type_ProductVariantGenerationService.php',
            'sylius.form.extension.type.promotion' => 'getSylius_Form_Extension_Type_PromotionService.php',
            'sylius.form.extension.type.promotion_coupon' => 'getSylius_Form_Extension_Type_PromotionCouponService.php',
            'sylius.form.extension.type.promotion_filter_collection' => 'getSylius_Form_Extension_Type_PromotionFilterCollectionService.php',
            'sylius.form.extension.type.shipping_method' => 'getSylius_Form_Extension_Type_ShippingMethodService.php',
            'sylius.form.extension.type.tax_rate' => 'getSylius_Form_Extension_Type_TaxRateService.php',
            'sylius.form.extension.type.taxon' => 'getSylius_Form_Extension_Type_TaxonService.php',
            'sylius.form.type.add_to_cart' => 'getSylius_Form_Type_AddToCartService.php',
            'sylius.form.type.address_choice' => 'getSylius_Form_Type_AddressChoiceService.php',
            'sylius.form.type.admin_user' => 'getSylius_Form_Type_AdminUserService.php',
            'sylius.form.type.api_checkout_address' => 'getSylius_Form_Type_ApiCheckoutAddressService.php',
            'sylius.form.type.api_client' => 'getSylius_Form_Type_ApiClientService.php',
            'sylius.form.type.api_customer' => 'getSylius_Form_Type_ApiCustomerService.php',
            'sylius.form.type.api_order' => 'getSylius_Form_Type_ApiOrderService.php',
            'sylius.form.type.api_order_item' => 'getSylius_Form_Type_ApiOrderItemService.php',
            'sylius.form.type.api_order_promotion_coupon' => 'getSylius_Form_Type_ApiOrderPromotionCouponService.php',
            'sylius.form.type.api_product' => 'getSylius_Form_Type_ApiProductService.php',
            'sylius.form.type.api_product_variant' => 'getSylius_Form_Type_ApiProductVariantService.php',
            'sylius.form.type.attribute_type.select' => 'getSylius_Form_Type_AttributeType_SelectService.php',
            'sylius.form.type.attribute_type.select.choices_collection' => 'getSylius_Form_Type_AttributeType_Select_ChoicesCollectionService.php',
            'sylius.form.type.attribute_type_choice' => 'getSylius_Form_Type_AttributeTypeChoiceService.php',
            'sylius.form.type.autocomplete_product_taxon_choice' => 'getSylius_Form_Type_AutocompleteProductTaxonChoiceService.php',
            'sylius.form.type.avatar_image' => 'getSylius_Form_Type_AvatarImageService.php',
            'sylius.form.type.cart' => 'getSylius_Form_Type_CartService.php',
            'sylius.form.type.cart_item' => 'getSylius_Form_Type_CartItemService.php',
            'sylius.form.type.channel' => 'getSylius_Form_Type_ChannelService.php',
            'sylius.form.type.channel_based_shipping_calculator.flat_rate' => 'getSylius_Form_Type_ChannelBasedShippingCalculator_FlatRateService.php',
            'sylius.form.type.channel_based_shipping_calculator.per_unit_rate' => 'getSylius_Form_Type_ChannelBasedShippingCalculator_PerUnitRateService.php',
            'sylius.form.type.channel_choice' => 'getSylius_Form_Type_ChannelChoiceService.php',
            'sylius.form.type.channel_pricing' => 'getSylius_Form_Type_ChannelPricingService.php',
            'sylius.form.type.channels_collection' => 'getSylius_Form_Type_ChannelsCollectionService.php',
            'sylius.form.type.checkout_address' => 'getSylius_Form_Type_CheckoutAddressService.php',
            'sylius.form.type.checkout_complete' => 'getSylius_Form_Type_CheckoutCompleteService.php',
            'sylius.form.type.checkout_payment' => 'getSylius_Form_Type_CheckoutPaymentService.php',
            'sylius.form.type.checkout_select_payment' => 'getSylius_Form_Type_CheckoutSelectPaymentService.php',
            'sylius.form.type.checkout_select_shipping' => 'getSylius_Form_Type_CheckoutSelectShippingService.php',
            'sylius.form.type.checkout_shipment' => 'getSylius_Form_Type_CheckoutShipmentService.php',
            'sylius.form.type.country' => 'getSylius_Form_Type_CountryService.php',
            'sylius.form.type.country_choice' => 'getSylius_Form_Type_CountryChoiceService.php',
            'sylius.form.type.country_code_choice' => 'getSylius_Form_Type_CountryCodeChoiceService.php',
            'sylius.form.type.currency' => 'getSylius_Form_Type_CurrencyService.php',
            'sylius.form.type.currency_choice' => 'getSylius_Form_Type_CurrencyChoiceService.php',
            'sylius.form.type.customer' => 'getSylius_Form_Type_CustomerService.php',
            'sylius.form.type.customer_choice' => 'getSylius_Form_Type_CustomerChoiceService.php',
            'sylius.form.type.customer_group' => 'getSylius_Form_Type_CustomerGroupService.php',
            'sylius.form.type.customer_group_choice' => 'getSylius_Form_Type_CustomerGroupChoiceService.php',
            'sylius.form.type.customer_group_code_choice' => 'getSylius_Form_Type_CustomerGroupCodeChoiceService.php',
            'sylius.form.type.customer_guest' => 'getSylius_Form_Type_CustomerGuestService.php',
            'sylius.form.type.customer_profile' => 'getSylius_Form_Type_CustomerProfileService.php',
            'sylius.form.type.customer_registration' => 'getSylius_Form_Type_CustomerRegistrationService.php',
            'sylius.form.type.customer_simple_registration' => 'getSylius_Form_Type_CustomerSimpleRegistrationService.php',
            'sylius.form.type.data_transformer.products_to_codes' => 'getSylius_Form_Type_DataTransformer_ProductsToCodesService.php',
            'sylius.form.type.data_transformer.products_to_product_associations' => 'getSylius_Form_Type_DataTransformer_ProductsToProductAssociationsService.php',
            'sylius.form.type.data_transformer.taxons_to_codes' => 'getSylius_Form_Type_DataTransformer_TaxonsToCodesService.php',
            'sylius.form.type.default' => 'getSylius_Form_Type_DefaultService.php',
            'sylius.form.type.exchange_rate' => 'getSylius_Form_Type_ExchangeRateService.php',
            'sylius.form.type.gateway_config' => 'getSylius_Form_Type_GatewayConfigService.php',
            'sylius.form.type.gateway_configuration.paypal' => 'getSylius_Form_Type_GatewayConfiguration_PaypalService.php',
            'sylius.form.type.gateway_configuration.stripe' => 'getSylius_Form_Type_GatewayConfiguration_StripeService.php',
            'sylius.form.type.grid_filter.boolean' => 'getSylius_Form_Type_GridFilter_BooleanService.php',
            'sylius.form.type.grid_filter.date' => 'getSylius_Form_Type_GridFilter_DateService.php',
            'sylius.form.type.grid_filter.entity' => 'getSylius_Form_Type_GridFilter_EntityService.php',
            'sylius.form.type.grid_filter.exists' => 'getSylius_Form_Type_GridFilter_ExistsService.php',
            'sylius.form.type.grid_filter.money' => 'getSylius_Form_Type_GridFilter_MoneyService.php',
            'sylius.form.type.grid_filter.select' => 'getSylius_Form_Type_GridFilter_SelectService.php',
            'sylius.form.type.grid_filter.string' => 'getSylius_Form_Type_GridFilter_StringService.php',
            'sylius.form.type.import' => 'getSylius_Form_Type_ImportService.php',
            'sylius.form.type.locale' => 'getSylius_Form_Type_LocaleService.php',
            'sylius.form.type.locale_choice' => 'getSylius_Form_Type_LocaleChoiceService.php',
            'sylius.form.type.money' => 'getSylius_Form_Type_MoneyService.php',
            'sylius.form.type.order' => 'getSylius_Form_Type_OrderService.php',
            'sylius.form.type.order_item' => 'getSylius_Form_Type_OrderItemService.php',
            'sylius.form.type.payment' => 'getSylius_Form_Type_PaymentService.php',
            'sylius.form.type.payment_gateway_choice' => 'getSylius_Form_Type_PaymentGatewayChoiceService.php',
            'sylius.form.type.payment_method' => 'getSylius_Form_Type_PaymentMethodService.php',
            'sylius.form.type.payment_method_choice' => 'getSylius_Form_Type_PaymentMethodChoiceService.php',
            'sylius.form.type.payment_method_translation' => 'getSylius_Form_Type_PaymentMethodTranslationService.php',
            'sylius.form.type.product' => 'getSylius_Form_Type_ProductService.php',
            'sylius.form.type.product_association' => 'getSylius_Form_Type_ProductAssociationService.php',
            'sylius.form.type.product_association_type' => 'getSylius_Form_Type_ProductAssociationTypeService.php',
            'sylius.form.type.product_association_type_choice' => 'getSylius_Form_Type_ProductAssociationTypeChoiceService.php',
            'sylius.form.type.product_association_type_translation' => 'getSylius_Form_Type_ProductAssociationTypeTranslationService.php',
            'sylius.form.type.product_attribute' => 'getSylius_Form_Type_ProductAttributeService.php',
            'sylius.form.type.product_attribute_choice' => 'getSylius_Form_Type_ProductAttributeChoiceService.php',
            'sylius.form.type.product_attribute_translation' => 'getSylius_Form_Type_ProductAttributeTranslationService.php',
            'sylius.form.type.product_attribute_value' => 'getSylius_Form_Type_ProductAttributeValueService.php',
            'sylius.form.type.product_choice' => 'getSylius_Form_Type_ProductChoiceService.php',
            'sylius.form.type.product_code_choice' => 'getSylius_Form_Type_ProductCodeChoiceService.php',
            'sylius.form.type.product_generate_variants' => 'getSylius_Form_Type_ProductGenerateVariantsService.php',
            'sylius.form.type.product_image' => 'getSylius_Form_Type_ProductImageService.php',
            'sylius.form.type.product_option' => 'getSylius_Form_Type_ProductOptionService.php',
            'sylius.form.type.product_option_choice' => 'getSylius_Form_Type_ProductOptionChoiceService.php',
            'sylius.form.type.product_option_translation' => 'getSylius_Form_Type_ProductOptionTranslationService.php',
            'sylius.form.type.product_option_value' => 'getSylius_Form_Type_ProductOptionValueService.php',
            'sylius.form.type.product_option_value_translation' => 'getSylius_Form_Type_ProductOptionValueTranslationService.php',
            'sylius.form.type.product_review' => 'getSylius_Form_Type_ProductReviewService.php',
            'sylius.form.type.product_translation' => 'getSylius_Form_Type_ProductTranslationService.php',
            'sylius.form.type.product_variant' => 'getSylius_Form_Type_ProductVariantService.php',
            'sylius.form.type.product_variant_generation' => 'getSylius_Form_Type_ProductVariantGenerationService.php',
            'sylius.form.type.product_variant_translation' => 'getSylius_Form_Type_ProductVariantTranslationService.php',
            'sylius.form.type.promotion' => 'getSylius_Form_Type_PromotionService.php',
            'sylius.form.type.promotion_action' => 'getSylius_Form_Type_PromotionActionService.php',
            'sylius.form.type.promotion_action.collection' => 'getSylius_Form_Type_PromotionAction_CollectionService.php',
            'sylius.form.type.promotion_action.filter.product' => 'getSylius_Form_Type_PromotionAction_Filter_ProductService.php',
            'sylius.form.type.promotion_action.filter.taxon' => 'getSylius_Form_Type_PromotionAction_Filter_TaxonService.php',
            'sylius.form.type.promotion_action_choice' => 'getSylius_Form_Type_PromotionActionChoiceService.php',
            'sylius.form.type.promotion_coupon' => 'getSylius_Form_Type_PromotionCouponService.php',
            'sylius.form.type.promotion_coupon_generator_instruction' => 'getSylius_Form_Type_PromotionCouponGeneratorInstructionService.php',
            'sylius.form.type.promotion_coupon_to_code' => 'getSylius_Form_Type_PromotionCouponToCodeService.php',
            'sylius.form.type.promotion_rule' => 'getSylius_Form_Type_PromotionRuleService.php',
            'sylius.form.type.promotion_rule.collection' => 'getSylius_Form_Type_PromotionRule_CollectionService.php',
            'sylius.form.type.promotion_rule.contains_product_configuration' => 'getSylius_Form_Type_PromotionRule_ContainsProductConfigurationService.php',
            'sylius.form.type.promotion_rule.customer_group_configuration' => 'getSylius_Form_Type_PromotionRule_CustomerGroupConfigurationService.php',
            'sylius.form.type.promotion_rule.has_taxon_configuration' => 'getSylius_Form_Type_PromotionRule_HasTaxonConfigurationService.php',
            'sylius.form.type.promotion_rule.total_of_items_from_taxon_configuration' => 'getSylius_Form_Type_PromotionRule_TotalOfItemsFromTaxonConfigurationService.php',
            'sylius.form.type.promotion_rule_choice' => 'getSylius_Form_Type_PromotionRuleChoiceService.php',
            'sylius.form.type.province' => 'getSylius_Form_Type_ProvinceService.php',
            'sylius.form.type.province_choice' => 'getSylius_Form_Type_ProvinceChoiceService.php',
            'sylius.form.type.province_code_choice' => 'getSylius_Form_Type_ProvinceCodeChoiceService.php',
            'sylius.form.type.resource_autocomplete_choice' => 'getSylius_Form_Type_ResourceAutocompleteChoiceService.php',
            'sylius.form.type.resource_translations' => 'getSylius_Form_Type_ResourceTranslationsService.php',
            'sylius.form.type.security_login' => 'getSylius_Form_Type_SecurityLoginService.php',
            'sylius.form.type.shipment' => 'getSylius_Form_Type_ShipmentService.php',
            'sylius.form.type.shipment_ship' => 'getSylius_Form_Type_ShipmentShipService.php',
            'sylius.form.type.shipping_calculator_choice' => 'getSylius_Form_Type_ShippingCalculatorChoiceService.php',
            'sylius.form.type.shipping_category' => 'getSylius_Form_Type_ShippingCategoryService.php',
            'sylius.form.type.shipping_category_choice' => 'getSylius_Form_Type_ShippingCategoryChoiceService.php',
            'sylius.form.type.shipping_method' => 'getSylius_Form_Type_ShippingMethodService.php',
            'sylius.form.type.shipping_method_choice' => 'getSylius_Form_Type_ShippingMethodChoiceService.php',
            'sylius.form.type.shipping_method_translation' => 'getSylius_Form_Type_ShippingMethodTranslationService.php',
            'sylius.form.type.shop_billing_data' => 'getSylius_Form_Type_ShopBillingDataService.php',
            'sylius.form.type.shop_user' => 'getSylius_Form_Type_ShopUserService.php',
            'sylius.form.type.shop_user_registration' => 'getSylius_Form_Type_ShopUserRegistrationService.php',
            'sylius.form.type.sylius_product_associations' => 'getSylius_Form_Type_SyliusProductAssociationsService.php',
            'sylius.form.type.tax_calculation_strategy_choice' => 'getSylius_Form_Type_TaxCalculationStrategyChoiceService.php',
            'sylius.form.type.tax_calculator_choice' => 'getSylius_Form_Type_TaxCalculatorChoiceService.php',
            'sylius.form.type.tax_category' => 'getSylius_Form_Type_TaxCategoryService.php',
            'sylius.form.type.tax_category_choice' => 'getSylius_Form_Type_TaxCategoryChoiceService.php',
            'sylius.form.type.tax_rate' => 'getSylius_Form_Type_TaxRateService.php',
            'sylius.form.type.taxon' => 'getSylius_Form_Type_TaxonService.php',
            'sylius.form.type.taxon_image' => 'getSylius_Form_Type_TaxonImageService.php',
            'sylius.form.type.taxon_position' => 'getSylius_Form_Type_TaxonPositionService.php',
            'sylius.form.type.taxon_translation' => 'getSylius_Form_Type_TaxonTranslationService.php',
            'sylius.form.type.user_change_password' => 'getSylius_Form_Type_UserChangePasswordService.php',
            'sylius.form.type.user_login' => 'getSylius_Form_Type_UserLoginService.php',
            'sylius.form.type.user_request_password_reset' => 'getSylius_Form_Type_UserRequestPasswordResetService.php',
            'sylius.form.type.user_reset_password' => 'getSylius_Form_Type_UserResetPasswordService.php',
            'sylius.form.type.zone' => 'getSylius_Form_Type_ZoneService.php',
            'sylius.form.type.zone_choice' => 'getSylius_Form_Type_ZoneChoiceService.php',
            'sylius.form.type.zone_code_choice' => 'getSylius_Form_Type_ZoneCodeChoiceService.php',
            'sylius.form.type.zone_member' => 'getSylius_Form_Type_ZoneMemberService.php',
            'sylius.form_registry.attribute_type' => 'getSylius_FormRegistry_AttributeTypeService.php',
            'sylius.form_registry.payum_gateway_config' => 'getSylius_FormRegistry_PayumGatewayConfigService.php',
            'sylius.form_registry.promotion_action' => 'getSylius_FormRegistry_PromotionActionService.php',
            'sylius.form_registry.promotion_rule_checker' => 'getSylius_FormRegistry_PromotionRuleCheckerService.php',
            'sylius.form_registry.shipping_calculator' => 'getSylius_FormRegistry_ShippingCalculatorService.php',
            'sylius.generator.product_variant' => 'getSylius_Generator_ProductVariantService.php',
            'sylius.generator.slug' => 'getSylius_Generator_SlugService.php',
            'sylius.generator.taxon_slug' => 'getSylius_Generator_TaxonSlugService.php',
            'sylius.grid.array_to_definition_converter' => 'getSylius_Grid_ArrayToDefinitionConverterService.php',
            'sylius.grid.data_extractor.property_access' => 'getSylius_Grid_DataExtractor_PropertyAccessService.php',
            'sylius.grid.data_source_provider' => 'getSylius_Grid_DataSourceProviderService.php',
            'sylius.grid.filters_applicator' => 'getSylius_Grid_FiltersApplicatorService.php',
            'sylius.grid.filters_criteria_resolver' => 'getSylius_Grid_FiltersCriteriaResolverService.php',
            'sylius.grid.provider' => 'getSylius_Grid_ProviderService.php',
            'sylius.grid.sorter' => 'getSylius_Grid_SorterService.php',
            'sylius.grid.view_factory' => 'getSylius_Grid_ViewFactoryService.php',
            'sylius.grid_driver.doctrine.dbal' => 'getSylius_GridDriver_Doctrine_DbalService.php',
            'sylius.grid_field.datetime' => 'getSylius_GridField_DatetimeService.php',
            'sylius.grid_field.string' => 'getSylius_GridField_StringService.php',
            'sylius.grid_field.twig' => 'getSylius_GridField_TwigService.php',
            'sylius.grid_filter.boolean' => 'getSylius_GridFilter_BooleanService.php',
            'sylius.grid_filter.date' => 'getSylius_GridFilter_DateService.php',
            'sylius.grid_filter.entity' => 'getSylius_GridFilter_EntityService.php',
            'sylius.grid_filter.exists' => 'getSylius_GridFilter_ExistsService.php',
            'sylius.grid_filter.money' => 'getSylius_GridFilter_MoneyService.php',
            'sylius.grid_filter.select' => 'getSylius_GridFilter_SelectService.php',
            'sylius.grid_filter.shop_string' => 'getSylius_GridFilter_ShopStringService.php',
            'sylius.grid_filter.string' => 'getSylius_GridFilter_StringService.php',
            'sylius.handler.shop_user_logout' => 'getSylius_Handler_ShopUserLogoutService.php',
            'sylius.image_uploader' => 'getSylius_ImageUploaderService.php',
            'sylius.importers_registry' => 'getSylius_ImportersRegistryService.php',
            'sylius.installer.checker.command_directory' => 'getSylius_Installer_Checker_CommandDirectoryService.php',
            'sylius.installer.checker.sylius_requirements' => 'getSylius_Installer_Checker_SyliusRequirementsService.php',
            'sylius.integer_distributor' => 'getSylius_IntegerDistributorService.php',
            'sylius.invoice_number_generator' => 'getSylius_InvoiceNumberGeneratorService.php',
            'sylius.listener.api.add_to_cart' => 'getSylius_Listener_Api_AddToCartService.php',
            'sylius.listener.avatar_upload' => 'getSylius_Listener_AvatarUploadService.php',
            'sylius.listener.canonicalizer' => 'getSylius_Listener_CanonicalizerService.php',
            'sylius.listener.cart_blamer' => 'getSylius_Listener_CartBlamerService.php',
            'sylius.listener.channel' => 'getSylius_Listener_ChannelService.php',
            'sylius.listener.customer_default_address' => 'getSylius_Listener_CustomerDefaultAddressService.php',
            'sylius.listener.default_username' => 'getSylius_Listener_DefaultUsernameService.php',
            'sylius.listener.email_updater' => 'getSylius_Listener_EmailUpdaterService.php',
            'sylius.listener.images_remove' => 'getSylius_Listener_ImagesRemoveService.php',
            'sylius.listener.images_upload' => 'getSylius_Listener_ImagesUploadService.php',
            'sylius.listener.locking' => 'getSylius_Listener_LockingService.php',
            'sylius.listener.order_complete' => 'getSylius_Listener_OrderCompleteService.php',
            'sylius.listener.order_customer_ip' => 'getSylius_Listener_OrderCustomerIpService.php',
            'sylius.listener.order_integrity_checker' => 'getSylius_Listener_OrderIntegrityCheckerService.php',
            'sylius.listener.order_recalculation' => 'getSylius_Listener_OrderRecalculationService.php',
            'sylius.listener.password_updater' => 'getSylius_Listener_PasswordUpdaterService.php',
            'sylius.listener.product_review_change' => 'getSylius_Listener_ProductReviewChangeService.php',
            'sylius.listener.review_create' => 'getSylius_Listener_ReviewCreateService.php',
            'sylius.listener.shipment_ship' => 'getSylius_Listener_ShipmentShipService.php',
            'sylius.listener.simple_product_locking' => 'getSylius_Listener_SimpleProductLockingService.php',
            'sylius.listener.taxon_deletion' => 'getSylius_Listener_TaxonDeletionService.php',
            'sylius.listener.user_cart_recalculation' => 'getSylius_Listener_UserCartRecalculationService.php',
            'sylius.listener.user_impersonated' => 'getSylius_Listener_UserImpersonatedService.php',
            'sylius.listener.user_mailer_listener' => 'getSylius_Listener_UserMailerListenerService.php',
            'sylius.listener.user_registration' => 'getSylius_Listener_UserRegistrationService.php',
            'sylius.locale_converter' => 'getSylius_LocaleConverterService.php',
            'sylius.mailer.default_settings_provider' => 'getSylius_Mailer_DefaultSettingsProviderService.php',
            'sylius.money_formatter' => 'getSylius_MoneyFormatterService.php',
            'sylius.oauth.user_provider' => 'getSylius_Oauth_UserProviderService.php',
            'sylius.oauth_server.client_manager' => 'getSylius_OauthServer_ClientManagerService.php',
            'sylius.oauth_user.pin_generator.password_reset' => 'getSylius_OauthUser_PinGenerator_PasswordResetService.php',
            'sylius.oauth_user.token_generator.email_verification' => 'getSylius_OauthUser_TokenGenerator_EmailVerificationService.php',
            'sylius.oauth_user.token_generator.password_reset' => 'getSylius_OauthUser_TokenGenerator_PasswordResetService.php',
            'sylius.order_item_names_setter' => 'getSylius_OrderItemNamesSetterService.php',
            'sylius.order_item_quantity_modifier.limiting' => 'getSylius_OrderItemQuantityModifier_LimitingService.php',
            'sylius.order_locale_assigner' => 'getSylius_OrderLocaleAssignerService.php',
            'sylius.order_modifier' => 'getSylius_OrderModifierService.php',
            'sylius.order_number_assigner' => 'getSylius_OrderNumberAssignerService.php',
            'sylius.order_payment_provider' => 'getSylius_OrderPaymentProviderService.php',
            'sylius.order_processing.order_adjustments_clearer' => 'getSylius_OrderProcessing_OrderAdjustmentsClearerService.php',
            'sylius.order_processing.order_payment_processor.after_checkout' => 'getSylius_OrderProcessing_OrderPaymentProcessor_AfterCheckoutService.php',
            'sylius.order_processing.order_payment_processor.checkout' => 'getSylius_OrderProcessing_OrderPaymentProcessor_CheckoutService.php',
            'sylius.order_processing.order_prices_recalculator' => 'getSylius_OrderProcessing_OrderPricesRecalculatorService.php',
            'sylius.order_processing.order_processor' => 'getSylius_OrderProcessing_OrderProcessorService.php',
            'sylius.order_processing.order_promotion_processor' => 'getSylius_OrderProcessing_OrderPromotionProcessorService.php',
            'sylius.order_processing.order_shipment_processor' => 'getSylius_OrderProcessing_OrderShipmentProcessorService.php',
            'sylius.order_processing.order_taxes_processor' => 'getSylius_OrderProcessing_OrderTaxesProcessorService.php',
            'sylius.order_shipping_state.exporter' => 'getSylius_OrderShippingState_ExporterService.php',
            'sylius.payment_description_provider' => 'getSylius_PaymentDescriptionProviderService.php',
            'sylius.payment_method_resolver.default' => 'getSylius_PaymentMethodResolver_DefaultService.php',
            'sylius.payment_methods_resolver' => 'getSylius_PaymentMethodsResolverService.php',
            'sylius.payment_methods_resolver.channel_based' => 'getSylius_PaymentMethodsResolver_ChannelBasedService.php',
            'sylius.payum.http_client' => 'getSylius_Payum_HttpClientService.php',
            'sylius.payum_action.authorize_payment' => 'getSylius_PayumAction_AuthorizePaymentService.php',
            'sylius.payum_action.capture_payment' => 'getSylius_PayumAction_CapturePaymentService.php',
            'sylius.payum_action.execute_same_request_with_payment_details' => 'getSylius_PayumAction_ExecuteSameRequestWithPaymentDetailsService.php',
            'sylius.payum_action.offline.convert_payment' => 'getSylius_PayumAction_Offline_ConvertPaymentService.php',
            'sylius.payum_action.offline.resolve_next_route' => 'getSylius_PayumAction_Offline_ResolveNextRouteService.php',
            'sylius.payum_action.paypal_express_checkout.convert_payment' => 'getSylius_PayumAction_PaypalExpressCheckout_ConvertPaymentService.php',
            'sylius.payum_action.resolve_next_route' => 'getSylius_PayumAction_ResolveNextRouteService.php',
            'sylius.payum_extension.update_payment_state' => 'getSylius_PayumExtension_UpdatePaymentStateService.php',
            'sylius.product_review.average_rating_updater' => 'getSylius_ProductReview_AverageRatingUpdaterService.php',
            'sylius.product_variant_resolver.default' => 'getSylius_ProductVariantResolver_DefaultService.php',
            'sylius.promotion.units_promotion_adjustments_applicator' => 'getSylius_Promotion_UnitsPromotionAdjustmentsApplicatorService.php',
            'sylius.promotion_action.fixed_discount' => 'getSylius_PromotionAction_FixedDiscountService.php',
            'sylius.promotion_action.percentage_discount' => 'getSylius_PromotionAction_PercentageDiscountService.php',
            'sylius.promotion_action.shipping_percentage_discount' => 'getSylius_PromotionAction_ShippingPercentageDiscountService.php',
            'sylius.promotion_action.unit_fixed_discount' => 'getSylius_PromotionAction_UnitFixedDiscountService.php',
            'sylius.promotion_action.unit_percentage_discount' => 'getSylius_PromotionAction_UnitPercentageDiscountService.php',
            'sylius.promotion_applicator' => 'getSylius_PromotionApplicatorService.php',
            'sylius.promotion_coupon_eligibility_checker' => 'getSylius_PromotionCouponEligibilityCheckerService.php',
            'sylius.promotion_coupon_generator' => 'getSylius_PromotionCouponGeneratorService.php',
            'sylius.promotion_coupon_generator.percentage_policy' => 'getSylius_PromotionCouponGenerator_PercentagePolicyService.php',
            'sylius.promotion_eligibility_checker' => 'getSylius_PromotionEligibilityCheckerService.php',
            'sylius.promotion_filter.price_range' => 'getSylius_PromotionFilter_PriceRangeService.php',
            'sylius.promotion_filter.product' => 'getSylius_PromotionFilter_ProductService.php',
            'sylius.promotion_filter.taxon' => 'getSylius_PromotionFilter_TaxonService.php',
            'sylius.promotion_processor' => 'getSylius_PromotionProcessorService.php',
            'sylius.promotion_rule_checker.cart_quantity' => 'getSylius_PromotionRuleChecker_CartQuantityService.php',
            'sylius.promotion_rule_checker.contains_product' => 'getSylius_PromotionRuleChecker_ContainsProductService.php',
            'sylius.promotion_rule_checker.has_taxon' => 'getSylius_PromotionRuleChecker_HasTaxonService.php',
            'sylius.promotion_rule_checker.item_total' => 'getSylius_PromotionRuleChecker_ItemTotalService.php',
            'sylius.promotion_rule_checker.nth_order' => 'getSylius_PromotionRuleChecker_NthOrderService.php',
            'sylius.promotion_rule_checker.shipping_country' => 'getSylius_PromotionRuleChecker_ShippingCountryService.php',
            'sylius.promotion_rule_checker.total_of_items_from_taxon' => 'getSylius_PromotionRuleChecker_TotalOfItemsFromTaxonService.php',
            'sylius.promotion_rule_updater.has_taxon' => 'getSylius_PromotionRuleUpdater_HasTaxonService.php',
            'sylius.promotion_rule_updater.total_of_items_from_taxon' => 'getSylius_PromotionRuleUpdater_TotalOfItemsFromTaxonService.php',
            'sylius.promotion_usage_modifier' => 'getSylius_PromotionUsageModifierService.php',
            'sylius.proportional_integer_distributor' => 'getSylius_ProportionalIntegerDistributorService.php',
            'sylius.provider.channel_based_default_zone_provider' => 'getSylius_Provider_ChannelBasedDefaultZoneProviderService.php',
            'sylius.provider.product_variants_prices' => 'getSylius_Provider_ProductVariantsPricesService.php',
            'sylius.province_naming_provider' => 'getSylius_ProvinceNamingProviderService.php',
            'sylius.random_generator' => 'getSylius_RandomGeneratorService.php',
            'sylius.registry.attribute_type' => 'getSylius_Registry_AttributeTypeService.php',
            'sylius.registry.grid_driver' => 'getSylius_Registry_GridDriverService.php',
            'sylius.registry.grid_field' => 'getSylius_Registry_GridFieldService.php',
            'sylius.registry.grid_filter' => 'getSylius_Registry_GridFilterService.php',
            'sylius.registry.payment_methods_resolver' => 'getSylius_Registry_PaymentMethodsResolverService.php',
            'sylius.registry.shipping_calculator' => 'getSylius_Registry_ShippingCalculatorService.php',
            'sylius.registry.shipping_methods_resolver' => 'getSylius_Registry_ShippingMethodsResolverService.php',
            'sylius.registry.tax_calculation_strategy' => 'getSylius_Registry_TaxCalculationStrategyService.php',
            'sylius.registry.tax_calculator' => 'getSylius_Registry_TaxCalculatorService.php',
            'sylius.registry_promotion_action' => 'getSylius_RegistryPromotionActionService.php',
            'sylius.registry_promotion_rule_checker' => 'getSylius_RegistryPromotionRuleCheckerService.php',
            'sylius.repository.address' => 'getSylius_Repository_AddressService.php',
            'sylius.repository.address_log_entry' => 'getSylius_Repository_AddressLogEntryService.php',
            'sylius.repository.adjustment' => 'getSylius_Repository_AdjustmentService.php',
            'sylius.repository.admin_user' => 'getSylius_Repository_AdminUserService.php',
            'sylius.repository.api_access_token' => 'getSylius_Repository_ApiAccessTokenService.php',
            'sylius.repository.api_auth_code' => 'getSylius_Repository_ApiAuthCodeService.php',
            'sylius.repository.api_client' => 'getSylius_Repository_ApiClientService.php',
            'sylius.repository.api_refresh_token' => 'getSylius_Repository_ApiRefreshTokenService.php',
            'sylius.repository.api_user' => 'getSylius_Repository_ApiUserService.php',
            'sylius.repository.avatar_image' => 'getSylius_Repository_AvatarImageService.php',
            'sylius.repository.channel_pricing' => 'getSylius_Repository_ChannelPricingService.php',
            'sylius.repository.country' => 'getSylius_Repository_CountryService.php',
            'sylius.repository.currency' => 'getSylius_Repository_CurrencyService.php',
            'sylius.repository.customer' => 'getSylius_Repository_CustomerService.php',
            'sylius.repository.customer_group' => 'getSylius_Repository_CustomerGroupService.php',
            'sylius.repository.exchange_rate' => 'getSylius_Repository_ExchangeRateService.php',
            'sylius.repository.gateway_config' => 'getSylius_Repository_GatewayConfigService.php',
            'sylius.repository.inventory_unit' => 'getSylius_Repository_InventoryUnitService.php',
            'sylius.repository.locale' => 'getSylius_Repository_LocaleService.php',
            'sylius.repository.oauth_user' => 'getSylius_Repository_OauthUserService.php',
            'sylius.repository.order_item' => 'getSylius_Repository_OrderItemService.php',
            'sylius.repository.order_item_unit' => 'getSylius_Repository_OrderItemUnitService.php',
            'sylius.repository.order_sequence' => 'getSylius_Repository_OrderSequenceService.php',
            'sylius.repository.payment' => 'getSylius_Repository_PaymentService.php',
            'sylius.repository.payment_method' => 'getSylius_Repository_PaymentMethodService.php',
            'sylius.repository.payment_method_translation' => 'getSylius_Repository_PaymentMethodTranslationService.php',
            'sylius.repository.payment_security_token' => 'getSylius_Repository_PaymentSecurityTokenService.php',
            'sylius.repository.product' => 'getSylius_Repository_ProductService.php',
            'sylius.repository.product_association' => 'getSylius_Repository_ProductAssociationService.php',
            'sylius.repository.product_association_type' => 'getSylius_Repository_ProductAssociationTypeService.php',
            'sylius.repository.product_association_type_translation' => 'getSylius_Repository_ProductAssociationTypeTranslationService.php',
            'sylius.repository.product_attribute' => 'getSylius_Repository_ProductAttributeService.php',
            'sylius.repository.product_attribute_translation' => 'getSylius_Repository_ProductAttributeTranslationService.php',
            'sylius.repository.product_attribute_value' => 'getSylius_Repository_ProductAttributeValueService.php',
            'sylius.repository.product_option' => 'getSylius_Repository_ProductOptionService.php',
            'sylius.repository.product_option_translation' => 'getSylius_Repository_ProductOptionTranslationService.php',
            'sylius.repository.product_option_value' => 'getSylius_Repository_ProductOptionValueService.php',
            'sylius.repository.product_option_value_translation' => 'getSylius_Repository_ProductOptionValueTranslationService.php',
            'sylius.repository.product_review' => 'getSylius_Repository_ProductReviewService.php',
            'sylius.repository.product_reviewer' => 'getSylius_Repository_ProductReviewerService.php',
            'sylius.repository.product_taxon' => 'getSylius_Repository_ProductTaxonService.php',
            'sylius.repository.product_translation' => 'getSylius_Repository_ProductTranslationService.php',
            'sylius.repository.product_variant' => 'getSylius_Repository_ProductVariantService.php',
            'sylius.repository.product_variant_translation' => 'getSylius_Repository_ProductVariantTranslationService.php',
            'sylius.repository.promotion' => 'getSylius_Repository_PromotionService.php',
            'sylius.repository.promotion_action' => 'getSylius_Repository_PromotionActionService.php',
            'sylius.repository.promotion_coupon' => 'getSylius_Repository_PromotionCouponService.php',
            'sylius.repository.promotion_rule' => 'getSylius_Repository_PromotionRuleService.php',
            'sylius.repository.promotion_subject' => 'getSylius_Repository_PromotionSubjectService.php',
            'sylius.repository.province' => 'getSylius_Repository_ProvinceService.php',
            'sylius.repository.shipment' => 'getSylius_Repository_ShipmentService.php',
            'sylius.repository.shipment_unit' => 'getSylius_Repository_ShipmentUnitService.php',
            'sylius.repository.shipping_category' => 'getSylius_Repository_ShippingCategoryService.php',
            'sylius.repository.shipping_method' => 'getSylius_Repository_ShippingMethodService.php',
            'sylius.repository.shipping_method_translation' => 'getSylius_Repository_ShippingMethodTranslationService.php',
            'sylius.repository.shop_user' => 'getSylius_Repository_ShopUserService.php',
            'sylius.repository.tax_category' => 'getSylius_Repository_TaxCategoryService.php',
            'sylius.repository.tax_rate' => 'getSylius_Repository_TaxRateService.php',
            'sylius.repository.taxon' => 'getSylius_Repository_TaxonService.php',
            'sylius.repository.taxon_image' => 'getSylius_Repository_TaxonImageService.php',
            'sylius.repository.taxon_translation' => 'getSylius_Repository_TaxonTranslationService.php',
            'sylius.repository.theme' => 'getSylius_Repository_ThemeService.php',
            'sylius.repository.zone' => 'getSylius_Repository_ZoneService.php',
            'sylius.repository.zone_member' => 'getSylius_Repository_ZoneMemberService.php',
            'sylius.requirements' => 'getSylius_RequirementsService.php',
            'sylius.resource_controller.resources_resolver.grid_aware' => 'getSylius_ResourceController_ResourcesResolver_GridAwareService.php',
            'sylius.reviewer_reviews_remover' => 'getSylius_ReviewerReviewsRemoverService.php',
            'sylius.section_resolver.admin_api_uri_based_section_resolver' => 'getSylius_SectionResolver_AdminApiUriBasedSectionResolverService.php',
            'sylius.section_resolver.admin_uri_based_section_resolver' => 'getSylius_SectionResolver_AdminUriBasedSectionResolverService.php',
            'sylius.section_resolver.shop_uri_based_section_resolver' => 'getSylius_SectionResolver_ShopUriBasedSectionResolverService.php',
            'sylius.section_resolver.uri_based_section_resolver' => 'getSylius_SectionResolver_UriBasedSectionResolverService.php',
            'sylius.security.password_encoder' => 'getSylius_Security_PasswordEncoderService.php',
            'sylius.security.password_updater' => 'getSylius_Security_PasswordUpdaterService.php',
            'sylius.security.user_login' => 'getSylius_Security_UserLoginService.php',
            'sylius.sequential_order_number_generator' => 'getSylius_SequentialOrderNumberGeneratorService.php',
            'sylius.setup.channel' => 'getSylius_Setup_ChannelService.php',
            'sylius.setup.currency' => 'getSylius_Setup_CurrencyService.php',
            'sylius.setup.locale' => 'getSylius_Setup_LocaleService.php',
            'sylius.shipping_calculator' => 'getSylius_ShippingCalculatorService.php',
            'sylius.shipping_calculator.flat_rate' => 'getSylius_ShippingCalculator_FlatRateService.php',
            'sylius.shipping_calculator.per_unit_rate' => 'getSylius_ShippingCalculator_PerUnitRateService.php',
            'sylius.shipping_eligibility_checker' => 'getSylius_ShippingEligibilityCheckerService.php',
            'sylius.shipping_method_resolver.default' => 'getSylius_ShippingMethodResolver_DefaultService.php',
            'sylius.shipping_methods_resolver' => 'getSylius_ShippingMethodsResolverService.php',
            'sylius.shipping_methods_resolver.default' => 'getSylius_ShippingMethodsResolver_DefaultService.php',
            'sylius.shop.menu_builder.account' => 'getSylius_Shop_MenuBuilder_AccountService.php',
            'sylius.shop_user.pin_generator.password_reset' => 'getSylius_ShopUser_PinGenerator_PasswordResetService.php',
            'sylius.shop_user.token_generator.email_verification' => 'getSylius_ShopUser_TokenGenerator_EmailVerificationService.php',
            'sylius.shop_user.token_generator.password_reset' => 'getSylius_ShopUser_TokenGenerator_PasswordResetService.php',
            'sylius.state_resolver.order' => 'getSylius_StateResolver_OrderService.php',
            'sylius.state_resolver.order_checkout' => 'getSylius_StateResolver_OrderCheckoutService.php',
            'sylius.state_resolver.order_payment' => 'getSylius_StateResolver_OrderPaymentService.php',
            'sylius.state_resolver.order_shipping' => 'getSylius_StateResolver_OrderShippingService.php',
            'sylius.storage.session' => 'getSylius_Storage_SessionService.php',
            'sylius.tax_calculator' => 'getSylius_TaxCalculatorService.php',
            'sylius.tax_calculator.default' => 'getSylius_TaxCalculator_DefaultService.php',
            'sylius.tax_rate_resolver' => 'getSylius_TaxRateResolverService.php',
            'sylius.taxation.order_item_units_based_strategy' => 'getSylius_Taxation_OrderItemUnitsBasedStrategyService.php',
            'sylius.taxation.order_item_units_taxes_applicator' => 'getSylius_Taxation_OrderItemUnitsTaxesApplicatorService.php',
            'sylius.taxation.order_items_based_strategy' => 'getSylius_Taxation_OrderItemsBasedStrategyService.php',
            'sylius.taxation.order_items_taxes_applicator' => 'getSylius_Taxation_OrderItemsTaxesApplicatorService.php',
            'sylius.taxation.order_shipment_taxes_applicator' => 'getSylius_Taxation_OrderShipmentTaxesApplicatorService.php',
            'sylius.templating.helper.adjustment' => 'getSylius_Templating_Helper_AdjustmentService.php',
            'sylius.templating.helper.bulk_action_grid' => 'getSylius_Templating_Helper_BulkActionGridService.php',
            'sylius.templating.helper.checkout_steps' => 'getSylius_Templating_Helper_CheckoutStepsService.php',
            'sylius.templating.helper.convert_money' => 'getSylius_Templating_Helper_ConvertMoneyService.php',
            'sylius.templating.helper.currency' => 'getSylius_Templating_Helper_CurrencyService.php',
            'sylius.templating.helper.format_money' => 'getSylius_Templating_Helper_FormatMoneyService.php',
            'sylius.templating.helper.grid' => 'getSylius_Templating_Helper_GridService.php',
            'sylius.templating.helper.inventory' => 'getSylius_Templating_Helper_InventoryService.php',
            'sylius.templating.helper.locale' => 'getSylius_Templating_Helper_LocaleService.php',
            'sylius.templating.helper.price' => 'getSylius_Templating_Helper_PriceService.php',
            'sylius.templating.helper.product_variants_prices' => 'getSylius_Templating_Helper_ProductVariantsPricesService.php',
            'sylius.templating.helper.variant_resolver' => 'getSylius_Templating_Helper_VariantResolverService.php',
            'sylius.theme.asset.assets_installer.output_aware' => 'getSylius_Theme_Asset_AssetsInstaller_OutputAwareService.php',
            'sylius.theme.configuration.provider' => 'getSylius_Theme_Configuration_ProviderService.php',
            'sylius.theme.locator.application_resource' => 'getSylius_Theme_Locator_ApplicationResourceService.php',
            'sylius.theme.locator.bundle_resource' => 'getSylius_Theme_Locator_BundleResourceService.php',
            'sylius.theme.templating.cache' => 'getSylius_Theme_Templating_CacheService.php',
            'sylius.translatable_entity_locale_assigner' => 'getSylius_TranslatableEntityLocaleAssignerService.php',
            'sylius.translation.translatable_listener.doctrine.orm' => 'getSylius_Translation_TranslatableListener_Doctrine_OrmService.php',
            'sylius.translation_locale_provider.admin' => 'getSylius_TranslationLocaleProvider_AdminService.php',
            'sylius.translation_locale_provider.immutable' => 'getSylius_TranslationLocaleProvider_ImmutableService.php',
            'sylius.twig.extension.channels_currencies' => 'getSylius_Twig_Extension_ChannelsCurrenciesService.php',
            'sylius.twig.extension.country_name' => 'getSylius_Twig_Extension_CountryNameService.php',
            'sylius.twig.extension.province_naming' => 'getSylius_Twig_Extension_ProvinceNamingService.php',
            'sylius.twig.extension.shop' => 'getSylius_Twig_Extension_ShopService.php',
            'sylius.twig.extension.sylius_bundle_loaded_checker' => 'getSylius_Twig_Extension_SyliusBundleLoadedCheckerService.php',
            'sylius.twig.extension.widget.admin_notification' => 'getSylius_Twig_Extension_Widget_AdminNotificationService.php',
            'sylius.unique_id_based_order_token_assigner' => 'getSylius_UniqueIdBasedOrderTokenAssignerService.php',
            'sylius.unpaid_orders_state_updater' => 'getSylius_UnpaidOrdersStateUpdaterService.php',
            'sylius.validator.cart_item_availability' => 'getSylius_Validator_CartItemAvailabilityService.php',
            'sylius.validator.channel_default_locale_enabled' => 'getSylius_Validator_ChannelDefaultLocaleEnabledService.php',
            'sylius.validator.customer_initializer' => 'getSylius_Validator_CustomerInitializerService.php',
            'sylius.validator.date_range' => 'getSylius_Validator_DateRangeService.php',
            'sylius.validator.different_source_target_currency' => 'getSylius_Validator_DifferentSourceTargetCurrencyService.php',
            'sylius.validator.has_all_prices_defined' => 'getSylius_Validator_HasAllPricesDefinedService.php',
            'sylius.validator.has_all_variant_prices_defined' => 'getSylius_Validator_HasAllVariantPricesDefinedService.php',
            'sylius.validator.has_enabled_entity' => 'getSylius_Validator_HasEnabledEntityService.php',
            'sylius.validator.in_stock' => 'getSylius_Validator_InStockService.php',
            'sylius.validator.locales_aware_valid_attribute_value' => 'getSylius_Validator_LocalesAwareValidAttributeValueService.php',
            'sylius.validator.payment_method_integrity' => 'getSylius_Validator_PaymentMethodIntegrityService.php',
            'sylius.validator.product_code_uniqueness' => 'getSylius_Validator_ProductCodeUniquenessService.php',
            'sylius.validator.product_integrity' => 'getSylius_Validator_ProductIntegrityService.php',
            'sylius.validator.product_variant_combination' => 'getSylius_Validator_ProductVariantCombinationService.php',
            'sylius.validator.promotion_coupon_generation_amount' => 'getSylius_Validator_PromotionCouponGenerationAmountService.php',
            'sylius.validator.promotion_subject_coupon' => 'getSylius_Validator_PromotionSubjectCouponService.php',
            'sylius.validator.shipping_method_integrity' => 'getSylius_Validator_ShippingMethodIntegrityService.php',
            'sylius.validator.unique.registered_user' => 'getSylius_Validator_Unique_RegisteredUserService.php',
            'sylius.validator.unique_currency_pair' => 'getSylius_Validator_UniqueCurrencyPairService.php',
            'sylius.validator.unique_reviewer_email' => 'getSylius_Validator_UniqueReviewerEmailService.php',
            'sylius.validator.valid_attribute_value' => 'getSylius_Validator_ValidAttributeValueService.php',
            'sylius.validator.valid_province_address' => 'getSylius_Validator_ValidProvinceAddressService.php',
            'sylius.validator.valid_select_attribute' => 'getSylius_Validator_ValidSelectAttributeService.php',
            'sylius.validator.valid_text_attribute' => 'getSylius_Validator_ValidTextAttributeService.php',
            'sylius.zone_matcher' => 'getSylius_ZoneMatcherService.php',
            'sylius_fixtures.fixture_loader' => 'getSyliusFixtures_FixtureLoaderService.php',
            'sylius_fixtures.fixture_registry' => 'getSyliusFixtures_FixtureRegistryService.php',
            'sylius_fixtures.listener.suite_loader_listener' => 'getSyliusFixtures_Listener_SuiteLoaderListenerService.php',
            'sylius_fixtures.logger' => 'getSyliusFixtures_LoggerService.php',
            'sylius_fixtures.logger.formatter.console' => 'getSyliusFixtures_Logger_Formatter_ConsoleService.php',
            'sylius_fixtures.logger.handler.console' => 'getSyliusFixtures_Logger_Handler_ConsoleService.php',
            'sylius_fixtures.suite_loader' => 'getSyliusFixtures_SuiteLoaderService.php',
            'sylius_fixtures.suite_registry' => 'getSyliusFixtures_SuiteRegistryService.php',
            'sylus.payum_action.offline.status' => 'getSylus_PayumAction_Offline_StatusService.php',
            'templating' => 'getTemplatingService.php',
            'templating.loader' => 'getTemplating_LoaderService.php',
            'twig' => 'getTwigService.php',
            'twig.controller.exception' => 'getTwig_Controller_ExceptionService.php',
            'twig.controller.preview_error' => 'getTwig_Controller_PreviewErrorService.php',
            'validator' => 'getValidatorService.php',
        ];
        $this->aliases = [
            'Payum\\Core\\Payum' => 'payum',
            'Sylius\\Bundle\\AdminBundle\\EmailManager\\ShipmentEmailManagerInterface' => 'sylius.email_manager.shipment',
            'Sylius\\Bundle\\CoreBundle\\Assigner\\IpAssignerInterface' => 'sylius.customer_ip_assigner',
            'Sylius\\Bundle\\CoreBundle\\Installer\\Provider\\DatabaseSetupCommandsProviderInterface' => 'sylius.commands_provider.database_setup',
            'Sylius\\Bundle\\CoreBundle\\Installer\\Setup\\ChannelSetupInterface' => 'sylius.setup.channel',
            'Sylius\\Bundle\\CoreBundle\\Installer\\Setup\\CurrencySetupInterface' => 'sylius.setup.currency',
            'Sylius\\Bundle\\CoreBundle\\Installer\\Setup\\LocaleSetupInterface' => 'sylius.setup.locale',
            'Sylius\\Bundle\\CoreBundle\\Remover\\ReviewerReviewsRemoverInterface' => 'sylius.reviewer_reviews_remover',
            'Sylius\\Bundle\\FixturesBundle\\Fixture\\FixtureRegistryInterface' => 'sylius_fixtures.fixture_registry',
            'Sylius\\Bundle\\FixturesBundle\\Loader\\FixtureLoaderInterface' => 'sylius_fixtures.fixture_loader',
            'Sylius\\Bundle\\FixturesBundle\\Loader\\SuiteLoaderInterface' => 'sylius_fixtures.suite_loader',
            'Sylius\\Bundle\\FixturesBundle\\Suite\\SuiteRegistryInterface' => 'sylius_fixtures.suite_registry',
            'Sylius\\Bundle\\MoneyBundle\\Formatter\\MoneyFormatterInterface' => 'sylius.money_formatter',
            'Sylius\\Bundle\\OrderBundle\\NumberAssigner\\OrderNumberAssignerInterface' => 'sylius.order_number_assigner',
            'Sylius\\Bundle\\PayumBundle\\Factory\\GetStatusFactoryInterface' => 'sylius.factory.payum_get_status_action',
            'Sylius\\Bundle\\PayumBundle\\Factory\\ResolveNextRouteFactoryInterface' => 'sylius.factory.payum_resolve_next_route',
            'Sylius\\Bundle\\PayumBundle\\Provider\\PaymentDescriptionProviderInterface' => 'sylius.payment_description_provider',
            'Sylius\\Bundle\\ShopBundle\\Calculator\\OrderItemsSubtotalCalculatorInterface' => 'sylius.calculator.order_items_subtotal',
            'Sylius\\Bundle\\ShopBundle\\EmailManager\\ContactEmailManagerInterface' => 'sylius.email_manager.contact',
            'Sylius\\Bundle\\ShopBundle\\EmailManager\\OrderEmailManagerInterface' => 'sylius.email_manager.order',
            'Sylius\\Bundle\\ThemeBundle\\Asset\\Installer\\AssetsInstallerInterface' => 'sylius.theme.asset.assets_installer.output_aware',
            'Sylius\\Bundle\\ThemeBundle\\Configuration\\ConfigurationProviderInterface' => 'sylius.theme.configuration.provider',
            'Sylius\\Bundle\\ThemeBundle\\Repository\\ThemeRepositoryInterface' => 'sylius.repository.theme',
            'Sylius\\Bundle\\UserBundle\\Security\\UserLoginInterface' => 'sylius.security.user_login',
            'Sylius\\Bundle\\UserBundle\\Security\\UserPasswordEncoderInterface' => 'sylius.security.password_encoder',
            'Sylius\\Component\\Addressing\\Comparator\\AddressComparatorInterface' => 'sylius.address_comparator',
            'Sylius\\Component\\Addressing\\Converter\\CountryNameConverterInterface' => 'sylius.converter.country_name',
            'Sylius\\Component\\Addressing\\Matcher\\ZoneMatcherInterface' => 'sylius.zone_matcher',
            'Sylius\\Component\\Addressing\\Provider\\ProvinceNamingProviderInterface' => 'sylius.province_naming_provider',
            'Sylius\\Component\\Core\\Checker\\OrderPaymentMethodSelectionRequirementCheckerInterface' => 'sylius.checker.order_payment_method_selection_requirement',
            'Sylius\\Component\\Core\\Checker\\OrderShippingMethodSelectionRequirementCheckerInterface' => 'sylius.checker.order_shipping_method_selection_requirement',
            'Sylius\\Component\\Core\\Context\\ShopperContextInterface' => 'sylius.context.shopper',
            'Sylius\\Component\\Core\\Currency\\CurrencyStorageInterface' => 'sylius.storage.currency',
            'Sylius\\Component\\Core\\Customer\\CustomerAddressAdderInterface' => 'sylius.customer_unique_address_adder',
            'Sylius\\Component\\Core\\Customer\\OrderAddressesSaverInterface' => 'sylius.customer_order_addresses_saver',
            'Sylius\\Component\\Core\\Customer\\Statistics\\CustomerStatisticsProviderInterface' => 'sylius.customer_statistics_provider',
            'Sylius\\Component\\Core\\Dashboard\\DashboardStatisticsProviderInterface' => 'sylius.dashboard.statistics_provider',
            'Sylius\\Component\\Core\\Distributor\\IntegerDistributorInterface' => 'sylius.integer_distributor',
            'Sylius\\Component\\Core\\Distributor\\ProportionalIntegerDistributorInterface' => 'sylius.proportional_integer_distributor',
            'Sylius\\Component\\Core\\Factory\\CustomerAfterCheckoutFactoryInterface' => 'sylius.factory.customer_after_checkout',
            'Sylius\\Component\\Core\\Order\\OrderItemNamesSetterInterface' => 'sylius.order_item_names_setter',
            'Sylius\\Component\\Core\\Payment\\InvoiceNumberGeneratorInterface' => 'sylius.invoice_number_generator',
            'Sylius\\Component\\Core\\Payment\\Provider\\OrderPaymentProviderInterface' => 'sylius.order_payment_provider',
            'Sylius\\Component\\Core\\Promotion\\Applicator\\UnitsPromotionAdjustmentsApplicatorInterface' => 'sylius.promotion.units_promotion_adjustments_applicator',
            'Sylius\\Component\\Core\\Promotion\\Modifier\\OrderPromotionsUsageModifierInterface' => 'sylius.promotion_usage_modifier',
            'Sylius\\Component\\Core\\Provider\\ProductVariantsPricesProviderInterface' => 'sylius.provider.product_variants_prices',
            'Sylius\\Component\\Core\\Storage\\CartStorageInterface' => 'sylius.storage.cart_session',
            'Sylius\\Component\\Core\\TokenAssigner\\OrderTokenAssignerInterface' => 'sylius.unique_id_based_order_token_assigner',
            'Sylius\\Component\\Core\\Updater\\UnpaidOrdersStateUpdaterInterface' => 'sylius.unpaid_orders_state_updater',
            'Sylius\\Component\\Core\\Uploader\\ImageUploaderInterface' => 'sylius.image_uploader',
            'Sylius\\Component\\Currency\\Converter\\CurrencyConverterInterface' => 'sylius.currency_converter',
            'Sylius\\Component\\Currency\\Converter\\CurrencyNameConverterInterface' => 'sylius.currency_name_converter',
            'Sylius\\Component\\Customer\\Context\\CustomerContextInterface' => 'sylius.context.customer',
            'Sylius\\Component\\Locale\\Converter\\LocaleConverterInterface' => 'sylius.locale_converter',
            'Sylius\\Component\\Locale\\Provider\\LocaleProviderInterface' => 'sylius.locale_provider.channel_based',
            'Sylius\\Component\\Mailer\\Factory\\EmailFactoryInterface' => 'sylius.factory.email',
            'Sylius\\Component\\Mailer\\Provider\\DefaultSettingsProviderInterface' => 'sylius.mailer.default_settings_provider',
            'Sylius\\Component\\Mailer\\Provider\\EmailProviderInterface' => 'sylius.email_provider',
            'Sylius\\Component\\Mailer\\Sender\\SenderInterface' => 'sylius.email_sender',
            'Sylius\\Component\\Order\\Aggregator\\AdjustmentsAggregatorInterface' => 'sylius.adjustments_aggregator',
            'Sylius\\Component\\Order\\Modifier\\OrderItemQuantityModifierInterface' => 'sylius.order_item_quantity_modifier.limiting',
            'Sylius\\Component\\Order\\Modifier\\OrderModifierInterface' => 'sylius.order_modifier',
            'Sylius\\Component\\Order\\Remover\\ExpiredCartsRemoverInterface' => 'sylius.expired_carts_remover',
            'Sylius\\Component\\Payment\\Resolver\\DefaultPaymentMethodResolverInterface' => 'sylius.payment_method_resolver.default',
            'Sylius\\Component\\Payment\\Resolver\\PaymentMethodsResolverInterface' => 'sylius.payment_methods_resolver',
            'Sylius\\Component\\Product\\Checker\\ProductVariantsParityCheckerInterface' => 'sylius.checker.product_variants_parity',
            'Sylius\\Component\\Product\\Generator\\ProductVariantGeneratorInterface' => 'sylius.generator.product_variant',
            'Sylius\\Component\\Product\\Generator\\SlugGeneratorInterface' => 'sylius.generator.slug',
            'Sylius\\Component\\Promotion\\Action\\PromotionApplicatorInterface' => 'sylius.promotion_applicator',
            'Sylius\\Component\\Promotion\\Generator\\PromotionCouponGeneratorInterface' => 'sylius.promotion_coupon_generator',
            'Sylius\\Component\\Promotion\\Processor\\PromotionProcessorInterface' => 'sylius.promotion_processor',
            'Sylius\\Component\\Promotion\\Provider\\ActivePromotionsProvider' => 'sylius.active_promotions_provider',
            'Sylius\\Component\\Resource\\Generator\\RandomnessGeneratorInterface' => 'sylius.random_generator',
            'Sylius\\Component\\Shipping\\Calculator\\DelegatingCalculatorInterface' => 'sylius.shipping_calculator',
            'Sylius\\Component\\Shipping\\Checker\\ShippingMethodEligibilityCheckerInterface' => 'sylius.shipping_eligibility_checker',
            'Sylius\\Component\\Shipping\\Resolver\\DefaultShippingMethodResolverInterface' => 'sylius.shipping_method_resolver.default',
            'Sylius\\Component\\Shipping\\Resolver\\ShippingMethodsResolverInterface' => 'sylius.shipping_methods_resolver',
            'Sylius\\Component\\Taxation\\Calculator\\CalculatorInterface' => 'sylius.tax_calculator',
            'Sylius\\Component\\Taxation\\Resolver\\TaxRateResolverInterface' => 'sylius.tax_rate_resolver',
            'Sylius\\Component\\Taxonomy\\Generator\\TaxonSlugGeneratorInterface' => 'sylius.generator.taxon_slug',
            'Sylius\\Component\\User\\Canonicalizer\\CanonicalizerInterface' => 'sylius.canonicalizer',
            'Sylius\\Component\\User\\Security\\PasswordUpdaterInterface' => 'sylius.security.password_updater',
            'app.manager.order_report' => 'doctrine.orm.default_entity_manager',
            'bitbag.manager.shipping_export' => 'doctrine.orm.default_entity_manager',
            'bitbag.manager.shipping_gateway' => 'doctrine.orm.default_entity_manager',
            'brille24.manager.tierprice' => 'doctrine.orm.default_entity_manager',
            'database_connection' => 'doctrine.dbal.default_connection',
            'doctrine.orm.entity_manager' => 'doctrine.orm.default_entity_manager',
            'hwi_oauth.http.client' => 'httplug.client',
            'hwi_oauth.http.message_factory' => 'httplug.message_factory',
            'liip_imagine.controller' => 'Liip\\ImagineBundle\\Controller\\ImagineController',
            'mailer' => 'swiftmailer.mailer.default',
            'nfq.manager.consent' => 'doctrine.orm.default_entity_manager',
            'omni.manager.parcel_machine' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.banner' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.banner_image' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.banner_image_translation' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.banner_position' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.banner_position_translation' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.banner_zone' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.banner_zone_translation' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.channel_logo' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.channel_watermark' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.export_job' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.import_job' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.manifest' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.node' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.node_image' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.node_image_translation' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.node_translation' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.search_index' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.seo_metadata' => 'doctrine.orm.default_entity_manager',
            'omni_sylius.manager.shipper_config' => 'doctrine.orm.default_entity_manager',
            'sylius.calculator.product_variant_price' => 'Sylius\\Component\\Core\\Calculator\\ProductVariantPriceCalculatorInterface',
            'sylius.context.channel.request_based.resolver' => 'Sylius\\Component\\Channel\\Context\\RequestBased\\RequestResolverInterface',
            'sylius.context.currency' => 'sylius.context.currency.channel_aware',
            'sylius.context.locale' => 'Sylius\\Component\\Locale\\Context\\LocaleContextInterface',
            'sylius.factory.adjustment' => 'Sylius\\Component\\Order\\Factory\\AdjustmentFactoryInterface',
            'sylius.factory.channel' => 'Sylius\\Component\\Channel\\Factory\\ChannelFactoryInterface',
            'sylius.factory.order_item' => 'sylius.factory.cart_item',
            'sylius.factory.payment' => 'Sylius\\Component\\Payment\\Factory\\PaymentFactoryInterface',
            'sylius.factory.payment_method' => 'Sylius\\Component\\Core\\Factory\\PaymentMethodFactoryInterface',
            'sylius.factory.product' => 'Sylius\\Component\\Product\\Factory\\ProductFactoryInterface',
            'sylius.factory.product_variant' => 'Sylius\\Component\\Product\\Factory\\ProductVariantFactoryInterface',
            'sylius.factory.promotion_action' => 'Sylius\\Component\\Core\\Factory\\PromotionActionFactoryInterface',
            'sylius.factory.promotion_coupon' => 'Sylius\\Component\\Promotion\\Factory\\PromotionCouponFactoryInterface',
            'sylius.factory.promotion_rule' => 'Sylius\\Component\\Core\\Factory\\PromotionRuleFactoryInterface',
            'sylius.factory.taxon' => 'Sylius\\Component\\Taxonomy\\Factory\\TaxonFactoryInterface',
            'sylius.factory.zone' => 'Sylius\\Component\\Addressing\\Factory\\ZoneFactoryInterface',
            'sylius.grid.bulk_action_renderer.twig' => 'sylius.custom_bulk_action_grid_renderer.twig',
            'sylius.grid.renderer.twig' => 'sylius.custom_grid_renderer.twig',
            'sylius.inventory.order_inventory_operator' => 'Sylius\\Component\\Core\\Inventory\\Operator\\OrderInventoryOperatorInterface',
            'sylius.locale_provider' => 'sylius.locale_provider.channel_based',
            'sylius.manager.address' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.address_log_entry' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.adjustment' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.admin_user' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.api_access_token' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.api_auth_code' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.api_client' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.api_refresh_token' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.api_user' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.avatar_image' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.channel' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.channel_pricing' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.country' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.currency' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.customer' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.customer_group' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.exchange_rate' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.gateway_config' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.inventory_unit' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.locale' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.oauth_user' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.order' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.order_item' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.order_item_unit' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.order_sequence' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.payment' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.payment_method' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.payment_method_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.payment_security_token' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_association' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_association_type' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_association_type_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_attribute' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_attribute_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_attribute_value' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_image' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_option' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_option_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_option_value' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_option_value_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_review' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_reviewer' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_taxon' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_variant' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.product_variant_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.promotion' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.promotion_action' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.promotion_coupon' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.promotion_rule' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.promotion_subject' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.province' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.shipment' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.shipment_unit' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.shipping_category' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.shipping_method' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.shipping_method_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.shop_user' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.tax_category' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.tax_rate' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.taxon' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.taxon_image' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.taxon_translation' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.zone' => 'doctrine.orm.default_entity_manager',
            'sylius.manager.zone_member' => 'doctrine.orm.default_entity_manager',
            'sylius.order_item_quantity_modifier' => 'sylius.order_item_quantity_modifier.limiting',
            'sylius.theme.asset.assets_installer' => 'sylius.theme.asset.assets_installer.output_aware',
            'sylius.translation_locale_provider' => 'sylius.translation_locale_provider.admin',
            'translator' => 'lexik_translation.translator',
        ];

        $this->privates['service_container'] = function () {
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/HttpKernelInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/KernelInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/RebootableInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/TerminableInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Kernel.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Kernel/MicroKernelTrait.php';
            include_once \dirname(__DIR__, 4).'/src/Kernel.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/ControllerNameParser.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/event-dispatcher/EventSubscriberInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/ResponseListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/StreamedResponseListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/LocaleListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/ValidateRequestListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/EventListener/ResolveControllerNameSubscriber.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/DisallowRobotsIndexingListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ParameterBag/ParameterBagInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ParameterBag/ParameterBag.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ParameterBag/FrozenParameterBag.php';
            include_once \dirname(__DIR__, 4).'/vendor/psr/container/src/ContainerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ParameterBag/ContainerBagInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ParameterBag/ContainerBag.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/HttpKernel.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Controller/ControllerResolverInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Controller/TraceableControllerResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Controller/ControllerResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Controller/ContainerControllerResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Controller/ControllerResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Controller/ArgumentResolverInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Controller/TraceableArgumentResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Controller/ArgumentResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/ControllerMetadata/ArgumentMetadataFactoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/ControllerMetadata/ArgumentMetadataFactory.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/RequestStack.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/config/ConfigCacheFactoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/config/ResourceCheckerConfigCacheFactory.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/LocaleAwareListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/psr/cache/src/CacheItemPoolInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Adapter/AdapterInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache-contracts/CacheInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/psr/log/Psr/Log/LoggerAwareInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/service-contracts/ResetInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/ResettableInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/psr/log/Psr/Log/LoggerAwareTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Traits/AbstractTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Traits/AbstractAdapterTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache-contracts/CacheTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Traits/ContractsTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Adapter/AbstractAdapter.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Marshaller/MarshallerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Marshaller/DefaultMarshaller.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/PruneableInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Traits/FilesystemCommonTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Traits/FilesystemTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Adapter/FilesystemAdapter.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/SessionInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/Session.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/Storage/SessionStorageInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/Storage/NativeSessionStorage.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/SessionBagInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/Storage/MetadataBag.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/AbstractSessionListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/SessionListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/service-contracts/ServiceProviderInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/service-contracts/ServiceLocatorTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ServiceLocator.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Formatter/MessageFormatterInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Formatter/IntlFormatterInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Formatter/ChoiceMessageFormatterInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Formatter/MessageFormatter.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation-contracts/LocaleAwareInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/TranslatorInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation-contracts/TranslatorInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation-contracts/TranslatorTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/IdentityTranslator.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/DebugHandlersListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/psr/log/Psr/Log/LoggerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/ResettableInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Logger.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Log/DebugLoggerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/monolog-bridge/Logger.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Debug/FileLinkFormatter.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/stopwatch/Stopwatch.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/routing/RequestContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/RouterListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/annotations/lib/Doctrine/Common/Annotations/Reader.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/annotations/lib/Doctrine/Common/Annotations/AnnotationReader.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/annotations/lib/Doctrine/Common/Annotations/AnnotationRegistry.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/annotations/lib/Doctrine/Common/Annotations/PsrCachedReader.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Traits/ProxyTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Traits/PhpArrayTrait.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Adapter/PhpArrayAdapter.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Processor/ProcessorInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Processor/PsrLogMessageProcessor.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/HandlerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/AbstractHandler.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/FingersCrossedHandler.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/FingersCrossed/ActivationStrategyInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/FingersCrossed/ErrorLevelActivationStrategy.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/monolog-bridge/Handler/FingersCrossed/NotFoundActivationStrategy.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/FilterHandler.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/AbstractProcessingHandler.php';
            include_once \dirname(__DIR__, 4).'/vendor/monolog/monolog/src/Monolog/Handler/StreamHandler.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/monolog-bridge/Handler/ConsoleHandler.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authorization/AuthorizationCheckerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authorization/AuthorizationChecker.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authentication/Token/Storage/TokenStorageInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/service-contracts/ServiceSubscriberInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authentication/Token/Storage/UsageTrackingTokenStorage.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authentication/Token/Storage/TokenStorage.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authentication/AuthenticationManagerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authentication/AuthenticationProviderManager.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Http/FirewallMapInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security-bundle/Security/FirewallMap.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Http/Logout/LogoutUrlGenerator.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Http/RememberMe/ResponseListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authorization/AccessDecisionManagerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authorization/TraceableAccessDecisionManager.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/Authorization/AccessDecisionManager.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Http/Firewall.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security-bundle/EventListener/FirewallListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/security-bundle/Debug/TraceableFirewallListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Driver/Connection.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Connection.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle/ConnectionFactory.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Configuration.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Logging/SQLLogger.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Logging/LoggerChain.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/doctrine-bridge/Logger/DbalLogger.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Logging/DebugStack.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/event-manager/lib/Doctrine/Common/EventManager.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/doctrine-bridge/ContainerAwareEventManager.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectRepository.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/collections/lib/Doctrine/Common/Collections/Selectable.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityRepository.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Component/Repository/RepositoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Doctrine/ORM/EntityRepository.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Order/Repository/OrderRepositoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/OrderBundle/Doctrine/ORM/OrderRepository.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Repository/OrderRepositoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Doctrine/ORM/OrderRepository.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/Mapping/ClassMetadata.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/ClassMetadataInfo.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/ClassMetadata.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Component/Factory/FactoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Component/Factory/Factory.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle/Listener/RequestLocaleSetter.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Context/FakeChannel/FakeChannelPersister.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Order/Context/CartContextInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Context/CustomerAndChannelBasedCartContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Currency/CurrencyStorageInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Currency/CurrencyStorage.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Currency/Context/CurrencyContextInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Currency/Context/StorageBasedCurrencyContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Currency/Context/ChannelAwareCurrencyContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Currency/Context/CompositeCurrencyContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Locale/Provider/LocaleProviderInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Provider/ChannelBasedLocaleProvider.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Locale/Context/LocaleContextInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle/Context/RequestBasedLocaleContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Locale/Context/ProviderBasedLocaleContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/EventListener/TranslatorListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Customer/Context/CustomerContextInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Context/CustomerContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Component/Storage/StorageInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Storage/CookieStorage.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Component/Metadata/RegistryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Component/Metadata/Registry.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/Mapping/Driver/MappingDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/Mapping/Driver/MappingDriverChain.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Doctrine/ResourceMappingDriverChain.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/Mapping/Driver/FileDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/XmlDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/SimplifiedXmlDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/Mapping/Driver/AnnotationDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/AnnotationDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/YamlDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/SimplifiedYamlDriver.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/Mapping/Driver/FileLocator.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/Mapping/Driver/SymfonyFileLocator.php';
            include_once \dirname(__DIR__, 4).'/vendor/winzou/state-machine/src/SM/Factory/FactoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/winzou/state-machine/src/SM/Factory/ClearableFactoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/winzou/state-machine/src/SM/Factory/AbstractFactory.php';
            include_once \dirname(__DIR__, 4).'/vendor/winzou/state-machine/src/SM/Factory/Factory.php';
            include_once \dirname(__DIR__, 4).'/vendor/winzou/state-machine/src/SM/Callback/CallbackFactoryInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/winzou/state-machine/src/SM/Callback/CallbackFactory.php';
            include_once \dirname(__DIR__, 4).'/vendor/winzou/state-machine-bundle/Callback/ContainerAwareCallbackFactory.php';
            include_once \dirname(__DIR__, 4).'/vendor/sonata-project/block-bundle/src/Cache/HttpCacheHandlerInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sonata-project/block-bundle/src/Cache/HttpCacheHandler.php';
            include_once \dirname(__DIR__, 4).'/vendor/friendsofsymfony/rest-bundle/EventListener/BodyListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/friendsofsymfony/rest-bundle/Decoder/DecoderProviderInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/friendsofsymfony/rest-bundle/Decoder/ContainerDecoderProvider.php';
            include_once \dirname(__DIR__, 4).'/vendor/friendsofsymfony/rest-bundle/EventListener/FormatListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/willdurand/negotiation/src/Negotiation/AbstractNegotiator.php';
            include_once \dirname(__DIR__, 4).'/vendor/willdurand/negotiation/src/Negotiation/Negotiator.php';
            include_once \dirname(__DIR__, 4).'/vendor/friendsofsymfony/rest-bundle/Negotiation/FormatNegotiator.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/RequestMatcherInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/RequestMatcher.php';
            include_once \dirname(__DIR__, 4).'/vendor/doctrine/event-manager/lib/Doctrine/Common/EventSubscriber.php';
            include_once \dirname(__DIR__, 4).'/vendor/gedmo/doctrine-extensions/lib/Gedmo/Mapping/MappedEventSubscriber.php';
            include_once \dirname(__DIR__, 4).'/vendor/gedmo/doctrine-extensions/lib/Gedmo/Loggable/LoggableListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/stof/doctrine-extensions-bundle/src/EventListener/LoggerListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Context/AdminBasedLocaleContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/EventListener/SessionCartSubscriber.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Context/SessionAndChannelBasedCartContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Storage/CartStorageInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Storage/CartSessionStorage.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/EventListener/NonChannelLocaleListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Checkout/CheckoutResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/routing/RequestContextAwareInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/routing/Generator/UrlGeneratorInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Checkout/CheckoutStateUrlGeneratorInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Checkout/CheckoutStateUrlGenerator.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/webpack-encore-bundle/src/Asset/EntrypointLookupCollectionInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/webpack-encore-bundle/src/Asset/EntrypointLookupCollection.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/webpack-encore-bundle/src/EventListener/ResetAssetsEventListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/httplug-bundle/src/Discovery/ConfiguredClientsStrategyListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/httplug-bundle/src/Collector/PluginClientFactory.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/message/src/Formatter.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/httplug-bundle/src/Collector/Formatter.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/message/src/Formatter/FullHttpMessageFormatter.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/message/src/Formatter/CurlCommandFormatter.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/DataCollector/DataCollectorInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/DataCollector/DataCollector.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/httplug-bundle/src/Collector/Collector.php';
            include_once \dirname(__DIR__, 4).'/vendor/php-http/httplug-bundle/src/Collector/PluginClientFactoryListener.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/TranslatorBagInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/translation/Translator.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/CacheWarmer/WarmableInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Translation/Translator.php';
            include_once \dirname(__DIR__, 4).'/vendor/lexik/translation-bundle/Translation/Translator.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/routing/Matcher/UrlMatcherInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/routing/RouterInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/routing/Matcher/RequestMatcherInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/routing/Router.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ServiceSubscriberInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/DependencyInjection/CompatibilityServiceSubscriberInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/framework-bundle/Routing/Router.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Locale/Context/CompositeLocaleContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/ChannelContextInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/CompositeChannelContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/RequestBased/ChannelContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/SingleChannelContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Context/FakeChannel/FakeChannelContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/RequestBased/RequestResolverInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/RequestBased/CompositeRequestResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/RequestBased/HostnameBasedRequestResolver.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Context/FakeChannel/FakeChannelCodeProviderInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Context/FakeChannel/FakeChannelCodeProvider.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Order/Context/CompositeCartContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Context/CachedPerRequestChannelContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Cart/Context/ShopBasedCartContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Order/Context/CartContext.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/event-dispatcher-contracts/EventDispatcherInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/event-dispatcher/EventDispatcherInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/event-dispatcher/Debug/TraceableEventDispatcherInterface.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/event-dispatcher/Debug/TraceableEventDispatcher.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/http-kernel/Debug/TraceableEventDispatcher.php';
            include_once \dirname(__DIR__, 4).'/vendor/symfony/event-dispatcher/EventDispatcher.php';
        };
    }

    public function compile(): void
    {
        throw new LogicException('You cannot compile a dumped container that was already compiled.');
    }

    public function isCompiled(): bool
    {
        return true;
    }

    public function getRemovedIds(): array
    {
        return require $this->containerDir.\DIRECTORY_SEPARATOR.'removed-ids.php';
    }

    protected function load($file, $lazyLoad = true)
    {
        return require $this->containerDir.\DIRECTORY_SEPARATOR.$file;
    }

    protected function createProxy($class, \Closure $factory)
    {
        class_exists($class, false) || $this->load("{$class}.php");

        return $factory();
    }

    /**
     * Gets the public 'Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelCodeProviderInterface' shared service.
     *
     * @return \Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelCodeProvider
     */
    protected function getFakeChannelCodeProviderInterfaceService()
    {
        return $this->services['Sylius\\Bundle\\ChannelBundle\\Context\\FakeChannel\\FakeChannelCodeProviderInterface'] = new \Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelCodeProvider();
    }

    /**
     * Gets the public 'Sylius\Component\Channel\Context\ChannelContextInterface' shared service.
     *
     * @return \Sylius\Component\Channel\Context\CompositeChannelContext
     */
    protected function getChannelContextInterfaceService()
    {
        $this->services['Sylius\\Component\\Channel\\Context\\ChannelContextInterface'] = $instance = new \Sylius\Component\Channel\Context\CompositeChannelContext();

        $a = ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack()));
        $b = ($this->services['sylius.repository.channel'] ?? $this->getSylius_Repository_ChannelService());

        $instance->addContext(new \Sylius\Component\Channel\Context\RequestBased\ChannelContext(($this->services['Sylius\\Component\\Channel\\Context\\RequestBased\\RequestResolverInterface'] ?? $this->getRequestResolverInterfaceService()), $a), 0);
        $instance->addContext(new \Sylius\Component\Channel\Context\SingleChannelContext($b), -128);
        $instance->addContext(new \Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelContext(($this->services['Sylius\\Bundle\\ChannelBundle\\Context\\FakeChannel\\FakeChannelCodeProviderInterface'] ?? ($this->services['Sylius\\Bundle\\ChannelBundle\\Context\\FakeChannel\\FakeChannelCodeProviderInterface'] = new \Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelCodeProvider())), $b, $a), 128);

        return $instance;
    }

    /**
     * Gets the public 'Sylius\Component\Channel\Context\RequestBased\RequestResolverInterface' shared service.
     *
     * @return \Sylius\Component\Channel\Context\RequestBased\CompositeRequestResolver
     */
    protected function getRequestResolverInterfaceService()
    {
        $this->services['Sylius\\Component\\Channel\\Context\\RequestBased\\RequestResolverInterface'] = $instance = new \Sylius\Component\Channel\Context\RequestBased\CompositeRequestResolver();

        $instance->addResolver(new \Sylius\Component\Channel\Context\RequestBased\HostnameBasedRequestResolver(($this->services['sylius.repository.channel'] ?? $this->getSylius_Repository_ChannelService())), 0);

        return $instance;
    }

    /**
     * Gets the public 'Sylius\Component\Locale\Context\LocaleContextInterface' shared service.
     *
     * @return \Sylius\Component\Locale\Context\CompositeLocaleContext
     */
    protected function getLocaleContextInterfaceService()
    {
        $this->services['Sylius\\Component\\Locale\\Context\\LocaleContextInterface'] = $instance = new \Sylius\Component\Locale\Context\CompositeLocaleContext();

        $instance->addContext(($this->services['sylius.context.locale.request_based'] ?? $this->getSylius_Context_Locale_RequestBasedService()), 64);
        $instance->addContext(($this->services['sylius.context.locale.provider_based'] ?? $this->getSylius_Context_Locale_ProviderBasedService()), -128);
        $instance->addContext(($this->services['sylius.context.locale.admin_based'] ?? $this->getSylius_Context_Locale_AdminBasedService()), 128);

        return $instance;
    }

    /**
     * Gets the public 'doctrine.dbal.default_connection' shared service.
     *
     * @return \Doctrine\DBAL\Connection
     */
    protected function getDoctrine_Dbal_DefaultConnectionService()
    {
        $a = new \Doctrine\DBAL\Configuration();

        $b = new \Doctrine\DBAL\Logging\LoggerChain();

        $c = new \Symfony\Bridge\Monolog\Logger('doctrine');
        $c->pushHandler(($this->privates['monolog.handler.main'] ?? $this->getMonolog_Handler_MainService()));

        $b->addLogger(new \Symfony\Bridge\Doctrine\Logger\DbalLogger($c, ($this->privates['debug.stopwatch'] ?? ($this->privates['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch(true)))));
        $b->addLogger(new \Doctrine\DBAL\Logging\DebugStack());

        $a->setSQLLogger($b);
        $d = new \Symfony\Bridge\Doctrine\ContainerAwareEventManager(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            'doctrine.orm.default_listeners.attach_entity_listeners' => ['privates', 'doctrine.orm.default_listeners.attach_entity_listeners', 'getDoctrine_Orm_DefaultListeners_AttachEntityListenersService.php', true],
            'doctrine.orm.listeners.resolve_target_entity' => ['privates', 'doctrine.orm.listeners.resolve_target_entity', 'getDoctrine_Orm_Listeners_ResolveTargetEntityService.php', true],
            'lexik_translation.orm.listener' => ['privates', 'lexik_translation.orm.listener', 'getLexikTranslation_Orm_ListenerService.php', true],
            'omni_seo.event_listener.dynamic_mapping' => ['privates', 'omni_seo.event_listener.dynamic_mapping', 'getOmniSeo_EventListener_DynamicMappingService.php', true],
            'omni_sylius_translator.event_listener.class_metadata' => ['privates', 'omni_sylius_translator.event_listener.class_metadata', 'getOmniSyliusTranslator_EventListener_ClassMetadataService.php', true],
            'stof_doctrine_extensions.listener.loggable' => ['privates', 'stof_doctrine_extensions.listener.loggable', 'getStofDoctrineExtensions_Listener_LoggableService', false],
            'stof_doctrine_extensions.listener.sluggable' => ['privates', 'stof_doctrine_extensions.listener.sluggable', 'getStofDoctrineExtensions_Listener_SluggableService.php', true],
            'stof_doctrine_extensions.listener.sortable' => ['privates', 'stof_doctrine_extensions.listener.sortable', 'getStofDoctrineExtensions_Listener_SortableService.php', true],
            'stof_doctrine_extensions.listener.timestampable' => ['privates', 'stof_doctrine_extensions.listener.timestampable', 'getStofDoctrineExtensions_Listener_TimestampableService.php', true],
            'stof_doctrine_extensions.listener.tree' => ['privates', 'stof_doctrine_extensions.listener.tree', 'getStofDoctrineExtensions_Listener_TreeService.php', true],
            'sylius.doctrine.orm.event_subscriber.load_metadata.attribute' => ['services', 'sylius.doctrine.orm.event_subscriber.load_metadata.attribute', 'getSylius_Doctrine_Orm_EventSubscriber_LoadMetadata_AttributeService.php', true],
            'sylius.doctrine.orm.event_subscriber.load_metadata.review' => ['services', 'sylius.doctrine.orm.event_subscriber.load_metadata.review', 'getSylius_Doctrine_Orm_EventSubscriber_LoadMetadata_ReviewService.php', true],
            'sylius.event_subscriber.orm_mapped_super_class' => ['services', 'sylius.event_subscriber.orm_mapped_super_class', 'getSylius_EventSubscriber_OrmMappedSuperClassService.php', true],
            'sylius.event_subscriber.orm_repository_class' => ['services', 'sylius.event_subscriber.orm_repository_class', 'getSylius_EventSubscriber_OrmRepositoryClassService.php', true],
            'sylius.listener.canonicalizer' => ['services', 'sylius.listener.canonicalizer', 'getSylius_Listener_CanonicalizerService.php', true],
            'sylius.listener.default_username' => ['services', 'sylius.listener.default_username', 'getSylius_Listener_DefaultUsernameService.php', true],
            'sylius.listener.images_remove' => ['services', 'sylius.listener.images_remove', 'getSylius_Listener_ImagesRemoveService.php', true],
            'sylius.listener.password_updater' => ['services', 'sylius.listener.password_updater', 'getSylius_Listener_PasswordUpdaterService.php', true],
            'sylius.listener.product_review_change' => ['services', 'sylius.listener.product_review_change', 'getSylius_Listener_ProductReviewChangeService.php', true],
            'sylius.translation.translatable_listener.doctrine.orm' => ['services', 'sylius.translation.translatable_listener.doctrine.orm', 'getSylius_Translation_TranslatableListener_Doctrine_OrmService.php', true],
        ], [
            'doctrine.orm.default_listeners.attach_entity_listeners' => '?',
            'doctrine.orm.listeners.resolve_target_entity' => '?',
            'lexik_translation.orm.listener' => '?',
            'omni_seo.event_listener.dynamic_mapping' => '?',
            'omni_sylius_translator.event_listener.class_metadata' => '?',
            'stof_doctrine_extensions.listener.loggable' => '?',
            'stof_doctrine_extensions.listener.sluggable' => '?',
            'stof_doctrine_extensions.listener.sortable' => '?',
            'stof_doctrine_extensions.listener.timestampable' => '?',
            'stof_doctrine_extensions.listener.tree' => '?',
            'sylius.doctrine.orm.event_subscriber.load_metadata.attribute' => '?',
            'sylius.doctrine.orm.event_subscriber.load_metadata.review' => '?',
            'sylius.event_subscriber.orm_mapped_super_class' => '?',
            'sylius.event_subscriber.orm_repository_class' => '?',
            'sylius.listener.canonicalizer' => '?',
            'sylius.listener.default_username' => '?',
            'sylius.listener.images_remove' => '?',
            'sylius.listener.password_updater' => '?',
            'sylius.listener.product_review_change' => '?',
            'sylius.translation.translatable_listener.doctrine.orm' => '?',
        ]), [0 => 'sylius.event_subscriber.orm_mapped_super_class', 1 => 'sylius.event_subscriber.orm_repository_class', 2 => 'sylius.translation.translatable_listener.doctrine.orm', 3 => 'stof_doctrine_extensions.listener.loggable', 4 => 'doctrine.orm.listeners.resolve_target_entity', 5 => 'sylius.doctrine.orm.event_subscriber.load_metadata.attribute', 6 => 'sylius.doctrine.orm.event_subscriber.load_metadata.review', 7 => 'stof_doctrine_extensions.listener.tree', 8 => 'stof_doctrine_extensions.listener.sluggable', 9 => 'stof_doctrine_extensions.listener.timestampable', 10 => 'stof_doctrine_extensions.listener.sortable', 11 => 'omni_sylius_translator.event_listener.class_metadata']);
        $d->addEventListener([0 => 'loadClassMetadata'], 'doctrine.orm.default_listeners.attach_entity_listeners');
        $d->addEventListener([0 => 'prePersist'], 'sylius.listener.password_updater');
        $d->addEventListener([0 => 'preUpdate'], 'sylius.listener.password_updater');
        $d->addEventListener([0 => 'postPersist'], 'sylius.listener.product_review_change');
        $d->addEventListener([0 => 'postUpdate'], 'sylius.listener.product_review_change');
        $d->addEventListener([0 => 'postRemove'], 'sylius.listener.product_review_change');
        $d->addEventListener([0 => 'onFlush'], 'sylius.listener.images_remove');
        $d->addEventListener([0 => 'postFlush'], 'sylius.listener.images_remove');
        $d->addEventListener([0 => 'onFlush'], 'sylius.listener.default_username');
        $d->addEventListener([0 => 'prePersist'], 'sylius.listener.canonicalizer');
        $d->addEventListener([0 => 'preUpdate'], 'sylius.listener.canonicalizer');
        $d->addEventListener([0 => 'loadClassMetadata'], 'omni_seo.event_listener.dynamic_mapping');
        $d->addEventListener([0 => 'loadClassMetadata'], 'lexik_translation.orm.listener');

        return $this->services['doctrine.dbal.default_connection'] = (new \Doctrine\Bundle\DoctrineBundle\ConnectionFactory([]))->createConnection(['driver' => 'pdo_mysql', 'charset' => 'UTF8', 'url' => $this->getEnv('resolve:DATABASE_URL'), 'host' => 'localhost', 'port' => NULL, 'user' => 'root', 'password' => NULL, 'driverOptions' => [], 'serverVersion' => '5.7', 'defaultTableOptions' => []], $a, $d, []);
    }

    /**
     * Gets the public 'doctrine.orm.default_entity_manager' shared service.
     *
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getDoctrine_Orm_DefaultEntityManagerService($lazyLoad = true)
    {
        if ($lazyLoad) {
            return $this->services['doctrine.orm.default_entity_manager'] = $this->createProxy('EntityManager_9a5be93', function () {
                return \EntityManager_9a5be93::staticProxyConstructor(function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getDoctrine_Orm_DefaultEntityManagerService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                });
            });
        }

        include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Configuration.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/Cache.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/FlushableCache.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/ClearableCache.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/MultiGetCache.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/MultiDeleteCache.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/MultiPutCache.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/MultiOperationCache.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/cache/lib/Doctrine/Common/Cache/CacheProvider.php';
        include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/DoctrineProvider.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/NamingStrategy.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/DefaultNamingStrategy.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/QuoteStrategy.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/DefaultQuoteStrategy.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/EntityListenerResolver.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle/Mapping/EntityListenerServiceResolver.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle/Mapping/ContainerEntityListenerResolver.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Repository/RepositoryFactory.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle/Repository/ContainerRepositoryFactory.php';
        include_once \dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle/ManagerConfigurator.php';

        $a = new \Doctrine\ORM\Configuration();

        $b = new \Symfony\Component\Cache\DoctrineProvider(($this->privates['doctrine.system_cache_pool'] ?? $this->getDoctrine_SystemCachePoolService()));

        $a->setEntityNamespaces(['payum' => 'Payum\\Core\\Model', 'App' => 'App\\Entity', 'FOSOAuthServerBundle' => 'FOS\\OAuthServerBundle\\Entity', 'BitBagSyliusShippingExportPlugin' => 'BitBag\\SyliusShippingExportPlugin\\Entity', 'OmniSyliusShippingPlugin' => 'Omni\\Sylius\\ShippingPlugin\\Entity', 'Brille24SyliusTierPricePlugin' => 'Brille24\\SyliusTierPricePlugin\\Entity', 'LexikTranslationBundle' => 'Lexik\\Bundle\\TranslationBundle\\Entity']);
        $a->setMetadataCacheImpl($b);
        $a->setQueryCacheImpl($b);
        $a->setResultCacheImpl(new \Symfony\Component\Cache\DoctrineProvider(($this->privates['doctrine.result_cache_pool'] ?? $this->getDoctrine_ResultCachePoolService())));
        $a->setMetadataDriverImpl(($this->services['sylius_resource.doctrine.mapping_driver_chain'] ?? $this->getSyliusResource_Doctrine_MappingDriverChainService()));
        $a->setProxyDir(($this->targetDir.''.'/doctrine/orm/Proxies'));
        $a->setProxyNamespace('Proxies');
        $a->setAutoGenerateProxyClasses(true);
        $a->setClassMetadataFactoryName('Doctrine\\ORM\\Mapping\\ClassMetadataFactory');
        $a->setDefaultRepositoryClassName('Doctrine\\ORM\\EntityRepository');
        $a->setNamingStrategy(new \Doctrine\ORM\Mapping\DefaultNamingStrategy());
        $a->setQuoteStrategy(new \Doctrine\ORM\Mapping\DefaultQuoteStrategy());
        $a->setEntityListenerResolver(new \Doctrine\Bundle\DoctrineBundle\Mapping\ContainerEntityListenerResolver($this));
        $a->setRepositoryFactory(new \Doctrine\Bundle\DoctrineBundle\Repository\ContainerRepositoryFactory(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [], [])));

        $instance = \Doctrine\ORM\EntityManager::create(($this->services['doctrine.dbal.default_connection'] ?? $this->getDoctrine_Dbal_DefaultConnectionService()), $a);

        (new \Doctrine\Bundle\DoctrineBundle\ManagerConfigurator([], []))->configure($instance);

        return $instance;
    }

    /**
     * Gets the public 'event_dispatcher' shared service.
     *
     * @return \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher
     */
    protected function getEventDispatcherService()
    {
        $a = new \Symfony\Bridge\Monolog\Logger('event');
        $a->pushHandler(($this->privates['monolog.handler.main'] ?? $this->getMonolog_Handler_MainService()));

        $this->services['event_dispatcher'] = $instance = new \Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher(new \Symfony\Component\EventDispatcher\EventDispatcher(), ($this->privates['debug.stopwatch'] ?? ($this->privates['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch(true))), $a, ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())));

        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['bitbag.shipping_export_plugin.menu.shipping_gateway'] ?? ($this->privates['bitbag.shipping_export_plugin.menu.shipping_gateway'] = new \BitBag\SyliusShippingExportPlugin\Menu\ShippingGatewayMenuBuilder()));
        }, 1 => 'buildMenu'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['bitbag.shipping_export_plugin.menu.shipping_export'] ?? ($this->privates['bitbag.shipping_export_plugin.menu.shipping_export'] = new \BitBag\SyliusShippingExportPlugin\Menu\ShippingExportMenuBuilder()));
        }, 1 => 'buildMenu'], 0);
        $instance->addListener('sylius.menu.admin.product_variant.form', [0 => function () {
            return ($this->privates['brille24_tier_price.listener.admin.product_variant.form.menu_builder'] ?? $this->load('getBrille24TierPrice_Listener_Admin_ProductVariant_Form_MenuBuilderService.php'));
        }, 1 => 'addItems'], 0);
        $instance->addListener('sylius.menu.admin.product.form', [0 => function () {
            return ($this->privates['brille24.listener.admin.product.form.menu_builder'] ?? $this->load('getBrille24_Listener_Admin_Product_Form_MenuBuilderService.php'));
        }, 1 => 'addItems'], 0);
        $instance->addListener('sonata.block.event.sylius.admin.layout.javascripts', [0 => function () {
            return ($this->privates['brille24_tier_price.block_event_listener.admin.layout.javascripts'] ?? ($this->privates['brille24_tier_price.block_event_listener.admin.layout.javascripts'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@Brille24SyliusTierPricePlugin/Admin/_javascripts.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('sonata.block.event.sylius.shop.layout.javascripts', [0 => function () {
            return ($this->privates['brille24_tier_price.block_event_listener.shop.layout.javascripts.tierprice'] ?? ($this->privates['brille24_tier_price.block_event_listener.shop.layout.javascripts.tierprice'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@Brille24SyliusTierPricePlugin/Shop/_javascripts.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('sylius.order_item.pre_add', [0 => function () {
            return ($this->services['App\\EventListener\\CartListener'] ?? $this->load('getCartListenerService.php'));
        }, 1 => 'change'], 0);
        $instance->addListener('sylius.order.initialize_address', [0 => function () {
            return ($this->services['App\\EventListener\\CartListener'] ?? $this->load('getCartListenerService.php'));
        }, 1 => 'checkCheckout'], 0);
        $instance->addListener('sylius.order.pre_select_shipping', [0 => function () {
            return ($this->services['App\\EventListener\\CartListener'] ?? $this->load('getCartListenerService.php'));
        }, 1 => 'onCheckoutChange'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['App\\EventListener\\ContentMenuBuilder'] ?? ($this->privates['App\\EventListener\\ContentMenuBuilder'] = new \App\EventListener\ContentMenuBuilder()));
        }, 1 => 'configureNodeMenu'], 0);
        $instance->addListener('app.order_report.pre_create', [0 => function () {
            return ($this->privates['App\\EventListener\\OrderReportListener'] ?? $this->load('getOrderReportListenerService.php'));
        }, 1 => 'preCreate'], 0);
        $instance->addListener('bitbag.export_shipment', [0 => function () {
            return ($this->privates['omni_sylius_dpd.event_listener.dpd_shipping_export'] ?? $this->load('getOmniSyliusDpd_EventListener_DpdShippingExportService.php'));
        }, 1 => 'exportShipment'], 0);
        $instance->addListener('omni.export_shipments', [0 => function () {
            return ($this->privates['omni_sylius_dpd.event_listener.dpd_shipping_export'] ?? $this->load('getOmniSyliusDpd_EventListener_DpdShippingExportService.php'));
        }, 1 => 'exportShipments'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->services['sylius.listener.request_locale_setter'] ?? $this->getSylius_Listener_RequestLocaleSetterService());
        }, 1 => 'onKernelRequest'], 4);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->services['sylius.context.channel.fake_channel.persister'] ?? $this->getSylius_Context_Channel_FakeChannel_PersisterService());
        }, 1 => 'onKernelResponse'], -8192);
        $instance->addListener('sylius.user.pre_password_reset', [0 => function () {
            return ($this->services['sylius.listener.password_updater'] ?? $this->load('getSylius_Listener_PasswordUpdaterService.php'));
        }, 1 => 'genericEventUpdater'], 0);
        $instance->addListener('sylius.user.pre_password_change', [0 => function () {
            return ($this->services['sylius.listener.password_updater'] ?? $this->load('getSylius_Listener_PasswordUpdaterService.php'));
        }, 1 => 'genericEventUpdater'], 0);
        $instance->addListener('sylius.admin_user.pre_update', [0 => function () {
            return ($this->services['sylius.listener.password_updater'] ?? $this->load('getSylius_Listener_PasswordUpdaterService.php'));
        }, 1 => 'genericEventUpdater'], 0);
        $instance->addListener('sylius.customer.pre_update', [0 => function () {
            return ($this->services['sylius.listener.password_updater'] ?? $this->load('getSylius_Listener_PasswordUpdaterService.php'));
        }, 1 => 'customerUpdateEvent'], 0);
        $instance->addListener('sylius.user.password_reset.request.pin', [0 => function () {
            return ($this->services['sylius.listener.user_mailer_listener'] ?? $this->load('getSylius_Listener_UserMailerListenerService.php'));
        }, 1 => 'sendResetPasswordPinEmail'], 0);
        $instance->addListener('sylius.user.password_reset.request.token', [0 => function () {
            return ($this->services['sylius.listener.user_mailer_listener'] ?? $this->load('getSylius_Listener_UserMailerListenerService.php'));
        }, 1 => 'sendResetPasswordTokenEmail'], 0);
        $instance->addListener('sylius.user.email_verification.token', [0 => function () {
            return ($this->services['sylius.listener.user_mailer_listener'] ?? $this->load('getSylius_Listener_UserMailerListenerService.php'));
        }, 1 => 'sendVerificationTokenEmail'], 0);
        $instance->addListener('sylius.customer.post_register', [0 => function () {
            return ($this->services['sylius.listener.user_mailer_listener'] ?? $this->load('getSylius_Listener_UserMailerListenerService.php'));
        }, 1 => 'sendUserRegistrationEmail'], 0);
        $instance->addListener('sylius.admin_user.post_create', [0 => function () {
            return ($this->privates['sylius.listener.admin_user.reloader'] ?? $this->load('getSylius_Listener_AdminUser_ReloaderService.php'));
        }, 1 => 'reloadUser'], 0);
        $instance->addListener('sylius.admin_user.post_update', [0 => function () {
            return ($this->privates['sylius.listener.admin_user.reloader'] ?? $this->load('getSylius_Listener_AdminUser_ReloaderService.php'));
        }, 1 => 'reloadUser'], 0);
        $instance->addListener('sylius.admin_user.pre_delete', [0 => function () {
            return ($this->privates['sylius.listener.admin_user_delete'] ?? $this->load('getSylius_Listener_AdminUserDeleteService.php'));
        }, 1 => 'deleteUser'], 0);
        $instance->addListener('sylius.shop_user.post_create', [0 => function () {
            return ($this->privates['sylius.listener.shop_user.reloader'] ?? $this->load('getSylius_Listener_ShopUser_ReloaderService.php'));
        }, 1 => 'reloadUser'], 0);
        $instance->addListener('sylius.shop_user.post_update', [0 => function () {
            return ($this->privates['sylius.listener.shop_user.reloader'] ?? $this->load('getSylius_Listener_ShopUser_ReloaderService.php'));
        }, 1 => 'reloadUser'], 0);
        $instance->addListener('sylius.shop_user.pre_delete', [0 => function () {
            return ($this->privates['sylius.listener.shop_user_delete'] ?? $this->load('getSylius_Listener_ShopUserDeleteService.php'));
        }, 1 => 'deleteUser'], 0);
        $instance->addListener('sylius.oauth_user.post_create', [0 => function () {
            return ($this->privates['sylius.listener.oauth_user.reloader'] ?? $this->load('getSylius_Listener_OauthUser_ReloaderService.php'));
        }, 1 => 'reloadUser'], 0);
        $instance->addListener('sylius.oauth_user.post_update', [0 => function () {
            return ($this->privates['sylius.listener.oauth_user.reloader'] ?? $this->load('getSylius_Listener_OauthUser_ReloaderService.php'));
        }, 1 => 'reloadUser'], 0);
        $instance->addListener('sylius.oauth_user.pre_delete', [0 => function () {
            return ($this->privates['sylius.listener.oauth_user_delete'] ?? $this->load('getSylius_Listener_OauthUserDeleteService.php'));
        }, 1 => 'deleteUser'], 0);
        $instance->addListener('security.interactive_login', [0 => function () {
            return ($this->privates['sylius.admin_user.listener.update_user_encoder'] ?? $this->load('getSylius_AdminUser_Listener_UpdateUserEncoderService.php'));
        }, 1 => 'onSecurityInteractiveLogin'], 0);
        $instance->addListener('security.interactive_login', [0 => function () {
            return ($this->privates['sylius.shop_user.listener.update_user_encoder'] ?? $this->load('getSylius_ShopUser_Listener_UpdateUserEncoderService.php'));
        }, 1 => 'onSecurityInteractiveLogin'], 0);
        $instance->addListener('sylius.user.security.implicit_login', [0 => function () {
            return ($this->services['sylius.listener.cart_blamer'] ?? $this->load('getSylius_Listener_CartBlamerService.php'));
        }, 1 => 'onImplicitLogin'], 0);
        $instance->addListener('security.interactive_login', [0 => function () {
            return ($this->services['sylius.listener.cart_blamer'] ?? $this->load('getSylius_Listener_CartBlamerService.php'));
        }, 1 => 'onInteractiveLogin'], 0);
        $instance->addListener('sylius.channel.pre_delete', [0 => function () {
            return ($this->services['sylius.listener.channel'] ?? $this->load('getSylius_Listener_ChannelService.php'));
        }, 1 => 'onChannelPreDelete'], 0);
        $instance->addListener('sylius.product.pre_create', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('sylius.product.pre_update', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('sylius.taxon.pre_create', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('sylius.taxon.pre_update', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('omni_sylius.banner.pre_create', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('omni_sylius.banner.pre_update', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('sylius.channel.pre_create', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('sylius.channel.pre_update', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('omni_sylius.node.pre_create', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('omni_sylius.node.pre_update', [0 => function () {
            return ($this->services['sylius.listener.images_upload'] ?? $this->load('getSylius_Listener_ImagesUploadService.php'));
        }, 1 => 'uploadImages'], 0);
        $instance->addListener('sylius.admin_user.pre_create', [0 => function () {
            return ($this->services['sylius.listener.avatar_upload'] ?? $this->load('getSylius_Listener_AvatarUploadService.php'));
        }, 1 => 'uploadImage'], 0);
        $instance->addListener('sylius.admin_user.pre_update', [0 => function () {
            return ($this->services['sylius.listener.avatar_upload'] ?? $this->load('getSylius_Listener_AvatarUploadService.php'));
        }, 1 => 'uploadImage'], 0);
        $instance->addListener('sylius.cart_change', [0 => function () {
            return ($this->services['sylius.listener.order_recalculation'] ?? $this->load('getSylius_Listener_OrderRecalculationService.php'));
        }, 1 => 'recalculateOrder'], 0);
        $instance->addListener('sylius.user.security.implicit_login', [0 => function () {
            return ($this->services['sylius.listener.user_cart_recalculation'] ?? $this->load('getSylius_Listener_UserCartRecalculationService.php'));
        }, 1 => 'recalculateCartWhileLogin'], 0);
        $instance->addListener('security.interactive_login', [0 => function () {
            return ($this->services['sylius.listener.user_cart_recalculation'] ?? $this->load('getSylius_Listener_UserCartRecalculationService.php'));
        }, 1 => 'recalculateCartWhileLogin'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->services['sylius.translator.listener'] ?? $this->getSylius_Translator_ListenerService());
        }, 1 => 'onKernelRequest'], 2);
        $instance->addListener('sylius.product_review.pre_create', [0 => function () {
            return ($this->services['sylius.listener.review_create'] ?? $this->load('getSylius_Listener_ReviewCreateService.php'));
        }, 1 => 'ensureReviewHasAuthor'], 0);
        $instance->addListener('sylius.product_variant.initialize_update', [0 => function () {
            return ($this->services['sylius.listener.locking'] ?? $this->load('getSylius_Listener_LockingService.php'));
        }, 1 => 'lock'], 0);
        $instance->addListener('sylius.product.initialize_update', [0 => function () {
            return ($this->services['sylius.listener.simple_product_locking'] ?? $this->load('getSylius_Listener_SimpleProductLockingService.php'));
        }, 1 => 'lock'], 0);
        $instance->addListener('sylius.address.pre_create', [0 => function () {
            return ($this->services['sylius.listener.customer_default_address'] ?? ($this->services['sylius.listener.customer_default_address'] = new \Sylius\Bundle\CoreBundle\EventListener\CustomerDefaultAddressListener()));
        }, 1 => 'preCreate'], 0);
        $instance->addListener('sylius.taxon.post_delete', [0 => function () {
            return ($this->services['sylius.listener.taxon_deletion'] ?? $this->load('getSylius_Listener_TaxonDeletionService.php'));
        }, 1 => 'removeTaxonFromPromotionRules'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->privates['sonata.block.cache.handler.default'] ?? ($this->privates['sonata.block.cache.handler.default'] = new \Sonata\BlockBundle\Cache\HttpCacheHandler()));
        }, 1 => 'onKernelResponse'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['fos_rest.body_listener'] ?? $this->getFosRest_BodyListenerService());
        }, 1 => 'onKernelRequest'], 10);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['fos_rest.format_listener'] ?? $this->getFosRest_FormatListenerService());
        }, 1 => 'onKernelRequest'], 34);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->services['payum.listener.reply_to_http_response'] ?? $this->load('getPayum_Listener_ReplyToHttpResponseService.php'));
        }, 1 => 'onKernelException'], 128);
        $instance->addListener('sylius.shipment.post_ship', [0 => function () {
            return ($this->services['sylius.listener.shipment_ship'] ?? $this->load('getSylius_Listener_ShipmentShipService.php'));
        }, 1 => 'sendConfirmationEmail'], 0);
        $instance->addListener('sylius.customer.pre_update', [0 => function () {
            return ($this->services['sylius.listener.email_updater'] ?? $this->load('getSylius_Listener_EmailUpdaterService.php'));
        }, 1 => 'eraseVerification'], 0);
        $instance->addListener('sylius.customer.post_update', [0 => function () {
            return ($this->services['sylius.listener.email_updater'] ?? $this->load('getSylius_Listener_EmailUpdaterService.php'));
        }, 1 => 'sendVerificationEmail'], 0);
        $instance->addListener('sylius.order.pre_complete', [0 => function () {
            return ($this->services['sylius.listener.order_customer_ip'] ?? $this->load('getSylius_Listener_OrderCustomerIpService.php'));
        }, 1 => 'assignCustomerIpToOrder'], 0);
        $instance->addListener('sylius.order.post_complete', [0 => function () {
            return ($this->services['sylius.listener.order_complete'] ?? $this->load('getSylius_Listener_OrderCompleteService.php'));
        }, 1 => 'sendConfirmationEmail'], 0);
        $instance->addListener('sylius.customer.post_register', [0 => function () {
            return ($this->services['sylius.listener.user_registration'] ?? $this->load('getSylius_Listener_UserRegistrationService.php'));
        }, 1 => 'handleUserVerification'], 0);
        $instance->addListener('sylius.order.pre_complete', [0 => function () {
            return ($this->services['sylius.listener.order_integrity_checker'] ?? $this->load('getSylius_Listener_OrderIntegrityCheckerService.php'));
        }, 1 => 'check'], 0);
        $instance->addListener('sylius.order.pre_complete', [0 => function () {
            return ($this->services['sylius.order_locale_assigner'] ?? $this->load('getSylius_OrderLocaleAssignerService.php'));
        }, 1 => 'assignLocale'], 0);
        $instance->addListener('sylius.user.security.impersonate', [0 => function () {
            return ($this->services['sylius.listener.user_impersonated'] ?? $this->load('getSylius_Listener_UserImpersonatedService.php'));
        }, 1 => 'onUserImpersonated'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->services['sylius.listener.non_channel_request_locale'] ?? $this->getSylius_Listener_NonChannelRequestLocaleService());
        }, 1 => 'restrictRequestLocale'], 10);
        $instance->addListener('sylius.order.post_address', [0 => function () {
            return ($this->privates['sylius.listener.checkout_redirect'] ?? $this->load('getSylius_Listener_CheckoutRedirectService.php'));
        }, 1 => 'handleCheckoutRedirect'], 0);
        $instance->addListener('sylius.order.post_select_shipping', [0 => function () {
            return ($this->privates['sylius.listener.checkout_redirect'] ?? $this->load('getSylius_Listener_CheckoutRedirectService.php'));
        }, 1 => 'handleCheckoutRedirect'], 0);
        $instance->addListener('sylius.order.post_payment', [0 => function () {
            return ($this->privates['sylius.listener.checkout_redirect'] ?? $this->load('getSylius_Listener_CheckoutRedirectService.php'));
        }, 1 => 'handleCheckoutRedirect'], 0);
        $instance->addListener('sylius.order_item.pre_create', [0 => function () {
            return ($this->services['sylius.listener.api.add_to_cart'] ?? $this->load('getSylius_Listener_Api_AddToCartService.php'));
        }, 1 => 'recalculateOrderOnAdd'], 0);
        $instance->addListener('sylius.order_item.pre_update', [0 => function () {
            return ($this->services['sylius.listener.api.add_to_cart'] ?? $this->load('getSylius_Listener_Api_AddToCartService.php'));
        }, 1 => 'recalculateOrderOnAdd'], 0);
        $instance->addListener('sylius.order_item.pre_delete', [0 => function () {
            return ($this->services['sylius.listener.api.add_to_cart'] ?? $this->load('getSylius_Listener_Api_AddToCartService.php'));
        }, 1 => 'recalculateOrderOnDelete'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['omni_banner.menu.content'] ?? ($this->privates['omni_banner.menu.content'] = new \Omni\Sylius\BannerPlugin\Menu\ContentMenuBuilder()));
        }, 1 => 'configureNodeMenu'], 0);
        $instance->addListener('sonata.block.event.sylius.admin.layout.javascripts', [0 => function () {
            return ($this->privates['omni_banner.block_event_listener.admin.layout.javascripts'] ?? ($this->privates['omni_banner.block_event_listener.admin.layout.javascripts'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@OmniSyliusBannerPlugin/_javascripts.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('sylius.product.post_create', [0 => function () {
            return ($this->privates['omni_search.event_listener.resource'] ?? $this->load('getOmniSearch_EventListener_ResourceService.php'));
        }, 1 => 'createResourceIndex'], 0);
        $instance->addListener('sylius.product.post_update', [0 => function () {
            return ($this->privates['omni_search.event_listener.resource'] ?? $this->load('getOmniSearch_EventListener_ResourceService.php'));
        }, 1 => 'createResourceIndex'], 0);
        $instance->addListener('sylius.product.pre_delete', [0 => function () {
            return ($this->privates['omni_search.event_listener.resource'] ?? $this->load('getOmniSearch_EventListener_ResourceService.php'));
        }, 1 => 'onRemove'], 0);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['webpack_encore.exception_listener'] ?? $this->load('getWebpackEncore_ExceptionListenerService.php'));
        }, 1 => 'onKernelException'], 0);
        $instance->addListener('sylius.product.pre_create', [0 => function () {
            return ($this->privates['omni_filter.event_listener.doctrine'] ?? $this->load('getOmniFilter_EventListener_DoctrineService.php'));
        }, 1 => 'preCreate'], 0);
        $instance->addListener('sylius.product.pre_update', [0 => function () {
            return ($this->privates['omni_filter.event_listener.doctrine'] ?? $this->load('getOmniFilter_EventListener_DoctrineService.php'));
        }, 1 => 'preUpdate'], 0);
        $instance->addListener('sonata.block.event.sylius.shop.layout.stylesheets', [0 => function () {
            return ($this->privates['omni_filter.block_event_listener.homepage.layout.stylesheets'] ?? ($this->privates['omni_filter.block_event_listener.homepage.layout.stylesheets'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@OmniSyliusFilterPlugin/layouts/stylesheets.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('sonata.block.event.sylius.shop.layout.javascripts', [0 => function () {
            return ($this->privates['omni_filter.block_event_listener.homepage.layout.javascripts'] ?? ($this->privates['omni_filter.block_event_listener.homepage.layout.javascripts'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@OmniSyliusFilterPlugin/layouts/javascripts.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('sylius.channel.pre_create', [0 => function () {
            return ($this->privates['omni_sylius.listener.logo_upload'] ?? $this->load('getOmniSylius_Listener_LogoUploadService.php'));
        }, 1 => 'uploadLogos'], 0);
        $instance->addListener('sylius.channel.pre_update', [0 => function () {
            return ($this->privates['omni_sylius.listener.logo_upload'] ?? $this->load('getOmniSylius_Listener_LogoUploadService.php'));
        }, 1 => 'uploadLogos'], 0);
        $instance->addListener('sylius.channel.pre_create', [0 => function () {
            return ($this->privates['omni_sylius.listener.logo_upload'] ?? $this->load('getOmniSylius_Listener_LogoUploadService.php'));
        }, 1 => 'uploadLogos'], 0);
        $instance->addListener('sylius.channel.pre_update', [0 => function () {
            return ($this->privates['omni_sylius.listener.logo_upload'] ?? $this->load('getOmniSylius_Listener_LogoUploadService.php'));
        }, 1 => 'uploadLogos'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['omni_sylius_cms.menu.content'] ?? ($this->privates['omni_sylius_cms.menu.content'] = new \Omni\Sylius\CmsPlugin\Menu\ContentMenuBuilder()));
        }, 1 => 'configureNodeMenu'], 0);
        $instance->addListener('sonata.block.event.sylius.admin.layout.javascripts', [0 => function () {
            return ($this->privates['omni_sylius.block_event_listener.admin.layout.javascripts.cms'] ?? ($this->privates['omni_sylius.block_event_listener.admin.layout.javascripts.cms'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@OmniSyliusCmsPlugin/_javascripts.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('omni.export_shipments', [0 => function () {
            return ($this->privates['omni.sylius.event.shipment_shipper_info'] ?? $this->load('getOmni_Sylius_Event_ShipmentShipperInfoService.php'));
        }, 1 => 'setShipperInfo'], -999);
        $instance->addListener('sonata.block.event.sylius.admin.layout.javascripts', [0 => function () {
            return ($this->privates['omni.sylius.block_event_listener.admin.layout.javascripts.shipper_configs'] ?? ($this->privates['omni.sylius.block_event_listener.admin.layout.javascripts.shipper_configs'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@OmniSyliusShippingPlugin/Admin/_javascripts.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('sonata.block.event.sylius.admin.layout.stylesheets', [0 => function () {
            return ($this->privates['omni.sylius.block_event_listener.admin.layout.stylesheets.shipper_configs'] ?? ($this->privates['omni.sylius.block_event_listener.admin.layout.stylesheets.shipper_configs'] = new \Sylius\Bundle\UiBundle\Block\BlockEventListener('@OmniSyliusShippingPlugin/Admin/_stylesheets.html.twig')));
        }, 1 => 'onBlockEvent'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['omni_parcel_machine.menu.parcel_machine'] ?? ($this->privates['omni_parcel_machine.menu.parcel_machine'] = new \Omni\Sylius\ParcelMachinePlugin\Menu\ParcelMachineMenuBuilder()));
        }, 1 => 'buildMenu'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['omni_sylis_manifest.menu.builder'] ?? ($this->privates['omni_sylis_manifest.menu.builder'] = new \Omni\Sylius\ManifestPlugin\Menu\ContentMenuBuilder(true)));
        }, 1 => 'configureMenu'], 0);
        $instance->addListener('lexik_translation.event.get_database_resources', [0 => function () {
            return ($this->privates['lexik_translation.listener.get_database_resources'] ?? $this->load('getLexikTranslation_Listener_GetDatabaseResourcesService.php'));
        }, 1 => 'onGetDatabaseResources'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['omni_sylius_translator.event_listener.admin_menu'] ?? ($this->privates['omni_sylius_translator.event_listener.admin_menu'] = new \Omni\Sylius\TranslatorPlugin\EventListener\AdminMenuListener()));
        }, 1 => 'onAdminMenuItems'], 0);
        $instance->addListener('sylius.menu.admin.main', [0 => function () {
            return ($this->privates['omni_import.sylius.menu.configuration.import_job'] ?? ($this->privates['omni_import.sylius.menu.configuration.import_job'] = new \Omni\Sylius\ImportPlugin\Menu\ContentMenuBuilder()));
        }, 1 => 'configureMenu'], 0);
        $instance->addListener('sylius.grid.admin_country', [0 => function () {
            return ($this->privates['app.grid_event_listener.admin.crud_sylius.country_csv_json_export'] ?? $this->load('getApp_GridEventListener_Admin_CrudSylius_CountryCsvJsonExportService.php'));
        }, 1 => 'onSyliusGridAdmin'], 0);
        $instance->addListener('sylius.grid.admin_order', [0 => function () {
            return ($this->privates['app.grid_event_listener.admin.crud_sylius.order_csv_json_export'] ?? $this->load('getApp_GridEventListener_Admin_CrudSylius_OrderCsvJsonExportService.php'));
        }, 1 => 'onSyliusGridAdmin'], 0);
        $instance->addListener('sylius.grid.admin_customer', [0 => function () {
            return ($this->privates['app.grid_event_listener.admin.crud_sylius.customer_csv_json_export'] ?? $this->load('getApp_GridEventListener_Admin_CrudSylius_CustomerCsvJsonExportService.php'));
        }, 1 => 'onSyliusGridAdmin'], 0);
        $instance->addListener('sylius.grid.admin_product', [0 => function () {
            return ($this->privates['app.grid_event_listener.admin.crud_sylius.product_csv_export'] ?? $this->load('getApp_GridEventListener_Admin_CrudSylius_ProductCsvExportService.php'));
        }, 1 => 'onSyliusGridAdmin'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->privates['response_listener'] ?? ($this->privates['response_listener'] = new \Symfony\Component\HttpKernel\EventListener\ResponseListener('UTF-8')));
        }, 1 => 'onKernelResponse'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->privates['streamed_response_listener'] ?? ($this->privates['streamed_response_listener'] = new \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener()));
        }, 1 => 'onKernelResponse'], -1024);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['locale_listener'] ?? $this->getLocaleListenerService());
        }, 1 => 'setDefaultLocale'], 100);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['locale_listener'] ?? $this->getLocaleListenerService());
        }, 1 => 'onKernelRequest'], 16);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ($this->privates['locale_listener'] ?? $this->getLocaleListenerService());
        }, 1 => 'onKernelFinishRequest'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['validate_request_listener'] ?? ($this->privates['validate_request_listener'] = new \Symfony\Component\HttpKernel\EventListener\ValidateRequestListener()));
        }, 1 => 'onKernelRequest'], 256);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['.legacy_resolve_controller_name_subscriber'] ?? $this->get_LegacyResolveControllerNameSubscriberService());
        }, 1 => 'resolveControllerName'], 24);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->privates['disallow_search_engine_index_response_listener'] ?? ($this->privates['disallow_search_engine_index_response_listener'] = new \Symfony\Component\HttpKernel\EventListener\DisallowRobotsIndexingListener()));
        }, 1 => 'onResponse'], -255);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['locale_aware_listener'] ?? $this->getLocaleAwareListenerService());
        }, 1 => 'onKernelRequest'], 15);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ($this->privates['locale_aware_listener'] ?? $this->getLocaleAwareListenerService());
        }, 1 => 'onKernelFinishRequest'], -15);
        $instance->addListener('console.error', [0 => function () {
            return ($this->privates['console.error_listener'] ?? $this->load('getConsole_ErrorListenerService.php'));
        }, 1 => 'onConsoleError'], -128);
        $instance->addListener('console.terminate', [0 => function () {
            return ($this->privates['console.error_listener'] ?? $this->load('getConsole_ErrorListenerService.php'));
        }, 1 => 'onConsoleTerminate'], -128);
        $instance->addListener('console.error', [0 => function () {
            return ($this->privates['console.suggest_missing_package_subscriber'] ?? ($this->privates['console.suggest_missing_package_subscriber'] = new \Symfony\Bundle\FrameworkBundle\EventListener\SuggestMissingPackageSubscriber()));
        }, 1 => 'onConsoleError'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['session_listener'] ?? $this->getSessionListenerService());
        }, 1 => 'onKernelRequest'], 128);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->privates['session_listener'] ?? $this->getSessionListenerService());
        }, 1 => 'onKernelResponse'], -1000);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ($this->privates['session_listener'] ?? $this->getSessionListenerService());
        }, 1 => 'onFinishRequest'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['debug.debug_handlers_listener'] ?? $this->getDebug_DebugHandlersListenerService());
        }, 1 => 'configure'], 2048);
        $instance->addListener('console.command', [0 => function () {
            return ($this->privates['debug.debug_handlers_listener'] ?? $this->getDebug_DebugHandlersListenerService());
        }, 1 => 'configure'], 2048);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['router_listener'] ?? $this->getRouterListenerService());
        }, 1 => 'onKernelRequest'], 32);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ($this->privates['router_listener'] ?? $this->getRouterListenerService());
        }, 1 => 'onKernelFinishRequest'], 0);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['router_listener'] ?? $this->getRouterListenerService());
        }, 1 => 'onKernelException'], -64);
        $instance->addListener('console.command', [0 => function () {
            return ($this->privates['monolog.handler.console'] ?? $this->getMonolog_Handler_ConsoleService());
        }, 1 => 'onCommand'], 255);
        $instance->addListener('console.terminate', [0 => function () {
            return ($this->privates['monolog.handler.console'] ?? $this->getMonolog_Handler_ConsoleService());
        }, 1 => 'onTerminate'], -255);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->privates['security.rememberme.response_listener'] ?? ($this->privates['security.rememberme.response_listener'] = new \Symfony\Component\Security\Http\RememberMe\ResponseListener()));
        }, 1 => 'onKernelResponse'], 0);
        $instance->addListener('debug.security.authorization.vote', [0 => function () {
            return ($this->privates['debug.security.voter.vote_listener'] ?? $this->load('getDebug_Security_Voter_VoteListenerService.php'));
        }, 1 => 'onVoterVote'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['debug.security.firewall'] ?? $this->getDebug_Security_FirewallService());
        }, 1 => 'configureLogoutUrlGenerator'], 8);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['debug.security.firewall'] ?? $this->getDebug_Security_FirewallService());
        }, 1 => 'onKernelRequest'], 8);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ($this->privates['debug.security.firewall'] ?? $this->getDebug_Security_FirewallService());
        }, 1 => 'onKernelFinishRequest'], 0);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['swiftmailer.email_sender.listener'] ?? $this->load('getSwiftmailer_EmailSender_ListenerService.php'));
        }, 1 => 'onException'], 0);
        $instance->addListener('kernel.terminate', [0 => function () {
            return ($this->privates['swiftmailer.email_sender.listener'] ?? $this->load('getSwiftmailer_EmailSender_ListenerService.php'));
        }, 1 => 'onTerminate'], 0);
        $instance->addListener('console.error', [0 => function () {
            return ($this->privates['swiftmailer.email_sender.listener'] ?? $this->load('getSwiftmailer_EmailSender_ListenerService.php'));
        }, 1 => 'onException'], 0);
        $instance->addListener('console.terminate', [0 => function () {
            return ($this->privates['swiftmailer.email_sender.listener'] ?? $this->load('getSwiftmailer_EmailSender_ListenerService.php'));
        }, 1 => 'onTerminate'], 0);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['twig.exception_listener'] ?? $this->load('getTwig_ExceptionListenerService.php'));
        }, 1 => 'logKernelException'], 0);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['twig.exception_listener'] ?? $this->load('getTwig_ExceptionListenerService.php'));
        }, 1 => 'onKernelException'], -128);
        $instance->addListener('security.interactive_login', [0 => function () {
            return ($this->privates['sylius.listener.admin_user_last_login'] ?? $this->load('getSylius_Listener_AdminUserLastLoginService.php'));
        }, 1 => 'onSecurityInteractiveLogin'], 0);
        $instance->addListener('sylius.user.security.implicit_login', [0 => function () {
            return ($this->privates['sylius.listener.admin_user_last_login'] ?? $this->load('getSylius_Listener_AdminUserLastLoginService.php'));
        }, 1 => 'onImplicitLogin'], 0);
        $instance->addListener('security.interactive_login', [0 => function () {
            return ($this->privates['sylius.listener.shop_user_last_login'] ?? $this->load('getSylius_Listener_ShopUserLastLoginService.php'));
        }, 1 => 'onSecurityInteractiveLogin'], 0);
        $instance->addListener('sylius.user.security.implicit_login', [0 => function () {
            return ($this->privates['sylius.listener.shop_user_last_login'] ?? $this->load('getSylius_Listener_ShopUserLastLoginService.php'));
        }, 1 => 'onImplicitLogin'], 0);
        $instance->addListener('security.interactive_login', [0 => function () {
            return ($this->privates['sylius.listener.oauth_user_last_login'] ?? $this->load('getSylius_Listener_OauthUserLastLoginService.php'));
        }, 1 => 'onSecurityInteractiveLogin'], 0);
        $instance->addListener('sylius.user.security.implicit_login', [0 => function () {
            return ($this->privates['sylius.listener.oauth_user_last_login'] ?? $this->load('getSylius_Listener_OauthUserLastLoginService.php'));
        }, 1 => 'onImplicitLogin'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->services['sylius.storage.cookie'] ?? ($this->services['sylius.storage.cookie'] = new \Sylius\Bundle\ResourceBundle\Storage\CookieStorage()));
        }, 1 => 'onKernelRequest'], 1024);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->services['sylius.storage.cookie'] ?? ($this->services['sylius.storage.cookie'] = new \Sylius\Bundle\ResourceBundle\Storage\CookieStorage()));
        }, 1 => 'onKernelResponse'], -1024);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['fos_rest.exception_listener'] ?? $this->load('getFosRest_ExceptionListenerService.php'));
        }, 1 => 'onKernelException'], -100);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['stof_doctrine_extensions.event_listener.logger'] ?? $this->getStofDoctrineExtensions_EventListener_LoggerService());
        }, 1 => 'onKernelRequest'], 0);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['pagerfanta.convert_not_valid_max_per_page_to_not_found_listener'] ?? ($this->privates['pagerfanta.convert_not_valid_max_per_page_to_not_found_listener'] = new \WhiteOctober\PagerfantaBundle\EventListener\ConvertNotValidMaxPerPageToNotFoundListener()));
        }, 1 => 'onException'], 512);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->privates['pagerfanta.convert_not_valid_current_page_to_not_found_listener'] ?? ($this->privates['pagerfanta.convert_not_valid_current_page_to_not_found_listener'] = new \WhiteOctober\PagerfantaBundle\EventListener\ConvertNotValidCurrentPageToNotFoundListener()));
        }, 1 => 'onException'], 512);
        $instance->addListener('console.command', [0 => function () {
            return ($this->services['sylius_fixtures.logger.handler.console'] ?? $this->load('getSyliusFixtures_Logger_Handler_ConsoleService.php'));
        }, 1 => 'onCommand'], 255);
        $instance->addListener('console.terminate', [0 => function () {
            return ($this->services['sylius_fixtures.logger.handler.console'] ?? $this->load('getSyliusFixtures_Logger_Handler_ConsoleService.php'));
        }, 1 => 'onTerminate'], -255);
        $instance->addListener('kernel.exception', [0 => function () {
            return ($this->services['sylius.event_subscriber.resource_delete'] ?? $this->load('getSylius_EventSubscriber_ResourceDeleteService.php'));
        }, 1 => 'onResourceDelete'], 0);
        $instance->addListener('kernel.response', [0 => function () {
            return ($this->services['sylius.listener.session_cart'] ?? $this->getSylius_Listener_SessionCartService());
        }, 1 => 'onKernelResponse'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['sylius.resolver.checkout'] ?? $this->getSylius_Resolver_CheckoutService());
        }, 1 => 'onKernelRequest'], 0);
        $instance->addListener('kernel.finish_request', [0 => function () {
            return ($this->privates['Symfony\\WebpackEncoreBundle\\EventListener\\ResetAssetsEventListener'] ?? $this->getResetAssetsEventListenerService());
        }, 1 => 'resetAssets'], 0);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['httplug.strategy_listener'] ?? ($this->privates['httplug.strategy_listener'] = new \Http\HttplugBundle\Discovery\ConfiguredClientsStrategyListener()));
        }, 1 => 'onEvent'], 1024);
        $instance->addListener('console.command', [0 => function () {
            return ($this->privates['httplug.strategy_listener'] ?? ($this->privates['httplug.strategy_listener'] = new \Http\HttplugBundle\Discovery\ConfiguredClientsStrategyListener()));
        }, 1 => 'onEvent'], 1024);
        $instance->addListener('kernel.request', [0 => function () {
            return ($this->privates['Http\\HttplugBundle\\Collector\\PluginClientFactoryListener'] ?? $this->getPluginClientFactoryListenerService());
        }, 1 => 'onEvent'], 1024);
        $instance->addListener('console.command', [0 => function () {
            return ($this->privates['Http\\HttplugBundle\\Collector\\PluginClientFactoryListener'] ?? $this->getPluginClientFactoryListenerService());
        }, 1 => 'onEvent'], 1024);

        return $instance;
    }

    /**
     * Gets the public 'http_kernel' shared service.
     *
     * @return \Symfony\Component\HttpKernel\HttpKernel
     */
    protected function getHttpKernelService()
    {
        $a = ($this->privates['debug.stopwatch'] ?? ($this->privates['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch(true)));

        return $this->services['http_kernel'] = new \Symfony\Component\HttpKernel\HttpKernel(($this->services['event_dispatcher'] ?? $this->getEventDispatcherService()), new \Symfony\Component\HttpKernel\Controller\TraceableControllerResolver(new \Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver($this, ($this->privates['monolog.logger.request'] ?? $this->getMonolog_Logger_RequestService()), ($this->privates['.legacy_controller_name_converter'] ?? ($this->privates['.legacy_controller_name_converter'] = new \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser(($this->services['kernel'] ?? $this->get('kernel', 1)), false)))), $a), ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())), new \Symfony\Component\HttpKernel\Controller\TraceableArgumentResolver(new \Symfony\Component\HttpKernel\Controller\ArgumentResolver(new \Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactory(), new RewindableGenerator(function () {
            yield 0 => ($this->privates['debug.argument_resolver.request_attribute'] ?? $this->load('getDebug_ArgumentResolver_RequestAttributeService.php'));
            yield 1 => ($this->privates['debug.argument_resolver.request'] ?? $this->load('getDebug_ArgumentResolver_RequestService.php'));
            yield 2 => ($this->privates['debug.argument_resolver.session'] ?? $this->load('getDebug_ArgumentResolver_SessionService.php'));
            yield 3 => ($this->privates['debug.security.user_value_resolver'] ?? $this->load('getDebug_Security_UserValueResolverService.php'));
            yield 4 => ($this->privates['debug.argument_resolver.service'] ?? $this->load('getDebug_ArgumentResolver_ServiceService.php'));
            yield 5 => ($this->privates['debug.argument_resolver.default'] ?? $this->load('getDebug_ArgumentResolver_DefaultService.php'));
            yield 6 => ($this->privates['debug.argument_resolver.variadic'] ?? $this->load('getDebug_ArgumentResolver_VariadicService.php'));
            yield 7 => ($this->privates['debug.argument_resolver.not_tagged_controller'] ?? $this->load('getDebug_ArgumentResolver_NotTaggedControllerService.php'));
        }, 8)), $a));
    }

    /**
     * Gets the public 'lexik_translation.translator' shared service.
     *
     * @return \Lexik\Bundle\TranslationBundle\Translation\Translator
     */
    protected function getLexikTranslation_TranslatorService()
    {
        $this->services['lexik_translation.translator'] = $instance = new \Lexik\Bundle\TranslationBundle\Translation\Translator(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            'event_dispatcher' => ['services', 'event_dispatcher', 'getEventDispatcherService', false],
            'lexik_translation.loader.database' => ['privates', 'lexik_translation.loader.database', 'getLexikTranslation_Loader_DatabaseService.php', true],
            'translation.loader.csv' => ['privates', 'translation.loader.csv', 'getTranslation_Loader_CsvService.php', true],
            'translation.loader.dat' => ['privates', 'translation.loader.dat', 'getTranslation_Loader_DatService.php', true],
            'translation.loader.ini' => ['privates', 'translation.loader.ini', 'getTranslation_Loader_IniService.php', true],
            'translation.loader.json' => ['privates', 'translation.loader.json', 'getTranslation_Loader_JsonService.php', true],
            'translation.loader.mo' => ['privates', 'translation.loader.mo', 'getTranslation_Loader_MoService.php', true],
            'translation.loader.php' => ['privates', 'translation.loader.php', 'getTranslation_Loader_PhpService.php', true],
            'translation.loader.po' => ['privates', 'translation.loader.po', 'getTranslation_Loader_PoService.php', true],
            'translation.loader.qt' => ['privates', 'translation.loader.qt', 'getTranslation_Loader_QtService.php', true],
            'translation.loader.res' => ['privates', 'translation.loader.res', 'getTranslation_Loader_ResService.php', true],
            'translation.loader.xliff' => ['privates', 'translation.loader.xliff', 'getTranslation_Loader_XliffService.php', true],
            'translation.loader.yml' => ['privates', 'translation.loader.yml', 'getTranslation_Loader_YmlService.php', true],
        ], [
            'event_dispatcher' => '?',
            'lexik_translation.loader.database' => '?',
            'translation.loader.csv' => '?',
            'translation.loader.dat' => '?',
            'translation.loader.ini' => '?',
            'translation.loader.json' => '?',
            'translation.loader.mo' => '?',
            'translation.loader.php' => '?',
            'translation.loader.po' => '?',
            'translation.loader.qt' => '?',
            'translation.loader.res' => '?',
            'translation.loader.xliff' => '?',
            'translation.loader.yml' => '?',
        ]), ($this->privates['translator.formatter.default'] ?? $this->getTranslator_Formatter_DefaultService()), $this->getEnv('DEFAULT_LOCALE'), ['translation.loader.php' => [0 => 'php'], 'translation.loader.yml' => [0 => 'yaml', 1 => 'yml'], 'translation.loader.xliff' => [0 => 'xlf', 1 => 'xliff'], 'translation.loader.po' => [0 => 'po'], 'translation.loader.mo' => [0 => 'mo'], 'translation.loader.qt' => [0 => 'ts'], 'translation.loader.csv' => [0 => 'csv'], 'translation.loader.res' => [0 => 'res'], 'translation.loader.dat' => [0 => 'dat'], 'translation.loader.ini' => [0 => 'ini'], 'translation.loader.json' => [0 => 'json'], 'lexik_translation.loader.database' => [0 => 'database']], $this->getParameter('lexik_translation.translator.options'));

        $instance->setConfigCacheFactory(($this->privates['config_cache_factory'] ?? $this->getConfigCacheFactoryService()));
        $instance->setFallbackLocales($this->parameters['lexik_translation.fallback_locale']);
        $instance->addResource('xlf', (\dirname(__DIR__, 4).'/vendor/symfony/validator/Resources/translations/validators.lt.xlf'), 'lt', 'validators');
        $instance->addResource('xlf', (\dirname(__DIR__, 4).'/vendor/symfony/validator/Resources/translations/validators.en.xlf'), 'en', 'validators');
        $instance->addResource('xlf', (\dirname(__DIR__, 4).'/vendor/symfony/form/Resources/translations/validators.lt.xlf'), 'lt', 'validators');
        $instance->addResource('xlf', (\dirname(__DIR__, 4).'/vendor/symfony/form/Resources/translations/validators.en.xlf'), 'en', 'validators');
        $instance->addResource('xlf', (\dirname(__DIR__, 4).'/vendor/symfony/security/Core/Exception/../Resources/translations/security.en.xlf'), 'en', 'security');
        $instance->addResource('xlf', (\dirname(__DIR__, 4).'/vendor/symfony/security/Core/Exception/../Resources/translations/security.lt.xlf'), 'lt', 'security');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/OrderBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/OrderBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/OrderBundle/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/OrderBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CurrencyBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CurrencyBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CurrencyBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CurrencyBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AttributeBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AttributeBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AttributeBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AttributeBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxationBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxationBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxationBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxationBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShippingBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShippingBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShippingBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShippingBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShippingBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AddressingBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AddressingBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AddressingBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AddressingBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxonomyBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxonomyBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxonomyBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxonomyBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UiBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UiBundle/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UiBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UiBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ReviewBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ReviewBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ReviewBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ReviewBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ReviewBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('xliff', (\dirname(__DIR__, 4).'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.en.xliff'), 'en', 'SonataCoreBundle');
        $instance->addResource('xliff', (\dirname(__DIR__, 4).'/vendor/sonata-project/core-bundle/src/CoreBundle/Resources/translations/SonataCoreBundle.lt.xliff'), 'lt', 'SonataCoreBundle');
        $instance->addResource('xliff', (\dirname(__DIR__, 4).'/vendor/sonata-project/block-bundle/src/Resources/translations/validators.en.xliff'), 'en', 'validators');
        $instance->addResource('xliff', (\dirname(__DIR__, 4).'/vendor/sonata-project/block-bundle/src/Resources/translations/SonataBlockBundle.en.xliff'), 'en', 'SonataBlockBundle');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/payum/payum-bundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/payum/payum-bundle/Resources/translations/PayumBundle.en.yml'), 'en', 'PayumBundle');
        $instance->addResource('xliff', (\dirname(__DIR__, 4).'/vendor/white-october/pagerfanta-bundle/Resources/translations/pagerfanta.en.xliff'), 'en', 'pagerfanta');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PayumBundle/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PayumBundle/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PayumBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PayumBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/friendsofsymfony/oauth-server-bundle/Resources/translations/FOSOAuthServerBundle.en.yml'), 'en', 'FOSOAuthServerBundle');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-banner-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-banner-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-filter-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-filter-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-core-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-core-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/Resources/translations/validators.en.yaml'), 'en', 'validators');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/Resources/translations/validators.lt.yaml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-seo-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-seo-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/bitbag/shipping-export-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-shipping-plugin/src/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-shipping-plugin/src/Resources/translations/validators.lt.yml'), 'lt', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-shipping-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-shipping-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-dpd-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-dpd-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-parcel-machine-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-parcel-machine-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-paysera-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-paysera-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/friendsofsylius/sylius-import-export-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-swedbank-spp-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-swedbank-spp-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/brille24/sylius-tierprice-plugin/src/Resources/translations/validators.en.yml'), 'en', 'validators');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/brille24/sylius-tierprice-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/hwi/oauth-bundle/Resources/translations/HWIOAuthBundle.en.yml'), 'en', 'HWIOAuthBundle');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-manifest-plugin/src/Resources/translations/flashes.en.yml'), 'en', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-manifest-plugin/src/Resources/translations/flashes.lt.yml'), 'lt', 'flashes');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-manifest-plugin/src/Resources/translations/messages.lt.yml'), 'lt', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-manifest-plugin/src/Resources/translations/messages.en.yml'), 'en', 'messages');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/lexik/translation-bundle/Resources/translations/LexikTranslationBundle.en.yml'), 'en', 'LexikTranslationBundle');
        $instance->addResource('yml', (\dirname(__DIR__, 4).'/vendor/lexik/translation-bundle/Resources/translations/LexikTranslationBundle.lt.yml'), 'lt', 'LexikTranslationBundle');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-import-plugin/src/Resources/translations/messages.lt.yaml'), 'lt', 'messages');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/vendor/omni/sylius-import-plugin/src/Resources/translations/messages.en.yaml'), 'en', 'messages');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/translations/messages.lt.yaml'), 'lt', 'messages');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/translations/validators.en.yaml'), 'en', 'validators');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/translations/validators.lt.yaml'), 'lt', 'validators');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/translations/flashes.lt.yaml'), 'lt', 'flashes');
        $instance->addResource('yaml', (\dirname(__DIR__, 4).'/translations/messages.en.yaml'), 'en', 'messages');
        $instance->addDatabaseResources();

        return $instance;
    }

    /**
     * Gets the public 'request_stack' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\RequestStack
     */
    protected function getRequestStackService()
    {
        return $this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack();
    }

    /**
     * Gets the public 'router' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\Routing\Router
     */
    protected function getRouterService()
    {
        $a = new \Symfony\Bridge\Monolog\Logger('router');
        $a->pushHandler(($this->privates['monolog.handler.console'] ?? $this->getMonolog_Handler_ConsoleService()));
        $a->pushHandler(($this->privates['monolog.handler.main'] ?? $this->getMonolog_Handler_MainService()));

        $this->services['router'] = $instance = new \Symfony\Bundle\FrameworkBundle\Routing\Router((new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            'routing.loader' => ['services', 'routing.loader', 'getRouting_LoaderService.php', true],
        ], [
            'routing.loader' => 'Symfony\\Component\\Config\\Loader\\LoaderInterface',
        ]))->withContext('router.default', $this), 'kernel::loadRoutes', ['cache_dir' => $this->targetDir.'', 'debug' => true, 'generator_class' => 'Symfony\\Component\\Routing\\Generator\\CompiledUrlGenerator', 'generator_dumper_class' => 'Symfony\\Component\\Routing\\Generator\\Dumper\\CompiledUrlGeneratorDumper', 'matcher_class' => 'Symfony\\Bundle\\FrameworkBundle\\Routing\\RedirectableCompiledUrlMatcher', 'matcher_dumper_class' => 'Symfony\\Component\\Routing\\Matcher\\Dumper\\CompiledUrlMatcherDumper', 'strict_requirements' => NULL, 'resource_type' => 'service'], ($this->privates['router.request_context'] ?? $this->getRouter_RequestContextService()), ($this->privates['parameter_bag'] ?? ($this->privates['parameter_bag'] = new \Symfony\Component\DependencyInjection\ParameterBag\ContainerBag($this))), $a, $this->getEnv('DEFAULT_LOCALE'));

        $instance->setConfigCacheFactory(($this->privates['config_cache_factory'] ?? $this->getConfigCacheFactoryService()));

        return $instance;
    }

    /**
     * Gets the public 'security.authorization_checker' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\AuthorizationChecker
     */
    protected function getSecurity_AuthorizationCheckerService()
    {
        return $this->services['security.authorization_checker'] = new \Symfony\Component\Security\Core\Authorization\AuthorizationChecker(($this->services['security.token_storage'] ?? $this->getSecurity_TokenStorageService()), ($this->privates['security.authentication.manager'] ?? $this->getSecurity_Authentication_ManagerService()), ($this->privates['debug.security.access.decision_manager'] ?? $this->getDebug_Security_Access_DecisionManagerService()), true);
    }

    /**
     * Gets the public 'security.token_storage' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage
     */
    protected function getSecurity_TokenStorageService()
    {
        return $this->services['security.token_storage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage(($this->privates['security.untracked_token_storage'] ?? ($this->privates['security.untracked_token_storage'] = new \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage())), new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            'session' => ['services', 'session', 'getSessionService', false],
        ], [
            'session' => '?',
        ]));
    }

    /**
     * Gets the public 'session' shared service.
     *
     * @return \Symfony\Component\HttpFoundation\Session\Session
     */
    protected function getSessionService()
    {
        return $this->services['session'] = new \Symfony\Component\HttpFoundation\Session\Session(new \Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage($this->parameters['session.storage.options'], NULL, new \Symfony\Component\HttpFoundation\Session\Storage\MetadataBag('_sf2_meta', 0)));
    }

    /**
     * Gets the public 'sm.callback_factory' shared service.
     *
     * @return \winzou\Bundle\StateMachineBundle\Callback\ContainerAwareCallbackFactory
     */
    protected function getSm_CallbackFactoryService()
    {
        return $this->services['sm.callback_factory'] = new \winzou\Bundle\StateMachineBundle\Callback\ContainerAwareCallbackFactory('winzou\\Bundle\\StateMachineBundle\\Callback\\ContainerAwareCallback', $this);
    }

    /**
     * Gets the public 'sm.factory' shared service.
     *
     * @return \SM\Factory\Factory
     */
    protected function getSm_FactoryService()
    {
        return $this->services['sm.factory'] = new \SM\Factory\Factory($this->parameters['sm.configs'], ($this->services['event_dispatcher'] ?? $this->getEventDispatcherService()), ($this->services['sm.callback_factory'] ?? ($this->services['sm.callback_factory'] = new \winzou\Bundle\StateMachineBundle\Callback\ContainerAwareCallbackFactory('winzou\\Bundle\\StateMachineBundle\\Callback\\ContainerAwareCallback', $this))));
    }

    /**
     * Gets the public 'sylius.context.cart' shared service.
     *
     * @return \Sylius\Component\Order\Context\CompositeCartContext
     */
    protected function getSylius_Context_CartService()
    {
        $this->services['sylius.context.cart'] = $instance = new \Sylius\Component\Order\Context\CompositeCartContext();

        $instance->addContext(($this->services['sylius.context.cart.new'] ?? $this->getSylius_Context_Cart_NewService()), -999);
        $instance->addContext(($this->services['sylius.context.cart.customer_and_channel_based'] ?? $this->getSylius_Context_Cart_CustomerAndChannelBasedService()), -555);
        $instance->addContext(($this->services['sylius.context.cart.session_and_channel_based'] ?? $this->getSylius_Context_Cart_SessionAndChannelBasedService()), -777);

        return $instance;
    }

    /**
     * Gets the public 'sylius.context.cart.customer_and_channel_based' shared service.
     *
     * @return \Sylius\Bundle\CoreBundle\Context\CustomerAndChannelBasedCartContext
     */
    protected function getSylius_Context_Cart_CustomerAndChannelBasedService()
    {
        return $this->services['sylius.context.cart.customer_and_channel_based'] = new \Sylius\Bundle\CoreBundle\Context\CustomerAndChannelBasedCartContext(($this->services['sylius.context.customer'] ?? $this->getSylius_Context_CustomerService()), ($this->services['sylius.context.channel'] ?? $this->getSylius_Context_ChannelService()), ($this->services['sylius.repository.order'] ?? $this->getSylius_Repository_OrderService()));
    }

    /**
     * Gets the public 'sylius.context.cart.new' shared service.
     *
     * @return \Sylius\Component\Core\Cart\Context\ShopBasedCartContext
     */
    protected function getSylius_Context_Cart_NewService()
    {
        return $this->services['sylius.context.cart.new'] = new \Sylius\Component\Core\Cart\Context\ShopBasedCartContext(new \Sylius\Component\Order\Context\CartContext(($this->services['sylius.factory.order'] ?? ($this->services['sylius.factory.order'] = new \Sylius\Component\Resource\Factory\Factory('App\\Entity\\Order\\Order')))), ($this->services['sylius.context.shopper'] ?? $this->getSylius_Context_ShopperService()));
    }

    /**
     * Gets the public 'sylius.context.cart.session_and_channel_based' shared service.
     *
     * @return \Sylius\Bundle\CoreBundle\Context\SessionAndChannelBasedCartContext
     */
    protected function getSylius_Context_Cart_SessionAndChannelBasedService()
    {
        return $this->services['sylius.context.cart.session_and_channel_based'] = new \Sylius\Bundle\CoreBundle\Context\SessionAndChannelBasedCartContext(($this->services['sylius.storage.cart_session'] ?? $this->getSylius_Storage_CartSessionService()), ($this->services['sylius.context.channel'] ?? $this->getSylius_Context_ChannelService()));
    }

    /**
     * Gets the public 'sylius.context.channel' shared service.
     *
     * @return \Sylius\Component\Channel\Context\CachedPerRequestChannelContext
     */
    protected function getSylius_Context_ChannelService()
    {
        return $this->services['sylius.context.channel'] = new \Sylius\Component\Channel\Context\CachedPerRequestChannelContext(($this->services['Sylius\\Component\\Channel\\Context\\ChannelContextInterface'] ?? $this->getChannelContextInterfaceService()), ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())));
    }

    /**
     * Gets the public 'sylius.context.channel.fake_channel.persister' shared service.
     *
     * @return \Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelPersister
     */
    protected function getSylius_Context_Channel_FakeChannel_PersisterService()
    {
        return $this->services['sylius.context.channel.fake_channel.persister'] = new \Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelPersister(($this->services['Sylius\\Bundle\\ChannelBundle\\Context\\FakeChannel\\FakeChannelCodeProviderInterface'] ?? ($this->services['Sylius\\Bundle\\ChannelBundle\\Context\\FakeChannel\\FakeChannelCodeProviderInterface'] = new \Sylius\Bundle\ChannelBundle\Context\FakeChannel\FakeChannelCodeProvider())));
    }

    /**
     * Gets the public 'sylius.context.currency.channel_aware' shared service.
     *
     * @return \Sylius\Component\Core\Currency\Context\ChannelAwareCurrencyContext
     */
    protected function getSylius_Context_Currency_ChannelAwareService()
    {
        $a = new \Sylius\Component\Currency\Context\CompositeCurrencyContext();
        $a->addContext(($this->services['sylius.context.currency.storage_based'] ?? $this->getSylius_Context_Currency_StorageBasedService()), 0);

        return $this->services['sylius.context.currency.channel_aware'] = new \Sylius\Component\Core\Currency\Context\ChannelAwareCurrencyContext($a, ($this->services['sylius.context.channel'] ?? $this->getSylius_Context_ChannelService()));
    }

    /**
     * Gets the public 'sylius.context.currency.storage_based' shared service.
     *
     * @return \Sylius\Component\Core\Currency\Context\StorageBasedCurrencyContext
     */
    protected function getSylius_Context_Currency_StorageBasedService()
    {
        return $this->services['sylius.context.currency.storage_based'] = new \Sylius\Component\Core\Currency\Context\StorageBasedCurrencyContext(($this->services['sylius.context.channel'] ?? $this->getSylius_Context_ChannelService()), ($this->services['sylius.storage.currency'] ?? $this->getSylius_Storage_CurrencyService()));
    }

    /**
     * Gets the public 'sylius.context.customer' shared service.
     *
     * @return \Sylius\Bundle\CoreBundle\Context\CustomerContext
     */
    protected function getSylius_Context_CustomerService()
    {
        return $this->services['sylius.context.customer'] = new \Sylius\Bundle\CoreBundle\Context\CustomerContext(($this->services['security.token_storage'] ?? $this->getSecurity_TokenStorageService()), ($this->services['security.authorization_checker'] ?? $this->getSecurity_AuthorizationCheckerService()));
    }

    /**
     * Gets the public 'sylius.context.locale.admin_based' shared service.
     *
     * @return \Sylius\Bundle\AdminBundle\Context\AdminBasedLocaleContext
     */
    protected function getSylius_Context_Locale_AdminBasedService()
    {
        return $this->services['sylius.context.locale.admin_based'] = new \Sylius\Bundle\AdminBundle\Context\AdminBasedLocaleContext(($this->services['security.token_storage'] ?? $this->getSecurity_TokenStorageService()));
    }

    /**
     * Gets the public 'sylius.context.locale.provider_based' shared service.
     *
     * @return \Sylius\Component\Locale\Context\ProviderBasedLocaleContext
     */
    protected function getSylius_Context_Locale_ProviderBasedService()
    {
        return $this->services['sylius.context.locale.provider_based'] = new \Sylius\Component\Locale\Context\ProviderBasedLocaleContext(($this->services['sylius.locale_provider.channel_based'] ?? $this->getSylius_LocaleProvider_ChannelBasedService()));
    }

    /**
     * Gets the public 'sylius.context.locale.request_based' shared service.
     *
     * @return \Sylius\Bundle\LocaleBundle\Context\RequestBasedLocaleContext
     */
    protected function getSylius_Context_Locale_RequestBasedService()
    {
        return $this->services['sylius.context.locale.request_based'] = new \Sylius\Bundle\LocaleBundle\Context\RequestBasedLocaleContext(($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())), ($this->services['sylius.locale_provider.channel_based'] ?? $this->getSylius_LocaleProvider_ChannelBasedService()));
    }

    /**
     * Gets the public 'sylius.context.shopper' shared service.
     *
     * @return \Sylius\Component\Core\Context\ShopperContext
     */
    protected function getSylius_Context_ShopperService($lazyLoad = true)
    {
        if ($lazyLoad) {
            return $this->services['sylius.context.shopper'] = $this->createProxy('ShopperContext_d398ef6', function () {
                return \ShopperContext_d398ef6::staticProxyConstructor(function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getSylius_Context_ShopperService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                });
            });
        }

        include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Context/ShopperContextInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Core/Context/ShopperContext.php';

        return new \Sylius\Component\Core\Context\ShopperContext(($this->services['sylius.context.channel'] ?? $this->getSylius_Context_ChannelService()), ($this->services['sylius.context.currency.channel_aware'] ?? $this->getSylius_Context_Currency_ChannelAwareService()), ($this->services['Sylius\\Component\\Locale\\Context\\LocaleContextInterface'] ?? $this->getLocaleContextInterfaceService()), ($this->services['sylius.context.customer'] ?? $this->getSylius_Context_CustomerService()));
    }

    /**
     * Gets the public 'sylius.factory.order' shared service.
     *
     * @return \Sylius\Component\Resource\Factory\Factory
     */
    protected function getSylius_Factory_OrderService()
    {
        return $this->services['sylius.factory.order'] = new \Sylius\Component\Resource\Factory\Factory('App\\Entity\\Order\\Order');
    }

    /**
     * Gets the public 'sylius.listener.non_channel_request_locale' shared service.
     *
     * @return \Sylius\Bundle\ShopBundle\EventListener\NonChannelLocaleListener
     */
    protected function getSylius_Listener_NonChannelRequestLocaleService()
    {
        return $this->services['sylius.listener.non_channel_request_locale'] = new \Sylius\Bundle\ShopBundle\EventListener\NonChannelLocaleListener(($this->services['sylius.locale_provider.channel_based'] ?? $this->getSylius_LocaleProvider_ChannelBasedService()), ($this->privates['security.firewall.map'] ?? $this->getSecurity_Firewall_MapService()), $this->parameters['hwi_oauth.firewall_names']);
    }

    /**
     * Gets the public 'sylius.listener.request_locale_setter' shared service.
     *
     * @return \Sylius\Bundle\LocaleBundle\Listener\RequestLocaleSetter
     */
    protected function getSylius_Listener_RequestLocaleSetterService()
    {
        return $this->services['sylius.listener.request_locale_setter'] = new \Sylius\Bundle\LocaleBundle\Listener\RequestLocaleSetter(($this->services['Sylius\\Component\\Locale\\Context\\LocaleContextInterface'] ?? $this->getLocaleContextInterfaceService()), ($this->services['sylius.locale_provider.channel_based'] ?? $this->getSylius_LocaleProvider_ChannelBasedService()));
    }

    /**
     * Gets the public 'sylius.listener.session_cart' shared service.
     *
     * @return \Sylius\Bundle\ShopBundle\EventListener\SessionCartSubscriber
     */
    protected function getSylius_Listener_SessionCartService()
    {
        return $this->services['sylius.listener.session_cart'] = new \Sylius\Bundle\ShopBundle\EventListener\SessionCartSubscriber(($this->services['sylius.context.cart'] ?? $this->getSylius_Context_CartService()), ($this->services['sylius.storage.cart_session'] ?? $this->getSylius_Storage_CartSessionService()));
    }

    /**
     * Gets the public 'sylius.locale_provider.channel_based' shared service.
     *
     * @return \Sylius\Component\Core\Provider\ChannelBasedLocaleProvider
     */
    protected function getSylius_LocaleProvider_ChannelBasedService()
    {
        return $this->services['sylius.locale_provider.channel_based'] = new \Sylius\Component\Core\Provider\ChannelBasedLocaleProvider(($this->services['sylius.context.channel'] ?? $this->getSylius_Context_ChannelService()), $this->getEnv('DEFAULT_LOCALE'));
    }

    /**
     * Gets the public 'sylius.repository.channel' shared service.
     *
     * @return \Sylius\Bundle\ChannelBundle\Doctrine\ORM\ChannelRepository
     */
    protected function getSylius_Repository_ChannelService($lazyLoad = true)
    {
        if ($lazyLoad) {
            return $this->services['sylius.repository.channel'] = $this->createProxy('ChannelRepository_04960bc', function () {
                return \ChannelRepository_04960bc::staticProxyConstructor(function (&$wrappedInstance, \ProxyManager\Proxy\LazyLoadingInterface $proxy) {
                    $wrappedInstance = $this->getSylius_Repository_ChannelService(false);

                    $proxy->setProxyInitializer(null);

                    return true;
                });
            });
        }

        include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Component/Channel/Repository/ChannelRepositoryInterface.php';
        include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Doctrine/ORM/ChannelRepository.php';

        $a = ($this->services['doctrine.orm.default_entity_manager'] ?? $this->getDoctrine_Orm_DefaultEntityManagerService());

        return new \Sylius\Bundle\ChannelBundle\Doctrine\ORM\ChannelRepository($a, $a->getClassMetadata('App\\Entity\\Channel\\Channel'));
    }

    /**
     * Gets the public 'sylius.repository.order' shared service.
     *
     * @return \Sylius\Bundle\CoreBundle\Doctrine\ORM\OrderRepository
     */
    protected function getSylius_Repository_OrderService()
    {
        $a = ($this->services['doctrine.orm.default_entity_manager'] ?? $this->getDoctrine_Orm_DefaultEntityManagerService());

        return $this->services['sylius.repository.order'] = new \Sylius\Bundle\CoreBundle\Doctrine\ORM\OrderRepository($a, $a->getClassMetadata('App\\Entity\\Order\\Order'));
    }

    /**
     * Gets the public 'sylius.storage.cart_session' shared service.
     *
     * @return \Sylius\Bundle\CoreBundle\Storage\CartSessionStorage
     */
    protected function getSylius_Storage_CartSessionService()
    {
        return $this->services['sylius.storage.cart_session'] = new \Sylius\Bundle\CoreBundle\Storage\CartSessionStorage(($this->services['session'] ?? $this->getSessionService()), '_sylius.cart', ($this->services['sylius.repository.order'] ?? $this->getSylius_Repository_OrderService()));
    }

    /**
     * Gets the public 'sylius.storage.cookie' shared service.
     *
     * @return \Sylius\Bundle\ResourceBundle\Storage\CookieStorage
     */
    protected function getSylius_Storage_CookieService()
    {
        return $this->services['sylius.storage.cookie'] = new \Sylius\Bundle\ResourceBundle\Storage\CookieStorage();
    }

    /**
     * Gets the public 'sylius.storage.currency' shared service.
     *
     * @return \Sylius\Component\Core\Currency\CurrencyStorage
     */
    protected function getSylius_Storage_CurrencyService()
    {
        return $this->services['sylius.storage.currency'] = new \Sylius\Component\Core\Currency\CurrencyStorage(($this->services['sylius.storage.cookie'] ?? ($this->services['sylius.storage.cookie'] = new \Sylius\Bundle\ResourceBundle\Storage\CookieStorage())));
    }

    /**
     * Gets the public 'sylius.translator.listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\TranslatorListener
     */
    protected function getSylius_Translator_ListenerService()
    {
        return $this->services['sylius.translator.listener'] = new \Symfony\Component\HttpKernel\EventListener\TranslatorListener(($this->services['lexik_translation.translator'] ?? $this->getLexikTranslation_TranslatorService()), ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())));
    }

    /**
     * Gets the public 'sylius_resource.doctrine.mapping_driver_chain' shared service.
     *
     * @return \Sylius\Bundle\ResourceBundle\Doctrine\ResourceMappingDriverChain
     */
    protected function getSyliusResource_Doctrine_MappingDriverChainService()
    {
        $a = new \Doctrine\Persistence\Mapping\Driver\MappingDriverChain();

        $b = new \Doctrine\ORM\Mapping\Driver\SimplifiedXmlDriver([(\dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/Bridge/Doctrine/Resources/mapping') => 'Payum\\Core\\Model', (\dirname(__DIR__, 4).'/vendor/friendsofsymfony/oauth-server-bundle/Resources/config/doctrine') => 'FOS\\OAuthServerBundle\\Entity', (\dirname(__DIR__, 4).'/vendor/brille24/sylius-tierprice-plugin/src/Resources/config/doctrine') => 'Brille24\\SyliusTierPricePlugin\\Entity', (\dirname(__DIR__, 4).'/vendor/lexik/translation-bundle/Resources/config/doctrine') => 'Lexik\\Bundle\\TranslationBundle\\Entity']);
        $b->setGlobalBasename('mapping');
        $c = new \Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver([(\dirname(__DIR__, 4).'/vendor/bitbag/shipping-export-plugin/src/Resources/config/doctrine') => 'BitBag\\SyliusShippingExportPlugin\\Entity', (\dirname(__DIR__, 4).'/vendor/omni/sylius-shipping-plugin/src/Resources/config/doctrine') => 'Omni\\Sylius\\ShippingPlugin\\Entity']);
        $c->setGlobalBasename('mapping');

        $a->addDriver($b, 'Payum\\Core\\Model');
        $a->addDriver($b, 'FOS\\OAuthServerBundle\\Entity');
        $a->addDriver($b, 'Brille24\\SyliusTierPricePlugin\\Entity');
        $a->addDriver($b, 'Lexik\\Bundle\\TranslationBundle\\Entity');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\AnnotationDriver(($this->privates['annotations.cached_reader'] ?? $this->getAnnotations_CachedReaderService()), [0 => (\dirname(__DIR__, 4).'/src/Entity')]), 'App\\Entity');
        $a->addDriver($c, 'BitBag\\SyliusShippingExportPlugin\\Entity');
        $a->addDriver($c, 'Omni\\Sylius\\ShippingPlugin\\Entity');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/OrderBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Order\\Model'], '.orm.xml')), 'Sylius\\Component\\Order\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CurrencyBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Currency\\Model'], '.orm.xml')), 'Sylius\\Component\\Currency\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Locale\\Model'], '.orm.xml')), 'Sylius\\Component\\Locale\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Product\\Model'], '.orm.xml')), 'Sylius\\Component\\Product\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Channel\\Model'], '.orm.xml')), 'Sylius\\Component\\Channel\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AttributeBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Attribute\\Model'], '.orm.xml')), 'Sylius\\Component\\Attribute\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxationBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Taxation\\Model'], '.orm.xml')), 'Sylius\\Component\\Taxation\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShippingBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Shipping\\Model'], '.orm.xml')), 'Sylius\\Component\\Shipping\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Payment\\Model'], '.orm.xml')), 'Sylius\\Component\\Payment\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Promotion\\Model'], '.orm.xml')), 'Sylius\\Component\\Promotion\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AddressingBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Addressing\\Model'], '.orm.xml')), 'Sylius\\Component\\Addressing\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/InventoryBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Inventory\\Model'], '.orm.xml')), 'Sylius\\Component\\Inventory\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxonomyBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Taxonomy\\Model'], '.orm.xml')), 'Sylius\\Component\\Taxonomy\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\User\\Model'], '.orm.xml')), 'Sylius\\Component\\User\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Customer\\Model'], '.orm.xml')), 'Sylius\\Component\\Customer\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ReviewBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Review\\Model'], '.orm.xml')), 'Sylius\\Component\\Review\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Resources/config/doctrine/model') => 'Sylius\\Component\\Core\\Model'], '.orm.xml')), 'Sylius\\Component\\Core\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PayumBundle/Resources/config/doctrine/model') => 'Sylius\\Bundle\\PayumBundle\\Model'], '.orm.xml')), 'Sylius\\Bundle\\PayumBundle\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\XmlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AdminApiBundle/Resources/config/doctrine/model') => 'Sylius\\Bundle\\AdminApiBundle\\Model'], '.orm.xml')), 'Sylius\\Bundle\\AdminApiBundle\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-banner-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\BannerPlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\BannerPlugin\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-search-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\SearchPlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\SearchPlugin\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-core-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\CorePlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\CorePlugin\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\CmsPlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\CmsPlugin\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-seo-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\SeoPlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\SeoPlugin\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-parcel-machine-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\ParcelMachinePlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\ParcelMachinePlugin\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-manifest-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\ManifestPlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\ManifestPlugin\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\SimplifiedXmlDriver([(\dirname(__DIR__, 4).'/vendor/lexik/translation-bundle/Resources/config/model') => 'Lexik\\Bundle\\TranslationBundle\\Model']), 'Lexik\\Bundle\\TranslationBundle\\Model');
        $a->addDriver(new \Doctrine\ORM\Mapping\Driver\YamlDriver(new \Doctrine\Common\Persistence\Mapping\Driver\SymfonyFileLocator([(\dirname(__DIR__, 4).'/vendor/omni/sylius-import-plugin/src/Resources/config/doctrine/model') => 'Omni\\Sylius\\ImportPlugin\\Model'], '.orm.yml')), 'Omni\\Sylius\\ImportPlugin\\Model');

        return $this->services['sylius_resource.doctrine.mapping_driver_chain'] = new \Sylius\Bundle\ResourceBundle\Doctrine\ResourceMappingDriverChain($a, ($this->privates['sylius.resource_registry'] ?? $this->getSylius_ResourceRegistryService()));
    }

    /**
     * Gets the private '.legacy_resolve_controller_name_subscriber' shared service.
     *
     * @return \Symfony\Bundle\FrameworkBundle\EventListener\ResolveControllerNameSubscriber
     */
    protected function get_LegacyResolveControllerNameSubscriberService()
    {
        return $this->privates['.legacy_resolve_controller_name_subscriber'] = new \Symfony\Bundle\FrameworkBundle\EventListener\ResolveControllerNameSubscriber(($this->privates['.legacy_controller_name_converter'] ?? ($this->privates['.legacy_controller_name_converter'] = new \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser(($this->services['kernel'] ?? $this->get('kernel', 1)), false))), false);
    }

    /**
     * Gets the private 'Http\Client\Common\PluginClientFactory' shared service.
     *
     * @return \Http\HttplugBundle\Collector\PluginClientFactory
     */
    protected function getPluginClientFactoryService()
    {
        return $this->privates['Http\\Client\\Common\\PluginClientFactory'] = new \Http\HttplugBundle\Collector\PluginClientFactory(($this->privates['httplug.collector.collector'] ?? ($this->privates['httplug.collector.collector'] = new \Http\HttplugBundle\Collector\Collector())), ($this->privates['httplug.collector.formatter'] ?? $this->getHttplug_Collector_FormatterService()), ($this->privates['debug.stopwatch'] ?? ($this->privates['debug.stopwatch'] = new \Symfony\Component\Stopwatch\Stopwatch(true))));
    }

    /**
     * Gets the private 'Http\HttplugBundle\Collector\PluginClientFactoryListener' shared service.
     *
     * @return \Http\HttplugBundle\Collector\PluginClientFactoryListener
     */
    protected function getPluginClientFactoryListenerService()
    {
        return $this->privates['Http\\HttplugBundle\\Collector\\PluginClientFactoryListener'] = new \Http\HttplugBundle\Collector\PluginClientFactoryListener(($this->privates['Http\\Client\\Common\\PluginClientFactory'] ?? $this->getPluginClientFactoryService()));
    }

    /**
     * Gets the private 'Symfony\WebpackEncoreBundle\EventListener\ResetAssetsEventListener' shared service.
     *
     * @return \Symfony\WebpackEncoreBundle\EventListener\ResetAssetsEventListener
     */
    protected function getResetAssetsEventListenerService()
    {
        return $this->privates['Symfony\\WebpackEncoreBundle\\EventListener\\ResetAssetsEventListener'] = new \Symfony\WebpackEncoreBundle\EventListener\ResetAssetsEventListener(($this->privates['webpack_encore.entrypoint_lookup_collection'] ?? $this->getWebpackEncore_EntrypointLookupCollectionService()), [0 => '_default', 1 => 'bootstrapTheme', 2 => 'bootstrapThemeProductV2', 3 => 'bootstrapThemeProductV3', 4 => 'bootstrapThemeProductV4']);
    }

    /**
     * Gets the private 'annotations.cache_adapter' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\PhpArrayAdapter
     */
    protected function getAnnotations_CacheAdapterService()
    {
        return \Symfony\Component\Cache\Adapter\PhpArrayAdapter::create(($this->targetDir.''.'/annotations.php'), ($this->privates['cache.annotations'] ?? $this->getCache_AnnotationsService()));
    }

    /**
     * Gets the private 'annotations.cached_reader' shared service.
     *
     * @return \Doctrine\Common\Annotations\PsrCachedReader
     */
    protected function getAnnotations_CachedReaderService()
    {
        return $this->privates['annotations.cached_reader'] = new \Doctrine\Common\Annotations\PsrCachedReader(($this->privates['annotations.reader'] ?? $this->getAnnotations_ReaderService()), $this->getAnnotations_CacheAdapterService(), true);
    }

    /**
     * Gets the private 'annotations.reader' shared service.
     *
     * @return \Doctrine\Common\Annotations\AnnotationReader
     */
    protected function getAnnotations_ReaderService()
    {
        $this->privates['annotations.reader'] = $instance = new \Doctrine\Common\Annotations\AnnotationReader();

        $a = new \Doctrine\Common\Annotations\AnnotationRegistry();
        $a->registerUniqueLoader('class_exists');

        $instance->addGlobalIgnoredName('required', $a);
        $instance->addGlobalIgnoredName('template');
        $instance->addGlobalIgnoredName('psalm');

        return $instance;
    }

    /**
     * Gets the private 'cache.annotations' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\AdapterInterface
     */
    protected function getCache_AnnotationsService()
    {
        return $this->privates['cache.annotations'] = \Symfony\Component\Cache\Adapter\AbstractAdapter::createSystemCache('g3RALWxqDL', 0, $this->getParameter('container.build_id'), ($this->targetDir.''.'/pools'), ($this->privates['monolog.logger.cache'] ?? $this->getMonolog_Logger_CacheService()));
    }

    /**
     * Gets the private 'config_cache_factory' shared service.
     *
     * @return \Symfony\Component\Config\ResourceCheckerConfigCacheFactory
     */
    protected function getConfigCacheFactoryService()
    {
        return $this->privates['config_cache_factory'] = new \Symfony\Component\Config\ResourceCheckerConfigCacheFactory(new RewindableGenerator(function () {
            yield 0 => ($this->privates['dependency_injection.config.container_parameters_resource_checker'] ?? ($this->privates['dependency_injection.config.container_parameters_resource_checker'] = new \Symfony\Component\DependencyInjection\Config\ContainerParametersResourceChecker($this)));
            yield 1 => ($this->privates['config.resource.self_checking_resource_checker'] ?? ($this->privates['config.resource.self_checking_resource_checker'] = new \Symfony\Component\Config\Resource\SelfCheckingResourceChecker()));
        }, 2));
    }

    /**
     * Gets the private 'debug.debug_handlers_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener
     */
    protected function getDebug_DebugHandlersListenerService()
    {
        $a = new \Symfony\Bridge\Monolog\Logger('php');
        $a->pushHandler(($this->privates['monolog.handler.console'] ?? $this->getMonolog_Handler_ConsoleService()));
        $a->pushHandler(($this->privates['monolog.handler.deprecation_filter'] ?? $this->getMonolog_Handler_DeprecationFilterService()));
        $a->pushHandler(($this->privates['monolog.handler.main'] ?? $this->getMonolog_Handler_MainService()));

        return $this->privates['debug.debug_handlers_listener'] = new \Symfony\Component\HttpKernel\EventListener\DebugHandlersListener(NULL, $a, NULL, -1, true, ($this->privates['debug.file_link_formatter'] ?? ($this->privates['debug.file_link_formatter'] = new \Symfony\Component\HttpKernel\Debug\FileLinkFormatter(NULL))), true);
    }

    /**
     * Gets the private 'debug.security.access.decision_manager' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager
     */
    protected function getDebug_Security_Access_DecisionManagerService()
    {
        return $this->privates['debug.security.access.decision_manager'] = new \Symfony\Component\Security\Core\Authorization\TraceableAccessDecisionManager(new \Symfony\Component\Security\Core\Authorization\AccessDecisionManager(new RewindableGenerator(function () {
            yield 0 => ($this->privates['debug.security.voter.security.access.authenticated_voter'] ?? $this->load('getDebug_Security_Voter_Security_Access_AuthenticatedVoterService.php'));
            yield 1 => ($this->privates['debug.security.voter.security.access.simple_role_voter'] ?? $this->load('getDebug_Security_Voter_Security_Access_SimpleRoleVoterService.php'));
            yield 2 => ($this->privates['debug.security.voter.security.access.expression_voter'] ?? $this->load('getDebug_Security_Voter_Security_Access_ExpressionVoterService.php'));
        }, 3), 'affirmative', false, true));
    }

    /**
     * Gets the private 'debug.security.firewall' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Debug\TraceableFirewallListener
     */
    protected function getDebug_Security_FirewallService()
    {
        return $this->privates['debug.security.firewall'] = new \Symfony\Bundle\SecurityBundle\Debug\TraceableFirewallListener(($this->privates['security.firewall.map'] ?? $this->getSecurity_Firewall_MapService()), ($this->services['event_dispatcher'] ?? $this->getEventDispatcherService()), ($this->privates['security.logout_url_generator'] ?? $this->getSecurity_LogoutUrlGeneratorService()));
    }

    /**
     * Gets the private 'doctrine.result_cache_pool' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\FilesystemAdapter
     */
    protected function getDoctrine_ResultCachePoolService()
    {
        $this->privates['doctrine.result_cache_pool'] = $instance = new \Symfony\Component\Cache\Adapter\FilesystemAdapter('o2CgH1f5OS', 0, ($this->targetDir.''.'/pools'), ($this->privates['cache.default_marshaller'] ?? ($this->privates['cache.default_marshaller'] = new \Symfony\Component\Cache\Marshaller\DefaultMarshaller(NULL))));

        $instance->setLogger(($this->privates['monolog.logger.cache'] ?? $this->getMonolog_Logger_CacheService()));

        return $instance;
    }

    /**
     * Gets the private 'doctrine.system_cache_pool' shared service.
     *
     * @return \Symfony\Component\Cache\Adapter\AdapterInterface
     */
    protected function getDoctrine_SystemCachePoolService()
    {
        return $this->privates['doctrine.system_cache_pool'] = \Symfony\Component\Cache\Adapter\AbstractAdapter::createSystemCache('xG0InekZhx', 0, $this->getParameter('container.build_id'), ($this->targetDir.''.'/pools'), ($this->privates['monolog.logger.cache'] ?? $this->getMonolog_Logger_CacheService()));
    }

    /**
     * Gets the private 'fos_rest.body_listener' shared service.
     *
     * @return \FOS\RestBundle\EventListener\BodyListener
     */
    protected function getFosRest_BodyListenerService()
    {
        $this->privates['fos_rest.body_listener'] = $instance = new \FOS\RestBundle\EventListener\BodyListener(new \FOS\RestBundle\Decoder\ContainerDecoderProvider(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            'fos_rest.decoder.json' => ['privates', 'fos_rest.decoder.json', 'getFosRest_Decoder_JsonService.php', true],
        ], [
            'fos_rest.decoder.json' => '?',
        ]), ['json' => 'fos_rest.decoder.json']), false);

        $instance->setDefaultFormat(NULL);

        return $instance;
    }

    /**
     * Gets the private 'fos_rest.format_listener' shared service.
     *
     * @return \FOS\RestBundle\EventListener\FormatListener
     */
    protected function getFosRest_FormatListenerService()
    {
        $a = new \FOS\RestBundle\Negotiation\FormatNegotiator(($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())));
        $a->add(new \Symfony\Component\HttpFoundation\RequestMatcher('^/api/.*', NULL, NULL, NULL, []), ['priorities' => [0 => 'json', 1 => 'xml'], 'fallback_format' => 'json', 'prefer_extension' => '2.0', 'methods' => NULL, 'attributes' => [], 'stop' => false]);
        $a->add(new \Symfony\Component\HttpFoundation\RequestMatcher('^/', NULL, NULL, NULL, []), ['stop' => true, 'methods' => NULL, 'attributes' => [], 'prefer_extension' => '2.0', 'fallback_format' => 'html', 'priorities' => []]);

        return $this->privates['fos_rest.format_listener'] = new \FOS\RestBundle\EventListener\FormatListener($a);
    }

    /**
     * Gets the private 'httplug.collector.formatter' shared service.
     *
     * @return \Http\HttplugBundle\Collector\Formatter
     */
    protected function getHttplug_Collector_FormatterService()
    {
        return $this->privates['httplug.collector.formatter'] = new \Http\HttplugBundle\Collector\Formatter(new \Http\Message\Formatter\FullHttpMessageFormatter(0), new \Http\Message\Formatter\CurlCommandFormatter());
    }

    /**
     * Gets the private 'locale_aware_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\LocaleAwareListener
     */
    protected function getLocaleAwareListenerService()
    {
        return $this->privates['locale_aware_listener'] = new \Symfony\Component\HttpKernel\EventListener\LocaleAwareListener(new RewindableGenerator(function () {
            yield 0 => ($this->privates['sylius.theme.translation.theme_aware_translator'] ?? $this->load('getSylius_Theme_Translation_ThemeAwareTranslatorService.php'));
            yield 1 => ($this->services['lexik_translation.translator'] ?? $this->getLexikTranslation_TranslatorService());
        }, 2), ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())));
    }

    /**
     * Gets the private 'locale_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\LocaleListener
     */
    protected function getLocaleListenerService()
    {
        return $this->privates['locale_listener'] = new \Symfony\Component\HttpKernel\EventListener\LocaleListener(($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())), $this->getEnv('DEFAULT_LOCALE'), ($this->services['router'] ?? $this->getRouterService()));
    }

    /**
     * Gets the private 'monolog.handler.console' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Handler\ConsoleHandler
     */
    protected function getMonolog_Handler_ConsoleService()
    {
        $this->privates['monolog.handler.console'] = $instance = new \Symfony\Bridge\Monolog\Handler\ConsoleHandler(NULL, true, [], []);

        $instance->pushProcessor(($this->privates['monolog.processor.psr_log_message'] ?? ($this->privates['monolog.processor.psr_log_message'] = new \Monolog\Processor\PsrLogMessageProcessor())));

        return $instance;
    }

    /**
     * Gets the private 'monolog.handler.deprecation_filter' shared service.
     *
     * @return \Monolog\Handler\FilterHandler
     */
    protected function getMonolog_Handler_DeprecationFilterService()
    {
        $a = new \Monolog\Handler\StreamHandler((\dirname(__DIR__, 3).'/log/prod.deprecations.log'), 100, true, NULL, false);
        $a->pushProcessor(($this->privates['monolog.processor.psr_log_message'] ?? ($this->privates['monolog.processor.psr_log_message'] = new \Monolog\Processor\PsrLogMessageProcessor())));

        return $this->privates['monolog.handler.deprecation_filter'] = new \Monolog\Handler\FilterHandler($a, 100, 200, true);
    }

    /**
     * Gets the private 'monolog.handler.main' shared service.
     *
     * @return \Monolog\Handler\FingersCrossedHandler
     */
    protected function getMonolog_Handler_MainService()
    {
        return $this->privates['monolog.handler.main'] = new \Monolog\Handler\FingersCrossedHandler(($this->privates['monolog.handler.nested'] ?? $this->getMonolog_Handler_NestedService()), new \Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy(400), 0, true, true, NULL);
    }

    /**
     * Gets the private 'monolog.handler.nested' shared service.
     *
     * @return \Monolog\Handler\StreamHandler
     */
    protected function getMonolog_Handler_NestedService()
    {
        $this->privates['monolog.handler.nested'] = $instance = new \Monolog\Handler\StreamHandler((\dirname(__DIR__, 3).'/log/prod.log'), 200, true, NULL, false);

        $instance->pushProcessor(($this->privates['monolog.processor.psr_log_message'] ?? ($this->privates['monolog.processor.psr_log_message'] = new \Monolog\Processor\PsrLogMessageProcessor())));

        return $instance;
    }

    /**
     * Gets the private 'monolog.handler.requests' shared service.
     *
     * @return \Monolog\Handler\FingersCrossedHandler
     */
    protected function getMonolog_Handler_RequestsService()
    {
        return $this->privates['monolog.handler.requests'] = new \Monolog\Handler\FingersCrossedHandler(($this->privates['monolog.handler.nested'] ?? $this->getMonolog_Handler_NestedService()), new \Symfony\Bridge\Monolog\Handler\FingersCrossed\NotFoundActivationStrategy(($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())), [0 => '^/'], new \Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy(500)), 0, true, true, NULL);
    }

    /**
     * Gets the private 'monolog.logger.cache' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonolog_Logger_CacheService()
    {
        $this->privates['monolog.logger.cache'] = $instance = new \Symfony\Bridge\Monolog\Logger('cache');

        $instance->pushHandler(($this->privates['monolog.handler.console'] ?? $this->getMonolog_Handler_ConsoleService()));
        $instance->pushHandler(($this->privates['monolog.handler.main'] ?? $this->getMonolog_Handler_MainService()));

        return $instance;
    }

    /**
     * Gets the private 'monolog.logger.request' shared service.
     *
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getMonolog_Logger_RequestService()
    {
        $this->privates['monolog.logger.request'] = $instance = new \Symfony\Bridge\Monolog\Logger('request');

        $instance->pushHandler(($this->privates['monolog.handler.console'] ?? $this->getMonolog_Handler_ConsoleService()));
        $instance->pushHandler(($this->privates['monolog.handler.requests'] ?? $this->getMonolog_Handler_RequestsService()));

        return $instance;
    }

    /**
     * Gets the private 'parameter_bag' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ParameterBag\ContainerBag
     */
    protected function getParameterBagService()
    {
        return $this->privates['parameter_bag'] = new \Symfony\Component\DependencyInjection\ParameterBag\ContainerBag($this);
    }

    /**
     * Gets the private 'router.request_context' shared service.
     *
     * @return \Symfony\Component\Routing\RequestContext
     */
    protected function getRouter_RequestContextService()
    {
        return $this->privates['router.request_context'] = new \Symfony\Component\Routing\RequestContext('', 'GET', 'localhost', 'http', 80, 443);
    }

    /**
     * Gets the private 'router_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\RouterListener
     */
    protected function getRouterListenerService()
    {
        return $this->privates['router_listener'] = new \Symfony\Component\HttpKernel\EventListener\RouterListener(($this->services['router'] ?? $this->getRouterService()), ($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())), ($this->privates['router.request_context'] ?? $this->getRouter_RequestContextService()), ($this->privates['monolog.logger.request'] ?? $this->getMonolog_Logger_RequestService()), \dirname(__DIR__, 4), true);
    }

    /**
     * Gets the private 'security.authentication.manager' shared service.
     *
     * @return \Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager
     */
    protected function getSecurity_Authentication_ManagerService()
    {
        $this->privates['security.authentication.manager'] = $instance = new \Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager(new RewindableGenerator(function () {
            yield 0 => ($this->privates['security.authentication.provider.dao.admin'] ?? $this->load('getSecurity_Authentication_Provider_Dao_AdminService.php'));
            yield 1 => ($this->privates['security.authentication.provider.rememberme.admin'] ?? $this->load('getSecurity_Authentication_Provider_Rememberme_AdminService.php'));
            yield 2 => ($this->privates['security.authentication.provider.anonymous.admin'] ?? ($this->privates['security.authentication.provider.anonymous.admin'] = new \Symfony\Component\Security\Core\Authentication\Provider\AnonymousAuthenticationProvider($this->getParameter('container.build_hash'))));
            yield 3 => ($this->privates['security.authentication.provider.fos_oauth_server.api'] ?? $this->load('getSecurity_Authentication_Provider_FosOauthServer_ApiService.php'));
            yield 4 => ($this->privates['security.authentication.provider.anonymous.api'] ?? ($this->privates['security.authentication.provider.anonymous.api'] = new \Symfony\Component\Security\Core\Authentication\Provider\AnonymousAuthenticationProvider($this->getParameter('container.build_hash'))));
            yield 5 => ($this->privates['security.authentication.provider.dao.shop'] ?? $this->load('getSecurity_Authentication_Provider_Dao_ShopService.php'));
            yield 6 => ($this->privates['hwi_oauth.authentication.provider.oauth.shop'] ?? $this->load('getHwiOauth_Authentication_Provider_Oauth_ShopService.php'));
            yield 7 => ($this->privates['security.authentication.provider.rememberme.shop'] ?? $this->load('getSecurity_Authentication_Provider_Rememberme_ShopService.php'));
            yield 8 => ($this->privates['security.authentication.provider.anonymous.shop'] ?? ($this->privates['security.authentication.provider.anonymous.shop'] = new \Symfony\Component\Security\Core\Authentication\Provider\AnonymousAuthenticationProvider($this->getParameter('container.build_hash'))));
        }, 9), true);

        $instance->setEventDispatcher(($this->services['event_dispatcher'] ?? $this->getEventDispatcherService()));

        return $instance;
    }

    /**
     * Gets the private 'security.firewall.map' shared service.
     *
     * @return \Symfony\Bundle\SecurityBundle\Security\FirewallMap
     */
    protected function getSecurity_Firewall_MapService()
    {
        return $this->privates['security.firewall.map'] = new \Symfony\Bundle\SecurityBundle\Security\FirewallMap(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            'security.firewall.map.context.admin' => ['privates', 'security.firewall.map.context.admin', 'getSecurity_Firewall_Map_Context_AdminService.php', true],
            'security.firewall.map.context.api' => ['privates', 'security.firewall.map.context.api', 'getSecurity_Firewall_Map_Context_ApiService.php', true],
            'security.firewall.map.context.dev' => ['privates', 'security.firewall.map.context.dev', 'getSecurity_Firewall_Map_Context_DevService.php', true],
            'security.firewall.map.context.oauth_token' => ['privates', 'security.firewall.map.context.oauth_token', 'getSecurity_Firewall_Map_Context_OauthTokenService.php', true],
            'security.firewall.map.context.shop' => ['privates', 'security.firewall.map.context.shop', 'getSecurity_Firewall_Map_Context_ShopService.php', true],
        ], [
            'security.firewall.map.context.admin' => '?',
            'security.firewall.map.context.api' => '?',
            'security.firewall.map.context.dev' => '?',
            'security.firewall.map.context.oauth_token' => '?',
            'security.firewall.map.context.shop' => '?',
        ]), new RewindableGenerator(function () {
            yield 'security.firewall.map.context.admin' => ($this->privates['.security.request_matcher.B3ldH_a'] ?? ($this->privates['.security.request_matcher.B3ldH_a'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/admin')));
            yield 'security.firewall.map.context.oauth_token' => ($this->privates['.security.request_matcher.2IPYWkw'] ?? ($this->privates['.security.request_matcher.2IPYWkw'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/api/oauth/v2/token')));
            yield 'security.firewall.map.context.api' => ($this->privates['.security.request_matcher.jsUtyW7'] ?? ($this->privates['.security.request_matcher.jsUtyW7'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/api/.*')));
            yield 'security.firewall.map.context.shop' => ($this->privates['.security.request_matcher.HMGYYxQ'] ?? ($this->privates['.security.request_matcher.HMGYYxQ'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/(?!admin|api/.*|api$|media/.*)[^/]++')));
            yield 'security.firewall.map.context.dev' => ($this->privates['.security.request_matcher.Iy.T22O'] ?? ($this->privates['.security.request_matcher.Iy.T22O'] = new \Symfony\Component\HttpFoundation\RequestMatcher('^/(_(profiler|wdt)|css|images|js)/')));
        }, 5));
    }

    /**
     * Gets the private 'security.logout_url_generator' shared service.
     *
     * @return \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator
     */
    protected function getSecurity_LogoutUrlGeneratorService()
    {
        $this->privates['security.logout_url_generator'] = $instance = new \Symfony\Component\Security\Http\Logout\LogoutUrlGenerator(($this->services['request_stack'] ?? ($this->services['request_stack'] = new \Symfony\Component\HttpFoundation\RequestStack())), ($this->services['router'] ?? $this->getRouterService()), ($this->services['security.token_storage'] ?? $this->getSecurity_TokenStorageService()));

        $instance->registerListener('admin', 'sylius_admin_logout', 'logout', '_csrf_token', NULL, 'admin');
        $instance->registerListener('shop', 'sylius_shop_logout', 'logout', '_csrf_token', NULL, 'shop');

        return $instance;
    }

    /**
     * Gets the private 'session_listener' shared service.
     *
     * @return \Symfony\Component\HttpKernel\EventListener\SessionListener
     */
    protected function getSessionListenerService()
    {
        return $this->privates['session_listener'] = new \Symfony\Component\HttpKernel\EventListener\SessionListener(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            'initialized_session' => ['services', 'session', NULL, false],
            'session' => ['services', 'session', 'getSessionService', false],
        ], [
            'initialized_session' => '?',
            'session' => '?',
        ]));
    }

    /**
     * Gets the private 'stof_doctrine_extensions.event_listener.logger' shared service.
     *
     * @return \Stof\DoctrineExtensionsBundle\EventListener\LoggerListener
     */
    protected function getStofDoctrineExtensions_EventListener_LoggerService()
    {
        return $this->privates['stof_doctrine_extensions.event_listener.logger'] = new \Stof\DoctrineExtensionsBundle\EventListener\LoggerListener(($this->privates['stof_doctrine_extensions.listener.loggable'] ?? $this->getStofDoctrineExtensions_Listener_LoggableService()), ($this->services['security.token_storage'] ?? $this->getSecurity_TokenStorageService()), ($this->services['security.authorization_checker'] ?? $this->getSecurity_AuthorizationCheckerService()));
    }

    /**
     * Gets the private 'stof_doctrine_extensions.listener.loggable' shared service.
     *
     * @return \Gedmo\Loggable\LoggableListener
     */
    protected function getStofDoctrineExtensions_Listener_LoggableService()
    {
        $this->privates['stof_doctrine_extensions.listener.loggable'] = $instance = new \Gedmo\Loggable\LoggableListener();

        $instance->setAnnotationReader(($this->privates['annotations.cached_reader'] ?? $this->getAnnotations_CachedReaderService()));

        return $instance;
    }

    /**
     * Gets the private 'sylius.resolver.checkout' shared service.
     *
     * @return \Sylius\Bundle\CoreBundle\Checkout\CheckoutResolver
     */
    protected function getSylius_Resolver_CheckoutService()
    {
        return $this->privates['sylius.resolver.checkout'] = new \Sylius\Bundle\CoreBundle\Checkout\CheckoutResolver(($this->services['sylius.context.cart'] ?? $this->getSylius_Context_CartService()), ($this->privates['sylius.router.checkout_state'] ?? $this->getSylius_Router_CheckoutStateService()), new \Symfony\Component\HttpFoundation\RequestMatcher('/checkout/.+'), ($this->services['sm.factory'] ?? $this->getSm_FactoryService()));
    }

    /**
     * Gets the private 'sylius.resource_registry' shared service.
     *
     * @return \Sylius\Component\Resource\Metadata\Registry
     */
    protected function getSylius_ResourceRegistryService()
    {
        $this->privates['sylius.resource_registry'] = $instance = new \Sylius\Component\Resource\Metadata\Registry();

        $instance->addFromAliasAndConfiguration('sylius.order', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\Order', 'controller' => 'App\\Controller\\OrderController', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\OrderRepository', 'interface' => 'Sylius\\Component\\Order\\Model\\OrderInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\OrderBundle\\Form\\Type\\OrderType']]);
        $instance->addFromAliasAndConfiguration('sylius.order_item', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\OrderItem', 'controller' => 'App\\Controller\\OrderItemController', 'interface' => 'Sylius\\Component\\Order\\Model\\OrderItemInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\OrderBundle\\Form\\Type\\OrderItemType']]);
        $instance->addFromAliasAndConfiguration('sylius.order_item_unit', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\OrderItemUnit', 'interface' => 'Sylius\\Component\\Order\\Model\\OrderItemUnitInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Order\\Factory\\OrderItemUnitFactory']]);
        $instance->addFromAliasAndConfiguration('sylius.order_sequence', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\OrderSequence', 'interface' => 'Sylius\\Component\\Order\\Model\\OrderSequenceInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.adjustment', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\Adjustment', 'interface' => 'Sylius\\Component\\Order\\Model\\AdjustmentInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.currency', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Currency\\Currency', 'interface' => 'Sylius\\Component\\Currency\\Model\\CurrencyInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\CurrencyBundle\\Form\\Type\\CurrencyType']]);
        $instance->addFromAliasAndConfiguration('sylius.exchange_rate', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Currency\\ExchangeRate', 'interface' => 'Sylius\\Component\\Currency\\Model\\ExchangeRateInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\CurrencyBundle\\Form\\Type\\ExchangeRateType']]);
        $instance->addFromAliasAndConfiguration('sylius.locale', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Locale\\Locale', 'interface' => 'Sylius\\Component\\Locale\\Model\\LocaleInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\LocaleBundle\\Form\\Type\\LocaleType']]);
        $instance->addFromAliasAndConfiguration('sylius.product', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\Product', 'repository' => 'App\\Repository\\Product\\ProductRepository', 'controller' => 'Omni\\Sylius\\FilterPlugin\\Controller\\ProductController', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Product\\ProductTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.product_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.product_variant', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductVariant', 'controller' => 'Sylius\\Bundle\\CoreBundle\\Controller\\ProductVariantController', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductVariantRepository', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductVariantInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductVariantType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Product\\ProductVariantTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductVariantTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductVariantTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.product_variant_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductVariantTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductVariantTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductVariantTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.product_option', ['driver' => 'doctrine/orm', 'classes' => ['repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductOptionRepository', 'model' => 'App\\Entity\\Product\\ProductOption', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Product\\ProductOptionTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.product_option_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductOptionTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.product_association_type', ['driver' => 'doctrine/orm', 'classes' => ['repository' => 'Sylius\\Bundle\\ProductBundle\\Doctrine\\ORM\\ProductAssociationTypeRepository', 'model' => 'App\\Entity\\Product\\ProductAssociationType', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationTypeInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationTypeType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Product\\ProductAssociationTypeTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationTypeTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationTypeTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.product_association_type_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductAssociationTypeTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationTypeTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationTypeTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.product_option_value', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductOptionValue', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionValueInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionValueType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Product\\ProductOptionValueTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionValueTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionValueTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.product_option_value_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductOptionValueTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionValueTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionValueTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.product_association', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductAssociation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationType']]);
        $instance->addFromAliasAndConfiguration('sylius.channel', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Channel\\Channel', 'interface' => 'Sylius\\Component\\Channel\\Model\\ChannelInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ChannelBundle\\Form\\Type\\ChannelType']]);
        $instance->addFromAliasAndConfiguration('sylius.product_attribute', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductAttribute', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeInterface', 'controller' => 'Sylius\\Bundle\\ProductBundle\\Controller\\ProductAttributeController', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeType', 'repository' => 'Omni\\Sylius\\FilterPlugin\\Doctrine\\ORM\\ProductAttributeRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Product\\ProductAttributeTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeTranslationInterface', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeTranslationType', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]]);
        $instance->addFromAliasAndConfiguration('sylius.product_attribute_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductAttributeTranslation', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeTranslationInterface', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeTranslationType', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.product_attribute_value', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductAttributeValue', 'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeValueInterface', 'repository' => 'Sylius\\Bundle\\ProductBundle\\Doctrine\\ORM\\ProductAttributeValueRepository', 'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeValueType', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.tax_rate', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Taxation\\TaxRate', 'interface' => 'Sylius\\Component\\Taxation\\Model\\TaxRateInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\TaxationBundle\\Form\\Type\\TaxRateType']]);
        $instance->addFromAliasAndConfiguration('sylius.tax_category', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Taxation\\TaxCategory', 'interface' => 'Sylius\\Component\\Taxation\\Model\\TaxCategoryInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\TaxationBundle\\Form\\Type\\TaxCategoryType']]);
        $instance->addFromAliasAndConfiguration('sylius.shipment', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Shipping\\Shipment', 'repository' => 'App\\Repository\\Shipping\\ShipmentRepository', 'interface' => 'Sylius\\Component\\Shipping\\Model\\ShipmentInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShipmentType']]);
        $instance->addFromAliasAndConfiguration('sylius.shipment_unit', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\OrderItemUnit', 'interface' => 'Sylius\\Component\\Shipping\\Model\\ShipmentUnitInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.shipping_method', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Shipping\\ShippingMethod', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ShippingMethodRepository', 'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingMethodInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingMethodType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Shipping\\ShippingMethodTranslation', 'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingMethodTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingMethodTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.shipping_method_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Shipping\\ShippingMethodTranslation', 'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingMethodTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingMethodTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.shipping_category', ['driver' => 'doctrine/orm', 'classes' => ['repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ShippingCategoryRepository', 'model' => 'App\\Entity\\Shipping\\ShippingCategory', 'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingCategoryInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingCategoryType']]);
        $instance->addFromAliasAndConfiguration('sylius.payment', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Payment\\Payment', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PaymentRepository', 'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentType']]);
        $instance->addFromAliasAndConfiguration('sylius.payment_method', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Payment\\PaymentMethod', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PaymentMethodRepository', 'controller' => 'Sylius\\Bundle\\CoreBundle\\Controller\\PaymentMethodController', 'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentMethodInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentMethodType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Payment\\PaymentMethodTranslation', 'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentMethodTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentMethodTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.payment_method_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Payment\\PaymentMethodTranslation', 'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentMethodTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentMethodTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.promotion_subject', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\Order']]);
        $instance->addFromAliasAndConfiguration('sylius.promotion', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Promotion\\Promotion', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PromotionRepository', 'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionType']]);
        $instance->addFromAliasAndConfiguration('sylius.promotion_coupon', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Promotion\\PromotionCoupon', 'repository' => 'Sylius\\Bundle\\PromotionBundle\\Doctrine\\ORM\\PromotionCouponRepository', 'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionCouponInterface', 'controller' => 'Sylius\\Bundle\\PromotionBundle\\Controller\\PromotionCouponController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionCouponType']]);
        $instance->addFromAliasAndConfiguration('sylius.promotion_rule', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Promotion\\PromotionRule', 'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionRuleInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionRuleType']]);
        $instance->addFromAliasAndConfiguration('sylius.promotion_action', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Promotion\\PromotionAction', 'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionActionInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionActionType']]);
        $instance->addFromAliasAndConfiguration('sylius.address', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Addressing\\Address', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\AddressRepository', 'interface' => 'Sylius\\Component\\Addressing\\Model\\AddressInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\AddressType']]);
        $instance->addFromAliasAndConfiguration('sylius.country', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Addressing\\Country', 'interface' => 'Sylius\\Component\\Addressing\\Model\\CountryInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\CountryType']]);
        $instance->addFromAliasAndConfiguration('sylius.province', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Addressing\\Province', 'interface' => 'Sylius\\Component\\Addressing\\Model\\ProvinceInterface', 'controller' => 'Sylius\\Bundle\\AddressingBundle\\Controller\\ProvinceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\ProvinceType']]);
        $instance->addFromAliasAndConfiguration('sylius.zone', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Addressing\\Zone', 'interface' => 'Sylius\\Component\\Addressing\\Model\\ZoneInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\ZoneType']]);
        $instance->addFromAliasAndConfiguration('sylius.zone_member', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Addressing\\ZoneMember', 'interface' => 'Sylius\\Component\\Addressing\\Model\\ZoneMemberInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\ZoneMemberType']]);
        $instance->addFromAliasAndConfiguration('sylius.address_log_entry', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Sylius\\Component\\Addressing\\Model\\AddressLogEntry', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'repository' => 'Sylius\\Bundle\\ResourceBundle\\Doctrine\\ORM\\ResourceLogEntryRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.inventory_unit', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Order\\OrderItemUnit', 'interface' => 'Sylius\\Component\\Inventory\\Model\\InventoryUnitInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.taxon', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Taxonomy\\Taxon', 'repository' => 'App\\Repository\\Taxon\\TaxonRepository', 'interface' => 'Sylius\\Component\\Taxonomy\\Model\\TaxonInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory', 'form' => 'Sylius\\Bundle\\TaxonomyBundle\\Form\\Type\\TaxonType'], 'translation' => ['classes' => ['model' => 'App\\Entity\\Taxonomy\\TaxonTranslation', 'interface' => 'Sylius\\Component\\Taxonomy\\Model\\TaxonTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\TaxonomyBundle\\Form\\Type\\TaxonTranslationType']]]);
        $instance->addFromAliasAndConfiguration('sylius.taxon_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Taxonomy\\TaxonTranslation', 'interface' => 'Sylius\\Component\\Taxonomy\\Model\\TaxonTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\TaxonomyBundle\\Form\\Type\\TaxonTranslationType']]);
        $instance->addFromAliasAndConfiguration('sylius.admin_user', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\User\\AdminUser', 'interface' => 'Sylius\\Component\\Core\\Model\\AdminUserInterface', 'repository' => 'Sylius\\Bundle\\UserBundle\\Doctrine\\ORM\\UserRepository', 'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\User\\AdminUserType', 'controller' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'templates' => 'SyliusUserBundle:User', 'encoder' => NULL, 'resetting' => ['token' => ['ttl' => 'P1D', 'length' => 16, 'field_name' => 'passwordResetToken'], 'pin' => ['length' => 4, 'field_name' => 'passwordResetToken']], 'verification' => ['token' => ['length' => 16, 'field_name' => 'emailVerificationToken']]]);
        $instance->addFromAliasAndConfiguration('sylius.shop_user', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\User\\ShopUser', 'interface' => 'Sylius\\Component\\Core\\Model\\ShopUserInterface', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\UserRepository', 'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\User\\ShopUserType', 'controller' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'templates' => 'SyliusUserBundle:User', 'encoder' => NULL, 'resetting' => ['token' => ['ttl' => 'P1D', 'length' => 16, 'field_name' => 'passwordResetToken'], 'pin' => ['length' => 4, 'field_name' => 'passwordResetToken']], 'verification' => ['token' => ['length' => 16, 'field_name' => 'emailVerificationToken']]]);
        $instance->addFromAliasAndConfiguration('sylius.oauth_user', ['driver' => 'doctrine/orm', 'encoder' => false, 'classes' => ['model' => 'App\\Entity\\User\\UserOAuth', 'interface' => 'Sylius\\Component\\User\\Model\\UserOAuthInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'templates' => 'SyliusUserBundle:User', 'resetting' => ['token' => ['ttl' => 'P1D', 'length' => 16, 'field_name' => 'passwordResetToken'], 'pin' => ['length' => 4, 'field_name' => 'passwordResetToken']], 'verification' => ['token' => ['length' => 16, 'field_name' => 'emailVerificationToken']]]);
        $instance->addFromAliasAndConfiguration('sylius.customer', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Customer\\Customer', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\CustomerRepository', 'interface' => 'Sylius\\Component\\Customer\\Model\\CustomerInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\CustomerBundle\\Form\\Type\\CustomerType']]);
        $instance->addFromAliasAndConfiguration('sylius.customer_group', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Customer\\CustomerGroup', 'interface' => 'Sylius\\Component\\Customer\\Model\\CustomerGroupInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\CustomerBundle\\Form\\Type\\CustomerGroupType']]);
        $instance->addFromAliasAndConfiguration('sylius.product_review', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductReview', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductReviewRepository', 'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\Product\\ProductReviewType', 'interface' => 'Sylius\\Component\\Review\\Model\\ReviewInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.product_reviewer', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Customer\\Customer', 'interface' => 'Sylius\\Component\\Review\\Model\\ReviewerInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.product_taxon', ['driver' => 'doctrine/orm', 'classes' => ['repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductTaxonRepository', 'model' => 'App\\Entity\\Product\\ProductTaxon', 'interface' => 'Sylius\\Component\\Core\\Model\\ProductTaxonInterface', 'controller' => 'Sylius\\Bundle\\CoreBundle\\Controller\\ProductTaxonController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.product_image', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Product\\ProductImage', 'interface' => 'Sylius\\Component\\Core\\Model\\ProductImageInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.taxon_image', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Taxonomy\\TaxonImage', 'interface' => 'Sylius\\Component\\Core\\Model\\TaxonImageInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.channel_pricing', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Channel\\ChannelPricing', 'interface' => 'Sylius\\Component\\Core\\Model\\ChannelPricingInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\Product\\ChannelPricingType']]);
        $instance->addFromAliasAndConfiguration('sylius.avatar_image', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Sylius\\Component\\Core\\Model\\AvatarImage', 'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\AvatarImageRepository']]);
        $instance->addFromAliasAndConfiguration('bitbag.shipping_export', ['classes' => ['model' => 'App\\Entity\\Shipping\\ShippingExport', 'interface' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingExportInterface', 'repository' => 'BitBag\\SyliusShippingExportPlugin\\Repository\\ShippingExportRepository', 'form' => 'BitBag\\SyliusShippingExportPlugin\\Form\\Type\\ShippingGatewayType', 'controller' => 'BitBag\\SyliusShippingExportPlugin\\Controller\\ShippingExportController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'driver' => 'doctrine/orm']);
        $instance->addFromAliasAndConfiguration('bitbag.shipping_gateway', ['classes' => ['model' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingGateway', 'interface' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingGatewayInterface', 'repository' => 'BitBag\\SyliusShippingExportPlugin\\Repository\\ShippingGatewayRepository', 'form' => 'BitBag\\SyliusShippingExportPlugin\\Form\\Type\\ShippingGatewayType', 'controller' => 'BitBag\\SyliusShippingExportPlugin\\Controller\\ShippingGatewayController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'driver' => 'doctrine/orm']);
        $instance->addFromAliasAndConfiguration('nfq.consent', ['classes' => ['model' => 'App\\Entity\\Consent\\Consent', 'repository' => 'App\\Repository\\Consent\\ConsentRepository', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\ResourceBundle\\Form\\Type\\DefaultResourceType'], 'driver' => 'doctrine/orm']);
        $instance->addFromAliasAndConfiguration('brille24.tierprice', ['classes' => ['model' => 'Brille24\\SyliusTierPricePlugin\\Entity\\TierPrice', 'form' => 'Brille24\\SyliusTierPricePlugin\\Form\\TierPriceType', 'repository' => 'Brille24\\SyliusTierPricePlugin\\Repository\\TierPriceRepository', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'driver' => 'doctrine/orm']);
        $instance->addFromAliasAndConfiguration('app.order_report', ['classes' => ['model' => 'App\\Entity\\Order\\OrderReport', 'form' => 'App\\Form\\Type\\OrderReportType', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'driver' => 'doctrine/orm']);
        $instance->addFromAliasAndConfiguration('sylius.payment_security_token', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Payment\\PaymentSecurityToken', 'interface' => 'Sylius\\Bundle\\PayumBundle\\Model\\PaymentSecurityTokenInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.gateway_config', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Payment\\GatewayConfig', 'interface' => 'Sylius\\Bundle\\PayumBundle\\Model\\GatewayConfigInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\PayumBundle\\Form\\Type\\GatewayConfigType']]);
        $instance->addFromAliasAndConfiguration('sylius.api_user', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\User\\AdminUser', 'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\UserInterface']]);
        $instance->addFromAliasAndConfiguration('sylius.api_client', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\AdminApi\\Client', 'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\ClientInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Sylius\\Bundle\\AdminApiBundle\\Form\\Type\\ClientType']]);
        $instance->addFromAliasAndConfiguration('sylius.api_access_token', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\AdminApi\\AccessToken', 'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\AccessTokenInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.api_refresh_token', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\AdminApi\\RefreshToken', 'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\RefreshTokenInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('sylius.api_auth_code', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\AdminApi\\AuthCode', 'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\AuthCodeInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.banner', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\Banner', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'repository' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerType']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.banner_zone', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZone', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'repository' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerZoneRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerZoneType'], 'translation' => ['classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslation', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslationInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]]);
        $instance->addFromAliasAndConfiguration('omni_sylius.banner_zone_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslation', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslationInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.banner_position', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPosition', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'repository' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerPositionRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerPositionType'], 'translation' => ['classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslation', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslationInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]]);
        $instance->addFromAliasAndConfiguration('omni_sylius.banner_position_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslation', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslationInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.banner_image', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImage', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerImageType'], 'translation' => ['classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslation', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslationInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]]);
        $instance->addFromAliasAndConfiguration('omni_sylius.banner_image_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslation', 'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslationInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.search_index', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\SearchPlugin\\Model\\SearchIndex', 'interface' => 'Omni\\Sylius\\SearchPlugin\\Model\\SearchIndexInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.channel_watermark', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelWatermark', 'interface' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelWatermarkInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\CorePlugin\\Form\\Type\\ChannelWatermarkType']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.channel_logo', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelLogo', 'interface' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelLogoInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\CorePlugin\\Form\\Type\\ChannelLogoType']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.node_image', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImage', 'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory'], 'translation' => ['classes' => ['model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslation', 'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeImageTranslationType']]]);
        $instance->addFromAliasAndConfiguration('omni_sylius.node_image_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslation', 'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeImageTranslationType']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.node', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\CmsPlugin\\Model\\Node', 'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeInterface', 'controller' => 'Omni\\Sylius\\CmsPlugin\\Controller\\NodeController', 'repository' => 'Omni\\Sylius\\CmsPlugin\\Doctrine\\ORM\\NodeRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeType'], 'translation' => ['classes' => ['model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslation', 'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeTranslationType']]]);
        $instance->addFromAliasAndConfiguration('omni_sylius.node_translation', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslation', 'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslationInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeTranslationType']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.seo_metadata', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\SeoPlugin\\Model\\SeoMetadata', 'interface' => 'Omni\\Sylius\\SeoPlugin\\Model\\SeoMetadataInterface', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\SeoPlugin\\Form\\Type\\SeoMetadataType']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.shipper_config', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Shipping\\ShipperConfig', 'repository' => 'App\\Repository\\Shipping\\ShipperConfigRepository', 'interface' => 'Omni\\Sylius\\ShippingPlugin\\Model\\ShipperConfigInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni.parcel_machine', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\ParcelMachinePlugin\\Model\\ParcelMachine', 'interface' => 'Omni\\Sylius\\ParcelMachinePlugin\\Model\\ParcelMachineInterface', 'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController', 'repository' => 'Omni\\Sylius\\ParcelMachinePlugin\\Doctrine\\ORM\\ParcelMachineRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.manifest', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'App\\Entity\\Shipping\\Manifest', 'interface' => 'Omni\\Sylius\\ManifestPlugin\\Model\\ManifestInterface', 'controller' => 'Omni\\Sylius\\ManifestPlugin\\Controller\\ManifestController', 'repository' => 'Omni\\Sylius\\ManifestPlugin\\Doctrine\\ORM\\ManifestRepository', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.import_job', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\ImportPlugin\\Model\\ImportJob', 'interface' => 'Omni\\Sylius\\ImportPlugin\\Model\\ImportJobInterface', 'controller' => 'Omni\\Sylius\\ImportPlugin\\Controller\\ImportJobController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\ImportPlugin\\Form\\Type\\ImportJobType']]);
        $instance->addFromAliasAndConfiguration('omni_sylius.export_job', ['driver' => 'doctrine/orm', 'classes' => ['model' => 'Omni\\Sylius\\ImportPlugin\\Model\\ExportJob', 'interface' => 'Omni\\Sylius\\ImportPlugin\\Model\\ExportJobInterface', 'controller' => 'Omni\\Sylius\\ImportPlugin\\Controller\\ExportJobController', 'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory', 'form' => 'Omni\\Sylius\\ImportPlugin\\Form\\Type\\ExportJobType']]);

        return $instance;
    }

    /**
     * Gets the private 'sylius.router.checkout_state' shared service.
     *
     * @return \Sylius\Bundle\CoreBundle\Checkout\CheckoutStateUrlGenerator
     */
    protected function getSylius_Router_CheckoutStateService()
    {
        return $this->privates['sylius.router.checkout_state'] = new \Sylius\Bundle\CoreBundle\Checkout\CheckoutStateUrlGenerator(($this->services['router'] ?? $this->getRouterService()), ['empty_order' => ['route' => 'sylius_shop_cart_summary'], 'cart' => ['route' => 'sylius_shop_checkout_address'], 'addressed' => ['route' => 'sylius_shop_checkout_select_shipping'], 'shipping_selected' => ['route' => 'sylius_shop_checkout_select_payment'], 'shipping_skipped' => ['route' => 'sylius_shop_checkout_select_payment'], 'payment_selected' => ['route' => 'sylius_shop_checkout_complete'], 'payment_skipped' => ['route' => 'sylius_shop_checkout_complete']]);
    }

    /**
     * Gets the private 'translator.formatter.default' shared service.
     *
     * @return \Symfony\Component\Translation\Formatter\MessageFormatter
     */
    protected function getTranslator_Formatter_DefaultService()
    {
        return $this->privates['translator.formatter.default'] = new \Symfony\Component\Translation\Formatter\MessageFormatter(new \Symfony\Component\Translation\IdentityTranslator());
    }

    /**
     * Gets the private 'webpack_encore.entrypoint_lookup_collection' shared service.
     *
     * @return \Symfony\WebpackEncoreBundle\Asset\EntrypointLookupCollection
     */
    protected function getWebpackEncore_EntrypointLookupCollectionService()
    {
        return $this->privates['webpack_encore.entrypoint_lookup_collection'] = new \Symfony\WebpackEncoreBundle\Asset\EntrypointLookupCollection(new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
            '_default' => ['privates', 'webpack_encore.entrypoint_lookup[_default]', 'getWebpackEncore_EntrypointLookupDefaultService.php', true],
            'bootstrapTheme' => ['privates', 'webpack_encore.entrypoint_lookup[bootstrapTheme]', 'getWebpackEncore_EntrypointLookupbootstrapThemeService.php', true],
            'bootstrapThemeProductV2' => ['privates', 'webpack_encore.entrypoint_lookup[bootstrapThemeProductV2]', 'getWebpackEncore_EntrypointLookupbootstrapThemeProductV2Service.php', true],
            'bootstrapThemeProductV3' => ['privates', 'webpack_encore.entrypoint_lookup[bootstrapThemeProductV3]', 'getWebpackEncore_EntrypointLookupbootstrapThemeProductV3Service.php', true],
            'bootstrapThemeProductV4' => ['privates', 'webpack_encore.entrypoint_lookup[bootstrapThemeProductV4]', 'getWebpackEncore_EntrypointLookupbootstrapThemeProductV4Service.php', true],
        ], [
            '_default' => '?',
            'bootstrapTheme' => '?',
            'bootstrapThemeProductV2' => '?',
            'bootstrapThemeProductV3' => '?',
            'bootstrapThemeProductV4' => '?',
        ]), '_default');
    }

    /**
     * Gets the public 'sonata.core.flashmessage.manager' alias.
     *
     * @return object The "sonata.twig.flashmessage.manager" service.
     */
    protected function getSonata_Core_Flashmessage_ManagerService()
    {
        @trigger_error('The "sonata.core.flashmessage.manager" service is deprecated since sonata-project/core-bundle 3.19 and will be removed in 4.0. Use "sonata.twig.flashmessage.manager" instead.', E_USER_DEPRECATED);

        return $this->get('sonata.twig.flashmessage.manager');
    }

    /**
     * Gets the public 'sonata.core.flashmessage.twig.extension' alias.
     *
     * @return object The "sonata.twig.flashmessage.twig.extension" service.
     */
    protected function getSonata_Core_Flashmessage_Twig_ExtensionService()
    {
        @trigger_error('The "sonata.core.flashmessage.twig.extension" service is deprecated since sonata-project/core-bundle 3.19 and will be removed in 4.0. Use "sonata.twig.flashmessage.twig.extension" instead.', E_USER_DEPRECATED);

        return $this->get('sonata.twig.flashmessage.twig.extension');
    }

    /**
     * @return array|bool|float|int|string|\UnitEnum|null
     */
    public function getParameter($name)
    {
        $name = (string) $name;
        if (isset($this->buildParameters[$name])) {
            return $this->buildParameters[$name];
        }

        if (!(isset($this->parameters[$name]) || isset($this->loadedDynamicParameters[$name]) || array_key_exists($name, $this->parameters))) {
            throw new InvalidArgumentException(sprintf('The parameter "%s" must be defined.', $name));
        }
        if (isset($this->loadedDynamicParameters[$name])) {
            return $this->loadedDynamicParameters[$name] ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
        }

        return $this->parameters[$name];
    }

    public function hasParameter($name): bool
    {
        $name = (string) $name;
        if (isset($this->buildParameters[$name])) {
            return true;
        }

        return isset($this->parameters[$name]) || isset($this->loadedDynamicParameters[$name]) || array_key_exists($name, $this->parameters);
    }

    public function setParameter($name, $value): void
    {
        throw new LogicException('Impossible to call set() on a frozen ParameterBag.');
    }

    public function getParameterBag(): ParameterBagInterface
    {
        if (null === $this->parameterBag) {
            $parameters = $this->parameters;
            foreach ($this->loadedDynamicParameters as $name => $loaded) {
                $parameters[$name] = $loaded ? $this->dynamicParameters[$name] : $this->getDynamicParameter($name);
            }
            foreach ($this->buildParameters as $name => $value) {
                $parameters[$name] = $value;
            }
            $this->parameterBag = new FrozenParameterBag($parameters);
        }

        return $this->parameterBag;
    }

    private $loadedDynamicParameters = [
        'kernel.cache_dir' => false,
        'locale' => false,
        'kernel.secret' => false,
        'kernel.default_locale' => false,
        'session.save_path' => false,
        'validator.mapping.cache.file' => false,
        'debug.container.dump' => false,
        'doctrine.orm.proxy_dir' => false,
        'sylius_money.locale' => false,
        'sylius_locale.locale' => false,
        'sylius.mailer.sender_name' => false,
        'sylius.mailer.sender_address' => false,
        'sylius.grids_definitions' => false,
        'stof_doctrine_extensions.default_locale' => false,
        'omni_sylius_dpd.username' => false,
        'omni_sylius_dpd.password' => false,
        'omni_sylius_dpd.host' => false,
        'omni_sylius_paysera.options' => false,
        'lexik_translation.translator.options' => false,
    ];
    private $dynamicParameters = [];

    private function getDynamicParameter(string $name)
    {
        switch ($name) {
            case 'kernel.cache_dir': $value = $this->targetDir.''; break;
            case 'locale': $value = $this->getEnv('DEFAULT_LOCALE'); break;
            case 'kernel.secret': $value = $this->getEnv('APP_SECRET'); break;
            case 'kernel.default_locale': $value = $this->getEnv('DEFAULT_LOCALE'); break;
            case 'session.save_path': $value = ($this->targetDir.''.'/sessions'); break;
            case 'validator.mapping.cache.file': $value = ($this->targetDir.''.'/validation.php'); break;
            case 'debug.container.dump': $value = ($this->targetDir.''.'/srcApp_KernelProdDebugContainer.xml'); break;
            case 'doctrine.orm.proxy_dir': $value = ($this->targetDir.''.'/doctrine/orm/Proxies'); break;
            case 'sylius_money.locale': $value = $this->getEnv('DEFAULT_LOCALE'); break;
            case 'sylius_locale.locale': $value = $this->getEnv('DEFAULT_LOCALE'); break;
            case 'sylius.mailer.sender_name': $value = $this->getEnv('MAILER_SENDER_NAME'); break;
            case 'sylius.mailer.sender_address': $value = $this->getEnv('MAILER_SENDER_EMAIL'); break;
            case 'sylius.grids_definitions': $value = [
                'sylius_admin_address_log_entry' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Sylius\\Component\\Addressing\\Model\\AddressLogEntry',
                            'repository' => [
                                'method' => 'createByObjectIdQueryBuilder',
                                'arguments' => [
                                    'objectId' => '$id',
                                ],
                            ],
                        ],
                    ],
                    'fields' => [
                        'action' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.action',
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/logAction.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'loggedAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.logged_at',
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'data' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.changes',
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/logData.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_admin_user' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\User\\AdminUser',
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'fields' => [
                        'firstName' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.first_name',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'lastName' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.last_name',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'username' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.username',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'email' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.email',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.registration_date',
                            'sortable' => NULL,
                            'options' => [
                                'format' => 'd-m-Y H:i',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'enabled' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.enabled',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/enabled.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'email',
                                    1 => 'username',
                                    2 => 'firstName',
                                    3 => 'lastName',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_channel' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Channel\\Channel',
                        ],
                    ],
                    'sorting' => [
                        'nameAndDescription' => 'asc',
                    ],
                    'fields' => [
                        'nameAndDescription' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.name',
                            'path' => '.',
                            'sortable' => 'name',
                            'options' => [
                                'template' => '@SyliusAdmin/Channel/Grid/Field/name.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.code',
                            'path' => '.',
                            'sortable' => 'code',
                            'options' => [
                                'template' => '@SyliusAdmin/Channel/Grid/Field/code.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'themeName' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.theme',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusAdmin/Channel/Grid/Field/themeName.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'enabled' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.enabled',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/enabled.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_country' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Addressing\\Country',
                        ],
                    ],
                    'sorting' => [
                        'code' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.name',
                            'path' => '.',
                            'sortable' => 'code',
                            'options' => [
                                'template' => '@SyliusAdmin/Country/Grid/Field/name.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'enabled' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.enabled',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/enabled.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_currency' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Currency\\Currency',
                        ],
                    ],
                    'sorting' => [
                        'code' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_customer' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Customer\\Customer',
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'fields' => [
                        'firstName' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.first_name',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'lastName' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.last_name',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'email' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.email',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.registration_date',
                            'sortable' => NULL,
                            'options' => [
                                'format' => 'd-m-Y H:i',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'enabled' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.enabled',
                            'path' => '.',
                            'options' => [
                                'template' => '@SyliusAdmin/Customer/Grid/Field/enabled.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'verified' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.verified',
                            'path' => '.',
                            'options' => [
                                'template' => '@SyliusAdmin/Customer/Grid/Field/verified.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'email',
                                    1 => 'firstName',
                                    2 => 'lastName',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'show_orders' => [
                                'type' => 'show',
                                'label' => 'sylius.ui.show_orders',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_admin_customer_order_index',
                                        'parameters' => [
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'show' => [
                                'type' => 'show',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_customer_group' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Customer\\CustomerGroup',
                        ],
                    ],
                    'sorting' => [
                        'name' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_customer_order' => [
                    'extends' => 'sylius_admin_order',
                    'driver' => [
                        'options' => [
                            'repository' => [
                                'method' => 'createByCustomerIdQueryBuilder',
                                'arguments' => [
                                    'customerId' => '$id',
                                ],
                            ],
                        ],
                        'name' => 'doctrine/orm',
                    ],
                    'sorting' => [
                        'number' => 'desc',
                    ],
                    'filters' => [
                        'customer' => [
                            'type' => 'string',
                            'enabled' => false,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'fields' => [

                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_exchange_rate' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Currency\\ExchangeRate',
                        ],
                    ],
                    'sorting' => [
                        'id' => 'desc',
                    ],
                    'fields' => [
                        'id' => [
                            'type' => 'string',
                            'enabled' => false,
                            'sortable' => NULL,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'sourceCurrency' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.source_currency',
                            'path' => '.',
                            'options' => [
                                'template' => '@SyliusAdmin/ExchangeRate/Grid/Field/sourceCurrencyName.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'targetCurrency' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.target_currency',
                            'path' => '.',
                            'options' => [
                                'template' => '@SyliusAdmin/ExchangeRate/Grid/Field/targetCurrencyName.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'ratio' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.ratio',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'currency' => [
                            'type' => 'entity',
                            'label' => 'sylius.ui.currency',
                            'options' => [
                                'fields' => [
                                    0 => 'sourceCurrency',
                                    1 => 'targetCurrency',
                                ],
                            ],
                            'form_options' => [
                                'class' => 'App\\Entity\\Currency\\Currency',
                                'choice_label' => 'name',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_inventory' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductVariant',
                            'repository' => [
                                'method' => 'createInventoryListQueryBuilder',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                ],
                            ],
                        ],
                    ],
                    'fields' => [
                        'name' => [
                            'type' => 'twig',
                            'path' => '.',
                            'label' => 'sylius.ui.name',
                            'options' => [
                                'template' => '@SyliusAdmin/ProductVariant/Grid/Field/name.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'inventory' => [
                            'type' => 'twig',
                            'path' => '.',
                            'label' => 'sylius.ui.inventory',
                            'options' => [
                                'template' => '@SyliusAdmin/ProductVariant/Grid/Field/inventory.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'options' => [
                                'fields' => [
                                    0 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'update_product' => [
                                'type' => 'update',
                                'label' => 'sylius.ui.edit_product',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_admin_product_update',
                                        'parameters' => [
                                            'id' => 'resource.product.id',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'update' => [
                                'type' => 'update',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'id' => 'resource.id',
                                            'productId' => 'resource.product.id',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_locale' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Locale\\Locale',
                        ],
                    ],
                    'sorting' => [
                        'code' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.name',
                            'path' => '.',
                            'sortable' => 'code',
                            'options' => [
                                'template' => '@SyliusAdmin/Locale/Grid/Field/name.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_order' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Order\\Order',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'sorting' => [
                        'number' => 'desc',
                    ],
                    'fields' => [
                        'channel' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.channel',
                            'sortable' => 'channel.code',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/channel.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'number' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.order',
                            'path' => '.',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/number.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'date' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.date',
                            'path' => 'checkoutCompletedAt',
                            'sortable' => 'checkoutCompletedAt',
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'customer' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.customer',
                            'sortable' => 'customer.lastName',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/customer.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'state' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.state',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/state.html.twig',
                                'vars' => [
                                    'labels' => '@SyliusAdmin/Order/Label/State',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'paymentState' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.payment_state',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/state.html.twig',
                                'vars' => [
                                    'labels' => '@SyliusAdmin/Order/Label/PaymentState',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'shippingState' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.shipping_state',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/state.html.twig',
                                'vars' => [
                                    'labels' => '@SyliusAdmin/Order/Label/ShippingState',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'total' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.total',
                            'path' => '.',
                            'sortable' => 'total',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/total.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'currencyCode' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.currency',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'number' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.number',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'customer' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.customer',
                            'options' => [
                                'fields' => [
                                    0 => 'customer.email',
                                    1 => 'customer.firstName',
                                    2 => 'customer.lastName',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'date' => [
                            'type' => 'date',
                            'label' => 'sylius.ui.date',
                            'options' => [
                                'field' => 'checkoutCompletedAt',
                                'inclusive_to' => true,
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'channel' => [
                            'type' => 'entity',
                            'label' => 'sylius.ui.channel',
                            'form_options' => [
                                'class' => 'App\\Entity\\Channel\\Channel',
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'total' => [
                            'type' => 'money',
                            'label' => 'sylius.ui.total',
                            'options' => [
                                'currency_field' => 'currencyCode',
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'show' => [
                                'type' => 'show',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_payment' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Payment\\Payment',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'fields' => [
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.date',
                            'sortable' => NULL,
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'number' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.order',
                            'path' => 'order',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/number.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'channel' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.channel',
                            'path' => 'order.channel',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/channel.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'customer' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.customer',
                            'path' => 'order.customer',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/customer.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'state' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.state',
                            'options' => [
                                'template' => '@SyliusAdmin/Common/Label/paymentState.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'state' => [
                            'type' => 'select',
                            'label' => 'sylius.ui.state',
                            'form_options' => [
                                'choices' => [
                                    'sylius.ui.cancelled' => 'cancelled',
                                    'sylius.ui.completed' => 'completed',
                                    'sylius.ui.failed' => 'failed',
                                    'sylius.ui.new' => 'new',
                                    'sylius.ui.processing' => 'processing',
                                    'sylius.ui.refunded' => 'refunded',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'complete' => [
                                'type' => 'apply_transition',
                                'label' => 'sylius.ui.complete',
                                'icon' => 'payment',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_admin_payment_complete',
                                        'parameters' => [
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                    'class' => 'teal',
                                    'transition' => 'complete',
                                    'graph' => 'sylius_payment',
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_payment_method' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Payment\\PaymentMethod',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'fields' => [
                        'position' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.position',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/position.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'sortable' => 'translation.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'gateway' => [
                            'type' => 'twig',
                            'path' => 'gatewayConfig.factoryName',
                            'label' => 'sylius.ui.gateway',
                            'sortable' => 'gatewayConfig.factoryName',
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/humanized.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'enabled' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.enabled',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/enabled.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create_payment_method',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_product' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\Product',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                    1 => '$taxonId',
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'code' => 'asc',
                    ],
                    'fields' => [
                        'image' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.image',
                            'path' => '.',
                            'options' => [
                                'template' => '@SyliusAdmin/Product/Grid/Field/image.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'sortable' => 'translation.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'mainTaxon' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.main_taxon',
                            'options' => [
                                'template' => '@SyliusAdmin/Product/Grid/Field/mainTaxon.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'enabled' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.enabled',
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/enabled.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'links',
                                'label' => 'sylius.ui.create',
                                'options' => [
                                    'class' => 'primary',
                                    'icon' => 'plus',
                                    'header' => [
                                        'icon' => 'cube',
                                        'label' => 'sylius.ui.type',
                                    ],
                                    'links' => [
                                        'simple' => [
                                            'label' => 'sylius.ui.simple_product',
                                            'icon' => 'plus',
                                            'route' => 'sylius_admin_product_create_simple',
                                        ],
                                        'configurable' => [
                                            'label' => 'sylius.ui.configurable_product',
                                            'icon' => 'plus',
                                            'route' => 'sylius_admin_product_create',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'details' => [
                                'label' => 'sylius.ui.details',
                                'type' => 'show',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'subitem' => [
                            'variants' => [
                                'type' => 'links',
                                'label' => 'sylius.ui.manage_variants',
                                'options' => [
                                    'icon' => 'cubes',
                                    'links' => [
                                        'index' => [
                                            'label' => 'sylius.ui.list_variants',
                                            'icon' => 'list',
                                            'route' => 'sylius_admin_product_variant_index',
                                            'parameters' => [
                                                'productId' => 'resource.id',
                                            ],
                                        ],
                                        'create' => [
                                            'label' => 'sylius.ui.create',
                                            'icon' => 'plus',
                                            'route' => 'sylius_admin_product_variant_create',
                                            'parameters' => [
                                                'productId' => 'resource.id',
                                            ],
                                        ],
                                        'generate' => [
                                            'label' => 'sylius.ui.generate',
                                            'icon' => 'random',
                                            'route' => 'sylius_admin_product_variant_generate',
                                            'visible' => 'resource.hasOptions',
                                            'parameters' => [
                                                'productId' => 'resource.id',
                                            ],
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_product_association_type' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductAssociationType',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'code' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'options' => [
                                'fields' => [
                                    0 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_product_attribute' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductAttribute',
                        ],
                    ],
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'position' => [
                            'type' => 'string',
                            'enabled' => false,
                            'sortable' => NULL,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'type' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.type',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/label.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create_product_attribute',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_product_from_taxon' => [
                    'extends' => 'sylius_admin_product',
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'fields' => [
                        'position' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.position',
                            'path' => '.',
                            'sortable' => 'productTaxon.position',
                            'options' => [
                                'template' => '@SyliusAdmin/Product/Grid/Field/position.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'update_positions' => [
                                'type' => 'update_product_positions',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [

                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'sylius_admin_product_option' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductOption',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'fields' => [
                        'position' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.position',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/position.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'sortable' => 'translation.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_product_review' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductReview',
                        ],
                    ],
                    'sorting' => [
                        'date' => 'desc',
                    ],
                    'fields' => [
                        'date' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.date',
                            'path' => 'createdAt',
                            'sortable' => 'createdAt',
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'title' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.title',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'rating' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.rating',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'status' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.status',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/state.html.twig',
                                'vars' => [
                                    'labels' => '@SyliusAdmin/ProductReview/Label/Status',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'reviewSubject' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.product',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'author' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.customer',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'title' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.title',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'accept' => [
                                'type' => 'apply_transition',
                                'label' => 'sylius.ui.accept',
                                'icon' => 'checkmark',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_admin_product_review_accept',
                                        'parameters' => [
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                    'class' => 'green',
                                    'transition' => 'accept',
                                    'graph' => 'sylius_product_review',
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'reject' => [
                                'type' => 'apply_transition',
                                'label' => 'sylius.ui.reject',
                                'icon' => 'remove',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_admin_product_review_reject',
                                        'parameters' => [
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                    'class' => 'yellow',
                                    'transition' => 'reject',
                                    'graph' => 'sylius_product_review',
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_product_variant' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductVariant',
                            'repository' => [
                                'method' => 'createQueryBuilderByProductId',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                    1 => '$productId',
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'fields' => [
                        'name' => [
                            'type' => 'twig',
                            'path' => '.',
                            'label' => 'sylius.ui.name',
                            'options' => [
                                'template' => '@SyliusAdmin/ProductVariant/Grid/Field/name.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'inventory' => [
                            'type' => 'twig',
                            'path' => '.',
                            'label' => 'sylius.ui.inventory',
                            'options' => [
                                'template' => '@SyliusAdmin/ProductVariant/Grid/Field/inventory.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'position' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.position',
                            'path' => '.',
                            'sortable' => 'position',
                            'options' => [
                                'template' => '@SyliusAdmin/ProductVariant/Grid/Field/position.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'options' => [
                                'fields' => [
                                    0 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'generate' => [
                                'type' => 'generate_variants',
                                'options' => [
                                    'product' => 'expr:service(\'sylius.repository.product\').find($productId)',
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'update_positions' => [
                                'type' => 'update_product_variant_positions',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'create' => [
                                'type' => 'create',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'productId' => '$productId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'id' => 'resource.id',
                                            'productId' => '$productId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'id' => 'resource.id',
                                            'productId' => '$productId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'productId' => '$productId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_promotion' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Promotion\\Promotion',
                        ],
                    ],
                    'sorting' => [
                        'priority' => 'desc',
                    ],
                    'fields' => [
                        'priority' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.priority',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/position.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.name',
                            'path' => '.',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/nameAndDescription.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'couponBased' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.coupons',
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/yesNo.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'usage' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.usage',
                            'path' => '.',
                            'sortable' => 'used',
                            'options' => [
                                'template' => '@SyliusAdmin/Promotion/Grid/Field/usage.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'couponBased' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.coupon_based',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'coupons' => [
                                'type' => 'links',
                                'label' => 'sylius.ui.manage_coupons',
                                'options' => [
                                    'visible' => 'resource.couponBased',
                                    'icon' => 'ticket',
                                    'links' => [
                                        'index' => [
                                            'label' => 'sylius.ui.list_coupons',
                                            'icon' => 'list',
                                            'route' => 'sylius_admin_promotion_coupon_index',
                                            'parameters' => [
                                                'promotionId' => 'resource.id',
                                            ],
                                        ],
                                        'create' => [
                                            'label' => 'sylius.ui.create',
                                            'icon' => 'plus',
                                            'route' => 'sylius_admin_promotion_coupon_create',
                                            'parameters' => [
                                                'promotionId' => 'resource.id',
                                            ],
                                        ],
                                        'generate' => [
                                            'label' => 'sylius.ui.generate',
                                            'icon' => 'random',
                                            'route' => 'sylius_admin_promotion_coupon_generate',
                                            'parameters' => [
                                                'promotionId' => 'resource.id',
                                            ],
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_promotion_coupon' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Promotion\\PromotionCoupon',
                            'repository' => [
                                'method' => 'createQueryBuilderByPromotionId',
                                'arguments' => [
                                    'promotionId' => '$promotionId',
                                ],
                            ],
                        ],
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'usageLimit' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.usage_limit',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'perCustomerUsageLimit' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.per_customer_usage_limit',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'used' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.used',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'expiresAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.expires_at',
                            'sortable' => NULL,
                            'options' => [
                                'format' => 'd-m-Y',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'generate' => [
                                'type' => 'default',
                                'label' => 'sylius.ui.generate',
                                'icon' => 'random',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_admin_promotion_coupon_generate',
                                        'parameters' => [
                                            'promotionId' => '$promotionId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'create' => [
                                'type' => 'create',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'promotionId' => '$promotionId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'id' => 'resource.id',
                                            'promotionId' => '$promotionId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'id' => 'resource.id',
                                            'promotionId' => '$promotionId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'promotionId' => '$promotionId',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_shipment' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Shipping\\Shipment',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'fields' => [
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.date',
                            'sortable' => NULL,
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'number' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.order',
                            'path' => 'order',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/number.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'channel' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.channel',
                            'path' => 'order.channel',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/channel.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'customer' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.customer',
                            'path' => 'order.customer',
                            'options' => [
                                'template' => '@SyliusAdmin/Order/Grid/Field/customer.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'state' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.state',
                            'options' => [
                                'template' => '@SyliusAdmin/Common/Label/shipmentState.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'state' => [
                            'type' => 'select',
                            'label' => 'sylius.ui.state',
                            'form_options' => [
                                'choices' => [
                                    'sylius.ui.cancelled' => 'cancelled',
                                    'sylius.ui.ready' => 'ready',
                                    'sylius.ui.shipped' => 'shipped',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'channel' => [
                            'type' => 'entity',
                            'label' => 'sylius.ui.channel',
                            'options' => [
                                'fields' => [
                                    0 => 'order.channel',
                                ],
                            ],
                            'form_options' => [
                                'class' => 'App\\Entity\\Channel\\Channel',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'ship' => [
                                'type' => 'apply_transition',
                                'label' => 'sylius.ui.ship',
                                'icon' => 'plane',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_admin_shipment_ship',
                                        'parameters' => [
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                    'class' => 'teal',
                                    'transition' => 'ship',
                                    'graph' => 'sylius_shipment',
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_shipping_category' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Shipping\\ShippingCategory',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.creation_date',
                            'options' => [
                                'format' => 'd-m-Y H:i',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'updatedAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.updating_date',
                            'options' => [
                                'format' => 'd-m-Y H:i',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_shipping_method' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Shipping\\ShippingMethod',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'fields' => [
                        'position' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.position',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/position.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'sortable' => 'translation.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'zone' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.zone',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.enabled',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/enabled.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'archival' => [
                            'type' => 'exists',
                            'label' => 'sylius.ui.archival',
                            'options' => [
                                'field' => 'archivedAt',
                            ],
                            'default_value' => false,
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'archive' => [
                                'type' => 'archive',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_tax_category' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Taxation\\TaxCategory',
                        ],
                    ],
                    'sorting' => [
                        'nameAndDescription' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'nameAndDescription' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.name',
                            'path' => '.',
                            'sortable' => 'name',
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/nameAndDescription.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_tax_rate' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Taxation\\TaxRate',
                        ],
                    ],
                    'sorting' => [
                        'name' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'zone' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.zone',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'category' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.category',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'amount' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.amount',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/percent.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_taxon' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Taxonomy\\Taxon',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'fields' => [

                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_zone' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Addressing\\Zone',
                        ],
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'type' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.type',
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/label.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'links',
                                'label' => 'sylius.ui.create',
                                'options' => [
                                    'class' => 'primary',
                                    'icon' => 'plus',
                                    'header' => [
                                        'icon' => 'cube',
                                        'label' => 'sylius.ui.type',
                                    ],
                                    'links' => [
                                        'country' => [
                                            'label' => 'sylius.ui.zone_consisting_of_countries',
                                            'icon' => 'plus',
                                            'route' => 'sylius_admin_zone_create',
                                            'parameters' => [
                                                'type' => 'country',
                                            ],
                                        ],
                                        'province' => [
                                            'label' => 'sylius.ui.zone_consisting_of_provinces',
                                            'icon' => 'plus',
                                            'route' => 'sylius_admin_zone_create',
                                            'parameters' => [
                                                'type' => 'province',
                                            ],
                                        ],
                                        'zone' => [
                                            'label' => 'sylius.ui.zone_consisting_of_other_zones',
                                            'icon' => 'plus',
                                            'route' => 'sylius_admin_zone_create',
                                            'parameters' => [
                                                'type' => 'zone',
                                            ],
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'bulk' => [
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'sylius_admin_api_cart' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Order\\Order',
                            'repository' => [
                                'method' => 'createCartQueryBuilder',
                            ],
                        ],
                    ],
                    'sorting' => [
                        'number' => 'desc',
                    ],
                    'fields' => [
                        'channel' => [
                            'type' => 'string',
                            'path' => 'channel.code',
                            'sortable' => 'channel.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'number' => [
                            'type' => 'string',
                            'path' => '.',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'date' => [
                            'type' => 'datetime',
                            'path' => 'checkoutCompletedAt',
                            'sortable' => 'checkoutCompletedAt',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'customer' => [
                            'type' => 'string',
                            'sortable' => 'customer.email',
                            'path' => 'customer.email',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'total' => [
                            'type' => 'integer',
                            'sortable' => 'total',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'currencyCode' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.currency',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'number' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'customer' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'customer.email',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'date' => [
                            'type' => 'date',
                            'label' => 'sylius.ui.date',
                            'options' => [
                                'field' => 'checkoutCompletedAt',
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'channel' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'channel.code',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'total' => [
                            'type' => 'money',
                            'options' => [
                                'currency_field' => 'currencyCode',
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_api_payment' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Payment\\Payment',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'fields' => [
                        'amount' => [
                            'type' => 'integer',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'state' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'updatedAt' => [
                            'type' => 'datetime',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'state' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_api_product' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\Product',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                    1 => '$taxonId',
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'code' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'sortable' => 'translation.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_api_product_review' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductReview',
                            'repository' => [
                                'method' => 'createQueryBuilderByProductCode',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                    1 => '$productCode',
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'date' => 'desc',
                    ],
                    'fields' => [
                        'date' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.date',
                            'path' => 'createdAt',
                            'sortable' => 'createdAt',
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'title' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.title',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'rating' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.rating',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'status' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.status',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'reviewSubject' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.product',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'author' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.customer',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'title' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.title',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_api_product_variant' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\ProductVariant',
                            'repository' => [
                                'method' => 'createQueryBuilderByProductCode',
                                'arguments' => [
                                    0 => $this->getEnv('DEFAULT_LOCALE'),
                                    1 => '$productCode',
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'fields' => [
                        'name' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'code' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'position' => [
                            'type' => 'string',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_api_promotion' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Promotion\\Promotion',
                        ],
                    ],
                    'sorting' => [
                        'priority' => 'desc',
                    ],
                    'fields' => [
                        'priority' => [
                            'type' => 'integer',
                            'label' => 'sylius.ui.priority',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'path' => '.',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'couponBased' => [
                            'type' => 'boolean',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_api_shipment' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Shipping\\Shipment',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'fields' => [
                        'state' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'updatedAt' => [
                            'type' => 'datetime',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'state' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'sylius_admin_api_taxon' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Taxonomy\\Taxon',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'filters' => [
                        'name' => [
                            'type' => 'string',
                            'options' => [
                                'fields' => [
                                    0 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'code' => [
                            'type' => 'string',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'fields' => [

                    ],
                    'actions' => [

                    ],
                ],
                'sylius_shop_account_order' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Order\\Order',
                            'repository' => [
                                'method' => 'createByCustomerAndChannelIdQueryBuilder',
                                'arguments' => [
                                    0 => 'expr:service(\'sylius.context.customer\').getCustomer().getId()',
                                    1 => 'expr:service(\'sylius.context.channel\').getChannel().getId()',
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'checkoutCompletedAt' => 'desc',
                    ],
                    'fields' => [
                        'number' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.number',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusShop/Account/Order/Grid/Field/number.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'checkoutCompletedAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.date',
                            'sortable' => NULL,
                            'options' => [
                                'format' => 'm/d/Y',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'shippingAddress' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.ship_to',
                            'options' => [
                                'template' => '@SyliusShop/Account/Order/Grid/Field/address.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'total' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.total',
                            'path' => '.',
                            'sortable' => 'total',
                            'options' => [
                                'template' => '@SyliusShop/Account/Order/Grid/Field/total.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'state' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.state',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@SyliusUi/Grid/Field/label.html.twig',
                                'vars' => [
                                    'labels' => '@SyliusShop/Account/Order/Label/State',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'show' => [
                                'type' => 'shop_show',
                                'label' => 'sylius.ui.show',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_shop_account_order_show',
                                        'parameters' => [
                                            'number' => 'resource.number',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'pay' => [
                                'type' => 'shop_pay',
                                'label' => 'sylius.ui.pay',
                                'options' => [
                                    'link' => [
                                        'route' => 'sylius_shop_order_show',
                                        'parameters' => [
                                            'tokenValue' => 'resource.tokenValue',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'sylius_shop_product' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Product\\Product',
                            'repository' => [
                                'method' => 'createShopListQueryBuilder',
                                'arguments' => [
                                    'channel' => 'expr:service(\'sylius.context.channel\').getChannel()',
                                    'taxon' => 'expr:notFoundOnNull(service(\'sylius.repository.taxon\').findOneBySlug($slug, service(\'sylius.context.locale\').getLocaleCode()))',
                                    'locale' => 'expr:service(\'sylius.context.locale\').getLocaleCode()',
                                    'sorting' => 'expr:service(\'request_stack\').getCurrentRequest().get(\'sorting\', [])',
                                    'includeAllDescendants' => 'expr:parameter(\'sylius_shop.product_grid.include_all_descendants\')',
                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'position' => 'asc',
                    ],
                    'limits' => [
                        0 => 9,
                        1 => 18,
                        2 => 27,
                    ],
                    'fields' => [
                        'createdAt' => [
                            'type' => 'datetime',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'position' => [
                            'type' => 'string ',
                            'sortable' => 'productTaxon.position',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string ',
                            'sortable' => 'translation.name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'price' => [
                            'type' => 'int',
                            'sortable' => 'channelPricing.price',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'shop_string',
                            'label' => false,
                            'options' => [
                                'fields' => [
                                    0 => 'translation.name',
                                ],
                            ],
                            'form_options' => [
                                'type' => 'contains',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'price' => [
                            'type' => 'omni_price',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'attributes' => [
                            'type' => 'omni_product_attribute',
                            'form_options' => [
                                'label' => false,
                                'clear_attribute_button' => true,
                                'clear_all_button' => true,
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [

                    ],
                ],
                'omni_admin_parcel_machine' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Omni\\Sylius\\ParcelMachinePlugin\\Model\\ParcelMachine',
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'asc',
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'provider' => [
                            'type' => 'string',
                            'label' => 'omni.ui.provider',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'country' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.country',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'city' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.city',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'street' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.street',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.creation_date',
                            'path' => 'createdAt',
                            'sortable' => 'createdAt',
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'updatedAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.updating_date',
                            'path' => 'updatedAt',
                            'sortable' => 'updatedAt',
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'actions' => [

                    ],
                ],
                'app_order_report' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Order\\OrderReport',
                        ],
                    ],
                    'fields' => [
                        'from' => [
                            'type' => 'datetime',
                            'label' => 'app.order.from',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'to' => [
                            'type' => 'datetime',
                            'label' => 'app.order.to',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'show' => [
                                'type' => 'show',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'omni_sylius_admin_banners' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZone',
                        ],
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'title' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.title',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'options' => [
                                    'link' => [
                                        'route' => 'omni_sylius_banner_zone_create',
                                        'parameters' => [
                                            'zone' => '$zone',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'show_banners' => [
                                'type' => 'show',
                                'label' => 'omni_sylius.ui.show_banners',
                                'options' => [
                                    'link' => [
                                        'route' => 'omni_sylius_banner_index_zone',
                                        'parameters' => [
                                            'zone' => 'resource.code',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'show_positions' => [
                                'type' => 'show',
                                'label' => 'omni_sylius.ui.show_positions',
                                'options' => [
                                    'link' => [
                                        'route' => 'omni_sylius_banner_position_index',
                                        'parameters' => [
                                            'zone' => 'resource.code',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'omni_sylius_admin_zone_banners' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Omni\\Sylius\\BannerPlugin\\Model\\Banner',
                            'repository' => [
                                'method' => 'createByZoneCodeQueryBuilder',
                                'arguments' => [
                                    'zone' => '$zone',
                                ],
                            ],
                        ],
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'position' => [
                            'type' => 'string',
                            'path' => 'position.translation.title',
                            'label' => 'omni_sylius.form.position',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'image' => [
                            'type' => 'twig',
                            'options' => [
                                'template' => 'OmniSyliusBannerPlugin:Admin/Banner:_gridImage.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.title',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'position' => [
                            'type' => 'string',
                            'label' => 'omni_sylius.form.position',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'zone' => '$zone',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'zone' => '$zone',
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'omni_sylius_admin_position_banners' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Omni\\Sylius\\BannerPlugin\\Model\\Banner',
                            'repository' => [
                                'method' => 'createByPositionCodeQueryBuilder',
                                'arguments' => [
                                    'position' => '$position',
                                ],
                            ],
                        ],
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'position' => [
                            'type' => 'string',
                            'path' => 'position.translation.title',
                            'label' => 'omni_sylius.form.position',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.title',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'position' => [
                            'type' => 'string',
                            'label' => 'omni_sylius.form.position',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                        'enabled' => [
                            'type' => 'boolean',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'zone' => '$zone',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'options' => [
                                    'link' => [
                                        'parameters' => [
                                            'zone' => 'resource.position.zone.code',
                                            'position' => 'resource.position.code',
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'omni_sylius_banner_positions' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPosition',
                            'repository' => [
                                'method' => 'createByZoneCodeQueryBuilder',
                                'arguments' => [
                                    'zone' => '$zone',
                                ],
                            ],
                        ],
                    ],
                    'fields' => [
                        'title' => [
                            'type' => 'string',
                            'path' => 'translation.title',
                            'label' => 'sylius.ui.title',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'type' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.type',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'link' => [
                            'type' => 'string',
                            'path' => 'translation.link',
                            'label' => 'omni_sylius.ui.link',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'options' => [
                                    'link' => [
                                        'route' => 'omni_sylius_banner_position_create',
                                        'parameters' => [
                                            'zone' => '$zone',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'options' => [
                                    'link' => [
                                        'route' => 'omni_sylius_banner_position_update',
                                        'parameters' => [
                                            'zone' => '$zone',
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'show_banners' => [
                                'type' => 'show',
                                'label' => 'omni_sylius.ui.show_banners',
                                'options' => [
                                    'link' => [
                                        'route' => 'omni_sylius_banner_index_position',
                                        'parameters' => [
                                            'position' => 'resource.code',
                                            'zone' => '$zone',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'options' => [
                                    'link' => [
                                        'route' => 'omni_sylius_banner_position_delete',
                                        'parameters' => [
                                            'id' => 'resource.id',
                                        ],
                                    ],
                                ],
                                'enabled' => true,
                                'position' => 100,
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'omni_sylius_imort_job' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Omni\\Sylius\\ImportPlugin\\Model\\ImportJob',
                        ],
                    ],
                    'fields' => [
                        'importer' => [
                            'type' => 'string',
                            'label' => 'omni_sylius.importer.form.importer',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'status' => [
                            'type' => 'string',
                            'label' => 'omni_sylius.importer.form.status',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'omni_sylius.importer.form.created_at',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'processedAt' => [
                            'type' => 'datetime',
                            'label' => 'omni_sylius.importer.form.processed_at',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'create',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'show' => [
                                'type' => 'show',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'omni_sylius_export_job' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'Omni\\Sylius\\ImportPlugin\\Model\\ExportJob',
                        ],
                    ],
                    'fields' => [
                        'exporter' => [
                            'type' => 'string',
                            'label' => 'omni_sylius.exporter.form.exporter',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'status' => [
                            'type' => 'string',
                            'label' => 'omni_sylius.exporter.form.status',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'omni_sylius.exporter.form.created_at',
                            'sortable' => NULL,
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'processedAt' => [
                            'type' => 'datetime',
                            'label' => 'omni_sylius.exporter.form.processed_at',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'show' => [
                                'type' => 'show',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [
                        'createdAt' => 'desc',
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'omni_sylius_manifest' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Shipping\\Manifest',
                        ],
                    ],
                    'fields' => [
                        'provider' => [
                            'type' => 'string',
                            'label' => 'omni_sylius_manifest.ui.provider',
                            'path' => 'shippingGateway.code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'createdAt' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.created_at',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'item' => [
                            'download' => [
                                'type' => 'download_manifest',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                    'filters' => [

                    ],
                ],
                'bitbag_admin_shipping_export' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'App\\Entity\\Shipping\\ShippingExport',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'sorting' => [
                        'shipment' => 'desc',
                    ],
                    'fields' => [
                        'order' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.order',
                            'path' => '.',
                            'sortable' => 'shipment.order',
                            'options' => [
                                'template' => '@BitBagSyliusShippingExportPlugin/ShippingExport/Grid/Field/orderNumber.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'shipment' => [
                            'type' => 'datetime',
                            'label' => 'sylius.ui.created_at',
                            'path' => 'shipment.createdAt',
                            'sortable' => 'shipment.createdAt',
                            'options' => [
                                'format' => 'd-m-Y H:i:s',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'exportedAt' => [
                            'type' => 'twig',
                            'label' => 'bitbag.ui.exported_at',
                            'path' => '.',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@BitBagSyliusShippingExportPlugin/ShippingExport/Grid/Field/exportedAt.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'shippingMethod' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.shipping_method',
                            'path' => '.',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@BitBagSyliusShippingExportPlugin/ShippingExport/Grid/Field/shippingMethod.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'labelPath' => [
                            'type' => 'twig',
                            'label' => 'bitbag.ui.shipping_export_label',
                            'path' => '.',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@BitBagSyliusShippingExportPlugin/ShippingExport/Grid/Field/shippingExportLabel.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                        'state' => [
                            'type' => 'twig',
                            'label' => 'sylius.ui.state',
                            'path' => '.',
                            'sortable' => NULL,
                            'options' => [
                                'template' => '@BitBagSyliusShippingExportPlugin/ShippingExport/Grid/Field/state.html.twig',
                            ],
                            'enabled' => true,
                            'position' => 100,
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'translation.name',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                        'state' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.enabled',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'create' => [
                                'type' => 'export_shipments',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
                'bitbag_admin_shipping_gateway' => [
                    'driver' => [
                        'name' => 'doctrine/orm',
                        'options' => [
                            'class' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingGateway',
                            'repository' => [
                                'method' => 'createListQueryBuilder',
                            ],
                        ],
                    ],
                    'fields' => [
                        'code' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.code',
                            'sortable' => 'code',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                        'name' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.name',
                            'sortable' => 'name',
                            'enabled' => true,
                            'position' => 100,
                            'options' => [

                            ],
                        ],
                    ],
                    'filters' => [
                        'search' => [
                            'type' => 'string',
                            'label' => 'sylius.ui.search',
                            'options' => [
                                'fields' => [
                                    0 => 'code',
                                    1 => 'shippingMethod.translation.name',
                                    2 => 'status',
                                ],
                            ],
                            'enabled' => true,
                            'position' => 100,
                            'form_options' => [

                            ],
                        ],
                    ],
                    'actions' => [
                        'main' => [
                            'export' => [
                                'type' => 'create_shipping_gateway',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                        'item' => [
                            'update' => [
                                'type' => 'update',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                            'delete' => [
                                'type' => 'delete',
                                'enabled' => true,
                                'position' => 100,
                                'options' => [

                                ],
                            ],
                        ],
                    ],
                    'sorting' => [

                    ],
                    'limits' => [
                        0 => 10,
                        1 => 25,
                        2 => 50,
                    ],
                ],
            ]; break;
            case 'stof_doctrine_extensions.default_locale': $value = $this->getEnv('DEFAULT_LOCALE'); break;
            case 'omni_sylius_dpd.username': $value = $this->getEnv('DPD_USERNAME'); break;
            case 'omni_sylius_dpd.password': $value = $this->getEnv('DPD_PASSWORD'); break;
            case 'omni_sylius_dpd.host': $value = $this->getEnv('DPD_HOST'); break;
            case 'omni_sylius_paysera.options': $value = [
                'default_project' => 'default',
                'projects' => [
                    'default' => [
                        'project_id' => $this->getEnv('PAYSERA_PROJECT_ID'),
                        'password' => $this->getEnv('PAYSERA_PASSWORD'),
                        'test_mode' => $this->getEnv('PAYSERA_TEST_MODE'),
                    ],
                ],
                'use_order_number' => false,
            ]; break;
            case 'lexik_translation.translator.options': $value = [
                'cache_dir' => ($this->targetDir.''.'/translations'),
                'debug' => true,
            ]; break;
            default: throw new InvalidArgumentException(sprintf('The dynamic parameter "%s" must be defined.', $name));
        }
        $this->loadedDynamicParameters[$name] = true;

        return $this->dynamicParameters[$name] = $value;
    }

    protected function getDefaultParameters(): array
    {
        return [
            'kernel.root_dir' => (\dirname(__DIR__, 4).'/src'),
            'kernel.project_dir' => \dirname(__DIR__, 4),
            'kernel.environment' => 'prod',
            'kernel.debug' => true,
            'kernel.name' => 'src',
            'kernel.logs_dir' => (\dirname(__DIR__, 3).'/log'),
            'kernel.bundles' => [
                'FrameworkBundle' => 'Symfony\\Bundle\\FrameworkBundle\\FrameworkBundle',
                'MonologBundle' => 'Symfony\\Bundle\\MonologBundle\\MonologBundle',
                'SecurityBundle' => 'Symfony\\Bundle\\SecurityBundle\\SecurityBundle',
                'SwiftmailerBundle' => 'Symfony\\Bundle\\SwiftmailerBundle\\SwiftmailerBundle',
                'TwigBundle' => 'Symfony\\Bundle\\TwigBundle\\TwigBundle',
                'DoctrineBundle' => 'Doctrine\\Bundle\\DoctrineBundle\\DoctrineBundle',
                'DoctrineCacheBundle' => 'Doctrine\\Bundle\\DoctrineCacheBundle\\DoctrineCacheBundle',
                'SyliusOrderBundle' => 'Sylius\\Bundle\\OrderBundle\\SyliusOrderBundle',
                'SyliusMoneyBundle' => 'Sylius\\Bundle\\MoneyBundle\\SyliusMoneyBundle',
                'SyliusCurrencyBundle' => 'Sylius\\Bundle\\CurrencyBundle\\SyliusCurrencyBundle',
                'SyliusLocaleBundle' => 'Sylius\\Bundle\\LocaleBundle\\SyliusLocaleBundle',
                'SyliusProductBundle' => 'Sylius\\Bundle\\ProductBundle\\SyliusProductBundle',
                'SyliusChannelBundle' => 'Sylius\\Bundle\\ChannelBundle\\SyliusChannelBundle',
                'SyliusAttributeBundle' => 'Sylius\\Bundle\\AttributeBundle\\SyliusAttributeBundle',
                'SyliusTaxationBundle' => 'Sylius\\Bundle\\TaxationBundle\\SyliusTaxationBundle',
                'SyliusShippingBundle' => 'Sylius\\Bundle\\ShippingBundle\\SyliusShippingBundle',
                'SyliusPaymentBundle' => 'Sylius\\Bundle\\PaymentBundle\\SyliusPaymentBundle',
                'SyliusMailerBundle' => 'Sylius\\Bundle\\MailerBundle\\SyliusMailerBundle',
                'SyliusPromotionBundle' => 'Sylius\\Bundle\\PromotionBundle\\SyliusPromotionBundle',
                'SyliusAddressingBundle' => 'Sylius\\Bundle\\AddressingBundle\\SyliusAddressingBundle',
                'SyliusInventoryBundle' => 'Sylius\\Bundle\\InventoryBundle\\SyliusInventoryBundle',
                'SyliusTaxonomyBundle' => 'Sylius\\Bundle\\TaxonomyBundle\\SyliusTaxonomyBundle',
                'SyliusUserBundle' => 'Sylius\\Bundle\\UserBundle\\SyliusUserBundle',
                'SyliusCustomerBundle' => 'Sylius\\Bundle\\CustomerBundle\\SyliusCustomerBundle',
                'SyliusUiBundle' => 'Sylius\\Bundle\\UiBundle\\SyliusUiBundle',
                'SyliusReviewBundle' => 'Sylius\\Bundle\\ReviewBundle\\SyliusReviewBundle',
                'SyliusCoreBundle' => 'Sylius\\Bundle\\CoreBundle\\SyliusCoreBundle',
                'SyliusResourceBundle' => 'Sylius\\Bundle\\ResourceBundle\\SyliusResourceBundle',
                'SyliusGridBundle' => 'Sylius\\Bundle\\GridBundle\\SyliusGridBundle',
                'winzouStateMachineBundle' => 'winzou\\Bundle\\StateMachineBundle\\winzouStateMachineBundle',
                'SonataCoreBundle' => 'Sonata\\CoreBundle\\SonataCoreBundle',
                'SonataBlockBundle' => 'Sonata\\BlockBundle\\SonataBlockBundle',
                'SonataIntlBundle' => 'Sonata\\IntlBundle\\SonataIntlBundle',
                'BazingaHateoasBundle' => 'Bazinga\\Bundle\\HateoasBundle\\BazingaHateoasBundle',
                'JMSSerializerBundle' => 'JMS\\SerializerBundle\\JMSSerializerBundle',
                'FOSRestBundle' => 'FOS\\RestBundle\\FOSRestBundle',
                'KnpGaufretteBundle' => 'Knp\\Bundle\\GaufretteBundle\\KnpGaufretteBundle',
                'KnpMenuBundle' => 'Knp\\Bundle\\MenuBundle\\KnpMenuBundle',
                'LiipImagineBundle' => 'Liip\\ImagineBundle\\LiipImagineBundle',
                'PayumBundle' => 'Payum\\Bundle\\PayumBundle\\PayumBundle',
                'StofDoctrineExtensionsBundle' => 'Stof\\DoctrineExtensionsBundle\\StofDoctrineExtensionsBundle',
                'WhiteOctoberPagerfantaBundle' => 'WhiteOctober\\PagerfantaBundle\\WhiteOctoberPagerfantaBundle',
                'DoctrineMigrationsBundle' => 'Doctrine\\Bundle\\MigrationsBundle\\DoctrineMigrationsBundle',
                'DoctrineFixturesBundle' => 'Doctrine\\Bundle\\FixturesBundle\\DoctrineFixturesBundle',
                'SyliusFixturesBundle' => 'Sylius\\Bundle\\FixturesBundle\\SyliusFixturesBundle',
                'SyliusPayumBundle' => 'Sylius\\Bundle\\PayumBundle\\SyliusPayumBundle',
                'SyliusThemeBundle' => 'Sylius\\Bundle\\ThemeBundle\\SyliusThemeBundle',
                'SyliusAdminBundle' => 'Sylius\\Bundle\\AdminBundle\\SyliusAdminBundle',
                'SyliusShopBundle' => 'Sylius\\Bundle\\ShopBundle\\SyliusShopBundle',
                'FOSOAuthServerBundle' => 'FOS\\OAuthServerBundle\\FOSOAuthServerBundle',
                'SyliusAdminApiBundle' => 'Sylius\\Bundle\\AdminApiBundle\\SyliusAdminApiBundle',
                'OmniSyliusBannerPlugin' => 'Omni\\Sylius\\BannerPlugin\\OmniSyliusBannerPlugin',
                'OmniSyliusSearchPlugin' => 'Omni\\Sylius\\SearchPlugin\\OmniSyliusSearchPlugin',
                'WebpackEncoreBundle' => 'Symfony\\WebpackEncoreBundle\\WebpackEncoreBundle',
                'OmniSyliusFilterPlugin' => 'Omni\\Sylius\\FilterPlugin\\OmniSyliusFilterPlugin',
                'OmniSyliusCorePlugin' => 'Omni\\Sylius\\CorePlugin\\OmniSyliusCorePlugin',
                'OmniSyliusCmsPlugin' => 'Omni\\Sylius\\CmsPlugin\\OmniSyliusCmsPlugin',
                'OmniSyliusSeoPlugin' => 'Omni\\Sylius\\SeoPlugin\\OmniSyliusSeoPlugin',
                'BitBagSyliusShippingExportPlugin' => 'BitBag\\SyliusShippingExportPlugin\\BitBagSyliusShippingExportPlugin',
                'OmniSyliusShippingPlugin' => 'Omni\\Sylius\\ShippingPlugin\\OmniSyliusShippingPlugin',
                'HttplugBundle' => 'Http\\HttplugBundle\\HttplugBundle',
                'OmniSyliusDpdPlugin' => 'Omni\\Sylius\\DpdPlugin\\OmniSyliusDpdPlugin',
                'OmniSyliusParcelMachinePlugin' => 'Omni\\Sylius\\ParcelMachinePlugin\\OmniSyliusParcelMachinePlugin',
                'OmniSyliusPayseraPlugin' => 'Omni\\Sylius\\PayseraPlugin\\OmniSyliusPayseraPlugin',
                'FOSSyliusImportExportPlugin' => 'FriendsOfSylius\\SyliusImportExportPlugin\\FOSSyliusImportExportPlugin',
                'OmniSyliusSwedbankSppPlugin' => 'Omni\\Sylius\\SwedbankSpp\\OmniSyliusSwedbankSppPlugin',
                'SitemapPlugin' => 'SitemapPlugin\\SitemapPlugin',
                'Brille24SyliusTierPricePlugin' => 'Brille24\\SyliusTierPricePlugin\\Brille24SyliusTierPricePlugin',
                'HWIOAuthBundle' => 'HWI\\Bundle\\OAuthBundle\\HWIOAuthBundle',
                'OmniSyliusManifestPlugin' => 'Omni\\Sylius\\ManifestPlugin\\OmniSyliusManifestPlugin',
                'KnpSnappyBundle' => 'Knp\\Bundle\\SnappyBundle\\KnpSnappyBundle',
                'LexikTranslationBundle' => 'Lexik\\Bundle\\TranslationBundle\\LexikTranslationBundle',
                'OmniSyliusTranslatorPlugin' => 'Omni\\Sylius\\TranslatorPlugin\\OmniSyliusTranslatorPlugin',
                'OmniSyliusImportPlugin' => 'Omni\\Sylius\\ImportPlugin\\OmniSyliusImportPlugin',
            ],
            'kernel.bundles_metadata' => [
                'FrameworkBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/symfony/framework-bundle'),
                    'namespace' => 'Symfony\\Bundle\\FrameworkBundle',
                ],
                'MonologBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/symfony/monolog-bundle'),
                    'namespace' => 'Symfony\\Bundle\\MonologBundle',
                ],
                'SecurityBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/symfony/security-bundle'),
                    'namespace' => 'Symfony\\Bundle\\SecurityBundle',
                ],
                'SwiftmailerBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/symfony/swiftmailer-bundle'),
                    'namespace' => 'Symfony\\Bundle\\SwiftmailerBundle',
                ],
                'TwigBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/symfony/twig-bundle'),
                    'namespace' => 'Symfony\\Bundle\\TwigBundle',
                ],
                'DoctrineBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\DoctrineBundle',
                ],
                'DoctrineCacheBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/doctrine/doctrine-cache-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\DoctrineCacheBundle',
                ],
                'SyliusOrderBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/OrderBundle'),
                    'namespace' => 'Sylius\\Bundle\\OrderBundle',
                ],
                'SyliusMoneyBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/MoneyBundle'),
                    'namespace' => 'Sylius\\Bundle\\MoneyBundle',
                ],
                'SyliusCurrencyBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CurrencyBundle'),
                    'namespace' => 'Sylius\\Bundle\\CurrencyBundle',
                ],
                'SyliusLocaleBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/LocaleBundle'),
                    'namespace' => 'Sylius\\Bundle\\LocaleBundle',
                ],
                'SyliusProductBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ProductBundle'),
                    'namespace' => 'Sylius\\Bundle\\ProductBundle',
                ],
                'SyliusChannelBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ChannelBundle'),
                    'namespace' => 'Sylius\\Bundle\\ChannelBundle',
                ],
                'SyliusAttributeBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AttributeBundle'),
                    'namespace' => 'Sylius\\Bundle\\AttributeBundle',
                ],
                'SyliusTaxationBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxationBundle'),
                    'namespace' => 'Sylius\\Bundle\\TaxationBundle',
                ],
                'SyliusShippingBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShippingBundle'),
                    'namespace' => 'Sylius\\Bundle\\ShippingBundle',
                ],
                'SyliusPaymentBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PaymentBundle'),
                    'namespace' => 'Sylius\\Bundle\\PaymentBundle',
                ],
                'SyliusMailerBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/mailer-bundle/src/Bundle'),
                    'namespace' => 'Sylius\\Bundle\\MailerBundle',
                ],
                'SyliusPromotionBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PromotionBundle'),
                    'namespace' => 'Sylius\\Bundle\\PromotionBundle',
                ],
                'SyliusAddressingBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AddressingBundle'),
                    'namespace' => 'Sylius\\Bundle\\AddressingBundle',
                ],
                'SyliusInventoryBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/InventoryBundle'),
                    'namespace' => 'Sylius\\Bundle\\InventoryBundle',
                ],
                'SyliusTaxonomyBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/TaxonomyBundle'),
                    'namespace' => 'Sylius\\Bundle\\TaxonomyBundle',
                ],
                'SyliusUserBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle'),
                    'namespace' => 'Sylius\\Bundle\\UserBundle',
                ],
                'SyliusCustomerBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CustomerBundle'),
                    'namespace' => 'Sylius\\Bundle\\CustomerBundle',
                ],
                'SyliusUiBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UiBundle'),
                    'namespace' => 'Sylius\\Bundle\\UiBundle',
                ],
                'SyliusReviewBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ReviewBundle'),
                    'namespace' => 'Sylius\\Bundle\\ReviewBundle',
                ],
                'SyliusCoreBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle'),
                    'namespace' => 'Sylius\\Bundle\\CoreBundle',
                ],
                'SyliusResourceBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle'),
                    'namespace' => 'Sylius\\Bundle\\ResourceBundle',
                ],
                'SyliusGridBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/grid-bundle/src/Bundle'),
                    'namespace' => 'Sylius\\Bundle\\GridBundle',
                ],
                'winzouStateMachineBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/winzou/state-machine-bundle'),
                    'namespace' => 'winzou\\Bundle\\StateMachineBundle',
                ],
                'SonataCoreBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sonata-project/core-bundle/src/CoreBundle'),
                    'namespace' => 'Sonata\\CoreBundle',
                ],
                'SonataBlockBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sonata-project/block-bundle/src'),
                    'namespace' => 'Sonata\\BlockBundle',
                ],
                'SonataIntlBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sonata-project/intl-bundle/src'),
                    'namespace' => 'Sonata\\IntlBundle',
                ],
                'BazingaHateoasBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/willdurand/hateoas-bundle'),
                    'namespace' => 'Bazinga\\Bundle\\HateoasBundle',
                ],
                'JMSSerializerBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/jms/serializer-bundle'),
                    'namespace' => 'JMS\\SerializerBundle',
                ],
                'FOSRestBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/friendsofsymfony/rest-bundle'),
                    'namespace' => 'FOS\\RestBundle',
                ],
                'KnpGaufretteBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/knplabs/knp-gaufrette-bundle'),
                    'namespace' => 'Knp\\Bundle\\GaufretteBundle',
                ],
                'KnpMenuBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/knplabs/knp-menu-bundle/src'),
                    'namespace' => 'Knp\\Bundle\\MenuBundle',
                ],
                'LiipImagineBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/liip/imagine-bundle'),
                    'namespace' => 'Liip\\ImagineBundle',
                ],
                'PayumBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/payum/payum-bundle'),
                    'namespace' => 'Payum\\Bundle\\PayumBundle',
                ],
                'StofDoctrineExtensionsBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/stof/doctrine-extensions-bundle/src'),
                    'namespace' => 'Stof\\DoctrineExtensionsBundle',
                ],
                'WhiteOctoberPagerfantaBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/white-october/pagerfanta-bundle'),
                    'namespace' => 'WhiteOctober\\PagerfantaBundle',
                ],
                'DoctrineMigrationsBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/doctrine/doctrine-migrations-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\MigrationsBundle',
                ],
                'DoctrineFixturesBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/doctrine/doctrine-fixtures-bundle'),
                    'namespace' => 'Doctrine\\Bundle\\FixturesBundle',
                ],
                'SyliusFixturesBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/fixtures-bundle/src'),
                    'namespace' => 'Sylius\\Bundle\\FixturesBundle',
                ],
                'SyliusPayumBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/PayumBundle'),
                    'namespace' => 'Sylius\\Bundle\\PayumBundle',
                ],
                'SyliusThemeBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/theme-bundle/src'),
                    'namespace' => 'Sylius\\Bundle\\ThemeBundle',
                ],
                'SyliusAdminBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AdminBundle'),
                    'namespace' => 'Sylius\\Bundle\\AdminBundle',
                ],
                'SyliusShopBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/ShopBundle'),
                    'namespace' => 'Sylius\\Bundle\\ShopBundle',
                ],
                'FOSOAuthServerBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/friendsofsymfony/oauth-server-bundle'),
                    'namespace' => 'FOS\\OAuthServerBundle',
                ],
                'SyliusAdminApiBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/AdminApiBundle'),
                    'namespace' => 'Sylius\\Bundle\\AdminApiBundle',
                ],
                'OmniSyliusBannerPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-banner-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\BannerPlugin',
                ],
                'OmniSyliusSearchPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-search-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\SearchPlugin',
                ],
                'WebpackEncoreBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/symfony/webpack-encore-bundle/src'),
                    'namespace' => 'Symfony\\WebpackEncoreBundle',
                ],
                'OmniSyliusFilterPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-filter-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\FilterPlugin',
                ],
                'OmniSyliusCorePlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-core-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\CorePlugin',
                ],
                'OmniSyliusCmsPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-cms-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\CmsPlugin',
                ],
                'OmniSyliusSeoPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-seo-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\SeoPlugin',
                ],
                'BitBagSyliusShippingExportPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/bitbag/shipping-export-plugin/src'),
                    'namespace' => 'BitBag\\SyliusShippingExportPlugin',
                ],
                'OmniSyliusShippingPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-shipping-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\ShippingPlugin',
                ],
                'HttplugBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/php-http/httplug-bundle/src'),
                    'namespace' => 'Http\\HttplugBundle',
                ],
                'OmniSyliusDpdPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-dpd-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\DpdPlugin',
                ],
                'OmniSyliusParcelMachinePlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-parcel-machine-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\ParcelMachinePlugin',
                ],
                'OmniSyliusPayseraPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-paysera-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\PayseraPlugin',
                ],
                'FOSSyliusImportExportPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/friendsofsylius/sylius-import-export-plugin/src'),
                    'namespace' => 'FriendsOfSylius\\SyliusImportExportPlugin',
                ],
                'OmniSyliusSwedbankSppPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-swedbank-spp-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\SwedbankSpp',
                ],
                'SitemapPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/stefandoorn/sitemap-plugin/src'),
                    'namespace' => 'SitemapPlugin',
                ],
                'Brille24SyliusTierPricePlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/brille24/sylius-tierprice-plugin/src'),
                    'namespace' => 'Brille24\\SyliusTierPricePlugin',
                ],
                'HWIOAuthBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/hwi/oauth-bundle'),
                    'namespace' => 'HWI\\Bundle\\OAuthBundle',
                ],
                'OmniSyliusManifestPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-manifest-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\ManifestPlugin',
                ],
                'KnpSnappyBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/knplabs/knp-snappy-bundle/src'),
                    'namespace' => 'Knp\\Bundle\\SnappyBundle',
                ],
                'LexikTranslationBundle' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/lexik/translation-bundle'),
                    'namespace' => 'Lexik\\Bundle\\TranslationBundle',
                ],
                'OmniSyliusTranslatorPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-translator-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\TranslatorPlugin',
                ],
                'OmniSyliusImportPlugin' => [
                    'path' => (\dirname(__DIR__, 4).'/vendor/omni/sylius-import-plugin/src'),
                    'namespace' => 'Omni\\Sylius\\ImportPlugin',
                ],
            ],
            'kernel.charset' => 'UTF-8',
            'kernel.container_class' => 'srcApp_KernelProdDebugContainer',
            'container.dumper.inline_class_loader' => true,
            'sylius.uploader.filesystem' => 'sylius_image',
            'sylius_core.public_dir' => (\dirname(__DIR__, 4).'/public'),
            'env(DATABASE_URL)' => '',
            'omni_sylius.model.import_job.class' => 'Omni\\Sylius\\ImportPlugin\\Model\\ImportJob',
            'omni_sylius.model.export_job.class' => 'Omni\\Sylius\\ImportPlugin\\Model\\ExportJob',
            'omni_sylius.model.manifest.class' => 'App\\Entity\\Shipping\\Manifest',
            'bitbag.shipping_gateway.validation_groups' => [
                0 => 'bitbag',
            ],
            'bitbag.shipping_labels_path' => (\dirname(__DIR__, 4).'/src/../shipping_labels'),
            'sylius.security.admin_regex' => '^/admin',
            'sylius.security.api_regex' => '^/api',
            'sylius.security.shop_regex' => '^/(?!admin|api/.*|api$|media/.*)[^/]++',
            'env(URL_SCHEME)' => 'http',
            'sylius.form.type.product_image.validation_groups' => [
                0 => 'app_product',
            ],
            'sylius.form.type.product.validation_groups' => [
                0 => 'app_product',
            ],
            'event_dispatcher.event_aliases' => [
                'Symfony\\Component\\Console\\Event\\ConsoleCommandEvent' => 'console.command',
                'Symfony\\Component\\Console\\Event\\ConsoleErrorEvent' => 'console.error',
                'Symfony\\Component\\Console\\Event\\ConsoleTerminateEvent' => 'console.terminate',
                'Symfony\\Component\\Form\\Event\\PreSubmitEvent' => 'form.pre_submit',
                'Symfony\\Component\\Form\\Event\\SubmitEvent' => 'form.submit',
                'Symfony\\Component\\Form\\Event\\PostSubmitEvent' => 'form.post_submit',
                'Symfony\\Component\\Form\\Event\\PreSetDataEvent' => 'form.pre_set_data',
                'Symfony\\Component\\Form\\Event\\PostSetDataEvent' => 'form.post_set_data',
                'Symfony\\Component\\HttpKernel\\Event\\ControllerArgumentsEvent' => 'kernel.controller_arguments',
                'Symfony\\Component\\HttpKernel\\Event\\ControllerEvent' => 'kernel.controller',
                'Symfony\\Component\\HttpKernel\\Event\\ResponseEvent' => 'kernel.response',
                'Symfony\\Component\\HttpKernel\\Event\\FinishRequestEvent' => 'kernel.finish_request',
                'Symfony\\Component\\HttpKernel\\Event\\RequestEvent' => 'kernel.request',
                'Symfony\\Component\\HttpKernel\\Event\\ViewEvent' => 'kernel.view',
                'Symfony\\Component\\HttpKernel\\Event\\ExceptionEvent' => 'kernel.exception',
                'Symfony\\Component\\HttpKernel\\Event\\TerminateEvent' => 'kernel.terminate',
                'Symfony\\Component\\Workflow\\Event\\GuardEvent' => 'workflow.guard',
                'Symfony\\Component\\Workflow\\Event\\LeaveEvent' => 'workflow.leave',
                'Symfony\\Component\\Workflow\\Event\\TransitionEvent' => 'workflow.transition',
                'Symfony\\Component\\Workflow\\Event\\EnterEvent' => 'workflow.enter',
                'Symfony\\Component\\Workflow\\Event\\EnteredEvent' => 'workflow.entered',
                'Symfony\\Component\\Workflow\\Event\\CompletedEvent' => 'workflow.completed',
                'Symfony\\Component\\Workflow\\Event\\AnnounceEvent' => 'workflow.announce',
                'Symfony\\Component\\Security\\Core\\Event\\AuthenticationSuccessEvent' => 'security.authentication.success',
                'Symfony\\Component\\Security\\Core\\Event\\AuthenticationFailureEvent' => 'security.authentication.failure',
                'Symfony\\Component\\Security\\Http\\Event\\InteractiveLoginEvent' => 'security.interactive_login',
                'Symfony\\Component\\Security\\Http\\Event\\SwitchUserEvent' => 'security.switch_user',
            ],
            'fragment.renderer.hinclude.global_template' => NULL,
            'fragment.path' => '/_fragment',
            'kernel.http_method_override' => true,
            'kernel.trusted_hosts' => [

            ],
            'kernel.error_controller' => 'error_controller',
            'templating.helper.code.file_link_format' => NULL,
            'debug.file_link_format' => NULL,
            'session.metadata.storage_key' => '_sf2_meta',
            'session.storage.options' => [
                'cache_limiter' => '0',
                'cookie_httponly' => true,
                'gc_probability' => 1,
            ],
            'session.metadata.update_threshold' => 0,
            'form.type_extension.csrf.enabled' => true,
            'form.type_extension.csrf.field_name' => '_token',
            'asset.request_context.base_path' => '',
            'asset.request_context.secure' => false,
            'templating.loader.cache.path' => NULL,
            'templating.engines' => [
                0 => 'twig',
            ],
            'validator.translation_domain' => 'validators',
            'translator.logging' => false,
            'translator.default_path' => (\dirname(__DIR__, 4).'/translations'),
            'data_collector.templates' => [

            ],
            'debug.error_handler.throw_at' => -1,
            'router.request_context.host' => 'localhost',
            'router.request_context.scheme' => 'http',
            'router.request_context.base_url' => '',
            'router.resource' => 'kernel::loadRoutes',
            'router.cache_class_prefix' => 'srcApp_KernelProdDebugContainer',
            'request_listener.http_port' => 80,
            'request_listener.https_port' => 443,
            'monolog.use_microseconds' => true,
            'monolog.swift_mailer.handlers' => [

            ],
            'monolog.handlers_to_channels' => [
                'monolog.handler.console' => [
                    'type' => 'exclusive',
                    'elements' => [
                        0 => 'event',
                        1 => 'doctrine',
                    ],
                ],
                'monolog.handler.deprecation_filter' => [
                    'type' => 'inclusive',
                    'elements' => [
                        0 => 'php',
                    ],
                ],
                'monolog.handler.main' => [
                    'type' => 'exclusive',
                    'elements' => [
                        0 => 'swedbank_payment',
                        1 => 'integrations',
                        2 => 'request',
                        3 => 'security',
                    ],
                ],
                'monolog.handler.security' => [
                    'type' => 'inclusive',
                    'elements' => [
                        0 => 'security',
                    ],
                ],
                'monolog.handler.requests' => [
                    'type' => 'inclusive',
                    'elements' => [
                        0 => 'request',
                    ],
                ],
                'monolog.handler.integrations' => [
                    'type' => 'inclusive',
                    'elements' => [
                        0 => 'integrations',
                    ],
                ],
                'monolog.handler.payments' => [
                    'type' => 'inclusive',
                    'elements' => [
                        0 => 'swedbank_payment',
                    ],
                ],
            ],
            'security.authentication.trust_resolver.anonymous_class' => NULL,
            'security.authentication.trust_resolver.rememberme_class' => NULL,
            'security.role_hierarchy.roles' => [

            ],
            'security.access.denied_url' => NULL,
            'security.authentication.manager.erase_credentials' => true,
            'security.authentication.session_strategy.strategy' => 'migrate',
            'security.access.always_authenticate_before_granting' => true,
            'security.authentication.hide_user_not_found' => true,
            'hwi_oauth.resource_ownermap.configured.shop' => [
                'facebook' => '/login/check-facebook',
                'google' => '/login/check-google',
            ],
            'swiftmailer.mailer.default.transport.name' => 'dynamic',
            'swiftmailer.mailer.default.spool.enabled' => false,
            'swiftmailer.mailer.default.plugin.impersonate' => NULL,
            'swiftmailer.mailer.default.single_address' => NULL,
            'swiftmailer.mailer.default.delivery.enabled' => true,
            'swiftmailer.spool.enabled' => false,
            'swiftmailer.delivery.enabled' => true,
            'swiftmailer.single_address' => NULL,
            'swiftmailer.mailers' => [
                'default' => 'swiftmailer.mailer.default',
            ],
            'swiftmailer.default_mailer' => 'default',
            'twig.exception_listener.controller' => 'twig.controller.exception::showAction',
            'twig.form.resources' => [
                0 => 'form_div_layout.html.twig',
                1 => '@LiipImagine/Form/form_div_layout.html.twig',
            ],
            'twig.default_path' => (\dirname(__DIR__, 4).'/templates'),
            'doctrine_cache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrine_cache.apcu.class' => 'Doctrine\\Common\\Cache\\ApcuCache',
            'doctrine_cache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrine_cache.chain.class' => 'Doctrine\\Common\\Cache\\ChainCache',
            'doctrine_cache.couchbase.class' => 'Doctrine\\Common\\Cache\\CouchbaseCache',
            'doctrine_cache.couchbase.connection.class' => 'Couchbase',
            'doctrine_cache.couchbase.hostnames' => 'localhost:8091',
            'doctrine_cache.file_system.class' => 'Doctrine\\Common\\Cache\\FilesystemCache',
            'doctrine_cache.php_file.class' => 'Doctrine\\Common\\Cache\\PhpFileCache',
            'doctrine_cache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrine_cache.memcache.connection.class' => 'Memcache',
            'doctrine_cache.memcache.host' => 'localhost',
            'doctrine_cache.memcache.port' => 11211,
            'doctrine_cache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrine_cache.memcached.connection.class' => 'Memcached',
            'doctrine_cache.memcached.host' => 'localhost',
            'doctrine_cache.memcached.port' => 11211,
            'doctrine_cache.mongodb.class' => 'Doctrine\\Common\\Cache\\MongoDBCache',
            'doctrine_cache.mongodb.collection.class' => 'MongoCollection',
            'doctrine_cache.mongodb.connection.class' => 'MongoClient',
            'doctrine_cache.mongodb.server' => 'localhost:27017',
            'doctrine_cache.predis.client.class' => 'Predis\\Client',
            'doctrine_cache.predis.scheme' => 'tcp',
            'doctrine_cache.predis.host' => 'localhost',
            'doctrine_cache.predis.port' => 6379,
            'doctrine_cache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrine_cache.redis.connection.class' => 'Redis',
            'doctrine_cache.redis.host' => 'localhost',
            'doctrine_cache.redis.port' => 6379,
            'doctrine_cache.riak.class' => 'Doctrine\\Common\\Cache\\RiakCache',
            'doctrine_cache.riak.bucket.class' => 'Riak\\Bucket',
            'doctrine_cache.riak.connection.class' => 'Riak\\Connection',
            'doctrine_cache.riak.bucket_property_list.class' => 'Riak\\BucketPropertyList',
            'doctrine_cache.riak.host' => 'localhost',
            'doctrine_cache.riak.port' => 8087,
            'doctrine_cache.sqlite3.class' => 'Doctrine\\Common\\Cache\\SQLite3Cache',
            'doctrine_cache.sqlite3.connection.class' => 'SQLite3',
            'doctrine_cache.void.class' => 'Doctrine\\Common\\Cache\\VoidCache',
            'doctrine_cache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrine_cache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrine_cache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrine_cache.security.acl.cache.class' => 'Doctrine\\Bundle\\DoctrineCacheBundle\\Acl\\Model\\AclCache',
            'doctrine.dbal.logger.chain.class' => 'Doctrine\\DBAL\\Logging\\LoggerChain',
            'doctrine.dbal.logger.profiling.class' => 'Doctrine\\DBAL\\Logging\\DebugStack',
            'doctrine.dbal.logger.class' => 'Symfony\\Bridge\\Doctrine\\Logger\\DbalLogger',
            'doctrine.dbal.configuration.class' => 'Doctrine\\DBAL\\Configuration',
            'doctrine.data_collector.class' => 'Doctrine\\Bundle\\DoctrineBundle\\DataCollector\\DoctrineDataCollector',
            'doctrine.dbal.connection.event_manager.class' => 'Symfony\\Bridge\\Doctrine\\ContainerAwareEventManager',
            'doctrine.dbal.connection_factory.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ConnectionFactory',
            'doctrine.dbal.events.mysql_session_init.class' => 'Doctrine\\DBAL\\Event\\Listeners\\MysqlSessionInit',
            'doctrine.dbal.events.oracle_session_init.class' => 'Doctrine\\DBAL\\Event\\Listeners\\OracleSessionInit',
            'doctrine.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Registry',
            'doctrine.entity_managers' => [
                'default' => 'doctrine.orm.default_entity_manager',
            ],
            'doctrine.default_entity_manager' => 'default',
            'doctrine.dbal.connection_factory.types' => [

            ],
            'doctrine.connections' => [
                'default' => 'doctrine.dbal.default_connection',
            ],
            'doctrine.default_connection' => 'default',
            'doctrine.orm.configuration.class' => 'Doctrine\\ORM\\Configuration',
            'doctrine.orm.entity_manager.class' => 'Doctrine\\ORM\\EntityManager',
            'doctrine.orm.manager_configurator.class' => 'Doctrine\\Bundle\\DoctrineBundle\\ManagerConfigurator',
            'doctrine.orm.cache.array.class' => 'Doctrine\\Common\\Cache\\ArrayCache',
            'doctrine.orm.cache.apc.class' => 'Doctrine\\Common\\Cache\\ApcCache',
            'doctrine.orm.cache.memcache.class' => 'Doctrine\\Common\\Cache\\MemcacheCache',
            'doctrine.orm.cache.memcache_host' => 'localhost',
            'doctrine.orm.cache.memcache_port' => 11211,
            'doctrine.orm.cache.memcache_instance.class' => 'Memcache',
            'doctrine.orm.cache.memcached.class' => 'Doctrine\\Common\\Cache\\MemcachedCache',
            'doctrine.orm.cache.memcached_host' => 'localhost',
            'doctrine.orm.cache.memcached_port' => 11211,
            'doctrine.orm.cache.memcached_instance.class' => 'Memcached',
            'doctrine.orm.cache.redis.class' => 'Doctrine\\Common\\Cache\\RedisCache',
            'doctrine.orm.cache.redis_host' => 'localhost',
            'doctrine.orm.cache.redis_port' => 6379,
            'doctrine.orm.cache.redis_instance.class' => 'Redis',
            'doctrine.orm.cache.xcache.class' => 'Doctrine\\Common\\Cache\\XcacheCache',
            'doctrine.orm.cache.wincache.class' => 'Doctrine\\Common\\Cache\\WinCacheCache',
            'doctrine.orm.cache.zenddata.class' => 'Doctrine\\Common\\Cache\\ZendDataCache',
            'doctrine.orm.metadata.driver_chain.class' => 'Doctrine\\Persistence\\Mapping\\Driver\\MappingDriverChain',
            'doctrine.orm.metadata.annotation.class' => 'Doctrine\\ORM\\Mapping\\Driver\\AnnotationDriver',
            'doctrine.orm.metadata.xml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedXmlDriver',
            'doctrine.orm.metadata.yml.class' => 'Doctrine\\ORM\\Mapping\\Driver\\SimplifiedYamlDriver',
            'doctrine.orm.metadata.php.class' => 'Doctrine\\ORM\\Mapping\\Driver\\PHPDriver',
            'doctrine.orm.metadata.staticphp.class' => 'Doctrine\\ORM\\Mapping\\Driver\\StaticPHPDriver',
            'doctrine.orm.proxy_cache_warmer.class' => 'Symfony\\Bridge\\Doctrine\\CacheWarmer\\ProxyCacheWarmer',
            'form.type_guesser.doctrine.class' => 'Symfony\\Bridge\\Doctrine\\Form\\DoctrineOrmTypeGuesser',
            'doctrine.orm.validator.unique.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\Constraints\\UniqueEntityValidator',
            'doctrine.orm.validator_initializer.class' => 'Symfony\\Bridge\\Doctrine\\Validator\\DoctrineInitializer',
            'doctrine.orm.security.user.provider.class' => 'Symfony\\Bridge\\Doctrine\\Security\\User\\EntityUserProvider',
            'doctrine.orm.listeners.resolve_target_entity.class' => 'Doctrine\\ORM\\Tools\\ResolveTargetEntityListener',
            'doctrine.orm.listeners.attach_entity_listeners.class' => 'Doctrine\\ORM\\Tools\\AttachEntityListenersListener',
            'doctrine.orm.naming_strategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultNamingStrategy',
            'doctrine.orm.naming_strategy.underscore.class' => 'Doctrine\\ORM\\Mapping\\UnderscoreNamingStrategy',
            'doctrine.orm.quote_strategy.default.class' => 'Doctrine\\ORM\\Mapping\\DefaultQuoteStrategy',
            'doctrine.orm.quote_strategy.ansi.class' => 'Doctrine\\ORM\\Mapping\\AnsiQuoteStrategy',
            'doctrine.orm.entity_listener_resolver.class' => 'Doctrine\\Bundle\\DoctrineBundle\\Mapping\\ContainerEntityListenerResolver',
            'doctrine.orm.second_level_cache.default_cache_factory.class' => 'Doctrine\\ORM\\Cache\\DefaultCacheFactory',
            'doctrine.orm.second_level_cache.default_region.class' => 'Doctrine\\ORM\\Cache\\Region\\DefaultRegion',
            'doctrine.orm.second_level_cache.filelock_region.class' => 'Doctrine\\ORM\\Cache\\Region\\FileLockRegion',
            'doctrine.orm.second_level_cache.logger_chain.class' => 'Doctrine\\ORM\\Cache\\Logging\\CacheLoggerChain',
            'doctrine.orm.second_level_cache.logger_statistics.class' => 'Doctrine\\ORM\\Cache\\Logging\\StatisticsCacheLogger',
            'doctrine.orm.second_level_cache.cache_configuration.class' => 'Doctrine\\ORM\\Cache\\CacheConfiguration',
            'doctrine.orm.second_level_cache.regions_configuration.class' => 'Doctrine\\ORM\\Cache\\RegionsConfiguration',
            'doctrine.orm.auto_generate_proxy_classes' => true,
            'doctrine.orm.proxy_namespace' => 'Proxies',
            'sylius.repository.order.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\OrderRepository',
            'sylius.repository.order_item.class' => 'Sylius\\Bundle\\OrderBundle\\Doctrine\\ORM\\OrderItemRepository',
            'sylius_order.driver.doctrine/orm' => true,
            'sylius_order.driver' => 'doctrine/orm',
            'sylius.resources' => [
                'sylius.order' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\Order',
                        'controller' => 'App\\Controller\\OrderController',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\OrderRepository',
                        'interface' => 'Sylius\\Component\\Order\\Model\\OrderInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\OrderBundle\\Form\\Type\\OrderType',
                    ],
                ],
                'sylius.order_item' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\OrderItem',
                        'controller' => 'App\\Controller\\OrderItemController',
                        'interface' => 'Sylius\\Component\\Order\\Model\\OrderItemInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\OrderBundle\\Form\\Type\\OrderItemType',
                    ],
                ],
                'sylius.order_item_unit' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\OrderItemUnit',
                        'interface' => 'Sylius\\Component\\Order\\Model\\OrderItemUnitInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Order\\Factory\\OrderItemUnitFactory',
                    ],
                ],
                'sylius.order_sequence' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\OrderSequence',
                        'interface' => 'Sylius\\Component\\Order\\Model\\OrderSequenceInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.adjustment' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\Adjustment',
                        'interface' => 'Sylius\\Component\\Order\\Model\\AdjustmentInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.currency' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Currency\\Currency',
                        'interface' => 'Sylius\\Component\\Currency\\Model\\CurrencyInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\CurrencyBundle\\Form\\Type\\CurrencyType',
                    ],
                ],
                'sylius.exchange_rate' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Currency\\ExchangeRate',
                        'interface' => 'Sylius\\Component\\Currency\\Model\\ExchangeRateInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\CurrencyBundle\\Form\\Type\\ExchangeRateType',
                    ],
                ],
                'sylius.locale' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Locale\\Locale',
                        'interface' => 'Sylius\\Component\\Locale\\Model\\LocaleInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\LocaleBundle\\Form\\Type\\LocaleType',
                    ],
                ],
                'sylius.product' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\Product',
                        'repository' => 'App\\Repository\\Product\\ProductRepository',
                        'controller' => 'Omni\\Sylius\\FilterPlugin\\Controller\\ProductController',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductTranslation',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductTranslationType',
                        ],
                    ],
                ],
                'sylius.product_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductTranslation',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductTranslationType',
                    ],
                ],
                'sylius.product_variant' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductVariant',
                        'controller' => 'Sylius\\Bundle\\CoreBundle\\Controller\\ProductVariantController',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductVariantRepository',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductVariantInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductVariantType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductVariantTranslation',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductVariantTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductVariantTranslationType',
                        ],
                    ],
                ],
                'sylius.product_variant_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductVariantTranslation',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductVariantTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductVariantTranslationType',
                    ],
                ],
                'sylius.product_option' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductOptionRepository',
                        'model' => 'App\\Entity\\Product\\ProductOption',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductOptionTranslation',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionTranslationType',
                        ],
                    ],
                ],
                'sylius.product_option_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductOptionTranslation',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionTranslationType',
                    ],
                ],
                'sylius.product_association_type' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'repository' => 'Sylius\\Bundle\\ProductBundle\\Doctrine\\ORM\\ProductAssociationTypeRepository',
                        'model' => 'App\\Entity\\Product\\ProductAssociationType',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationTypeInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationTypeType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductAssociationTypeTranslation',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationTypeTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationTypeTranslationType',
                        ],
                    ],
                ],
                'sylius.product_association_type_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductAssociationTypeTranslation',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationTypeTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationTypeTranslationType',
                    ],
                ],
                'sylius.product_option_value' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductOptionValue',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionValueInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionValueType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductOptionValueTranslation',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionValueTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionValueTranslationType',
                        ],
                    ],
                ],
                'sylius.product_option_value_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductOptionValueTranslation',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductOptionValueTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductOptionValueTranslationType',
                    ],
                ],
                'sylius.product_association' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductAssociation',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductAssociationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAssociationType',
                    ],
                ],
                'sylius.channel' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Channel\\Channel',
                        'interface' => 'Sylius\\Component\\Channel\\Model\\ChannelInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ChannelBundle\\Form\\Type\\ChannelType',
                    ],
                ],
                'sylius.product_attribute' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductAttribute',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeInterface',
                        'controller' => 'Sylius\\Bundle\\ProductBundle\\Controller\\ProductAttributeController',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeType',
                        'repository' => 'Omni\\Sylius\\FilterPlugin\\Doctrine\\ORM\\ProductAttributeRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductAttributeTranslation',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeTranslationInterface',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeTranslationType',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                    ],
                ],
                'sylius.product_attribute_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductAttributeTranslation',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeTranslationInterface',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeTranslationType',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.product_attribute_value' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductAttributeValue',
                        'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeValueInterface',
                        'repository' => 'Sylius\\Bundle\\ProductBundle\\Doctrine\\ORM\\ProductAttributeValueRepository',
                        'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeValueType',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.tax_rate' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Taxation\\TaxRate',
                        'interface' => 'Sylius\\Component\\Taxation\\Model\\TaxRateInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\TaxationBundle\\Form\\Type\\TaxRateType',
                    ],
                ],
                'sylius.tax_category' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Taxation\\TaxCategory',
                        'interface' => 'Sylius\\Component\\Taxation\\Model\\TaxCategoryInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\TaxationBundle\\Form\\Type\\TaxCategoryType',
                    ],
                ],
                'sylius.shipment' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Shipping\\Shipment',
                        'repository' => 'App\\Repository\\Shipping\\ShipmentRepository',
                        'interface' => 'Sylius\\Component\\Shipping\\Model\\ShipmentInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShipmentType',
                    ],
                ],
                'sylius.shipment_unit' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\OrderItemUnit',
                        'interface' => 'Sylius\\Component\\Shipping\\Model\\ShipmentUnitInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.shipping_method' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Shipping\\ShippingMethod',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ShippingMethodRepository',
                        'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingMethodInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingMethodType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Shipping\\ShippingMethodTranslation',
                            'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingMethodTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingMethodTranslationType',
                        ],
                    ],
                ],
                'sylius.shipping_method_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Shipping\\ShippingMethodTranslation',
                        'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingMethodTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingMethodTranslationType',
                    ],
                ],
                'sylius.shipping_category' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ShippingCategoryRepository',
                        'model' => 'App\\Entity\\Shipping\\ShippingCategory',
                        'interface' => 'Sylius\\Component\\Shipping\\Model\\ShippingCategoryInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ShippingBundle\\Form\\Type\\ShippingCategoryType',
                    ],
                ],
                'sylius.payment' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Payment\\Payment',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PaymentRepository',
                        'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentType',
                    ],
                ],
                'sylius.payment_method' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Payment\\PaymentMethod',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PaymentMethodRepository',
                        'controller' => 'Sylius\\Bundle\\CoreBundle\\Controller\\PaymentMethodController',
                        'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentMethodInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentMethodType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Payment\\PaymentMethodTranslation',
                            'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentMethodTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentMethodTranslationType',
                        ],
                    ],
                ],
                'sylius.payment_method_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Payment\\PaymentMethodTranslation',
                        'interface' => 'Sylius\\Component\\Payment\\Model\\PaymentMethodTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\PaymentBundle\\Form\\Type\\PaymentMethodTranslationType',
                    ],
                ],
                'sylius.promotion_subject' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\Order',
                    ],
                ],
                'sylius.promotion' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Promotion\\Promotion',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PromotionRepository',
                        'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionType',
                    ],
                ],
                'sylius.promotion_coupon' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Promotion\\PromotionCoupon',
                        'repository' => 'Sylius\\Bundle\\PromotionBundle\\Doctrine\\ORM\\PromotionCouponRepository',
                        'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionCouponInterface',
                        'controller' => 'Sylius\\Bundle\\PromotionBundle\\Controller\\PromotionCouponController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionCouponType',
                    ],
                ],
                'sylius.promotion_rule' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Promotion\\PromotionRule',
                        'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionRuleInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionRuleType',
                    ],
                ],
                'sylius.promotion_action' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Promotion\\PromotionAction',
                        'interface' => 'Sylius\\Component\\Promotion\\Model\\PromotionActionInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\PromotionBundle\\Form\\Type\\PromotionActionType',
                    ],
                ],
                'sylius.address' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Addressing\\Address',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\AddressRepository',
                        'interface' => 'Sylius\\Component\\Addressing\\Model\\AddressInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\AddressType',
                    ],
                ],
                'sylius.country' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Addressing\\Country',
                        'interface' => 'Sylius\\Component\\Addressing\\Model\\CountryInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\CountryType',
                    ],
                ],
                'sylius.province' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Addressing\\Province',
                        'interface' => 'Sylius\\Component\\Addressing\\Model\\ProvinceInterface',
                        'controller' => 'Sylius\\Bundle\\AddressingBundle\\Controller\\ProvinceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\ProvinceType',
                    ],
                ],
                'sylius.zone' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Addressing\\Zone',
                        'interface' => 'Sylius\\Component\\Addressing\\Model\\ZoneInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\ZoneType',
                    ],
                ],
                'sylius.zone_member' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Addressing\\ZoneMember',
                        'interface' => 'Sylius\\Component\\Addressing\\Model\\ZoneMemberInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\AddressingBundle\\Form\\Type\\ZoneMemberType',
                    ],
                ],
                'sylius.address_log_entry' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Sylius\\Component\\Addressing\\Model\\AddressLogEntry',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'repository' => 'Sylius\\Bundle\\ResourceBundle\\Doctrine\\ORM\\ResourceLogEntryRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.inventory_unit' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\OrderItemUnit',
                        'interface' => 'Sylius\\Component\\Inventory\\Model\\InventoryUnitInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.taxon' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Taxonomy\\Taxon',
                        'repository' => 'App\\Repository\\Taxon\\TaxonRepository',
                        'interface' => 'Sylius\\Component\\Taxonomy\\Model\\TaxonInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        'form' => 'Sylius\\Bundle\\TaxonomyBundle\\Form\\Type\\TaxonType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Taxonomy\\TaxonTranslation',
                            'interface' => 'Sylius\\Component\\Taxonomy\\Model\\TaxonTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Sylius\\Bundle\\TaxonomyBundle\\Form\\Type\\TaxonTranslationType',
                        ],
                    ],
                ],
                'sylius.taxon_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Taxonomy\\TaxonTranslation',
                        'interface' => 'Sylius\\Component\\Taxonomy\\Model\\TaxonTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\TaxonomyBundle\\Form\\Type\\TaxonTranslationType',
                    ],
                ],
                'sylius.admin_user' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\User\\AdminUser',
                        'interface' => 'Sylius\\Component\\Core\\Model\\AdminUserInterface',
                        'repository' => 'Sylius\\Bundle\\UserBundle\\Doctrine\\ORM\\UserRepository',
                        'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\User\\AdminUserType',
                        'controller' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'templates' => 'SyliusUserBundle:User',
                    'encoder' => NULL,
                    'resetting' => [
                        'token' => [
                            'ttl' => 'P1D',
                            'length' => 16,
                            'field_name' => 'passwordResetToken',
                        ],
                        'pin' => [
                            'length' => 4,
                            'field_name' => 'passwordResetToken',
                        ],
                    ],
                    'verification' => [
                        'token' => [
                            'length' => 16,
                            'field_name' => 'emailVerificationToken',
                        ],
                    ],
                ],
                'sylius.shop_user' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\User\\ShopUser',
                        'interface' => 'Sylius\\Component\\Core\\Model\\ShopUserInterface',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\UserRepository',
                        'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\User\\ShopUserType',
                        'controller' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'templates' => 'SyliusUserBundle:User',
                    'encoder' => NULL,
                    'resetting' => [
                        'token' => [
                            'ttl' => 'P1D',
                            'length' => 16,
                            'field_name' => 'passwordResetToken',
                        ],
                        'pin' => [
                            'length' => 4,
                            'field_name' => 'passwordResetToken',
                        ],
                    ],
                    'verification' => [
                        'token' => [
                            'length' => 16,
                            'field_name' => 'emailVerificationToken',
                        ],
                    ],
                ],
                'sylius.oauth_user' => [
                    'driver' => 'doctrine/orm',
                    'encoder' => false,
                    'classes' => [
                        'model' => 'App\\Entity\\User\\UserOAuth',
                        'interface' => 'Sylius\\Component\\User\\Model\\UserOAuthInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'templates' => 'SyliusUserBundle:User',
                    'resetting' => [
                        'token' => [
                            'ttl' => 'P1D',
                            'length' => 16,
                            'field_name' => 'passwordResetToken',
                        ],
                        'pin' => [
                            'length' => 4,
                            'field_name' => 'passwordResetToken',
                        ],
                    ],
                    'verification' => [
                        'token' => [
                            'length' => 16,
                            'field_name' => 'emailVerificationToken',
                        ],
                    ],
                ],
                'sylius.customer' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Customer\\Customer',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\CustomerRepository',
                        'interface' => 'Sylius\\Component\\Customer\\Model\\CustomerInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\CustomerBundle\\Form\\Type\\CustomerType',
                    ],
                ],
                'sylius.customer_group' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Customer\\CustomerGroup',
                        'interface' => 'Sylius\\Component\\Customer\\Model\\CustomerGroupInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\CustomerBundle\\Form\\Type\\CustomerGroupType',
                    ],
                ],
                'sylius.product_review' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductReview',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductReviewRepository',
                        'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\Product\\ProductReviewType',
                        'interface' => 'Sylius\\Component\\Review\\Model\\ReviewInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.product_reviewer' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Customer\\Customer',
                        'interface' => 'Sylius\\Component\\Review\\Model\\ReviewerInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.product_taxon' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductTaxonRepository',
                        'model' => 'App\\Entity\\Product\\ProductTaxon',
                        'interface' => 'Sylius\\Component\\Core\\Model\\ProductTaxonInterface',
                        'controller' => 'Sylius\\Bundle\\CoreBundle\\Controller\\ProductTaxonController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.product_image' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Product\\ProductImage',
                        'interface' => 'Sylius\\Component\\Core\\Model\\ProductImageInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.taxon_image' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Taxonomy\\TaxonImage',
                        'interface' => 'Sylius\\Component\\Core\\Model\\TaxonImageInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.channel_pricing' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Channel\\ChannelPricing',
                        'interface' => 'Sylius\\Component\\Core\\Model\\ChannelPricingInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\Product\\ChannelPricingType',
                    ],
                ],
                'sylius.avatar_image' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Sylius\\Component\\Core\\Model\\AvatarImage',
                        'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\AvatarImageRepository',
                    ],
                ],
                'bitbag.shipping_export' => [
                    'classes' => [
                        'model' => 'App\\Entity\\Shipping\\ShippingExport',
                        'interface' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingExportInterface',
                        'repository' => 'BitBag\\SyliusShippingExportPlugin\\Repository\\ShippingExportRepository',
                        'form' => 'BitBag\\SyliusShippingExportPlugin\\Form\\Type\\ShippingGatewayType',
                        'controller' => 'BitBag\\SyliusShippingExportPlugin\\Controller\\ShippingExportController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'driver' => 'doctrine/orm',
                ],
                'bitbag.shipping_gateway' => [
                    'classes' => [
                        'model' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingGateway',
                        'interface' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingGatewayInterface',
                        'repository' => 'BitBag\\SyliusShippingExportPlugin\\Repository\\ShippingGatewayRepository',
                        'form' => 'BitBag\\SyliusShippingExportPlugin\\Form\\Type\\ShippingGatewayType',
                        'controller' => 'BitBag\\SyliusShippingExportPlugin\\Controller\\ShippingGatewayController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'driver' => 'doctrine/orm',
                ],
                'nfq.consent' => [
                    'classes' => [
                        'model' => 'App\\Entity\\Consent\\Consent',
                        'repository' => 'App\\Repository\\Consent\\ConsentRepository',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\ResourceBundle\\Form\\Type\\DefaultResourceType',
                    ],
                    'driver' => 'doctrine/orm',
                ],
                'brille24.tierprice' => [
                    'classes' => [
                        'model' => 'Brille24\\SyliusTierPricePlugin\\Entity\\TierPrice',
                        'form' => 'Brille24\\SyliusTierPricePlugin\\Form\\TierPriceType',
                        'repository' => 'Brille24\\SyliusTierPricePlugin\\Repository\\TierPriceRepository',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'driver' => 'doctrine/orm',
                ],
                'app.order_report' => [
                    'classes' => [
                        'model' => 'App\\Entity\\Order\\OrderReport',
                        'form' => 'App\\Form\\Type\\OrderReportType',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'driver' => 'doctrine/orm',
                ],
                'sylius.payment_security_token' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Payment\\PaymentSecurityToken',
                        'interface' => 'Sylius\\Bundle\\PayumBundle\\Model\\PaymentSecurityTokenInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.gateway_config' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Payment\\GatewayConfig',
                        'interface' => 'Sylius\\Bundle\\PayumBundle\\Model\\GatewayConfigInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\PayumBundle\\Form\\Type\\GatewayConfigType',
                    ],
                ],
                'sylius.api_user' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\User\\AdminUser',
                        'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\UserInterface',
                    ],
                ],
                'sylius.api_client' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\AdminApi\\Client',
                        'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\ClientInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Sylius\\Bundle\\AdminApiBundle\\Form\\Type\\ClientType',
                    ],
                ],
                'sylius.api_access_token' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\AdminApi\\AccessToken',
                        'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\AccessTokenInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.api_refresh_token' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\AdminApi\\RefreshToken',
                        'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\RefreshTokenInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'sylius.api_auth_code' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\AdminApi\\AuthCode',
                        'interface' => 'Sylius\\Bundle\\AdminApiBundle\\Model\\AuthCodeInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni_sylius.banner' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\Banner',
                        'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'repository' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerType',
                    ],
                ],
                'omni_sylius.banner_zone' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZone',
                        'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'repository' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerZoneRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerZoneType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslation',
                            'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslationInterface',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                    ],
                ],
                'omni_sylius.banner_zone_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslation',
                        'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslationInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni_sylius.banner_position' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPosition',
                        'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'repository' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerPositionRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerPositionType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslation',
                            'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslationInterface',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                    ],
                ],
                'omni_sylius.banner_position_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslation',
                        'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslationInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni_sylius.banner_image' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImage',
                        'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\BannerPlugin\\Form\\Type\\BannerImageType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslation',
                            'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslationInterface',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                    ],
                ],
                'omni_sylius.banner_image_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslation',
                        'interface' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslationInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni_sylius.search_index' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\SearchPlugin\\Model\\SearchIndex',
                        'interface' => 'Omni\\Sylius\\SearchPlugin\\Model\\SearchIndexInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni_sylius.channel_watermark' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelWatermark',
                        'interface' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelWatermarkInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\CorePlugin\\Form\\Type\\ChannelWatermarkType',
                    ],
                ],
                'omni_sylius.channel_logo' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelLogo',
                        'interface' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelLogoInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\CorePlugin\\Form\\Type\\ChannelLogoType',
                    ],
                ],
                'omni_sylius.node_image' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImage',
                        'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslation',
                            'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeImageTranslationType',
                        ],
                    ],
                ],
                'omni_sylius.node_image_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslation',
                        'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeImageTranslationType',
                    ],
                ],
                'omni_sylius.node' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\CmsPlugin\\Model\\Node',
                        'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeInterface',
                        'controller' => 'Omni\\Sylius\\CmsPlugin\\Controller\\NodeController',
                        'repository' => 'Omni\\Sylius\\CmsPlugin\\Doctrine\\ORM\\NodeRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeType',
                    ],
                    'translation' => [
                        'classes' => [
                            'model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslation',
                            'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslationInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeTranslationType',
                        ],
                    ],
                ],
                'omni_sylius.node_translation' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslation',
                        'interface' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslationInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\CmsPlugin\\Form\\Type\\NodeTranslationType',
                    ],
                ],
                'omni_sylius.seo_metadata' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\SeoPlugin\\Model\\SeoMetadata',
                        'interface' => 'Omni\\Sylius\\SeoPlugin\\Model\\SeoMetadataInterface',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\SeoPlugin\\Form\\Type\\SeoMetadataType',
                    ],
                ],
                'omni_sylius.shipper_config' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Shipping\\ShipperConfig',
                        'repository' => 'App\\Repository\\Shipping\\ShipperConfigRepository',
                        'interface' => 'Omni\\Sylius\\ShippingPlugin\\Model\\ShipperConfigInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni.parcel_machine' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\ParcelMachinePlugin\\Model\\ParcelMachine',
                        'interface' => 'Omni\\Sylius\\ParcelMachinePlugin\\Model\\ParcelMachineInterface',
                        'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                        'repository' => 'Omni\\Sylius\\ParcelMachinePlugin\\Doctrine\\ORM\\ParcelMachineRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni_sylius.manifest' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'App\\Entity\\Shipping\\Manifest',
                        'interface' => 'Omni\\Sylius\\ManifestPlugin\\Model\\ManifestInterface',
                        'controller' => 'Omni\\Sylius\\ManifestPlugin\\Controller\\ManifestController',
                        'repository' => 'Omni\\Sylius\\ManifestPlugin\\Doctrine\\ORM\\ManifestRepository',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                    ],
                ],
                'omni_sylius.import_job' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\ImportPlugin\\Model\\ImportJob',
                        'interface' => 'Omni\\Sylius\\ImportPlugin\\Model\\ImportJobInterface',
                        'controller' => 'Omni\\Sylius\\ImportPlugin\\Controller\\ImportJobController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\ImportPlugin\\Form\\Type\\ImportJobType',
                    ],
                ],
                'omni_sylius.export_job' => [
                    'driver' => 'doctrine/orm',
                    'classes' => [
                        'model' => 'Omni\\Sylius\\ImportPlugin\\Model\\ExportJob',
                        'interface' => 'Omni\\Sylius\\ImportPlugin\\Model\\ExportJobInterface',
                        'controller' => 'Omni\\Sylius\\ImportPlugin\\Controller\\ExportJobController',
                        'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        'form' => 'Omni\\Sylius\\ImportPlugin\\Form\\Type\\ExportJobType',
                    ],
                ],
            ],
            'sylius.model.order.class' => 'App\\Entity\\Order\\Order',
            'sylius.controller.order.class' => 'App\\Controller\\OrderController',
            'sylius.factory.order.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.order_item.class' => 'App\\Entity\\Order\\OrderItem',
            'sylius.controller.order_item.class' => 'App\\Controller\\OrderItemController',
            'sylius.factory.order_item.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.order_item_unit.class' => 'App\\Entity\\Order\\OrderItemUnit',
            'sylius.controller.order_item_unit.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.order_item_unit.class' => 'Sylius\\Component\\Order\\Factory\\OrderItemUnitFactory',
            'sylius.model.order_sequence.class' => 'App\\Entity\\Order\\OrderSequence',
            'sylius.factory.order_sequence.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.adjustment.class' => 'App\\Entity\\Order\\Adjustment',
            'sylius.controller.adjustment.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.adjustment.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.order.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.order_item.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.cart.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.cart_item.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius_order.cart_expiration_period' => '2 days',
            'sylius_order.order_expiration_period' => '5 days',
            'sylius.repository.exchange_rate.class' => 'Sylius\\Bundle\\CurrencyBundle\\Doctrine\\ORM\\ExchangeRateRepository',
            'sylius_currency.driver.doctrine/orm' => true,
            'sylius_currency.driver' => 'doctrine/orm',
            'sylius.model.currency.class' => 'App\\Entity\\Currency\\Currency',
            'sylius.controller.currency.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.currency.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.exchange_rate.class' => 'App\\Entity\\Currency\\ExchangeRate',
            'sylius.controller.exchange_rate.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.exchange_rate.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.currency.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.exchange_rate.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius_locale.driver.doctrine/orm' => true,
            'sylius_locale.driver' => 'doctrine/orm',
            'sylius.model.locale.class' => 'App\\Entity\\Locale\\Locale',
            'sylius.controller.locale.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.locale.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.locale.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.repository.product.class' => 'App\\Repository\\Product\\ProductRepository',
            'sylius.repository.product_variant.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductVariantRepository',
            'sylius.repository.product_attribute.class' => 'Omni\\Sylius\\FilterPlugin\\Doctrine\\ORM\\ProductAttributeRepository',
            'sylius.repository.product_option.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductOptionRepository',
            'sylius_product.driver.doctrine/orm' => true,
            'sylius_product.driver' => 'doctrine/orm',
            'sylius.model.product.class' => 'App\\Entity\\Product\\Product',
            'sylius.controller.product.class' => 'Omni\\Sylius\\FilterPlugin\\Controller\\ProductController',
            'sylius.factory.product.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.model.product_translation.class' => 'App\\Entity\\Product\\ProductTranslation',
            'sylius.controller.product_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.product_variant.class' => 'App\\Entity\\Product\\ProductVariant',
            'sylius.controller.product_variant.class' => 'Sylius\\Bundle\\CoreBundle\\Controller\\ProductVariantController',
            'sylius.factory.product_variant.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.model.product_variant_translation.class' => 'App\\Entity\\Product\\ProductVariantTranslation',
            'sylius.controller.product_variant_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_variant_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.product_option.class' => 'App\\Entity\\Product\\ProductOption',
            'sylius.controller.product_option.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_option.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.model.product_option_translation.class' => 'App\\Entity\\Product\\ProductOptionTranslation',
            'sylius.controller.product_option_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_option_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.product_association_type.class' => 'App\\Entity\\Product\\ProductAssociationType',
            'sylius.controller.product_association_type.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_association_type.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.repository.product_association_type.class' => 'Sylius\\Bundle\\ProductBundle\\Doctrine\\ORM\\ProductAssociationTypeRepository',
            'sylius.model.product_association_type_translation.class' => 'App\\Entity\\Product\\ProductAssociationTypeTranslation',
            'sylius.controller.product_association_type_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_association_type_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.product_option_value.class' => 'App\\Entity\\Product\\ProductOptionValue',
            'sylius.controller.product_option_value.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_option_value.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.model.product_option_value_translation.class' => 'App\\Entity\\Product\\ProductOptionValueTranslation',
            'sylius.controller.product_option_value_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_option_value_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.product_association.class' => 'App\\Entity\\Product\\ProductAssociation',
            'sylius.controller.product_association.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_association.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.product_association.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_association_type.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_association_type_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_attribute.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_attribute_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_attribute_value.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_generate_variants.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_option.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_option_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_option_value.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_option_value_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_variant.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_variant_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_variant_generation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.repository.channel.class' => 'Sylius\\Bundle\\ChannelBundle\\Doctrine\\ORM\\ChannelRepository',
            'sylius_channel.driver.doctrine/orm' => true,
            'sylius_channel.driver' => 'doctrine/orm',
            'sylius.model.channel.class' => 'App\\Entity\\Channel\\Channel',
            'sylius.controller.channel.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.channel.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.channel.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.model.attribute.interface' => 'Sylius\\Component\\Attribute\\AttributeType\\AttributeTypeInterface',
            'sylius.attribute.subjects' => [
                'product' => [
                    'subject' => 'App\\Entity\\Product\\Product',
                    'attribute' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductAttribute',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeInterface',
                            'controller' => 'Sylius\\Bundle\\ProductBundle\\Controller\\ProductAttributeController',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeType',
                            'repository' => 'Omni\\Sylius\\FilterPlugin\\Doctrine\\ORM\\ProductAttributeRepository',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
                        ],
                        'translation' => [
                            'classes' => [
                                'model' => 'App\\Entity\\Product\\ProductAttributeTranslation',
                                'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeTranslationInterface',
                                'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeTranslationType',
                                'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                                'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                            ],
                        ],
                    ],
                    'attribute_value' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductAttributeValue',
                            'interface' => 'Sylius\\Component\\Product\\Model\\ProductAttributeValueInterface',
                            'repository' => 'Sylius\\Bundle\\ProductBundle\\Doctrine\\ORM\\ProductAttributeValueRepository',
                            'form' => 'Sylius\\Bundle\\ProductBundle\\Form\\Type\\ProductAttributeValueType',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                    ],
                ],
            ],
            'sylius_attribute.driver.doctrine/orm' => true,
            'sylius_attribute.driver' => 'doctrine/orm',
            'sylius.model.product_attribute.class' => 'App\\Entity\\Product\\ProductAttribute',
            'sylius.controller.product_attribute.class' => 'Sylius\\Bundle\\ProductBundle\\Controller\\ProductAttributeController',
            'sylius.factory.product_attribute.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.model.product_attribute_translation.class' => 'App\\Entity\\Product\\ProductAttributeTranslation',
            'sylius.controller.product_attribute_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_attribute_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.product_attribute_value.class' => 'App\\Entity\\Product\\ProductAttributeValue',
            'sylius.controller.product_attribute_value.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_attribute_value.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.product_attribute_value.class' => 'Sylius\\Bundle\\ProductBundle\\Doctrine\\ORM\\ProductAttributeValueRepository',
            'sylius.repository.tax_category.class' => 'Sylius\\Bundle\\TaxationBundle\\Doctrine\\ORM\\TaxCategoryRepository',
            'sylius_taxation.driver.doctrine/orm' => true,
            'sylius_taxation.driver' => 'doctrine/orm',
            'sylius.model.tax_rate.class' => 'App\\Entity\\Taxation\\TaxRate',
            'sylius.controller.tax_rate.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.tax_rate.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.tax_category.class' => 'App\\Entity\\Taxation\\TaxCategory',
            'sylius.controller.tax_category.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.tax_category.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.tax_category.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.tax_rate.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.tax_calculator.interface' => 'Sylius\\Component\\Taxation\\Calculator\\CalculatorInterface',
            'sylius.repository.shipping_method.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ShippingMethodRepository',
            'sylius_shipping.driver.doctrine/orm' => true,
            'sylius_shipping.driver' => 'doctrine/orm',
            'sylius.model.shipment.class' => 'App\\Entity\\Shipping\\Shipment',
            'sylius.controller.shipment.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.shipment.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.shipment.class' => 'App\\Repository\\Shipping\\ShipmentRepository',
            'sylius.model.shipment_unit.class' => 'App\\Entity\\Order\\OrderItemUnit',
            'sylius.controller.shipment_unit.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.shipment_unit.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.shipping_method.class' => 'App\\Entity\\Shipping\\ShippingMethod',
            'sylius.controller.shipping_method.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.shipping_method.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.model.shipping_method_translation.class' => 'App\\Entity\\Shipping\\ShippingMethodTranslation',
            'sylius.controller.shipping_method_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.shipping_method_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.shipping_category.class' => 'App\\Entity\\Shipping\\ShippingCategory',
            'sylius.controller.shipping_category.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.shipping_category.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.shipping_category.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ShippingCategoryRepository',
            'sylius.form.type.shipping_method.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.shipping_method_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.shipping_category.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.shipment.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.shipment_ship.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.shipping_methods_resolver.interface' => 'Sylius\\Component\\Shipping\\Resolver\\ShippingMethodsResolverInterface',
            'sylius_payment.driver.doctrine/orm' => true,
            'sylius_payment.driver' => 'doctrine/orm',
            'sylius.model.payment.class' => 'App\\Entity\\Payment\\Payment',
            'sylius.controller.payment.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.payment.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.payment.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PaymentRepository',
            'sylius.model.payment_method.class' => 'App\\Entity\\Payment\\PaymentMethod',
            'sylius.controller.payment_method.class' => 'Sylius\\Bundle\\CoreBundle\\Controller\\PaymentMethodController',
            'sylius.factory.payment_method.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.repository.payment_method.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PaymentMethodRepository',
            'sylius.model.payment_method_translation.class' => 'App\\Entity\\Payment\\PaymentMethodTranslation',
            'sylius.controller.payment_method_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.payment_method_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.payment.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.payment_method.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.payment_method_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.payment_methods_resolver.interface' => 'Sylius\\Component\\Payment\\Resolver\\PaymentMethodsResolverInterface',
            'sylius.payment_gateways' => [
                'offline' => 'sylius.payum_gateway.offline',
            ],
            'sylius.mailer.emails' => [
                'shipment_confirmation' => [
                    'subject' => 'app.shipment_confirmation',
                    'template' => 'Email/shipmentConfirmation.html.twig',
                    'enabled' => true,
                ],
                'contact_request' => [
                    'subject' => 'sylius.emails.contact_request.subject',
                    'template' => '@SyliusShop/Email/contactRequest.html.twig',
                    'enabled' => true,
                ],
                'order_confirmation' => [
                    'subject' => 'sylius.emails.order_confirmation.subject',
                    'template' => 'Email/orderConfirmation.html.twig',
                    'enabled' => true,
                ],
                'user_registration' => [
                    'subject' => 'sylius.emails.user_registration.subject',
                    'template' => 'Email/userRegistration.html.twig',
                    'enabled' => true,
                ],
                'password_reset' => [
                    'subject' => 'sylius.emails.user.password_reset.subject',
                    'template' => '@SyliusShop/Email/passwordReset.html.twig',
                    'enabled' => true,
                ],
                'reset_password_token' => [
                    'subject' => 'sylius.emails.user.password_reset.subject',
                    'template' => 'Email/passwordReset.html.twig',
                    'enabled' => true,
                ],
                'reset_password_pin' => [
                    'subject' => 'sylius.emails.user.password_reset.subject',
                    'template' => '@SyliusShop/Email/passwordReset.html.twig',
                    'enabled' => true,
                ],
                'verification_token' => [
                    'subject' => 'sylius.emails.user.verification_token.subject',
                    'template' => 'Email/verification.html.twig',
                    'enabled' => true,
                ],
                'quote' => [
                    'subject' => 'New Quote',
                    'template' => 'Email/quote.html.twig',
                    'enabled' => true,
                ],
                'payment_received' => [
                    'subject' => 'app.shipment_ready',
                    'template' => 'Email/payment_received.html.twig',
                    'enabled' => true,
                ],
                'shipment_ready' => [
                    'subject' => 'app.shipment_ready',
                    'template' => 'Email/shipment_ready.html.twig',
                    'enabled' => true,
                ],
            ],
            'sylius.mailer.templates' => [

            ],
            'sylius.form.type.promotion.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.promotion_action.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.promotion_rule.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.promotion_coupon.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.repository.promotion.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\PromotionRepository',
            'sylius.repository.promotion_coupon.class' => 'Sylius\\Bundle\\PromotionBundle\\Doctrine\\ORM\\PromotionCouponRepository',
            'sylius_promotion.driver.doctrine/orm' => true,
            'sylius_promotion.driver' => 'doctrine/orm',
            'sylius.model.promotion_subject.class' => 'App\\Entity\\Order\\Order',
            'sylius.model.promotion.class' => 'App\\Entity\\Promotion\\Promotion',
            'sylius.controller.promotion.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.promotion.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.promotion_coupon.class' => 'App\\Entity\\Promotion\\PromotionCoupon',
            'sylius.controller.promotion_coupon.class' => 'Sylius\\Bundle\\PromotionBundle\\Controller\\PromotionCouponController',
            'sylius.factory.promotion_coupon.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.promotion_rule.class' => 'App\\Entity\\Promotion\\PromotionRule',
            'sylius.controller.promotion_rule.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.promotion_rule.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.promotion_action.class' => 'App\\Entity\\Promotion\\PromotionAction',
            'sylius.controller.promotion_action.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.promotion_action.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius_addressing.driver.doctrine/orm' => true,
            'sylius_addressing.driver' => 'doctrine/orm',
            'sylius.model.address.class' => 'App\\Entity\\Addressing\\Address',
            'sylius.controller.address.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.address.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.address.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\AddressRepository',
            'sylius.model.country.class' => 'App\\Entity\\Addressing\\Country',
            'sylius.controller.country.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.country.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.province.class' => 'App\\Entity\\Addressing\\Province',
            'sylius.controller.province.class' => 'Sylius\\Bundle\\AddressingBundle\\Controller\\ProvinceController',
            'sylius.factory.province.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.zone.class' => 'App\\Entity\\Addressing\\Zone',
            'sylius.controller.zone.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.zone.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.zone_member.class' => 'App\\Entity\\Addressing\\ZoneMember',
            'sylius.controller.zone_member.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.zone_member.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.address_log_entry.class' => 'Sylius\\Component\\Addressing\\Model\\AddressLogEntry',
            'sylius.controller.address_log_entry.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.address_log_entry.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.address_log_entry.class' => 'Sylius\\Bundle\\ResourceBundle\\Doctrine\\ORM\\ResourceLogEntryRepository',
            'sylius.form.type.address.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.country.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.province.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.zone.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.zone_member.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.scope.zone' => [
                'shipping' => 'sylius.form.zone.scopes.shipping',
                'tax' => 'sylius.form.zone.scopes.tax',
                'all' => 'sylius.form.zone.scopes.all',
            ],
            'sylius_inventory.driver.doctrine/orm' => true,
            'sylius_inventory.driver' => 'doctrine/orm',
            'sylius.model.inventory_unit.class' => 'App\\Entity\\Order\\OrderItemUnit',
            'sylius.controller.inventory_unit.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.inventory_unit.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.taxon.class' => 'App\\Repository\\Taxon\\TaxonRepository',
            'sylius_taxonomy.driver.doctrine/orm' => true,
            'sylius_taxonomy.driver' => 'doctrine/orm',
            'sylius.model.taxon.class' => 'App\\Entity\\Taxonomy\\Taxon',
            'sylius.controller.taxon.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.taxon.class' => 'Sylius\\Component\\Resource\\Factory\\TranslatableFactory',
            'sylius.model.taxon_translation.class' => 'App\\Entity\\Taxonomy\\TaxonTranslation',
            'sylius.controller.taxon_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.taxon_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.taxon.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.taxon_translation.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.taxon_position.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.repository.user.class' => 'Sylius\\Bundle\\UserBundle\\Doctrine\\ORM\\UserRepository',
            'sylius.user.users' => [
                'admin' => [
                    'user' => [
                        'classes' => [
                            'model' => 'App\\Entity\\User\\AdminUser',
                            'interface' => 'Sylius\\Component\\Core\\Model\\AdminUserInterface',
                            'repository' => 'Sylius\\Bundle\\UserBundle\\Doctrine\\ORM\\UserRepository',
                            'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\User\\AdminUserType',
                            'controller' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                        'templates' => 'SyliusUserBundle:User',
                        'encoder' => NULL,
                        'resetting' => [
                            'token' => [
                                'ttl' => 'P1D',
                                'length' => 16,
                                'field_name' => 'passwordResetToken',
                            ],
                            'pin' => [
                                'length' => 4,
                                'field_name' => 'passwordResetToken',
                            ],
                        ],
                        'verification' => [
                            'token' => [
                                'length' => 16,
                                'field_name' => 'emailVerificationToken',
                            ],
                        ],
                    ],
                ],
                'shop' => [
                    'user' => [
                        'classes' => [
                            'model' => 'App\\Entity\\User\\ShopUser',
                            'interface' => 'Sylius\\Component\\Core\\Model\\ShopUserInterface',
                            'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\UserRepository',
                            'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\User\\ShopUserType',
                            'controller' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                        'templates' => 'SyliusUserBundle:User',
                        'encoder' => NULL,
                        'resetting' => [
                            'token' => [
                                'ttl' => 'P1D',
                                'length' => 16,
                                'field_name' => 'passwordResetToken',
                            ],
                            'pin' => [
                                'length' => 4,
                                'field_name' => 'passwordResetToken',
                            ],
                        ],
                        'verification' => [
                            'token' => [
                                'length' => 16,
                                'field_name' => 'emailVerificationToken',
                            ],
                        ],
                    ],
                ],
                'oauth' => [
                    'user' => [
                        'encoder' => false,
                        'classes' => [
                            'model' => 'App\\Entity\\User\\UserOAuth',
                            'interface' => 'Sylius\\Component\\User\\Model\\UserOAuthInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                        'templates' => 'SyliusUserBundle:User',
                        'resetting' => [
                            'token' => [
                                'ttl' => 'P1D',
                                'length' => 16,
                                'field_name' => 'passwordResetToken',
                            ],
                            'pin' => [
                                'length' => 4,
                                'field_name' => 'passwordResetToken',
                            ],
                        ],
                        'verification' => [
                            'token' => [
                                'length' => 16,
                                'field_name' => 'emailVerificationToken',
                            ],
                        ],
                    ],
                ],
            ],
            'sylius_user.driver.doctrine/orm' => true,
            'sylius_user.driver' => 'doctrine/orm',
            'sylius.model.admin_user.class' => 'App\\Entity\\User\\AdminUser',
            'sylius.controller.admin_user.class' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController',
            'sylius.factory.admin_user.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.admin_user.class' => 'Sylius\\Bundle\\UserBundle\\Doctrine\\ORM\\UserRepository',
            'sylius.model.shop_user.class' => 'App\\Entity\\User\\ShopUser',
            'sylius.controller.shop_user.class' => 'Sylius\\Bundle\\UserBundle\\Controller\\UserController',
            'sylius.factory.shop_user.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.shop_user.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\UserRepository',
            'sylius.model.oauth_user.class' => 'App\\Entity\\User\\UserOAuth',
            'sylius.controller.oauth_user.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.oauth_user.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.user_request_password_reset.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.user_reset_password.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.user_change_password.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius_customer.driver.doctrine/orm' => true,
            'sylius_customer.driver' => 'doctrine/orm',
            'sylius.model.customer.class' => 'App\\Entity\\Customer\\Customer',
            'sylius.controller.customer.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.customer.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.customer.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\CustomerRepository',
            'sylius.model.customer_group.class' => 'App\\Entity\\Customer\\CustomerGroup',
            'sylius.controller.customer_group.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.customer_group.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.customer.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.customer_profile.validation_groups' => [
                0 => 'sylius',
                1 => 'sylius_customer_profile',
            ],
            'sylius.form.type.customer_group.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.review.subjects' => [
                'product' => [
                    'subject' => 'App\\Entity\\Product\\Product',
                    'review' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Product\\ProductReview',
                            'repository' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductReviewRepository',
                            'form' => 'Sylius\\Bundle\\CoreBundle\\Form\\Type\\Product\\ProductReviewType',
                            'interface' => 'Sylius\\Component\\Review\\Model\\ReviewInterface',
                            'controller' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                    ],
                    'reviewer' => [
                        'classes' => [
                            'model' => 'App\\Entity\\Customer\\Customer',
                            'interface' => 'Sylius\\Component\\Review\\Model\\ReviewerInterface',
                            'factory' => 'Sylius\\Component\\Resource\\Factory\\Factory',
                        ],
                    ],
                ],
            ],
            'sylius_review.driver.doctrine/orm' => true,
            'sylius_review.driver' => 'doctrine/orm',
            'sylius.model.product_review.class' => 'App\\Entity\\Product\\ProductReview',
            'sylius.controller.product_review.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_review.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.product_review.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductReviewRepository',
            'sylius.model.product_reviewer.class' => 'App\\Entity\\Customer\\Customer',
            'sylius.factory.product_reviewer.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius_core.driver.doctrine/orm' => true,
            'sylius_core.driver' => 'doctrine/orm',
            'sylius.model.product_taxon.class' => 'App\\Entity\\Product\\ProductTaxon',
            'sylius.controller.product_taxon.class' => 'Sylius\\Bundle\\CoreBundle\\Controller\\ProductTaxonController',
            'sylius.factory.product_taxon.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.repository.product_taxon.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\ProductTaxonRepository',
            'sylius.model.product_image.class' => 'App\\Entity\\Product\\ProductImage',
            'sylius.controller.product_image.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.product_image.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.taxon_image.class' => 'App\\Entity\\Taxonomy\\TaxonImage',
            'sylius.controller.taxon_image.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.taxon_image.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.channel_pricing.class' => 'App\\Entity\\Channel\\ChannelPricing',
            'sylius.controller.channel_pricing.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.channel_pricing.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.avatar_image.class' => 'Sylius\\Component\\Core\\Model\\AvatarImage',
            'sylius.repository.avatar_image.class' => 'Sylius\\Bundle\\CoreBundle\\Doctrine\\ORM\\AvatarImageRepository',
            'sylius.form.type.checkout_address.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.checkout_select_shipping.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.checkout_shipment.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.checkout_select_payment.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.checkout_payment.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.checkout_complete.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.product_review.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.admin_user.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.shop_user.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.shop_user_registration.validation_groups' => [
                0 => 'sylius',
                1 => 'sylius_user_registration',
            ],
            'sylius.form.type.customer_guest.validation_groups' => [
                0 => 'sylius_customer_guest',
            ],
            'sylius.form.type.customer_simple_registration.validation_groups' => [
                0 => 'sylius',
                1 => 'sylius_user_registration',
            ],
            'sylius.form.type.customer_registration.validation_groups' => [
                0 => 'sylius',
                1 => 'sylius_user_registration',
                2 => 'sylius_customer_profile',
            ],
            'sylius.form.type.add_to_cart.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.channel_pricing.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.model.shop_billing_data.class' => 'Sylius\\Component\\Core\\Model\\ShopBillingData',
            'sylius.tax_calculation_strategy.interface' => 'Sylius\\Component\\Core\\Taxation\\Strategy\\TaxCalculationStrategyInterface',
            'sylius.state_machine.class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
            'sylius.resource.settings' => [
                'paginate' => NULL,
                'limit' => NULL,
                'allowed_paginate' => [
                    0 => 10,
                    1 => 20,
                    2 => 30,
                ],
                'default_page_size' => 10,
                'sortable' => false,
                'sorting' => NULL,
                'filterable' => false,
                'criteria' => NULL,
            ],
            'sylius.orm.repository.class' => 'Sylius\\Bundle\\ResourceBundle\\Doctrine\\ORM\\EntityRepository',
            'sylius.translation.translatable_listener.doctrine.orm.class' => 'Sylius\\Bundle\\ResourceBundle\\EventListener\\ORMTranslatableListener',
            'bitbag.model.shipping_export.class' => 'App\\Entity\\Shipping\\ShippingExport',
            'bitbag.controller.shipping_export.class' => 'BitBag\\SyliusShippingExportPlugin\\Controller\\ShippingExportController',
            'bitbag.factory.shipping_export.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'bitbag.repository.shipping_export.class' => 'BitBag\\SyliusShippingExportPlugin\\Repository\\ShippingExportRepository',
            'bitbag.model.shipping_gateway.class' => 'BitBag\\SyliusShippingExportPlugin\\Entity\\ShippingGateway',
            'bitbag.controller.shipping_gateway.class' => 'BitBag\\SyliusShippingExportPlugin\\Controller\\ShippingGatewayController',
            'bitbag.factory.shipping_gateway.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'bitbag.repository.shipping_gateway.class' => 'BitBag\\SyliusShippingExportPlugin\\Repository\\ShippingGatewayRepository',
            'nfq.model.consent.class' => 'App\\Entity\\Consent\\Consent',
            'nfq.controller.consent.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'nfq.factory.consent.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'nfq.repository.consent.class' => 'App\\Repository\\Consent\\ConsentRepository',
            'brille24.model.tierprice.class' => 'Brille24\\SyliusTierPricePlugin\\Entity\\TierPrice',
            'brille24.controller.tierprice.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'brille24.factory.tierprice.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'brille24.repository.tierprice.class' => 'Brille24\\SyliusTierPricePlugin\\Repository\\TierPriceRepository',
            'app.model.order_report.class' => 'App\\Entity\\Order\\OrderReport',
            'app.controller.order_report.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'app.factory.order_report.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.grid.templates.action' => [
                'default' => '@SyliusUi/Grid/Action/default.html.twig',
                'create' => '@SyliusUi/Grid/Action/create.html.twig',
                'delete' => '@SyliusUi/Grid/Action/delete.html.twig',
                'show' => '@SyliusUi/Grid/Action/show.html.twig',
                'update' => '@SyliusUi/Grid/Action/update.html.twig',
                'apply_transition' => '@SyliusUi/Grid/Action/applyTransition.html.twig',
                'links' => '@SyliusUi/Grid/Action/links.html.twig',
                'archive' => '@SyliusUi/Grid/Action/archive.html.twig',
                'create_payment_method' => '@SyliusAdmin/PaymentMethod/Grid/Action/create.html.twig',
                'create_product_attribute' => '@SyliusAdmin/ProductAttribute/Grid/Action/create.html.twig',
                'generate_variants' => '@SyliusAdmin/Product/Grid/Action/generateVariants.html.twig',
                'update_product_positions' => '@SyliusAdmin/Product/Grid/Action/updatePositions.html.twig',
                'update_product_variant_positions' => '@SyliusAdmin/ProductVariant/Grid/Action/updatePositions.html.twig',
                'pay' => '@SyliusShop/Account/Order/Grid/Action/_pay_deprecated.html.twig',
                'shop_show' => '@SyliusShop/Grid/Action/show.html.twig',
                'shop_pay' => '@SyliusShop/Account/Order/Grid/Action/pay.html.twig',
                'download_manifest' => '@OmniSyliusManifestPlugin/Manifest/Action/_download_single.html.twig',
                'create_shipping_gateway' => '@BitBagSyliusShippingExportPlugin/ShippingGateway/Action/_create.html.twig',
                'export_shipments' => '@BitBagSyliusShippingExportPlugin/ShippingExport/Action/_exportShipments.html.twig',
            ],
            'sylius.grid.templates.bulk_action' => [
                'delete' => '@SyliusUi/Grid/BulkAction/delete.html.twig',
            ],
            'sylius.grid.templates.filter' => [
                'string' => '@SyliusUi/Grid/Filter/string.html.twig',
                'boolean' => '@SyliusUi/Grid/Filter/boolean.html.twig',
                'date' => '@SyliusUi/Grid/Filter/date.html.twig',
                'entity' => '@SyliusUi/Grid/Filter/entity.html.twig',
                'money' => '@SyliusUi/Grid/Filter/money.html.twig',
                'exists' => '@SyliusUi/Grid/Filter/exists.html.twig',
                'select' => '@SyliusUi/Grid/Filter/select.html.twig',
                'shop_string' => '@SyliusShop/Grid/Filter/string.html.twig',
                'omni_product_attribute' => '@OmniSyliusFilterPlugin/Grid/Filter/product_attribute.html.twig',
                'omni_price' => '@OmniSyliusFilterPlugin/Grid/Filter/price.html.twig',
            ],
            'sm.configs' => [
                'sylius_order_checkout' => [
                    'class' => 'App\\Entity\\Order\\Order',
                    'property_path' => 'checkoutState',
                    'graph' => 'sylius_order_checkout',
                    'state_machine_class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
                    'states' => [
                        0 => 'cart',
                        1 => 'addressed',
                        2 => 'shipping_selected',
                        3 => 'shipping_skipped',
                        4 => 'payment_skipped',
                        5 => 'payment_selected',
                        6 => 'completed',
                    ],
                    'transitions' => [
                        'address' => [
                            'from' => [
                                0 => 'cart',
                                1 => 'addressed',
                                2 => 'shipping_selected',
                                3 => 'shipping_skipped',
                                4 => 'payment_selected',
                                5 => 'payment_skipped',
                            ],
                            'to' => 'addressed',
                        ],
                        'skip_shipping' => [
                            'from' => [
                                0 => 'addressed',
                            ],
                            'to' => 'shipping_skipped',
                        ],
                        'select_shipping' => [
                            'from' => [
                                0 => 'addressed',
                                1 => 'shipping_selected',
                                2 => 'payment_selected',
                                3 => 'payment_skipped',
                            ],
                            'to' => 'shipping_selected',
                        ],
                        'skip_payment' => [
                            'from' => [
                                0 => 'shipping_selected',
                                1 => 'shipping_skipped',
                            ],
                            'to' => 'payment_skipped',
                        ],
                        'select_payment' => [
                            'from' => [
                                0 => 'payment_selected',
                                1 => 'shipping_skipped',
                                2 => 'shipping_selected',
                            ],
                            'to' => 'payment_selected',
                        ],
                        'complete' => [
                            'from' => [
                                0 => 'payment_selected',
                                1 => 'payment_skipped',
                            ],
                            'to' => 'completed',
                        ],
                    ],
                    'callbacks' => [
                        'after' => [
                            'sylius_process_cart' => [
                                'on' => [
                                    0 => 'select_shipping',
                                    1 => 'address',
                                    2 => 'select_payment',
                                    3 => 'skip_shipping',
                                    4 => 'skip_payment',
                                ],
                                'do' => [
                                    0 => '@sylius.order_processing.order_processor',
                                    1 => 'process',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_create_order' => [
                                'on' => [
                                    0 => 'complete',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object',
                                    1 => 'event',
                                    2 => '\'create\'',
                                    3 => '\'sylius_order\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_save_checkout_completion_date' => [
                                'on' => [
                                    0 => 'complete',
                                ],
                                'do' => [
                                    0 => 'object',
                                    1 => 'completeCheckout',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_control_payment_state' => [
                                'on' => [
                                    0 => 'complete',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order_payment',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_control_shipping_state' => [
                                'on' => [
                                    0 => 'complete',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order_shipping',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_skip_shipping' => [
                                'on' => [
                                    0 => 'address',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order_checkout',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'priority' => 1,
                                'disabled' => false,
                            ],
                            'sylius_skip_payment' => [
                                'on' => [
                                    0 => 'select_shipping',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order_checkout',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'priority' => 1,
                                'disabled' => false,
                            ],
                        ],
                        'guard' => [

                        ],
                        'before' => [

                        ],
                    ],
                ],
                'sylius_order_shipping' => [
                    'class' => 'App\\Entity\\Order\\Order',
                    'property_path' => 'shippingState',
                    'graph' => 'sylius_order_shipping',
                    'state_machine_class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
                    'states' => [
                        0 => 'cart',
                        1 => 'ready',
                        2 => 'cancelled',
                        3 => 'partially_shipped',
                        4 => 'shipped',
                        5 => 'packed',
                        6 => 'exported',
                    ],
                    'transitions' => [
                        'request_shipping' => [
                            'from' => [
                                0 => 'cart',
                                1 => 'cart',
                            ],
                            'to' => 'ready',
                        ],
                        'cancel' => [
                            'from' => [
                                0 => 'ready',
                                1 => 'ready',
                            ],
                            'to' => 'cancelled',
                        ],
                        'partially_ship' => [
                            'from' => [
                                0 => 'ready',
                                1 => 'ready',
                            ],
                            'to' => 'partially_shipped',
                        ],
                        'ship' => [
                            'from' => [
                                0 => 'ready',
                                1 => 'partially_shipped',
                                2 => 'ready',
                                3 => 'packed',
                                4 => 'exported',
                                5 => 'partially_shipped',
                            ],
                            'to' => 'shipped',
                        ],
                        'pack' => [
                            'from' => [
                                0 => 'ready',
                            ],
                            'to' => 'packed',
                        ],
                        'export' => [
                            'from' => [
                                0 => 'ready',
                            ],
                            'to' => 'exported',
                        ],
                    ],
                    'callbacks' => [
                        'after' => [
                            'sylius_resolve_state' => [
                                'on' => [
                                    0 => 'ship',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                        ],
                        'guard' => [

                        ],
                        'before' => [

                        ],
                    ],
                ],
                'sylius_order_payment' => [
                    'class' => 'App\\Entity\\Order\\Order',
                    'property_path' => 'paymentState',
                    'graph' => 'sylius_order_payment',
                    'state_machine_class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
                    'states' => [
                        0 => 'cart',
                        1 => 'awaiting_payment',
                        2 => 'partially_authorized',
                        3 => 'authorized',
                        4 => 'partially_paid',
                        5 => 'cancelled',
                        6 => 'paid',
                        7 => 'partially_refunded',
                        8 => 'refunded',
                    ],
                    'transitions' => [
                        'request_payment' => [
                            'from' => [
                                0 => 'cart',
                            ],
                            'to' => 'awaiting_payment',
                        ],
                        'partially_authorize' => [
                            'from' => [
                                0 => 'awaiting_payment',
                                1 => 'partially_authorized',
                            ],
                            'to' => 'partially_authorized',
                        ],
                        'authorize' => [
                            'from' => [
                                0 => 'awaiting_payment',
                                1 => 'partially_authorized',
                            ],
                            'to' => 'authorized',
                        ],
                        'partially_pay' => [
                            'from' => [
                                0 => 'awaiting_payment',
                                1 => 'partially_paid',
                                2 => 'partially_authorized',
                            ],
                            'to' => 'partially_paid',
                        ],
                        'cancel' => [
                            'from' => [
                                0 => 'awaiting_payment',
                                1 => 'authorized',
                                2 => 'partially_authorized',
                            ],
                            'to' => 'cancelled',
                        ],
                        'pay' => [
                            'from' => [
                                0 => 'awaiting_payment',
                                1 => 'partially_paid',
                                2 => 'authorized',
                            ],
                            'to' => 'paid',
                        ],
                        'partially_refund' => [
                            'from' => [
                                0 => 'paid',
                                1 => 'partially_paid',
                                2 => 'partially_refunded',
                            ],
                            'to' => 'partially_refunded',
                        ],
                        'refund' => [
                            'from' => [
                                0 => 'paid',
                                1 => 'partially_paid',
                                2 => 'partially_refunded',
                            ],
                            'to' => 'refunded',
                        ],
                    ],
                    'callbacks' => [
                        'after' => [
                            'sylius_order_paid' => [
                                'on' => [
                                    0 => 'pay',
                                ],
                                'do' => [
                                    0 => '@sylius.inventory.order_inventory_operator',
                                    1 => 'sell',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_resolve_state' => [
                                'on' => [
                                    0 => 'pay',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_order_paid_inform' => [
                                'on' => [
                                    0 => 'pay',
                                ],
                                'do' => [
                                    0 => '@App\\EventListener\\Payment\\OrderPaymentListener',
                                    1 => 'onOrderPay',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                        ],
                        'guard' => [

                        ],
                        'before' => [

                        ],
                    ],
                ],
                'sylius_order' => [
                    'class' => 'App\\Entity\\Order\\Order',
                    'property_path' => 'state',
                    'graph' => 'sylius_order',
                    'state_machine_class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
                    'states' => [
                        0 => 'cart',
                        1 => 'new',
                        2 => 'cancelled',
                        3 => 'fulfilled',
                    ],
                    'transitions' => [
                        'create' => [
                            'from' => [
                                0 => 'cart',
                            ],
                            'to' => 'new',
                        ],
                        'cancel' => [
                            'from' => [
                                0 => 'new',
                            ],
                            'to' => 'cancelled',
                        ],
                        'fulfill' => [
                            'from' => [
                                0 => 'new',
                            ],
                            'to' => 'fulfilled',
                        ],
                    ],
                    'callbacks' => [
                        'before' => [
                            'sylius_assign_number' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sylius.order_number_assigner',
                                    1 => 'assignNumber',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_assign_token' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sylius.unique_id_based_order_token_assigner',
                                    1 => 'assignTokenValueIfNotSet',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                        ],
                        'after' => [
                            'sylius_request_shipping' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object',
                                    1 => 'event',
                                    2 => '\'request_shipping\'',
                                    3 => '\'sylius_order_shipping\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_request_payment' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object',
                                    1 => 'event',
                                    2 => '\'request_payment\'',
                                    3 => '\'sylius_order_payment\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_create_payment' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object.getPayments()',
                                    1 => 'event',
                                    2 => '\'create\'',
                                    3 => '\'sylius_payment\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_create_shipment' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object.getShipments()',
                                    1 => 'event',
                                    2 => '\'create\'',
                                    3 => '\'sylius_shipment\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_hold_inventory' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sylius.inventory.order_inventory_operator',
                                    1 => 'hold',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_increment_promotions_usages' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sylius.promotion_usage_modifier',
                                    1 => 'increment',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_save_addresses_on_customer' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sylius.customer_order_addresses_saver',
                                    1 => 'saveAddresses',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_set_order_immutable_names' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => '@sylius.order_item_names_setter',
                                    1 => '__invoke',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_cancel_payment' => [
                                'on' => [
                                    0 => 'cancel',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object.getPayments()',
                                    1 => 'event',
                                    2 => '\'cancel\'',
                                    3 => '\'sylius_payment\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_cancel_shipment' => [
                                'on' => [
                                    0 => 'cancel',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object.getShipments()',
                                    1 => 'event',
                                    2 => '\'cancel\'',
                                    3 => '\'sylius_shipment\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_cancel_order_payment' => [
                                'on' => [
                                    0 => 'cancel',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object',
                                    1 => 'event',
                                    2 => '\'cancel\'',
                                    3 => '\'sylius_order_payment\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_cancel_order_shipment' => [
                                'on' => [
                                    0 => 'cancel',
                                ],
                                'do' => [
                                    0 => '@sm.callback.cascade_transition',
                                    1 => 'apply',
                                ],
                                'args' => [
                                    0 => 'object',
                                    1 => 'event',
                                    2 => '\'cancel\'',
                                    3 => '\'sylius_order_shipping\'',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylis_cancel_order' => [
                                'on' => [
                                    0 => 'cancel',
                                ],
                                'do' => [
                                    0 => '@sylius.inventory.order_inventory_operator',
                                    1 => 'cancel',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_decrement_promotions_usages' => [
                                'on' => [
                                    0 => 'cancel',
                                ],
                                'do' => [
                                    0 => '@sylius.promotion_usage_modifier',
                                    1 => 'decrement',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                        ],
                        'guard' => [

                        ],
                    ],
                ],
                'sylius_shipment' => [
                    'class' => 'App\\Entity\\Shipping\\Shipment',
                    'property_path' => 'state',
                    'graph' => 'sylius_shipment',
                    'state_machine_class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
                    'states' => [
                        0 => 'cart',
                        1 => 'ready',
                        2 => 'shipped',
                        3 => 'cancelled',
                        4 => 'new',
                        5 => 'in_progress',
                        6 => 'pickup_ready',
                        7 => 'courier_ready',
                        8 => 'in_transit',
                        9 => 'cant_deliver',
                        10 => 'back_to_seller',
                        11 => 'error',
                        12 => 'label_generated',
                        13 => 'packed',
                        14 => 'exported',
                    ],
                    'transitions' => [
                        'create' => [
                            'from' => [
                                0 => 'cart',
                                1 => 'error',
                                2 => 'cart',
                                3 => 'cart',
                            ],
                            'to' => 'ready',
                        ],
                        'ship' => [
                            'from' => [
                                0 => 'ready',
                                1 => 'error',
                                2 => 'label_generated',
                                3 => 'in_progress',
                                4 => 'pickup_ready',
                                5 => 'courier_ready',
                                6 => 'ready',
                                7 => 'packed',
                                8 => 'exported',
                            ],
                            'to' => 'shipped',
                        ],
                        'cancel' => [
                            'from' => [
                                0 => 'ready',
                                1 => 'error',
                                2 => 'in_progress',
                                3 => 'pickup_ready',
                                4 => 'courier_ready',
                                5 => 'label_generated',
                                6 => 'ready',
                            ],
                            'to' => 'cancelled',
                        ],
                        'in_progress' => [
                            'from' => [
                                0 => 'error',
                                1 => 'cart',
                                2 => 'new',
                            ],
                            'to' => 'in_progress',
                        ],
                        'pickup_ready' => [
                            'from' => [
                                0 => 'new',
                                1 => 'error',
                                2 => 'cancelled',
                                3 => 'in_progress',
                            ],
                            'to' => 'pickup_ready',
                        ],
                        'shipment_exported' => [
                            'from' => [
                                0 => 'new',
                                1 => 'error',
                                2 => 'cancelled',
                                3 => 'in_progress',
                            ],
                            'to' => 'label_generated',
                        ],
                        'in_transit' => [
                            'from' => [
                                0 => 'error',
                                1 => 'cart',
                                2 => 'new',
                                3 => 'courier_ready',
                                4 => 'pickup_ready',
                                5 => 'cant_deliver',
                                6 => 'back_to_seller',
                                7 => 'shipped',
                                8 => 'cancelled',
                                9 => 'label_generated',
                            ],
                            'to' => 'in_transit',
                        ],
                        'cant_deliver' => [
                            'from' => [
                                0 => 'error',
                                1 => 'cart',
                                2 => 'new',
                                3 => 'courier_ready',
                                4 => 'pickup_ready',
                                5 => 'in_transit',
                                6 => 'back_to_seller',
                                7 => 'shipped',
                                8 => 'cancelled',
                                9 => 'label_generated',
                            ],
                            'to' => 'cant_deliver',
                        ],
                        'back_to_seller' => [
                            'from' => [
                                0 => 'error',
                                1 => 'cart',
                                2 => 'new',
                                3 => 'in_progress',
                                4 => 'courier_ready',
                                5 => 'pickup_ready',
                                6 => 'in_transit',
                                7 => 'cant_deliver',
                                8 => 'shipped',
                                9 => 'cancelled',
                                10 => 'label_generated',
                            ],
                            'to' => 'back_to_seller',
                        ],
                        'shipped' => [
                            'from' => [
                                0 => 'error',
                                1 => 'cart',
                                2 => 'new',
                                3 => 'in_progress',
                                4 => 'courier_ready',
                                5 => 'pickup_ready',
                                6 => 'in_transit',
                                7 => 'cant_deliver',
                                8 => 'back_to_seller',
                                9 => 'cancelled',
                                10 => 'label_generated',
                            ],
                            'to' => 'shipped',
                        ],
                        'cancelled' => [
                            'from' => [
                                0 => 'error',
                                1 => 'cart',
                                2 => 'new',
                                3 => 'in_progress',
                                4 => 'courier_ready',
                                5 => 'pickup_ready',
                                6 => 'in_transit',
                                7 => 'cant_deliver',
                                8 => 'back_to_seller',
                                9 => 'shipped',
                                10 => 'cancelled',
                                11 => 'label_generated',
                            ],
                            'to' => 'cancelled',
                        ],
                        'error' => [
                            'from' => [
                                0 => 'cart',
                                1 => 'new',
                                2 => 'in_progress',
                                3 => 'courier_ready',
                                4 => 'pickup_ready',
                                5 => 'in_transit',
                                6 => 'cant_deliver',
                                7 => 'back_to_seller',
                                8 => 'shipped',
                                9 => 'label_generated',
                            ],
                            'to' => 'error',
                        ],
                        'simple_create' => [
                            'from' => [
                                0 => 'cart',
                            ],
                            'to' => 'new',
                        ],
                        'pack' => [
                            'from' => [
                                0 => 'ready',
                            ],
                            'to' => 'packed',
                        ],
                        'export' => [
                            'from' => [
                                0 => 'ready',
                                1 => 'new',
                            ],
                            'to' => 'exported',
                        ],
                    ],
                    'callbacks' => [
                        'after' => [
                            'sylius_resolve_state' => [
                                'on' => [
                                    0 => 'ship',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order_shipping',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object.getOrder()',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'app_shipment_created' => [
                                'on' => [
                                    0 => 'create',
                                ],
                                'do' => [
                                    0 => 'App\\EventListener\\Shipment\\StatesOnShipmentCreateListener',
                                    1 => 'process',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                        ],
                        'guard' => [

                        ],
                        'before' => [

                        ],
                    ],
                ],
                'sylius_payment' => [
                    'class' => 'App\\Entity\\Payment\\Payment',
                    'property_path' => 'state',
                    'graph' => 'sylius_payment',
                    'state_machine_class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
                    'states' => [
                        0 => 'cart',
                        1 => 'new',
                        2 => 'processing',
                        3 => 'authorized',
                        4 => 'completed',
                        5 => 'failed',
                        6 => 'cancelled',
                        7 => 'refunded',
                    ],
                    'transitions' => [
                        'create' => [
                            'from' => [
                                0 => 'cart',
                            ],
                            'to' => 'new',
                        ],
                        'process' => [
                            'from' => [
                                0 => 'new',
                            ],
                            'to' => 'processing',
                        ],
                        'authorize' => [
                            'from' => [
                                0 => 'new',
                                1 => 'processing',
                            ],
                            'to' => 'authorized',
                        ],
                        'complete' => [
                            'from' => [
                                0 => 'new',
                                1 => 'processing',
                                2 => 'authorized',
                            ],
                            'to' => 'completed',
                        ],
                        'fail' => [
                            'from' => [
                                0 => 'new',
                                1 => 'processing',
                            ],
                            'to' => 'failed',
                        ],
                        'cancel' => [
                            'from' => [
                                0 => 'new',
                                1 => 'processing',
                                2 => 'authorized',
                            ],
                            'to' => 'cancelled',
                        ],
                        'refund' => [
                            'from' => [
                                0 => 'completed',
                            ],
                            'to' => 'refunded',
                        ],
                    ],
                    'callbacks' => [
                        'after' => [
                            'sylius_process_order' => [
                                'on' => [
                                    0 => 'fail',
                                    1 => 'cancel',
                                ],
                                'do' => [
                                    0 => '@sylius.order_processing.order_payment_processor.after_checkout',
                                    1 => 'process',
                                ],
                                'args' => [
                                    0 => 'object.getOrder()',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                            'sylius_resolve_state' => [
                                'on' => [
                                    0 => 'complete',
                                    1 => 'refund',
                                    2 => 'authorize',
                                ],
                                'do' => [
                                    0 => '@sylius.state_resolver.order_payment',
                                    1 => 'resolve',
                                ],
                                'args' => [
                                    0 => 'object.getOrder()',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                        ],
                        'guard' => [

                        ],
                        'before' => [

                        ],
                    ],
                ],
                'sylius_product_review' => [
                    'class' => 'App\\Entity\\Product\\ProductReview',
                    'property_path' => 'status',
                    'graph' => 'sylius_product_review',
                    'state_machine_class' => 'Sylius\\Component\\Resource\\StateMachine\\StateMachine',
                    'states' => [
                        0 => 'new',
                        1 => 'accepted',
                        2 => 'rejected',
                    ],
                    'transitions' => [
                        'accept' => [
                            'from' => [
                                0 => 'new',
                            ],
                            'to' => 'accepted',
                        ],
                        'reject' => [
                            'from' => [
                                0 => 'new',
                            ],
                            'to' => 'rejected',
                        ],
                    ],
                    'callbacks' => [
                        'after' => [
                            'sylius_update_rating' => [
                                'on' => [
                                    0 => 'accept',
                                ],
                                'do' => [
                                    0 => '@sylius.product_review.average_rating_updater',
                                    1 => 'updateFromReview',
                                ],
                                'args' => [
                                    0 => 'object',
                                ],
                                'disabled' => false,
                                'priority' => 0,
                            ],
                        ],
                        'guard' => [

                        ],
                        'before' => [

                        ],
                    ],
                ],
            ],
            'sm.factory.class' => 'SM\\Factory\\Factory',
            'sm.callback_factory.class' => 'winzou\\Bundle\\StateMachineBundle\\Callback\\ContainerAwareCallbackFactory',
            'sm.callback.class' => 'winzou\\Bundle\\StateMachineBundle\\Callback\\ContainerAwareCallback',
            'sm.twig_extension.class' => 'SM\\Extension\\Twig\\SMExtension',
            'sm.callback.cascade_transition.class' => 'SM\\Callback\\CascadeTransitionCallback',
            'sonata.twig.flashmessage.manager.class' => 'Sonata\\Twig\\FlashMessage\\FlashManager',
            'sonata.twig.extension.flashmessage.class' => 'Sonata\\Twig\\Extension\\FlashMessageExtension',
            'sonata.core.form' => [
                'mapping' => [
                    'enabled' => false,
                    'type' => [

                    ],
                    'extension' => [

                    ],
                ],
            ],
            'sonata.core.form_type' => 'standard',
            'sonata.core.flashmessage' => [

            ],
            'sonata.core.serializer' => [
                'formats' => [
                    0 => 'json',
                    1 => 'xml',
                    2 => 'yml',
                ],
            ],
            'sonata.block.service.container.class' => 'Sonata\\BlockBundle\\Block\\Service\\ContainerBlockService',
            'sonata.block.service.empty.class' => 'Sonata\\BlockBundle\\Block\\Service\\EmptyBlockService',
            'sonata.block.service.text.class' => 'Sonata\\BlockBundle\\Block\\Service\\TextBlockService',
            'sonata.block.service.rss.class' => 'Sonata\\BlockBundle\\Block\\Service\\RssBlockService',
            'sonata.block.service.menu.class' => 'Sonata\\BlockBundle\\Block\\Service\\MenuBlockService',
            'sonata.block.service.template.class' => 'Sonata\\BlockBundle\\Block\\Service\\TemplateBlockService',
            'sonata.block.exception.strategy.manager.class' => 'Sonata\\BlockBundle\\Exception\\Strategy\\StrategyManager',
            'sonata.block.container.types' => [
                0 => 'sonata.block.service.container',
                1 => 'sonata.page.block.container',
                2 => 'sonata.dashboard.block.container',
                3 => 'cmf.block.container',
                4 => 'cmf.block.slideshow',
            ],
            'sonata_block.blocks' => [
                'sonata.block.service.template' => [
                    'settings' => [
                        'address' => NULL,
                        'addresses' => NULL,
                        'cart' => NULL,
                        'channel' => NULL,
                        'customer' => NULL,
                        'form' => NULL,
                        'order' => NULL,
                        'order_item' => NULL,
                        'orders' => NULL,
                        'product' => NULL,
                        'product_review' => NULL,
                        'product_reviews' => NULL,
                        'products' => NULL,
                        'resource' => NULL,
                        'resources' => NULL,
                        'statistics' => NULL,
                        'taxon' => NULL,
                    ],
                    'contexts' => [

                    ],
                    'templates' => [

                    ],
                    'cache' => 'sonata.cache.noop',
                ],
                'sonata.block.service.container' => [
                    'contexts' => [

                    ],
                    'templates' => [

                    ],
                    'cache' => 'sonata.cache.noop',
                    'settings' => [

                    ],
                ],
                'sonata.block.service.empty' => [
                    'contexts' => [

                    ],
                    'templates' => [

                    ],
                    'cache' => 'sonata.cache.noop',
                    'settings' => [

                    ],
                ],
                'sonata.block.service.text' => [
                    'contexts' => [

                    ],
                    'templates' => [

                    ],
                    'cache' => 'sonata.cache.noop',
                    'settings' => [

                    ],
                ],
                'sonata.block.service.rss' => [
                    'contexts' => [

                    ],
                    'templates' => [

                    ],
                    'cache' => 'sonata.cache.noop',
                    'settings' => [

                    ],
                ],
                'sonata.block.service.menu' => [
                    'contexts' => [

                    ],
                    'templates' => [

                    ],
                    'cache' => 'sonata.cache.noop',
                    'settings' => [

                    ],
                ],
            ],
            'sonata_block.blocks_by_class' => [

            ],
            'sonata_blocks.block_types' => [
                0 => 'sonata.block.service.template',
                1 => 'sonata.block.service.container',
                2 => 'sonata.block.service.empty',
                3 => 'sonata.block.service.text',
                4 => 'sonata.block.service.rss',
                5 => 'sonata.block.service.menu',
            ],
            'sonata_block.cache_blocks' => [
                'by_type' => [
                    'sonata.block.service.template' => 'sonata.cache.noop',
                    'sonata.block.service.container' => 'sonata.cache.noop',
                    'sonata.block.service.empty' => 'sonata.cache.noop',
                    'sonata.block.service.text' => 'sonata.cache.noop',
                    'sonata.block.service.rss' => 'sonata.cache.noop',
                    'sonata.block.service.menu' => 'sonata.cache.noop',
                ],
            ],
            'sonata_blocks.default_contexts' => [

            ],
            'sonata.intl.locale_detector.request.class' => 'Sonata\\IntlBundle\\Locale\\RequestDetector',
            'sonata.intl.templating.helper.locale.class' => 'Sonata\\IntlBundle\\Templating\\Helper\\LocaleHelper',
            'sonata.intl.templating.helper.number.class' => 'Sonata\\IntlBundle\\Templating\\Helper\\NumberHelper',
            'sonata.intl.templating.helper.datetime.class' => 'Sonata\\IntlBundle\\Templating\\Helper\\DateTimeHelper',
            'sonata.intl.timezone_detector.chain.class' => 'Sonata\\IntlBundle\\Timezone\\ChainTimezoneDetector',
            'sonata.intl.timezone_detector.user.class' => 'Sonata\\IntlBundle\\Timezone\\UserBasedTimezoneDetector',
            'sonata.intl.timezone_detector.locale.class' => 'Sonata\\IntlBundle\\Timezone\\LocaleBasedTimezoneDetector',
            'sonata.intl.twig.helper.locale.class' => 'Sonata\\IntlBundle\\Twig\\Extension\\LocaleExtension',
            'sonata.intl.twig.helper.number.class' => 'Sonata\\IntlBundle\\Twig\\Extension\\NumberExtension',
            'sonata.intl.twig.helper.datetime.class' => 'Sonata\\IntlBundle\\Twig\\Extension\\DateTimeExtension',
            'sonata_intl.timezone.detectors' => [
                0 => 'sonata.intl.timezone_detector.user',
                1 => 'sonata.intl.timezone_detector.locale',
            ],
            'hateoas.link_factory.class' => 'Hateoas\\Factory\\LinkFactory',
            'hateoas.links_factory.class' => 'Hateoas\\Factory\\LinksFactory',
            'hateoas.embeds_factory.class' => 'Hateoas\\Factory\\EmbeddedsFactory',
            'hateoas.expression.evaluator.class' => 'Bazinga\\Bundle\\HateoasBundle\\Hateoas\\Expression\\LazyFunctionExpressionEvaluator',
            'bazinga_hateoas.expression_language.class' => 'Bazinga\\Bundle\\HateoasBundle\\ExpressionLanguage\\ExpressionLanguage',
            'hateoas.expression.link.class' => 'Hateoas\\Expression\\LinkExpressionFunction',
            'hateoas.serializer.xml.class' => 'Hateoas\\Serializer\\XmlSerializer',
            'hateoas.serializer.json_hal.class' => 'Hateoas\\Serializer\\JsonHalSerializer',
            'hateoas.serializer.exclusion_manager.class' => 'Hateoas\\Serializer\\ExclusionManager',
            'hateoas.event_subscriber.xml.class' => 'Hateoas\\Serializer\\EventSubscriber\\XmlEventSubscriber',
            'hateoas.event_subscriber.json.class' => 'Hateoas\\Serializer\\EventSubscriber\\JsonEventSubscriber',
            'hateoas.inline_deferrer.embeds.class' => 'Hateoas\\Serializer\\Metadata\\InlineDeferrer',
            'hateoas.inline_deferrer.links.class' => 'Hateoas\\Serializer\\Metadata\\InlineDeferrer',
            'hateoas.configuration.provider.resolver.chain.class' => 'Hateoas\\Configuration\\Provider\\Resolver\\ChainResolver',
            'hateoas.configuration.provider.resolver.method.class' => 'Hateoas\\Configuration\\Provider\\Resolver\\MethodResolver',
            'hateoas.configuration.provider.resolver.static_method.class' => 'Hateoas\\Configuration\\Provider\\Resolver\\StaticMethodResolver',
            'hateoas.configuration.provider.resolver.symfony_container.class' => 'Hateoas\\Configuration\\Provider\\Resolver\\SymfonyContainerResolver',
            'hateoas.configuration.relation_provider.class' => 'Hateoas\\Configuration\\Provider\\RelationProvider',
            'hateoas.configuration.relations_repository.class' => 'Hateoas\\Configuration\\RelationsRepository',
            'hateoas.configuration.metadata.yaml_driver.class' => 'Hateoas\\Configuration\\Metadata\\Driver\\YamlDriver',
            'hateoas.configuration.metadata.xml_driver.class' => 'Hateoas\\Configuration\\Metadata\\Driver\\XmlDriver',
            'hateoas.configuration.metadata.annotation_driver.class' => 'Hateoas\\Configuration\\Metadata\\Driver\\AnnotationDriver',
            'hateoas.configuration.metadata.extension_driver.class' => 'Hateoas\\Configuration\\Metadata\\Driver\\ExtensionDriver',
            'hateoas.generator.registry.class' => 'Hateoas\\UrlGenerator\\UrlGeneratorRegistry',
            'hateoas.generator.symfony.class' => 'Hateoas\\UrlGenerator\\SymfonyUrlGenerator',
            'hateoas.helper.link.class' => 'Hateoas\\Helper\\LinkHelper',
            'hateoas.twig.link.class' => 'Hateoas\\Twig\\Extension\\LinkExtension',
            'jms_serializer.metadata.file_locator.class' => 'Metadata\\Driver\\FileLocator',
            'jms_serializer.metadata.annotation_driver.class' => 'JMS\\Serializer\\Metadata\\Driver\\AnnotationDriver',
            'jms_serializer.metadata.chain_driver.class' => 'Metadata\\Driver\\DriverChain',
            'jms_serializer.metadata.yaml_driver.class' => 'JMS\\Serializer\\Metadata\\Driver\\YamlDriver',
            'jms_serializer.metadata.xml_driver.class' => 'JMS\\Serializer\\Metadata\\Driver\\XmlDriver',
            'jms_serializer.metadata.php_driver.class' => 'JMS\\Serializer\\Metadata\\Driver\\PhpDriver',
            'jms_serializer.metadata.doctrine_type_driver.class' => 'JMS\\Serializer\\Metadata\\Driver\\DoctrineTypeDriver',
            'jms_serializer.metadata.doctrine_phpcr_type_driver.class' => 'JMS\\Serializer\\Metadata\\Driver\\DoctrinePHPCRTypeDriver',
            'jms_serializer.metadata.lazy_loading_driver.class' => 'Metadata\\Driver\\LazyLoadingDriver',
            'jms_serializer.metadata.metadata_factory.class' => 'Metadata\\MetadataFactory',
            'jms_serializer.metadata.cache.file_cache.class' => 'Metadata\\Cache\\FileCache',
            'jms_serializer.event_dispatcher.class' => 'JMS\\Serializer\\EventDispatcher\\LazyEventDispatcher',
            'jms_serializer.camel_case_naming_strategy.class' => 'JMS\\Serializer\\Naming\\CamelCaseNamingStrategy',
            'jms_serializer.identical_property_naming_strategy.class' => 'JMS\\Serializer\\Naming\\IdenticalPropertyNamingStrategy',
            'jms_serializer.serialized_name_annotation_strategy.class' => 'JMS\\Serializer\\Naming\\SerializedNameAnnotationStrategy',
            'jms_serializer.cache_naming_strategy.class' => 'JMS\\Serializer\\Naming\\CacheNamingStrategy',
            'jms_serializer.doctrine_object_constructor.class' => 'JMS\\Serializer\\Construction\\DoctrineObjectConstructor',
            'jms_serializer.unserialize_object_constructor.class' => 'JMS\\Serializer\\Construction\\UnserializeObjectConstructor',
            'jms_serializer.version_exclusion_strategy.class' => 'JMS\\Serializer\\Exclusion\\VersionExclusionStrategy',
            'jms_serializer.serializer.class' => 'JMS\\Serializer\\Serializer',
            'jms_serializer.twig_extension.class' => 'JMS\\Serializer\\Twig\\SerializerExtension',
            'jms_serializer.twig_runtime_extension.class' => 'JMS\\Serializer\\Twig\\SerializerRuntimeExtension',
            'jms_serializer.twig_runtime_extension_helper.class' => 'JMS\\Serializer\\Twig\\SerializerRuntimeHelper',
            'jms_serializer.templating.helper.class' => 'JMS\\SerializerBundle\\Templating\\SerializerHelper',
            'jms_serializer.json_serialization_visitor.class' => 'JMS\\Serializer\\JsonSerializationVisitor',
            'jms_serializer.json_serialization_visitor.options' => 1088,
            'jms_serializer.json_deserialization_visitor.class' => 'JMS\\Serializer\\JsonDeserializationVisitor',
            'jms_serializer.xml_serialization_visitor.class' => 'JMS\\Serializer\\XmlSerializationVisitor',
            'jms_serializer.xml_deserialization_visitor.class' => 'JMS\\Serializer\\XmlDeserializationVisitor',
            'jms_serializer.xml_deserialization_visitor.doctype_whitelist' => [

            ],
            'jms_serializer.xml_serialization_visitor.format_output' => true,
            'jms_serializer.yaml_serialization_visitor.class' => 'JMS\\Serializer\\YamlSerializationVisitor',
            'jms_serializer.handler_registry.class' => 'JMS\\Serializer\\Handler\\LazyHandlerRegistry',
            'jms_serializer.datetime_handler.class' => 'JMS\\Serializer\\Handler\\DateHandler',
            'jms_serializer.array_collection_handler.class' => 'JMS\\Serializer\\Handler\\ArrayCollectionHandler',
            'jms_serializer.php_collection_handler.class' => 'JMS\\Serializer\\Handler\\PhpCollectionHandler',
            'jms_serializer.form_error_handler.class' => 'JMS\\Serializer\\Handler\\FormErrorHandler',
            'jms_serializer.constraint_violation_handler.class' => 'JMS\\Serializer\\Handler\\ConstraintViolationHandler',
            'jms_serializer.doctrine_proxy_subscriber.class' => 'JMS\\Serializer\\EventDispatcher\\Subscriber\\DoctrineProxySubscriber',
            'jms_serializer.stopwatch_subscriber.class' => 'JMS\\SerializerBundle\\Serializer\\StopwatchEventSubscriber',
            'jms_serializer.configured_context_factory.class' => 'JMS\\SerializerBundle\\ContextFactory\\ConfiguredContextFactory',
            'jms_serializer.expression_evaluator.class' => 'JMS\\Serializer\\Expression\\ExpressionEvaluator',
            'jms_serializer.expression_language.class' => 'Symfony\\Component\\ExpressionLanguage\\ExpressionLanguage',
            'jms_serializer.expression_language.function_provider.class' => 'JMS\\SerializerBundle\\ExpressionLanguage\\BasicSerializerFunctionsProvider',
            'jms_serializer.accessor_strategy.default.class' => 'JMS\\Serializer\\Accessor\\DefaultAccessorStrategy',
            'jms_serializer.accessor_strategy.expression.class' => 'JMS\\Serializer\\Accessor\\ExpressionAccessorStrategy',
            'jms_serializer.cache.cache_warmer.class' => 'JMS\\SerializerBundle\\Cache\\CacheWarmer',
            'fos_rest.format_listener.rules' => NULL,
            'knp_gaufrette.filesystem_map.class' => 'Knp\\Bundle\\GaufretteBundle\\FilesystemMap',
            'knp_menu.factory.class' => 'Knp\\Menu\\MenuFactory',
            'knp_menu.factory_extension.routing.class' => 'Knp\\Menu\\Integration\\Symfony\\RoutingExtension',
            'knp_menu.helper.class' => 'Knp\\Menu\\Twig\\Helper',
            'knp_menu.matcher.class' => 'Knp\\Menu\\Matcher\\Matcher',
            'knp_menu.menu_provider.chain.class' => 'Knp\\Menu\\Provider\\ChainProvider',
            'knp_menu.menu_provider.container_aware.class' => 'Knp\\Bundle\\MenuBundle\\Provider\\ContainerAwareProvider',
            'knp_menu.menu_provider.builder_alias.class' => 'Knp\\Bundle\\MenuBundle\\Provider\\BuilderAliasProvider',
            'knp_menu.renderer_provider.class' => 'Knp\\Bundle\\MenuBundle\\Renderer\\ContainerAwareProvider',
            'knp_menu.renderer.list.class' => 'Knp\\Menu\\Renderer\\ListRenderer',
            'knp_menu.renderer.list.options' => [

            ],
            'knp_menu.listener.voters.class' => 'Knp\\Bundle\\MenuBundle\\EventListener\\VoterInitializerListener',
            'knp_menu.voter.router.class' => 'Knp\\Menu\\Matcher\\Voter\\RouteVoter',
            'knp_menu.twig.extension.class' => 'Knp\\Menu\\Twig\\MenuExtension',
            'knp_menu.renderer.twig.class' => 'Knp\\Menu\\Renderer\\TwigRenderer',
            'knp_menu.renderer.twig.options' => [

            ],
            'knp_menu.renderer.twig.template' => '@KnpMenu/menu.html.twig',
            'knp_menu.default_renderer' => 'twig',
            'liip_imagine.resolvers' => [
                'default' => [
                    'web_path' => [
                        'web_root' => (\dirname(__DIR__, 4).'/public'),
                        'cache_prefix' => 'media/cache',
                    ],
                ],
            ],
            'liip_imagine.loaders' => [
                'default' => [
                    'filesystem' => [
                        'data_root' => [
                            0 => (\dirname(__DIR__, 4).'/public/media/image'),
                        ],
                        'locator' => 'filesystem',
                        'allow_unresolvable_data_roots' => false,
                        'bundle_resources' => [
                            'enabled' => false,
                            'access_control_type' => 'blacklist',
                            'access_control_list' => [

                            ],
                        ],
                    ],
                ],
            ],
            'liip_imagine.jpegoptim.binary' => '/usr/bin/jpegoptim',
            'liip_imagine.jpegoptim.stripAll' => true,
            'liip_imagine.jpegoptim.max' => NULL,
            'liip_imagine.jpegoptim.progressive' => true,
            'liip_imagine.jpegoptim.tempDir' => NULL,
            'liip_imagine.optipng.binary' => '/usr/bin/optipng',
            'liip_imagine.optipng.level' => 7,
            'liip_imagine.optipng.stripAll' => true,
            'liip_imagine.optipng.tempDir' => NULL,
            'liip_imagine.pngquant.binary' => '/usr/bin/pngquant',
            'liip_imagine.mozjpeg.binary' => '/opt/mozjpeg/bin/cjpeg',
            'liip_imagine.driver_service' => 'liip_imagine.gd',
            'liip_imagine.cache.resolver.default' => 'default',
            'liip_imagine.default_image' => NULL,
            'liip_imagine.filter_sets' => [
                'sylius_small' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 120,
                                1 => 90,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_medium' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 240,
                                1 => 180,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_large' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 640,
                                1 => 480,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_admin_product_original' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [

                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_admin_admin_user_avatar_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 50,
                                1 => 50,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_admin_product_tiny_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 64,
                                1 => 64,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_admin_product_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 50,
                                1 => 50,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_admin_product_small_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 150,
                                1 => 112,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_admin_product_large_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 550,
                                1 => 412,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_shop_product_original' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [

                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_shop_product_tiny_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 64,
                                1 => 64,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_shop_product_small_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 150,
                                1 => 112,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_shop_product_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 260,
                                1 => 260,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'sylius_shop_product_large_thumbnail' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 550,
                                1 => 412,
                            ],
                            'mode' => 'outbound',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'cache' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [

                    ],
                    'post_processors' => [

                    ],
                ],
                'product_thumbnail_white_border' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [
                        'thumbnail' => [
                            'size' => [
                                0 => 240,
                                1 => 240,
                            ],
                            'mode' => 'inset',
                        ],
                        'background' => [
                            'size' => [
                                0 => 260,
                                1 => 260,
                            ],
                            'position' => 'center',
                            'color' => '#fff',
                        ],
                    ],
                    'post_processors' => [

                    ],
                ],
                'omni_sylius_banner' => [
                    'quality' => 100,
                    'jpeg_quality' => NULL,
                    'png_compression_level' => NULL,
                    'png_compression_filter' => NULL,
                    'format' => NULL,
                    'animated' => false,
                    'cache' => NULL,
                    'data_loader' => NULL,
                    'default_image' => NULL,
                    'filters' => [

                    ],
                    'post_processors' => [

                    ],
                ],
            ],
            'liip_imagine.binary.loader.default' => 'default',
            'liip_imagine.controller.filter_action' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterAction',
            'liip_imagine.controller.filter_runtime_action' => 'Liip\\ImagineBundle\\Controller\\ImagineController::filterRuntimeAction',
            'liip_imagine.webp.generate' => false,
            'liip_imagine.webp.options' => [
                'quality' => 100,
                'cache' => NULL,
                'data_loader' => NULL,
                'post_processors' => [

                ],
            ],
            'payum.capture_path' => 'payum_capture_do',
            'payum.notify_path' => 'payum_notify_do',
            'payum.authorize_path' => 'payum_authorize_do',
            'payum.refund_path' => 'payum_refund_do',
            'payum.cancel_path' => 'payum_cancel_do',
            'payum.payout_path' => 'payum_payout_do',
            'payum.available_gateway_factories' => [

            ],
            'payum.storage.doctrine.orm.class' => 'Payum\\Core\\Bridge\\Doctrine\\Storage\\DoctrineStorage',
            'stof_doctrine_extensions.listener.tree.class' => 'Gedmo\\Tree\\TreeListener',
            'stof_doctrine_extensions.listener.sluggable.class' => 'Gedmo\\Sluggable\\SluggableListener',
            'stof_doctrine_extensions.listener.timestampable.class' => 'Gedmo\\Timestampable\\TimestampableListener',
            'stof_doctrine_extensions.listener.loggable.class' => 'Gedmo\\Loggable\\LoggableListener',
            'stof_doctrine_extensions.event_listener.logger.class' => 'Stof\\DoctrineExtensionsBundle\\EventListener\\LoggerListener',
            'stof_doctrine_extensions.listener.sortable.class' => 'Gedmo\\Sortable\\SortableListener',
            'stof_doctrine_extensions.translation_fallback' => false,
            'stof_doctrine_extensions.persist_default_translation' => false,
            'stof_doctrine_extensions.skip_translation_on_load' => false,
            'stof_doctrine_extensions.listener.translatable.class' => 'Gedmo\\Translatable\\TranslatableListener',
            'stof_doctrine_extensions.listener.blameable.class' => 'Gedmo\\Blameable\\BlameableListener',
            'stof_doctrine_extensions.listener.softdeleteable.class' => 'Gedmo\\SoftDeleteable\\SoftDeleteableListener',
            'stof_doctrine_extensions.listener.uploadable.class' => 'Gedmo\\Uploadable\\UploadableListener',
            'stof_doctrine_extensions.listener.reference_integrity.class' => 'Gedmo\\ReferenceIntegrity\\ReferenceIntegrityListener',
            'white_october_pagerfanta.default_view' => 'default',
            'white_october_pagerfanta.view_factory.class' => 'Pagerfanta\\View\\ViewFactory',
            'doctrine_migrations.dir_name' => (\dirname(__DIR__, 4).'/src/Migrations'),
            'doctrine_migrations.namespace' => 'DoctrineMigrations',
            'doctrine_migrations.name' => 'Application Migrations',
            'doctrine_migrations.migrations_paths' => [

            ],
            'doctrine_migrations.table_name' => 'migration_versions',
            'doctrine_migrations.column_name' => 'version',
            'doctrine_migrations.column_length' => 14,
            'doctrine_migrations.executed_at_column_name' => 'executed_at',
            'doctrine_migrations.all_or_nothing' => false,
            'doctrine_migrations.custom_template' => NULL,
            'doctrine_migrations.organize_migrations' => false,
            'sylius_payum.driver.doctrine/orm' => true,
            'sylius_payum.driver' => 'doctrine/orm',
            'sylius.model.payment_security_token.class' => 'App\\Entity\\Payment\\PaymentSecurityToken',
            'sylius.controller.payment_security_token.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.payment_security_token.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.gateway_config.class' => 'App\\Entity\\Payment\\GatewayConfig',
            'sylius.controller.gateway_config.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.gateway_config.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.gateway_config.validation_groups' => [
                0 => 'sylius',
            ],
            'payum.template.layout' => '@SyliusPayum/layout.html.twig',
            'payum.template.obtain_credit_card' => '@SyliusPayum/Action/obtainCreditCard.html.twig',
            'sylius.admin.notification.enabled' => true,
            'sylius.admin.notification.frequency' => 60,
            'sylius.admin.shop_enabled' => true,
            'sylius.admin.notification.uri' => 'http://gus.sylius.com/version',
            'sylius_shop.firewall_context_name' => 'shop',
            'sylius_shop.product_grid.include_all_descendants' => true,
            'fos_oauth_server.server.class' => 'OAuth2\\OAuth2',
            'fos_oauth_server.security.authentication.provider.class' => 'FOS\\OAuthServerBundle\\Security\\Authentication\\Provider\\OAuthProvider',
            'fos_oauth_server.security.authentication.listener.class' => 'FOS\\OAuthServerBundle\\Security\\Firewall\\OAuthListener',
            'fos_oauth_server.security.entry_point.class' => 'FOS\\OAuthServerBundle\\Security\\EntryPoint\\OAuthEntryPoint',
            'fos_oauth_server.server.options' => [

            ],
            'fos_oauth_server.model_manager_name' => NULL,
            'fos_oauth_server.model.client.class' => 'App\\Entity\\AdminApi\\Client',
            'fos_oauth_server.model.access_token.class' => 'App\\Entity\\AdminApi\\AccessToken',
            'fos_oauth_server.model.refresh_token.class' => 'App\\Entity\\AdminApi\\RefreshToken',
            'fos_oauth_server.model.auth_code.class' => 'App\\Entity\\AdminApi\\AuthCode',
            'fos_oauth_server.template.engine' => 'twig',
            'fos_oauth_server.authorize.form.type' => 'FOS\\OAuthServerBundle\\Form\\Type\\AuthorizeFormType',
            'fos_oauth_server.authorize.form.name' => 'fos_oauth_server_authorize_form',
            'fos_oauth_server.authorize.form.validation_groups' => [
                0 => 'Authorize',
                1 => 'Default',
            ],
            'sylius_admin_api.driver.doctrine/orm' => true,
            'sylius_admin_api.driver' => 'doctrine/orm',
            'sylius.model.api_user.class' => 'App\\Entity\\User\\AdminUser',
            'sylius.model.api_client.class' => 'App\\Entity\\AdminApi\\Client',
            'sylius.controller.api_client.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.api_client.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.api_access_token.class' => 'App\\Entity\\AdminApi\\AccessToken',
            'sylius.controller.api_access_token.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.api_access_token.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.api_refresh_token.class' => 'App\\Entity\\AdminApi\\RefreshToken',
            'sylius.controller.api_refresh_token.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.api_refresh_token.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.model.api_auth_code.class' => 'App\\Entity\\AdminApi\\AuthCode',
            'sylius.controller.api_auth_code.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'sylius.factory.api_auth_code.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.form.type.api_client.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.api_order.validation_groups' => [
                0 => 'sylius',
            ],
            'sylius.form.type.api_customer.validation_groups' => [
                0 => 'sylius',
            ],
            'omni_banner.position_types' => [
                'directional' => [
                    'label' => 'app.banner_position.directional',
                    'twig_template' => 'Banner/PositionType/directional.html.twig',
                    'example_image' => 'banners/examples/directional.png',
                ],
                'four_columns_images' => [
                    'label' => 'app.banner_position.four_columns',
                    'twig_template' => 'Banner/PositionType/four_columns.html.twig',
                    'example_image' => 'banners/examples/four_columns_cards.png',
                ],
                'four_columns_slider' => [
                    'label' => 'app.banner_position.four_columns_slider',
                    'twig_template' => 'Banner/PositionType/four_columns_slider.html.twig',
                    'example_image' => 'banners/examples/four_columns_slider.png',
                ],
                'content_line' => [
                    'label' => 'omni_sylius.banner.position.full_width_content_line',
                    'twig_template' => '@OmniSyliusBannerPlugin/PositionTypes/_content_line.html.twig',
                    'example_image' => 'bundles/omnisyliusbannerplugin/img/position_examples/content_line.png',
                    'descriptive' => true,
                ],
            ],
            'omni_sylius_banner.driver.doctrine/orm' => true,
            'omni_sylius_banner.driver' => 'doctrine/orm',
            'omni_sylius.model.banner.class' => 'Omni\\Sylius\\BannerPlugin\\Model\\Banner',
            'omni_sylius.controller.banner.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.banner.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.repository.banner.class' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerRepository',
            'omni_sylius.model.banner_zone.class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZone',
            'omni_sylius.controller.banner_zone.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.banner_zone.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.repository.banner_zone.class' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerZoneRepository',
            'omni_sylius.model.banner_zone_translation.class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerZoneTranslation',
            'omni_sylius.factory.banner_zone_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.model.banner_position.class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPosition',
            'omni_sylius.controller.banner_position.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.banner_position.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.repository.banner_position.class' => 'Omni\\Sylius\\BannerPlugin\\Doctrine\\ORM\\BannerPositionRepository',
            'omni_sylius.model.banner_position_translation.class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPositionTranslation',
            'omni_sylius.factory.banner_position_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.model.banner_image.class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImage',
            'omni_sylius.controller.banner_image.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.banner_image.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.model.banner_image_translation.class' => 'Omni\\Sylius\\BannerPlugin\\Model\\BannerImageTranslation',
            'omni_sylius.factory.banner_image_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_banner.form.type.banner_zone.validation_groups' => [
                0 => 'omni',
            ],
            'omni_sylius_search.driver.doctrine/orm' => true,
            'omni_sylius_search.driver' => 'doctrine/orm',
            'omni_sylius.model.search_index.class' => 'Omni\\Sylius\\SearchPlugin\\Model\\SearchIndex',
            'omni_sylius.controller.search_index.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.search_index.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_search.serialization_groups' => [
                0 => 'Default',
            ],
            'omni_search.config' => [
                'sylius.product' => [
                    'class' => 'App\\Entity\\Product\\Product',
                    'route' => 'sylius_shop_product_show',
                    'mappings' => [
                        0 => 'name',
                        1 => 'description',
                    ],
                    'only_available' => true,
                    'available_field_name' => 'enabled',
                ],
            ],
            'omni_search.count_per_page' => 12,
            'omni_filter.choice_formation_strategy' => 'restrictive',
            'omni_filter.show_products_amount' => false,
            'omni_sylius_core.driver.doctrine/orm' => true,
            'omni_sylius_core.driver' => 'doctrine/orm',
            'omni_sylius.model.channel_watermark.class' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelWatermark',
            'omni_sylius.controller.channel_watermark.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.channel_watermark.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.model.channel_logo.class' => 'Omni\\Sylius\\CorePlugin\\Model\\ChannelLogo',
            'omni_sylius.controller.channel_logo.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.channel_logo.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_cms.image_upload_dir' => 'uploads',
            'omni_sylius_cms.form.type.node.validation_groups' => [
                0 => 'omni',
            ],
            'omni_sylius_cms.form.type.node_image.validation_groups' => [
                0 => 'omni',
            ],
            'omni_sylius_cms.form.type.node_translation.validation_groups' => [
                0 => 'omni',
            ],
            'omni_sylius_cms.driver.doctrine/orm' => true,
            'omni_sylius_cms.driver' => 'doctrine/orm',
            'omni_sylius.model.node_image.class' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImage',
            'omni_sylius.controller.node_image.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.node_image.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.model.node_image_translation.class' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeImageTranslation',
            'omni_sylius.controller.node_image_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.node_image_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.model.node.class' => 'Omni\\Sylius\\CmsPlugin\\Model\\Node',
            'omni_sylius.controller.node.class' => 'Omni\\Sylius\\CmsPlugin\\Controller\\NodeController',
            'omni_sylius.factory.node.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.repository.node.class' => 'Omni\\Sylius\\CmsPlugin\\Doctrine\\ORM\\NodeRepository',
            'omni_sylius.model.node_translation.class' => 'Omni\\Sylius\\CmsPlugin\\Model\\NodeTranslation',
            'omni_sylius.controller.node_translation.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.node_translation.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius_cms.layoutable.nodes' => [

            ],
            'omni_seo.page.encoding' => 'UTF-8',
            'omni_seo.seo_view_data_parameters' => [
                0 => 'resource',
            ],
            'omni_sylius_seo.driver.doctrine/orm' => true,
            'omni_sylius_seo.driver' => 'doctrine/orm',
            'omni_sylius.model.seo_metadata.class' => 'Omni\\Sylius\\SeoPlugin\\Model\\SeoMetadata',
            'omni_sylius.factory.seo_metadata.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'shipment_pickup_code' => 'pickup',
            'omni_sylius_shipping.states' => [
                'courier_ready' => [
                    'dpd' => [
                        0 => 'Pickup scan',
                        1 => 'HUB-scan',
                        2 => 'Infoscan',
                    ],
                    'omniva' => [

                    ],
                    'venipak' => [

                    ],
                ],
                'in_transit' => [
                    'dpd' => [
                        0 => 'Out for delivery',
                    ],
                    'omniva' => [

                    ],
                    'venipak' => [

                    ],
                ],
                'cant_deliver' => [
                    'dpd' => [

                    ],
                    'omniva' => [

                    ],
                    'venipak' => [

                    ],
                ],
                'back_to_seller' => [
                    'dpd' => [

                    ],
                    'omniva' => [

                    ],
                    'venipak' => [

                    ],
                ],
                'shipped' => [
                    'dpd' => [
                        0 => 'Delivered',
                    ],
                    'omniva' => [

                    ],
                    'venipak' => [

                    ],
                ],
                'cancelled' => [
                    'dpd' => [

                    ],
                    'omniva' => [

                    ],
                    'venipak' => [

                    ],
                ],
            ],
            'omni_sylius_shipping.driver.doctrine/orm' => true,
            'omni_sylius_shipping.driver' => 'doctrine/orm',
            'omni_sylius.model.shipper_config.class' => 'App\\Entity\\Shipping\\ShipperConfig',
            'omni_sylius.controller.shipper_config.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni_sylius.factory.shipper_config.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.repository.shipper_config.class' => 'App\\Repository\\Shipping\\ShipperConfigRepository',
            'omni_sylius_dpd.shipment_shipped_state_code' => 'shipped',
            'omni_sylius_dpd.shipment_delivered_state_code' => NULL,
            'omni_sylius_dpd.shipment_delivered_transition_code' => NULL,
            'omni_sylius_dpd.parcel_machine.enabled_countries' => [
                0 => 'LT',
                1 => 'LV',
                2 => 'EE',
            ],
            'omni_sylius_dpd.provider.parcel_machine.class' => 'Omni\\Sylius\\DpdPlugin\\Provider\\ParcelMachineProvider',
            'omni_sylius_dpd.factory.parcel_machine.class' => 'Omni\\Sylius\\DpdPlugin\\Factory\\ParcelMachineFactory',
            'omni_sylius_parcel_machine.driver.doctrine/orm' => true,
            'omni_sylius_parcel_machine.driver' => 'doctrine/orm',
            'omni.model.parcel_machine.class' => 'Omni\\Sylius\\ParcelMachinePlugin\\Model\\ParcelMachine',
            'omni.controller.parcel_machine.class' => 'Sylius\\Bundle\\ResourceBundle\\Controller\\ResourceController',
            'omni.factory.parcel_machine.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni.repository.parcel_machine.class' => 'Omni\\Sylius\\ParcelMachinePlugin\\Doctrine\\ORM\\ParcelMachineRepository',
            'omni_parcel_machine.controller.api.parcel_machine.class' => 'Omni\\Sylius\\ParcelMachinePlugin\\Controller\\Api\\ParcelMachineController',
            'omni_sylius_paysera.use_order_number' => false,
            'sylius.importer.web_ui' => false,
            'sylius.importer.batch_size' => 200,
            'sylius.importer.fail_on_incomplete' => true,
            'sylius.importer.stop_on_failure' => true,
            'sylius.exporter.web_ui' => true,
            'sylius.message_queue' => [
                'enabled' => false,
                'service_id' => NULL,
                'importer_service_id' => NULL,
                'exporter_service_id' => NULL,
            ],
            'sylius.sitemap_template' => '@SitemapPlugin/show.xml.twig',
            'sylius.sitemap_index_template' => '@SitemapPlugin/index.xml.twig',
            'sylius.sitemap_exclude_taxon_root' => true,
            'sylius.sitemap_absolute_url' => true,
            'sylius.sitemap_hreflang' => true,
            'sylius.sitemap_static' => [
                0 => [
                    'route' => 'sylius_shop_homepage',
                    'parameters' => [

                    ],
                    'locales' => [

                    ],
                ],
                1 => [
                    'route' => 'sylius_shop_contact_request',
                    'parameters' => [

                    ],
                    'locales' => [

                    ],
                ],
            ],
            'sylius.provider.products' => false,
            'sylius.provider.taxons' => false,
            'sylius.provider.static' => false,
            'hwi_oauth.authentication.listener.oauth.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\Http\\Firewall\\OAuthListener',
            'hwi_oauth.authentication.provider.oauth.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\Core\\Authentication\\Provider\\OAuthProvider',
            'hwi_oauth.authentication.entry_point.oauth.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\Http\\EntryPoint\\OAuthEntryPoint',
            'hwi_oauth.user.provider.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\Core\\User\\OAuthUserProvider',
            'hwi_oauth.user.provider.entity.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\Core\\User\\EntityUserProvider',
            'hwi_oauth.user.provider.fosub_bridge.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\Core\\User\\FOSUBUserProvider',
            'hwi_oauth.registration.form.handler.fosub_bridge.class' => 'HWI\\Bundle\\OAuthBundle\\Form\\FOSUBRegistrationFormHandler',
            'hwi_oauth.resource_owner.oauth1.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\GenericOAuth1ResourceOwner',
            'hwi_oauth.resource_owner.oauth2.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\GenericOAuth2ResourceOwner',
            'hwi_oauth.resource_owner.amazon.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\AmazonResourceOwner',
            'hwi_oauth.resource_owner.asana.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\AsanaResourceOwner',
            'hwi_oauth.resource_owner.auth0.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\Auth0ResourceOwner',
            'hwi_oauth.resource_owner.azure.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\AzureResourceOwner',
            'hwi_oauth.resource_owner.bitbucket.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\BitbucketResourceOwner',
            'hwi_oauth.resource_owner.bitbucket2.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\Bitbucket2ResourceOwner',
            'hwi_oauth.resource_owner.bitly.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\BitlyResourceOwner',
            'hwi_oauth.resource_owner.box.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\BoxResourceOwner',
            'hwi_oauth.resource_owner.bufferapp.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\BufferAppResourceOwner',
            'hwi_oauth.resource_owner.clever.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\CleverResourceOwner',
            'hwi_oauth.resource_owner.dailymotion.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\DailymotionResourceOwner',
            'hwi_oauth.resource_owner.deviantart.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\DeviantartResourceOwner',
            'hwi_oauth.resource_owner.deezer.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\DeezerResourceOwner',
            'hwi_oauth.resource_owner.discogs.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\DiscogsResourceOwner',
            'hwi_oauth.resource_owner.disqus.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\DisqusResourceOwner',
            'hwi_oauth.resource_owner.dropbox.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\DropboxResourceOwner',
            'hwi_oauth.resource_owner.eve_online.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\EveOnlineResourceOwner',
            'hwi_oauth.resource_owner.eventbrite.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\EventbriteResourceOwner',
            'hwi_oauth.resource_owner.facebook.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\FacebookResourceOwner',
            'hwi_oauth.resource_owner.fiware.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\FiwareResourceOwner',
            'hwi_oauth.resource_owner.flickr.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\FlickrResourceOwner',
            'hwi_oauth.resource_owner.foursquare.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\FoursquareResourceOwner',
            'hwi_oauth.resource_owner.github.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\GitHubResourceOwner',
            'hwi_oauth.resource_owner.gitlab.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\GitLabResourceOwner',
            'hwi_oauth.resource_owner.google.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\GoogleResourceOwner',
            'hwi_oauth.resource_owner.youtube.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\YoutubeResourceOwner',
            'hwi_oauth.resource_owner.hubic.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\HubicResourceOwner',
            'hwi_oauth.resource_owner.instagram.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\InstagramResourceOwner',
            'hwi_oauth.resource_owner.jawbone.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\JawboneResourceOwner',
            'hwi_oauth.resource_owner.jira.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\JiraResourceOwner',
            'hwi_oauth.resource_owner.linkedin.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\LinkedinResourceOwner',
            'hwi_oauth.resource_owner.mailru.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\MailRuResourceOwner',
            'hwi_oauth.resource_owner.office365.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\Office365ResourceOwner',
            'hwi_oauth.resource_owner.paypal.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\PaypalResourceOwner',
            'hwi_oauth.resource_owner.qq.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\QQResourceOwner',
            'hwi_oauth.resource_owner.reddit.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\RedditResourceOwner',
            'hwi_oauth.resource_owner.runkeeper.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\RunKeeperResourceOwner',
            'hwi_oauth.resource_owner.salesforce.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\SalesforceResourceOwner',
            'hwi_oauth.resource_owner.sensio_connect.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\SensioConnectResourceOwner',
            'hwi_oauth.resource_owner.sina_weibo.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\SinaWeiboResourceOwner',
            'hwi_oauth.resource_owner.slack.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\SlackResourceOwner',
            'hwi_oauth.resource_owner.spotify.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\SpotifyResourceOwner',
            'hwi_oauth.resource_owner.soundcloud.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\SoundcloudResourceOwner',
            'hwi_oauth.resource_owner.stack_exchange.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\StackExchangeResourceOwner',
            'hwi_oauth.resource_owner.stereomood.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\StereomoodResourceOwner',
            'hwi_oauth.resource_owner.strava.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\StravaResourceOwner',
            'hwi_oauth.resource_owner.toshl.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\ToshlResourceOwner',
            'hwi_oauth.resource_owner.trakt.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\TraktResourceOwner',
            'hwi_oauth.resource_owner.trello.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\TrelloResourceOwner',
            'hwi_oauth.resource_owner.twitch.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\TwitchResourceOwner',
            'hwi_oauth.resource_owner.twitter.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\TwitterResourceOwner',
            'hwi_oauth.resource_owner.vkontakte.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\VkontakteResourceOwner',
            'hwi_oauth.resource_owner.windows_live.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\WindowsLiveResourceOwner',
            'hwi_oauth.resource_owner.wordpress.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\WordpressResourceOwner',
            'hwi_oauth.resource_owner.wunderlist.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\WunderlistResourceOwner',
            'hwi_oauth.resource_owner.xing.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\XingResourceOwner',
            'hwi_oauth.resource_owner.yahoo.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\YahooResourceOwner',
            'hwi_oauth.resource_owner.yandex.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\YandexResourceOwner',
            'hwi_oauth.resource_owner.odnoklassniki.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\OdnoklassnikiResourceOwner',
            'hwi_oauth.resource_owner.37signals.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\ThirtySevenSignalsResourceOwner',
            'hwi_oauth.resource_owner.itembase.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\ResourceOwner\\ItembaseResourceOwner',
            'hwi_oauth.resource_ownermap.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\Http\\ResourceOwnerMap',
            'hwi_oauth.security.oauth_utils.class' => 'HWI\\Bundle\\OAuthBundle\\Security\\OAuthUtils',
            'hwi_oauth.storage.session.class' => 'HWI\\Bundle\\OAuthBundle\\OAuth\\RequestDataStorage\\SessionStorage',
            'hwi_oauth.templating.helper.oauth.class' => 'HWI\\Bundle\\OAuthBundle\\Templating\\Helper\\OAuthHelper',
            'hwi_oauth.twig.extension.oauth.class' => 'HWI\\Bundle\\OAuthBundle\\Twig\\Extension\\OAuthExtension',
            'hwi_oauth.firewall_names' => [
                0 => 'shop',
            ],
            'hwi_oauth.target_path_parameter' => NULL,
            'hwi_oauth.use_referer' => false,
            'hwi_oauth.failed_use_referer' => false,
            'hwi_oauth.failed_auth_path' => 'hwi_oauth_connect',
            'hwi_oauth.grant_rule' => 'IS_AUTHENTICATED_REMEMBERED',
            'hwi_oauth.resource_owners' => [
                'facebook' => 'facebook',
                'google' => 'google',
            ],
            'hwi_oauth.fosub_enabled' => false,
            'hwi_oauth.connect' => false,
            'omni_sylius_manifest.driver.doctrine/orm' => true,
            'omni_sylius_manifest.driver' => 'doctrine/orm',
            'omni_sylius.controller.manifest.class' => 'Omni\\Sylius\\ManifestPlugin\\Controller\\ManifestController',
            'omni_sylius.factory.manifest.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.repository.manifest.class' => 'Omni\\Sylius\\ManifestPlugin\\Doctrine\\ORM\\ManifestRepository',
            'omni_sylius_manifest.pdf_path' => (\dirname(__DIR__, 3).'/manifests'),
            'omni_manifest.shipment_ready_for_courier_states' => [
                0 => 'exported',
            ],
            'omni_manifest.menu_item' => true,
            'knp_snappy.pdf.binary' => 'wkhtmltopdf',
            'knp_snappy.pdf.options' => [

            ],
            'knp_snappy.pdf.env' => [

            ],
            'knp_snappy.image.binary' => 'wkhtmltoimage',
            'knp_snappy.image.options' => [

            ],
            'knp_snappy.image.env' => [

            ],
            'lexik_translation.translator.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Translator',
            'lexik_translation.loader.database.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Loader\\DatabaseLoader',
            'lexik_translation.trans_unit.manager.class' => 'Lexik\\Bundle\\TranslationBundle\\Manager\\TransUnitManager',
            'lexik_translation.file.manager.class' => 'Lexik\\Bundle\\TranslationBundle\\Manager\\FileManager',
            'lexik_translation.locale.manager.class' => 'Lexik\\Bundle\\TranslationBundle\\Manager\\LocaleManager',
            'lexik_translation.importer.file.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Importer\\FileImporter',
            'lexik_translation.exporter_collector.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Exporter\\ExporterCollector',
            'lexik_translation.exporter.xliff.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Exporter\\XliffExporter',
            'lexik_translation.exporter.json.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Exporter\\JsonExporter',
            'lexik_translation.exporter.yml.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Exporter\\YamlExporter',
            'lexik_translation.exporter.php.class' => 'Lexik\\Bundle\\TranslationBundle\\Translation\\Exporter\\PhpExporter',
            'lexik_translation.orm.translation_storage.class' => 'Lexik\\Bundle\\TranslationBundle\\Storage\\DoctrineORMStorage',
            'lexik_translation.orm.listener.class' => 'Lexik\\Bundle\\TranslationBundle\\Storage\\Listener\\DoctrineORMListener',
            'lexik_translation.orm.trans_unit.class' => 'Lexik\\Bundle\\TranslationBundle\\Entity\\TransUnit',
            'lexik_translation.orm.translation.class' => 'Lexik\\Bundle\\TranslationBundle\\Entity\\Translation',
            'lexik_translation.orm.file.class' => 'Lexik\\Bundle\\TranslationBundle\\Entity\\File',
            'lexik_translation.mongodb.translation_storage.class' => 'Lexik\\Bundle\\TranslationBundle\\Storage\\DoctrineMongoDBStorage',
            'lexik_translation.mongodb.trans_unit.class' => 'Lexik\\Bundle\\TranslationBundle\\Document\\TransUnit',
            'lexik_translation.mongodb.translation.class' => 'Lexik\\Bundle\\TranslationBundle\\Document\\Translation',
            'lexik_translation.mongodb.file.class' => 'Lexik\\Bundle\\TranslationBundle\\Document\\File',
            'lexik_translation.propel.translation_storage.class' => 'Lexik\\Bundle\\TranslationBundle\\Storage\\PropelStorage',
            'lexik_translation.propel.trans_unit.class' => 'Lexik\\Bundle\\TranslationBundle\\Propel\\TransUnit',
            'lexik_translation.propel.translation.class' => 'Lexik\\Bundle\\TranslationBundle\\Propel\\Translation',
            'lexik_translation.propel.file.class' => 'Lexik\\Bundle\\TranslationBundle\\Propel\\File',
            'lexik_translation.data_grid.formatter.class' => 'Lexik\\Bundle\\TranslationBundle\\Util\\DataGrid\\DataGridFormatter',
            'lexik_translation.data_grid.request_handler.class' => 'Lexik\\Bundle\\TranslationBundle\\Util\\DataGrid\\DataGridRequestHandler',
            'lexik_translation.overview.stats_aggregator.class' => 'Lexik\\Bundle\\TranslationBundle\\Util\\Overview\\StatsAggregator',
            'lexik_translation.form.handler.trans_unit.class' => 'Lexik\\Bundle\\TranslationBundle\\Form\\Handler\\TransUnitFormHandler',
            'lexik_translation.listener.get_database_resources.class' => 'Lexik\\Bundle\\TranslationBundle\\EventDispatcher\\GetDatabaseResourcesListener',
            'lexik_translation.listener.clean_translation_cache.class' => 'Lexik\\Bundle\\TranslationBundle\\EventDispatcher\\CleanTranslationCacheListener',
            'lexik_translation.token_finder.class' => 'Lexik\\Bundle\\TranslationBundle\\Util\\Profiler\\TokenFinder',
            'lexik_translation.command.import_translations.class' => 'Lexik\\Bundle\\TranslationBundle\\Command\\ImportTranslationsCommand',
            'lexik_translation.command.export_translations.class' => 'Lexik\\Bundle\\TranslationBundle\\Command\\ExportTranslationsCommand',
            'lexik_translation.importer.case_insensitive' => false,
            'lexik_translation.token_finder.limit' => 15,
            'lexik_translation.managed_locales' => [
                0 => 'en',
                1 => 'lt',
            ],
            'lexik_translation.fallback_locale' => [
                0 => 'en',
            ],
            'lexik_translation.storage' => [
                'type' => 'orm',
            ],
            'lexik_translation.base_layout' => 'OmniSyliusTranslatorPlugin:Lexik:layout.html.twig',
            'lexik_translation.grid_input_type' => 'textarea',
            'lexik_translation.grid_toggle_similar' => false,
            'lexik_translation.auto_cache_clean' => false,
            'lexik_translation.dev_tools.enable' => false,
            'lexik_translation.dev_tools.create_missing' => false,
            'lexik_translation.dev_tools.file_format' => 'yml',
            'lexik_translation.exporter.json.hierarchical_format' => false,
            'lexik_translation.exporter.yml.use_tree' => false,
            'lexik_translation.storage.type' => 'orm',
            'omni_import.additional_importer_types' => [

            ],
            'omni_import.file_upload_dir' => (\dirname(__DIR__, 3).'/import/'),
            'omni_import.file_export_upload_dir' => (\dirname(__DIR__, 3).'/export/'),
            'omni_import.single_file_processor_map' => [
                'variant_code' => 'code',
            ],
            'omni_import.single_file_processor_disable_variants' => false,
            'omni_sylius_import.driver.doctrine/orm' => true,
            'omni_sylius_import.driver' => 'doctrine/orm',
            'omni_sylius.controller.import_job.class' => 'Omni\\Sylius\\ImportPlugin\\Controller\\ImportJobController',
            'omni_sylius.factory.import_job.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'omni_sylius.controller.export_job.class' => 'Omni\\Sylius\\ImportPlugin\\Controller\\ExportJobController',
            'omni_sylius.factory.export_job.class' => 'Sylius\\Component\\Resource\\Factory\\Factory',
            'sylius.attribute.attribute_types' => [
                'text' => 'Text',
                'textarea' => 'Textarea',
                'checkbox' => 'Checkbox',
                'integer' => 'Integer',
                'percent' => 'Percent',
                'datetime' => 'Datetime',
                'date' => 'Date',
                'select' => 'Select',
            ],
            'sylius.tax_calculators' => [
                'default' => 'default',
            ],
            'sylius.shipping_calculators' => [
                'flat_rate' => 'sylius.form.shipping_calculator.flat_rate_configuration.label',
                'per_unit_rate' => 'sylius.form.shipping_calculator.per_unit_rate_configuration.label',
            ],
            'sylius.shipping_method_resolvers' => [
                'zones_and_channel_based' => 'sylius.shipping_methods_resolver.zones_and_channel_based',
                'default' => 'Default',
            ],
            'sylius.payment_method_resolvers' => [
                'default' => 'Default',
                'channel_based' => 'sylius.payment_methods_resolver.channel_based',
            ],
            'sylius.promotion_rules' => [
                'cart_quantity' => 'sylius.form.promotion_rule.cart_quantity',
                'customer_group' => 'sylius.form.promotion_rule.customer_group',
                'nth_order' => 'sylius.form.promotion_rule.nth_order',
                'shipping_country' => 'sylius.form.promotion_rule.shipping_country',
                'has_taxon' => 'sylius.form.promotion_rule.has_at_least_one_from_taxons',
                'total_of_items_from_taxon' => 'sylius.form.promotion_rule.total_price_of_items_from_taxon',
                'contains_product' => 'sylius.form.promotion_rule.contains_product',
                'item_total' => 'sylius.form.promotion_rule.item_total',
            ],
            'sylius.promotion_actions' => [
                'order_fixed_discount' => 'sylius.form.promotion_action.order_fixed_discount',
                'unit_fixed_discount' => 'sylius.form.promotion_action.item_fixed_discount',
                'order_percentage_discount' => 'sylius.form.promotion_action.order_percentage_discount',
                'unit_percentage_discount' => 'sylius.form.promotion_action.item_percentage_discount',
                'shipping_percentage_discount' => 'sylius.form.promotion_action.shipping_percentage_discount',
            ],
            'sylius.tax_calculation_strategies' => [
                'order_items_based' => 'Order items based',
                'order_item_units_based' => 'Order item units based',
            ],
            'sonata.core.form.types' => [
                0 => 'bitbag.shipping_export_plugin.shipping_gateway.form.type',
                1 => 'brille24_tier_price.form.tier_price_type',
                2 => 'App\\Form\\Type\\ConsentType',
                3 => 'App\\Form\\Type\\OrderReportType',
                4 => 'App\\Form\\Type\\QuoteType',
                5 => 'sylius.form.type.address',
                6 => 'form.type.form',
                7 => 'form.type.choice',
                8 => 'form.type.file',
                9 => 'form.type.entity',
                10 => 'sylius.form.type.order',
                11 => 'sylius.form.type.order_item',
                12 => 'sylius.form.type.cart',
                13 => 'sylius.form.type.cart_item',
                14 => 'sylius.form.type.money',
                15 => 'sylius.form.type.currency',
                16 => 'sylius.form.type.currency_choice',
                17 => 'sylius.form.type.exchange_rate',
                18 => 'sylius.form.type.locale',
                19 => 'sylius.form.type.locale_choice',
                20 => 'sylius.form.type.product_association',
                21 => 'sylius.form.type.product_association_type',
                22 => 'sylius.form.type.product_association_type_translation',
                23 => 'sylius.form.type.product_association_type_choice',
                24 => 'sylius.form.type.sylius_product_associations',
                25 => 'sylius.form.type.product_attribute',
                26 => 'sylius.form.type.product_attribute_translation',
                27 => 'sylius.form.type.product_attribute_choice',
                28 => 'sylius.form.type.product_attribute_value',
                29 => 'sylius.form.type.product',
                30 => 'sylius.form.type.product_translation',
                31 => 'sylius.form.type.product_choice',
                32 => 'sylius.form.type.product_code_choice',
                33 => 'sylius.form.type.product_generate_variants',
                34 => 'sylius.form.type.product_option',
                35 => 'sylius.form.type.product_option_translation',
                36 => 'sylius.form.type.product_option_choice',
                37 => 'sylius.form.type.product_option_value',
                38 => 'sylius.form.type.product_option_value_translation',
                39 => 'sylius.form.type.product_variant',
                40 => 'sylius.form.type.product_variant_translation',
                41 => 'sylius.form.type.product_variant_generation',
                42 => 'sylius.form.type.channel',
                43 => 'sylius.form.type.channel_choice',
                44 => 'sylius.form.type.attribute_type.select',
                45 => 'sylius.form.type.attribute_type.select.choices_collection',
                46 => 'sylius.attribute_type.select.value.translations',
                47 => 'sylius.form.type.attribute_type_choice',
                48 => 'sylius.form.type.tax_category',
                49 => 'sylius.form.type.tax_category_choice',
                50 => 'sylius.form.type.tax_rate',
                51 => 'sylius.form.type.tax_calculator_choice',
                52 => 'sylius.form.type.shipping_method',
                53 => 'sylius.form.type.shipping_method_translation',
                54 => 'sylius.form.type.shipping_method_choice',
                55 => 'sylius.form.type.shipping_category',
                56 => 'sylius.form.type.shipping_category_choice',
                57 => 'sylius.form.type.shipment',
                58 => 'sylius.form.type.shipment_ship',
                59 => 'sylius.form.type.shipping_calculator_choice',
                60 => 'sylius.form.type.payment',
                61 => 'sylius.form.type.payment_method',
                62 => 'sylius.form.type.payment_method_translation',
                63 => 'sylius.form.type.payment_method_choice',
                64 => 'sylius.form.type.payment_gateway_choice',
                65 => 'sylius.form.type.promotion',
                66 => 'sylius.form.type.promotion_action',
                67 => 'sylius.form.type.promotion_rule',
                68 => 'sylius.form.type.promotion_coupon',
                69 => 'sylius.form.type.promotion_action.collection',
                70 => 'sylius.form.type.promotion_rule.collection',
                71 => 'sylius.form.type.promotion_action_choice',
                72 => 'sylius.form.type.promotion_rule_choice',
                73 => 'sylius.form.type.promotion_coupon_to_code',
                74 => 'sylius.form.type.promotion_coupon_generator_instruction',
                75 => 'sylius.form.type.country',
                76 => 'sylius.form.type.country_choice',
                77 => 'sylius.form.type.country_code_choice',
                78 => 'sylius.form.type.province',
                79 => 'sylius.form.type.province_choice',
                80 => 'sylius.form.type.province_code_choice',
                81 => 'sylius.form.type.zone',
                82 => 'sylius.form.type.zone_choice',
                83 => 'sylius.form.type.zone_code_choice',
                84 => 'sylius.form.type.zone_member',
                85 => 'sylius.form.type.taxon',
                86 => 'sylius.form.type.taxon_translation',
                87 => 'sylius.form.type.taxon_position',
                88 => 'sylius.form.type.user_login',
                89 => 'sylius.form.type.user_request_password_reset',
                90 => 'sylius.form.type.user_reset_password',
                91 => 'sylius.form.type.user_change_password',
                92 => 'sylius.form.type.customer',
                93 => 'sylius.form.type.customer_profile',
                94 => 'sylius.form.type.customer_choice',
                95 => 'sylius.form.type.customer_group',
                96 => 'sylius.form.type.customer_group_choice',
                97 => 'sylius.form.type.customer_group_code_choice',
                98 => 'sylius.form.type.security_login',
                99 => 'sylius.form.type.checkout_address',
                100 => 'sylius.form.type.checkout_select_shipping',
                101 => 'sylius.form.type.checkout_shipment',
                102 => 'sylius.form.type.checkout_select_payment',
                103 => 'sylius.form.type.checkout_payment',
                104 => 'sylius.form.type.checkout_complete',
                105 => 'sylius.form.type.product_review',
                106 => 'sylius.form.type.admin_user',
                107 => 'sylius.form.type.shop_user',
                108 => 'sylius.form.type.shop_user_registration',
                109 => 'sylius.form.type.product_image',
                110 => 'sylius.form.type.taxon_image',
                111 => 'sylius.form.type.avatar_image',
                112 => 'sylius.form.type.tax_calculation_strategy_choice',
                113 => 'sylius.form.type.promotion_rule.customer_group_configuration',
                114 => 'sylius.form.type.promotion_rule.has_taxon_configuration',
                115 => 'sylius.form.type.promotion_rule.total_of_items_from_taxon_configuration',
                116 => 'sylius.form.type.promotion_rule.contains_product_configuration',
                117 => 'sylius.form.type.promotion_action.filter.taxon',
                118 => 'sylius.form.type.promotion_action.filter.product',
                119 => 'sylius.form.type.customer_guest',
                120 => 'sylius.form.type.customer_simple_registration',
                121 => 'sylius.form.type.customer_registration',
                122 => 'sylius.form.type.address_choice',
                123 => 'sylius.form.type.add_to_cart',
                124 => 'sylius.form.type.channel_pricing',
                125 => 'sylius.form.type.channels_collection',
                126 => 'sylius.form.type.channel_based_shipping_calculator.flat_rate',
                127 => 'sylius.form.type.channel_based_shipping_calculator.per_unit_rate',
                128 => 'sylius.form.type.shop_billing_data',
                129 => 'sylius.form.type.autocomplete_product_taxon_choice',
                130 => 'sylius.form.type.resource_autocomplete_choice',
                131 => 'sylius.form.type.default',
                132 => 'sylius.form.type.resource_translations',
                133 => 'sylius.form.type.grid_filter.string',
                134 => 'sylius.form.type.grid_filter.boolean',
                135 => 'sylius.form.type.grid_filter.date',
                136 => 'sylius.form.type.grid_filter.entity',
                137 => 'sylius.form.type.grid_filter.money',
                138 => 'sylius.form.type.grid_filter.exists',
                139 => 'sylius.form.type.grid_filter.select',
                140 => 'sonata.core.form.type.array_legacy',
                141 => 'sonata.core.form.type.boolean_legacy',
                142 => 'sonata.core.form.type.collection_legacy',
                143 => 'sonata.core.form.type.translatable_choice',
                144 => 'sonata.core.form.type.date_range_legacy',
                145 => 'sonata.core.form.type.datetime_range_legacy',
                146 => 'sonata.core.form.type.date_picker_legacy',
                147 => 'sonata.core.form.type.datetime_picker_legacy',
                148 => 'sonata.core.form.type.date_range_picker_legacy',
                149 => 'sonata.core.form.type.datetime_range_picker_legacy',
                150 => 'sonata.core.form.type.equal_legacy',
                151 => 'sonata.core.form.type.color_selector',
                152 => 'sonata.core.form.type.color_legacy',
                153 => 'sonata.form.type.array',
                154 => 'sonata.form.type.boolean',
                155 => 'sonata.form.type.collection',
                156 => 'sonata.form.type.date_range',
                157 => 'sonata.form.type.datetime_range',
                158 => 'sonata.form.type.date_picker',
                159 => 'sonata.form.type.datetime_picker',
                160 => 'sonata.form.type.date_range_picker',
                161 => 'sonata.form.type.datetime_range_picker',
                162 => 'sonata.form.type.equal',
                163 => 'sonata.block.form.type.block',
                164 => 'sonata.block.form.type.container_template',
                165 => 'liip_imagine.form.type.image',
                166 => 'payum.form.type.credit_card_expiration_date',
                167 => 'payum.form.type.credit_card',
                168 => 'payum.form.type.gateway_config',
                169 => 'payum.form.type.gateway_factories_choice',
                170 => 'sylius.form.type.gateway_config',
                171 => 'sylius.form.type.gateway_configuration.paypal',
                172 => 'sylius.form.type.gateway_configuration.stripe',
                173 => 'sylius.theme.form.type.theme_choice',
                174 => 'sylius.theme.form.type.theme_name_choice',
                175 => 'fos_oauth_server.authorize.form.type',
                176 => 'sylius.form.type.api_client',
                177 => 'sylius.form.type.api_order',
                178 => 'sylius.form.type.api_order_promotion_coupon',
                179 => 'sylius.form.type.api_customer',
                180 => 'sylius.form.type.api_product',
                181 => 'sylius.form.type.api_product_variant',
                182 => 'sylius.form.type.api_checkout_address',
                183 => 'sylius.form.extension.type.api_order_item',
                184 => 'sylius.form.type.api_order_item',
                185 => 'omni_sylius_banner.form_type.banner_image',
                186 => 'omni_sylius_banner.form_type.banner_image_translation',
                187 => 'omni_sylius_banner.form_type.banner_zone',
                188 => 'omni_sylius_banner.form_type.banner_zone_translation',
                189 => 'omni_sylius_banner.form_type.banner_position',
                190 => 'omni_sylius_banner.form_type.banner_position_translation',
                191 => 'omni_sylius_banner.form_type.banner',
                192 => 'omni_filter.form.type.product_attribute',
                193 => 'omni_filter.form.type.price',
                194 => 'Omni\\Sylius\\CorePlugin\\Form\\Type\\AddressCountryCodeChoiceType',
                195 => 'Omni\\Sylius\\CorePlugin\\Form\\Type\\AddressCountryChoiceType',
                196 => 'omni_core.form.type.channel_watermark',
                197 => 'omni_core.form.type.channel_logo',
                198 => 'omni_sylius_cms.form_type.node',
                199 => 'omni_sylius_cms.form_type.node_image',
                200 => 'omni_sylius_cms.form_type.node_image_translation',
                201 => 'omni_sylius_cms.form_type.node_translation',
                202 => 'omni_sylius_cms.form_type.resource_autocomplete',
                203 => 'omni_sylius.form_type.seo_metadata',
                204 => 'omni_sylius_shipping.form.shipper_config',
                205 => 'omni_sylius_shipping.form.abstract_shipper_config',
                206 => 'omni_sylius.form.type.paysera_gateway_configuration',
                207 => 'sylius.form.type.import',
                208 => 'Omni\\Sylius\\SwedbankSpp\\Form\\Type\\GatewayConfigurationType',
                209 => 'omni_import.form.type.import_job',
                210 => 'omni_import.form.type.export_job',
            ],
            'sonata.core.form.type_extensions' => [
                0 => 'brille24_tier_price.form.extension.product_variant_type',
                1 => 'App\\Form\\Extension\\Addressing\\AddressTypeExtension',
                2 => 'App\\Form\\Extension\\Channel\\ChannelTypeExtension',
                3 => 'App\\Form\\Extension\\Channel\\SenderShipperConfigTypeExtension',
                4 => 'App\\Form\\Extension\\Product\\ProductOptionValueChoiceTypeExtension',
                5 => 'App\\Form\\Extension\\Product\\ProductTranslationTypeExtension',
                6 => 'App\\Form\\Extension\\Product\\ProductTypeExtension',
                7 => 'App\\Form\\Extension\\Product\\ProductVariantTypeExtension',
                8 => 'App\\Form\\Extension\\SelectPayment\\SelectPaymentTypeExtension',
                9 => 'App\\Form\\Extension\\Shipping\\ShipmentTypeExtension',
                10 => 'App\\Form\\Extension\\Shipping\\ShippingMethodTranslationTypeExtension',
                11 => 'form.type_extension.form.transformation_failure_handling',
                12 => 'form.type_extension.form.http_foundation',
                13 => 'form.type_extension.form.validator',
                14 => 'form.type_extension.repeated.validator',
                15 => 'form.type_extension.submit.validator',
                16 => 'form.type_extension.upload.validator',
                17 => 'form.type_extension.csrf',
                18 => 'sylius.form.extension.type.country',
                19 => 'sylius.form.extension.type.channel',
                20 => 'sylius.form.extension.type.locale',
                21 => 'sylius.form.extension.type.order',
                22 => 'sylius.form.extension.type.cart',
                23 => 'sylius.form.extension.type.cart_item',
                24 => 'sylius.form.extension.type.payment_method',
                25 => 'sylius.form.extension.type.tax_rate',
                26 => 'sylius.form.extension.type.taxon',
                27 => 'sylius.form.extension.type.customer',
                28 => 'sylius.form.extension.type.promotion',
                29 => 'sylius.form.extension.type.promotion_coupon',
                30 => 'sylius.form.extension.type.promotion_filter_collection',
                31 => 'sylius.form.extension.type.shipping_method',
                32 => 'sylius.form.extension.type.product',
                33 => 'sylius.form.extension.type.product_translation',
                34 => 'sylius.form.extension.type.product_variant',
                35 => 'sylius.form.extension.type.product_variant_generation',
                36 => 'sylius.form.extension.type.collection',
                37 => 'payum.form.extension.gateway_factories_choice',
                38 => 'sylius.form.extension.type.gateway_config.crypted',
                39 => 'omni_filter.form.extension.type.product_attribute',
                40 => 'Omni\\Sylius\\CorePlugin\\Form\\Extension\\ChannelTypeExtension',
                41 => 'Omni\\Sylius\\CorePlugin\\Form\\Extension\\AddressTypeExtension',
                42 => 'Omni\\Sylius\\ShippingPlugin\\Form\\Extension\\ShippingGatewayTypeExtension',
                43 => 'omni_sylius_dpd.form.type_extension.shipper_config_extension',
                44 => 'omni_parcel_machine.form.extension.type.shipping_method',
            ],
            'sylius.gateway_factories' => [
                'offline' => 'sylius.payum_gateway_factory.offline',
                'omnipay_paysera' => 'omni_sylius.form.paysera_label',
                'paypal_express_checkout' => 'sylius.payum_gateway_factory.paypal_express_checkout',
                'stripe_checkout' => 'sylius.payum_gateway_factory.stripe_checkout',
                'swedbank_spp' => 'omni_swedbank_spp.payum_gateway_factory.swedbank_spp',
            ],
            'bitbag.shipping_gateways' => [
                'dpd' => 'DPD',
            ],
            'console.command.ids' => [
                0 => 'console.command.public_alias.doctrine_cache.contains_command',
                1 => 'console.command.public_alias.doctrine_cache.delete_command',
                2 => 'console.command.public_alias.doctrine_cache.flush_command',
                3 => 'console.command.public_alias.doctrine_cache.stats_command',
                4 => 'Sylius\\Bundle\\OrderBundle\\Command\\RemoveExpiredCartsCommand',
                5 => 'Sylius\\Bundle\\UserBundle\\Command\\DemoteUserCommand',
                6 => 'Sylius\\Bundle\\UserBundle\\Command\\PromoteUserCommand',
                7 => 'Sylius\\Bundle\\CoreBundle\\Command\\CancelUnpaidOrdersCommand',
                8 => 'Sylius\\Bundle\\CoreBundle\\Command\\CheckRequirementsCommand',
                9 => 'Sylius\\Bundle\\CoreBundle\\Command\\InstallAssetsCommand',
                10 => 'Sylius\\Bundle\\CoreBundle\\Command\\InstallCommand',
                11 => 'Sylius\\Bundle\\CoreBundle\\Command\\InstallDatabaseCommand',
                12 => 'Sylius\\Bundle\\CoreBundle\\Command\\InstallSampleDataCommand',
                13 => 'Sylius\\Bundle\\CoreBundle\\Command\\SetupCommand',
                14 => 'Sylius\\Bundle\\CoreBundle\\Command\\ShowAvailablePluginsCommand',
                15 => 'Sylius\\Bundle\\CoreBundle\\Command\\InformAboutGUSCommand',
                16 => 'sylius.console.command.resource_debug',
                17 => 'console.command.public_alias.Sonata\\CoreBundle\\Command\\SonataDumpDoctrineMetaCommand',
                18 => 'console.command.public_alias.Sonata\\CoreBundle\\Command\\SonataListFormMappingCommand',
                19 => 'console.command.public_alias.Sylius\\Bundle\\FixturesBundle\\Command\\FixturesListCommand',
                20 => 'console.command.public_alias.Sylius\\Bundle\\FixturesBundle\\Command\\FixturesLoadCommand',
                21 => 'Sylius\\Bundle\\ThemeBundle\\Command\\AssetsInstallCommand',
                22 => 'Sylius\\Bundle\\ThemeBundle\\Command\\ListCommand',
                23 => 'console.command.public_alias.fos_oauth_server.clean_command',
                24 => 'console.command.public_alias.fos_oauth_server.create_client_command',
                25 => 'Sylius\\Bundle\\AdminApiBundle\\Command\\CreateClientCommand',
                26 => 'console.command.public_alias.sylius.command.import_data',
                27 => 'console.command.public_alias.sylius.command.import_data_from_message_queue',
                28 => 'console.command.public_alias.sylius.command.export_data',
                29 => 'console.command.public_alias.sylius.command.export_data_to_message_queue',
                30 => 'console.command.public_alias.Omni\\Sylius\\SwedbankSpp\\Command\\UpdatePaymentStatusCommand',
                31 => 'console.command.public_alias.Lexik\\Bundle\\TranslationBundle\\Command\\ImportTranslationsCommand',
                32 => 'console.command.public_alias.Lexik\\Bundle\\TranslationBundle\\Command\\ExportTranslationsCommand',
            ],
        ];
    }

    protected function throw($message)
    {
        throw new RuntimeException($message);
    }
}
