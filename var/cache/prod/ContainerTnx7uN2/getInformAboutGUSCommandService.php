<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'Sylius\Bundle\CoreBundle\Command\InformAboutGUSCommand' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/console/Command/Command.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/Command/InformAboutGUSCommand.php';

return $this->services['Sylius\\Bundle\\CoreBundle\\Command\\InformAboutGUSCommand'] = new \Sylius\Bundle\CoreBundle\Command\InformAboutGUSCommand();
