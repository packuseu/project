<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the public 'payum.builder' shared service.

include_once \dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/PayumBuilder.php';
include_once \dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/Bridge/Symfony/Builder/HttpRequestVerifierBuilder.php';
include_once \dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/Bridge/Symfony/Builder/TokenFactoryBuilder.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ContainerAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/dependency-injection/ContainerAwareTrait.php';
include_once \dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/Bridge/Symfony/Builder/CoreGatewayFactoryBuilder.php';
include_once \dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/Bridge/Symfony/Builder/GatewayFactoryBuilder.php';
include_once \dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/GatewayFactoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/GatewayFactory.php';
include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-paysera-plugin/src/OmniPayPayseraGatewayFactory.php';

$this->services['payum.builder'] = $instance = new \Payum\Core\PayumBuilder();

$a = new \Payum\Core\Bridge\Symfony\Builder\CoreGatewayFactoryBuilder();
$a->setContainer($this);

$instance->setMainRegistry(($this->services['payum.static_registry'] ?? $this->load('getPayum_StaticRegistryService.php')));
$instance->setHttpRequestVerifier(new \Payum\Core\Bridge\Symfony\Builder\HttpRequestVerifierBuilder());
$instance->setTokenFactory(new \Payum\Core\Bridge\Symfony\Builder\TokenFactoryBuilder(($this->services['router'] ?? $this->getRouterService())));
$instance->setTokenStorage(($this->services['payum.security.token_storage'] ?? $this->load('getPayum_Security_TokenStorageService.php')));
$instance->setGenericTokenFactoryPaths(['capture' => 'payum_capture_do', 'notify' => 'payum_notify_do', 'authorize' => 'payum_authorize_do', 'refund' => 'payum_refund_do', 'cancel' => 'payum_cancel_do', 'payout' => 'payum_payout_do']);
$instance->setCoreGatewayFactory($a);
$instance->addCoreGatewayFactoryConfig(['payum.template.layout' => '@PayumCore\\layout.html.twig', 'payum.template.obtain_credit_card' => '@PayumSymfonyBridge\\obtainCreditCard.html.twig', 'payum.paths' => ['PayumSymfonyBridge' => (\dirname(__DIR__, 4).'/vendor/payum/payum/src/Payum/Core/Bridge/Symfony/Resources/views')], 'payum.action.get_http_request' => ($this->services['payum.action.get_http_request'] ?? $this->load('getPayum_Action_GetHttpRequestService.php')), 'payum.action.obtain_credit_card' => ($this->services['payum.action.obtain_credit_card_builder'] ?? $this->load('getPayum_Action_ObtainCreditCardBuilderService.php'))]);
$instance->addGateway('offline', ['factory' => 'offline']);
$instance->setGatewayConfigStorage(($this->services['payum.dynamic_gateways.config_storage'] ?? $this->load('getPayum_DynamicGateways_ConfigStorageService.php')));
$instance->addCoreGatewayFactoryConfig(['twig.env' => '@twig']);
$instance->addCoreGatewayFactoryConfig(['payum.action.sylius.authorize_payment' => '@sylius.payum_action.authorize_payment', 'payum.action.sylius.capture_payment' => '@sylius.payum_action.capture_payment', 'payum.action.sylius.payum_action.execute_same_request_with_payment_details' => '@sylius.payum_action.execute_same_request_with_payment_details', 'payum.action.sylius.resolve_next_route' => '@sylius.payum_action.resolve_next_route', 'payum.extension.psr_logger' => '@payum.extension.logger', 'payum.extension.log_executed_actions' => '@payum.extension.log_executed_actions', 'payum.extension.profile_collector' => '@payum.profiler.payum_collector', 'payum.prepend_extensions' => [0 => 'payum.extension.profile_collector', 1 => 'payum.extension.sylius.payum_extension.update_payment_state'], 'payum.extension.payum.extension.storage.app_entity_order_order' => '@payum.extension.storage.app_entity_order_order', 'payum.extension.payum.extension.storage.app_entity_payment_payment' => '@payum.extension.storage.app_entity_payment_payment', 'payum.extension.sylius.payum_extension.update_payment_state' => '@sylius.payum_extension.update_payment_state']);
$instance->addGatewayFactoryConfig('paypal_express_checkout', ['payum.action.sylius.paypal_express_checkout.convert_payment' => '@sylius.payum_action.paypal_express_checkout.convert_payment']);
$instance->addGatewayFactoryConfig('offline', ['payum.action.sylius.offline.convert_payment' => '@sylius.payum_action.offline.convert_payment', 'payum.action.sylius.offline.status' => '@sylus.payum_action.offline.status', 'payum.action.sylius.offline.resolve_next_route' => '@sylius.payum_action.offline.resolve_next_route']);
$instance->addGatewayFactoryConfig('omnipay_paysera', ['payum.action.payum.action.convert_payment' => '@omni_sylius.payum.paysera.action.convert_payment', 'payum.action.payum.action.notify' => '@omni_sylius.payum.paysera.action.notify']);
$instance->addGatewayFactoryConfig('swedbank_spp', ['payum.action.payum.action.capture' => '@Omni\\Sylius\\SwedbankSpp\\Payum\\Action\\CreditCard\\CaptureAction', 'payum.action.payum.action.status' => '@Omni\\Sylius\\SwedbankSpp\\Payum\\Action\\CreditCard\\StatusAction']);
$instance->addGatewayFactory('omnipay_paysera', new \Payum\Core\Bridge\Symfony\Builder\GatewayFactoryBuilder(new \Omni\Sylius\PayseraPlugin\OmniPayPayseraGatewayFactory()));
$instance->addGatewayFactory('swedbank_spp', new \Payum\Core\Bridge\Symfony\Builder\GatewayFactoryBuilder('Omni\\Sylius\\SwedbankSpp\\Factory\\CreditCard\\CreditCardGatewayFactory'));
$instance->addGatewayFactoryConfig('omnipay_paysera', $this->getParameter('omni_sylius_paysera.options'));

return $instance;
