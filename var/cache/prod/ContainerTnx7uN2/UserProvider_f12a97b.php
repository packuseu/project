<?php
include_once \dirname(__DIR__, 4).'/vendor/symfony/security/Core/User/UserProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Provider/UserProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Provider/AbstractUserProvider.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/UserBundle/Provider/UsernameOrEmailProvider.php';
include_once \dirname(__DIR__, 4).'/vendor/hwi/oauth-bundle/Connect/AccountConnectorInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/hwi/oauth-bundle/Security/Core/User/OAuthAwareUserProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/sylius/src/Sylius/Bundle/CoreBundle/OAuth/UserProvider.php';

class UserProvider_f12a97b extends \Sylius\Bundle\CoreBundle\OAuth\UserProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Sylius\Bundle\CoreBundle\OAuth\UserProvider|null wrapped object, if the proxy is initialized
     */
    private $valueHoldera8ec9 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer1f165 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties11497 = [
        
    ];

    public function loadUserByOAuthUserResponse(\HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response) : \Symfony\Component\Security\Core\User\UserInterface
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'loadUserByOAuthUserResponse', array('response' => $response), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->loadUserByOAuthUserResponse($response);
    }

    public function connect(\Symfony\Component\Security\Core\User\UserInterface $user, \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response) : void
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'connect', array('user' => $user, 'response' => $response), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $this->valueHoldera8ec9->connect($user, $response);
return;
    }

    public function loadUserByUsername($username) : \Symfony\Component\Security\Core\User\UserInterface
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'loadUserByUsername', array('username' => $username), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->loadUserByUsername($username);
    }

    public function refreshUser(\Symfony\Component\Security\Core\User\UserInterface $user) : \Symfony\Component\Security\Core\User\UserInterface
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'refreshUser', array('user' => $user), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->refreshUser($user);
    }

    public function supportsClass($class) : bool
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'supportsClass', array('class' => $class), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return $this->valueHoldera8ec9->supportsClass($class);
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        unset($instance->supportedUserClass, $instance->userRepository, $instance->canonicalizer);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\OAuth\UserProvider $instance) {
            unset($instance->oauthFactory, $instance->oauthRepository, $instance->customerFactory, $instance->userFactory, $instance->userManager, $instance->customerRepository);
        }, $instance, 'Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider')->__invoke($instance);

        $instance->initializer1f165 = $initializer;

        return $instance;
    }

    public function __construct(string $supportedUserClass, \Sylius\Component\Resource\Factory\FactoryInterface $customerFactory, \Sylius\Component\Resource\Factory\FactoryInterface $userFactory, \Sylius\Component\User\Repository\UserRepositoryInterface $userRepository, \Sylius\Component\Resource\Factory\FactoryInterface $oauthFactory, \Sylius\Component\Resource\Repository\RepositoryInterface $oauthRepository, \Doctrine\Common\Persistence\ObjectManager $userManager, \Sylius\Component\User\Canonicalizer\CanonicalizerInterface $canonicalizer, \Sylius\Component\Core\Repository\CustomerRepositoryInterface $customerRepository)
    {
        static $reflection;

        if (! $this->valueHoldera8ec9) {
            $reflection = $reflection ?? new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider');
            $this->valueHoldera8ec9 = $reflection->newInstanceWithoutConstructor();
        unset($this->supportedUserClass, $this->userRepository, $this->canonicalizer);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\OAuth\UserProvider $instance) {
            unset($instance->oauthFactory, $instance->oauthRepository, $instance->customerFactory, $instance->userFactory, $instance->userManager, $instance->customerRepository);
        }, $this, 'Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider')->__invoke($this);

        }

        $this->valueHoldera8ec9->__construct($supportedUserClass, $customerFactory, $userFactory, $userRepository, $oauthFactory, $oauthRepository, $userManager, $canonicalizer, $customerRepository);
    }

    public function & __get($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__get', ['name' => $name], $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        if (isset(self::$publicProperties11497[$name])) {
            return $this->valueHoldera8ec9->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__isset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__unset', array('name' => $name), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $realInstanceReflection = new \ReflectionClass('Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHoldera8ec9;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHoldera8ec9;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__clone', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        $this->valueHoldera8ec9 = clone $this->valueHoldera8ec9;
    }

    public function __sleep()
    {
        $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, '__sleep', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;

        return array('valueHoldera8ec9');
    }

    public function __wakeup()
    {
        unset($this->supportedUserClass, $this->userRepository, $this->canonicalizer);

        \Closure::bind(function (\Sylius\Bundle\CoreBundle\OAuth\UserProvider $instance) {
            unset($instance->oauthFactory, $instance->oauthRepository, $instance->customerFactory, $instance->userFactory, $instance->userManager, $instance->customerRepository);
        }, $this, 'Sylius\\Bundle\\CoreBundle\\OAuth\\UserProvider')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1f165 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1f165;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer1f165 && ($this->initializer1f165->__invoke($valueHoldera8ec9, $this, 'initializeProxy', array(), $this->initializer1f165) || 1) && $this->valueHoldera8ec9 = $valueHoldera8ec9;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHoldera8ec9;
    }

    public function getWrappedValueHolderValue() : ?object
    {
        return $this->valueHoldera8ec9;
    }


}
