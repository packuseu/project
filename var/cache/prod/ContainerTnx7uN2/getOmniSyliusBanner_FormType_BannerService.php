<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'omni_sylius_banner.form_type.banner' shared service.

include_once \dirname(__DIR__, 4).'/vendor/symfony/form/FormTypeInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/form/AbstractType.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Form/Type/AbstractResourceType.php';
include_once \dirname(__DIR__, 4).'/vendor/omni/sylius-banner-plugin/src/Form/Type/BannerType.php';

return $this->privates['omni_sylius_banner.form_type.banner'] = new \Omni\Sylius\BannerPlugin\Form\Type\BannerType('Omni\\Sylius\\BannerPlugin\\Model\\Banner', $this->parameters['omni_banner.form.type.banner_zone.validation_groups'], ($this->services['omni_sylius.repository.banner_position'] ?? $this->load('getOmniSylius_Repository_BannerPositionService.php')), 'Omni\\Sylius\\BannerPlugin\\Model\\BannerPosition');
