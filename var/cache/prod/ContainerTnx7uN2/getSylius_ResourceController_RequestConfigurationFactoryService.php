<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private 'sylius.resource_controller.request_configuration_factory' shared service.

include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Controller/RequestConfigurationFactoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/sylius/resource-bundle/src/Bundle/Controller/RequestConfigurationFactory.php';

return $this->privates['sylius.resource_controller.request_configuration_factory'] = new \Sylius\Bundle\ResourceBundle\Controller\RequestConfigurationFactory(($this->privates['sylius.resource_controller.parameters_parser'] ?? $this->load('getSylius_ResourceController_ParametersParserService.php')), 'Sylius\\Bundle\\ResourceBundle\\Controller\\RequestConfiguration', $this->parameters['sylius.resource.settings']);
