<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerTnx7uN2\srcApp_KernelProdDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerTnx7uN2/srcApp_KernelProdDebugContainer.php') {
    touch(__DIR__.'/ContainerTnx7uN2.legacy');

    return;
}

if (!\class_exists(srcApp_KernelProdDebugContainer::class, false)) {
    \class_alias(\ContainerTnx7uN2\srcApp_KernelProdDebugContainer::class, srcApp_KernelProdDebugContainer::class, false);
}

return new \ContainerTnx7uN2\srcApp_KernelProdDebugContainer([
    'container.build_hash' => 'Tnx7uN2',
    'container.build_id' => '49df3d0e',
    'container.build_time' => 1648727602,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerTnx7uN2');
