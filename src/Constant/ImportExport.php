<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Constant;

class ImportExport
{
    public const DEFAULT_LOCALE = 'lt_LT';
    public const DEFAULT_IMAGE_COLUMN_PREFIX = 'img_';
    public const COLLECTION_DELIMITER = ' | ';
    public const OPTION_ATTRIBUTE_TYPE = 'variant_option';
}
