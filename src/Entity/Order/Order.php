<?php

declare(strict_types=1);

namespace App\Entity\Order;

use App\Entity\Consent\ConsentSubjectAwareInterface;
use App\Entity\Consent\ConsentSubjectInterface;
use App\Entity\Customer\Customer;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Order as BaseOrder;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_order")
 */
class Order extends BaseOrder implements ConsentSubjectAwareInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConsentSubject(): ConsentSubjectInterface
    {
        /** @var Customer $customer */
        $customer = $this->getCustomer();

        return $customer;
    }
}
