<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="app_order_report")
 */
class OrderReport implements ResourceInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", name="date_from")
     */
    private $from;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", name="date_to")
     */
    private $to;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="file_path")
     */
    private $filePath;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFrom(): ?\DateTime
    {
        return $this->from;
    }

    public function setFrom(?\DateTime $from): void
    {
        $this->from = $from;
    }

    public function getTo(): ?\DateTime
    {
        return $this->to;
    }

    public function setTo(?\DateTime $to): void
    {
        $this->to = $to;
    }

    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    public function setFilePath(?string $filePath): void
    {
        $this->filePath = $filePath;
    }
}
