<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Brille24\SyliusTierPricePlugin\Entity\ProductVariantInterface;
use Brille24\SyliusTierPricePlugin\Entity\TierPriceInterface;
use Brille24\SyliusTierPricePlugin\Traits\TierPriceableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\ProductVariant as BaseProductVariant;
use Sylius\Component\Product\Model\ProductVariantTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_product_variant")
 */
class ProductVariant extends BaseProductVariant implements ProductVariantInterface
{
    use TierPriceableTrait;

    /**
     * @var TierPriceInterface[]|ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Brille24\SyliusTierPricePlugin\Entity\TierPriceInterface",
     *     mappedBy="productVariant",
     *     orphanRemoval=true,
     *     cascade={"all"}
     * )
     */
    protected $tierPrices;

    /**
     * @var bool
     *
     * @ORM\Column(name="non_sellable", type="boolean")
     */
    protected $nonSellable = false;

    /**
     * @var int
     *
     * @ORM\Column(name="max_sellable", type="integer", nullable=true)
     */
    protected $maxSellable;

    /**
     * @var int
     *
     * @ORM\Column(name="min_sellable", type="integer", nullable=true)
     */
    protected $minSellable;

    public function __construct()
    {
        parent::__construct();

        $this->initTierPriceableTrait();
    }

    public function isNonSellable(): bool
    {
        return $this->nonSellable;
    }

    public function setNonSellable(bool $nonSellable): void
    {
        $this->nonSellable = $nonSellable;
    }

    public function getMaxSellable(): ?int
    {
        return $this->maxSellable;
    }

    public function setMaxSellable(?int $maxSellable): void
    {
        $this->maxSellable = $maxSellable;
    }

    protected function createTranslation(): ProductVariantTranslationInterface
    {
        return new ProductVariantTranslation();
    }

    /**
     * @return int|null
     */
    public function getMinSellable(): ?int
    {
        return $this->minSellable;
    }

    /**
     * @param int $minSellable
     */
    public function setMinSellable(?int $minSellable): void
    {
        $this->minSellable = $minSellable;
    }
}
