<?php

declare(strict_types=1);

namespace App\Entity\Product;

use Doctrine\ORM\Mapping as ORM;
use Omni\Sylius\FilterPlugin\Model\SearchTagAwareInterface;
use Omni\Sylius\FilterPlugin\Model\Traits\SearchTagAwareTrait;
use Sylius\Component\Core\Model\Product as BaseProduct;
use Sylius\Component\Product\Model\ProductTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="sylius_product",
 *     indexes={
 *         @ORM\Index(name="fulltext_search_idx", columns={"tagIndex"}, flags={"fulltext"})
 *     }
 * )
 */
class Product extends BaseProduct implements SearchTagAwareInterface
{
    use SearchTagAwareTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity_step", type="integer", nullable=true)
     */
    private $quantityStep;

    protected function createTranslation(): ProductTranslationInterface
    {
        return new ProductTranslation();
    }

    /**
     * @return int
     */
    public function getQuantityStep(): ?int
    {
        return $this->quantityStep;
    }

    /**
     * @param int $quantityStep
     */
    public function setQuantityStep(?int $quantityStep): self
    {
        $this->quantityStep = $quantityStep;

        return $this;
    }
}
