<?php

declare(strict_types=1);

namespace App\Entity\Shipping;

use Doctrine\ORM\Mapping as ORM;
use Omni\Sylius\ParcelMachinePlugin\Model\Traits\ParcelMachineProviderCodeAwareTrait;
use Sylius\Component\Core\Model\ShippingMethod as BaseShippingMethod;
use Sylius\Component\Shipping\Model\ShippingMethodTranslationInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_shipping_method")
 */
class ShippingMethod extends BaseShippingMethod
{
    use ParcelMachineProviderCodeAwareTrait;

    /**
     * @var string|null
     * @ORM\Column(name="parcel_machine_provider_code", nullable=true, type="string")
     */
    private $parcelMachineProviderCode;

    public function getAmountForChannel(string $channel): ?int
    {
        $channelConfig = $this->configuration[$channel] ?? null;

        if (!$channelConfig) {
            return null;
        }

        return $channelConfig['amount'] ?? null;
    }

    protected function createTranslation(): ShippingMethodTranslationInterface
    {
        return new ShippingMethodTranslation();
    }
}
