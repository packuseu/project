<?php

declare(strict_types=1);

namespace App\Entity\Shipping;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Omni\Sylius\ShippingPlugin\Model\Traits\ParcelMachineAwareTrait;
use Omni\Sylius\ShippingPlugin\Model\Traits\ShippingExportAwareTrait;
use Omni\Sylius\ShippingPlugin\Model\Traits\ShippingPayOnDeliveryAwareTrait;
use Omni\Sylius\ShippingPlugin\Model\Traits\ShippingShipperAwareTrait;
use Omni\Sylius\ShippingPlugin\Model\Traits\ShippingUnitAwareTrait;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\Shipment as BaseShipment;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_shipment")
 */
class Shipment extends BaseShipment
{
    // TODO: Uncomment when annotation is fixed
//    use ShippingPayOnDeliveryAwareTrait;
    use ShippingShipperAwareTrait;
    use ShippingUnitAwareTrait;
    use ParcelMachineAwareTrait;

    /**
     * TODO: Remove when ShippingPayOnDeliveryAwareTrait annotation is fixed
     * @var int
     * @ORM\Column(name="shipment_total", nullable=true, type="integer")
     */
    private $shipmentTotal = 0;

    /**
     * @var ChannelInterface|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Channel\Channel")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    private $sender;

    /**
     * @var ParcelMachineInterface|null
     * @ORM\ManyToOne(targetEntity="Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface")
     * @ORM\JoinColumn(name="parcel_machine_id", referencedColumnName="id")
     */
    private $parcelMachine;

    /**
     * @var ShippingExportInterface
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Shipping\ShippingExport", mappedBy="shipment")
     */
    private $shippingExports;

    /**
     * @var int|null
     *
     * @ORM\Column(name="parcel_count", nullable=true, type="integer")
     */
    private $parcelCount;

    public function __construct()
    {
        $this->shippingExports = new ArrayCollection();

        parent::__construct();
    }

    /**
     * TODO: Remove when ShippingPayOnDeliveryAwareTrait annotation is fixed
     * @return int
     */
    public function getShipmentTotal(): int
    {
        return $this->shipmentTotal;
    }

    /**
     * TODO: Remove when ShippingPayOnDeliveryAwareTrait annotation is fixed
     * @param int $shipmentTotal
     * @return self
     */
    public function setShipmentTotal(int $shipmentTotal): self
    {
        $this->shipmentTotal = $shipmentTotal;

        return $this;
    }

    /**
     * @return ChannelInterface|null
     */
    public function getSender(): ?ChannelInterface
    {
        return $this->sender;
    }

    /**
     * @param ChannelInterface|null $sender
     *
     * @return Shipment
     */
    public function setSender(?ChannelInterface $sender): Shipment
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @return ShippingExportInterface[]|ArrayCollection
     */
    public function getShippingExports(): Collection
    {
        return $this->shippingExports;
    }

    /**
     * @param ShippingExportInterface|null $shippingExport
     *
     * @return $this
     */
    public function addShippingExport(ShippingExportInterface $shippingExport)
    {
        if (!$this->shippingExports->contains($shippingExport)) {
            $this->shippingExports->add($shippingExport);
        }

        return $this;
    }

    /**
     * @param ShippingExportInterface|null $shippingExport
     *
     * @return $this
     */
    public function removeShippingExport(ShippingExportInterface $shippingExport)
    {
        if ($this->shippingExports->contains($shippingExport)) {
            $this->shippingExports->remove($shippingExport);
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getParcelCount(): ?int
    {
        return $this->parcelCount;
    }

    /**
     * @param int|null $parcelCount
     */
    public function setParcelCount(?int $parcelCount): void
    {
        $this->parcelCount = $parcelCount;
    }
}
