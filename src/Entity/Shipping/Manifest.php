<?php

declare(strict_types=1);

namespace App\Entity\Shipping;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Channel\Channel;
use Omni\Sylius\ManifestPlugin\Model\Manifest as BaseManifest;
use Sylius\Component\Addressing\Model\AddressInterface;
use Sylius\Component\Channel\Model\ChannelAwareInterface;
use Sylius\Component\Channel\Model\ChannelInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="omni_manifest")
 */
class Manifest extends BaseManifest implements ChannelAwareInterface
{
    /**
     * @var ChannelInterface|Channel
     *
     * @ORM\ManyToOne(targetEntity="Sylius\Component\Channel\Model\ChannelInterface")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     */
    protected $channel;

    /**
     * @return Channel|ChannelInterface
     */
    public function getChannel(): ChannelInterface
    {
        return $this->channel;
    }

    /**
     * @param Channel|ChannelInterface $channel
     */
    public function setChannel($channel): void
    {
        $this->channel = $channel;
    }

    /**
     * TODO: check on this!
     *
     * @return bool
     */
    public function isCallCourier(): bool
    {
        return true;
    }

    /**
     * @return AddressInterface
     */
    public function getAddress(): AddressInterface
    {
        if (!count($this->shippingExports)) {
            return null;
        }

        /** @var Channel $channel */
        $channel = $this->shippingExports->first()->getShipment()->getSender();

        return $channel->getAddress();
    }

    /**
     * @return string
     */
    public function getWorkingHoursUntil(): string
    {
        return '18:00:00';
    }

    /**
     * @return string
     */
    public function getWorkingHoursFrom(): string
    {
        return '10:00:00';
    }

    /**
     * @return int
     */
    public function getPalletsCount(): int
    {
        return 0;
    }

    /**
     * @return Channel|null
     */
    public function getSender(): ?Channel
    {
        if ($this->channel) {
            return $this->channel;
        }

        return $this->shippingExports->first()->getShipment()->getSender();
    }
}
