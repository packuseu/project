<?php

declare(strict_types=1);

namespace App\Entity\Shipping;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport as BaseExport;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\ShipmentInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="bitbag_shipping_export")
 */
class ShippingExport extends BaseExport
{
    /**
     * @ORM\ManyToOne(targetEntity="Sylius\Component\Shipping\Model\ShipmentInterface", cascade={"ALL"})
     * @ORM\JoinColumn(name="shipment_id", referencedColumnName="id")
     *
     * @var ShipmentInterface
     */
    protected $shipment;

}
