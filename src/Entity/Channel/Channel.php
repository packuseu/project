<?php

declare(strict_types=1);

namespace App\Entity\Channel;

use App\Entity\Addressing\Address;
use App\Entity\Shipping\ShipperConfig;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Omni\Sylius\CorePlugin\Model\ChannelCountryTrait;
use Omni\Sylius\CorePlugin\Model\ChannelLogoTrait;
use Omni\Sylius\CorePlugin\Model\ChannelWatermarkTrait;
use Omni\Sylius\CorePlugin\Model\LogosAwareInterface;
use Omni\Sylius\ShippingPlugin\Model\Traits\SenderShipperConfigAwareTrait;
use Sylius\Component\Addressing\Model\AddressInterface;
use Sylius\Component\Addressing\Model\CountryInterface;
use Sylius\Component\Core\Model\Channel as BaseChannel;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Core\Model\ImagesAwareInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_channel")
 */
class Channel extends BaseChannel implements ImagesAwareInterface, LogosAwareInterface
{
    use SenderShipperConfigAwareTrait;

    use ChannelWatermarkTrait{
        ChannelWatermarkTrait::__construct as private _ChannelWatermarkConstruct;
    }
    use ChannelCountryTrait {
        ChannelCountryTrait::__construct as private _ChannelCountryConstruct;
    }
    use ChannelLogoTrait {
        ChannelLogoTrait::__construct as private _ChannelLogoConstruct;
    }

    public function __construct()
    {
        parent::__construct();

        $this->_ChannelWatermarkConstruct();
        $this->_ChannelCountryConstruct();
        $this->_ChannelLogoConstruct();
    }

    /**
     * @var Collection|ImageInterface[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Omni\Sylius\CorePlugin\Model\ChannelWatermarkInterface",
     *     mappedBy="owner",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $images;

    /**
     * @var Collection|ImageInterface[]
     *
     * @ORM\OneToMany(
     *     targetEntity="Omni\Sylius\CorePlugin\Model\ChannelLogoInterface",
     *     mappedBy="owner",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $logos;

    /**
     * @var Collection|CountryInterface[]
     *
     * @ORM\ManyToMany(targetEntity="Sylius\Component\Addressing\Model\CountryInterface")
     * @ORM\JoinTable(name="omni_channel_country",
     *      joinColumns={@ORM\JoinColumn(name="channel_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="country_id", referencedColumnName="id")}
     * )
     */
    protected $countries;

    /**
     * @var Collection|ShipperConfig[]
     * @ORM\OneToMany(targetEntity="App\Entity\Shipping\ShipperConfig",
     *     mappedBy="sender",
     *     cascade={"persist"}
     * )
     */
    protected $shipperConfigs;

    /**
     * @var Address|null
     *
     * @ORM\OneToOne(
     *     targetEntity="Sylius\Component\Addressing\Model\AddressInterface",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     */
    protected $address;

    /**
     * @var int
     *
     * @ORM\Column(name="minimal_cart_size", type="integer", nullable=true)
     */
    protected $minimalCartSize;

    /**
     * @param ShipperConfig $shipperConfig
     */
    public function addShipperConfig(ShipperConfig $shipperConfig): void
    {
        $shipperConfig->setSender($this);
        $this->shipperConfigs->add($shipperConfig);
    }

    /**
     * @return Collection|null
     */
    public function getShipperConfigs(): ?Collection
    {
        return $this->shipperConfigs;
    }

    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }

    /**
     * @param Address|null $address
     */
    public function setAddress(?Address $address): void
    {
        $this->address = $address;
    }

    /**
     * @return int|null
     */
    public function getMinimalCartSize(): ?int
    {
        return $this->minimalCartSize;
    }

    /**
     * @param int $minimalCartSize
     */
    public function setMinimalCartSize(?int $minimalCartSize): void
    {
        $this->minimalCartSize = $minimalCartSize;
    }
}
