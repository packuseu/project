<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Entity\Consent;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="nfq_consent_agreement")
 */
class ConsentAgreement
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Consent
     *
     * @ORM\ManyToOne(targetEntity="Consent")
     */
    private $consent;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $agreed;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    public function __construct()
    {
        $this->datetime = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ConsentAgreement
     */
    public function setId(int $id): ConsentAgreement
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Consent
     */
    public function getConsent(): Consent
    {
        return $this->consent;
    }

    /**
     * @param Consent $consent
     *
     * @return ConsentAgreement
     */
    public function setConsent(Consent $consent): ConsentAgreement
    {
        $this->consent = $consent;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAgreed(): bool
    {
        return $this->agreed;
    }

    /**
     * @param bool $agreed
     *
     * @return ConsentAgreement
     */
    public function setAgreed(bool $agreed): ConsentAgreement
    {
        $this->agreed = $agreed;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDatetime(): \DateTimeInterface
    {
        return $this->datetime;
    }

    /**
     * @param \DateTimeInterface $datetime
     *
     * @return ConsentAgreement
     */
    public function setDatetime(\DateTimeInterface $datetime): ConsentAgreement
    {
        $this->datetime = $datetime;

        return $this;
    }
}
