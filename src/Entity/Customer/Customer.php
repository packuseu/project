<?php

declare(strict_types=1);

namespace App\Entity\Customer;

use App\Entity\Consent\ConsentAgreement;
use App\Entity\Consent\ConsentSubjectInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Customer as BaseCustomer;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_customer")
 */
class Customer extends BaseCustomer implements ConsentSubjectInterface
{
    /**
     * @var ConsentAgreement[]
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Consent\ConsentAgreement", cascade={"PERSIST"})
     * @ORM\JoinTable(
     *     name="sylius_customer_consent_agreement",
     *     joinColumns={@ORM\JoinColumn(name="customer_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="consent_agreement_id", referencedColumnName="id", unique=true)},
     * )
     */
    private $consentAgreements;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        parent::__construct();

        $this->consentAgreements = new ArrayCollection();
    }

    /**
     * @return ConsentAgreement[]
     */
    public function getConsentAgreements(): array
    {
        return $this->consentAgreements;
    }

    /**
     * @param ConsentAgreement $consentAgreement
     */
    public function addConsentAgreement(ConsentAgreement $consentAgreement): void
    {
        if (false === $this->consentAgreements->contains($consentAgreement)) {
            $this->consentAgreements->add($consentAgreement);
        }
    }

    /**
     * @param ConsentAgreement $consentAgreement
     */
    public function removeConsentAgreement(ConsentAgreement $consentAgreement): void
    {
        if ($this->consentAgreements->contains($consentAgreement)) {
            $this->consentAgreements->removeElement($consentAgreement);
        }
    }
}
