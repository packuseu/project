<?php

declare(strict_types=1);

namespace App\Entity\Addressing;

use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Core\Model\Address as BaseAddress;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_address")
 */
class Address extends BaseAddress
{
    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $houseNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $apartmentNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="vat_code", type="string", length=36, nullable=true)
     */
    private $vatCode;

    /**
     * @var string
     *
     * @ORM\Column(name="company_code", type="string", length=36, nullable=true)
     */
    private $companyCode;

    /**
     * @var string
     */
    private $phoneNumberPrefix = '+370';

    /**
     * @var string
     */
    private $phoneNumberWithoutPrefix;

    /**
     * @return string|null
     */
    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    /**
     * @param string|null $apartmentNumber
     * @return self
     */
    public function setApartmentNumber(?string $apartmentNumber): self
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    /**
     * @param string|null $houseNumber
     * @return self
     */
    public function setHouseNumber(?string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumberPrefix(): string
    {
        return $this->phoneNumberPrefix;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumberWithoutPrefix(): ?string
    {
        if (null !== $this->phoneNumberWithoutPrefix) {
            return $this->phoneNumberWithoutPrefix;
        }

        return ltrim($this->phoneNumber ?? '', $this->phoneNumberPrefix);
    }

    /**
     * @param string|null $phoneNumberWithoutPrefix
     *
     * @return $this
     */
    public function setPhoneNumberWithoutPrefix(?string $phoneNumberWithoutPrefix): Address
    {
        $this->phoneNumberWithoutPrefix = $phoneNumberWithoutPrefix;

        return $this;
    }

    /**
     * @return string
     */
    public function getCompanyCode(): ?string
    {
        return $this->companyCode;
    }

    /**
     * @param string $companyCode
     */
    public function setCompanyCode(?string $companyCode)
    {
        $this->companyCode = $companyCode;
    }

    /**
     * @return string
     */
    public function getVatCode(): ?string
    {
        return $this->vatCode;
    }

    /**
     * @param string $vatCode
     */
    public function setVatCode(?string $vatCode)
    {
        $this->vatCode = $vatCode;
    }
}
