<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Command;

use SitemapPlugin\Builder\SitemapBuilderInterface;
use SitemapPlugin\Renderer\SitemapRendererInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Routing\RequestContext;

class GenerateSiteMapCommand extends Command
{
    private const PATH = __DIR__ . '/../../public/sitemap/';

    /**
     * @var string
     */
    protected static $defaultName = 'app:generate:sitemap';

    /**
     * @var SitemapRendererInterface
     */
    private $sitemapRenderer;

    /**
     * @var SitemapBuilderInterface
     */
    private $sitemapBuilder;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var RequestContext
     */
    private $requestContext;

    /**
     * @param SitemapRendererInterface $sitemapRenderer
     * @param SitemapBuilderInterface $sitemapBuilder
     * @param Filesystem $filesystem
     * @param RequestContext $requestContext
     */
    public function __construct(
        SitemapRendererInterface $sitemapRenderer,
        SitemapBuilderInterface $sitemapBuilder,
        Filesystem $filesystem,
        RequestContext $requestContext
    ) {
        parent::__construct();

        $this->sitemapRenderer = $sitemapRenderer;
        $this->sitemapBuilder = $sitemapBuilder;
        $this->filesystem = $filesystem;
        $this->requestContext = $requestContext;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->requestContext->setHost(getenv('SITEMAP_HOST'));

        $this->generate('products');
        $this->generate('nodes');

        return 0;
    }

    /**
     * @param string $type
     */
    private function generate(string $type): void
    {
        $productsSitemap = $this->sitemapRenderer->render($this->sitemapBuilder->build([$type]));

        $filepath = sprintf('%s/%s.xml', self::PATH, $type);

        $this->filesystem->dumpFile($filepath, $productsSitemap);
    }
}
