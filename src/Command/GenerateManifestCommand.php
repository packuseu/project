<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Command;

use App\Entity\Channel\Channel;
use App\Repository\Shipping\ShipmentRepository;
use BitBag\SyliusShippingExportPlugin\Repository\ShippingGatewayRepository;
use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ManifestPlugin\Doctrine\ORM\ManifestRepository;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGenerationProviderPicker;
use Omni\Sylius\ManifestPlugin\Manager\ManifestGeneratorManager;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Shipping\Model\ShipmentInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGeneratorInterface;
use App\Entity\Shipping\Manifest;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateManifestCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:manifest:generate';

    /**
     * @var RepositoryInterface
     */
    protected $channelsRepository;

    /**
     * @var RepositoryInterface|ShippingGatewayRepository
     */
    protected $shippingGatewayRepository;

    /**
     * @var RepositoryInterface|ShipmentRepository
     */
    protected $shipmentRepository;

    /**
     * @var RepositoryInterface|ManifestRepository
     */
    protected $manifestRepository;

    /**
     * @var ManifestGeneratorManager
     */
    protected $manifestGeneratorManager;

    /**
     * @var ManifestGenerationProviderPicker
     */
    protected $providerPicker;

    /**
     * @var FactoryInterface
     */
    protected $manifestFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var array
     */
    protected $shipmentsReadyForCourierStates;

    public function __construct(
        RepositoryInterface $channelsRepository,
        RepositoryInterface $shippingGatewayRepository,
        RepositoryInterface $shipmentRepository,
        RepositoryInterface $manifestRepository,
        ManifestGeneratorManager $manifestGeneratorManager,
        ManifestGenerationProviderPicker $providerPicker,
        FactoryInterface $manifestFactory,
        EntityManagerInterface $em,
        array $shipmentsReadyForCourierStates
    ) {
        parent::__construct();

        $this->channelsRepository = $channelsRepository;
        $this->shippingGatewayRepository = $shippingGatewayRepository;
        $this->shipmentRepository = $shipmentRepository;
        $this->manifestRepository = $manifestRepository;
        $this->manifestGeneratorManager = $manifestGeneratorManager;
        $this->providerPicker = $providerPicker;
        $this->manifestFactory = $manifestFactory;
        $this->em = $em;
        $this->shipmentsReadyForCourierStates = $shipmentsReadyForCourierStates;
    }

    protected function configure()
    {
        $this
            ->addArgument('channel', InputArgument::REQUIRED, 'Which channel do you want to generate manifest for?')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $channel = $this->channelsRepository->findOneBy(['code' => $input->getArgument('channel')]);

        if (!$channel) {
            $output->writeln('No such channel found, aborting.');

            return 1;
        }

        $generated = $this->generateManifest($channel, 'dpd');
        $output->writeln("Manifest generation successful. $generated exports found for manifest.");

        return 0;
    }

    protected function generateManifest(Channel $channel, string $gatewayCode): int
    {
        $gateway = $this->shippingGatewayRepository->findOneBy(['code' => $gatewayCode]);
        $exports = $this->getShipmentExports($channel, $gatewayCode);

        if (count($exports) > 0) {
            /** @var Manifest $manifest */
            $manifest = $this->manifestFactory->createNew();

            $manifest->setShippingGateway($gateway);
            $manifest->setShippingExports($exports);
            $manifest->setChannel($channel);

            $path = $this->manifestGeneratorManager->generate($manifest);
            $manifest->setPath($path);
            $this->em->persist($manifest);
            $this->em->flush();
        }

        return count($exports);
    }

    protected function getShipmentExports(Channel $channel, string $gatewayCode): array
    {
        $states = $this->shipmentsReadyForCourierStates;
        $shipments = $this->shipmentRepository->findShipmentsForManifest($states, $channel);

        $exports = [];

        foreach ($shipments as $shipment) {
            $gateway = $this->shippingGatewayRepository->findOneByShippingMethod($shipment->getMethod());

            if (!$gateway instanceof ShippingGatewayInterface
                || strpos($gateway->getCode(), $gatewayCode) === false
            ) {
                continue;
            }

            foreach ($shipment->getShippingExports() as $export) {
                if (!$export) {
                    continue;
                }

                $manifest = $this->manifestRepository->findManifestByExport($export);

                if (!$manifest instanceof Manifest) {
                    $exports[] = $export;
                }
            }
        }

        return $exports;
    }
}
