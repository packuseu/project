<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Transitions;

class ShippingStates
{
    public const GRAPH = 'sylius_shipment';

    public const STATE_CART = 'cart';
    public const STATE_NEW = 'new';
    public const STATE_IN_PROGRESS = 'in_progress';
    public const STATE_PICKUP_READY = 'pickup_ready';
    public const STATE_PARTIALLY_PICKUP_READY = 'partially_pickup_ready';
    public const STATE_COURIER_READY = 'courier_ready';
    public const STATE_LABEL_GENERATED = 'label_generated';
    public const STATE_PARTIALLY_COURIER_READY = 'partially_courier_ready';
    public const STATE_IN_TRANSIT = 'in_transit';
    public const STATE_CANT_DELIVER = 'cant_deliver';
    public const STATE_BACK_TO_SELLER = 'back_to_seller';
    public const STATE_SHIPPED = 'shipped';
    public const STATE_CANCELLED = 'cancelled';
    public const STATE_ERROR = 'error';

    public const TRANSITION_IN_TRANSIT = 'in_transit';
    public const TRANSITION_PARTIALLY_IN_TRANSIT = 'partially_in_transit';
    public const TRANSITION_IN_PROGRESS = 'in_progress';
    public const TRANSITION_PARTIALLY_PICKUP_READY = 'partially_pickup_ready';
    public const TRANSITION_PICKUP_READY = 'pickup_ready';
    public const TRANSITION_PARTIALLY_COURIER_READY = 'partially_courier_ready';
    public const TRANSITION_PARTIALLY_LABEL_GENERATED = 'partially_label_generated';
    public const TRANSITION_LABEL_GENERATED = 'label_generated';
    public const TRANSITION_COURIER_READY = 'courier_ready';
    public const TRANSITION_CANCEL = 'cancelled';
}
