<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace App\Importer\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Omni\Sylius\ImportPlugin\Processor\ProductProcessor as BaseProductProcessor;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Product\Generator\SlugGeneratorInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Shipping\Model\ShippingCategoryInterface;

class ProductProcessor extends BaseProductProcessor
{
    /** @var RepositoryInterface */
    protected $shippingCategoryRepository;

    public function __construct(
        ResourcesCache $cache,
        EntityManagerInterface $em,
        FactoryInterface $productFactory,
        FactoryInterface $productVariantFactory,
        FactoryInterface $productAttributeValueFactory,
        FactoryInterface $productTaxonFactory,
        FactoryInterface $channelPricingFactory,
        FactoryInterface $attributeFactory,
        RepositoryInterface $productRepository,
        RepositoryInterface $productVariantRepository,
        SlugGeneratorInterface $slugGenerator,
        RepositoryInterface $shippingCategoryRepository
    ) {
        parent::__construct(
            $cache,
            $em,
            $productFactory,
            $productVariantFactory,
            $productAttributeValueFactory,
            $productTaxonFactory,
            $channelPricingFactory,
            $attributeFactory,
            $productRepository,
            $productVariantRepository,
            $slugGenerator
        );

        $this->shippingCategoryRepository = $shippingCategoryRepository;
    }

    protected function doProcess(array $data): void
    {
        $product = $this->getProduct($data);
        $variant = $this->getProductVariant($data['code'], $product);

        unset($data['code']);
        unset($data['parent']);

        $this->setDetails($product, $variant, $data);
        $this->handleVariant($variant, $data);
        $this->handleChannels($product, $data);
        $this->handleTaxons($product, $data);
        $this->handleAttributes($product, $variant, $data);

        /** Custom project fields */
        $this->handleQuantityStep($product, $variant, $data);
        $this->handleSoldInShop($product, $variant, $data);
        $this->handleShippingCategory($product, $variant, $data);
        $this->handleWeight($product, $variant, $data);

        $this->cache->cacheProduct($product);
        $this->cache->cacheVariant($variant);
        $this->em->persist($product);
    }

    protected function handleQuantityStep(ProductInterface $product, ProductVariantInterface $variant, array $data): void
    {
        if (isset($data['custom-quantity_step'])) {
            $product->setQuantityStep((int) $data['custom-quantity_step']);

        }
    }

    protected function handleSoldInShop(ProductInterface $product, ProductVariantInterface $variant, array $data): void
    {
        if (isset($data['custom-min_amount_sold_in_shop'])) {
            $variant->setMinSellable((int) $data['custom-min_amount_sold_in_shop']);
        }
        if (isset($data['custom-max_amount_sold_in_shop'])) {
            $variant->setMaxSellable((int) $data['custom-max_amount_sold_in_shop']);
        }
    }

    protected function handleShippingCategory(ProductInterface $product, ProductVariantInterface $variant, array $data): void
    {
        if (isset($data['custom-shipping_category'])) {
            /** @var ShippingCategoryInterface|null $shippingCategory */
            $shippingCategory = $this->shippingCategoryRepository->findOneBy(['code' => $data['custom-shipping_category']]);

            $variant->setShippingCategory($shippingCategory);
        }
    }

    protected function handleWeight(ProductInterface $product, ProductVariantInterface $variant, array $data): void
    {
        if (isset($data['custom-weight'])) {
            $variant->setWeight((float) $data['custom-weight']);
        }
    }
}
