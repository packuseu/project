<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\Form\FormError;

class QuoteValidationException extends \Exception
{
    /**
     * @var FormError[]
     */
    protected $errors = [];

    public function __construct(string $message, array $errors)
    {
        parent::__construct($message);

        $this->errors = $errors;
    }

    /**
     * @return FormError[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
