<?php

declare(strict_types=1);

namespace App\Sitemap\Provider;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use SitemapPlugin\Factory\SitemapUrlFactoryInterface;
use SitemapPlugin\Model\ChangeFrequency;
use SitemapPlugin\Model\SitemapUrlInterface;
use SitemapPlugin\Provider\UrlProviderInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductTranslationInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Locale\Model\LocaleInterface;
use Sylius\Component\Resource\Model\TranslationInterface;
use Symfony\Component\Routing\RouterInterface;

final class ProductUrlProvider implements UrlProviderInterface
{
    private const BATCH_SIZE = 100;

    /**
     * @var ProductRepositoryInterface|EntityRepository
     */
    private $productRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SitemapUrlFactoryInterface
     */
    private $sitemapUrlFactory;

    /**
     * @var LocaleContextInterface
     */
    private $localeContext;

    /**
     * @var ChannelContextInterface
     */
    private $channelContext;

    /**
     * @var array
     */
    private $urls = [];

    /**
     * @var array
     */
    private $channelLocaleCodes;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param RouterInterface $router
     * @param SitemapUrlFactoryInterface $sitemapUrlFactory
     * @param LocaleContextInterface $localeContext
     * @param ChannelContextInterface $channelContext
     * @param EntityManagerInterface $em
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        RouterInterface $router,
        SitemapUrlFactoryInterface $sitemapUrlFactory,
        LocaleContextInterface $localeContext,
        ChannelContextInterface $channelContext,
        EntityManagerInterface $em
    ) {
        $this->productRepository = $productRepository;
        $this->router = $router;
        $this->sitemapUrlFactory = $sitemapUrlFactory;
        $this->localeContext = $localeContext;
        $this->channelContext = $channelContext;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function generate(): iterable
    {
        $offset = 0;

        do {
            $products = $this->getProducts($offset, self::BATCH_SIZE);

            foreach ($products as $product) {
                $this->urls[] = $this->createProductUrl($product);
            }

            $offset += self::BATCH_SIZE;
            $this->em->clear();
        } while (\count($products) === self::BATCH_SIZE);

        return $this->urls;
    }

    /**
     * @param ProductInterface $product
     * @return Collection
     */
    private function getTranslations(ProductInterface $product): Collection
    {
        return $product->getTranslations()->filter(
            function (TranslationInterface $translation) {
                return $this->localeInLocaleCodes($translation);
            }
        );
    }

    /**
     * @param TranslationInterface $translation
     * @return bool
     */
    private function localeInLocaleCodes(TranslationInterface $translation): bool
    {
        return in_array($translation->getLocale(), $this->getLocaleCodes());
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return @return array|Collection|ProductInterface[]
     */
    private function getProducts(int $offset, int $limit): iterable
    {
        return $this->productRepository->findEnabledeByChannel($this->channelContext->getChannel(), $offset, $limit);
    }

    /**
     * @return array
     */
    private function getLocaleCodes(): array
    {
        if ($this->channelLocaleCodes === null) {
            /** @var ChannelInterface $channel */
            $channel = $this->channelContext->getChannel();

            $this->channelLocaleCodes = $channel->getLocales()->map(
                function (LocaleInterface $locale) {
                    return $locale->getCode();
                }
            )->toArray();
        }

        return $this->channelLocaleCodes;
    }

    /**
     * @param ProductInterface $product
     * @return SitemapUrlInterface
     */
    private function createProductUrl(ProductInterface $product): SitemapUrlInterface
    {
        $productUrl = $this->sitemapUrlFactory->createNew();
        $productUrl->setChangeFrequency(ChangeFrequency::always());
        $productUrl->setPriority(0.5);
        $updatedAt = $product->getUpdatedAt();
        if ($updatedAt) {
            $productUrl->setLastModification($updatedAt);
        }

        /** @var ProductTranslationInterface $translation */
        foreach ($this->getTranslations($product) as $translation) {
            $locale = $translation->getLocale();

            if (!$locale) {
                continue;
            }

            if (!$this->localeInLocaleCodes($translation)) {
                continue;
            }

            $location = $this->router->generate(
                'sylius_shop_product_show',
                [
                    'slug' => $translation->getSlug(),
                    '_locale' => $translation->getLocale(),
                ]
            );

            if ($locale === $this->localeContext->getLocaleCode()) {
                $productUrl->setLocalization($location);

                continue;
            }

            $productUrl->addAlternative($location, $locale);
        }

        return $productUrl;
    }
}
