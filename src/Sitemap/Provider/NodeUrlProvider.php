<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Sitemap\Provider;

use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Model\NodeTranslationInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;
use Omni\Sylius\CmsPlugin\UrlResolver\UrlResolverInterface;
use SitemapPlugin\Factory\SitemapUrlFactoryInterface;
use SitemapPlugin\Model\ChangeFrequency;
use SitemapPlugin\Provider\UrlProviderInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Symfony\Component\Routing\RouterInterface;

class NodeUrlProvider implements UrlProviderInterface
{
    private const EXCLUDE_NODE_TYPES = ['main_menu', 'footer_menu', 'placeholder', 'link'];

    /**
     * @var NodeRepositoryInterface
     */
    private $nodeRepository;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var SitemapUrlFactoryInterface
     */
    private $sitemapUrlFactory;

    /**
     * @var LocaleContextInterface
     */
    private $localeContext;

    /**
     * @var UrlResolverInterface
     */
    private $urlResolver;

    /**
     * @var array
     */
    private $urls = [];

    /**
     * @param NodeRepositoryInterface $nodeRepository
     * @param RouterInterface $router
     * @param SitemapUrlFactoryInterface $sitemapUrlFactory
     * @param LocaleContextInterface $localeContext
     * @param UrlResolverInterface $urlResolver
     */
    public function __construct(
        NodeRepositoryInterface $nodeRepository,
        RouterInterface $router,
        SitemapUrlFactoryInterface $sitemapUrlFactory,
        LocaleContextInterface $localeContext,
        UrlResolverInterface $urlResolver
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->router = $router;
        $this->sitemapUrlFactory = $sitemapUrlFactory;
        $this->localeContext = $localeContext;
        $this->urlResolver = $urlResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'nodes';
    }

    /**
     * {@inheritdoc}
     */
    public function generate(): iterable
    {
        /** @var NodeInterface $node */
        foreach ($this->getNodes() as $node) {
            if (in_array($node->getType(), self::EXCLUDE_NODE_TYPES)) {
                continue;
            }

            $nodeUrl = $this->sitemapUrlFactory->createNew();
            $nodeUrl->setChangeFrequency(ChangeFrequency::always());
            $nodeUrl->setPriority(0.5);

            /** @var NodeTranslationInterface $translation */
            foreach ($node->getTranslations() as $translation) {
                $location = $this->urlResolver->getNodeUrl($node)->getUrl();

                if (empty($location)) {
                    continue;
                }

                $location = trim($location);

                if ($translation->getLocale() === $this->localeContext->getLocaleCode()) {
                    $nodeUrl->setLocalization($location);

                    continue;
                }

                $locale = $translation->getLocale();
                if ($locale) {
                    $nodeUrl->addAlternative($location, $locale);
                }
            }

            if (false === empty($nodeUrl->getLocalization())) {
                $this->urls[] = $nodeUrl;
            }
        }

        return $this->urls;
    }

    /**
     * @return NodeInterface[]
     */
    private function getNodes(): iterable
    {
        return $this->nodeRepository->findAll();
    }
}
