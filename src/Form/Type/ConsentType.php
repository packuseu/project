<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Consent\Consent;
use App\Entity\Consent\ConsentAgreement;
use App\Entity\Consent\ConsentSubjectAwareInterface;
use App\Entity\Customer\Customer;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Webmozart\Assert\Assert;

class ConsentType extends AbstractType
{
    /** @var RepositoryInterface */
    private $consentRepository;

    /**
     * @param RepositoryInterface $consentRepository
     */
    public function __construct(RepositoryInterface $consentRepository)
    {
        $this->consentRepository = $consentRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var Consent[] $consents */
        $consents = $this->consentRepository->findBy(['locationCode' => $options['locationCode']]);

        foreach ($consents as $consent) {
            $constraints = [];

            if ($consent->isMandatory()) {
                $constraints[] = new IsTrue(['groups' => ['sylius'], 'message' => 'shop24.consent.required']);
            }

            $builder->add(
                'consent_' . $consent->getId(),
                CheckboxType::class,
                [
                    'required' => $consent->isMandatory(),
                    'label' => $consent->getDescription(),
                    'constraints' => $constraints,
                    'attr' => ['consent' => $consent],
                ]
            );
        }

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            static function (FormEvent $event) use ($builder) {
                /** @var ConsentSubjectAwareInterface $parentForm */
                $parentForm = $event->getForm()->getParent()->getData();
                Assert::isInstanceOf($parentForm, ConsentSubjectAwareInterface::class);

                /** @var Customer $customer */
                $consentSubject = $parentForm->getConsentSubject();
                $data = $event->getData();

                foreach ($data as $consentName => $agreed) {
                    /** @var Consent $consent */
                    $consent = $builder->get($consentName)->getOption('attr')['consent'];

                    $consentAgreement = new ConsentAgreement();
                    $consentAgreement->setConsent($consent);
                    $consentAgreement->setAgreed($agreed);

                    $consentSubject->addConsentAgreement($consentAgreement);
                }
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('locationCode');
    }
}
