<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Type;

use App\Model\Quote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuoteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'required' => true,
                    'label' => 'app.form.email',
                ]
            )
            ->add(
                'phone',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'app.form.phone',
                ]
            )
            ->add(
                'name',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'app.form.name',
                ]
            )
            ->add(
                'product',
                ChoiceType::class,
                [
                    'required' => false,
                    'label' => 'app.form.product_name',
                ]
            )
            ->add(
                'quantity',
                IntegerType::class,
                [
                    'required' => false,
                    'label' => 'app.form.quantity',
                ]
            )
            ->add(
                'deadline',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'required' => false,
                    'label' => 'app.lead_catcher.deadline',
                ]
            )
            ->add(
                'note',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'app.form.note',
                ]
            )
        ;

        $builder->get('product')->resetViewTransformers();
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'      => Quote::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            'csrf_token_id'   => 'quote_submit',
        ]);
    }
}
