<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Extension\SelectPayment;

use SM\Factory\Factory as StateMachineFactory;
use App\Entity\Consent\Consent;
use App\Entity\Order\Order;
use App\Form\Type\ConsentType;
use Sylius\Bundle\CoreBundle\Form\Type\Checkout\SelectPaymentType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Valid;

class SelectPaymentTypeExtension extends AbstractTypeExtension
{
    /** @var StateMachineFactory */
    private $stateMachineFactory;

    /**
     * @param StateMachineFactory $stateMachineFactory
     */
    public function __construct(StateMachineFactory $stateMachineFactory)
    {
        $this->stateMachineFactory = $stateMachineFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add(
            'consent',
            ConsentType::class,
            [
                'locationCode' => Consent::LOCATION_PAYMENT_METHOD,
                'label' => false,
                'mapped' => false,
                'constraints' => [
                    new Valid(['groups' => ['sylius']]),
                ],
            ]
        );

//        $builder->addEventListener(
//            FormEvents::PRE_SET_DATA,
//            function (FormEvent $event) {
//                /** @var Order $order */
//                $order = $event->getData();
//                $lastPayment = $order->getLastPayment();
//
//                $sm = $this->stateMachineFactory->get($lastPayment, 'sylius_payment');
//
//                if ($sm->can('cancel')) {
//                    $sm->apply('cancel');
//                }
//            }
//        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [SelectPaymentType::class];
    }
}
