<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Extension\Shipping;

use Omni\Sylius\ParcelMachinePlugin\Doctrine\ORM\ParcelMachineRepositoryInterface;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Omni\Sylius\ParcelMachinePlugin\Model\Traits\ParcelMachineAwareTrait;
use Omni\Sylius\ParcelMachinePlugin\Model\Traits\ParcelMachineProviderCodeAwareTrait;
use Sylius\Bundle\CoreBundle\Form\Type\Checkout\ShipmentType;
use Sylius\Component\Core\Model\ShippingMethodInterface;
use Sylius\Component\Shipping\Model\ShipmentInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ShipmentTypeExtension extends AbstractTypeExtension
{
    /**
     * @var ParcelMachineRepositoryInterface
     */
    private $repository;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var string
     */
    private $parcelMachineClass;

    /**
     * ShipmentTypeExtension constructor.
     * @param ParcelMachineRepositoryInterface $repository
     * @param string $locale
     * @param string $parcelMachineClass
     */
    public function __construct(
        string $parcelMachineClass,
        ParcelMachineRepositoryInterface $repository,
        string $locale
    ) {
        $this->repository = $repository;
        $this->locale = $locale;
        $this->parcelMachineClass = $parcelMachineClass;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            /** @var ShipmentInterface $shipment */
            $shipment = $event->getData();
            /** @var ShippingMethodInterface|ParcelMachineProviderCodeAwareTrait $method */
            $method = $shipment->getMethod();

            if (null === $shipment || null === $method) {
                return;
            }

            if (null === $method->getParcelMachineProviderCode()) {
                return;
            }

            $form
                ->add(
                    'parcelMachine',
                    EntityType::class,
                    [
                        'label' => 'app.ui.parcel_machine',
                        'class' => $this->parcelMachineClass,
                        'choice_label' => function ($parcelMachine) {
                            return $parcelMachine->getCity() . ', ' . $parcelMachine->getStreet();
                        },
                        'choices' => $this->getAddressChoices($method->getParcelMachineProviderCode()),
                        'required' => true,
                        'placeholder' => '---'
                    ]
                )
            ;
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var ShipmentInterface|ParcelMachineAwareTrait $shipment */
            $shipment = $event->getData();
            /** @var ShippingMethodInterface|ParcelMachineProviderCodeAwareTrait $method */
            $method = $shipment->getMethod();

            if ($method && null === $method->getParcelMachineProviderCode()) {
                $shipment->setParcelMachine(null);
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return ShipmentType::class;
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [ShipmentType::class];
    }

    /**
     * @param string $code
     *
     * @return ParcelMachineInterface[]
     */
    private function getAddressChoices(string $code): array
    {
        $choices = $this->repository->findEnabledByProvider($code);

        usort($choices, function ($a, $b) {
            return strcmp($a->getCity() . $a->getStreet(), $b->getCity() . $b->getStreet());
        });

        return $choices;
    }

    /**
     * @param string $code
     *
     * @return array
     */
    private function getCityChoices(string $code): array
    {
        $cities = $this->repository->getCities($code, strtoupper($this->locale));

        return array_combine($cities, $cities);
    }
}
