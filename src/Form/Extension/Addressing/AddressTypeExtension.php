<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Form\Extension\Addressing;

use App\Entity\Addressing\Address;
use Sylius\Bundle\AddressingBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class AddressTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'vatCode',
                TextType::class,
                ['label' => 'app.form.vat_code', 'required' => false]
            )
            ->add(
                'companyCode',
                TextType::class,
                ['label' => 'app.form.company_code', 'required' => false]
            )
        ;

        $builder->add(
            'phoneNumber',
            TextType::class,
            [
                'label' => 'sylius.form.address.phone_number',
                'attr' => ['placeholder' => '+37060011222'],
                'constraints' => [
                    new NotBlank(['groups' => 'sylius']),
                    new Regex(
                        [
                            'pattern' => '/^\+[\d]{11}$/',
                            'groups' => ['sylius'],
                            'message' => 'shop24.phone_number.invalid',
                        ]
                    ),
                ],
            ]
        );

        $builder->add(
            'phoneNumberWithoutPrefix',
            TextType::class,
            [
                'label' => 'sylius.form.address.phone_number',
                'attr' => ['placeholder' => '60011222'],
                'constraints' => [
                    new NotBlank(['groups' => 'sylius']),
                    new Regex(
                        [
                            'pattern' => '/^[\d]{8}$/',
                            'groups' => ['sylius'],
                            'message' => 'shop24.phone_number.invalid',
                        ]
                    ),
                ],
            ]
        );

        $builder->add(
            'postcode',
            TextType::class,
            [
                'label' => 'sylius.form.address.postcode',
                'constraints' => [
                    new Length(['min' => 5, 'max' => 5, 'groups' => ['sylius']]),
                ],
            ]
        );

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            static function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();

                if (isset($data['phoneNumber'])) {
                    $form->remove('phoneNumberWithoutPrefix');
                }

                if (isset($data['phoneNumberWithoutPrefix'])) {
                    $form->remove('phoneNumber');
                }
            }
        );

        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            static function (FormEvent $event) {
                /** @var Address $data */
                $data = $event->getData();

                if (false === $event->getForm()->has('phoneNumberWithoutPrefix')) {
                    return;
                }

                $phoneNumber = $data->getPhoneNumberPrefix() . $data->getPhoneNumberWithoutPrefix();
                $data->setPhoneNumber($phoneNumber);
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function getExtendedTypes(): iterable
    {
        return [AddressType::class];
    }
}
