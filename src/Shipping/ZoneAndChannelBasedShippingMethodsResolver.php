<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Shipping;

use App\Entity\Shipping\ShippingMethod;
use Sylius\Component\Addressing\Matcher\ZoneMatcherInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Sylius\Component\Core\Model\Scope;
use Sylius\Component\Core\Model\ShipmentInterface;
use Sylius\Component\Core\Repository\ShippingMethodRepositoryInterface;
use Sylius\Component\Shipping\Checker\ShippingMethodEligibilityCheckerInterface;
use Sylius\Component\Shipping\Model\ShippingSubjectInterface;
use Sylius\Component\Shipping\Resolver\ShippingMethodsResolverInterface;
use Webmozart\Assert\Assert;

class ZoneAndChannelBasedShippingMethodsResolver implements ShippingMethodsResolverInterface
{
    /** @var ShippingMethodRepositoryInterface */
    private $shippingMethodRepository;

    /** @var ZoneMatcherInterface */
    private $zoneMatcher;

    /** @var ShippingMethodEligibilityCheckerInterface */
    private $eligibilityChecker;

    public function __construct(
        ShippingMethodRepositoryInterface $shippingMethodRepository,
        ZoneMatcherInterface $zoneMatcher,
        ShippingMethodEligibilityCheckerInterface $eligibilityChecker
    ) {
        $this->shippingMethodRepository = $shippingMethodRepository;
        $this->zoneMatcher = $zoneMatcher;
        $this->eligibilityChecker = $eligibilityChecker;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException
     */
    public function getSupportedMethods(ShippingSubjectInterface $subject): array
    {
        /** @var ShipmentInterface $subject */
        Assert::isInstanceOf($subject, ShipmentInterface::class);
        Assert::true($this->supports($subject));

        /** @var OrderInterface $order */
        $order = $subject->getOrder();
        $isOrderParcelMachineCompatible = $this->isOrderParcelMachineCompatible($order);

        $zones = $this->zoneMatcher->matchAll($order->getShippingAddress(), Scope::SHIPPING);

        if (empty($zones)) {
            return [];
        }

        $methods = [];

        $shippingMethods = $this->shippingMethodRepository->findEnabledForZonesAndChannel($zones, $order->getChannel());

        foreach ($shippingMethods as $shippingMethod) {
            if (!$this->isParcelMachineValid($shippingMethod, $isOrderParcelMachineCompatible)) {
                continue;
            }

            if ($this->eligibilityChecker->isEligible($subject, $shippingMethod)) {
                $methods[] = $shippingMethod;
            }
        }

        return $methods;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ShippingSubjectInterface $subject): bool
    {
        return $subject instanceof ShipmentInterface &&
            null !== $subject->getOrder() &&
            null !== $subject->getOrder()->getShippingAddress() &&
            null !== $subject->getOrder()->getChannel()
            ;
    }

    private function isParcelMachineValid(ShippingMethod $method, bool $orderCompatible): bool
    {
        if ($method->getParcelMachineProviderCode() && !$orderCompatible) {
            return false;
        }

        return true;
    }

    private function isOrderParcelMachineCompatible(OrderInterface $order): bool
    {
        /** @var OrderItemInterface $orderItem */
        foreach ($order->getItems() as $orderItem) {
            $variant = $orderItem->getVariant();

            if ($variant->getShippingCategory() && $variant->getShippingCategory()->getCode() == '0001') {
                return false;
            }
        }

        return true;
    }
}
