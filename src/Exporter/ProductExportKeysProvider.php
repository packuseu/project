<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace App\Exporter;

use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Omni\Sylius\ImportPlugin\Exporter\Provider\ProductExportKeysProvider as BaseProductExportKeysProvider;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class ProductExportKeysProvider extends BaseProductExportKeysProvider
{
    /**
     * @return array
     */
    public function getKeys(): array
    {
        $keys = parent::getKeys();
        $keys[] = 'custom-quantity_step';
        $keys[] = 'custom-max_amount_sold_in_shop';
        $keys[] = 'custom-min_amount_sold_in_shop';
        $keys[] = 'custom-shipping_category';
        $keys[] = 'custom-weight';

        return $keys;
    }
}
