<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace App\Exporter;

use Omni\Sylius\ImportPlugin\Exporter\Plugin\ProductResourcePlugin as BaseProductResourcePlugin;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Product\Model\ProductTranslationInterface;

class ProductResourcePlugin extends BaseProductResourcePlugin
{
    /**
     * {@inheritdoc}
     */
    public function init(array $idsToExport): void
    {
        parent::init($idsToExport);

        /** @var ProductVariantInterface $resource */
        foreach ($this->resources as $resource) {
            $this->addQuantityStepData($resource);
            $this->addAmountSoldInShopData($resource);
            $this->addShippingCategoryData($resource);
            $this->addWeightStepData($resource);
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addQuantityStepData(ProductVariantInterface $resource): void
    {
        $this->addDataForResource($resource, 'custom-quantity_step', $resource->getProduct()->getQuantityStep());
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addAmountSoldInShopData(ProductVariantInterface $resource): void
    {
        $this->addDataForResource($resource, 'custom-min_amount_sold_in_shop', $resource->getMinSellable());
        $this->addDataForResource($resource, 'custom-max_amount_sold_in_shop', $resource->getMaxSellable());
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addShippingCategoryData(ProductVariantInterface $resource): void
    {
        $this->addDataForResource($resource, 'custom-shipping_category', $resource->getShippingCategory() ? $resource->getShippingCategory()->getCode() : null);
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addWeightStepData(ProductVariantInterface $resource): void
    {
        $this->addDataForResource($resource, 'custom-weight', $resource->getWeight());
    }
}
