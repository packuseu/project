<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace App\EventListener;

use App\Entity\Shipping\Shipment;
use App\Factory\DpdCreateShipmentRequestFactory;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Nfq\DpdClient\Client;
use Omni\Sylius\DpdPlugin\Constants\Gateway;
use Omni\Sylius\DpdPlugin\EventListener\ShippingExportEventListener as BaseListener;
use Omni\Sylius\DpdPlugin\Factory\CreateParcelLabelRequestFactory;
use Omni\Sylius\DpdPlugin\Factory\CreateShipmentRequestFactory;
use Omni\Sylius\DpdPlugin\HttpClient\DPDClientFactory;
use Omni\Sylius\ShippingPlugin\Event\ExportShipmentsEvent;
use Omni\Sylius\ShippingPlugin\Provider\CredentialProviderInterface;
use Psr\Log\LoggerAwareTrait;
use SM\Factory\Factory;
use Sylius\Component\Core\Model\ShipmentInterface;
use Sylius\Component\Shipping\ShipmentTransitions;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\Translation\TranslatorInterface;
use Webmozart\Assert\Assert;

class DpdShippingExportEventListener extends BaseListener
{
    use LoggerAwareTrait;
    private const DEFAULT_FORMAT = 'pdf';

    /**
     * @var DPDClientFactory
     */
    private $factory;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Factory
     */
    private $smFactory;

    /**
     * @var CredentialProviderInterface
     */
    private $credentialProvider;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $labelPath;

    public function __construct(
        DPDClientFactory $factory,
        TranslatorInterface $translator,
        Factory $smFactory,
        CredentialProviderInterface $credentialProvider,
        Filesystem $filesystem,
        string $labelPath
    ) {
        $this->factory = $factory;
        $this->translator = $translator;
        $this->smFactory = $smFactory;
        $this->credentialProvider = $credentialProvider;
        $this->filesystem = $filesystem;
        $this->labelPath = $labelPath;
    }

    public function exportShipments(ExportShipmentsEvent $exportShipmentsEvent): void
    {
        /** @var ShippingExport[] $shippingExport */
        $shippingExports = $exportShipmentsEvent->getShippingExports();

        if (!$this->isDPDProvider($shippingExports)) {
            return;
        }

        try {
            $this->createClient($shippingExports[0]);

            $shipmentRequest = DpdCreateShipmentRequestFactory::createForMultipleShipments($exportShipmentsEvent);
            /** @var Shipment $shipment */
            $shipment = $shippingExports[0]->getShipment();

            foreach ($shippingExports as $export) {
                if ($export->getShipment() != $shipment) {
                    throw new \Exception('All exports need to belong to the same shipment');
                }
            }

            $shipmentResponse = $this->client->createShipment($shipmentRequest);

            Assert::eq(count($shipmentResponse->getNumbers()), count($shippingExports));

            foreach ($shipmentResponse->getNumbers() as $key => $number) {
                /** @var ShippingExportInterface $exportShipment */
                $exportShipment = $shippingExports[$key];
                /** @var ShipmentInterface $shipment */
                $shipment = $exportShipment->getShipment();
                $shipment->setTracking($number);
                $exportShipment->setExternalId($number);

                $sm = $this->smFactory->get($shipment, ShipmentTransitions::GRAPH);
                if (\in_array(ExportShipmentsEvent::EXPORTED_TRANSITION, $sm->getPossibleTransitions(), true)) {
                    $sm->apply(ExportShipmentsEvent::EXPORTED_TRANSITION);
                }

                $labelRequest = CreateParcelLabelRequestFactory::create([$number], $shippingExports[0]->getShippingGateway());
                $labelResponse = $this->client->createParcelLabel($labelRequest);
                $extension = $labelRequest->getPrintFormat() ?? self::DEFAULT_FORMAT;

                $exportShipmentsEvent->persist($exportShipment);

                $this->saveShippingLabel($exportShipment, $labelResponse->getContents(), $extension);
                $exportShipmentsEvent->exportShipment($exportShipment);
            }

            $exportShipmentsEvent->addSuccessFlash();
            $this->logger->info('Successfully generated dpd label', [
                'request' => $shipmentRequest->toArray(),
            ]);
        } catch (\Exception $exception) {
            $this->logger->warning('Error in dpd shipment export', [
                'exception' => $exception->getMessage(),
                'request' => isset($shipmentRequest) ? $shipmentRequest->toArray() : null,
            ]);
            $exportShipmentsEvent->addErrorFlash(
                $this->translator->trans('omni_sylius_dpd.error', ['message' => $exception->getMessage()])
            );
            $exportShipmentsEvent->setException($exception);
        }
    }

    private function createClient(ShippingExportInterface $shippingExport): void
    {
        $sender = $shippingExport->getShipment()->getSender();
        $credentials = $this->getCredentials($sender);

        $this->client = $this->factory->create(
            $credentials['dpdHost'],
            $credentials['dpdUsername'],
            $credentials['dpdPassword']
        );
    }

    /**
     * @param ShippingExport[] $shippingExports
     * @return bool
     */
    private function isDPDProvider(array $shippingExports): bool
    {
        return count($shippingExports) > 0 &&
            $shippingExports[0]->getShippingGateway() instanceof ShippingGatewayInterface &&
            Gateway::CODE === $shippingExports[0]->getShippingGateway()->getCode();
    }

    /**
     * @param $sender
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getCredentials($sender): array
    {
        $credentials = $this->credentialProvider->getCredentials($sender->getId(), Gateway::CODE);

        if ($credentials === null) {
            throw new \Exception('No credentials found for this shipment.');
        }

        return $credentials['config'];
    }

    /**
     * @param ShippingExportInterface $shippingExport
     * @param $labelContent
     * @param string $labelExtension
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveShippingLabel(ShippingExportInterface $shippingExport, $labelContent, string $labelExtension): void
    {
        $shipment = $shippingExport->getShipment();
        $orderNumber = str_replace('#', '', $shipment->getOrder()->getNumber());
        $labelPath = $this->labelPath
            . '/' . $shippingExport->getExternalId()
            . '_' . $orderNumber
            . '.' . $labelExtension
        ;

        $this->filesystem->dumpFile($labelPath, $labelContent);
        $shippingExport->setLabelPath($labelPath);
    }
}
