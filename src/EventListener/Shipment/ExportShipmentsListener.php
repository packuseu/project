<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener\Shipment;

use App\Entity\Shipping\Shipment;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use BitBag\SyliusShippingExportPlugin\Repository\ShippingGatewayRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ShippingPlugin\Event\ExportShipmentsEvent;
use Omni\Sylius\ShippingPlugin\Services\ExportShipmentsEventFactory;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\ShipmentInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ExportShipmentsListener
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var FactoryInterface */
    private $shipmentExportFactory;

    /** @var ShippingGatewayRepositoryInterface */
    private $shippingGatewayRepository;

    /** @var ExportShipmentsEventFactory */
    private $exportShipmentsEventFactory;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    public function __construct(
        EntityManagerInterface $em,
        FactoryInterface $shipmentExportFactory,
        ShippingGatewayRepositoryInterface $shippingGatewayRepository,
        ExportShipmentsEventFactory $exportShipmentsEventFactory,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->shipmentExportFactory = $shipmentExportFactory;
        $this->shippingGatewayRepository = $shippingGatewayRepository;
        $this->exportShipmentsEventFactory = $exportShipmentsEventFactory;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param OrderInterface $order
     */
    public function export(OrderInterface $order): void
    {
        $shippingExports = [];

        /** @var Shipment $shipment */
        foreach ($order->getShipments() as $shipment) {
            for ($i = 0; $i < $shipment->getParcelCount(); $i++) {
                $export = $this->createExport($shipment);
                $shipment->addShippingExport($export);
                $this->em->persist($shipment);
                $this->em->persist($export);

                $shippingExports[] = $export;
            }
        }

        $this->dispatchEvent($shippingExports);
        $this->em->flush();
    }

    /**
     * @param ShipmentInterface $shipment
     *
     * @return ShippingExportInterface
     */
    private function createExport(ShipmentInterface $shipment): ShippingExportInterface
    {
        /** @var ShippingExportInterface $export */
        $export = $this->shipmentExportFactory->createNew();
        $export->setShippingGateway($this->getShippingGateway($shipment));
        $export->setState(ShippingExportInterface::STATE_NEW);
        $export->setShipment($shipment);

        return $export;
    }

    /**
     * @param ShipmentInterface $shipment
     *
     * @return ShippingGatewayInterface|null
     */
    private function getShippingGateway(ShipmentInterface $shipment): ?ShippingGatewayInterface
    {
        $method = $shipment->getMethod();

        if ($method === null) {
            return null;
        }

        return $this->shippingGatewayRepository->findOneByShippingMethod($method);
    }

    /**
     * @param array $shippingExports
     */
    private function dispatchEvent(array $shippingExports): void
    {
        $event = $this->exportShipmentsEventFactory->createShipmentExportsEvent($shippingExports);
        $this->eventDispatcher->dispatch($event, ExportShipmentsEvent::EXPORT);
    }
}
