<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener\Shipment;

use App\Entity\Shipping\Shipment;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\ShipmentInterface;

class StatesOnShipmentCreateListener
{
    /**
     * @param Shipment|ShipmentInterface $shipment
     */
    public function process(ShipmentInterface $shipment): void
    {
        if ($shipment->getSender() !== null) {
            return;
        }

        /** @var OrderInterface $order */
        $order = $shipment->getOrder();

        $shipment->setSender($order->getChannel());
    }
}
