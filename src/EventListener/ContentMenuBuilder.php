<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ContentMenuBuilder
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function configureNodeMenu(MenuBuilderEvent $event)
    {
        $adminMenu = $event->getMenu();

        $adminMenu->getChild('sales')
            ->addChild('node', ['route' => 'app_admin_order_report_index'])
            ->setLabel('app.order.reports')
            ->setLabelAttribute('icon', 'folder open outline')
        ;
    }
}
