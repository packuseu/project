<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener\Payment;

use App\Model\Emails;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Mailer\Sender\SenderInterface;

class OrderPaymentListener
{
    /**
     * @var SenderInterface
     */
    private $emilSender;

    public function __construct(SenderInterface $emilSender)
    {
        $this->emilSender = $emilSender;
    }

    public function onOrderPay(OrderInterface $order): void
    {
        $this->emilSender->send(Emails::PAYMENT_RECEIVED, [$order->getCustomer()->getEmail()], ['order' => $order]);
    }
}
