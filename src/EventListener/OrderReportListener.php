<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Order\OrderReport;
use FriendsOfSylius\SyliusImportExportPlugin\Writer\CsvWriter;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\OrderPaymentStates;
use Sylius\Component\Core\Repository\OrderRepositoryInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class OrderReportListener
{
    /**
     * @var OrderRepositoryInterface
     */
    public $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * @throws \Exception
     */
    public function preCreate(GenericEvent $event): void
    {
        /** @var OrderReport $orderReport */
        $orderReport = $event->getSubject();
        $orderReport->setFilePath($this->getReportFile($orderReport));
    }

    /**
     * @throws \Exception
     */
    private function getReportFile(OrderReport $orderReport): string
    {
        $filePath = $this->getFilePath();
        $this->createReport($orderReport, $filePath);

        return $filePath;
    }

    /**
     * @throws \Exception
     */
    private function getFilePath(): string
    {
        $i = 1;
        $dir = __DIR__ . '/../../var/reports/';
        $name = (new \DateTime())->format('Y-m-d') . '_report';
        $filename = $dir . $name . '.csv';

        if (!is_dir($dir)) {
            mkdir($dir);
        }


        while (file_exists($filename)) {
            $filename = $dir . $name . '_' . $i++ . '.csv';

            if ($i > 100) {
                throw new \Exception('Could not parse a valid filename for the report');
            }
        }

        return $filename;
    }

    private function createReport(OrderReport $orderReport, string $path): void
    {
        $headers = [
            "Data (Užsakymo įvykdymo data)", "Kliento kodas (įmonės kodas)", "Kliento PVM kodas",
            "Kliento pavadinimas", "Gatvė", "Namo Nr.", "Buto Nr.", "Pašto indeksas", "Miestas", "Šalis",
            "El. Paštas", "Tel. Nr.", "Kliento tipas (privatus asmuo / įmonė)", "Produktas", "Produkto kodas",
            "Produktų kiekis", "Produkto suma", "Užsakymo nr"
        ];
        $fp = fopen($path, 'w');
        fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        fputcsv($fp, $headers, ';');
        foreach ($this->getOrders($orderReport) as $order) {
            foreach ($this->formatOrderArray($order) as $item) {
                fputcsv($fp, $item, ';');
            }
        }
        fclose($fp);
    }

    private function formatOrderArray(OrderInterface $order): array
    {
        $orderReport = [];
        $billingAddress = $order->getBillingAddress();
        $companyCode = $billingAddress->getCompanyCode();
        $vatCode = $billingAddress->getVatCode();
        $company = $billingAddress->getCompany();
        $city = $billingAddress->getCity();
        $country = $billingAddress->getCountryCode();
        $street = $billingAddress->getStreet();
        $postCode = $billingAddress->getPostcode();
        $houseNumber = $billingAddress->getHouseNumber();
        $apartmentNumber = $billingAddress->getApartmentNumber();
        $email = $order->getCustomer()->getEmail();
        $telNr = $billingAddress->getPhoneNumber();
        $buyer = $company ? $company : $billingAddress->getFirstName() . ' ' . $billingAddress->getLastName();
        $buyerType = $companyCode ? 'Įmonė' : 'Privatus asmuo';
        $payment = $order->getLastPayment();
        $shippingCost = $order->getShippingTotal();

        // TODO: consider implementing multiple payments
        if (!$payment || count($order->getPayments()) > 1) {
            throw new \Exception('Unexpected amount of payments detected for order ' . $order->getNumber());
        }

        foreach ($order->getItems() as $item) {
            $orderReport[] = [
                $payment->getUpdatedAt()->format('Y-m d'),
                $companyCode,
                $vatCode,
                $buyer,
                $street,
                $houseNumber,
                $apartmentNumber,
                $postCode,
                $city,
                $country,
                $email,
                $telNr,
                $buyerType,
                $item->getVariantName(),
                $item->getVariant()->getCode(),
                $item->getQuantity(),
                (string) round($item->getTotal() / 100, 2),
                $order->getNumber()
            ];
        }

        if ($shippingCost > 0) {
            $orderReport[] = [
                $payment->getUpdatedAt()->format('Y-m d'),
                $companyCode,
                $vatCode,
                $buyer,
                $street,
                $houseNumber,
                $apartmentNumber,
                $postCode,
                $city,
                $country,
                $email,
                $telNr,
                $buyerType,
                'Shipping',
                'Shipping',
                0,
                (string) round($shippingCost / 100, 2),
                $order->getNumber()
            ];
        }

        return $orderReport;
    }

    private function getOrders(OrderReport $orderReport): array
    {
        $qb = $this->orderRepository
            ->createListQueryBuilder()
            ->innerJoin('o.payments', 'payment')
            ->andWhere('o.paymentState = :ps')
            ->andWhere('payment.updatedAt > :from')
            ->andWhere('payment.updatedAt < :to')
            ->setParameter('ps', OrderPaymentStates::STATE_PAID)
            ->setParameter('from', $orderReport->getFrom())
            ->setParameter('to', $orderReport->getTo())
        ;

        return $qb->getQuery()->getResult();
    }
}
