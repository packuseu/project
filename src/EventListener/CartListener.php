<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Channel\Channel;
use App\Entity\Order\Order;
use App\Entity\Product\ProductVariant;
use App\Entity\Shipping\Shipment;
use App\Entity\Shipping\ShippingMethod;
use Sylius\Bundle\MoneyBundle\Templating\Helper\FormatMoneyHelperInterface;
use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CartListener
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var ChannelContextInterface
     */
    protected $channelContext;

    /**
     * @var FlashBagInterface
     */
    protected $flashBag;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var FormatMoneyHelperInterface
     */
    protected $moneyFormatter;

    /**
     * @var bool
     */
    protected $checkShipment = false;

    public function __construct(
        TranslatorInterface $translator,
        ChannelContextInterface $channelContext,
        FlashBagInterface $flashBag,
        RouterInterface $router,
        FormatMoneyHelperInterface $moneyFormatter
    ) {
        $this->translator = $translator;
        $this->channelContext = $channelContext;
        $this->flashBag = $flashBag;
        $this->router = $router;
        $this->moneyFormatter = $moneyFormatter;
    }

    public function checkShipment(): void
    {
        $this->checkShipment = true;
    }

    public function change(ResourceControllerEvent $event): void
    {
        /** @var OrderItemInterface $orderItem */
        $orderItem = $event->getSubject();
        /** @var ProductVariant $variant */
        $variant = $orderItem->getVariant();

        if ($this->isItemAddingPossible($orderItem, $variant)) {
            $event->stop($this->translator->trans('app.product.cannot_add'));
        }

        if ($orderItem->getQuantity() < $variant->getMinSellable()) {
            $event->stop($this->translator->trans('app.product.minimal_amount') . ': ' . $variant->getMinSellable());
            $event->setErrorCode(400);
        }
    }

    public function checkCheckout(ResourceControllerEvent $event): void
    {
        /** @var OrderInterface $order */
        $order = $event->getSubject();
        /** @var Channel $channel */
        $channel = $this->channelContext->getChannel();

        if ($order->getItemsTotal() < $channel->getMinimalCartSize()) {
            $this->flashBag->add(
                'error',
                sprintf(
                    '%s: %s',
                    $this->translator->trans('app.minimal_cart_size'),
                    $this->moneyFormatter->formatAmount(
                        $channel->getMinimalCartSize(),
                        $channel->getBaseCurrency()->getCode(),
                        $channel->getDefaultLocale()->getCode()
                    )
                )
            );
            $response = new RedirectResponse($this->router->generate('sylius_shop_cart_summary'));

            $event->setResponse($response);
        }
    }

    public function onCheckoutChange(ResourceControllerEvent $event): void
    {
        /** @var Order $order */
        $order = $event->getSubject();
        /** @var Shipment $shipment */
        $shipment = $order->getShipments()->first();

        if (!$shipment) {
            return;
        }

        /** @var ShippingMethod $method */
        $method = $shipment->getMethod();

        if (!$method) {
            return;
        }

        if ($method->getParcelMachineProviderCode() && !$shipment->getParcelMachine() && $this->checkShipment) {
            $event->stop($this->translator->trans('app.shipment.parcel_not_selected'));
            $event->setResponse(
                new RedirectResponse($this->router->generate('sylius_shop_checkout_select_shipping'))
            );
        }
    }

    private function isItemAddingPossible(OrderItemInterface $orderItem, ProductVariant $variant): bool
    {
        return $variant->isNonSellable() ||
            ($variant->getMaxSellable() && $orderItem->getQuantity() > $variant->getMaxSellable());
    }
}
