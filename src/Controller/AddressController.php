<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Sylius\Component\Addressing\Model\Address;

class AddressController extends AbstractController
{
        public function addressReturn(Address $address): Response
        {
            $addressCompany = [
                'company' => $address->getCompany()
            ];

            return $this->render('@SyliusAdminBundle/Order/Grid/Field/customer.html.twig', ['address' => $addressCompany]);
        }
}