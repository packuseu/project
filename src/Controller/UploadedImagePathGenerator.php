<?php

declare(strict_types=1);

namespace App\Controller;

use Sylius\Component\Core\Generator\ImagePathGeneratorInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class UploadedImagePathGenerator implements ImagePathGeneratorInterface
{

    public function generate(ImageInterface $image): string
    {
        /** @var UploadedFile $file */
        $file = $image->getFile();

        return $this->expandPath('test.' . $file->guessExtension());
    }

    private function expandPath(string $path): string
    {
        return sprintf('%s/%s/%s', substr($path, 0, 2), substr($path, 2, 2), substr($path, 4));
    }
}