<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Order\Order;
use App\Entity\Order\OrderReport;
use App\Entity\Shipping\Shipment;
use App\EventListener\Shipment\ExportShipmentsListener;
use App\Exception\QuoteValidationException;
use App\Form\Type\QuoteType;
use App\Model\Emails;
use App\Model\Quote;
use Doctrine\ORM\EntityManagerInterface;
use SM\Factory\Factory;
use Sylius\Component\Mailer\Sender\SenderInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Shipping\ShipmentTransitions;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Mime\FileinfoMimeTypeGuesser;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class AppController
{
    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var SenderInterface
     */
    private $emailSender;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var RepositoryInterface
     */
    private $orderReportRepository;

    /**
     * @var RepositoryInterface
     */
    private $orderRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ExportShipmentsListener
     */
    private $exportShipmentsListener;

    /**
     * @var Factory
     */
    private $stateMachineFactory;

    public function __construct(
        FormFactoryInterface $formFactory,
        TranslatorInterface $translator,
        SenderInterface $emailSender,
        Environment $twig,
        RepositoryInterface $orderReportRepository,
        RepositoryInterface $orderRepository,
        Factory $stateMachineFactory,
        EntityManagerInterface $em,
        ExportShipmentsListener $exportShipmentsListener
    ) {
        $this->formFactory = $formFactory;
        $this->translator = $translator;
        $this->emailSender = $emailSender;
        $this->twig = $twig;
        $this->orderReportRepository = $orderReportRepository;
        $this->orderRepository = $orderRepository;
        $this->stateMachineFactory = $stateMachineFactory;
        $this->em = $em;
        $this->exportShipmentsListener = $exportShipmentsListener;
    }

    public function pack(Request $request, int $shipmentId, int $orderId): Response
    {
        $shipment = null;
        /** @var Order $order */
        $order = $this->orderRepository->find($orderId);
        $flashBag = $request->getSession()->getFlashBag();

        if (!$order) {
            $flashBag->add('error', $this->translator->trans('app.order.order_not_found'));

            return new RedirectResponse('/admin');
        }

        foreach ($order->getShipments() as $orderShipment) {
            if ($orderShipment->getId() == $shipmentId) {
                $shipment = $orderShipment;

                break;
            }
        }

        if (!$shipment) {
            $flashBag->add('error', $this->translator->trans('app.order.shipment_not_found'));

            return new RedirectResponse('/admin/orders/' . $order->getId());
        }

        $stateMachine = $this->stateMachineFactory->get($shipment, ShipmentTransitions::GRAPH);
        $orderStateMachine = $this->stateMachineFactory->get($order, 'sylius_order_shipping');

        if (!$stateMachine->can('pack') || !$orderStateMachine->can('pack')) {
            $flashBag->add('error', $this->translator->trans('app.order.bad_state'));

            return new RedirectResponse('/admin/orders/' . $order->getId());
        }

        $stateMachine->apply('pack');
        $orderStateMachine->apply('pack');

        $this->em->persist($order);
        $this->em->persist($shipment);
        $this->em->flush();
        $this->emailSender->send(
            Emails::SHIPMENT_READY,
            [$order->getCustomer()->getEmail()],
            ['order' => $order, 'shipment' => $shipment]
        );

        $flashBag->add('success', $this->translator->trans('app.order.shipment_packed'));

        return new RedirectResponse('/admin/orders/' . $order->getId());
    }

    public function exportShipment(Request $request, int $shipmentId, int $orderId): Response
    {
        /** @var Shipment|null $shipment */
        $shipment = null;
        /** @var Order $order */
        $order = $this->orderRepository->find($orderId);
        $flashBag = $request->getSession()->getFlashBag();
        $numberOfParcels = (int) $request->request->get('number_of_parcels');

        if (!$order) {
            $flashBag->add('error', $this->translator->trans('app.order.order_not_found'));

            return new RedirectResponse('/admin');
        }

        foreach ($order->getShipments() as $orderShipment) {
            if ($orderShipment->getId() == $shipmentId) {
                $shipment = $orderShipment;

                break;
            }
        }

        if (!$shipment) {
            $flashBag->add('error', $this->translator->trans('app.order.shipment_not_found'));

            return new RedirectResponse('/admin/orders/' . $order->getId());
        }

        $stateMachine = $this->stateMachineFactory->get($shipment, ShipmentTransitions::GRAPH);
        $orderStateMachine = $this->stateMachineFactory->get($order, 'sylius_order_shipping');

        if (!$stateMachine->can('export')) {
            $flashBag->add('error', $this->translator->trans('app.order.bad_state'));

            return new RedirectResponse('/admin/orders/' . $order->getId());
        }

        try {
            if (!$numberOfParcels) {
                $numberOfParcels = 1;
            }

            $shipment->setParcelCount($numberOfParcels);
            $stateMachine->apply('export');
            $orderStateMachine->apply('export');
            $this->exportShipmentsListener->export($order);

            $flashBag->add('success', $this->translator->trans('app.order.shipment_exported'));
        } catch (\Throwable $e) {
            $flashBag->add('error', $this->translator->trans('app.order.shipment_export_error'));
        }

        return new RedirectResponse('/admin/orders/' . $order->getId());
    }

    public function downloadOrderReport(Request $request, int $report): Response
    {
        /** @var OrderReport|null $report */
        $report = $this->orderReportRepository->find($report);

        if (!$report) {
            return new Response('Not found', Response::HTTP_NOT_FOUND);
        }

        $found = [];
        $mimeTypeGuesser = new FileinfoMimeTypeGuesser();
        $response = new BinaryFileResponse($report->getFilePath());
        preg_match('#reports/(.+?)$#', $report->getFilePath(), $found);
        $filename = $found[1] ?? null;

        if($mimeTypeGuesser->isGuesserSupported()){
            $response->headers->set('Content-Type', $mimeTypeGuesser->guessMimeType($report->getFilePath()));
        } else {
            $response->headers->set('Content-Type', 'text/plain');
        }

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
        );

        return $response;
    }

    public function submitQuote(Request $request): Response
    {
        return $request->isXmlHttpRequest() ? $this->handleAjaxQuoteSubmit($request) :
            $this->handleQuoteSubmit($request);
    }

    public function getQuote(Request $request): Response
    {
        return new Response($this->twig->render('quote.html.twig'));
    }

    private function handleQuoteSubmit(Request $request): Response
    {
        $quote = new Quote();

        $form = $this->formFactory->create(QuoteType::class, $quote);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            $request->getSession()->getFlashBag()->add('error', $this->translator->trans('app.quote.error'));

            return new Response($this->twig->render('quote.html.twig', ['form' => $form->createView()]));
        }

        $this->emailSender->send(Emails::QUOTE, [$_ENV['MAILER_RECEIVER_EMAIL']], ['quote' => $quote]);
        $request->getSession()->getFlashBag()->add('success', $this->translator->trans('app.quote.success'));

        return new RedirectResponse('/'.$request->getLocale());
    }

    private function handleAjaxQuoteSubmit(Request $request): Response
    {
        try {
            $quote = $this->getQuoteData($request);
        } catch (QuoteValidationException $e) {
            $data = ['message' => $e, 'errors' => []];

            foreach ($e->getErrors() as $error) {
                $data['errors'][] = [
                    'field' => $error->getOrigin()->getConfig()->getName(),
                    'error' => $error->getMessage(),
                ];
            }

            return new JsonResponse($data, Response::HTTP_BAD_REQUEST);
        }

        $this->emailSender->send(Emails::QUOTE, [$_ENV['MAILER_RECEIVER_EMAIL']], ['quote' => $quote]);

        return new JsonResponse('success');
    }

    /**
     * @throws QuoteValidationException
     */
    private function getQuoteData(Request $request): Quote
    {
        $errors = [];
        $quote = new Quote();

        $form = $this->formFactory->create(QuoteType::class, $quote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $quote;
        }

        foreach ($form->getErrors(true) as $error) {
            $errors[] = $error;
        }

        throw new QuoteValidationException($this->translator->trans('app.quote.bad_request'), $errors);
    }
}
