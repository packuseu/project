<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200408095336 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE sylius_customer_consent_agreement (customer_id INT NOT NULL, consent_agreement_id INT NOT NULL, INDEX IDX_C4714DF09395C3F3 (customer_id), UNIQUE INDEX UNIQ_C4714DF0937BDF (consent_agreement_id), PRIMARY KEY(customer_id, consent_agreement_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nfq_consent (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, mandatory TINYINT(1) NOT NULL, location_code VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE nfq_consent_agreement (id INT AUTO_INCREMENT NOT NULL, consent_id INT DEFAULT NULL, agreed TINYINT(1) NOT NULL, datetime DATETIME NOT NULL, INDEX IDX_25182CFE41079D63 (consent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE sylius_customer_consent_agreement ADD CONSTRAINT FK_C4714DF09395C3F3 FOREIGN KEY (customer_id) REFERENCES sylius_customer (id)');
        $this->addSql('ALTER TABLE sylius_customer_consent_agreement ADD CONSTRAINT FK_C4714DF0937BDF FOREIGN KEY (consent_agreement_id) REFERENCES nfq_consent_agreement (id)');
        $this->addSql('ALTER TABLE nfq_consent_agreement ADD CONSTRAINT FK_25182CFE41079D63 FOREIGN KEY (consent_id) REFERENCES nfq_consent (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE nfq_consent_agreement DROP FOREIGN KEY FK_25182CFE41079D63');
        $this->addSql('ALTER TABLE sylius_customer_consent_agreement DROP FOREIGN KEY FK_C4714DF0937BDF');
        $this->addSql('DROP TABLE sylius_customer_consent_agreement');
        $this->addSql('DROP TABLE nfq_consent');
        $this->addSql('DROP TABLE nfq_consent_agreement');
    }
}
