<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200415155625 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX fulltext_search_idx ON omni_search_index');
        $this->addSql('CREATE INDEX fulltext_search_idx ON omni_search_index (resource_id)');
        $this->addSql('ALTER TABLE omni_node ADD slug_from_relation TINYINT(1) DEFAULT \'0\' NOT NULL, DROP link');
        $this->addSql('ALTER TABLE omni_node_translation ADD link LONGTEXT DEFAULT NULL, ADD link_target VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE omni_node ADD link LONGTEXT CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`, DROP slug_from_relation');
        $this->addSql('ALTER TABLE omni_node_translation DROP link, DROP link_target');
        $this->addSql('DROP INDEX fulltext_search_idx ON omni_search_index');
        $this->addSql('CREATE FULLTEXT INDEX fulltext_search_idx ON omni_search_index (`index`)');
    }
}
