<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201126031618 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE sylius_channel ADD address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_channel ADD CONSTRAINT FK_16C8119EF5B7AF75 FOREIGN KEY (address_id) REFERENCES sylius_address (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_16C8119EF5B7AF75 ON sylius_channel (address_id)');
        $this->addSql('ALTER TABLE sylius_shipment ADD shippingExport_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_shipment ADD CONSTRAINT FK_FD707B33541326EE FOREIGN KEY (shippingExport_id) REFERENCES bitbag_shipping_export (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FD707B33541326EE ON sylius_shipment (shippingExport_id)');
        $this->addSql('ALTER TABLE omni_manifest ADD channel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE omni_manifest ADD CONSTRAINT FK_D1698F0572F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id)');
        $this->addSql('CREATE INDEX IDX_D1698F0572F5A1AA ON omni_manifest (channel_id)');
        $this->addSql('ALTER TABLE omni_parcel_machine ADD postcode VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE omni_manifest DROP FOREIGN KEY FK_D1698F0572F5A1AA');
        $this->addSql('DROP INDEX IDX_D1698F0572F5A1AA ON omni_manifest');
        $this->addSql('ALTER TABLE omni_manifest DROP channel_id');
        $this->addSql('ALTER TABLE omni_parcel_machine DROP postcode');
        $this->addSql('ALTER TABLE sylius_channel DROP FOREIGN KEY FK_16C8119EF5B7AF75');
        $this->addSql('DROP INDEX UNIQ_16C8119EF5B7AF75 ON sylius_channel');
        $this->addSql('ALTER TABLE sylius_channel DROP address_id');
        $this->addSql('ALTER TABLE sylius_shipment DROP FOREIGN KEY FK_FD707B33541326EE');
        $this->addSql('DROP INDEX UNIQ_FD707B33541326EE ON sylius_shipment');
        $this->addSql('ALTER TABLE sylius_shipment DROP shippingExport_id');
    }
}
