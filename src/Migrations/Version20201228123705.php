<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201228123705 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bitbag_shipping_export DROP INDEX UNIQ_20E62D9F7BE036FC, ADD INDEX IDX_20E62D9F7BE036FC (shipment_id)');
        $this->addSql('ALTER TABLE sylius_shipment DROP FOREIGN KEY FK_FD707B33541326EE');
        $this->addSql('DROP INDEX UNIQ_FD707B33541326EE ON sylius_shipment');
        $this->addSql('ALTER TABLE sylius_shipment DROP shippingExport_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bitbag_shipping_export DROP INDEX IDX_20E62D9F7BE036FC, ADD UNIQUE INDEX UNIQ_20E62D9F7BE036FC (shipment_id)');
        $this->addSql('ALTER TABLE sylius_shipment ADD shippingExport_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE sylius_shipment ADD CONSTRAINT FK_FD707B33541326EE FOREIGN KEY (shippingExport_id) REFERENCES bitbag_shipping_export (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FD707B33541326EE ON sylius_shipment (shippingExport_id)');
    }
}
