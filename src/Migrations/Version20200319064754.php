<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200319064754 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE omni_banner_image (id INT AUTO_INCREMENT NOT NULL, owner_id INT NOT NULL, type VARCHAR(255) DEFAULT NULL, path VARCHAR(255) NOT NULL, content_position VARCHAR(255) DEFAULT NULL, content_space INT DEFAULT NULL, content_background VARCHAR(255) DEFAULT NULL, position INT DEFAULT 0 NOT NULL, INDEX IDX_FC3866247E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_banner_zone (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_A63A79E477153098 (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_banner_image_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, link VARCHAR(255) DEFAULT NULL, content VARCHAR(255) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_4264B92F2C2AC5D3 (translatable_id), UNIQUE INDEX omni_banner_image_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_banner_position_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_784BAF332C2AC5D3 (translatable_id), UNIQUE INDEX omni_banner_position_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_banner_position (id INT AUTO_INCREMENT NOT NULL, zone_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, priority INT NOT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_39299ED777153098 (code), INDEX IDX_39299ED79F2C3FAB (zone_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_banner (id INT AUTO_INCREMENT NOT NULL, position_id INT NOT NULL, code VARCHAR(255) DEFAULT NULL, enabled TINYINT(1) DEFAULT \'0\' NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, publish_from DATETIME NOT NULL, publish_to DATETIME NOT NULL, UNIQUE INDEX UNIQ_DC753D6E77153098 (code), INDEX IDX_DC753D6EDD842E46 (position_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_banner_channels (banner_id INT NOT NULL, channel_id INT NOT NULL, INDEX IDX_8C119894684EC833 (banner_id), INDEX IDX_8C11989472F5A1AA (channel_id), PRIMARY KEY(banner_id, channel_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_banner_zone_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT NOT NULL, title VARCHAR(255) DEFAULT NULL, link VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_CC474AE72C2AC5D3 (translatable_id), UNIQUE INDEX omni_banner_zone_translation_uniq_trans (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE omni_banner_image ADD CONSTRAINT FK_FC3866247E3C61F9 FOREIGN KEY (owner_id) REFERENCES omni_banner (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE omni_banner_image_translation ADD CONSTRAINT FK_4264B92F2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES omni_banner_image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE omni_banner_position_translation ADD CONSTRAINT FK_784BAF332C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES omni_banner_position (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE omni_banner_position ADD CONSTRAINT FK_39299ED79F2C3FAB FOREIGN KEY (zone_id) REFERENCES omni_banner_zone (id)');
        $this->addSql('ALTER TABLE omni_banner ADD CONSTRAINT FK_DC753D6EDD842E46 FOREIGN KEY (position_id) REFERENCES omni_banner_position (id)');
        $this->addSql('ALTER TABLE omni_banner_channels ADD CONSTRAINT FK_8C119894684EC833 FOREIGN KEY (banner_id) REFERENCES omni_banner (id)');
        $this->addSql('ALTER TABLE omni_banner_channels ADD CONSTRAINT FK_8C11989472F5A1AA FOREIGN KEY (channel_id) REFERENCES sylius_channel (id)');
        $this->addSql('ALTER TABLE omni_banner_zone_translation ADD CONSTRAINT FK_CC474AE72C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES omni_banner_zone (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE omni_banner_image_translation DROP FOREIGN KEY FK_4264B92F2C2AC5D3');
        $this->addSql('ALTER TABLE omni_banner_position DROP FOREIGN KEY FK_39299ED79F2C3FAB');
        $this->addSql('ALTER TABLE omni_banner_zone_translation DROP FOREIGN KEY FK_CC474AE72C2AC5D3');
        $this->addSql('ALTER TABLE omni_banner_position_translation DROP FOREIGN KEY FK_784BAF332C2AC5D3');
        $this->addSql('ALTER TABLE omni_banner DROP FOREIGN KEY FK_DC753D6EDD842E46');
        $this->addSql('ALTER TABLE omni_banner_image DROP FOREIGN KEY FK_FC3866247E3C61F9');
        $this->addSql('ALTER TABLE omni_banner_channels DROP FOREIGN KEY FK_8C119894684EC833');
        $this->addSql('DROP TABLE omni_banner_image');
        $this->addSql('DROP TABLE omni_banner_zone');
        $this->addSql('DROP TABLE omni_banner_image_translation');
        $this->addSql('DROP TABLE omni_banner_position_translation');
        $this->addSql('DROP TABLE omni_banner_position');
        $this->addSql('DROP TABLE omni_banner');
        $this->addSql('DROP TABLE omni_banner_channels');
        $this->addSql('DROP TABLE omni_banner_zone_translation');
    }
}
