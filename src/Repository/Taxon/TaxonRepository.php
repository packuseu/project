<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Taxon;

use App\Entity\Taxonomy\Taxon;
use Sylius\Bundle\TaxonomyBundle\Doctrine\ORM\TaxonRepository as BaseTaxonRepository;
use Sylius\Component\Core\Model\TaxonInterface;

class TaxonRepository extends BaseTaxonRepository
{
    /**
     * @param string $name
     * @param string $locale
     * @param TaxonInterface|null $parent
     *
     * @return Taxon
     */
    public function findOneByNameAndParent(string $name, string $locale, ?TaxonInterface $parent): ?Taxon
    {
        $qb = $this->createQueryBuilder('taxon');

        $qb->innerJoin('taxon.translations', 'translation');

        $qb->where($qb->expr()->eq('translation.name', ':name'));
        $qb->andWhere($qb->expr()->eq('translation.locale', ':locale'));

        $qb->setParameter('name', $name);
        $qb->setParameter('locale', $locale);

        if (null === $parent) {
            $qb->andWhere($qb->expr()->isNull('taxon.parent'));
        } else {
            $qb->andWhere($qb->expr()->eq('taxon.parent', ':parent'));
            $qb->setParameter('parent', $parent->getId());
        }

        return $qb->getQuery()->getOneOrNullResult();
    }
}
