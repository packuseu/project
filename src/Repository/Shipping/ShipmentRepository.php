<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Repository\Shipping;

use App\Entity\Shipping\Shipment;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Sylius\Bundle\CoreBundle\Doctrine\ORM\ShipmentRepository as SyliusShipmentRepository;

class ShipmentRepository extends SyliusShipmentRepository
{
    /**
     * @param $states
     * @param $channel
     *
     * @return Shipment[]
     */
    public function findShipmentsForManifest($states, $channel)
    {
        $qb = $this->createQueryBuilder('shipment');

        return $qb
            ->where($qb->expr()->in('shipment.state', ':states'))
            ->andWhere('shipment.sender = :channel')
            ->setParameter(':channel', $channel)
            ->setParameter(':states', $states, Type::SIMPLE_ARRAY)
            ->getQuery()
            ->getResult();
    }
}
