<?php

namespace App\Model;

interface Emails
{
    const QUOTE = 'quote';
    const SHIPMENT_READY = 'shipment_ready';
    const PAYMENT_RECEIVED = 'payment_received';
}
