<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Pagerfanta\View\Template;

use Pagerfanta\View\Template\TwitterBootstrap4Template;
use Symfony\Contracts\Translation\TranslatorInterface;

class StovendoTemplate extends TwitterBootstrap4Template
{
    static protected $defaultOptions = [
        'active_suffix'       => '',
        'css_container_class' => 'pagination',
        'css_prev_class'      => 'prev',
        'css_next_class'      => 'next',
        'css_disabled_class'  => 'disabled',
        'css_dots_class'      => 'disabled',
        'css_active_class'    => 'active',
        'rel_previous'        => 'prev',
        'rel_next'            => 'next'
    ];

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        parent::__construct();

        $this->setOptions(
            [
                'prev_message' => $translator->trans('shop24.pagerfanta.previous'),
                'next_message' => $translator->trans('shop24.pagerfanta.next'),
                'dots_message' => $translator->trans('shop24.pagerfanta.dots'),
            ]
        );
    }
}
