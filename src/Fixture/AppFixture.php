<?php

namespace App\Fixture;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\CmsPlugin\Factory\NodeFactoryInterface;
use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Bundle\FixturesBundle\Fixture\FixtureInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

class AppFixture extends AbstractFixture implements FixtureInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var NodeFactoryInterface
     */
    protected $nodeFactory;

    /**
     * @var TaxonRepositoryInterface
     */
    protected $taxonRepository;

    /**
     * @var NodeTypeManager
     */
    protected $handler;

    /**
     * @var NodeRepositoryInterface
     */
    protected $nodeRepository;

    /**
     * @var LocaleContextInterface
     */
    protected $localeContext;

    public function __construct(
        EntityManagerInterface $em,
        NodeFactoryInterface $nodeFactory,
        TaxonRepositoryInterface $taxonRepository,
        NodeRepositoryInterface $nodeRepository,
        NodeTypeManager $handler,
        LocaleContextInterface $localeContext
    ) {
        $this->em = $em;
        $this->nodeFactory = $nodeFactory;
        $this->taxonRepository = $taxonRepository;
        $this->nodeRepository = $nodeRepository;
        $this->handler = $handler;
        $this->localeContext = $localeContext;
    }

    /**
     * @param array $options
     */
    public function load(array $options): void
    {
        /** @var NodeInterface $mainMenuNode */
        $mainMenuNode = $this->nodeRepository->findOneBy(['code' => 'main_menu']);

        foreach ($mainMenuNode->getChildren() as $subNode) {
            $this->em->remove($subNode);
        }

        $this->loadTaxonNodes($mainMenuNode);

        $this->em->persist($mainMenuNode);
        $this->em->flush();
    }

    public function getName(): string
    {
        return 'app_fixture';
    }

    protected function loadTaxonNodes(NodeInterface $parent): void
    {
        $count = 0;

        /** @var TaxonInterface $taxon */
        foreach ($this->taxonRepository->findBy(['code' => ['packages', 'fat_resistant_paper', 'cardboard']]) as $taxon) {
            $node = $this
                ->createNodeForParent($parent, $taxon->getCode(), $taxon->getName(), 'taxon')
                ->setRelationId($taxon->getId())
                ->setRelationType($this->handler->getRelationMetadata('taxon')['alias'])
            ;

            $this->em->persist($node);
        }
    }

    /**
     * @param NodeInterface $parent
     * @param string $code
     * @param string $title
     * @param string $type
     *
     * @return NodeInterface
     */
    protected function createNodeForParent(NodeInterface $parent, string $code, string $title, string $type): NodeInterface
    {
        $node = $this->nodeFactory->createForParent($parent);
        $node->setCurrentLocale($this->localeContext->getLocaleCode());
        $node->setCode($code);
        $node->getTranslation()->setTitle($title);
        $node->setType($type);

        return $node;
    }

    protected function getFormatedTitle(string $type): string
    {
        return ucfirst(str_replace('_', ' ', $type));
    }
}
