<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace App\Factory;

use App\Entity\Addressing\Address;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use Nfq\DpdClient\Request\CreateShipmentRequest;
use Omni\Sylius\DpdPlugin\Factory\CreateShipmentRequestFactory;
use Omni\Sylius\ShippingPlugin\Event\ExportShipmentsEvent;
use Sylius\Component\Core\Model\AddressInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\ShipmentInterface;

class DpdCreateShipmentRequestFactory extends CreateShipmentRequestFactory
{
    public static function createForMultipleShipments(ExportShipmentsEvent $exportShipmentsEvent): CreateShipmentRequest
    {
        $request = parent::createForMultipleShipments($exportShipmentsEvent);
        self::modifyRequestAddress($request, $exportShipmentsEvent->getShippingExports()[0]);

        return $request;
    }

    private static function modifyRequestAddress(CreateShipmentRequest $request, ShippingExport $shippingExport): void
    {
        /** @var ShipmentInterface $shipment */
        $shipment = $shippingExport->getShipment();
        /** @var OrderInterface $order */
        $order = $shipment->getOrder();
        /** @var Address $billingAddress */
        $billingAddress = $order->getBillingAddress();
        /** @var Address $shippingAddress */
        $shippingAddress = $order->getShippingAddress();
        if ($shipment->getParcelMachine()) {
            return;
        }
        if ($billingAddress->getVatCode() || $billingAddress->getCompanyCode()) {
            $request->setName($billingAddress->getFullName())
                ->setSecondaryName($billingAddress->getCompany())
                ->setStreet($billingAddress->getStreet())
                ->setCity($billingAddress->getCity())
                ->setPostCode($billingAddress->getPostcode())
                ->setPhone($billingAddress->getPhoneNumber());
            self::addHouseNumberToRequest($request, $billingAddress);
        } else {
            self::addHouseNumberToRequest($request, $shippingAddress);
        }
    }

    private static function addHouseNumberToRequest(CreateShipmentRequest $request, AddressInterface $address): void
    {
        if ($address->getApartmentNumber()) {
            $street = sprintf('%s %s-%s', $address->getStreet(), $address->getHouseNumber(), $address->getApartmentNumber());
        } else {
            $street = sprintf('%s %s', $address->getStreet(), $address->getHouseNumber());
        }

        $request->setStreet($street);
    }
}
