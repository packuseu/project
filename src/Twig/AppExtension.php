<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace App\Twig;

use App\Entity\Channel\Channel;
use App\Entity\Product\Product;
use App\Entity\Product\ProductVariant;
use App\Entity\Shipping\Shipment;
use App\Form\Type\QuoteType;
use App\Model\Quote;
use App\Repository\Taxon\TaxonRepository;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;
use Sylius\Bundle\MoneyBundle\Formatter\MoneyFormatterInterface;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ShipmentInterface;
use Sylius\Component\Core\Model\ShopUserInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Taxation\Calculator\CalculatorInterface;
use Sylius\Component\Taxation\Resolver\TaxRateResolverInterface;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /**
     * @var NodeRepositoryInterface
     */
    private $nodeRepository;

    /**
     * @var TaxonRepository
     */
    private $taxonRepository;

    /**
     * @var RepositoryInterface
     */
    private $shippingExportRepository;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @var ChannelContextInterface
     */
    protected $channelContext;

    /**
     * @var LocaleContextInterface
     */
    protected $localeContext;

    /**
     * @var MoneyFormatterInterface
     */
    protected $moneyFormatter;

    /**
     * @var TaxRateResolverInterface
     */
    private $taxRateResolver;

    /**
     * @var CalculatorInterface
     */
    private $calculator;

    public function __construct(
        NodeRepositoryInterface $nodeRepository,
        TaxonRepository $taxonRepository,
        RepositoryInterface $shippingExportRepository,
        RequestStack $requestStack,
        FormFactoryInterface $formFactory,
        TokenStorageInterface $tokenStorage,
        Environment $twig,
        ChannelContextInterface $channelContext,
        LocaleContextInterface $localeContext,
        MoneyFormatterInterface $moneyFormatter,
        TaxRateResolverInterface $taxRateResolver,
        CalculatorInterface $calculator
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->taxonRepository = $taxonRepository;
        $this->shippingExportRepository = $shippingExportRepository;
        $this->requestStack = $requestStack;
        $this->formFactory = $formFactory;
        $this->tokenStorage = $tokenStorage;
        $this->twig = $twig;
        $this->channelContext = $channelContext;
        $this->localeContext = $localeContext;
        $this->moneyFormatter = $moneyFormatter;
        $this->taxRateResolver = $taxRateResolver;
        $this->calculator = $calculator;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('app_render_shipping_label_download_button', [$this, 'renderDownloadLabelButton']),
            new TwigFunction('app_get_product_node', [$this, 'getProductNode']),
            new TwigFunction('app_get_master_request', [$this, 'getMasterRequest']),
            new TwigFunction('app_render_quote_form', [$this, 'renderQuoteForm']),
            new TwigFunction('app_get_product_pricing_data', [$this, 'getProductPricingData']),
            new TwigFunction('app_get_price_with_tax', [$this, 'getPriceWithTax']),
            new TwigFunction('app_get_taxon_by_slug', [$this, 'getTaxonBySlug']),
            new TwigFunction('app_filter_product_options', [$this, 'filterProductOptions']),
        ];
    }

    /**
     * @param ShipmentInterface|Shipment $shipment
     *
     * @return string
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function renderDownloadLabelButton(ShipmentInterface $shipment): string
    {
        $return = '';
        $exports = $shipment->getShippingExports();

        foreach ($exports as $export) {
            if (!$export || !$export->getLabelPath()) {
                continue;
            }

            $return .= $this->twig->render(
                '@BitBagSyliusShippingExportPlugin/ShippingExport/Partial/_shipping_details_download.html.twig',
                ['data' => $export]
            );
        }

        return $return;
    }

    public function getProductNode(Product $product): ?NodeInterface
    {
        $taxon = $product->getMainTaxon();
        if (null === $taxon) {
            return null;
        }

        return $this->nodeRepository->findOneBy(
            [
                'type' => 'taxon',
                'relationId' => $taxon->getId(),
                'enabled' => true,
            ]
        );
    }

    public function renderQuoteForm(array $disable = [], Request $request = null, FormView $form = null): string
    {
        if ($form) {
            return $this->twig->render('Form/_quote_form.html.twig', ['form' => $form, 'disable' => $disable]);
        }

        $user = $this->tokenStorage->getToken()->getUser();
        $user = $user instanceof ShopUserInterface ? $user : null;
        $quote = new Quote($user);

        if ($request) {
            $this->processRequestInfo($quote, $request);
        }

        $form = $this->formFactory->create(QuoteType::class, $quote);

        return $this->twig->render('Form/_quote_form.html.twig', ['form' => $form->createView(), 'disable' => $disable]);
    }

    public function getProductPricingData(ProductInterface $product): string
    {
        $data = [];
        /** @var ChannelInterface $channel */
        $channel = $this->channelContext->getChannel();

        /** @var ProductVariant $variant */
        foreach ($product->getVariants() as $variant) {
            $data[$variant->getCode()]['options'] = [];
            $data[$variant->getCode()]['sellable'] = !$variant->isNonSellable();
            $data[$variant->getCode()]['max_sellable'] = $variant->getMaxSellable();
            $data[$variant->getCode()]['min_sellable'] = $variant->getMinSellable();
            $data[$variant->getCode()]['tier_prices'] = [];
            $data[$variant->getCode()]['price'] = $this->moneyFormatter->format(
                $variant->getChannelPricingForChannel($channel)->getPrice(),
                $channel->getBaseCurrency()->getCode(),
                $this->localeContext->getLocaleCode()
            );
            $data[$variant->getCode()]['full_price'] = $this->getPriceWithTax(
                $variant->getChannelPricingForChannel($channel)->getPrice(),
                $variant
            );
            $data[$variant->getCode()]['name'] = $variant->getName();

            foreach ($variant->getOptionValues() as $value) {
                $data[$variant->getCode()]['options'][] = [
                    'option' => $value->getOption()->getCode(),
                    'value' => $value->getCode(),
                    'name' => $value->getValue(),
                ];
            }

            foreach ($variant->getTierPricesForChannel($channel) as $tierPrice) {
                $data[$variant->getCode()]['tier_prices'][] = [
                    'price' => $this->moneyFormatter->format(
                        $tierPrice->getPrice(),
                        $channel->getBaseCurrency()->getCode(),
                        $this->localeContext->getLocaleCode()
                    ),
                    'full_price' => $this->getPriceWithTax($tierPrice->getPrice(), $variant),
                    'quantity' => $tierPrice->getQty()
                ];
            }
        }

        return json_encode($data);
    }

    public function getPriceWithTax(int $price, ProductVariant $variant): string
    {
        /** @var ChannelInterface|Channel $channel */
        $channel = $this->channelContext->getChannel();
        $taxRate = $this->taxRateResolver->resolve($variant);

        if (!$taxRate) {
            return '--';
        }

        $tax = $this->calculator->calculate(
            (float) $price,
            $taxRate
        );

        return $this->moneyFormatter->format(
            (int) ($price + $tax),
            $channel->getBaseCurrency()->getCode(),
            $this->localeContext->getLocaleCode()
        );
    }

    public function getTaxonBySlug(string $slug): ?TaxonInterface
    {
        return $this->taxonRepository->findOneBySlug($slug, $this->localeContext->getLocaleCode());
    }

    /**
     * @return Request|null
     */
    public function getMasterRequest(): ?Request
    {
        return $this->requestStack->getMasterRequest();
    }

    public function filterProductOptions(FormView $optionForm, Product $product): void
    {
        $code = $optionForm->vars['name'] ?? null;
        $choices = [];
        $variantOptions = [];

        if (!$code) {
            return;
        }

        /** @var ProductVariant $variant */
        foreach ($product->getVariants() as $variant) {
            /** @var ProductOptionValueInterface $value */
            foreach ($variant->getOptionValues() as $value) {
                if ($value->getOptionCode() == $code) {
                    $variantOptions[] = $value->getCode();
                }
            }
        }

        /** @var ChoiceView $choice */
        foreach ($optionForm->vars['choices'] as $choice) {
            if (in_array($choice->value, $variantOptions)) {
                $choices[] = $choice;
            }
        }

        $optionForm->vars['choices'] = $choices;
    }

    private function processRequestInfo(Quote $quote, Request $request): void
    {
        $quantity = $request->query->get('quantity');
        $deadline = $request->query->get('deadline');

        if ($quantity && is_numeric($quantity)) {
            $quote->setQuantity((int) $quantity);
        }

        if ($deadline) {
            try {
                $deadline = new \DateTime($deadline);
            } catch (\Exception $e) {
                $deadline = null;
            }

            $quote->setDeadline($deadline);
        }
    }
}
