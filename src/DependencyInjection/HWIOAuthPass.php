<?php

namespace App\DependencyInjection;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class HWIOAuthPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container) {
        // This is a temp solution because we cannot use the latest version of hwi oauth bundle as it
        // conflicts with php-http/client-common 1.10 that is used by our current guzzle adaptors
        $container->getDefinition('hwi_oauth.resource_owner.facebook')->setPublic(true);
    }
}
