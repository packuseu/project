<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(__DIR__);
$baseDir = dirname($vendorDir);

return array(
    'winzou\\Bundle\\StateMachineBundle\\' => array($vendorDir . '/winzou/state-machine-bundle'),
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/reflection-docblock/src', $vendorDir . '/phpdocumentor/type-resolver/src'),
    'Zend\\Stdlib\\' => array($vendorDir . '/zendframework/zend-stdlib/src'),
    'Zend\\Hydrator\\' => array($vendorDir . '/zendframework/zend-hydrator/src'),
    'WhiteOctober\\PagerfantaBundle\\' => array($vendorDir . '/white-october/pagerfanta-bundle'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Webimpress\\SafeWriter\\' => array($vendorDir . '/webimpress/safe-writer/src'),
    'Twig\\Extensions\\' => array($vendorDir . '/twig/extensions/src'),
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'Tests\\Omni\\Plugin\\SyliusParcelMachinePlugin\\' => array($vendorDir . '/omni/sylius-parcel-machine-plugin/tests'),
    'Tests\\Omni\\Plugin\\SyliusDpdPlugin\\' => array($vendorDir . '/omni/sylius-dpd-plugin/tests'),
    'Tests\\FriendsOfSylius\\SyliusImportExportPlugin\\' => array($vendorDir . '/friendsofsylius/sylius-import-export-plugin/tests'),
    'Tests\\Brille24\\SyliusTierPricePlugin\\' => array($vendorDir . '/brille24/sylius-tierprice-plugin/tests'),
    'Tests\\BitBag\\SyliusShippingExportPlugin\\' => array($vendorDir . '/bitbag/shipping-export-plugin/tests'),
    'Symplify\\TokenRunner\\' => array($vendorDir . '/symplify/token-runner/src'),
    'Symplify\\PackageBuilder\\' => array($vendorDir . '/symplify/package-builder/src'),
    'Symplify\\EasyCodingStandard\\SniffRunner\\' => array($vendorDir . '/symplify/easy-coding-standard/packages/SniffRunner/src'),
    'Symplify\\EasyCodingStandard\\FixerRunner\\' => array($vendorDir . '/symplify/easy-coding-standard/packages/FixerRunner/src'),
    'Symplify\\EasyCodingStandard\\Configuration\\' => array($vendorDir . '/symplify/easy-coding-standard/packages/Configuration/src'),
    'Symplify\\EasyCodingStandard\\ChangedFilesDetector\\' => array($vendorDir . '/symplify/easy-coding-standard/packages/ChangedFilesDetector/src'),
    'Symplify\\EasyCodingStandard\\' => array($vendorDir . '/symplify/easy-coding-standard/src'),
    'Symplify\\CodingStandard\\' => array($vendorDir . '/symplify/coding-standard/src'),
    'Symplify\\BetterPhpDocParser\\' => array($vendorDir . '/symplify/better-phpdoc-parser/src'),
    'Symfony\\WebpackEncoreBundle\\' => array($vendorDir . '/symfony/webpack-encore-bundle/src'),
    'Symfony\\Thanks\\' => array($vendorDir . '/symfony/thanks/src'),
    'Symfony\\Polyfill\\Php81\\' => array($vendorDir . '/symfony/polyfill-php81'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Php72\\' => array($vendorDir . '/symfony/polyfill-php72'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Normalizer\\' => array($vendorDir . '/symfony/polyfill-intl-normalizer'),
    'Symfony\\Polyfill\\Intl\\Idn\\' => array($vendorDir . '/symfony/polyfill-intl-idn'),
    'Symfony\\Polyfill\\Intl\\Icu\\' => array($vendorDir . '/symfony/polyfill-intl-icu'),
    'Symfony\\Polyfill\\Intl\\Grapheme\\' => array($vendorDir . '/symfony/polyfill-intl-grapheme'),
    'Symfony\\Polyfill\\Iconv\\' => array($vendorDir . '/symfony/polyfill-iconv'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Flex\\' => array($vendorDir . '/symfony/flex/src'),
    'Symfony\\Contracts\\Translation\\' => array($vendorDir . '/symfony/translation-contracts'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Contracts\\HttpClient\\' => array($vendorDir . '/symfony/http-client-contracts'),
    'Symfony\\Contracts\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher-contracts'),
    'Symfony\\Contracts\\Cache\\' => array($vendorDir . '/symfony/cache-contracts'),
    'Symfony\\Component\\Yaml\\' => array($vendorDir . '/symfony/yaml'),
    'Symfony\\Component\\VarExporter\\' => array($vendorDir . '/symfony/var-exporter'),
    'Symfony\\Component\\VarDumper\\' => array($vendorDir . '/symfony/var-dumper'),
    'Symfony\\Component\\Validator\\' => array($vendorDir . '/symfony/validator'),
    'Symfony\\Component\\Translation\\' => array($vendorDir . '/symfony/translation'),
    'Symfony\\Component\\Templating\\' => array($vendorDir . '/symfony/templating'),
    'Symfony\\Component\\String\\' => array($vendorDir . '/symfony/string'),
    'Symfony\\Component\\Stopwatch\\' => array($vendorDir . '/symfony/stopwatch'),
    'Symfony\\Component\\Security\\' => array($vendorDir . '/symfony/security'),
    'Symfony\\Component\\Routing\\' => array($vendorDir . '/symfony/routing'),
    'Symfony\\Component\\PropertyAccess\\' => array($vendorDir . '/symfony/property-access'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\OptionsResolver\\' => array($vendorDir . '/symfony/options-resolver'),
    'Symfony\\Component\\Mime\\' => array($vendorDir . '/symfony/mime'),
    'Symfony\\Component\\Lock\\' => array($vendorDir . '/symfony/lock'),
    'Symfony\\Component\\Intl\\' => array($vendorDir . '/symfony/intl'),
    'Symfony\\Component\\Inflector\\' => array($vendorDir . '/symfony/inflector'),
    'Symfony\\Component\\HttpKernel\\' => array($vendorDir . '/symfony/http-kernel'),
    'Symfony\\Component\\HttpFoundation\\' => array($vendorDir . '/symfony/http-foundation'),
    'Symfony\\Component\\Form\\' => array($vendorDir . '/symfony/form'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\ExpressionLanguage\\' => array($vendorDir . '/symfony/expression-language'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Symfony\\Component\\ErrorHandler\\' => array($vendorDir . '/symfony/error-handler'),
    'Symfony\\Component\\Dotenv\\' => array($vendorDir . '/symfony/dotenv'),
    'Symfony\\Component\\DomCrawler\\' => array($vendorDir . '/symfony/dom-crawler'),
    'Symfony\\Component\\DependencyInjection\\' => array($vendorDir . '/symfony/dependency-injection'),
    'Symfony\\Component\\Debug\\' => array($vendorDir . '/symfony/debug'),
    'Symfony\\Component\\CssSelector\\' => array($vendorDir . '/symfony/css-selector'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Symfony\\Component\\Config\\' => array($vendorDir . '/symfony/config'),
    'Symfony\\Component\\Cache\\' => array($vendorDir . '/symfony/cache'),
    'Symfony\\Component\\BrowserKit\\' => array($vendorDir . '/symfony/browser-kit'),
    'Symfony\\Component\\Asset\\' => array($vendorDir . '/symfony/asset'),
    'Symfony\\Bundle\\WebProfilerBundle\\' => array($vendorDir . '/symfony/web-profiler-bundle'),
    'Symfony\\Bundle\\TwigBundle\\' => array($vendorDir . '/symfony/twig-bundle'),
    'Symfony\\Bundle\\SwiftmailerBundle\\' => array($vendorDir . '/symfony/swiftmailer-bundle'),
    'Symfony\\Bundle\\SecurityBundle\\' => array($vendorDir . '/symfony/security-bundle'),
    'Symfony\\Bundle\\MonologBundle\\' => array($vendorDir . '/symfony/monolog-bundle'),
    'Symfony\\Bundle\\FrameworkBundle\\' => array($vendorDir . '/symfony/framework-bundle'),
    'Symfony\\Bundle\\DebugBundle\\' => array($vendorDir . '/symfony/debug-bundle'),
    'Symfony\\Bridge\\Twig\\' => array($vendorDir . '/symfony/twig-bridge'),
    'Symfony\\Bridge\\ProxyManager\\' => array($vendorDir . '/symfony/proxy-manager-bridge'),
    'Symfony\\Bridge\\PhpUnit\\' => array($vendorDir . '/symfony/phpunit-bridge'),
    'Symfony\\Bridge\\Monolog\\' => array($vendorDir . '/symfony/monolog-bridge'),
    'Symfony\\Bridge\\Doctrine\\' => array($vendorDir . '/symfony/doctrine-bridge'),
    'Sylius\\Component\\Resource\\' => array($vendorDir . '/sylius/resource-bundle/src/Component'),
    'Sylius\\Component\\Registry\\' => array($vendorDir . '/sylius/registry/src'),
    'Sylius\\Component\\Mailer\\' => array($vendorDir . '/sylius/mailer-bundle/src/Component'),
    'Sylius\\Component\\Grid\\' => array($vendorDir . '/sylius/grid-bundle/src/Component'),
    'Sylius\\Component\\' => array($vendorDir . '/sylius/sylius/src/Sylius/Component'),
    'Sylius\\Bundle\\ThemeBundle\\' => array($vendorDir . '/sylius/theme-bundle/src'),
    'Sylius\\Bundle\\ResourceBundle\\' => array($vendorDir . '/sylius/resource-bundle/src/Bundle'),
    'Sylius\\Bundle\\MailerBundle\\' => array($vendorDir . '/sylius/mailer-bundle/src/Bundle'),
    'Sylius\\Bundle\\GridBundle\\' => array($vendorDir . '/sylius/grid-bundle/src/Bundle'),
    'Sylius\\Bundle\\FixturesBundle\\' => array($vendorDir . '/sylius/fixtures-bundle/src'),
    'Sylius\\Bundle\\' => array($vendorDir . '/sylius/sylius/src/Sylius/Bundle'),
    'Sylius\\Behat\\' => array($vendorDir . '/sylius/sylius/src/Sylius/Behat'),
    'SyliusLabs\\Polyfill\\Symfony\\EventDispatcher\\' => array($vendorDir . '/sylius-labs/polyfill-symfony-event-dispatcher/src'),
    'SyliusLabs\\AssociationHydrator\\' => array($vendorDir . '/sylius-labs/association-hydrator/src'),
    'SwedbankPaymentPortal\\' => array($vendorDir . '/swedbank-spp/swedbank-payment-portal/src'),
    'Stripe\\' => array($vendorDir . '/stripe/stripe-php/lib'),
    'Stof\\DoctrineExtensionsBundle\\' => array($vendorDir . '/stof/doctrine-extensions-bundle/src'),
    'Sonata\\Twig\\' => array($vendorDir . '/sonata-project/core-bundle/src/Twig'),
    'Sonata\\Serializer\\' => array($vendorDir . '/sonata-project/core-bundle/src/Serializer'),
    'Sonata\\IntlBundle\\' => array($vendorDir . '/sonata-project/intl-bundle/src'),
    'Sonata\\Form\\' => array($vendorDir . '/sonata-project/core-bundle/src/Form'),
    'Sonata\\Doctrine\\Bridge\\Symfony\\' => array($vendorDir . '/sonata-project/doctrine-extensions/src/Bridge/Symfony'),
    'Sonata\\Doctrine\\' => array($vendorDir . '/sonata-project/doctrine-extensions/src'),
    'Sonata\\CoreBundle\\' => array($vendorDir . '/sonata-project/core-bundle/src/CoreBundle'),
    'Sonata\\Cache\\' => array($vendorDir . '/sonata-project/cache/src'),
    'Sonata\\BlockBundle\\' => array($vendorDir . '/sonata-project/block-bundle/src'),
    'SlevomatCodingStandard\\' => array($vendorDir . '/slevomat/coding-standard/SlevomatCodingStandard'),
    'SlamCsFixer\\' => array($vendorDir . '/slam/php-cs-fixer-extensions/lib'),
    'SitemapPlugin\\' => array($vendorDir . '/stefandoorn/sitemap-plugin/src'),
    'SensioLabs\\Security\\' => array($vendorDir . '/sensiolabs/security-checker/SensioLabs/Security'),
    'Safe\\' => array($vendorDir . '/thecodingmachine/safe/lib', $vendorDir . '/thecodingmachine/safe/generated'),
    'Ramsey\\Uuid\\' => array($vendorDir . '/ramsey/uuid/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'ProxyManager\\' => array($vendorDir . '/ocramius/proxy-manager/src/ProxyManager'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src/Prophecy'),
    'Port\\Csv\\' => array($vendorDir . '/portphp/csv/src'),
    'Port\\' => array($vendorDir . '/portphp/portphp/src'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src/PhpOption'),
    'PhpCsFixer\\' => array($vendorDir . '/friendsofphp/php-cs-fixer/src'),
    'Payum\\ISO4217\\' => array($vendorDir . '/payum/iso4217'),
    'Payum\\Bundle\\PayumBundle\\' => array($vendorDir . '/payum/payum-bundle'),
    'Payum\\' => array($vendorDir . '/payum/payum/src/Payum'),
    'Pagerfanta\\Twig\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Twig'),
    'Pagerfanta\\Solarium\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Adapter/Solarium'),
    'Pagerfanta\\Elastica\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Adapter/Elastica'),
    'Pagerfanta\\Doctrine\\PHPCRODM\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Adapter/Doctrine/PHPCRODM'),
    'Pagerfanta\\Doctrine\\ORM\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Adapter/Doctrine/ORM'),
    'Pagerfanta\\Doctrine\\MongoDBODM\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Adapter/Doctrine/MongoDBODM'),
    'Pagerfanta\\Doctrine\\DBAL\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Adapter/Doctrine/DBAL'),
    'Pagerfanta\\Doctrine\\Collections\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Adapter/Doctrine/Collections'),
    'Pagerfanta\\Adapter\\' => array($vendorDir . '/pagerfanta/pagerfanta/src/Adapter'),
    'Pagerfanta\\' => array($vendorDir . '/pagerfanta/pagerfanta/lib/Core'),
    'PackageVersions\\' => array($vendorDir . '/composer/package-versions-deprecated/src/PackageVersions'),
    'PSS\\SymfonyMockerContainer\\' => array($vendorDir . '/polishsymfonycommunity/symfony-mocker-container/src'),
    'PHPStan\\ExtensionInstaller\\' => array($vendorDir . '/phpstan/extension-installer/src'),
    'PHPStan\\' => array($vendorDir . '/phpstan/phpstan-doctrine/src', $vendorDir . '/phpstan/phpstan-webmozart-assert/src'),
    'Omnipay\\Common\\' => array($vendorDir . '/omnipay/common/src/Common'),
    'Omni\\Sylius\\TranslatorPlugin\\' => array($vendorDir . '/omni/sylius-translator-plugin/src'),
    'Omni\\Sylius\\SwedbankSpp\\' => array($vendorDir . '/omni/sylius-swedbank-spp-plugin/src'),
    'Omni\\Sylius\\ShippingPlugin\\' => array($vendorDir . '/omni/sylius-shipping-plugin/src'),
    'Omni\\Sylius\\SeoPlugin\\' => array($vendorDir . '/omni/sylius-seo-plugin/src'),
    'Omni\\Sylius\\SearchPlugin\\' => array($vendorDir . '/omni/sylius-search-plugin/src'),
    'Omni\\Sylius\\PayseraPlugin\\' => array($vendorDir . '/omni/sylius-paysera-plugin/src'),
    'Omni\\Sylius\\ParcelMachinePlugin\\' => array($vendorDir . '/omni/sylius-parcel-machine-plugin/src'),
    'Omni\\Sylius\\ManifestPlugin\\' => array($vendorDir . '/omni/sylius-manifest-plugin/src'),
    'Omni\\Sylius\\ImportPlugin\\' => array($vendorDir . '/omni/sylius-import-plugin/src'),
    'Omni\\Sylius\\FilterPlugin\\' => array($vendorDir . '/omni/sylius-filter-plugin/src'),
    'Omni\\Sylius\\DpdPlugin\\' => array($vendorDir . '/omni/sylius-dpd-plugin/src'),
    'Omni\\Sylius\\CorePlugin\\' => array($vendorDir . '/omni/sylius-core-plugin/src'),
    'Omni\\Sylius\\CmsPlugin\\' => array($vendorDir . '/omni/sylius-cms-plugin/src'),
    'Omni\\Sylius\\BannerPlugin\\' => array($vendorDir . '/omni/sylius-banner-plugin/src'),
    'OAuth2\\' => array($vendorDir . '/friendsofsymfony/oauth2-php/lib'),
    'Nfq\\DpdClient\\' => array($vendorDir . '/nfq/dpd-client/src'),
    'Nelmio\\Alice\\' => array($vendorDir . '/nelmio/alice/src'),
    'Negotiation\\' => array($vendorDir . '/willdurand/negotiation/src/Negotiation'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'Money\\' => array($vendorDir . '/moneyphp/money/src'),
    'Liip\\ImagineBundle\\' => array($vendorDir . '/liip/imagine-bundle'),
    'Lexik\\Bundle\\TranslationBundle\\' => array($vendorDir . '/lexik/translation-bundle'),
    'League\\Uri\\' => array($vendorDir . '/league/uri/src', $vendorDir . '/league/uri-components/src', $vendorDir . '/league/uri-interfaces/src'),
    'Laminas\\ZendFrameworkBridge\\' => array($vendorDir . '/laminas/laminas-zendframework-bridge/src'),
    'Laminas\\EventManager\\' => array($vendorDir . '/laminas/laminas-eventmanager/src'),
    'Laminas\\Code\\' => array($vendorDir . '/laminas/laminas-code/src'),
    'Lakion\\Behat\\MinkDebugExtension\\' => array($vendorDir . '/lakion/mink-debug-extension/src'),
    'Lakion\\ApiTestCase\\' => array($vendorDir . '/lchrusciel/api-test-case/src'),
    'Knp\\Snappy\\' => array($vendorDir . '/knplabs/knp-snappy/src/Knp/Snappy'),
    'Knp\\Menu\\' => array($vendorDir . '/knplabs/knp-menu/src/Knp/Menu'),
    'Knp\\Bundle\\SnappyBundle\\' => array($vendorDir . '/knplabs/knp-snappy-bundle/src'),
    'Knp\\Bundle\\MenuBundle\\' => array($vendorDir . '/knplabs/knp-menu-bundle/src'),
    'Knp\\Bundle\\GaufretteBundle\\' => array($vendorDir . '/knplabs/knp-gaufrette-bundle'),
    'Jean85\\' => array($vendorDir . '/jean85/pretty-package-versions/src'),
    'JMS\\SerializerBundle\\' => array($vendorDir . '/jms/serializer-bundle'),
    'Interop\\Queue\\' => array($vendorDir . '/queue-interop/queue-interop/src'),
    'Imagine\\' => array($vendorDir . '/imagine/imagine/src'),
    'Http\\Promise\\' => array($vendorDir . '/php-http/promise/src'),
    'Http\\Message\\' => array($vendorDir . '/php-http/message/src', $vendorDir . '/php-http/message-factory/src'),
    'Http\\HttplugBundle\\' => array($vendorDir . '/php-http/httplug-bundle/src'),
    'Http\\Discovery\\' => array($vendorDir . '/php-http/discovery/src'),
    'Http\\Client\\Common\\Plugin\\' => array($vendorDir . '/php-http/logger-plugin/src', $vendorDir . '/php-http/stopwatch-plugin/src'),
    'Http\\Client\\Common\\' => array($vendorDir . '/php-http/client-common/src'),
    'Http\\Client\\' => array($vendorDir . '/php-http/httplug/src'),
    'Http\\Adapter\\Guzzle6\\' => array($vendorDir . '/php-http/guzzle6-adapter/src'),
    'HWI\\Bundle\\OAuthBundle\\' => array($vendorDir . '/hwi/oauth-bundle'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'Gedmo\\' => array($vendorDir . '/gedmo/doctrine-extensions/lib/Gedmo'),
    'FriendsOfSylius\\SyliusImportExportPlugin\\' => array($vendorDir . '/friendsofsylius/sylius-import-export-plugin/src'),
    'FriendsOfBehat\\VariadicExtension\\' => array($vendorDir . '/friends-of-behat/variadic-extension/src'),
    'FriendsOfBehat\\SymfonyExtension\\' => array($vendorDir . '/friends-of-behat/symfony-extension/src'),
    'FriendsOfBehat\\SuiteSettingsExtension\\' => array($vendorDir . '/friends-of-behat/suite-settings-extension/src'),
    'FriendsOfBehat\\PageObjectExtension\\' => array($vendorDir . '/friends-of-behat/page-object-extension/src'),
    'Fidry\\AliceDataFixtures\\' => array($vendorDir . '/theofidry/alice-data-fixtures/src'),
    'Faker\\' => array($vendorDir . '/fzaninotto/faker/src/Faker'),
    'FOS\\RestBundle\\' => array($vendorDir . '/friendsofsymfony/rest-bundle'),
    'FOS\\OAuthServerBundle\\' => array($vendorDir . '/friendsofsymfony/oauth-server-bundle'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/src'),
    'Doctrine\\Persistence\\' => array($vendorDir . '/doctrine/persistence/lib/Doctrine/Persistence'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib/Doctrine/ORM'),
    'Doctrine\\Migrations\\' => array($vendorDir . '/doctrine/migrations/lib/Doctrine/Migrations'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\Deprecations\\' => array($vendorDir . '/doctrine/deprecations/lib/Doctrine/Deprecations'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib/Doctrine/DBAL'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Common/Inflector'),
    'Doctrine\\Common\\DataFixtures\\' => array($vendorDir . '/doctrine/data-fixtures/lib/Doctrine/Common/DataFixtures'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib/Doctrine/Common/Collections'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib/Doctrine/Common', $vendorDir . '/doctrine/event-manager/lib/Doctrine/Common', $vendorDir . '/doctrine/persistence/lib/Doctrine/Common', $vendorDir . '/doctrine/reflection/lib/Doctrine/Common'),
    'Doctrine\\Bundle\\MigrationsBundle\\' => array($vendorDir . '/doctrine/doctrine-migrations-bundle'),
    'Doctrine\\Bundle\\FixturesBundle\\' => array($vendorDir . '/doctrine/doctrine-fixtures-bundle'),
    'Doctrine\\Bundle\\DoctrineCacheBundle\\' => array($vendorDir . '/doctrine/doctrine-cache-bundle'),
    'Doctrine\\Bundle\\DoctrineBundle\\' => array($vendorDir . '/doctrine/doctrine-bundle'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
    'Composer\\XdebugHandler\\' => array($vendorDir . '/composer/xdebug-handler/src'),
    'Composer\\Semver\\' => array($vendorDir . '/composer/semver/src'),
    'Composer\\Pcre\\' => array($vendorDir . '/composer/pcre/src'),
    'Composer\\CaBundle\\' => array($vendorDir . '/composer/ca-bundle/src'),
    'Coduo\\PHPMatcher\\' => array($vendorDir . '/coduo/php-matcher/src'),
    'Cocur\\Slugify\\' => array($vendorDir . '/cocur/slugify/src'),
    'Clue\\StreamFilter\\' => array($vendorDir . '/clue/stream-filter/src'),
    'CacheTool\\' => array($vendorDir . '/gordalina/cachetool/src'),
    'Brille24\\SyliusTierPricePlugin\\' => array($vendorDir . '/brille24/sylius-tierprice-plugin/src'),
    'BitBag\\SyliusShippingExportPlugin\\' => array($vendorDir . '/bitbag/shipping-export-plugin/src'),
    'Behat\\Transliterator\\' => array($vendorDir . '/behat/transliterator/src/Behat/Transliterator'),
    'Behat\\Testwork\\' => array($vendorDir . '/behat/behat/src/Behat/Testwork'),
    'Behat\\Step\\' => array($vendorDir . '/behat/behat/src/Behat/Step'),
    'Behat\\Mink\\Driver\\' => array($vendorDir . '/behat/mink-browserkit-driver/src', $vendorDir . '/behat/mink-selenium2-driver/src'),
    'Behat\\Mink\\' => array($vendorDir . '/behat/mink/src'),
    'Behat\\Hook\\' => array($vendorDir . '/behat/behat/src/Behat/Hook'),
    'Behat\\Behat\\' => array($vendorDir . '/behat/behat/src/Behat/Behat'),
    'Bazinga\\Bundle\\HateoasBundle\\' => array($vendorDir . '/willdurand/hateoas-bundle'),
    'App\\Tests\\' => array($baseDir . '/tests'),
    'App\\' => array($baseDir . '/src', $baseDir . '/src'),
    'Alcohol\\' => array($vendorDir . '/alcohol/iso4217'),
);
