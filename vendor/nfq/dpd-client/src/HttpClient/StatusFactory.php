<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\HttpClient;

use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\ErrorPlugin;
use Http\Client\Common\Plugin\HeaderSetPlugin;
use Http\Client\Common\PluginClient;
use Http\Client\HttpClient;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\UriFactoryDiscovery;

class StatusFactory
{
    private const BASE_URL = 'https://status.dpd.lt';

    public static function create(
        HttpClient $client = null
    ): HttpClient {
        if (null === $client) {
            $client = HttpClientDiscovery::find();
        }

        $plugins = [
            new BaseUriPlugin(UriFactoryDiscovery::find()->createUri(self::BASE_URL), ['replace' => true]),
            new ErrorPlugin(),
            new HeaderSetPlugin(['Content-Type' => 'application/x-www-form-urlencoded']),
        ];

        return new PluginClient($client, $plugins);
    }
}
