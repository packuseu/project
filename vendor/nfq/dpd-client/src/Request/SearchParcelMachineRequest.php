<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Request;

class SearchParcelMachineRequest implements RequestInterface
{
    /**
     * @var string|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $company;

    /**
     * @var string (ISO 3166-1 alpha-3 country codes format) e.g. LT, LV, EE.
     */
    private $country;

    /**
     * @var string|null
     */
    private $city;

    /**
     * @var string|null
     */
    private $street;

    /**
     * @var string|null Post code without country code.
     */
    private $postCode;

    public function __construct(string $country)
    {
        $this->country = $country;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id): SearchParcelMachineRequest
    {
        $this->id = $id;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): SearchParcelMachineRequest
    {
        $this->company = $company;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): SearchParcelMachineRequest
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): SearchParcelMachineRequest
    {
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): SearchParcelMachineRequest
    {
        $this->street = $street;

        return $this;
    }

    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    public function setPostCode(?string $postCode): SearchParcelMachineRequest
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function toArray(): array
    {
        $data = [
            'id' => $this->getId(),
            'company' => $this->getCompany(),
            'country' => $this->getCountry(),
            'city' => $this->getCity(),
            'street' => $this->getStreet(),
            'pcode' => $this->getPostCode(),
            'fetchGsPUDOpoint' => '1', // Mandatory value, always should be 1.
        ];

        return array_filter($data);
    }
}
