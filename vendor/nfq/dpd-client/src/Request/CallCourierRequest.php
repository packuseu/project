<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Request;

class CallCourierRequest implements RequestInterface
{
    /**
     * @var int
     */
    private $requestOrderNr = 1;

    /**
     * DPD API username
     * @var string
     */
    private $payerId;

    /**
     * @var string
     */
    private $senderAddress;

    /**
     * @var string
     */
    private $senderCity;

    /**
     * @var string
     */
    private $senderCountry;

    /**
     * @var string
     */
    private $senderPostalCode;

    /**
     * @var string
     */
    private $senderContact;

    /**
     * @var string
     */
    private $senderPhone;

    /**
     * @var string
     */
    private $senderWorkUntil;

    /**
     * @var string
     */
    private $pickupTime;

    /**
     * @var float
     */
    private $weight;

    /**
     * @var int
     */
    private $parcelsCount;

    /**
     * @var integer
     */
    private $palletsCount = 0;

    /**
     * @var string|null
     */
    private $comments;

    /**
     * @return int
     */
    public function getRequestOrderNr(): int
    {
        return $this->requestOrderNr;
    }

    /**
     * @param int $requestOrderNr
     */
    public function setRequestOrderNr(int $requestOrderNr): void
    {
        $this->requestOrderNr = $requestOrderNr;
    }

    /**
     * @return string
     */
    public function getPayerId(): string
    {
        return $this->payerId;
    }

    /**
     * @param string $payerId
     */
    public function setPayerId(string $payerId): void
    {
        $this->payerId = $payerId;
    }

    /**
     * @return string
     */
    public function getSenderAddress(): string
    {
        return $this->senderAddress;
    }

    /**
     * @param string $senderAddress
     */
    public function setSenderAddress(string $senderAddress): void
    {
        $this->senderAddress = $senderAddress;
    }

    /**
     * @return string
     */
    public function getSenderCity(): string
    {
        return $this->senderCity;
    }

    /**
     * @param string $senderCity
     */
    public function setSenderCity(string $senderCity): void
    {
        $this->senderCity = $senderCity;
    }

    /**
     * @return string
     */
    public function getSenderCountry(): string
    {
        return $this->senderCountry;
    }

    /**
     * @param string $senderCountry
     */
    public function setSenderCountry(string $senderCountry): void
    {
        $this->senderCountry = $senderCountry;
    }

    /**
     * @return string
     */
    public function getSenderPostalCode(): string
    {
        return $this->senderPostalCode;
    }

    /**
     * @param string $senderPostalCode
     */
    public function setSenderPostalCode(string $senderPostalCode): void
    {
        $this->senderPostalCode = $senderPostalCode;
    }

    /**
     * @return string
     */
    public function getSenderContact(): string
    {
        return $this->senderContact;
    }

    /**
     * @param string $senderContact
     */
    public function setSenderContact(string $senderContact): void
    {
        $this->senderContact = $senderContact;
    }

    /**
     * @return string
     */
    public function getSenderPhone(): string
    {
        return $this->senderPhone;
    }

    /**
     * @param string $senderPhone
     */
    public function setSenderPhone(string $senderPhone): void
    {
        $this->senderPhone = $senderPhone;
    }

    /**
     * @return string
     */
    public function getSenderWorkUntil(): string
    {
        return $this->senderWorkUntil;
    }

    /**
     * @param string $senderWorkUntil
     */
    public function setSenderWorkUntil(string $senderWorkUntil): void
    {
        $this->senderWorkUntil = $senderWorkUntil;
    }

    /**
     * @return string
     */
    public function getPickupTime(): string
    {
        return $this->pickupTime;
    }

    /**
     * @param string $pickupTime
     */
    public function setPickupTime(string $pickupTime): void
    {
        $this->pickupTime = $pickupTime;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getParcelsCount(): int
    {
        return $this->parcelsCount;
    }

    /**
     * @param int $parcelsCount
     */
    public function setParcelsCount(int $parcelsCount): void
    {
        $this->parcelsCount = $parcelsCount;
    }

    /**
     * @return int
     */
    public function getPalletsCount(): int
    {
        return $this->palletsCount;
    }

    /**
     * @param int $palletsCount
     */
    public function setPalletsCount(int $palletsCount): void
    {
        $this->palletsCount = $palletsCount;
    }

    /**
     * @return string|null
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string|null $comments
     */
    public function setComments(?string $comments): void
    {
        $this->comments = $comments;
    }

    public function toArray(): array
    {
        $data = [
            'orderNr'          => $this->getRequestOrderNr(),
            'payerId'          => $this->getPayerId(),
            'senderAddress'    => $this->getSenderAddress(),
            'senderCity'       => $this->getSenderCity(),
            'senderCountry'    => $this->getSenderCountry(),
            'senderPostalCode' => $this->getSenderPostalCode(),
            'senderContact'    => $this->getSenderContact(),
            'senderPhone'      => $this->getSenderPhone(),
            'senderWorkUntil'  => $this->getSenderWorkUntil(),
            'pickupTime'       => $this->getPickupTime(),
            'weight'           => $this->getWeight(),
            'parcelsCount'     => $this->getParcelsCount(),
            'palletsCount'     => $this->getPalletsCount(),
            'nonStandard'      => $this->getComments()
        ];

        return $data;
    }
}
