<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Request;

class CheckTrackingRequest implements RequestInterface
{
    /** @var string */
    private $pknr;

    /** @var string */
    private $lang = 'en';

    /** @var bool */
    private $showAll = false;

    /** @var bool */
    private $detail = false;

    public function __construct(string $pknr)
    {
        $this->pknr = $pknr;
    }

    /**
     * @return string
     */
    public function getPknr(): string
    {
        return $this->pknr;
    }

    /**
     * @param string $pknr
     */
    public function setPknr(string $pknr): void
    {
        $this->pknr = $pknr;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang): void
    {
        $this->lang = $lang;
    }

    /**
     * @return bool
     */
    public function isShowAll(): bool
    {
        return $this->showAll;
    }

    /**
     * @param bool $showAll
     */
    public function setShowAll(bool $showAll): void
    {
        $this->showAll = $showAll;
    }

    /**
     * @return bool
     */
    public function isDetail(): bool
    {
        return $this->detail;
    }

    /**
     * @param bool $detail
     */
    public function setDetail(bool $detail): void
    {
        $this->detail = $detail;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'pknr' => $this->getPknr(),
            'lang' => $this->getLang(),
            'show_all' => $this->isShowAll(),
            'detail' => $this->isDetail(),
        ];
    }
}
