<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Request;

class CancelShipmentRequest implements RequestInterface
{
    /**
     * @var string[]
     */
    private $parcelNumbers = [];

    /**
     * @return string[]
     */
    public function getParcelNumbers(): array
    {
        return $this->parcelNumbers;
    }

    /**
     * @param array $parcelNumbers
     * @return CancelShipmentRequest
     */
    public function setParcelNumbers(array $parcelNumbers): CancelShipmentRequest
    {
        $this->parcelNumbers = $parcelNumbers;

        return $this;
    }

    /**
     * @param string $parcelNumber
     * @return CancelShipmentRequest
     */
    public function addParcelNumber(string $parcelNumber): CancelShipmentRequest
    {
        $this->parcelNumbers[] = $parcelNumber;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'parcels' => implode('|', $this->getParcelNumbers())
        ];
    }
}
