<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Request;

class CloseManifestRequest implements RequestInterface
{
    /**
     * @var \DateTime
     */
    private $closeDate;

    /**
     * @return \DateTime
     */
    public function getCloseDate(): \DateTime
    {
        return $this->closeDate;
    }

    /**
     * @param \DateTime $closeDate
     * @return CloseManifestRequest
     */
    public function setCloseDate(\DateTime $closeDate): CloseManifestRequest
    {
        $this->closeDate = $closeDate;

        return $this;
    }



    public function toArray(): array
    {
        return [
            'date' => $this->getCloseDate()->format('Y-m-d')
        ];
    }
}
