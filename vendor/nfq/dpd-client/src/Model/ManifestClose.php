<?php
/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Model;


class ManifestClose
{
    /**
     * @var string|null
     */
    private $status;

    /**
     * @var string
     */
    private $pdf;

    /**
     * @var \DateTime
     */
    private $closeDate;

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     *
     * @return $this
     */
    public function setStatus(?string $status): ManifestClose
    {
        $this->status = $status;

        return $this;
    }


    public function getPdf(): string
    {
        return $this->pdf;
    }

    public function setPdf(?string $pdf): ManifestClose
    {
        $this->pdf = $pdf;

        return $this;
    }

    public function getCloseDate(): \DateTime
    {
        return $this->closeDate;
    }

    public function setCloseDate(?\DateTime $closeDate): ManifestClose
    {
        $this->closeDate = $closeDate;

        return $this;
    }
}
