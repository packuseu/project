<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Model\Tracking;

class Detail
{
    /** @var string|null */
    private $status;

    /** @var string|null */
    private $statusCode;

    /** @var string|null */
    private $serviceCode;

    /** @var string */
    private $dateTime;

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     *
     * @return Detail
     */
    public function setStatus(?string $status): Detail
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatusCode(): ?string
    {
        return $this->statusCode;
    }

    /**
     * @param string|null $statusCode
     *
     * @return Detail
     */
    public function setStatusCode(?string $statusCode): Detail
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getServiceCode(): ?string
    {
        return $this->serviceCode;
    }

    /**
     * @param string|null $serviceCode
     *
     * @return Detail
     */
    public function setServiceCode(?string $serviceCode): Detail
    {
        $this->serviceCode = $serviceCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateTime(): string
    {
        return $this->dateTime;
    }

    /**
     * @param string $dateTime
     *
     * @return Detail
     */
    public function setDateTime(string $dateTime): Detail
    {
        $this->dateTime = $dateTime;

        return $this;
    }
}
