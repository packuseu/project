<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Factory\Model;

use Nfq\DpdClient\Model\ManifestClose;

class ManifestCloseFactory
{
    /**
     * @param string $content
     * @param \DateTime $closeDate
     *
     * @return ManifestClose
     */
    public static function createFromTestEnvResponse(array $data, \DateTime $closeDate): ManifestClose
    {
        $closeManifest = new ManifestClose();
        $closeManifest
            ->setStatus($data['status'])
            ->setPdf($data['pdf'])
            ->setCloseDate($closeDate);

        return $closeManifest;
    }

    /**
     * @param string $content
     * @param \DateTime $closeDate
     *
     * @return ManifestClose
     */
    public static function create(string $content, \DateTime $closeDate): ManifestClose
    {
        $closeManifest = new ManifestClose();
        $closeManifest
            ->setPdf($content)
            ->setCloseDate($closeDate);

        return $closeManifest;
    }
}
