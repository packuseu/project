<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Factory\Model;

use Nfq\DpdClient\Model\CancelShipment;

class CancelShipmentFactory
{
    /**
     * @param array $data
     * @return CancelShipment
     */
    public static function fromResponse(array $data): CancelShipment
    {
        return new CancelShipment($data['status']);
    }
}
