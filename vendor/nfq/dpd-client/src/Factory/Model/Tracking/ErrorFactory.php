<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Factory\Model\Tracking;

use Nfq\DpdClient\Model\Tracking\Error;

class ErrorFactory
{
    /** @var string */
    private const CODE = 'code';

    /** @var string */
    private const MESSAGE = 'message';

    /**
     * @param array $data
     *
     * @return Error
     */
    public static function fromResponse(array $data): Error
    {
        $error = new Error();
        $error
            ->setCode($data[self::CODE])
            ->setMessage($data[self::MESSAGE]);

        return $error;
    }

    public static function toResponse(Error $error): array
    {
        return [
            self::CODE => $error->getCode(),
            self::MESSAGE => $error->getMessage(),
        ];
    }
}
