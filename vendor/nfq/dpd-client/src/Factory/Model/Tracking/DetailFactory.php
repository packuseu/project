<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Factory\Model\Tracking;

use Nfq\DpdClient\Model\Tracking\Detail;

class DetailFactory
{
    /**
     * @param array $data
     *
     * @return Detail
     */
    public static function create(array $data): Detail
    {
        $detail = new Detail();
        $detail
            ->setStatus($data['status'] ?? null)
            ->setStatusCode($data['statusCode'] ?? null)
            ->setServiceCode($data['serviceCode'] ?? null)
            ->setDateTime($data['dateTime']);

        return $detail;
    }
    /**
     * @param array $data
     *
     * @return array
     */
    public static function fromResponse(array $data): array
    {
        $details = [];

        foreach ($data as $key => $value) {
            $details[] = self::create($value);
        }

        return $details;
    }
}
