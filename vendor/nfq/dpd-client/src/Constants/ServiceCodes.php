<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Constants;

class ServiceCodes
{
    public const STANDARD_PARCEL = 'D';
    public const STANDARD_PARCEL_DELIVERY_ON_SATURDAY = 'D-SAT';
    public const STANDARD_PARCEL_COD = 'D-COD';
    public const STANDARD_PARCEL_DELIVERY_ON_SATURDAY_COD = 'D-SAT-COD';
    public const STANDARD_PARCEL_SWAP = 'D-SWAP';
    public const STANDARD_PARCEL_DELIVERY_ON_SATURDAY_SWAP = 'D-SAT-SWAP';
    public const STANDARD_PARCEL_DOCUMENT_RETURN = 'D-DOCRET';
    public const STANDARD_PARCEL_DELIVERY_ON_SATURDAY_DOCUMENT_RETURN = 'D-SAT-DOCRET';
    public const STANDARD_PARCEL_COD_DOCUMENT_RETURN = 'D-COD-DOCRET';
    public const STANDARD_PARCEL_DELIVERY_ON_SATURDAY_COD_DOCUMENT_RETURN = 'D-SAT-COD-DOCRET';
    public const STANDARD_PARCEL_SWAP_DOCUMENT_RETURN = 'D-SWAP-DOCRET';
    public const STANDARD_PARCEL_DELIVERY_ON_SATURDAY_SWAP_DOCUMENT_RETURN = 'D-SAT-SWAP-DOCRET';
    public const EXPRESS10 = 'E10';
    public const EXPRESS10_DOCUMENT_RETURN = 'E10-DOCRET';
    public const EXPRESS12 = 'E12';
    public const EXPRESS12_COD = 'E12-COD';
    public const EXPRESS12_DOCUMENT_RETURN = 'E12-DOCRET';
    public const STANDARD_PARCEL_B2C = 'D-B2C';
    public const STANDARD_PARCEL_B2C_COD = 'D-B2C-COD';
    public const STANDARD_PARCEL_B2C_DOCUMENT_RETURN = 'D-B2C-DOCRET';
    public const STANDARD_PARCEL_B2C_COD_DOCUMENT_RETURN = 'D-COD-B2C-DOCRET';
    public const STANDARD_PARCEL_PARCEL_SHOP = 'PS';
    public const STANDARD_PARCEL_B2C_DOCUMENT_RETURN_DELIVERY_ON_SATURDAY = 'D-B2C-DOCRET-SAT';
    public const STANDARD_PARCEL_B2C_DELIVERY_ON_SATURDAY_COD_DOCUMENT_RETURN = 'D-COD-B2C-DOCRET-SAT';
    public const STANDARD_PARCEL_B2C_SWAP = 'D-B2C-SWAP';
    public const STANDARD_PARCEL_B2C_DELIVERY_ON_SATURDAY = 'D-B2C-SAT';
    public const STANDARD_PARCEL_B2C_DELIVERY_ON_SATURDAY_COD = 'D-B2C-SAT-COD';
    public const STANDARD_PARCEL_B2C_DELIVERY_ON_SATURDAY_SWAP = 'D-B2C-SAT-SWAP';
    public const STANDARD_PARCEL_B2C_CASH_ON_DELIVERY_SWAP = 'D-B2C-COD-SWAP';
    public const SAME_DAY = 'SD';
    public const DPD_MAX = 'DPD MAX';
}
