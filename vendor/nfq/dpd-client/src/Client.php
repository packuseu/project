<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient;

use Nfq\DpdClient\Exception\InvalidResponseException;
use Nfq\DpdClient\Factory\Model\CancelShipmentFactory;
use Nfq\DpdClient\Factory\Model\ManifestCloseFactory;
use Nfq\DpdClient\Factory\Model\ParcelMachineFactory;
use Nfq\DpdClient\Factory\Model\ParcelNumberListFactory;
use Nfq\DpdClient\Factory\Model\Tracking\ErrorFactory;
use Nfq\DpdClient\Factory\Model\Tracking\TrackingFactory;
use Nfq\DpdClient\HttpClient\Client as HttpClient;
use Nfq\DpdClient\Model\CancelShipment;
use Nfq\DpdClient\Model\ManifestClose;
use Nfq\DpdClient\Model\ParcelMachine;
use Nfq\DpdClient\Model\ParcelNumberList;
use Nfq\DpdClient\Request\CallCourierRequest;
use Nfq\DpdClient\Request\CancelShipmentRequest;
use Nfq\DpdClient\Request\CheckTrackingRequest;
use Nfq\DpdClient\Request\CloseManifestRequest;
use Nfq\DpdClient\Request\CreateParcelLabelRequest;
use Nfq\DpdClient\Request\CreateShipmentRequest;
use Nfq\DpdClient\Request\SearchParcelMachineRequest;
use Psr\Http\Message\StreamInterface;
use Webmozart\Assert\Assert;

class Client
{
    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var HttpClient
     */
    private $statusClient;

    public function __construct(HttpClient $httpClient, HttpClient $statusClient)
    {
        $this->httpClient = $httpClient;
        $this->statusClient = $statusClient;
    }

    /**
     * @param SearchParcelMachineRequest $request
     *
     * @return ParcelMachine[]
     *
     * @throws InvalidResponseException
     */
    public function getParcelMachines(SearchParcelMachineRequest $request): array
    {
        $response = $this->httpClient->post('/ws-mapper-rest/parcelShopSearch_', $request->toArray());
        Assert::isArray($response);

        if (false === isset($response['parcelshops'])) {
            $this->throwException($response);
        }

        return ParcelMachineFactory::fromResponse($response);
    }

    public function createShipment(CreateShipmentRequest $request): ParcelNumberList
    {
        $response = $this->httpClient->post('/ws-mapper-rest/createShipment_', $request->toArray());
        Assert::isArray($response);

        if (false === isset($response['pl_number'])) {
            $this->throwException($response);
        }

        return ParcelNumberListFactory::fromResponse($response);
    }

    public function createParcelLabel(CreateParcelLabelRequest $request): StreamInterface
    {
        return $this->httpClient->post('/ws-mapper-rest/parcelPrint_', $request->toArray());
    }

    public function cancelShipment(CancelShipmentRequest $request): CancelShipment
    {
        $response = $this->httpClient->post('/ws-mapper-rest/parcelDelete_', $request->toArray());
        Assert::isArray($response);

        return CancelShipmentFactory::fromResponse($response);
    }

    public function closeManifest(CloseManifestRequest $request): ManifestClose
    {
        $response = $this->httpClient->post('/ws-mapper-rest/parcelManifestPrint_', $request->toArray());

        if (is_array($response)) {
            //Env for test endpoint
            $closeResponse = ManifestCloseFactory::createFromTestEnvResponse($response, $request->getCloseDate());
        } else {
            $closeResponse = ManifestCloseFactory::create($response->getContents(), $request->getCloseDate());
        }

        return $closeResponse;
    }

    public function callCourier(CallCourierRequest $request): bool
    {
        $response = $this->httpClient->post('/ws-mapper-rest/pickupOrderSave_', $request->toArray());

        //DPD returns only string
        if ($response == '<p>DONE') {
            return true;
        }

        return false;
    }


    public function getTrackings(CheckTrackingRequest $request): array
    {
        $response = $this->statusClient->get('/external/tracking', $request->toArray());

        $trackingResponse = TrackingFactory::fromResponse($response);

        foreach($trackingResponse as $key => $value) {
            if (!$value->isValid()) {
                $this->throwException(ErrorFactory::toResponse($value->getError()));
            }
        }

        return $trackingResponse;
    }

    private function throwException(array $response)
    {
        throw new InvalidResponseException('Response is missing required information: ' . json_encode($response));
    }
}
