<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Nfq\DpdClient\HttpClient;

use GuzzleHttp\Psr7\Stream;
use Http\Mock\Client as MockClient;
use Nfq\DpdClient\Exception\BadRequestException;
use Nfq\DpdClient\Exception\InvalidResponseException;
use Nfq\DpdClient\HttpClient\Client;
use Nfq\DpdClient\HttpClient\ClientFactory;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Tests\Nfq\DpdClient\Mock\ResponseMock;

class ClientTest extends TestCase
{
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @var MockClient
     */
    private $mockClient;

    protected function setUp()
    {
        $this->mockClient = new MockClient();
        $this->httpClient = new Client(ClientFactory::create('testuser1', 'testpassword1', true, $this->mockClient));
    }

    public function testClientSendsAuthenticationParameters()
    {
        $response = ResponseMock::fromArray(['parcelshops' => []]);
        $this->mockClient->addResponse($response);

        $this->httpClient->post('/something');

        $this->assertEquals(
            'username=testuser1&password=testpassword1',
            $this->mockClient->getLastRequest()->getUri()->getQuery()
        );
    }

    public function testClientSendsContentType()
    {
        $response = ResponseMock::fromArray(['parcelshops' => []]);
        $this->mockClient->addResponse($response);

        $this->httpClient->post('/something');

        $this->assertEquals(
            'application/x-www-form-urlencoded',
            $this->mockClient->getLastRequest()->getHeader('Content-Type')[0]
        );
    }

    public function testClientSendsRequestToTestUrlWithTestModeOn()
    {
        $this->httpClient = new Client(ClientFactory::create('testuser1', 'testpassword1', true, $this->mockClient));

        $response = ResponseMock::fromArray(['parcelshops' => []]);
        $this->mockClient->addResponse($response);

        $this->httpClient->post('/something');

        $uri = $this->mockClient->getLastRequest()->getUri();

        $this->assertEquals('lt.integration.dpd.eo.pl', $uri->getHost());
        $this->assertEquals('https', $uri->getScheme());
    }

    public function testClientSendsRequestToProductionUrlWithTestModeOff()
    {
        $this->httpClient = new Client(ClientFactory::create('testuser1', 'testpassword1', false, $this->mockClient));

        $response = ResponseMock::fromArray(['parcelshops' => []]);
        $this->mockClient->addResponse($response);

        $this->httpClient->post('/something');

        $uri = $this->mockClient->getLastRequest()->getUri();

        $this->assertEquals('integracijos.dpd.lt', $uri->getHost());
        $this->assertEquals('https', $uri->getScheme());
    }

    public function testClientThrowsExceptionOnEmptyResponse()
    {
        $response = ResponseMock::fromString('');
        $response->method('getBody')->willReturn('');

        $this->mockClient->addResponse($response);

        $this->expectException(InvalidResponseException::class);
        $this->httpClient->post('/something');
    }

    public function testClientThrowsExceptionOnInvalidRequest()
    {
        $response = ResponseMock::fromFile('error-response.json');

        $this->mockClient->addResponse($response);

        $this->expectException(BadRequestException::class);
        $this->httpClient->post('/something');
    }

    public function testClientThrowsExceptionOnInvalidJson()
    {
        $response = ResponseMock::fromString('invalid_json');

        $this->mockClient->addResponse($response);

        $this->expectException(InvalidResponseException::class);
        $this->httpClient->post('/something');
    }

    public function testClientConvertsJsonResponseToArray()
    {
        $response = ResponseMock::fromArray(['key' => 'value']);
        $this->mockClient->addResponse($response);

        $result = $this->httpClient->post('/something');

        $this->assertEquals(['key' => 'value'], $result);
    }

    public function testClientReturnsNonJsonResponsesAsStreams()
    {
        $response = $this->createMock(ResponseInterface::class);

        $stream = fopen('php://memory', 'r+');
        fwrite($stream, 'pdf_content');

        $response
            ->method('getBody')
            ->willReturn(new Stream($stream));

        $this->mockClient->addResponse($response);

        $result = $this->httpClient->post('/something');

        $this->assertEquals('pdf_content', (string) $result);
    }
}
