# DPD API Client

## Installation

We use HTTPPlug as the HTTP client abstraction layer. In this example, we will use Guzzle v6 as the HTTP client implementation.

```bash
$ composer require nfq/dpd-client php-http/guzzle6-adapter
```

## Usage

### Initialize client

```php
$httpClient = new \Http\Adapter\Guzzle6\Client(
    new \GuzzleHttp\Client(['curl' => [CURLOPT_SSL_VERIFYPEER => false]])
);

$client = new DpdClient(
    new HttpClient(ClientFactory::create('testuser1', 'testpassword1', true, $httpClient)),
    new HttpClient(StatusFactory::create($httpClient))
);
```

### Get parcel shop list

```php
$request = new SearchParcelMachineRequest('LT');
/** @var ParcelMachine[] $parcelMachines */
$parcelMachines = $this->client->getParcelMachines($request);
```

### Create parcel shop shipment

```php
$createShipmentRequest = new CreateShipmentRequest();

$createShipmentRequest
    ->setName('Jonas')
    ->setStreet('Liepkalnio g. 180-1')
    ->setCity('Vilnius')
    ->setCountry('LT')
    ->setPostCode('02121')
    ->setPhone('+37065111111')
    ->setWeight(1.5)
    ->setNumberOfParcels(1)
    ->setParcelType(ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP)
    ->setParcelShopId('LT10008');

/** @var ParcelNumberList */
$parcelNumberList = $this->client->createShipment($createShipmentRequest);
```

### Create regular shipment

```php
$createShipmentRequest = new CreateShipmentRequest();

$createShipmentRequest
    ->setName('Jonas')
    ->setStreet('Liepkalnio g. 180-1')
    ->setCity('Vilnius')
    ->setCountry('LT')
    ->setPostCode('02121')
    ->setPhone('+37065111111')
    ->setWeight(1.5)
    ->setNumberOfParcels(1)
    ->setParcelType(ServiceCodes::STANDARD_PARCEL_B2C_COD)
    ->setCodAmount(15.99);

/** @var ParcelNumberList */
$parcelNumberList = $this->client->createShipment($createShipmentRequest);
```

### Get parcel label

By default parcel labels will be returned in PDF format.

```php
$createShipmentRequest = new CreateParcelLabelRequest();
$createShipmentRequest->setParcelNumbers(['05809023121122']);

file_put_contents('output.pdf', (string) $client->createParcelLabel($createShipmentRequest))
```


### Cancel Shipment

Cancels the shipment.
Current PDP Logic: if even one parcel is canceled, it cancels the whole shipment. 
A good practice would be still to pass all parcel numbers from the shipment

```php
$cancelShipmentRequest = new CancelShipmentRequest();
$cancelShipmentRequest->setParcelNumbers(['05809023121122']);

$cancelResponse = $client->cancelShipment($cancelShipmentRequest); // CancelShipment

$cancelResponse->isCancelSuccessfull();
```

### Close the manifest

Closing the manifest returns the status text and the content of the manifest (format: pdf)

```php
$closeManifestRequest = new CloseManifestRequest();
$closeManifestRequest->setCloseDate(new \DateTime());

$response = $client->closeManifest($closeManifestRequest); // ManifestClose

file_put_contents('manifest.pdf', (string) $response->getPdf())
```

### Get trackings

```php
$checkTrackingRequest = new CheckTrackingRequest('13409600123862');

/** @var Tracking[] $trackings */
$trackings = $this->client->getTrackings($checkTrackingRequest);
```

## Running tests

```bash
$ bin/phpunit
```
