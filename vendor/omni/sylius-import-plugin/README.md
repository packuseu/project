# OmniSyliusImportPlugin

This plugin is an abstraction for `friendsofsylius/sylius-import-export-plugin`. 
It comes along with additional importers for `.csv` files that can handle multi
locales and variants, performance fixes and a UI for import files uploading along
with the entire system of processing them.  

## Installation

Run

```bash
composer require omni/sylius-import-plugin
```

Add the following to your bundles file

```php
<?php

// config/bundles.php

return [
    // ...
    FriendsOfSylius\SyliusImportExportPlugin\FOSSyliusImportExportPlugin::class => ['all' => true],
    Omni\Sylius\ImportPlugin\OmniSyliusImportPlugin::class => ['all' => true],
];
```

Add base configs in the packeges file:

```
parameters:
    omni_sylius.model.import_job.class: Omni\Sylius\ImportPlugin\Model\ImportJob

imports:
    - { resource: '@OmniSyliusImportPlugin/Resources/config/app/config.yaml' }

omni_sylius_import:
    file_upload_dir: '%kernel.project_dir%/var/import/'
    # optional. If you configure any custom importers 
    # you should add their types here so you could configure 
    # imports over the admin panel.
    additional_importer_types:
        business_unit_stock: 
            name: omni_sylius.importer.type.business_unit_stock
    single_file_processor_map:
        variant_code: code
```

Lastly, update your DB schema with wither `doctrine:schema:update --force`
or doctrine migrations.

## Usage

There are 2 ways to use the plugin: 

### Running importers

The first one is runing importers manually from the command line.
You can read up on it in the docs of the underlying plugin, but
essentially its this:

```bash
$ bin/console sylius:import {importer type} {path to file} --format=csv
```

The out of the box importer types are these:

* `taxon`
* `product_attribute`
* `product_attribute_option`
* `product_option`
* `product_option_value`
* `product`
* `product_image`
* `product_image_local`

> Documentation on .csv file structure is in progress.

### Setting up jobs

With this plugin there is a way to register jobs for the importers that
can then be processed with the console command:

```bash
$ bin/console omni:import:process
```

The jobs can be set up in the admin pannel, notice the new "import jobs"
button in your sidebar. There you will be able to set up the type of the 
importer and upload the file to be processed.

> Note that if you add any new importers they need to be configured to
>show up here. See configuration section.

Once you add the jobs for imports in the admin, you can configure cron or
supervisor to run the `omni:import:process` which will consume the jobs
configured in your newly created `omni_import_job` table. You will then 
be able to check the statuses and outputs of these jobs in the admin.

# Exporter
> Instead of instantly exporting data, this plugin creates export jobs.

## Yaml configuration needed for this to work

services.yaml:
```yaml
omni_sylius_import:
    file_export_upload_dir: '%kernel.project_dir%/var/export/' #Path where export files are stored after running a job

parameters:
  omni_sylius.model.export_job.class: Omni\Sylius\ImportPlugin\Model\ExportJob #Export job model
```

routes.yaml:
```yaml
app_export_data_product:
    path: /export/product
    methods: [GET]
    defaults:
        exporter: omni.sylius.product #Point to omni exporter or configure your own custom exporter
        _controller: omni_sylius.controller.export_job:createAction #Point to custom controller
        _sylius:
            redirect: omni_sylius_admin_export_job_index #Redirect to export page
            filterable: true
            grid: sylius_admin_product
```
## Running export jobs

`bin/console omni:export:process` - creates csv file from export job

### Usage

1) If your project web_ui (fos_sylius_import_export: exporter: web_ui) is enabled for exporters, 
go to /admin/products, apply filter, and press export - this will create export job. Otherwise go to /export/product,
this will create export job without filters (all products).
2) Run the process command, it will create csv file.
3) Go to /admin/export-jobs, open your export job (press Details), there should be a download link to the csv file.
