<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types = 1);

namespace Omni\Sylius\ImportPlugin\DependencyInjection;

use Omni\Sylius\ImportPlugin\Controller\ExportJobController;
use Omni\Sylius\ImportPlugin\Controller\ImportJobController;
use Omni\Sylius\ImportPlugin\Form\Type\ExportJobType;
use Omni\Sylius\ImportPlugin\Form\Type\ImportJobType;
use Omni\Sylius\ImportPlugin\Model\ExportJob;
use Omni\Sylius\ImportPlugin\Model\ExportJobInterface;
use Omni\Sylius\ImportPlugin\Model\ImportJob;
use Omni\Sylius\ImportPlugin\Model\ImportJobInterface;
use Sylius\Bundle\ResourceBundle\SyliusResourceBundle;
use Sylius\Component\Resource\Factory\Factory;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('omni_sylius_import');

        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('driver')->defaultValue(SyliusResourceBundle::DRIVER_DOCTRINE_ORM)->end()
                ->scalarNode('file_upload_dir')
                    ->info('The directory where import files will be uploaded from admin')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('file_export_upload_dir')
                    ->info('The directory where export files will be uploaded')
                    ->defaultNull()
                ->end()
                ->arrayNode('additional_importer_types')
                    ->useAttributeAsKey('code')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('code')->end()
                            ->scalarNode('name')->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('single_file_processor_map')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('variant_code')->defaultValue('code')->end()
                    ->end()
                ->end()
                ->booleanNode('single_file_processor_disable_variants')->defaultFalse()->end()
            ->end()
        ;

        $this->addResourcesSection($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addResourcesSection(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('resources')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('import_job')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->variableNode('options')->end()
                                ->arrayNode('classes')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('model')->defaultValue(ImportJob::class)->cannotBeEmpty()->end()
                                        ->scalarNode('interface')->defaultValue(ImportJobInterface::class)->cannotBeEmpty()->end()
                                        ->scalarNode('controller')->defaultValue(ImportJobController::class)->end()
                                        ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                        ->scalarNode('form')->defaultValue(ImportJobType::class)->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('export_job')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->variableNode('options')->end()
                                ->arrayNode('classes')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('model')->defaultValue(ExportJob::class)->cannotBeEmpty()->end()
                                        ->scalarNode('interface')->defaultValue(ExportJobInterface::class)->cannotBeEmpty()->end()
                                        ->scalarNode('controller')->defaultValue(ExportJobController::class)->end()
                                        ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                        ->scalarNode('form')->defaultValue(ExportJobType::class)->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
