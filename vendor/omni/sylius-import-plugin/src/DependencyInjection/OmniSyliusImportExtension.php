<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\DependencyInjection;

use Sylius\Bundle\ResourceBundle\DependencyInjection\Extension\AbstractResourceExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class OmniSyliusImportExtension extends AbstractResourceExtension
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');

        $container->setParameter('omni_import.additional_importer_types', $config['additional_importer_types']);
        $container->setParameter('omni_import.file_upload_dir', $config['file_upload_dir']);
        $container->setParameter('omni_import.file_export_upload_dir', $config['file_export_upload_dir']);
        $container->setParameter('omni_import.single_file_processor_map', $config['single_file_processor_map']);
        $container->setParameter('omni_import.single_file_processor_disable_variants', $config['single_file_processor_disable_variants']);

        $this->registerResources('omni_sylius', $config['driver'], $config['resources'], $container);
    }
}
