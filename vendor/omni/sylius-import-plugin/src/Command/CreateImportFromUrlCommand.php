<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace Omni\Sylius\ImportPlugin\Command;

use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImporterRegistry;
use Omni\Sylius\ImportPlugin\Model\ImportJob;
use Omni\Sylius\ImportPlugin\Model\ImportJobInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CreateImportFromUrlCommand extends Command
{
    /** @var string */
    protected static $defaultName = 'omni:import:create';

    /** @var string */
    protected $uploadDir;

    /*** @var RepositoryInterface */
    protected $importJobRepository;

    /** @var ImporterRegistry */
    private $importerRegistry;

    public function __construct(
        string $uploadDir,
        RepositoryInterface $importJobRepository,
        ImporterRegistry $importerRegistry
    ) {
        parent::__construct(self::$defaultName);

        $this->uploadDir = $uploadDir;
        $this->importJobRepository = $importJobRepository;
        $this->importerRegistry = $importerRegistry;
    }

    protected function configure(): void
    {
        $this->setDescription('Fetch import file, create import job.');
        $this->setDefinition(
            [
                new InputArgument('importer', InputArgument::REQUIRED, 'The importer to use.'),
                new InputArgument('url', InputArgument::REQUIRED, 'Path to file.'),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        $importer = $input->getArgument('importer');
        $url = $input->getArgument('url');
        $fileName = $this->uploadDir.uniqid().'.csv';

        if (!$this->validateImporter($input, $output)) {
            return;
        }

        try {
            file_put_contents($fileName, file_get_contents($url));

            $job = new ImportJob();
            $job->setImporter($importer);
            $job->setFile($fileName);
            $job->setStatus(ImportJobInterface::STATE_NEW);
            $this->importJobRepository->add($job);
            $io->success('Job created');
        } catch (\Exception $e) {
            $io->error('Job failed: '.$e->getMessage());
        }
    }

    private function validateImporter(InputInterface $input, OutputInterface $output): bool
    {
        $valid = true;
        $importer = $input->getArgument('importer');
        $serviceName = ImporterRegistry::buildServiceName($importer, 'csv');
        if (!$this->importerRegistry->has($serviceName)) {
            $output->writeln('<info>Available importers:</info>');
            $all = array_keys($this->importerRegistry->all());
            $importers = [];
            foreach ($all as $importer) {
                $importer = explode('.', $importer);
                $format = \array_pop($importer);
                $type = \implode('.', $importer);

                $importers[$type][] = $format;
            }

            $list = [];
            foreach ($importers as $importer => $formats) {
                $list[] = sprintf(
                    '%s (formats: %s)',
                    $importer,
                    implode(', ', $formats)
                );
            }

            $io = new SymfonyStyle($input, $output);
            $io->listing($list);
            $valid = false;
        }

        return $valid;
    }
}
