<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Command;

use Doctrine\Common\Persistence\ObjectManager;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\ExporterRegistry;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\ResourceExporterInterface;
use Omni\Sylius\ImportPlugin\Model\ExportJobInterface;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfigurationFactoryInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourcesCollectionProviderInterface;
use Sylius\Bundle\ResourceBundle\Grid\View\ResourceGridView;
use Sylius\Component\Registry\ServiceRegistryInterface;
use Sylius\Component\Resource\Metadata\Metadata;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class ProcessExportsCommand extends Command
{
    protected const OMNI_PRODUCT_EXPORTER = 'omni.sylius.product';
    protected const SYLIUS_PRODUCT_RESOURCE = 'sylius.product';

    use LockableTrait;

    /** @var string */
    protected static $defaultName = 'omni:export:process';

    /** @var ObjectManager */
    protected $objectManager;

    /** @var RepositoryInterface */
    protected $exportJobRepository;

    /** @var ServiceRegistryInterface */
    private $registry;

    /** @var ContainerInterface */
    private $container;

    /** @var RequestConfigurationFactoryInterface */
    private $requestConfigurationFactory;

    /** @var ResourcesCollectionProviderInterface */
    private $resourcesCollectionProvider;

    /** @var array */
    private $resources;

    /** @var string */
    private $exportDir;

    /** @var int */
    private $batchSize;

    /** @var int */
    protected $batchCount = 0;

    public function __construct(
        ObjectManager $objectManager,
        RepositoryInterface $exportJobRepository,
        ServiceRegistryInterface $registry,
        ContainerInterface $container,
        RequestConfigurationFactoryInterface $requestConfigurationFactory,
        ResourcesCollectionProviderInterface $resourcesCollectionProvider,
        array $resources,
        string $exportDir,
        int $batchSize = 100
    ) {
        parent::__construct(self::$defaultName);

        $this->objectManager = $objectManager;
        $this->exportJobRepository = $exportJobRepository;
        $this->registry = $registry;
        $this->container = $container;
        $this->requestConfigurationFactory = $requestConfigurationFactory;
        $this->resourcesCollectionProvider = $resourcesCollectionProvider;
        $this->resources = $resources;
        $this->exportDir = $exportDir;
        $this->batchSize = $batchSize;
    }

    protected function configure(): void
    {
        $this->setDescription('Process the exports.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $started = (new \DateTime())->getTimestamp();
        $io = new SymfonyStyle($input, $output);

        if (!$this->lock()) {
            $io->writeln('Command is already running');

            return 0;
        }

        /** @var ExportJobInterface $job */
        $job = $this->exportJobRepository->findOneBy(['status' => 'new'], ['createdAt' => 'asc']);

        if (!empty($job)) {
            $format = 'csv';
            $exporter = $job->getExporter();

            if (!$this->isThereStillTime($started)) {
                $io->writeln('Done');

                return 0;
            }

            $io->writeln('Processing export with processor: ' . $exporter);

            $name = ExporterRegistry::buildServiceName($exporter, $format);
            if (!$this->registry->has($name)) {
                throw new \Exception(sprintf("No exporter found of type '%s' for format '%s'", $exporter, $format));
            }

            try {
                $this->startJob($job);

                if (!$job->getExportFile()) {
                    $fileName = $this->exportDir . $exporter . '_' . uniqid() . '.csv';
                    if (!file_exists($this->exportDir)) {
                        mkdir($this->exportDir);
                    }
                    file_put_contents($fileName, NULL, FILE_USE_INCLUDE_PATH);
                    $job->setExportFile($fileName);
                }

                /** @var ResourceExporterInterface $service */
                $service = $this->registry->get($name);
                $service->setExportFile($job->getExportFile());

                $resourceGrid = $this->getResourceGrid($job);
                while($resources = $this->getResources($resourceGrid, $this->batchSize, $this->batchCount)) {
                    $this->batchCount += count($resources);

                    $resourcesIds = $this->getResourceIds($resources, $job);

                    $service->export($resourcesIds);
                    $this->objectManager->flush();
                    $this->objectManager->clear();
                }

                $service->finish();

                $job->setComments(sprintf(
                    "Exported %d item(s) to '%s' via the %s exporter",
                    $this->batchCount,
                    $job->getExportFile(),
                    $exporter
                ));
                $job->setProcessedAt(new \DateTime());
                $job->setStatus(ExportJobInterface::STATE_DONE);
                $io->success('Job exported successfully');
            } catch (\Throwable $e) {
                $job->setStatus(ExportJobInterface::STATE_ERROR);
                $job->setProcessedAt(new \DateTime());
                $job->setComments('Error: ' . $e->getMessage());
                $io->error('Job failed');
            }

            /**
             * Process each job individually instead of running a foreach, reason for this - clear() is being invoked,
             * which detaches all entities managed by the EntityManager and we
             * would need to reinstate each entity individually again.
             */

            $tmp = $job;
            /** @var ExportJobInterface $job */
            $job = $this->exportJobRepository->findOneBy(['id' => $tmp->getId()]);
            $job->setStatus($tmp->getStatus());
            $job->setComments($tmp->getComments());
            $job->setProcessedAt($tmp->getProcessedAt());
            $job->setProcessingStartedAt($tmp->getProcessingStartedAt());
            $this->exportJobRepository->add($job);
        }

        $io->writeln('Finished running export jobs');

        return 0;
    }

    protected function isThereStillTime(int $started): bool
    {
        $now = (new \DateTime())->getTimestamp();

        // We don't want to keep the processes running too long for multiple reasons.
        // Better approach is to schedule the crons more freaquently
        return $started + 900 > $now;
    }

    protected function startJob(ExportJobInterface $job): void
    {
        $job->setProcessingStartedAt(new \DateTime());
        $job->setStatus(ExportJobInterface::STATE_PROCESSING);

        // In case the export is large we let people know that the process is started.
        $this->exportJobRepository->add($job);
    }

    /**
     * @param ExportJobInterface $job
     *
     * @return Request
     */
    private function getRequest(ExportJobInterface $job): Request
    {
        $query = [];
        if (!empty($job->getQueryString())) {
            parse_str($job->getQueryString(), $query);
        }
        $attributes = ['_sylius' => ['filterable' => true, 'grid' => $job->getGrid()]];

        return new Request($query, [], $attributes);
    }

    /**
     * @param ExportJobInterface $job
     *
     * @return ResourceGridView
     */
    private function getResourceGrid(ExportJobInterface $job): ResourceGridView
    {
        $alias = $job->getExporter();
        $request = $this->getRequest($job);

        if ($alias === self::OMNI_PRODUCT_EXPORTER) {
            $alias = self::SYLIUS_PRODUCT_RESOURCE;
        }

        $metadata = Metadata::fromAliasAndConfiguration($alias, $this->resources[$alias]);
        $configuration = $this->requestConfigurationFactory->create($metadata, $request);

        /** @var RepositoryInterface $repository */
        $repository = $this->container->get($metadata->getServiceId('repository'));

        return $this->resourcesCollectionProvider->get($configuration, $repository);
    }

    /**
     * @param ResourceGridView $resources
     * @param int $maxResult
     * @param int $firstResult
     *
     * @return array
     */
    private function getResources(
        ResourceGridView $resources,
        int $maxResult,
        int $firstResult
    ): array
    {
        $query = $resources->getData()->getAdapter()->getQuery()->setMaxResults($maxResult)->setFirstResult($firstResult);

        return $query->getResult();
    }

    /**
     * @param array $resources
     * @param ExportJobInterface $job
     *
     * @return int[]
     */
    private function getResourceIds(
        array $resources,
        ExportJobInterface $job
    ): array
    {
        $resourcesIds = [];
        foreach ($resources as $resource) {
            if ($job->getExporter() === self::OMNI_PRODUCT_EXPORTER) {
                foreach ($resource->getVariants() as $variant) {
                    $resourcesIds[] = $variant->getId();
                }
            } else {
                $resourcesIds[] = $resource->getId();
            }
        }

        return $resourcesIds;
    }
}
