<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Command;

use Omni\Sylius\ImportPlugin\Model\ImportJobInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class ProcessImportsCommand extends Command
{
    use LockableTrait;

    /** @var string */
    protected static $defaultName = 'omni:import:process';

    /**
     * @var RepositoryInterface
     */
    protected $importJobRepository;

    public function __construct(RepositoryInterface $importJobRepository)
    {
        parent::__construct(self::$defaultName);

        $this->importJobRepository = $importJobRepository;
    }

    protected function configure(): void
    {
        $this->setDescription('Process the imports from the queue.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $started = (new \DateTime())->getTimestamp();
        $command = $this->getApplication()->find('sylius:import');
        $io = new SymfonyStyle($input, $output);

        if (!$this->lock()) {
            $io->writeln('Command is already running');

            return 0;
        }

        /** @var ImportJobInterface $job */
        $job = $this->importJobRepository->findOneBy(['status' => 'new'], ['createdAt' => 'asc']);

        if (!empty($job)) {
            if (!$this->isThereStillTime($started)) {
                $io->writeln('Done');

                return 0;
            }

            $io->writeln('Processing import with processor: ' . $job->getImporter());
            $buffer = new BufferedOutput();

            try {
                $this->startJob($job);

                $return = $command->run(new ArrayInput($this->getCommandArguments($job)), $buffer);

                $job->setComments($buffer->fetch());
                $job->setProcessedAt(new \DateTime());

                if ($return === 0) {
                    $job->setStatus(ImportJobInterface::STATE_DONE);
                    $io->success('Job processed successfully');
                    $filesystem = new FileSystem();
                    $filesystem->remove($job->getFile());
                } else {
                    $job->setStatus(ImportJobInterface::STATE_ERROR);
                    $io->error('Job failed');
                }
            } catch (\Throwable $e) {
                $job->setStatus(ImportJobInterface::STATE_ERROR);
                $job->setProcessedAt(new \DateTime());
                $job->setComments('Error: ' . $e->getMessage() . "\n Console output: " . $buffer->fetch());
                $io->error('Job failed');
            }

            /**
             * Process each job individually instead of running a foreach, reason for this - inside import function
             * doctrine clear() is being invoked, which detaches all entities managed by the EntityManager and we
             * would need to reinstate each entity individually again.
             */

            $tmp = $job;
            /** @var ImportJobInterface $job */
            $job = $this->importJobRepository->findOneBy(['id' => $tmp->getId()]);
            $job->setStatus($tmp->getStatus());
            $job->setComments($tmp->getComments());
            $job->setProcessedAt($tmp->getProcessedAt());
            $job->setProcessingStartedAt($tmp->getProcessingStartedAt());
            $this->importJobRepository->add($job);
        }

        $io->writeln('Finished running import jobs');

        return 0;
    }

    protected function isThereStillTime(int $started): bool
    {
        $now = (new \DateTime())->getTimestamp();

        // We don't want to keep the processes running too long for multiple reasons.
        // Better approach is to schedule the crons more freaquently
        return $started + 900 > $now;
    }

    protected function startJob(ImportJobInterface $job): void
    {
        $job->setProcessingStartedAt(new \DateTime());
        $job->setStatus(ImportJobInterface::STATE_PROCESSING);

        // In case the import is large we let people know that the process is started.
        $this->importJobRepository->add($job);
    }

    protected function getCommandArguments(ImportJobInterface $job): array
    {
        return [
            'importer' => $job->getImporter(),
            'file' => $job->getFile(),
            '--format' => 'csv',
        ];
    }
}
