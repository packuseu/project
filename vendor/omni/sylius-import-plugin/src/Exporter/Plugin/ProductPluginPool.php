<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace Omni\Sylius\ImportPlugin\Exporter\Plugin;

use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Plugin\PluginPool;
use Omni\Sylius\ImportPlugin\Exporter\Provider\ProductExportKeysProvider;

class ProductPluginPool extends PluginPool
{
    /** @var ProductExportKeysProvider */
    private $productExportKeysProvider;

    /**
     * @param array $plugins
     * @param ProductExportKeysProvider $productExportKeysProvider
     */
    public function __construct(
        array $plugins,
        ProductExportKeysProvider $productExportKeysProvider
    ) {
        parent::__construct($plugins, []);

        $this->productExportKeysProvider = $productExportKeysProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function initPlugins(array $ids): void
    {
        $this->exportKeys = array_merge($this->exportKeys, $this->productExportKeysProvider->getKeys());
        $this->exportKeysAvailable = $this->exportKeys;

        parent::initPlugins($ids);
    }
}
