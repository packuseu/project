<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Exporter\Plugin;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Plugin\ResourcePlugin;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Product\Model\ProductTranslationInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class ProductResourcePlugin extends ResourcePlugin
{
    /** @var RepositoryInterface */
    public $channelRepository;

    /**
     * @param RepositoryInterface $productVariantRepository
     * @param PropertyAccessorInterface $propertyAccessor
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RepositoryInterface $productVariantRepository,
        PropertyAccessorInterface $propertyAccessor,
        EntityManagerInterface $entityManager,
        RepositoryInterface $channelRepository
    ) {
        parent::__construct($productVariantRepository, $propertyAccessor, $entityManager);
        $this->channelRepository = $channelRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function init(array $idsToExport): void
    {
        parent::init($idsToExport);

        /** @var ProductVariantInterface $resource */
        foreach ($this->resources as $resource) {
            $this->addCodeData($resource);
            $this->addParentData($resource);
            $this->addDescriptionData($resource);
            $this->addMetaData($resource);
            $this->addSlugData($resource);
            $this->addOptionData($resource);
            $this->addPriceData($resource);
            $this->addNameData($resource);
            $this->addChannelData($resource);
            $this->getAttributeData($resource);
            $this->addTaxonData($resource);
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addCodeData(ProductVariantInterface $resource): void
    {
        $this->addDataForResource($resource, 'code', $resource->getCode());
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addParentData(ProductVariantInterface $resource): void
    {
        $this->addDataForResource($resource, 'parent', $resource->getProduct()->getCode());
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addDescriptionData(ProductVariantInterface $resource): void
    {
        /** @var ProductTranslationInterface $translation */
        foreach ($resource->getProduct()->getTranslations() as $translation) {
            $this->addDataForResource($resource, 'description-' . $translation->getLocale(), $translation->getDescription());
            $this->addDataForResource($resource, 'short_description-' . $translation->getLocale(), $translation->getShortDescription());
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addSlugData(ProductVariantInterface $resource): void
    {
        /** @var ProductTranslationInterface $translation */
        foreach ($resource->getProduct()->getTranslations() as $translation) {
            $this->addDataForResource($resource, 'slug-' . $translation->getLocale(), $translation->getSlug());
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addOptionData(ProductVariantInterface $resource): void
    {
        foreach ($resource->getOptionValues() as $optionValue) {
            $this->addDataForResource($resource, 'option-'.strtolower($optionValue->getOptionCode()), $optionValue->getCode());
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addPriceData(ProductVariantInterface $resource): void
    {
        $channels = $this->channelRepository->findAll();
        $currencies = [];
        /** @var ChannelInterface $channel */
        foreach ($channels as $channel) {
            $currencies[$channel->getCode()] = $channel->getBaseCurrency()->getCode();
        }

        /** @var ChannelPricingInterface $channelPricing */
        foreach ($resource->getChannelPricings() as $channelPricing) {
            $price = number_format(($channelPricing->getPrice() / 100), 2);
            $origPrice = $channelPricing->getOriginalPrice();
            if ($origPrice) {
                $origPrice = number_format(($channelPricing->getOriginalPrice() / 100), 2);
            }

            $this->addDataForResource($resource, 'price-'.$currencies[$channelPricing->getChannelCode()], $price);
            $this->addDataForResource($resource, 'orig_price-'.$currencies[$channelPricing->getChannelCode()], $origPrice);
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addNameData(ProductVariantInterface $resource): void
    {
        /** @var ProductTranslationInterface $translation */
        foreach ($resource->getProduct()->getTranslations() as $translation) {
            $this->addDataForResource($resource, 'name-' . $translation->getLocale(), $translation->getName());
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addChannelData(ProductVariantInterface $resource): void
    {
        /** @var ChannelInterface $channel */
        foreach ($resource->getProduct()->getChannels() as $channel) {
            $this->addDataForResource($resource, 'available_in_channel-' . $channel->getCode(), $channel->isEnabled() ? 1 : '');
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function getAttributeData(ProductVariantInterface $resource): void
    {
        /** @var ProductAttributeInterface $attribute */
        foreach ($resource->getProduct()->getAttributes() as $attribute) {
            $this->addDataForResource($resource, $attribute->getCode(), $attribute->getValue());
        }
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addTaxonData(ProductVariantInterface $resource): void
    {
        $categories = [];
        /** @var TaxonInterface $taxon */
        foreach ($resource->getProduct()->getTaxons() as $taxon) {
            if (!$taxon->isRoot()) {
                $categories[] = $taxon->getCode();
            }
        }

        $this->addDataForResource($resource, 'categories', implode(',', $categories));
    }

    /**
     * @param ProductVariantInterface $resource
     */
    public function addMetaData(ProductVariantInterface $resource): void
    {
        /** @var ProductTranslationInterface $translation */
        foreach ($resource->getProduct()->getTranslations() as $translation) {
            $this->addDataForResource($resource, 'meta-description-' . $translation->getLocale(), $translation->getMetaDescription());
            $this->addDataForResource($resource, 'meta-keywords-' . $translation->getLocale(), $translation->getMetaKeywords());
        }
    }
}
