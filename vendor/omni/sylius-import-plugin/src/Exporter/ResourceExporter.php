<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\ImportPlugin\Exporter;

use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Plugin\PluginPoolInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\Transformer\TransformerPoolInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Writer\WriterInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\ResourceExporter as BaseResourceExporter;

class ResourceExporter extends BaseResourceExporter
{
    private $keysWritten = false;

    public function __construct(
        WriterInterface $writer,
        PluginPoolInterface $pluginPool,
        array $resourceKeys,
        ?TransformerPoolInterface $transformerPool
    ) {
        $this->writer = $writer;
        $this->pluginPool = $pluginPool;
        $this->transformerPool = $transformerPool;
        $this->resourceKeys = $resourceKeys;
    }

    /**
     * {@inheritdoc}
     */
    public function export(array $idsToExport): void
    {
        $this->pluginPool->initPlugins($idsToExport);

        foreach ($idsToExport as $id) {
            $this->writeDataForId((string) $id);
        }
    }

    private function writeDataForId(string $id): void
    {
        $dataForId = $this->getDataForId($id);

        if (!$this->keysWritten) {
            $this->writer->write(array_keys($dataForId));
            $this->keysWritten = true;
        }

        $this->writer->write($dataForId);
    }
}
