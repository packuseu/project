<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace Omni\Sylius\ImportPlugin\Exporter\Provider;

use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Omni\Sylius\ImportPlugin\Processor\AvailableChannelsTrait;
use Omni\Sylius\ImportPlugin\Processor\AvailableLocalesTrait;
use Sylius\Component\Channel\Model\ChannelInterface;
use Sylius\Component\Currency\Model\CurrencyInterface;
use Sylius\Component\Locale\Model\LocaleInterface;
use Sylius\Component\Product\Model\ProductAttribute;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class ProductExportKeysProvider
{
    use AvailableChannelsTrait;
    use AvailableLocalesTrait;

    /** @var ResourcesCache */
    private $cache;
    /** @var RepositoryInterface */
    private $productOptionRepository;
    /** @var RepositoryInterface */
    private $productAttributeRepository;
    /** @var RepositoryInterface */
    private $channelRepository;

    /**
     * @param ResourcesCache $cache
     * @param RepositoryInterface $productOptionRepository
     * @param RepositoryInterface $productAttributeRepository
     */
    public function __construct(
        ResourcesCache $cache,
        RepositoryInterface $productOptionRepository,
        RepositoryInterface $productAttributeRepository,
        RepositoryInterface $channelRepository
    ) {
        $this->cache = $cache;
        $this->productOptionRepository = $productOptionRepository;
        $this->productAttributeRepository = $productAttributeRepository;
        $this->channelRepository = $channelRepository;
    }

    /**
     * @return array
     */
    public function getKeys(): array
    {
        $keys = [];
        $keys[] = 'code';
        $keys[] = 'parent';

        $keys[] = 'categories';

        $keys = array_merge($keys, $this->getNameKeys());
        $keys = array_merge($keys, $this->getPriceKeys());
        $keys = array_merge($keys, $this->getDescriptionKeys());
        $keys = array_merge($keys, $this->getMetaKeys());
        $keys = array_merge($keys, $this->addSlugKeys());
        $keys = array_merge($keys, $this->getChannelKeys());
        $keys = array_merge($keys, $this->getOptionKeys());
        $keys = array_merge($keys, $this->getAttributeKeys());

        return $keys;
    }

    /**
     * @return array
     */
    public function getOptionKeys(): array
    {
        $keys = [];
        $options = $this->productOptionRepository->findBy([], ['id' => 'ASC']);
        /** @var ProductOptionInterface $option */
        foreach ($options as $option) {
            if (!empty($option->getCode())) {
                $keys[] = 'option-'.$option->getCode();
            }
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function getAttributeKeys(): array
    {
        $keys = [];
        $attributes = $this->productAttributeRepository->findBy([], ['id' => 'ASC']);
        /** @var ProductAttribute $attribute */
        foreach ($attributes as $attribute) {
            if (!empty($attribute->getCode())) {
                $keys[] = $attribute->getCode();
            }
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function addSlugKeys(): array
    {
        $keys = [];
        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $keys[] = 'slug-'.$locale;
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function getChannelKeys(): array
    {
        $keys = [];
        foreach ($this->getAvailableChannelCodes($this->cache) as $channel) {
            $keys[] = 'available_in_channel-'.$channel;
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function getNameKeys(): array
    {
        $keys = [];
        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $keys[] = 'name-'.$locale;
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function getPriceKeys(): array
    {
        $keys = [];
        $currencies = [];
        $channels = $this->channelRepository->findBy([], ['id' => 'ASC']);
        /** @var ChannelInterface $channel */
        foreach ($channels as $channel) {
            if (!in_array($channel->getBaseCurrency()->getCode(), $currencies)) {
                $currencies[] = $channel->getBaseCurrency()->getCode();
                $keys[] = 'price-'.$channel->getBaseCurrency()->getCode();
                $keys[] = 'orig_price-'.$channel->getBaseCurrency()->getCode();
            }
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function getDescriptionKeys(): array
    {
        $keys = [];
        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $keys[] = 'description-'.$locale;
            $keys[] = 'short_description-'.$locale;
        }

        return $keys;
    }

    /**
     * @return array
     */
    public function getMetaKeys(): array
    {
        $keys = [];
        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $keys[] = 'meta-description-'.$locale;
            $keys[] = 'meta-keywords-'.$locale;
        }

        return $keys;
    }
}
