<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Controller;

use FOS\RestBundle\View\View;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\ExporterRegistry;
use FriendsOfSylius\SyliusImportExportPlugin\Exporter\ResourceExporterInterface;
use Omni\Sylius\ImportPlugin\Model\ExportJobInterface;
use Omni\Sylius\ImportPlugin\Model\ImportJobInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\Metadata\Metadata;
use Sylius\Component\Resource\ResourceActions;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Filesystem\Filesystem;

class ExportJobController extends ResourceController
{
    public function createAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::CREATE);
        /** @var ExportJobInterface $newResource */
        $newResource = $this->newResourceFactory->create($configuration, $this->factory);

        $newResource->setStatus(ExportJobInterface::STATE_NEW);
        $newResource->setExporter($request->attributes->get('exporter'));
        $newResource->setGrid($request->attributes->get('_sylius')['grid']);
        $newResource->setQueryString($request->getQueryString());

        $form = $this->resourceFormFactory->create($configuration, $newResource);

        $this->repository->add($newResource);

        return $this->redirectHandler->redirectToResource($configuration, $newResource);
    }

    public function downloadAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::SHOW);
        /** @var ExportJobInterface $resource */
        $resource = $this->findOr404($configuration);

        $response = new BinaryFileResponse($resource->getExportFile());
        $explode = explode('_', $resource->getExportFile());
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            end($explode)
        );

        $response->headers->set('Content-Type', 'application/csv');
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    public function deleteAction(Request $request): Response
    {
        $configuration = $this->requestConfigurationFactory->create($this->metadata, $request);

        $this->isGrantedOr403($configuration, ResourceActions::DELETE);
        /** @var ExportJobInterface $resource */
        $resource = $this->findOr404($configuration);

        if ($resource->getExportFile()) {
            $filesystem = new FileSystem();
            $filesystem->remove($resource->getExportFile());
        }

        $this->manager->remove($resource);
        $this->manager->flush();

        return $this->redirectHandler->redirectToIndex($configuration, $resource);
    }
}
