<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Cache;

use Sylius\Component\Attribute\Model\AttributeInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Locale\Model\LocaleInterface;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

class ResourcesCache implements ResourcesCacheInterface
{
    const RESOURCE_CHANNEL = 'channel';
    const RESOURCE_LOCALE = 'locale';

    /**
     * @var RepositoryInterface
     */
    protected $channelRepository;

    /**
     * @var RepositoryInterface
     */
    protected $localeRepository;

    /**
     * @var RepositoryInterface
     */
    protected $attributeRepository;

    /**
     * @var RepositoryInterface
     */
    protected $taxonRepository;

    /**
     * @var RepositoryInterface
     */
    protected $optionRepository;

    /** @var RepositoryInterface */
    private $optionValueRepository;

    /**
     * @var ChannelInterface[]
     */
    protected $channels = [];

    /**
     * @var LocaleInterface[]
     */
    protected $locales = [];

    /**
     * @var AttributeInterface[]
     */
    protected $attributes = [];

    /**
     * @var TaxonInterface[]
     */
    protected $taxons = [];

    /**
     * @var ProductOptionInterface[]
     */
    protected $options = [];

    /**
     * @var ProductOptionValueInterface[]
     */
    protected $optionValues = [];

    /**
     * @var ProductInterface[]
     */
    protected $products = [];

    /**
     * @var ProductVariantInterface[]
     */
    protected $variants = [];

    public function __construct(
        RepositoryInterface $channelRepository,
        RepositoryInterface $localeRepository,
        RepositoryInterface $attributeRepository,
        RepositoryInterface $taxonRepository,
        RepositoryInterface $optionRepository,
        RepositoryInterface $optionValueRepository
    ) {
        $this->channelRepository = $channelRepository;
        $this->localeRepository = $localeRepository;
        $this->attributeRepository = $attributeRepository;
        $this->taxonRepository = $taxonRepository;
        $this->optionRepository = $optionRepository;
        $this->optionValueRepository = $optionValueRepository;
    }

    public function getChannel(string $code): ?ChannelInterface
    {
        /** @var ChannelInterface $channel */
        $channel = $this->getResource($code, self::RESOURCE_CHANNEL);

        return $channel;
    }

    public function getLocale(string $code): ?LocaleInterface
    {
        /** @var LocaleInterface $locale */
        $locale = $this->getResource($code, self::RESOURCE_LOCALE);

        return $locale;
    }

    public function cacheAttribute(AttributeInterface $attribute): void
    {
        $this->attributes[$attribute->getCode()] = $attribute;
    }

    public function getAttribute(string $code): ?AttributeInterface
    {
        if (!isset($this->attributes[$code])) {
            $this->attributes[$code] = $this->attributeRepository->findOneBy(['code' => $code]);
        }

        return $this->attributes[$code];
    }

    public function getTaxon(string $code): ?TaxonInterface
    {
        if (!isset($this->taxons[$code])) {
            $this->taxons[$code] = $this->taxonRepository->findOneBy(['code' => $code]);
        }

        return $this->taxons[$code];
    }

    public function storeTaxon(TaxonInterface $taxon): void
    {
        $this->taxons[$taxon->getCode()] = $taxon;
    }

    public function getOption(string $code): ?ProductOptionInterface
    {
        if (!isset($this->options[$code])) {
            $this->options[$code] = $this->optionRepository->findOneBy(['code' => $code]);
        }

        return $this->options[$code];
    }

    public function getOptionValue(string $code): ?ProductOptionValueInterface
    {
        if (!isset($this->optionValues[$code])) {
            $this->optionValues[$code] = $this->optionValueRepository->findOneBy(['code' => $code]);
        }

        return $this->optionValues[$code];
    }

    public function cacheProductOptionValue(ProductOptionValueInterface $optionValue): void
    {
        $this->optionValues[$optionValue->getCode()] = $optionValue;
    }

    public function getChannelCodes(): array
    {
        $codes = [];

        foreach ($this->getChannels() as $channel) {
            $codes[] = $channel->getCode();
        }

        return $codes;
    }

    /**
     * @return ChannelInterface[]
     */
    public function getChannels(): array
    {
        if (!$this->isResourceLoaded(self::RESOURCE_CHANNEL)) {
            $this->loadResource(self::RESOURCE_CHANNEL);
        }

        return array_values($this->channels);
    }

    /**
     * @return LocaleInterface[]
     */
    public function getLocales(): array
    {
        if (!$this->isResourceLoaded(self::RESOURCE_LOCALE)) {
            $this->loadResource(self::RESOURCE_LOCALE);
        }

        return array_values($this->locales);
    }

    public function cacheProduct(ProductInterface $product): void
    {
        $this->products[$product->getCode()] = $product;
    }

    public function getProduct(string $code): ?ProductInterface
    {
        return $this->products[$code] ?? null;
    }

    /**
     * @return ProductInterface[]
     */
    public function getCachedProducts(): array
    {
        return $this->products;
    }

    public function cacheVariant(ProductVariantInterface $variant): void
    {
        $this->variants[$variant->getCode()] = $variant;
    }

    public function getVariant(string $code): ?ProductVariantInterface
    {
        return $this->variants[$code] ?? null;
    }

    public function clear(): void
    {
        $this->channels = [];
        $this->locales = [];
        $this->attributes = [];
        $this->taxons = [];
        $this->options = [];
        $this->optionValues = [];
        $this->products = [];
        $this->variants = [];
    }

    protected function getResource(string $code, string $type): ?ResourceInterface
    {
        $var = $type . 's';

        if (!$this->isResourceLoaded($type)) {
            $this->loadResource($type);
        }

        if (!array_key_exists($code, $this->$var)) {
            $this->$var[$code] = null;
        }

        return $this->$var[$code];
    }

    protected function isResourceLoaded(string $type): bool
    {
        $var = $type . 's';

        return !empty($this->$var);
    }

    protected function loadResource(string $type): void
    {
        $var = $type . 's';
        $repo = $type . 'Repository';

        /** @var CodeAwareInterface $resource */
        foreach ($this->$repo->findAll() as $resource) {
            $this->$var[$resource->getCode()] = $resource;
        }
    }
}
