<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use App\Entity\Product\ProductTaxon;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Internal\Hydration\IterableResult;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductTaxonInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class ProductTaxonProcessor implements ResourceProcessorInterface
{
    protected const BATCH_SIZE = 20;

    /** @var ResourcesCache */
    protected $cache;

    /** @var EntityManagerInterface  */
    protected $em;

    /** @var FactoryInterface */
    protected $productTaxonFactory;

    /** @var int */
    protected $batchCount = 0;

    public function __construct(
        ResourcesCache $cache,
        EntityManagerInterface $em,
        FactoryInterface $productTaxonFactory
    ) {
        $this->cache = $cache;
        $this->em = $em;
        $this->productTaxonFactory = $productTaxonFactory;
    }

    /**
     * {@inheritdoc}
     *
     * Accepted format:
     *
     * searchable_taxon,taxons
     */
    public function process(array $data): void
    {
        /** @var TaxonInterface $searchableTaxon */
        $searchableTaxon = $this->cache->getTaxon($data['searchable_taxon']);

        if ($searchableTaxon === null) {
            return;
        }

        $iterableProductTaxons = $this->getIterableProductTaxons($searchableTaxon);

        $this->batchCount = 0;

        /** @var ProductTaxon $productTaxon */
        foreach ($iterableProductTaxons as $productTaxon) {
            $product = $productTaxon[0]->getProduct();
            $this->batchCount++;

            foreach ($this->getTaxonsToAdd($data['taxons']) as $taxon) {
                if ($product->hasTaxon($taxon)) {
                    continue;
                }

                $this->addTaxonToProduct(
                    $product,
                    $taxon
                );
            }

            if ($this->batchCount === self::BATCH_SIZE) {
                $this->batchCount = 0;
                $this->flushAndClear();
            }
        }

        $this->flushAndClear();
    }

    /**
     * @param ProductInterface $product
     * @param TaxonInterface $taxon
     */
    protected function addTaxonToProduct(ProductInterface $product, TaxonInterface $taxon): void
    {
        /** @var ProductTaxonInterface $productTaxon */
        $productTaxon = $this->productTaxonFactory->createNew();
        $productTaxon->setTaxon($taxon);
        $product->addProductTaxon($productTaxon);

        $this->em->persist($product);
    }

    /**
     * @param string $taxons
     * @return TaxonInterface[]
     */
    protected function getTaxonsToAdd(string $taxonsCodes): array
    {
        $taxonsToAdd = [];
        $taxonsCodes = explode(';', $taxonsCodes);

        foreach ($taxonsCodes as $taxonsCode) {
        /** @var TaxonInterface $taxonToAdd[] */

            $taxon = $this->cache->getTaxon($taxonsCode);

            if ($taxon === null) {
                continue;
            }

            $taxonsToAdd[] = $taxon;
        }

        return $taxonsToAdd;
    }

    /**
     * @param TaxonInterface $searchableTaxon
     * @return IterableResult
     */
    protected function getIterableProductTaxons(TaxonInterface $searchableTaxon): IterableResult
    {
        $query = $this->em
            ->createQuery('SELECT pt FROM ' . ProductTaxonInterface::class . ' pt WHERE pt.taxon = :taxon')
            ->setParameter('taxon', $searchableTaxon);

        return $query->iterate();
    }

    protected function flushAndClear(): void
    {
        $this->em->flush();
        $this->em->clear();
        $this->cache->clear();
    }
}
