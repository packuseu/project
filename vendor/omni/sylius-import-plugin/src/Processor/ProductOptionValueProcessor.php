<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ImporterException;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Product\Model\ProductOptionInterface;
use Sylius\Component\Product\Model\ProductOptionValueInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class ProductOptionValueProcessor implements ResourceProcessorInterface
{
    use AvailableLocalesTrait;

    /** @var FactoryInterface */
    private $productOptionValueFactory;

    /** @var ResourcesCache */
    private $cache;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(FactoryInterface $productOptionValueFactory, ResourcesCache $cache, EntityManagerInterface $em)
    {
        $this->productOptionValueFactory = $productOptionValueFactory;
        $this->cache = $cache;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     *
     * Required fields:
     *
     * code - option value code
     * option - option code
     *
     * Optional fields:
     *
     * label-lt_LT - labels for all wanted translations (lt_LT can be changed to any valid locale)
     *
     * @throws ImporterException
     * @throws ItemIncompleteException
     */
    public function process(array $data): void
    {
        $code = $data['code'] ?? '';
        $option = $this->cache->getOption($data['option'] ?? '');

        if (!$code || !$option) {
            throw new ItemIncompleteException('Missing data detected');
        }

        $productOptionValue = $this->getOptionValue($code, $option);

        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $label = 'label-' . $locale;

            if (!isset($data[$label])) {
                continue;
            }

            $productOptionValue->setCurrentLocale($locale);
            $productOptionValue->setFallbackLocale($locale);
            $productOptionValue->setValue($data[$label]);
        }

        $this->cache->cacheProductOptionValue($productOptionValue);
        $this->em->persist($productOptionValue);
    }

    protected function getOptionValue(string $code, ProductOptionInterface $option): ProductOptionValueInterface
    {
        $productOptionValue = $this->cache->getOptionValue($code);

        if (!$productOptionValue) {
            /** @var ProductOptionValueInterface $productOptionValue */
            $productOptionValue = $this->productOptionValueFactory->createNew();
            $productOptionValue->setCode($code);
            $productOptionValue->setOption($option);
        } elseif ($productOptionValue->getOptionCode() != $option->getCode()) {
            throw new ImporterException('Option value `' . $code .  '` cannot be reassigned to another option! ');
        }

        return $productOptionValue;
    }
}
