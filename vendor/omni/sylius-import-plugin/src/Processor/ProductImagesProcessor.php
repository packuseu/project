<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use App\Entity\Product\ProductVariant;
use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ImporterException;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\MetadataValidatorInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Core\Model\ProductImageInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Repository\ProductRepositoryInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Omni\Sylius\ImportPlugin\Model\ImagesAwareInterface;

class ProductImagesProcessor implements ResourceProcessorInterface
{
    /** @var ProductRepositoryInterface */
    protected $productRepository;
    /** @var FactoryInterface  */
    protected $productImageFactory;
    /** @var EntityManagerInterface  */
    protected $em;
    /** @var FactoryInterface */
    protected $imageUploader;
    /** @var MetadataValidatorInterface */
    protected $metadataValidator;
    /** @var array */
    protected $headerKeys;

    public function __construct(
        RepositoryInterface $productRepository,
        FactoryInterface $productImageFactory,
        EntityManagerInterface $em,
        ImageUploaderInterface $imageUploader,
        MetadataValidatorInterface $metadataValidator,
        array $headerKeys = ['code']
    ) {
        $this->productRepository = $productRepository;
        $this->productImageFactory = $productImageFactory;
        $this->em = $em;
        $this->imageUploader = $imageUploader;
        $this->metadataValidator = $metadataValidator;
        $this->headerKeys = $headerKeys;
    }

    /**
     * {@inheritdoc}
     */
    public function process(array $data): void
    {
        $this->metadataValidator->validateHeaders($this->headerKeys, $data);
        $product = $this->getProduct($data['code']);
        unset($data['code']);

        $this->uploadImages($product, $data);
    }

    protected function uploadImages(ProductInterface $product, array $data): void
    {
        /** @var ProductVariant|null $variant */
        $variant = null;
        $variantCode = $data['variant'] ?? null;

        if ($variantCode) {
            foreach ($product->getVariants() as $var) {
                if ($var->getCode() == $variantCode) {
                    $variant = $var;

                    break;
                }
            }

            if (!$variant) {
                throw new ItemIncompleteException('Invalid variant code provided');
            }
        }

        foreach ($data as $imageType => $imagePath) {
            if (empty($imagePath)) {
                continue;
            }

            if ($this->keepImage($product, $imageType, $imagePath)) {
                continue;
            }

            $this->removeCurrentImages($product, $imageType, $imagePath);

            try {
                $productImage = $this->getImage($imagePath);
            } catch (\Exception $e) {
                // TODO: log exception
                continue;
            }

            if (!$productImage) {
                continue;
            }

            $productImage->setType($imageType);

            if ($variant) {
                $variant->addImage($productImage);
            } else {
                $product->addImage($productImage);
            }
        }

        $this->em->persist($product);
    }

    protected function getImage(string $path): ?ProductImageInterface
    {
        if (preg_match('/^https?:\/\//', $path)) {
            $productImage = $this->uploadProductImageFromUrl($path);
        } else {
            $productImage = $this->uploadProductImageFromFtp($path);
        }

        if ($productImage instanceof ImagesAwareInterface) {
            $productImage->setOriginalPath($path);
        }

        return $productImage;
    }

    protected function removeCurrentImages(ProductInterface $product, string $type, string $path): void
    {
        $images = $type ? $product->getImagesByType($type) : $product->getImages();

        foreach ($images as $image) {
            if (!$image instanceof ImagesAwareInterface) {
                $product->removeImage($image);

                continue;
            }

            if ($image->getType() === $type && $image->getOriginalPath() !== $path) {
                $product->removeImage($image);
            }
        }
    }

    protected function keepImage(ProductInterface $product, string $type = null, string $path): bool
    {
        foreach ($product->getImages() as $image) {
            if ($image instanceof ImagesAwareInterface
                && $image->getType() === $type
                && $image->getOriginalPath() === $path
            ) {
                return true;
            }
        }

        return false;
    }

    protected function getProduct(string $code): ProductInterface
    {
        $product = $this->productRepository->findOneByCode($code);

        if (null === $product) {
            throw new ImporterException(sprintf('Product with code "%s" not found', $code));
        }

        return $product;
    }

    /**
     * @param string $url
     *
     * @return ProductImage|null
     */
    private function uploadProductImageFromUrl(string $url): ?ProductImageInterface
    {
        try {
            $tmpImg = tempnam(sys_get_temp_dir(), 'product_image');
            $handle = fopen($tmpImg, 'wb');
            fwrite($handle, file_get_contents($url));
            fclose($handle);

            $image = $this->productImageFactory->createNew();
            $image->setFile(new UploadedFile($tmpImg, 'product_image'));
            $this->imageUploader->upload($image);

            unlink($tmpImg);

            return $image;
        } catch (\Throwable $e) {
            // TODO: Log error?
            return null;
        }
    }

    /**
     * @param string $path
     *
     * @return ProductImage|null
     */
    private function uploadProductImageFromFtp(string $path): ?ProductImageInterface
    {
        try {
            $tmpImg = tempnam(sys_get_temp_dir(), 'product_image');
            $ftpConnection = ftp_connect(getenv('CDN_FTP_HOST'));
            ftp_login($ftpConnection, getenv('CDN_FTP_USERNAME'), getenv('CDN_FTP_PASSWORD'));
            ftp_set_option($ftpConnection, FTP_USEPASVADDRESS, false);
            ftp_pasv($ftpConnection, true);
            ftp_get($ftpConnection, $tmpImg, $path);
            ftp_close($ftpConnection);

            $image = $this->productImageFactory->createNew();
            $image->setFile(new File($tmpImg, false));
            $this->imageUploader->upload($image);

            unlink($tmpImg);

            return $image;
        } catch (\Throwable $e) {
            // TODO: Log error?
            return null;
        }
    }
}
