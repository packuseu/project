<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\MetadataValidatorInterface;
use Sylius\Component\Core\Model\ProductImageInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductImagesUploadProcessor extends ProductImagesProcessor
{
    /** @var string  */
    private $fileDir;

    public function __construct(
        RepositoryInterface $productRepository,
        FactoryInterface $productImageFactory,
        EntityManagerInterface $em,
        ImageUploaderInterface $imageUploader,
        MetadataValidatorInterface $metadataValidator,
        string $fileDir,
        array $headerKeys = ['code']
    ) {
        $this->productRepository = $productRepository;
        $this->productImageFactory = $productImageFactory;
        $this->em = $em;
        $this->imageUploader = $imageUploader;
        $this->metadataValidator = $metadataValidator;
        $this->fileDir = $fileDir;
        $this->headerKeys = $headerKeys;
    }

    protected function getImage(string $path): ?ProductImageInterface
    {
        /** @var ProductImageInterface $image */
        $image = $this->productImageFactory->createNew();
        $image->setFile(new UploadedFile($this->fileDir . "/$path", 'product_image'));
        $this->imageUploader->upload($image);

        return $image;
    }

}
