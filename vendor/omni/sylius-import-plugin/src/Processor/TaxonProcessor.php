<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Factory\TaxonFactoryInterface;
use Sylius\Component\Taxonomy\Generator\TaxonSlugGeneratorInterface;

class TaxonProcessor implements ResourceProcessorInterface
{
    /** @var RepositoryInterface|TaxonRepositoryInterface */
    protected $taxonRepository;
    /** @var FactoryInterface|TaxonFactoryInterface */
    protected $taxonFactory;
    /** @var TaxonSlugGeneratorInterface */
    protected $taxonSlugGenerator;
    /** @var ResourcesCache */
    protected $cache;
    /** @var EntityManagerInterface  */
    protected $em;

    /** @var string[] */
    protected $localeList = [];

    public function __construct(
        RepositoryInterface $taxonRepository,
        FactoryInterface $taxonFactory,
        TaxonSlugGeneratorInterface $taxonSlugGenerator,
        ResourcesCache $cache,
        EntityManagerInterface $em
    ) {
        $this->taxonRepository = $taxonRepository;
        $this->taxonFactory = $taxonFactory;
        $this->taxonSlugGenerator = $taxonSlugGenerator;
        $this->cache = $cache;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     *
     * Accepted format:
     *
     * code,parent,label-locale1,label-locale2,...description-locale1
     */
    public function process(array $data): void
    {
        /** @var TaxonInterface $taxon */
        $taxon = $this->cache->getTaxon($data['code']) ?? $this->taxonFactory->createNew();
        $taxon->setCode($data['code']);

        if ($data['parent']) {
            /** @var TaxonInterface $parent */
            $parent = $this->cache->getTaxon($data['parent']);
            $taxon->setParent($parent);
        }

        foreach ($data as $key => $translation) {
            if (strpos($key, 'label-') === false) {
                continue;
            }

            $locale = substr($key, 6);

            if (!$this->isLocaleSupported($locale)) {
                continue;
            }

            $taxon->setCurrentLocale($locale);
            $taxon->setFallbackLocale($locale);
            $taxon->setName($translation);
            $taxon->setDescription($data['description-' . $locale] ?? null);
            $taxon->setSlug($this->taxonSlugGenerator->generate($taxon, $locale));

            $this->em->persist($taxon);
            $this->cache->storeTaxon($taxon);
        }
    }

    public function isLocaleSupported(string $locale): bool
    {
        if (!$this->localeList) {
            foreach ($this->cache->getLocales() as $systemLocale) {
                $this->localeList[] = $systemLocale->getCode();
            }
        }

        return in_array($locale, $this->localeList);
    }
}
