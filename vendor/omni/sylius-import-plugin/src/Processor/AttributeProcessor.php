<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Attribute\Model\AttributeValueInterface;
use Sylius\Component\Product\Model\ProductAttributeInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class AttributeProcessor implements ResourceProcessorInterface
{
    use AvailableLocalesTrait;

    protected const AVAILABLE_TYPES = [
        'text' => AttributeValueInterface::STORAGE_TEXT,
        'textarea' => AttributeValueInterface::STORAGE_TEXT,
        'select' => AttributeValueInterface::STORAGE_JSON,
        'checkbox' => AttributeValueInterface::STORAGE_BOOLEAN,
        'integer' => AttributeValueInterface::STORAGE_INTEGER,
        'percent' => AttributeValueInterface::STORAGE_FLOAT,
        'date' => AttributeValueInterface::STORAGE_DATE,
        'datetime' => AttributeValueInterface::STORAGE_DATETIME,
    ];

    /** @var ResourcesCache */
    protected $cache;

    /** @var FactoryInterface */
    protected $productAttributeFactory;

    /** @var EntityManagerInterface  */
    protected $em;

    public function __construct(
        ResourcesCache $cache,
        FactoryInterface $productAttributeFactory,
        EntityManagerInterface $em
    ) {
        $this->cache = $cache;
        $this->productAttributeFactory = $productAttributeFactory;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     *
     * Required fields:
     *
     * code: attribute code
     * type: attribute type, e.g. text, textarea, checkbox, select, integer, date
     *
     * Optional fields:
     * label-{locale}: where {locale} is any valid locale, eg lt, en, en_US, etc.
     * multiple: 1 or 0. Needed if attribute is of the type select (default 0)
     * useable_as_grid_filter: if filter or ES plugin is installed values of this attribute will be used as filters
     * scopable: 1 or 0. Attribute will depend on channel. Only used with Attribute plugin installed.
     * localizable: 1 or 0. Attribute will depend on locale. Only used with Attribute plugin installed.
     *
     */
    public function process(array $data): void
    {
        $type = $data['type'] ?? null;
        /** @var string|null $code */
        $code = $data['code'] ?? null;

        if (!$type || !array_key_exists($type, self::AVAILABLE_TYPES)) {
            throw new ItemIncompleteException('Invalid attribute type provided');
        } elseif (!$code) {
            throw new ItemIncompleteException('Code must be set');
        }

        $attr = $this->getProductAttribute($code);

        // We can't change these after the attribute is created
        if (!$attr->getId()) {
            $attr->setType($type);
            $attr->setStorageType(self::AVAILABLE_TYPES[$type]);
        }

        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $attr->setFallbackLocale($locale);
            $attr->setCurrentLocale($locale);

            $attr->setName($data['label-'.$locale]);
        }

        if ($attr->getType() == 'select') {
            $attr->setConfiguration(
                array_merge($attr->getConfiguration(), ['multiple' => (bool) $data['multiple'] ?? false])
            );
        }

        // Needs ES or Filter plugin for this
        if (method_exists($attr, 'setFilterable')) {
            $attr->setFilterable((bool) $data['useable_as_grid_filter'] ?? false);
        }

        // Needs Attribute plugin for this
        if (method_exists($attr, 'setValuePerChannel')) {
            $attr->setValuePerChannel((bool) $data['scopable'] ?? false);
        }

        // Needs Attribute plugin for this
        if (method_exists($attr, 'setValuePerLocale')) {
            $attr->setValuePerLocale((bool) $data['localizable'] ?? false);
        }

        $this->cache->cacheAttribute($attr);
        $this->em->persist($attr);
    }

    private function getProductAttribute(string $code): ProductAttributeInterface
    {
        /** @var ProductAttributeInterface|null $productAttribute */
        $productAttribute = $this->cache->getAttribute($code);

        if (null === $productAttribute) {
            /** @var ProductAttributeInterface $productAttribute */
            $productAttribute = $this->productAttributeFactory->createNew();
            $productAttribute->setCode($code);
        }

        return $productAttribute;
    }
}
