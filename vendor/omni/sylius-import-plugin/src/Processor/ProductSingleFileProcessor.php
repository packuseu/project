<?php
/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ImporterException;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImporterRegistry;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Attribute\Model\AttributeValueInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Writer\CsvWriter;
use Omni\Sylius\ImportPlugin\Model\ImportJobInterface;
use Omni\Sylius\ImportPlugin\Model\ImportJob;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Product\Generator\SlugGeneratorInterface;

class ProductSingleFileProcessor implements ResourceProcessorInterface, FullDataProcessorInterface
{
    use AvailableLocalesTrait;
    use AvailableChannelsTrait;

    protected const IMPORTER_PRODUCT = 'product';
    protected const IMPORTER_PRODUCT_ATTRIBUTE = 'product_attribute';
    protected const IMPORTER_PRODUCT_ATTRIBUTE_OPTION = 'product_attribute_option';
    protected const IMPORTER_PRODUCT_OPTION = 'product_option';
    protected const IMPORTER_PRODUCT_OPTION_VALUE = 'product_option_value';
    protected const IMPORTER_PRODUCT_IMAGE = 'product_image';
    protected const IMPORTER_BUSINESS = 'business_unit_stock';
    protected const IMPORTER_VARIANT_DISABLE_MISSING = 'product_variant_disable_missing';

    /** @var ResourcesCache */
    protected $cache;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var CsvWriter */
    protected $csvWriter;

    /*** @var RepositoryInterface */
    protected $importJobRepository;

    /** @var SlugGeneratorInterface */
    protected $slugGenerator;

    /** @var ImporterRegistry */
    protected $importerRegistry;

    /** @var string */
    protected $uploadDir;

    /** @var array */
    protected $keyMap;

    /** @var array */
    protected $jobs;

    /** @var bool */
    protected $disableVariants;

    public function __construct(
        ResourcesCache $cache,
        EntityManagerInterface $em,
        CsvWriter $csvWriter,
        string $uploadDir,
        RepositoryInterface $importJobRepository,
        SlugGeneratorInterface $slugGenerator,
        array $keyMap,
        ImporterRegistry $importerRegistry,
        bool $disableVariants
    ) {
        $this->cache = $cache;
        $this->em = $em;
        $this->csvWriter = $csvWriter;
        $this->uploadDir = $uploadDir;
        $this->importJobRepository = $importJobRepository;
        $this->slugGenerator = $slugGenerator;
        $this->keyMap = $keyMap;
        $this->importerRegistry = $importerRegistry;
        $this->locales = $this->getAvailableLocaleCodes($this->cache);
        $this->channels = $this->getAvailableChannelCodes($this->cache);
        $this->disableVariants = $disableVariants;
    }

    /**
     * {@inheritdoc}
     *
     * Processes single file, splitting it into different files for other processors.
     *
     * @throws ItemIncompleteException
     * @throws ImporterException
     */
    public function process(array $data): void
    {
        try {
            $this->handleAttributeProcessor($data);
            $this->handleProductOptionProcessor($data);
            $this->handleProductOptionValueProcessor($data);
            $this->handleProductProcessor($data);
            $this->handleProductImagesProcessor($data);
            if ($this->disableVariants) {
                $this->handleProductVariantDisable($data);
            }
            $this->handleBusinessProcessor($data);
        } catch (ImporterException $exception) {
            throw new ImporterException($exception->getMessage());
        }

        $this->commitJobs();
    }

    /**
     * {@inheritdoc}
     *
     * code
     * variant
     * img1
     * img2
     * img3
     * img4
     * img5
     *
     */
    protected function handleProductImagesProcessor(array $data): void
    {
        $csvData = [];
        $processedProducts = [];

        foreach ($data as $row) {
            $code = $row['master_product_code'];

            if (array_key_exists($code, $processedProducts)) {
                continue;
            }

            $processedProducts[$code] = $code;
            $data = $this->getFilteredData($row, 'image:', 'img');
            $data = array_merge(['code' => $code], $data);

            if ($row['master_product_code'] !== $row['code']) {
                $data['variant'] = $row['code'];
            }
            $csvData[] = $data;
        }

        $this->prepareFiles($csvData, self::IMPORTER_PRODUCT_IMAGE);
    }

    /**
     * {@inheritdoc}
     *
     * code
     *
     */
    protected function handleProductVariantDisable(array $data): void
    {
        $csvData = [];
        $processedVariants = [];

        foreach ($data as $row) {
            $variantCode = $this->getVariantCode($row);
            if (empty($variantCode)) {
                continue;
            }

            if (array_key_exists($variantCode, $processedVariants)) {
                continue;
            }

            $processedVariants[$variantCode] = $variantCode;
            $csvData[]['code'] = $variantCode;
        }

        $this->prepareFiles($csvData, self::IMPORTER_VARIANT_DISABLE_MISSING);
    }

    /**
     * {@inheritdoc}
     *
     * option
     * code
     * label-en_US
     * label-lt_LT
     *
     */
    protected function handleProductOptionValueProcessor(array $data): void
    {
        $csvData = [];
        $i = 0;
        foreach ($data as $k => $row) {
            $variants = $this->getFilteredData($row, 'variant:');

            foreach ($variants as $option => $optionValue) {
                if (empty($optionValue)) {
                    continue;
                }

                $csvData[$i]['code'] = $this->codifyString($optionValue);
                $csvData[$i]['option'] = $this->codifyString($option);
                foreach ($this->locales as $locale) {
                    $csvData[$i]['label-'.$locale] = $this->getOptionValueLabel($optionValue);
                }
                $i++;
            }
        }

        $csvData = array_unique($csvData, SORT_REGULAR);

        $this->prepareFiles($csvData, self::IMPORTER_PRODUCT_OPTION_VALUE);
    }

    /**
     * {@inheritdoc}
     *
     * code
     * label-en_US
     * label-lt_LT
     *
     */
    protected function handleProductOptionProcessor(array $data): void
    {
        $csvData = [];
        $i = 0;
        foreach ($data as $k => $row) {
            $variants = $this->getFilteredData($row, 'variant:');
            foreach ($variants as $option => $optionValue) {
                $csvData[$i]['code'] = $this->codifyString($option);
                foreach ($this->locales as $locale) {
                    $csvData[$i]['label-'.$locale] = $option;
                }
                $i++;
            }
        }

        $csvData = array_unique($csvData, SORT_REGULAR);

        $this->prepareFiles($csvData, self::IMPORTER_PRODUCT_OPTION);
    }

    /**
     * @param array $data
     *
     * code
     * label-en_US
     * label-lt_LT
     * type
     * useable_as_grid_filter
     * multiple
     *
     */
    protected function handleAttributeProcessor(array $data): void
    {
        $csvData = [];
        $attributes = $this->getFilteredData(current($data), 'attribute:');

        foreach ($attributes as $name => $value) {
            $data = [];

            $data['code'] = $this->codifyString($name);
            $data['type'] = AttributeValueInterface::STORAGE_TEXT;
            $data['useable_as_grid_filter'] = 1;
            $data['multiple'] = 0;

            foreach ($this->locales as $locale) {
                $data['label-'.$locale] = $name;
            }

            $csvData[] = $data;
        }

        $this->prepareFiles($csvData, self::IMPORTER_PRODUCT_ATTRIBUTE);
    }

    /**
     * {@inheritdoc}
     *
     * code
     * parent
     * categories
     * name-en_US
     * name-lt_LT
     * price-EUR
     * option-color
     * knitting_method
     * features
     * t_shirt_brand
     * ecological
     * variant_weight
     * variant_height
     * available_in_channel-b2c
     * available_in_channel-b2b
     * description-en_US
     * short_description-en_US
     *
     */
    protected function handleProductProcessor(array $data): void
    {
        $csvData = [];

        foreach ($data as $k => $row) {
            $variantCode = $this->getVariantCode($row);
            if (empty($variantCode)) {
                continue;
            }

            $csvData[$k]['code'] = $variantCode;
            $csvData[$k]['parent'] = $row['master_product_code'];
            $csvData[$k]['price-EUR'] = $row['price'];
            $csvData[$k]['orig_price-EUR'] = $row['old_price'] ?? null;
            $csvData[$k]['barcode'] = $row['ean'] ?? null;

            $variants = $this->getFilteredData($row, 'variant:');
            $attributes = $this->getFilteredData($row, 'attribute:');

            foreach ($variants as $option => $optionValue) {
                if (!empty($optionValue)) {
                    $csvData[$k]['option-'.$this->codifyString($option)] = $this->codifyString($optionValue);
                } else {
                    $csvData[$k]['option-'.$this->codifyString($option)] = '';
                }
            }

            foreach ($attributes as $attribute => $attributeValue) {
                $csvData[$k][$this->codifyString($attribute)] = $attributeValue;
            }

            foreach ($this->locales as $locale) {
                $csvData[$k]['name-'.$locale] = $row['title'];
                $csvData[$k]['description-'.$locale] = $row['description'];
            }

            foreach ($this->channels as $channel) {
                $csvData[$k]['available_in_channel-'.$channel] = 1;
            }
        }

        $this->prepareFiles($csvData, self::IMPORTER_PRODUCT);
    }

    /**
     * {@inheritdoc}
     *
     * business_code
     * variant_code
     * stock
     *
     */
    protected function handleBusinessProcessor(array $data): void
    {
        $csvData = [];
        $i = 0;
        foreach ($data as $k => $row) {
            $variantCode = $this->getVariantCode($row); //code must be unique
            if (empty($variantCode)) {
                continue;
            }

            $stocks = $this->getFilteredData($row, 'stock:');

            foreach ($stocks as $stock => $stockValue) {
                $csvData[$i]['variant_code'] = $variantCode;
                $csvData[$i]['business_code'] = $stock;
                $csvData[$i]['stock'] = $stockValue;
                $i++;
            }
        }

        $this->prepareFiles($csvData, self::IMPORTER_BUSINESS);
    }

    protected function getFilteredData(array $data, string $filterBy, string $prefix = ''): array
    {
        $values = [];

        foreach ($data as $key => $value) {
            if (stripos($key, $filterBy) === 0) {
                $values[str_replace($filterBy, $prefix, $key)] = $value;
            }
        }

        return $values;
    }

    protected function codifyString(string $value): string
    {
        return str_replace('-', '_', $this->slugGenerator->generate($value));
    }

    protected function getOptionValueLabel(string $value): string
    {
        // Replace commas with dots, to prevent duplicate entries.
        return str_replace(',', '.', $value);
    }

    protected function getVariantCode(array $row): string
    {
        if (!isset($row[$this->keyMap['variant_code']])) {
            throw new ItemIncompleteException(
                'Invalid variant code provided for ' . $this->keyMap['variant_code'].'. File does not contain such column.'
            );
        }
        return $row[$this->keyMap['variant_code']];
    }

    protected function prepareFiles(array $csvData, string $importer): void
    {
        if (!empty($csvData)) {
            $this->jobs[$importer] = $csvData;
        }
    }

    protected function commitJobs(): void
    {
        $order = [
            self::IMPORTER_PRODUCT_ATTRIBUTE,
            self::IMPORTER_PRODUCT_ATTRIBUTE_OPTION,
            self::IMPORTER_PRODUCT_OPTION,
            self::IMPORTER_PRODUCT_OPTION_VALUE,
            self::IMPORTER_PRODUCT,
            self::IMPORTER_PRODUCT_IMAGE,
            self::IMPORTER_BUSINESS,
            self::IMPORTER_VARIANT_DISABLE_MISSING,
        ];

        foreach ($order as $key => $importer) {
            if (!isset($this->jobs[$importer])) {
                unset($order[$key]);
            }
        }

        $ordered = array_merge(array_flip($order), $this->jobs);

        foreach ($ordered as $importer => $csvData) {
            $this->handleFileAndJob($csvData, $importer);
        }
    }

    protected function handleFileAndJob(array $csvData, string $importer): void
    {
        if (!empty($csvData)) {
            $name = ImporterRegistry::buildServiceName($importer, 'csv');
            if ($this->importerRegistry->has($name)) {
                $file = $this->createFile($csvData);
                $this->createJob($file, $importer);
            }
        }
    }

    protected function createFile(array $data): string
    {
        $file = $this->uploadDir.uniqid().'.csv';

        $this->csvWriter->setFile($file);
        $this->csvWriter->write(array_keys($data[0]));

        foreach ($data as $row) {
            $this->csvWriter->write($row);
        }

        $this->csvWriter->finish();

        return $file;
    }

    protected function createJob(string $file, string $importer): void
    {
        $job = new ImportJob();
        $job->setImporter($importer);
        $job->setFile($file);
        $job->setStatus(ImportJobInterface::STATE_NEW);
        $this->importJobRepository->add($job);
    }
}
