<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;

trait AvailableChannelsTrait
{
    /** @var string[]  */
    protected $channels = [];

    protected function getAvailableChannelCodes(ResourcesCache $cache): array
    {
        if ($this->channels) {
            return $this->channels;
        }

        foreach ($cache->getChannelCodes() as $channel) {
            $channels[] = $channel;
        }

        return $channels;
    }
}
