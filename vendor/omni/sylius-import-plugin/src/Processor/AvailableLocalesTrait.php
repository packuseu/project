<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;

trait AvailableLocalesTrait
{
    /** @var string[]  */
    protected $locales = [];

    protected function getAvailableLocaleCodes(ResourcesCache $cache): array
    {
        if ($this->locales) {
            return $this->locales;
        }

        foreach ($cache->getLocales() as $locale) {
            $locales[] = $locale->getCode();
        }

        return $locales;
    }
}
