<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 *
 */

declare (strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Product\Generator\SlugGeneratorInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Resource\Model\ToggleableInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;

class ProductVariantDisableMissingProcessor implements ResourceProcessorInterface, FullDataProcessorInterface
{
    /** @var EntityManagerInterface */
    protected $em;

    protected const BATCH_SIZE = 20;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    public function process(array $data): void
    {
        $enabledVariants = array_column($data, 'code');
        $q = $this->em->createQuery('SELECT p
            FROM ' . ProductInterface::class . ' p
            WHERE p.enabled = 1'
        );
        $iterableResult = $q->iterate();

        $i = 0;
        foreach ($iterableResult as $row) {
            $i++;
            $product = $row[0];
            $variants = $product->getVariants();
            $disableProduct = true;

            foreach ($variants as $variant) {
                if (in_array($variant->getCode(), $enabledVariants)) {
                    $disableProduct = false;
                } else if ($variant instanceof ToggleableInterface) {
                    $variant->setEnabled(false);
                }
            }

            if ($disableProduct) {
                $product->setEnabled(false);
            }

            if ($i === 20) {
                $this->em->flush();
                $this->em->clear();
                $i = 0;
            }
        }

        if ($i > 0) {
            $this->em->flush();
            $this->em->clear();
        }
    }
}
