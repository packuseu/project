<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ImporterException;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Attribute\Model\AttributeInterface;
use Sylius\Component\Attribute\Model\AttributeValueInterface;
use Sylius\Component\Core\Model\ChannelPricingInterface;
use Sylius\Component\Core\Model\ProductInterface;
use Sylius\Component\Core\Model\ProductTaxonInterface;
use Sylius\Component\Core\Model\ProductVariantInterface;
use Sylius\Component\Core\Model\Taxon;
use Sylius\Component\Product\Generator\SlugGeneratorInterface;
use Sylius\Component\Product\Model\ProductAttributeValueInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Taxonomy\Model\TaxonInterface;
use Sylius\Component\Resource\Model\ToggleableInterface;

class ProductProcessor implements ResourceProcessorInterface
{
    use AvailableLocalesTrait;

    protected const PRODUCT = 'product';
    protected const VARIANT = 'variant';
    protected const NAMES = 'names';
    protected const DESCRIPTIONS = 'desc';
    protected const FULL = 'full';
    protected const SHORT = 'short';
    protected const SLUG = 'slug';
    protected const WEIGHT = 'variant_weight';
    protected const WIDTH = 'variant_width';
    protected const HEIGHT = 'variant_height';
    protected const DEPTH = 'variant_depth';

    protected const KEY_NAME = 'name-';
    protected const KEY_VAR_NAME = 'variant_name-';
    protected const KEY_SLUG = 'slug-';
    protected const KEY_DESC = 'description-';
    protected const KEY_SHORT_DESC = 'short_description-';
    protected const KEY_AVAILABLE_IN_CHANNEL = 'available_in_channel-';

    /** @var ResourcesCache */
    protected $cache;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var FactoryInterface */
    protected $productFactory;

    /** @var FactoryInterface */
    protected $productVariantFactory;

    /** @var FactoryInterface */
    protected $productAttributeValueFactory;

    /** @var FactoryInterface */
    protected $productTaxonFactory;

    /** @var FactoryInterface */
    protected $channelPricingFactory;

    /** @var RepositoryInterface */
    protected $productRepository;

    /** @var RepositoryInterface */
    protected $productVariantRepository;

    /** @var SlugGeneratorInterface */
    protected $slugGenerator;

    /** @var FactoryInterface  */
    protected $attributeFactory;

    public function __construct(
        ResourcesCache $cache,
        EntityManagerInterface $em,
        FactoryInterface $productFactory,
        FactoryInterface $productVariantFactory,
        FactoryInterface $productAttributeValueFactory,
        FactoryInterface $productTaxonFactory,
        FactoryInterface $channelPricingFactory,
        FactoryInterface $attributeFactory,
        RepositoryInterface $productRepository,
        RepositoryInterface $productVariantRepository,
        SlugGeneratorInterface $slugGenerator
    ) {
        $this->cache = $cache;
        $this->em = $em;
        $this->productFactory = $productFactory;
        $this->productVariantFactory = $productVariantFactory;
        $this->productAttributeValueFactory = $productAttributeValueFactory;
        $this->productTaxonFactory = $productTaxonFactory;
        $this->channelPricingFactory = $channelPricingFactory;
        $this->productRepository = $productRepository;
        $this->productVariantRepository = $productVariantRepository;
        $this->slugGenerator = $slugGenerator;
        $this->attributeFactory = $attributeFactory;
    }

    /**
     * {@inheritdoc}
     *
     * Special keys for processing:
     *
     * code       - the code of the product (or product variant if parent is defined)
     * parent     - the code of the product to whom the variant belongs
     * categories - list of categories that the product belongs to.
     *
     * name-lt_LT              - name of the product (lt_LT is a locale, any number of locales can be provided by different fields)
     *                           The names of the products are required for all locales;
     * variant_name-lt_LT      - name of the product variant (if empty - name of product + option values will be given)
     * slug-lt_LT              - optional
     * description-lt_LT       - product description
     * short-description-lt_LT - product short description
     *
     * available_in_channel-ch1 - bool that defines which channels are available and which are not (ch1 - channel code).
     *                            This value is optional, as if no channels will be provided - all channels will be
     *                            assigned to the product.
     *
     * attributes are set as simple key => value pairs;
     * options are set exactly as attributes are, but have the prefix option- beforehand;
     *
     * @throws ItemIncompleteException
     * @throws ImporterException
     */
    public function process(array $data): void
    {
        try {
            $this->doProcess($data);
        } catch (ItemIncompleteException $exception) {
            throw $exception;
        } catch (\Throwable $exception) {
            throw new ImporterException($exception->getMessage());
        }
    }

    protected function doProcess(array $data): void
    {
        $product = $this->getProduct($data);
        $variant = $this->getProductVariant($data['code'], $product);

        unset($data['code']);
        unset($data['parent']);

        $this->setDetails($product, $variant, $data);
        $this->handleVariant($variant, $data);
        $this->handleChannels($product, $data);
        $this->handleTaxons($product, $data);
        $this->handleAttributes($product, $variant, $data);

        $this->cache->cacheProduct($product);
        $this->cache->cacheVariant($variant);
        $this->em->persist($product);
    }

    /**
     * @throws ItemIncompleteException
     */
    protected function getProduct(array $data): ProductInterface
    {
        $code = isset($data['parent']) && $data['parent'] ? $data['parent'] : $data['code'];

        if (!$code) {
            throw new ItemIncompleteException('Product must have a code!');
        }

        $product = $this->cache->getProduct($code);

        if ($product) {
            return $product;
        }

        /** @var ProductInterface|null $product */
        $product = $this->productRepository->findOneBy(['code' => $code]);

        if (null === $product) {
            /** @var ProductInterface $product */
            $product = $this->productFactory->createNew();
            $product->setCode($code);
            $product->setVariantSelectionMethod('match');
        }

        $product->setEnabled(true);

        return $product;
    }

    /**
     * @throws ItemIncompleteException
     */
    protected function getProductVariant(string $code, ProductInterface $product): ProductVariantInterface
    {
        $variant = $this->cache->getVariant($code);

        if ($variant) {
            return $variant;
        }

        /** @var ProductVariantInterface|null $productVariant */
        $productVariant = $this->productVariantRepository->findOneBy(['code' => $code]);

        if ($productVariant && $productVariant->getProduct()->getCode() != $product->getCode()) {
            throw new ItemIncompleteException('Invalid product variant code provided');
        }

        if ($productVariant === null) {
            /** @var ProductVariantInterface $productVariant */
            $productVariant = $this->productVariantFactory->createNew();
            $productVariant->setCode($code);
            $productVariant->setProduct($product);
            $product->addVariant($productVariant);
        }

        return $productVariant;
    }

    protected function setDetails(ProductInterface $product, ProductVariantInterface $variant, array &$data): void
    {
        $productDetails = $this->getDetails($data);

        $variant->setWeight($productDetails[self::WEIGHT]);
        $variant->setWidth($productDetails[self::WIDTH]);
        $variant->setHeight($productDetails[self::HEIGHT]);
        $variant->setDepth($productDetails[self::DEPTH]);

        foreach ($productDetails['translations'] as $locale => $details) {
            $product->setCurrentLocale($locale);
            $product->setFallbackLocale($locale);
            $variant->setCurrentLocale($locale);
            $variant->setFallbackLocale($locale);

            $product->setName($details[self::NAMES][self::PRODUCT]);
            $variant->setName($details[self::NAMES][self::VARIANT]);
            $product->setDescription($details[self::DESCRIPTIONS][self::FULL]);
            $product->setShortDescription($details[self::DESCRIPTIONS][self::SHORT]);
            $product->setSlug($this->getValidSlug($product, $details[self::NAMES][self::SLUG]));

            $product->setMetaDescription(substr(($data['meta-description-'.$locale] ?? ''), 0, 255));
            $product->setMetaKeywords(substr(($data['meta-keywords-'.$locale] ?? ''), 0, 255));
        }
    }

    protected function getValidSlug(ProductInterface $product, ?string $slug): string
    {
        // TODO: Validate that slug is unique
        if ($slug) {
            return $slug;
        }

        if ($product->getSlug()) {
            return $product->getSlug();
        }

        $slug = $this->slugGenerator->generate($product->getName());

        if ($this->isSlugUnique($slug, $product->getTranslation()->getLocale())) {
            return $slug;
        } else {
            return $slug . '-' . $product->getCode();
        }
    }

    protected function isSlugUnique(string $slug, string $locale): bool
    {
        $stmt = $this->em->getConnection()->prepare(
            'SELECT id FROM sylius_product_translation WHERE locale = :locale AND slug = :slug'
        );

        $stmt->execute(['locale' => $locale, 'slug' => $slug]);
        $result = $stmt->fetchAll();

        if (!empty($result)) {
            return false;
        }

        foreach ($this->cache->getCachedProducts() as $product) {
            if ($product->getTranslation($locale)->getSlug() == $slug) {
                return false;
            }
        }

        return true;
    }

    /**
     * @throws ItemIncompleteException
     *
     * @todo: handle multi-currencies
     * @todo: consider price per channel
     */
    protected function handleVariant(ProductVariantInterface $productVariant, array &$data): void
    {
        if (!isset($data['price-EUR'])) {
            throw new ItemIncompleteException('No price provided');
        }

        $origPrice = null;

        if (isset($data['orig_price-EUR']) && !empty($data['orig_price-EUR'])) {
            $data['orig_price-EUR'] = str_replace(',', '.', $data['orig_price-EUR']);

            $origPrice = (int)round($data['orig_price-EUR'] * 100, 2);
        }

        $data['price-EUR'] = str_replace(',', '.', $data['price-EUR']);

        $this->handlePricing($productVariant, (int)round($data['price-EUR'] * 100, 2), $origPrice);
        unset($data['price-EUR']);

        foreach ($data as $key => $value) {
            if (strpos($key, 'option-') === 0 && $value) {
                $this->handleVariantOptionProcessing($productVariant, substr($key, 7), $value);
                $this->handleVariantName($productVariant);

                unset($data[$key]);
            }
        }

        if ($productVariant instanceof ToggleableInterface) {
            $productVariant->setEnabled(true);
        }
    }

    protected function handleChannels(ProductInterface $product, array &$data): void
    {
        $channels = [];

        foreach ($data as $key => $value) {
            if (strpos($key, self::KEY_AVAILABLE_IN_CHANNEL) === 0) {
                $channels[] = substr($key, 21);
                unset($data[$key]);
            }
        }

        foreach ($this->cache->getChannels() as $channel) {
            if (empty($channels) || in_array($channel->getCode(), $channels)) {
                $product->addChannel($channel);
            }
        }
    }

    protected function handleTaxons(ProductInterface $product, array &$data): void
    {
        $taxons = explode(',', $data['categories'] ?? '');
        unset($data['categories']);

        if (empty($taxons)) {
            return;
        }

        foreach ($taxons as $taxon) {
            $this->handleRecursiveTaxonAddition($product, $taxon);
        }

        $this->handleMainTaxon($product);
    }

    protected function handleRecursiveTaxonAddition(ProductInterface $product, string $taxon): void
    {
        $taxon = $this->cache->getTaxon($taxon);

        while ($taxon && !$product->hasTaxon($taxon)) {
            $this->addTaxonToProduct($product, $taxon);
            $taxon = $taxon->getParent();
        }
    }

    protected function addTaxonToProduct(ProductInterface $product, TaxonInterface $taxon): void
    {
        /** @var ProductTaxonInterface $productTaxon */
        $productTaxon = $this->productTaxonFactory->createNew();
        $productTaxon->setTaxon($taxon);
        $product->addProductTaxon($productTaxon);
    }

    protected function handleMainTaxon(ProductInterface $product): void
    {
        $mainTaxon = $product->getMainTaxon();

        if ($mainTaxon) {
            return;
        }

        foreach ($product->getTaxons() as $taxon) {
            if (!$mainTaxon || $mainTaxon->getLevel() < $taxon->getLevel()) {
                $mainTaxon = $taxon;
            }
        }

        $product->setMainTaxon($mainTaxon);
    }

    protected function handleAttributes(ProductInterface $product, ProductVariantInterface $variant, array $data): void
    {
        $isAttributePluginInstalled = $this->isAttributePluginInstalled();

        foreach ($data as $key => $value) {
            if ($isAttributePluginInstalled) {
                $this->handleAdvancedAttributes($product, $key, $value);

                continue;
            }

            $attribute = $this->cache->getAttribute($key);

            if (!$attribute) {
                continue;
            }

            foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
                $this->handleProductAttribute($product, $attribute, $value, $locale);
            }
        }
    }

    protected function isAttributePluginInstalled(): bool
    {
        $attribute = $this->attributeFactory->createNew();

        return method_exists($attribute, 'isValuePerChannel') && method_exists($attribute, 'isValuePerLocale');
    }

    protected function handleAdvancedAttributes(ProductInterface $product, string $key, $value): void
    {
        $data = explode('-', $key);
        $key = $data[0] ?? '';
        $locale = null;
        $channel = null;

        $attribute = $this->cache->getAttribute($key);

        if (!$attribute) {
            return;
        }

        if ($attribute->isValuePerLocale() && $attribute->isValuePerChannel()) {
            $locale = $data[1] ?? null;
            $channel = $data[2] ?? null;

            if (!$locale || !$channel) {
                throw new ItemIncompleteException('Locale and channel need to be provided for attribtue ' . $key);
            }

            if (!in_array($locale, $this->getAvailableLocaleCodes($this->cache))) {
                throw new ItemIncompleteException('Invalid locale provided for ' . $key);
            }

            if (!in_array($channel, $this->cache->getChannelCodes())) {
                throw new ItemIncompleteException('Invalid channel provided for ' . $key);
            }
        } elseif ($attribute->isValuePerLocale()) {
            $locale = $data[1] ?? null;

            if (!in_array($locale, $this->getAvailableLocaleCodes($this->cache))) {
                throw new ItemIncompleteException('Invalid locale provided for ' . $key);
            }
        } elseif ($attribute->isValuePerChannel()) {
            $channel = $data[1] ?? null;

            if (!in_array($channel, $this->cache->getChannelCodes())) {
                throw new ItemIncompleteException('Invalid channel provided for ' . $key);
            }
        }

        $this->handleProductAttribute($product, $attribute, $value, $locale, $channel);
    }

    protected function handleProductAttribute(
        ProductInterface $product,
        AttributeInterface $attribute,
        $value,
        string $locale,
        string $channel = null
    ): void {
        $attributeValue = $product->getAttributeByCodeAndLocale($attribute->getCode(), $locale, $channel);

        if ($attributeValue) {
            if ($value === null || $value === '') {
                $product->removeAttribute($attributeValue);

                return;
            }

            $attributeValue->setValue($this->getAttributeValue($attributeValue, $value));
        } elseif ($value !== null && $value !== '') {
            /** @var ProductAttributeValueInterface $attr */
            $attr = $this->productAttributeValueFactory->createNew();
            $attr->setAttribute($attribute);
            $attr->setProduct($product);
            $attr->setLocaleCode($locale);

            if (method_exists($attr, 'setChannelCode')) {
                $attr->setChannelCode($channel);
            }

            $attr->setValue($this->getAttributeValue($attr, $value));
            $product->addAttribute($attr);
        }
    }

    protected function getAttributeValue(AttributeValueInterface $attributeValue, $value)
    {
        switch ($attributeValue->getAttribute()->getStorageType()) {
            case AttributeValueInterface::STORAGE_INTEGER:
                return (int)$value;
            case AttributeValueInterface::STORAGE_BOOLEAN:
                return (bool)$value;
            case AttributeValueInterface::STORAGE_FLOAT:
                return (float)$value;
            case AttributeValueInterface::STORAGE_DATE:
                return new \DateTime($value);
            case AttributeValueInterface::STORAGE_JSON:
                return explode(',', $value);
            case AttributeValueInterface::STORAGE_TEXT:
                return iconv("UTF-8", "UTF-8//IGNORE", $value);
        }

        return $value;
    }

    protected function handlePricing(ProductVariantInterface $productVariant, int $price, ?int $origPrice): void
    {
        foreach ($this->cache->getChannels() as $channel) {
            $channelCode = $channel->getCode();

            $channelPricing = $productVariant->getChannelPricingForChannel($channel);

            if (null === $channelPricing) {
                /** @var ChannelPricingInterface $channelPricing */
                $channelPricing = $this->channelPricingFactory->createNew();
                $channelPricing->setChannelCode($channelCode);
                $productVariant->addChannelPricing($channelPricing);
            }

            $channelPricing->setPrice($price);

            if ($origPrice && $origPrice > $price) {
                $channelPricing->setOriginalPrice($origPrice);
            }
        }
    }

    /**
     * @throws ItemIncompleteException
     */
    protected function handleVariantOptionProcessing(ProductVariantInterface $productVariant, $optionCode, $value): void
    {
        /** @var ProductOptionInterface $option */
        $option = $this->cache->getOption($optionCode);
        /** @var ProductOptionValueInterface $optionValue */
        $optionValue = $this->cache->getOptionValue($value);

        if (!$option) {
            throw new ItemIncompleteException('Option ' . $optionCode . ' not found!');
        }

        if (!$optionValue || $optionValue->getOptionCode() != $optionCode) {
            throw new ItemIncompleteException('Invalid option value `' . $value . '` provided for option ' . $optionCode);
        }

        if (!$productVariant->getProduct()->hasOption($option)) {
            $productVariant->getProduct()->addOption($option);
        }

        foreach ($productVariant->getOptionValues() as $optValue) {
            if ($optValue->getOptionCode() === $optionValue->getOptionCode()
                && $optValue->getValue() !== $optionValue->getValue()
            ) {
                $productVariant->removeOptionValue($optValue);
            }
        }

        $productVariant->addOptionValue($optionValue);
    }

    protected function handleVariantName(ProductVariantInterface $productVariant): void
    {
        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $productVariant->setCurrentLocale($locale);
            $productVariant->setFallbackLocale($locale);

            if ($productVariant->getName()) {
                continue;
            }

            $i = 0;
            $values = $productVariant->getOptionValues();
            $name = $productVariant->getProduct()->getTranslation($locale)->getName();

            if (count($values)) {
                $name .= ' (';
            }

            foreach ($values as $value) {
                $name .= $value->getTranslation($locale)->getValue();

                if (++$i < count($values)) {
                    $name .= ', ';
                }
            }

            if (count($values)) {
                $name .= ')';
            }

            $productVariant->setName($name);
        }
    }

    protected function getDetails(array &$data): array
    {
        $details = [
            self::WIDTH => $data[self::WIDTH] ?? null,
            self::WEIGHT => $data[self::WEIGHT] ?? null,
            self::HEIGHT => $data[self::HEIGHT] ?? null,
            self::DEPTH => $data[self::DEPTH] ?? null,
        ];

        foreach ($details as $key => $value) {
            if ($value) {
                $details[$key] = (float) $value;
            } else {
                $details[$key] = null;
            }
        }

        unset($data[self::WIDTH], $data[self::WEIGHT], $data[self::HEIGHT], $data[self::DEPTH]);

        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $localeDetails = [
                self::NAMES => [self::PRODUCT => null, self::VARIANT => null, self::SLUG => null],
                self::DESCRIPTIONS => [self::FULL => null, self::SHORT => null],
            ];

            foreach ($data as $key => $value) {
                if ($key == self::KEY_NAME.$locale) {
                    $localeDetails[self::NAMES][self::PRODUCT] = substr($value, 0, 255);
                    unset($data[$key]);
                }

                if ($key == self::KEY_VAR_NAME.$locale) {
                    $localeDetails[self::NAMES][self::VARIANT] = substr($value, 0, 255);
                    unset($data[$key]);
                }

                if ($key == self::KEY_SLUG.$locale) {
                    $localeDetails[self::NAMES][self::SLUG] = substr($value, 0, 255);
                    unset($data[$key]);
                }

                if ($key == self::KEY_DESC.$locale) {
                    $localeDetails[self::DESCRIPTIONS][self::FULL] = $value;
                    unset($data[$key]);
                }

                if ($key == self::KEY_SHORT_DESC.$locale) {
                    $localeDetails[self::DESCRIPTIONS][self::SHORT] = substr($value, 0, 255);
                    unset($data[$key]);
                }
            }

            $details['translations'][$locale] = $localeDetails;
        }

        return $details;
    }
}
