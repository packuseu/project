<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Product\Model\ProductAttributeInterface;

class AttributeOptionProcessor implements ResourceProcessorInterface
{
    use AvailableLocalesTrait;

    protected const AVAILABLE_TYPE = 'select';

    /** @var ResourcesCache */
    protected $cache;

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(ResourcesCache $cache, EntityManagerInterface $em)
    {
        $this->cache = $cache;
        $this->em = $em;
    }

    public function process(array $data): void
    {
        $code = $data['code'] ?? '';
        $attributeCode = $data['attribute'] ?? '';

        /** @var ProductAttributeInterface $attribute */
        $attribute = $this->cache->getAttribute($attributeCode);

        if (!$code || !$attribute || $attribute->getType() != self::AVAILABLE_TYPE) {
            throw new ItemIncompleteException('Unable to find a selectable attribute with given code');
        }

        if (array_key_exists($code, $attribute->getConfiguration()['choices'] ?? [])) {
            return;
        }

        $configuration = $attribute->getConfiguration();

        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $configuration['choices'][$code][$locale] = $data['label-' . $locale];
        }

        $attribute->setConfiguration($configuration);

        $this->em->persist($attribute);
    }
}
