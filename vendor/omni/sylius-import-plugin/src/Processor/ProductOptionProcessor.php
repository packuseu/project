<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Processor;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCache;
use Sylius\Component\Product\Model\ProductOptionInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class ProductOptionProcessor implements ResourceProcessorInterface
{
    use AvailableLocalesTrait;

    /** @var FactoryInterface */
    protected $productOptionFactory;

    /** @var ResourcesCache */
    protected $cache;

    /** @var EntityManagerInterface */
    protected $em;

    public function __construct(
        FactoryInterface $productOptionFactory,
        ResourcesCache $cache,
        EntityManagerInterface $em
    ) {
        $this->productOptionFactory = $productOptionFactory;
        $this->cache = $cache;
        $this->em = $em;
    }

    public function process(array $data): void
    {
        $code = $data['code'] ?? '';

        if (!$code) {
            throw new ItemIncompleteException('No code provided');
        }

        $option = $this->getOption($code);

        foreach ($this->getAvailableLocaleCodes($this->cache) as $locale) {
            $label = $data['label-' . $locale] ?? null;

            if (!$label) {
                continue;
            }

            $option->setFallbackLocale($locale);
            $option->setCurrentLocale($locale);
            $option->setName($label);
        }

        $this->em->persist($option);
    }

    private function getOption(string $code): ProductOptionInterface
    {
        $option = $this->cache->getOption($code);

        if (!$option) {
            $option = $this->productOptionFactory->createNew();
            $option->setCode($code);
        }

        return $option;
    }
}
