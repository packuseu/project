<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;
use Knp\Menu\Util\MenuManipulator;

class ContentMenuBuilder
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function configureMenu(MenuBuilderEvent $event): void
    {
        $configurationMenu = $event->getMenu()->getChild('configuration');

        $configurationMenu
            ->addChild('import_jobs', ['route' => 'omni_sylius_admin_import_job_index'])
            ->setLabel('omni_sylius.ui.import_jobs')
            ->setLabelAttribute('icon', 'folder open outline')
        ;

        $configurationMenu
            ->addChild('export_jobs', ['route' => 'omni_sylius_admin_export_job_index'])
            ->setLabel('omni_sylius.ui.export_jobs')
            ->setLabelAttribute('icon', 'folder open outline')
        ;
    }
}
