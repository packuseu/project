<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Model;

class ExportJob implements ExportJobInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $exporter;

    /**
     * @var string
     */
    protected $grid;

    /**
     * @var string|null
     */
    protected $queryString;

    /**
     * @var string|null
     */
    protected $exportFile;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $comments;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $processingStartedAt;

    /**
     * @var \DateTime
     */
    protected $processedAt;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getExporter(): ?string
    {
        return $this->exporter;
    }

    /**
     * @param string $exporter
     */
    public function setExporter(?string $exporter): void
    {
        $this->exporter = $exporter;
    }

    /**
     * @return string
     */
    public function getGrid(): string
    {
        return $this->grid;
    }

    /**
     * @param string $grid
     */
    public function setGrid(?string $grid): void
    {
        $this->grid = $grid;
    }

    /**
     * @return string|null
     */
    public function getQueryString(): ?string
    {
        return $this->queryString;
    }

    /**
     * @param string|null $queryString
     */
    public function setQueryString(?string $queryString): void
    {
        $this->queryString = $queryString;
    }

    /**
     * @return string|null
     */
    public function getExportFile(): ?string
    {
        return $this->exportFile;
    }

    /**
     * @param string|null $exportFile
     */
    public function setExportFile(?string $exportFile): void
    {
        $this->exportFile = $exportFile;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments(?string $comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getProcessingStartedAt(): ?\DateTime
    {
        return $this->processingStartedAt;
    }

    /**
     * @param \DateTime $processingStartedAt
     */
    public function setProcessingStartedAt(?\DateTime $processingStartedAt): void
    {
        $this->processingStartedAt = $processingStartedAt;
    }

    /**
     * @return \DateTime
     */
    public function getProcessedAt(): ?\DateTime
    {
        return $this->processedAt;
    }

    /**
     * @param \DateTime $processedAt
     */
    public function setProcessedAt(?\DateTime $processedAt): void
    {
        $this->processedAt = $processedAt;
    }
}
