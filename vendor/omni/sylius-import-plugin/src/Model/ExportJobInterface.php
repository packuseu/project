<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;

interface ExportJobInterface extends ResourceInterface
{
    const STATE_NEW = 'new';
    const STATE_PROCESSING = 'processing';
    const STATE_DONE = 'done';
    const STATE_ERROR = 'error';

    /**
     * @return string
     */
    public function getExporter(): ?string;

    /**
     * @param string $exporter
     */
    public function setExporter(?string $exporter): void;

    /**
     * @return string
     */
    public function getGrid(): ?string;

    /**
     * @param string $grid
     */
    public function setGrid(?string $grid): void;

    /**
     * @return string
     */
    public function getQueryString(): ?string;

    /**
     * @param string $queryString
     */
    public function setQueryString(?string $queryString): void;

    /**
     * @return string
     */
    public function getExportFile(): ?string;

    /**
     * @param string $exportFile
     */
    public function setExportFile(?string $exportFile): void;

    /**
     * @return string
     */
    public function getStatus(): ?string;

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void;

    /**
     * @return string
     */
    public function getComments(): ?string;

    /**
     * @param string $comments
     */
    public function setComments(?string $comments): void;

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?\DateTime;

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void;

    /**
     * @return \DateTime
     */
    public function getProcessingStartedAt(): ?\DateTime;

    /**
     * @param \DateTime $processingStartedAt
     */
    public function setProcessingStartedAt(?\DateTime $processingStartedAt): void;

    /**
     * @return \DateTime
     */
    public function getProcessedAt(): ?\DateTime;

    /**
     * @param \DateTime $processedAt
     */
    public function setProcessedAt(?\DateTime $processedAt): void;
}
