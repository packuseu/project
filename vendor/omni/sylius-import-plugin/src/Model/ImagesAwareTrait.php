<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types = 1);

namespace Omni\Sylius\ImportPlugin\Model;

use Doctrine\ORM\Mapping as ORM;

trait ImagesAwareTrait
{
    /**
     * @var string
     *
     * @ORM\Column(name="original_path", type="string", nullable=true)
     */
    private $originalPath;

    /**
     * @return string
     */
    public function getOriginalPath(): ?string
    {
        return $this->originalPath;
    }

    /**
     * @param string $originalPath
     */
    public function setOriginalPath(string $originalPath): void
    {
        $this->originalPath = $originalPath;
    }
}
