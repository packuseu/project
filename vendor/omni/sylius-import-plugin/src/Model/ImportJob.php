<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ImportPlugin\Model;

class ImportJob implements ImportJobInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $importer;

    /**
     * @var string
     */
    protected $file;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $comments;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $processingStartedAt;

    /**
     * @var \DateTime
     */
    protected $processedAt;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getImporter(): ?string
    {
        return $this->importer;
    }

    /**
     * @param string $importer
     */
    public function setImporter(?string $importer): void
    {
        $this->importer = $importer;
    }

    /**
     * @return string
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile(?string $file): void
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     */
    public function setComments(?string $comments): void
    {
        $this->comments = $comments;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(?\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getProcessingStartedAt(): ?\DateTime
    {
        return $this->processingStartedAt;
    }

    /**
     * @param \DateTime $processingStartedAt
     */
    public function setProcessingStartedAt(?\DateTime $processingStartedAt): void
    {
        $this->processingStartedAt = $processingStartedAt;
    }

    /**
     * @return \DateTime
     */
    public function getProcessedAt(): ?\DateTime
    {
        return $this->processedAt;
    }

    /**
     * @param \DateTime $processedAt
     */
    public function setProcessedAt(?\DateTime $processedAt): void
    {
        $this->processedAt = $processedAt;
    }
}
