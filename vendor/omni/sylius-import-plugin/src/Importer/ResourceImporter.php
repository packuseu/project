<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\ImportPlugin\Importer;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ManagerRegistry;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ImporterException;
use FriendsOfSylius\SyliusImportExportPlugin\Exception\ItemIncompleteException;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImporterInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImporterResultInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Importer\ImportResultLoggerInterface;
use FriendsOfSylius\SyliusImportExportPlugin\Processor\ResourceProcessorInterface;
use Omni\Sylius\ImportPlugin\Cache\ResourcesCacheInterface;
use Port\Reader\ReaderFactory;
use Omni\Sylius\ImportPlugin\Processor\FullDataProcessorInterface;

class ResourceImporter implements ImporterInterface
{
    /** @var ReaderFactory */
    private $readerFactory;

    /** @var ObjectManager */
    protected $objectManager;

    /** @var ResourceProcessorInterface */
    protected $resourceProcessor;

    /** @var ImportResultLoggerInterface */
    protected $result;

    /** @var ResourcesCacheInterface  */
    protected $cache;

    /** @var ManagerRegistry */
    private $managerRegistry;

    /** @var int */
    protected $batchSize;

    /** @var bool */
    protected $failOnIncomplete;

    /** @var bool */
    protected $stopOnFailure;

    /** @var int */
    protected $batchCount = 0;

    public function __construct(
        ReaderFactory $readerFactory,
        ObjectManager $objectManager,
        ResourceProcessorInterface $resourceProcessor,
        ImportResultLoggerInterface $importerResult,
        ResourcesCacheInterface $cache,
        ManagerRegistry $managerRegistry,
        int $batchSize = 50,
        bool $failOnIncomplete = false,
        bool $stopOnFailure = false
    ) {
        $this->readerFactory = $readerFactory;
        $this->objectManager = $objectManager;
        $this->resourceProcessor = $resourceProcessor;
        $this->result = $importerResult;
        $this->cache = $cache;
        $this->managerRegistry = $managerRegistry;
        $this->batchSize = $batchSize;
        $this->failOnIncomplete = $failOnIncomplete;
        $this->stopOnFailure = $stopOnFailure;
    }

    public function import(string $fileName): ImporterResultInterface
    {
        $reader = $this->readerFactory->getReader(new \SplFileObject($fileName));

        $this->result->start();

        if ($this->resourceProcessor instanceof FullDataProcessorInterface) {
            foreach ($reader as $i => $row) {
                $rows[] = $row;
            }
            $this->resourceProcessor->process($rows);
        } else {
            foreach ($reader as $i => $row) {
                if ($this->importData((int) $i, $row)) {
                    break;
                }
            }
        }

        if ($this->batchCount) {
            try {
                $this->objectManager->flush();
            } catch (\Throwable $exception) {
                $this->handleException($exception);
            }
        }

        $this->result->stop();

        return $this->result;
    }

    public function importData(int $i, array $row): bool
    {
        try {
            $this->resourceProcessor->process($row);
            $this->result->success($i);

            ++$this->batchCount;
            if ($this->batchSize && $this->batchCount === $this->batchSize) {
                $this->objectManager->flush();
                $this->objectManager->clear();
                $this->cache->clear();
                $this->batchCount = 0;
            }
        } catch (ItemIncompleteException $e) {
            $this->handleException($e);

            if ($this->failOnIncomplete) {
                $this->result->failed($i);
                if ($this->stopOnFailure) {
                    return true;
                }
            } else {
                $this->result->skipped($i);
            }
        } catch (ImporterException $e) {
            $this->handleException($e, $i);

            if ($this->stopOnFailure) {
                return true;
            }
        } catch (\Throwable $exception) {
            $this->handleException($exception, $i);
            $this->result->setMessage('Critical failure detected.');

            if ($this->stopOnFailure) {
                return true;
            }
        }

        return false;
    }

    protected function handleException(\Throwable $exception, $iteration = null): void
    {
        if (!$this->objectManager->isOpen()) {
            $this->managerRegistry->resetManager();
            $this->cache->clear();
        }

        if ($iteration) {
            $this->result->failed($iteration);
        }
        $this->result->setMessage($exception->getMessage());
        $this->result->getLogger()->error($exception->getMessage());
    }
}
