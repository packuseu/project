<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types = 1);

namespace Omni\Sylius\ImportPlugin\Form\Type;

use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class ImportJobType extends AbstractResourceType
{
    /**
     * @var array
     */
    protected $additionalImporterTypes = [];

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @inheritDoc
     */
    public function __construct(
        string $dataClass,
        array $validationGroups,
        TranslatorInterface $translator,
        $additionalImporterTypes = [])
    {
        parent::__construct($dataClass, $validationGroups);

        $this->translator = $translator;
        $this->additionalImporterTypes = $additionalImporterTypes;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [
            $this->translator->trans('omni_sylius.importer.types.taxon') => 'taxon',
            $this->translator->trans('omni_sylius.importer.types.product_attribute') => 'product_attribute',
            $this->translator->trans('omni_sylius.importer.types.product_attribute_option') => 'product_attribute_option',
            $this->translator->trans('omni_sylius.importer.types.product_option') => 'product_option',
            $this->translator->trans('omni_sylius.importer.types.product_option_value') => 'product_option_value',
            $this->translator->trans('omni_sylius.importer.types.product') => 'product',
            $this->translator->trans('omni_sylius.importer.types.product_image') => 'product_image',
            $this->translator->trans('omni_sylius.importer.types.product_image_local') => 'product_image_local',
            $this->translator->trans('omni_sylius.importer.types.product_single_file') => 'product_single_file',
            $this->translator->trans('omni_sylius.importer.types.product_taxon') => 'product_taxon',
        ];

        foreach ($this->additionalImporterTypes as $key => $importerType) {
            $choices[$this->translator->trans($importerType['name'] ?? '')] = $key;
        }

        $builder
            ->add('importer', ChoiceType::class, [
                'label' => 'omni_sylius.importer.form.importer',
                'choices' => $choices,
            ])
            ->add('file', FileType::class, [
                'label' => 'omni_sylius.importer.form.file',
                'constraints' => [
                    new File([
                        'maxSize' => '10024k',
                        'mimeTypes' => [
                            'text/csv',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid CSV document',
                    ])
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'omni_sylius_import_job';
    }
}
