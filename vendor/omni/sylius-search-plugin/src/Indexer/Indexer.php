<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\Indexer;

use Doctrine\DBAL\Schema\Index;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;
use Omni\Sylius\SearchPlugin\Model\SearchIndex;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Sylius\Component\Resource\Model\TranslationInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class Indexer
{
    const SPACER = ' ';

    const BATCH_SIZE = 100;

    /**
     * @var array
     */
    private $config;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var PropertyAccessor
     */
    private $accessor;

    /**
     * @param array $config
     * @param EntityManagerInterface $em
     */
    public function __construct(array $config, EntityManagerInterface $em)
    {
        $this->config = $config;
        $this->em = $em;
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * @param OutputInterface $output
     *
     * @return Indexer
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function populate(): void
    {
        $this->output && $this->output->writeln('Resetting index...');

        $connection = $this->em->getConnection();

        $sm = $connection->getSchemaManager();

        $tableIndexes = array_keys($sm->listTableIndexes('omni_search_index'));

        if (in_array('fulltext_search_idx', array_values($tableIndexes))) {
            $sm->dropIndex('fulltext_search_idx', 'omni_search_index');
        }

        $dbPlatform = $connection->getDatabasePlatform();
        $connection->executeUpdate($dbPlatform->getTruncateTableSQL('omni_search_index'));

        foreach ($this->config as $index) {
            $this->createIndex($index['class']);
        }

        $index = new Index('fulltext_search_idx', ['`index`'], false, false, ['fulltext']);
        $sm->createIndex($index, 'omni_search_index');
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return bool
     */
    public function isIndexable(ResourceInterface $resource): bool
    {
        if (false === $resource instanceof TranslatableInterface) {
            return false;
        }

        $class = $this->getClassName($resource);

        foreach ($this->config as $index) {
            if ($index['class'] === $class) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param ResourceInterface $resource
     */
    public function createIndexForResource(ResourceInterface $resource): void
    {
        $metadata = $this->getResourceMetadata($resource);
        $content = $this->compileSearchableContent($metadata['mappings'], $resource);

        if (empty($content)) {
            return;
        }

        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder
            ->select('u')
            ->from(SearchIndex::class, 'u')
            ->where('u.resourceId = :resource_id')
            ->andWhere('u.resourceClass = :resource_namespace')
            ->setParameter(':resource_id', $resource->getId())
            ->setParameter(':resource_namespace', $this->getClassName($resource));
        try {
            $searchIndex = $queryBuilder->getQuery()->getSingleResult();
            $searchIndex->setIndex($content);
        } catch (NoResultException $e) {
            $searchIndex = new SearchIndex();
            $searchIndex->setResourceId($resource->getId());
            $searchIndex->setResourceClass($metadata['class']);
            $searchIndex->setIndex($content);
        }

        $this->em->persist($searchIndex);
    }

    /**
     * @param ResourceInterface $resource
     */
    public function removeIndexForResource(ResourceInterface $resource)
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder
            ->delete(SearchIndex::class, 'u')
            ->where('u.resourceId = :resource_id')
            ->andWhere('u.resourceClass = :resource_namespace')
            ->setParameter(':resource_id', $resource->getId())
            ->setParameter(':resource_namespace', $this->getClassName($resource));

        $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string $resourceClass
     */
    private function createIndex(string $resourceClass): void
    {
        $this->output && $this->output->writeln('Populating index table with data from "' . $resourceClass . '"...');
        $resourceConfig = $this->getResourceMetadata($resourceClass);
        $iteratorBuilder = $this->em->getRepository($resourceClass)->createQueryBuilder('r');

        if (isset($resourceConfig['only_available']) && $resourceConfig['only_available'] === true) {
            $iteratorBuilder
                ->where('r.'.$resourceConfig['available_field_name'].' = true');
        }

        $iterator = $iteratorBuilder->getQuery()->iterate();
        $i = 0;

        foreach ($iterator as $resource) {
            $this->createIndexForResource($resource[0]);

            if (++$i % self::BATCH_SIZE === 0) {
                $this->em->flush();
                $this->em->clear();
            }
        }

        $this->em->flush();
        $this->output && $this->output->writeln('Index created successfully');
    }

    /**
     * @param array             $fields
     * @param ResourceInterface $element
     *
     * @return string
     */
    private function compileSearchableContent(array $fields, ResourceInterface $element)
    {
        $content = '';

        foreach ($fields as $field) {
            if ($element instanceof TranslatableInterface && $this->isTranslatable($element, $field)) {
                $content .= $this->compileTranslationContent($element, $field) . self::SPACER;
            } elseif ($this->accessor->isReadable($element, $field)) {
                $content .= $this->accessor->getValue($element, $field) . self::SPACER;
            }
        }

        return trim($content);
    }

    /**
     * @param TranslatableInterface $element
     * @param string                $field
     *
     * @return string
     */
    private function compileTranslationContent(TranslatableInterface $element, string $field): string
    {
        $content = '';

        foreach ($element->getTranslations() as $translation) {
            $content .= $this->compileSingleTranslationContent($translation, $field);
        }

        return $content;
    }

    /**
     * @param TranslationInterface $translation
     * @param string               $field
     *
     * @return string
     */
    private function compileSingleTranslationContent(TranslationInterface $translation, string $field): string
    {
        if (false === method_exists($translation, 'getSlug') || null === $translation->getSlug()) {
            return '';
        }

        if (false === $this->accessor->isReadable($translation, $field)) {
            isset($this->output) && $this->output->writeln(
                sprintf(
                    "\033[31m Field `%s` cannot be accessed in translation `%s` or its parent resource\033[0m",
                    $field,
                    $this->getClassName($translation)
                )
            );

            return '';
        }

        return $this->accessor->getValue($translation, $field) ?? '';
    }

    /**
     * @param ResourceInterface $resource
     *
     * @return array
     */
    private function getResourceMetadata($resource)
    {
        $class = $this->getClassName($resource);

        foreach ($this->config as $config) {
            if ($config['class'] === $class) {
                return $config;
            }
        }

        throw new \InvalidArgumentException('Class ' . $class . ' is not defined in configuration.');
    }

    /**
     * Get root (not proxy) class name
     * @param object $resource
     * @return string
     */
    private function getClassName($resource): string
    {
        if (is_object($resource)) {
            return $this->em->getClassMetadata(get_class($resource))->rootEntityName;
        } else {
            return $this->em->getClassMetadata($resource)->rootEntityName;
        }
    }

    private function isTranslatable(TranslatableInterface $element, string $field): bool
    {
        $translation = $element->getTranslation();
        $translatable = $this->accessor->isReadable($translation, $field);
        if ($translation instanceof ResourceInterface && null === $translation->getId()) {
            $element->removeTranslation($translation);
        }

        return $translatable;
    }
}
