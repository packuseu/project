<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\Finder;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Omni\Sylius\SearchPlugin\Model\SearchIndexInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\HttpFoundation\Request;

class Finder implements FinderInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var string
     */
    protected $indexClass;

    /**
     * @var int
     */
    protected $countPerPage;

    /**
     * @param array $config
     * @param EntityManager $em
     * @param string $indexClass
     * @param int $countPerPage
     */
    public function __construct(array $config, EntityManager $em, string $indexClass, int $countPerPage)
    {
        $this->config = $config;
        $this->em = $em;
        $this->indexClass = $indexClass;
        $this->countPerPage = $countPerPage;
    }

    /**
     * {@inheritdoc}
     */
    public function find(\Iterator $indexes): array
    {
        $results = [];

        if (empty($indexes)) {
            return $results;
        }

        foreach ($this->config as $resource => $config) {
            $qb = $this->em->getRepository($config['class'])->createQueryBuilder('r');
            $qb
                ->join($this->indexClass, 'i', Join::WITH, 'r.id = i.resourceId')
                ->andWhere('i.resourceClass = :class')
                ->andWhere($qb->expr()->in('i.id', ':indexes'))
                ->setParameter('class', $config['class'])
                ->setParameter('indexes', $this->getCurrentPageIndexIds($indexes));
            $results = array_merge($results, $qb->getQuery()->getResult());

            if (count($results) === $this->countPerPage) {
                break;
            }
        }

        return $results;
    }

    /**
     * {@inheritdoc}
     */
    public function getPager(Request $request): Pagerfanta
    {
        $adapter = new DoctrineORMAdapter(
            $this->em
                ->getRepository($this->indexClass)
                ->createQueryBuilder('i')
                ->where('i.index LIKE :term')
                ->setParameter('term', '%' . $request->get('q') . '%')
        );

        $pager = new Pagerfanta($adapter);
        $pager->setCurrentPage($request->get('page') ?? 1);
        $pager->setMaxPerPage($this->countPerPage);

        return $pager;
    }

    /**
     * @param SearchIndexInterface[]|\Iterator $indexes
     *
     * @return array
     */
    private function getCurrentPageIndexIds(\Iterator $indexes): array
    {
        $ids = [];

        foreach ($indexes as $index) {
            $ids[] = $index->getId();
        }

        return $ids;
    }
}
