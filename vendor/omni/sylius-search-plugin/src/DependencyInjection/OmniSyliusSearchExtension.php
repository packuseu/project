<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\DependencyInjection;

use Sylius\Bundle\ResourceBundle\DependencyInjection\Extension\AbstractResourceExtension;
use Omni\Sylius\SearchPlugin\Extension\Doctrine\MatchAgainstFunction;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class OmniSyliusSearchExtension extends AbstractResourceExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $config = $this->processConfiguration($this->getConfiguration($config, $container), $config);
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $this->registerResources('omni_sylius', $config['driver'], $config['resources'], $container);
        $loader->load('services.yml');
        $container->setParameter('omni_search.serialization_groups', $config['serialization_groups']);
        $container->setParameter('omni_search.config', $config['indexes']);
        $container->setParameter('omni_search.count_per_page', $config['count_per_page']);
        $listener = $container->getDefinition('omni_search.event_listener.resource');

        foreach ($config['indexes'] as $resource => $index) {
            $this->addListenerTags($listener, $resource);
        }
    }

    /**
     * @param Definition $eventListener
     * @param string     $name The name of the resource
     */
    private function addListenerTags(Definition $eventListener, $name)
    {
        $eventListener
            ->addTag(
                'kernel.event_listener',
                ['event' => $name . '.post_create', 'method' => 'createResourceIndex']
            )
            ->addTag(
                'kernel.event_listener',
                ['event' => $name . '.post_update', 'method' => 'createResourceIndex']
            )
            ->addTag(
                'kernel.event_listener',
                ['event' => $name . '.pre_delete', 'method' => 'onRemove']
            )
        ;
    }
}
