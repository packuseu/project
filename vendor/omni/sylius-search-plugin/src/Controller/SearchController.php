<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SearchPlugin\Controller;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use JMS\Serializer\SerializationContext;
use Omni\Sylius\SearchPlugin\Event\SearchPluginEvents;
use Omni\Sylius\SearchPlugin\Event\SearchResultsEvent;
use Omni\Sylius\SearchPlugin\Finder\FinderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class SearchController
{
    /**
     * @var FinderInterface
     */
    protected $finder;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var Environment
     */
    protected $twig;

    public function __construct(FinderInterface $finder, EventDispatcherInterface $dispatcher, Environment $twig)
    {
        $this->finder = $finder;
        $this->dispatcher = $dispatcher;
        $this->twig = $twig;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $request)
    {
        $pager = $this->finder->getPager($request);
        $results = $this->finder->find($pager->getCurrentPageResults());
        $event = new SearchResultsEvent($results);

        $this->dispatcher->dispatch(SearchPluginEvents::SEARCH_RESULTS, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $data = [
            'results' => $results,
            'resultsTotalCount' => $pager->count(),
            'searchTerm' => $request->get('q'),
        ];

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse(
                $this->serializeData($data),
                Response::HTTP_OK,
                [],
                true
            );
        }

        $data['pager'] = $pager;
        $response = new Response();

        $response->setContent($this->twig->render('@OmniSyliusSearchPlugin/search_results.html.twig', $data));

        return $response;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function serializeData(array $data): string
    {
        $serializationContext = SerializationContext::create()->setGroups(
            $this->getParameter('omni_search.serialization_groups')
        );

        return $this->get('jms_serializer')->serialize($data, 'json', $serializationContext);
    }
}
