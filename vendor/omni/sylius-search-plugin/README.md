# OmniSearchPlugin 

## Compatibility

| Sylius   | Symfony | Plugin   |
|----------|---------|----------|
| < `1.6`  |  `~3.4` |  < `1.2` |
|  `^1.6`  |  `~4.4` |   `1.2`  |

## How it works?

This plugin indexes all the pre-configured fields of defined resources to a separate table and provides a full-text
search feature, complete with a search bar and a paginated list of results.

It is important to note that the plugin not only looks for the mapped fields in the resource itself, it also indexes
all the mapped fields of the resource translations. When providing resource for mapping, please be sure to check that 
all the resources implement `TranslatableInterface` and all translations implement `SlugAwareInterface` otherwise,
the plugin will not be able to generate links to associated pages when parsing the results or fail to work at all.

To begin with, use `bin/console omni:search:index` for initial product indexing.

## Installation

### Step 1: add bundle to Kernel

```php
    # config/bundles.php
    <?php
    
    return [
        // ...
        Omni\Sylius\SearchPlugin\OmniSyliusSearchPlugin::class => ['all' => true],
        // ...
    ];
```

###  Step 2: add configuration

In order to use the plugin for searching models, you first need to configure them in `config.yml`, this is how you
can do it:

```yaml
# config/omni_sylius_search.yaml
 
omni_sylius_search:
    indexes:
        sylius.product:
            class: '%sylius.model.product.class%'
            route: sylius_shop_product_show
            mappings: [name, description]
            only_available: true
        omni_sylius.node:
            class: '%omni_sylius.model.node.class%'
            route: omni_sylius_cms_frontend_show
            mappings: [title, content]
            only_available: true

```

> Important! The names of the indexes MUST be the names of the corresponding Sylius resources!

###  Step 3: update product mapping

Use `bin/console doctrine:schema:update -f` or DoctrineMigrations.

###  Step 4: add routing:

```yaml
# config/routing.yml

omni_sylius_search_routes:
  resource: "@OmniSyliusSearchPlugin/Resources/config/routing.yml"
```

###  Step 5: add the search bar:

In order to use the templates you must add a search bar. You can add the search bar wherever you like, as well as 
extend it and customize it, however, in this example we show you how to simply add it in the top menu, right next to
the security items. Extend the `_security.html.twig` and include a single line like so:

```twig

{#app/Resources/SyliusShopBundle/views/Menu/_security.html.twig#}

{% include '@OmniSyliusSearchPlugin/Menu/_security.html.twig' %}

```

### Step 6: initial resource indexing:

```bash
$ bin/console omni:search:index
```

## What's next?

That's it! You can now fully use and exploit the OMNI search plugin, if you want to customise the way it looks you
can extend any of the templates. Please make sure that all the mapped fields of the 
