<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Tests\Functional\Manager;

use Lakion\ApiTestCase\JsonApiTestCase;
use Omni\Sylius\CmsPlugin\Model\Node;

class NodeTypeManagerTest extends JsonApiTestCase
{
    /**
     * @var array
     */
    private $nodeTypes = [
        'content' => 'content',
        'brand' => 'brand',
        'taxon' => 'taxon',
        'placeholder' => 'placeholder',
        'main_menu' => 'main_menu',
        'footer_menu' => 'footer_menu',
    ];

    public function testGetAvailableTypes()
    {
        $this->loadFixturesFromFile('cms.yml');
        $manager = $this->get('omni_sylius.manager.node_type');

        $this->assertEquals($this->nodeTypes, $manager->getAvailableTypes(new Node()));
    }

    public function testGetAvailableTypesWithPersistedSingularNode()
    {
        $this->loadFixturesFromFile('cms.yml');
        $node = new Node();
        $node
            ->setType('main_menu')
            ->setCode('test-code')
            ->setLeft(9)
            ->setRight(10)
            ->setLevel(1);

        $this->getEntityManager()->persist($node);
        $this->getEntityManager()->flush();
        $manager = $this->get('omni_sylius.manager.node_type');
        $types = $this->nodeTypes;
        unset($types['main_menu']);
        $actualNodes = $manager->getAvailableTypes(new Node());

        $this->assertEquals($types, $actualNodes);
    }
}
