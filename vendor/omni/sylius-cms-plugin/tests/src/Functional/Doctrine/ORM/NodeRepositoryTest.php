<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Tests\Functional\Doctrine\ORM;

use Lakion\ApiTestCase\JsonApiTestCase;
use Omni\Sylius\CmsPlugin\Model\Node;

class NodeRepositoryTest extends JsonApiTestCase
{
    public function testFindRootNodes()
    {
        $this->loadFixturesFromFile('cms.yml');
        $repository = $this->getEntityManager()->getRepository(Node::class);

        $nodes = $repository->findRootNodes();

        $this->assertCount(2, $nodes);
        $this->assertEquals(1, $nodes[0]->getId());
    }

    public function testFindOneBySlug()
    {
        $this->loadFixturesFromFile('cms.yml');
        $repository = $this->get('omni_sylius.repository.node');
        $node = $repository->findOneBySlug('foo');

        $this->assertEquals(3, $node->getId());
    }
}
