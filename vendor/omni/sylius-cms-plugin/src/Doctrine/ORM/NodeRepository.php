<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Doctrine\ORM;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use SyliusLabs\AssociationHydrator\AssociationHydrator;

/**
 * @method NodeInterface|null findOneByType(string $type)
 * @method NodeInterface|null findOneByCode(string $type)
 */
class NodeRepository extends NestedTreeRepository implements NodeRepositoryInterface
{
    /** @var AssociationHydrator */
    private $associationHydrator;

    public function __construct(EntityManager $entityManager, ClassMetadata $class)
    {
        parent::__construct($entityManager, $class);

        $this->associationHydrator = new AssociationHydrator($entityManager, $class);
    }

    /**
     * @return NodeInterface[]
     */
    public function findRootNodes(string $positionOrder = self::ORDER_ASCENDING)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.parent IS NULL')
            ->addOrderBy('o.level')
            ->addOrderBy('o.position', $positionOrder)
            ->getQuery()
            ->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBySlug($slug)
    {
        $node = $this->createQueryBuilder('n')
            ->join('n.translations', 't')
            ->andWhere('t.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        return $node;
    }

    /**
     * {@inheritdoc}
     */
    public function createPaginator(array $criteria = [], array $sorting = []): iterable
    {
    }

    /**
     * {@inheritdoc}
     */
    public function add(ResourceInterface $resource): void
    {
        $this->_em->persist($resource);
        $this->_em->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function remove(ResourceInterface $resource): void
    {
        foreach ($resource->getChildren() as $child) {
            $this->remove($child);
        }

        $this->removeFromTree($resource);
    }

    /**
     * @param string $type
     * @param int $levels
     *
     * @return NodeInterface|null
     */
    public function findOneWithChildrenByType(string $type, int $levels = 1): ?NodeInterface
    {
        $node = $this->findOneByType($type);
        if (null === $node) {
            return null;
        }

        $this->hydrateChildren($node, $levels);

        return $node;
    }

    /**
     * @param string $type
     * @param int $levels
     *
     * @return NodeInterface|null
     */
    public function findOneWithChildrenByCode(string $type, int $levels = 1): ?NodeInterface
    {
        $node = $this->findOneByCode($type);
        if (null === $node) {
            return null;
        }

        $this->hydrateChildren($node, $levels);

        return $node;
    }

    private function hydrateChildren(NodeInterface $node, int $levels): void
    {
        $associations = [
            'translations'
        ];
        $prefix = '';
        for ($level = 0; $level < $levels; $level++) {
            $associations[] = $prefix . 'children';
            $associations[] = $prefix . 'children.translations';
            $prefix .= 'children.';
        }

        $this->associationHydrator->hydrateAssociations([$node], $associations);
    }
}
