<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Manager;

use Doctrine\Common\Persistence\ObjectRepository;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;

class NodeTypeManager
{
    /**
     * @var NodeRepositoryInterface|ObjectRepository
     */
    private $nodeRepository;

    /**
     * @var string
     */
    private $defaultTemplate;

    /**
     * @var array
     */
    private $types;

    /**
     * NodeTypeManager constructor.
     *
     * @param NodeRepositoryInterface $nodeRepository
     * @param string $defaultTemplate
     */
    public function __construct(
        NodeRepositoryInterface $nodeRepository,
        string $defaultTemplate
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->defaultTemplate = $defaultTemplate;
    }

    /**
     * Returns unprocessed node type configuration
     *
     * @return array
     */
    public function getTypesConfig(): array
    {
        return $this->types;
    }

    /**
     * @param string $type
     * @param array $options
     */
    public function addType($type, $options): void
    {
        $this->types[$type] = $options;
    }

    /**
     * Removes singular node types from choice list.
     *
     * @param NodeInterface $node
     *
     * @return array
     */
    public function getAvailableTypes(NodeInterface $node): array
    {
        $singularTypes = [];

        foreach ($this->types as $type => $options) {
            if (false === $options['multiple']) {
                $singularTypes[] = $type;
            }
        }

        // TODO: refactor this, there can, potentially, be unlimited number of nodes provided to foreach
        /** @var NodeInterface[] $nodes */
        $nodes = $this->nodeRepository->findBy(['type' => $singularTypes]);
        $availableTypes = $this->types;

        foreach ($nodes as $existingNode) {
            // Keep singular type in list of $node.
            if ($node->getType() !== $existingNode->getType()) {
                unset($availableTypes[$existingNode->getType()]);
            }
        }

        $availableTypes = array_keys($availableTypes);

        return array_combine($availableTypes, $availableTypes);
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function isNestable(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['nestable'];
    }

    /**
     * @param null|string $type
     *
     * @return bool
     */
    public function hasContent(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['content'];
    }

    /**
     * @param null|string $type
     *
     * @return string
     */
    public function getTemplate(?string $type): string
    {
        if (isset($this->types[$type]) && !empty($this->types[$type]['template'])) {
            return $this->types[$type]['template'];
        }

        return $this->defaultTemplate;
    }

    /**
     * @param string $type
     *
     * @return bool
     */
    public function isRelated(?string $type): bool
    {
        return isset($this->types[$type]) && isset($this->types[$type]['relation']);
    }

    /**
     * @param string $type
     *
     * @return array|null
     */
    public function getRelationMetadata(string $type): ?array
    {
        if (false === $this->isRelated($type)) {
            return null;
        }

        return $this->types[$type]['relation'];
    }

    /**
     * @param null|string $type
     *
     * @return bool
     */
    public function isImagesAware(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['images']['enabled'];
    }

    /**
     * @param null|string $type
     *
     * @return bool
     */
    public function isLinkable(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['linkable'];
    }

    /**
     * @param null|string $type
     *
     * @return bool
     */
    public function areImagesLinkable(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['images']['linkable'];
    }

    /**
     * @param null|string $type
     *
     * @return bool
     */
    public function isSeoAware(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['seo_aware'];
    }

    public function isScopable(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['scopable'];
    }

    public function isSlugable(?string $type): bool
    {
        return isset($this->types[$type]) && $this->types[$type]['slugable'];
    }

    /**
     * @param string $type
     *
     * @return array
     */
    public function getImagesConfiguration(string $type): array
    {
        return $this->types[$type]['images'] ?? [];
    }

    /**
     * @return array
     */
    public function getNestableNodeTypes(): array
    {
        $nestable = [];

        foreach ($this->types as $type => $options) {
            if ($this->isNestable($type)) {
                $nestable[] = $type;
            }
        }

        return $nestable;
    }

    /**
     * @param string $type
     *
     * @return string|null
     */
    public function getRelationRoute(string $type): ?string
    {
        return $this->types[$type]['relation_route'] ?? null;
    }

    /**
     * @param string $type
     *
     * @return array
     */
    public function getRelationRouteParameters(string $type): array
    {
        return $this->types[$type]['relation_route_parameters'] ?? [];
    }
}
