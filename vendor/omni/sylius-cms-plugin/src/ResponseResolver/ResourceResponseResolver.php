<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\ResponseResolver;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\UrlGenerator\RelationUrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

final class ResourceResponseResolver implements ResponseResolverInterface
{
    /** @var NodeTypeManager */
    private $typeManager;

    /** @var RelationUrlGeneratorInterface */
    private $urlGenerator;

    /** @var HttpKernelInterface */
    private $httpKernel;

    /**
     * @param NodeTypeManager $typeManager
     * @param RelationUrlGeneratorInterface $urlGenerator
     * @param HttpKernelInterface $httpKernel
     */
    public function __construct(
        NodeTypeManager $typeManager,
        RelationUrlGeneratorInterface $urlGenerator,
        HttpKernelInterface $httpKernel
    ) {
        $this->typeManager = $typeManager;
        $this->urlGenerator = $urlGenerator;
        $this->httpKernel = $httpKernel;
    }

    /**
     * @param Request $request
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function supports(Request $request, NodeInterface $node): bool
    {
        return false === $node->isSlugFromRelation()
            && null !== $node->getRelationType()
            && null !== $this->typeManager->getRelationRoute($node->getType());
    }

    /**
     * @param Request $request
     * @param NodeInterface $node
     *
     * @return Response
     *
     * @throws HttpExceptionInterface
     */
    public function getResponse(Request $request, NodeInterface $node): Response
    {
        $server = $request->server->all();
        $server['REQUEST_METHOD'] = Request::METHOD_GET;

        try {
            $server['REQUEST_URI'] = $this->urlGenerator->generate($node);
        } catch (\Throwable $e) {
            throw new NotFoundHttpException($e->getMessage(), $e);
        }

        $subRequest = new Request(
            $request->query->all(),
            [],
            ['origin_node' => $request->attributes->get('node')],
            $request->cookies->all(),
            [],
            $server
        );
        
        $subRequest->setMethod(Request::METHOD_GET);

        return $this->httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
    }
}
