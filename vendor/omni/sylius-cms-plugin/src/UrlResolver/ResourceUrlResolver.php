<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\UrlResolver;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\UrlGenerator\RelationUrlGeneratorInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

final class ResourceUrlResolver implements UrlResolverInterface
{
    /** @var NodeTypeManager */
    private $typeManager;

    /** @var RelationUrlGeneratorInterface */
    private $urlGenerator;

    /** @var LoggerInterface */
    private $logger;

    /**
     * @param NodeTypeManager $typeManager
     * @param RelationUrlGeneratorInterface $urlGenerator
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        NodeTypeManager $typeManager,
        RelationUrlGeneratorInterface $urlGenerator,
        LoggerInterface $logger = null
    ) {
        $this->typeManager = $typeManager;
        $this->urlGenerator = $urlGenerator;
        $this->logger = $logger ?? new NullLogger();
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function supports(NodeInterface $node): bool
    {
        return $node->isSlugFromRelation()
            && null !== $node->getRelationType()
            && null !== $this->typeManager->getRelationRoute($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return NodeUrl
     */
    public function getNodeUrl(NodeInterface $node): NodeUrl
    {
        try {
            return new NodeUrl($this->urlGenerator->generate($node));
        } catch (\Throwable $e) {
            $this->logger->error(
                'Could not generate url for node',
                [
                    'node' => $node->getId(),
                    'error' => $e->getMessage(),
                    'trace' => $e->getTraceAsString(),
                ]
            );

            return new NodeUrl('');
        }
    }
}
