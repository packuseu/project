<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\UrlResolver;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;

final class LinkUrlResolver implements UrlResolverInterface
{
    /** @var NodeTypeManager */
    private $typeManager;

    /**
     * @param NodeTypeManager $typeManager
     */
    public function __construct(NodeTypeManager $typeManager)
    {
        $this->typeManager = $typeManager;
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function supports(NodeInterface $node): bool
    {
        return $this->typeManager->isLinkable($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return NodeUrl
     */
    public function getNodeUrl(NodeInterface $node): NodeUrl
    {
        return new NodeUrl($node->getLink() ?? '', $node->getLinkTarget());
    }
}
