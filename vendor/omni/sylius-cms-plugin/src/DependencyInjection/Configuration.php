<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\DependencyInjection;

use Omni\Sylius\CmsPlugin\Controller\NodeController;
use Omni\Sylius\CmsPlugin\Doctrine\ORM\NodeRepository;
use Omni\Sylius\CmsPlugin\Form\Type\NodeImageTranslationType;
use Omni\Sylius\CmsPlugin\Form\Type\NodeTranslationType;
use Omni\Sylius\CmsPlugin\Form\Type\NodeType;
use Omni\Sylius\CmsPlugin\Model\Node;
use Omni\Sylius\CmsPlugin\Model\NodeImage;
use Omni\Sylius\CmsPlugin\Model\NodeImageInterface;
use Omni\Sylius\CmsPlugin\Model\NodeImageTranslation;
use Omni\Sylius\CmsPlugin\Model\NodeImageTranslationInterface;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Model\NodeTranslation;
use Omni\Sylius\CmsPlugin\Model\NodeTranslationInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Bundle\ResourceBundle\SyliusResourceBundle;
use Sylius\Component\Resource\Factory\Factory;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('omni_sylius_cms');

        $rootNode
            ->children()
                ->scalarNode('driver')->defaultValue(SyliusResourceBundle::DRIVER_DOCTRINE_ORM)->end()
                ->scalarNode('image_upload_dir')->defaultValue('uploads')->end()
                ->arrayNode('node_types')
                    ->useAttributeAsKey('name')
                        ->prototype('array')
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('relation')->end()
                            ->scalarNode('relation_route')->end()
                            ->arrayNode('relation_route_parameters')
                                ->defaultValue(['slug' => '$relation.slug'])
                                ->scalarPrototype()->end()
                            ->end()
                            ->booleanNode('nestable')->defaultFalse()->end()
                            ->booleanNode('linkable')->defaultFalse()->end()
                            ->booleanNode('slugable')->defaultFalse()->end()
                            ->booleanNode('seo_aware')->defaultFalse()->end()
                            ->booleanNode('multiple')->defaultFalse()->end()
                            ->booleanNode('scopable')->defaultFalse()->end()
                            ->booleanNode('content')->defaultFalse()->end()
                            ->booleanNode('layout')->defaultFalse()->end()
                            ->scalarNode('template')->end()
                            ->arrayNode('images')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->booleanNode('enabled')->defaultFalse()->end()
                                    ->arrayNode('relations')
                                        ->prototype('scalar')->end()
                                    ->end()
                                    ->booleanNode('linkable')
                                        ->defaultFalse()
                                    ->end()
                                    ->arrayNode('available_positions')
                                        ->prototype('scalar')->end()
                                    ->end()
                                    ->booleanNode('title_aware')->defaultFalse()->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        $this->addResourcesSection($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addResourcesSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('resources')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('node_image')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->variableNode('options')->end()
                                ->arrayNode('classes')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('model')->defaultValue(NodeImage::class)->cannotBeEmpty()->end()
                                        ->scalarNode('interface')->defaultValue(NodeImageInterface::class)->cannotBeEmpty()->end()
                                        ->scalarNode('controller')->defaultValue(ResourceController::class)->end()
                                        ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                    ->end()
                                ->end()
                                ->arrayNode('translation')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->arrayNode('classes')
                                            ->addDefaultsIfNotSet()
                                            ->children()
                                                ->scalarNode('model')->defaultValue(NodeImageTranslation::class)->cannotBeEmpty()->end()
                                                ->scalarNode('interface')->defaultValue(NodeImageTranslationInterface::class)->cannotBeEmpty()->end()
                                                ->scalarNode('controller')->defaultValue(ResourceController::class)->cannotBeEmpty()->end()
                                                ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                                ->scalarNode('form')->defaultValue(NodeImageTranslationType::class)->cannotBeEmpty()->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('node')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->variableNode('options')->end()
                                ->arrayNode('classes')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('model')->defaultValue(Node::class)->cannotBeEmpty()->end()
                                        ->scalarNode('interface')->defaultValue(NodeInterface::class)->cannotBeEmpty()->end()
                                        ->scalarNode('controller')->defaultValue(NodeController::class)->cannotBeEmpty()->end()
                                        ->scalarNode('repository')->defaultValue(NodeRepository::class)->cannotBeEmpty()->end()
                                        ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                        ->scalarNode('form')->defaultValue(NodeType::class)->cannotBeEmpty()->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->children()
                                ->arrayNode('translation')
                                ->addDefaultsIfNotSet()
                                ->children()
                                    ->arrayNode('classes')
                                        ->addDefaultsIfNotSet()
                                        ->children()
                                            ->scalarNode('model')->defaultValue(NodeTranslation::class)->cannotBeEmpty()->end()
                                            ->scalarNode('interface')->defaultValue(NodeTranslationInterface::class)->cannotBeEmpty()->end()
                                            ->scalarNode('controller')->defaultValue(ResourceController::class)->cannotBeEmpty()->end()
                                            ->scalarNode('repository')->cannotBeEmpty()->end()
                                            ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                            ->scalarNode('form')->defaultValue(NodeTranslationType::class)->cannotBeEmpty()->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
