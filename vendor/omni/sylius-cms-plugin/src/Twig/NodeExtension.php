<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Twig;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Manager\RelationManager;
use Omni\Sylius\CmsPlugin\Model\NodeImageInterface;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Model\RelatableInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;
use Omni\Sylius\CmsPlugin\UrlResolver\NodeUrl;
use Omni\Sylius\CmsPlugin\UrlResolver\UrlResolverInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class xNodeExtension extends AbstractExtension
{
    /** @var NodeRepositoryInterface */
    private $nodeRepository;

    /** @var NodeTypeManager */
    private $typeManager;

    /** @var RelationManager */
    private $relationManager;

    /** @var UrlResolverInterface */
    private $urlResolver;

    /** @var RequestStack */
    private $requestStack;

    /**
     * @param NodeRepositoryInterface $nodeRepository
     * @param NodeTypeManager $typeManager
     * @param RelationManager $relationManager
     * @param UrlResolverInterface $resolver
     * @param RequestStack $requestStack
     */
    public function __construct(
        NodeRepositoryInterface $nodeRepository,
        NodeTypeManager $typeManager,
        RelationManager $relationManager,
        UrlResolverInterface $resolver,
        RequestStack $requestStack
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->typeManager = $typeManager;
        $this->relationManager = $relationManager;
        $this->urlResolver = $resolver;
        $this->requestStack = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'omni_sylius_cms_render_node',
                [$this, 'renderNode'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
            new TwigFunction('omni_sylius_is_node_nestable', [$this, 'isNodeNestable']),
            new TwigFunction('omni_sylius_is_node_linkable', [$this, 'isNodeLinkable']),
            new TwigFunction('omni_sylius_is_node_related', [$this, 'isNodeRelated']),
            new TwigFunction('omni_sylius_is_node_scopable', [$this, 'isNodeScopable']),
            new TwigFunction('omni_sylius_is_node_image_aware', [$this, 'isNodeImageAware']),
            new TwigFunction('omni_sylius_get_node_image_config', [$this, 'getNodeImageConfig']),
            new TwigFunction('omni_sylius_get_node_by_type', [$this, 'getNodeByType']),
            new TwigFunction('omni_sylius_get_node_with_children_by_type', [$this, 'getNodeWithChildrenByType']),
            new TwigFunction('omni_sylius_get_node_by_code', [$this, 'getNodeByCode']),
            new TwigFunction('omni_sylius_get_node_with_children_by_code', [$this, 'getNodeWithChildrenByCode']),
            new TwigFunction('omni_sylius_get_relation', [$this, 'getRelation']),
            new TwigFunction('omni_sylius_get_relation_label', [$this, 'getRelationLabel']),
            new TwigFunction('omni_sylius_instantiate_node_relations', [$this, 'instantiateNodeRelations']),
            new TwigFunction('omni_sylius_get_node_image_positions', [$this, 'getAvailableImagePositions']),
            new TwigFunction('omni_sylius_get_node_url', [$this, 'getNodeUrl']),
            new TwigFunction('omni_sylius_get_breadcrumbs', [$this, 'getBreadcrumbs']),
            new TwigFunction('omni_sylius_get_request_node', [$this, 'getNodeFromRequest']),
        ];
    }

    /**
     * @param Environment $twig
     * @param NodeInterface $node
     *
     * @return string
     *
     * @throws LoaderError
     * @throws SyntaxError
     * @throws RuntimeError
     */
    public function renderNode(Environment $twig, NodeInterface $node): string
    {
        return $twig->render($this->typeManager->getTemplate($node->getType()), ['node' => $node]);
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function isNodeNestable(NodeInterface $node): bool
    {
        return $this->typeManager->isNestable($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function isNodeLinkable(NodeInterface $node): bool
    {
        return $this->typeManager->isLinkable($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function isNodeRelated(NodeInterface $node): bool
    {
        return $this->typeManager->isRelated($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function isNodeScopable(NodeInterface $node): bool
    {
        return $this->typeManager->isScopable($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return bool
     */
    public function isNodeImageAware(NodeInterface $node): bool
    {
        return $this->typeManager->isImagesAware($node->getType());
    }

    /**
     * @param NodeInterface $node
     *
     * @return array|null
     */
    public function getNodeImageConfig(NodeInterface $node): ?array
    {
        return $this->typeManager->getImagesConfiguration($node->getType());
    }

    /**
     * @param string $type
     *
     * @return NodeInterface|null
     */
    public function getNodeByType(string $type): ?NodeInterface
    {
        return $this->nodeRepository->findOneByType($type);
    }

    /**
     * @param string $type
     * @param int $levels
     *
     * @return NodeInterface|null
     */
    public function getNodeWithChildrenByType(string $type, int $levels = 1): ?NodeInterface
    {
        return $this->nodeRepository->findOneWithChildrenByType($type, $levels);
    }

    /**
     * @param string $code
     *
     * @return NodeInterface|null
     */
    public function getNodeByCode(string $code): ?NodeInterface
    {
        return $this->nodeRepository->findOneBy(['code' => $code]);
    }

    /**
     * @param string $code
     * @param int $levels
     *
     * @return NodeInterface|null
     */
    public function getNodeWithChildrenByCode(string $code, int $levels = 1): ?NodeInterface
    {
        return $this->nodeRepository->findOneWithChildrenByCode($code, $levels);
    }

    /**
     * @param RelatableInterface $relatable
     *
     * @return null|ResourceInterface
     */
    public function getRelation(RelatableInterface $relatable): ?ResourceInterface
    {
        if (!$relatable->getRelation()) {
            $this->relationManager->getRelation($relatable);
        }

        return $relatable->getRelation();
    }

    /**
     * @param NodeInterface $node
     */
    public function instantiateNodeRelations(?NodeInterface $node): void
    {
        if (!$node) {
            return;
        }

        $this->relationManager->instantiateRelations($node);
    }

    /**
     * @param TranslatableInterface $relation
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getRelationLabel(TranslatableInterface $relation): string
    {
        return $this->relationManager->getRelationLabel($relation);
    }

    /**
     * @param NodeInterface $node
     *
     * @return array
     */
    public function getAvailableImagePositions(NodeInterface $node): array
    {
        $positions = [];

        /** @var NodeImageInterface $image */
        foreach ($node->getImages() as $image) {
            if (null !== $image->getPosition() && false === \in_array($image->getPosition(), $positions, true)) {
                $positions[] = $image->getPosition();
            }
        }

        return $positions;
    }

    /**
     * @param NodeInterface $node
     *
     * @return NodeUrl
     */
    public function getNodeUrl(NodeInterface $node): NodeUrl
    {
        return $this->urlResolver->getNodeUrl($node);
    }

    /**
     * @param NodeInterface|null $node
     *
     * @return array
     */
    public function getBreadcrumbs(NodeInterface $node = null): array
    {
        if (null === $node) {
            $node = $this->getNodeFromRequest();
        }

        $breadcrumbs = [];
        while ($node && $node->getParent()) {
            $breadcrumbs[] = ['node' => $node, 'current' => false];
            $node = $node->getParent();
        }
        if (isset($breadcrumbs[0])) {
            $breadcrumbs[0]['current'] = true;
        }

        return array_reverse($breadcrumbs);
    }

    /**
     * @return NodeInterface|null
     */
    public function getNodeFromRequest(): ?NodeInterface
    {
        $request = $this->requestStack->getCurrentRequest();
        if (null === $request) {
            return null;
        }

        $node = $request->attributes->get('node');
        if ($node instanceof NodeInterface) {
            return $node;
        }

        $masterRequest = $this->requestStack->getMasterRequest();
        if (null === $masterRequest) {
            return null;
        }

        $node = $masterRequest->attributes->get('node');

        return $node instanceof NodeInterface ? $node : null;
    }
}
