<?php

namespace Omni\Sylius\CmsPlugin\Factory;

use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

interface NodeFactoryInterface extends FactoryInterface
{
    /**
     * @param NodeInterface $parent
     *
     * @return NodeInterface
     */
    public function createForParent(NodeInterface $parent);
}
