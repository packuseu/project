<?php

namespace Omni\Sylius\CmsPlugin\Factory;

use Omni\Sylius\CmsPlugin\Model\Node;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class NodeFactory implements NodeFactoryInterface
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    /**
     * {@inheritdoc}
     */
    public function createNew()
    {
        return $this->factory->createNew();
    }

    /**
     * {@inheritdoc}
     */
    public function createForParent(NodeInterface $parent)
    {
        /** @var Node $node */
        $node = $this->createNew();
        $node->setParent($parent);

        return $node;
    }
}
