<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types = 1);

namespace Omni\Sylius\CmsPlugin\Controller;

use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\File;

class AjaxController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function resourceAutocompleteAction(Request $request): Response
    {
        $resourceType = $request->query->get('type');
        $repository = $this->get('omni_sylius.manager.relation')->getRepository($resourceType);

        if (!$repository) {
            throw new \Exception('Invalid resource type specified');
        }

        $results = $repository->createQueryBuilder('o')
            ->where('o.code LIKE :phrase')
            ->setParameter('phrase', '%' . $request->query->get('phrase') . '%')
            ->setMaxResults(10)
            ->getQuery();

        $view = View::create($results->getResult());
        $metadata = $this->get('sylius.resource_registry')->get($resourceType);
        $configuration = $this->get('sylius.resource_controller.request_configuration_factory')->create(
            $metadata,
            $request
        );

        return $this->get('sylius.resource_controller.view_handler')->handle($configuration, $view);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function resourceAutocompleteByIdAction(Request $request): Response
    {
        $resourceType = $request->query->get('type');
        $repository = $this->get('omni_sylius.manager.relation')->getRepository($resourceType);

        if (!$repository) {
            throw new \Exception('Invalid resource type specified');
        }

        $view = View::create([$repository->find($request->query->get('id')[0] ?? null)]);
        $metadata = $this->get('sylius.resource_registry')->get($resourceType);
        $configuration = $this->get('sylius.resource_controller.request_configuration_factory')->create(
            $metadata,
            $request
        );

        return $this->get('sylius.resource_controller.view_handler')->handle($configuration, $view);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadImageAction(Request $request): Response
    {
        $file = $request->files->get('upload');
        $destinationDir = $this->container->getParameter('omni_cms.image_upload_dir');
        $rootDir = $this->container->getParameter('kernel.project_dir');
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        $violations = $this->get('validator')->validate(
            $file,
            new File([
                'maxSize' => '6M',
                'mimeTypes' => ['image/*']
            ])
        );

        if ($violations->count() > 0) {
            $violationArray = [];

            foreach ($violations as $violation) {
                $violationArray[] = $violation->getMessage();
            }

            return new JsonResponse(
                [
                    'uploaded' => 0,
                    'error' => $violationArray
                ]
            );
        }

        // this is needed to safely include the file name as part of the URL
        $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
        $newFilename = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();
        $fullDestinationDir = is_dir("$rootDir/web") ? "$rootDir/web/$destinationDir" : "$rootDir/public/$destinationDir";

        if (!is_dir($fullDestinationDir)) {
            mkdir($fullDestinationDir, 0755, true);
        }

        $file->move($fullDestinationDir, $newFilename);

        return new JsonResponse(
            [
                'uploaded' => 1,
                'fileName' => $newFilename,
                'url' => "/$destinationDir/$newFilename"
            ]
        );
    }
}
