<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Controller;

use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Omni\Sylius\CmsPlugin\Repository\NodeRepositoryInterface;
use Omni\Sylius\CmsPlugin\ResponseResolver\ResponseResolverInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ShowNodeAction
{
    /** @var NodeRepositoryInterface */
    private $nodeRepository;

    /** @var ResponseResolverInterface */
    private $responseResolver;

    /**
     * @param NodeRepositoryInterface $nodeRepository
     * @param ResponseResolverInterface $responseResolver
     */
    public function __construct(NodeRepositoryInterface $nodeRepository, ResponseResolverInterface $responseResolver)
    {
        $this->nodeRepository = $nodeRepository;
        $this->responseResolver = $responseResolver;
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws HttpExceptionInterface
     */
    public function __invoke(Request $request): Response
    {
        /** @var NodeInterface $node */
        $node = $this->nodeRepository->findOneBySlug($request->attributes->get('slug'));
        if (false === $node instanceof NodeInterface || false === $node->isEnabled()) {
            throw new NotFoundHttpException();
        }

        $request->attributes->set('node', $node);

        return $this->responseResolver->getResponse($request, $node);
    }
}
