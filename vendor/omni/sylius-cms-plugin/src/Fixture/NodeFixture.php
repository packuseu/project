<?php

namespace Omni\Sylius\CmsPlugin\Fixture;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\CmsPlugin\Factory\NodeFactoryInterface;
use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Sylius\Bundle\FixturesBundle\Fixture\AbstractFixture;
use Sylius\Bundle\FixturesBundle\Fixture\FixtureInterface;
use Sylius\Component\Core\Model\TaxonInterface;
use Sylius\Component\Locale\Context\LocaleContextInterface;
use Sylius\Component\Taxonomy\Repository\TaxonRepositoryInterface;

class NodeFixture extends AbstractFixture implements FixtureInterface
{
    const TAXON_NODE_COUNT = 4;

    const FOOTER_NODES = [
        'your_store' => [
            'about', 'terms_and_conditions', 'privacy_policy', 'contact',
        ],
        'customer_care' => [
            'faq\'s', 'delivery_and_shipping', 'Returns_policy',
        ],
        'information' => [
            'suppliers', 'history', 'More',
        ],
    ];

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var NodeFactoryInterface
     */
    protected $nodeFactory;

    /**
     * @var TaxonRepositoryInterface
     */
    protected $taxonRepository;

    /**
     * @var NodeTypeManager
     */
    protected $handler;

    /**
     * @var LocaleContextInterface
     */
    protected $localeContext;

    /**
     * @param EntityManagerInterface $em
     * @param NodeFactoryInterface $nodeFactory
     * @param TaxonRepositoryInterface $taxonRepository
     * @param NodeTypeManager $handler
     * @param LocaleContextInterface $localeContext
     */
    public function __construct(
        EntityManagerInterface $em,
        NodeFactoryInterface $nodeFactory,
        TaxonRepositoryInterface $taxonRepository,
        NodeTypeManager $handler,
        LocaleContextInterface $localeContext
    ) {
        $this->em = $em;
        $this->nodeFactory = $nodeFactory;
        $this->taxonRepository = $taxonRepository;
        $this->handler = $handler;
        $this->localeContext = $localeContext;
    }

    /**
     * @param array $options
     */
    public function load(array $options): void
    {
        $mainMenuNode = $this->getTopLevelNode('main_menu');
        $footerMenuNode = $this->getTopLevelNode('footer_menu');

        $this->loadTaxonNodes($mainMenuNode);
        $this->loadFooterNodes($footerMenuNode);

        $this->em->persist($mainMenuNode);
        $this->em->persist($footerMenuNode);
        $this->em->flush();
    }

    public function getName(): string
    {
        return 'omni_node';
    }

    protected function getTopLevelNode(string $type): NodeInterface
    {
        /** @var NodeInterface $node */
        $node = $this->nodeFactory->createNew();
        $node->setCurrentLocale($this->localeContext->getLocaleCode());
        $node
            ->setType($type)
            ->setCode($type)
        ;

        $node->getTranslation()->setTitle($this->getFormatedTitle($type));

        return $node;
    }

    protected function loadTaxonNodes(NodeInterface $parent): void
    {
        $count = 0;

        /** @var TaxonInterface $taxon */
        foreach ($this->taxonRepository->findAll() as $taxon) {
            if (null !== $taxon->getParent() && $count++ < self::TAXON_NODE_COUNT) {
                $node = $this
                    ->createNodeForParent($parent, $taxon->getCode(), $taxon->getName(), 'taxon')
                    ->setRelationId($taxon->getId())
                    ->setRelationType($this->handler->getRelationMetadata('taxon')['alias'])
                ;

                $this->em->persist($node);
            }
        }
    }

    protected function loadFooterNodes(NodeInterface $parent): void
    {
        foreach (self::FOOTER_NODES as $code => $contents) {
            $placeholder = $this->createNodeForParent($parent, $code, $this->getFormatedTitle($code), 'placeholder');

            foreach ($contents as $contentCode) {
                $this->createContentNode($placeholder, $contentCode);
            }

            $this->em->persist($placeholder);
        }
    }

    protected function createContentNode(NodeInterface $parent, string $code): void
    {
        $node = $this->createNodeForParent($parent, $code, $this->getFormatedTitle($code), 'content');
        $node->getTranslation()->setSlug($code);
        $node->getTranslation()->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit.');

        $this->em->persist($node);
    }

    /**
     * @param NodeInterface $parent
     * @param string $code
     * @param string $title
     * @param string $type
     *
     * @return NodeInterface
     */
    protected function createNodeForParent(NodeInterface $parent, string $code, string $title, string $type): NodeInterface
    {
        $node = $this->nodeFactory->createForParent($parent);
        $node->setCurrentLocale($this->localeContext->getLocaleCode());
        $node->setCode($code);
        $node->getTranslation()->setTitle($title);
        $node->setType($type);

        return $node;
    }

    protected function getFormatedTitle(string $type): string
    {
        return ucfirst(str_replace('_', ' ', $type));
    }
}
