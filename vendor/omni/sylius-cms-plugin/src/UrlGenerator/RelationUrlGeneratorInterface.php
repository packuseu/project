<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\UrlGenerator;

use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

interface RelationUrlGeneratorInterface
{
    /**
     * @param NodeInterface $node
     * @param int $referenceType
     *
     * @return string
     */
    public function generate(NodeInterface $node, int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string;
}
