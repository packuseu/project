<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;

trait RelatableTrait
{
    /**
     * @var string
     */
    protected $relationType;

    /**
     * @var int
     */
    protected $relationId;

    /**
     * @var ResourceInterface
     */
    protected $relation;

    /**
     * {@inheritdoc}
     */
    public function getRelationType()
    {
        return $this->relationType;
    }

    /**
     * {@inheritdoc}
     */
    public function setRelationType($relationType)
    {
        $this->relationType = $relationType;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRelationId()
    {
        return $this->relationId;
    }

    /**
     * {@inheritdoc}
     */
    public function setRelationId($relationId)
    {
        $this->relationId = $relationId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRelation(): ?ResourceInterface
    {
        return $this->relation;
    }

    /**
     * {@inheritdoc}
     */
    public function setRelation(?ResourceInterface $relation)
    {
        $this->relation = $relation;

        return $this;
    }
}
