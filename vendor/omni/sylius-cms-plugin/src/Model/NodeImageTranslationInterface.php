<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslationInterface;

interface NodeImageTranslationInterface extends ResourceInterface, TranslationInterface
{
    /**
     * @return string
     */
    public function getTitle(): ?string;

    /**
     * @param null|string $title
     *
     * @return NodeImageTranslationInterface
     */
    public function setTitle(?string $title): NodeImageTranslationInterface;

    /**
     * @return null|string
     */
    public function getLink(): ?string;

    /**
     * @param null|string $link
     *
     * @return NodeImageTranslationInterface
     */
    public function setLink(?string $link): NodeImageTranslationInterface;
}
