<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddTypeChoiceSubscriber implements EventSubscriberInterface
{
    /**
     * @var NodeTypeManager
     */
    private $nodeTypeManager;

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    /**
     * AddTypeChoiceSubscriber constructor.
     *
     * @param NodeTypeManager $nodeTypeManager
     */
    public function __construct(NodeTypeManager $nodeTypeManager)
    {
        $this->nodeTypeManager = $nodeTypeManager;
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        if (null === $node = $event->getData()) {
            return;
        }

        $event->getForm()->add('type', ChoiceType::class, [
            'label' => 'omni_sylius.form.node.type',
            'choices' => $this->nodeTypeManager->getAvailableTypes($node),
        ]);
    }
}
