<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\CmsPlugin\Form\Subscriber;

use Omni\Sylius\CmsPlugin\Form\Type\NodeType;
use Omni\Sylius\CmsPlugin\Manager\NodeTypeManager;
use Omni\Sylius\CmsPlugin\Model\NodeInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class AddLinkToImageTranslationSubscriber implements EventSubscriberInterface
{
    /** @var NodeTypeManager */
    protected $handler;

    /**
     * @param NodeTypeManager $handler
     */
    public function __construct(NodeTypeManager $handler)
    {
        $this->handler = $handler;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::POST_SET_DATA => 'postSetData',
        ];
    }

    /**
     * @param FormEvent $event
     */
    public function postSetData(FormEvent $event): void
    {
        $dataForm = $event->getForm();
        while ($dataForm && false === $dataForm->getConfig()->getType()->getInnerType() instanceof NodeType) {
            $dataForm = $dataForm->getParent();
        }

        if (null === $dataForm) {
            throw new \RuntimeException('Must be used on type that has node as one of its parents');
        }

        $data = $dataForm->getData();

        if ($data instanceof NodeInterface && $this->handler->areImagesLinkable($data->getType())) {
            $event->getForm()->add(
                'link',
                TextType::class,
                [
                    'label' => 'omni_sylius.form.node.link',
                ]
            );
        }
    }
}
