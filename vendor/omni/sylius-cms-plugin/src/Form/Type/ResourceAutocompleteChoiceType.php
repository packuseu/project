<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CmsPlugin\Form\Type;

use Omni\Sylius\CmsPlugin\Form\DataTransformer\MultiResourceToIdentifierTransformer;
use Omni\Sylius\CmsPlugin\Manager\RelationManager;
use Omni\Sylius\CmsPlugin\Model\NodeImageInterface;
use Sylius\Bundle\ResourceBundle\Form\DataTransformer\CollectionToStringTransformer;
use Sylius\Bundle\ResourceBundle\Form\DataTransformer\RecursiveTransformer;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceAutocompleteChoiceType as BaseType;
use Sylius\Component\Registry\ServiceRegistryInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Needed to implement custom data transformer
 */
class ResourceAutocompleteChoiceType extends BaseType
{
    /**
     * @var RelationManager
     */
    private $relationManager;

    /**
     * @param ServiceRegistryInterface $resourceRepositoryRegistry
     * @param RelationManager          $relationManager
     */
    public function __construct(ServiceRegistryInterface $resourceRepositoryRegistry, RelationManager $relationManager)
    {
        parent::__construct($resourceRepositoryRegistry);

        $this->relationManager = $relationManager;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if (!$options['multiple']) {
            $builder->addModelTransformer(
                new MultiResourceToIdentifierTransformer(
                    $options['repository'],
                    $options['choice_value']
                )
            );
        }

        if ($options['multiple']) {
            $builder
                ->addModelTransformer(
                    new RecursiveTransformer(
                        new MultiResourceToIdentifierTransformer(
                            $options['repository'],
                            $options['choice_value']
                        )
                    )
                )
                ->addViewTransformer(new CollectionToStringTransformer(','))
            ;
        }

        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'preSubmit']);
    }

    /**
     * @param FormEvent $event
     */
    public function preSubmit(FormEvent $event): void
    {
        $image = $event->getForm()->getParent()->getData();

        if (!$image || !$image instanceof NodeImageInterface || !$image->getRelationType()) {
            return;
        }

        $repository = $this->relationManager->getRepository($image->getRelationType());

        foreach ($event->getForm()->getConfig()->getModelTransformers() as $transformer) {
            if (method_exists($transformer, 'setRepository')) {
                $transformer->setRepository($repository);
            }
        }
    }
}
