Omni Sylius Banner Plugin
------------

This is a Sylius plugin that enables rendering and management of banners. 
There is a user interface added to the admin panel through which one can create 
banner zones and positions that contain different configurations of banners. There 
is a set of twig helper functions to help display these banners in the 
front end of your Sylius e-commerce site. An example of such a banners is
shown bellow:

![banner top](docs/img/banner_homepage.png "Header banner")

## Installation

Require it via composer:

```bash
$ composer require omni/sylius-banner-plugin
```

Import banner routes:
 
```
omni_sylius_banner:
  resource: "@OmniSyliusBannerPlugin/Resources/config/admin_routing.yml"
  prefix: /admin
```

Import banner configs:

```
# app/config.yml
imports:
    - { resource: "@OmniSyliusBannerPlugin/Resources/config/app/config.yml" }
    # optional
    - { resource: "@OmniSyliusBannerPlugin/Resources/config/app/fixtures.yml" }
    
liip_imagine:
    filter_sets:
        omni_sylius_banner: ~
```

> please note that if you add default fixtures then make sure that you import them 
after the base fixtures so you would have your loacales and channels loaded

Start using the plugin

## Usage

### Understanding the models

If you would pay a closer attention to the models presented in this plugin, 
you would notice that there are quite a few of them and its essential to have
a firm understanding of them before you start using the plugin. In short, you have
these models available to you once you start using the plugin:

* Zone - a zone is essentially a model that represents a certain place in your 
applications front-end (e.g. a header zone or a product sidebar zone). It does
little more then contain a set of positions for banners. Zones are designed to be
static, that is, you should consider how many zones you will need in the development
stage of your application, create the corresponding zones, add them to your twig 
templates and then leave them there for the remainder of the lifetime of your application.
* Position - is a model that is assigned to the zones. Every zone can have multiple
positions and every position can have only a single zone. The responsibilities of the 
position are to contain a set of banners and rules on how those banners will be displayed.
The rules on the display of the banners depend on the position type (more on that in
the configuration section). 
* Banner - is a model that holds the general business information that is needed
to form the banner. It holds the dates from and up to when it should be displayed,
channels where it should be displayed and finally a set of images that will be displayed.
* Banner images - essentially, banner itself does not contain any information that you 
see in the frontend of your application. This information is preserved for banner
images. Every image must be assigned to a single banner and can hold the information
such as the image itself, color, percentage and the amount of overlay content, content
itself, links and more.

As you can see one zone can contain multiple positions, each of which can contain
multiple banners, each of which can contain multiple images. Each position is responsible
for determinig how its banners will be displayed and you can completely change the way
your banner looks simply by assigning it to a different position. This will change both
the position of the banner in the page and the manner in which it is displayed. Read the
next section to find out more about it.

### Position Configuration

Beside the usual possibility to override the classes used for resources you can add 
such configuration:

```yaml

omni_sylius_banner:
    position_types:
        big_wide_slider:
            label: omni_sylius.banner.position.big_wide_slider
            twig_template: '@OmniSyliusBannerPlugin/PositionTypes/_big_wide_slider.html.twig'
            example_image: 'bundles/omnisyliusbannerplugin/img/position_examples/wide_slider_example.png'
            options:
                descriptive: true
        unified_descriptive_grid:
            label: omni_sylius.banner.position.unified_descriptive_grid
            twig_template: '@OmniSyliusBannerPlugin/PositionTypes/_unified_descriptive_grid.html.twig'
            example_image: 'bundles/omnisyliusbannerplugin/img/position_examples/unified_descriptive_grid_example.png'
            options:
                descriptive: true
                background_color: '#EE2E4F'
```

With the configuration above you would add two position types. Both of them are configured
in the default configuration of the bundle (`@OmniSyliusBannerPlugin/Resources/config/config.yml`)
However if you wish you can override them or add your own ones. Every position type is determined
by its name (`big_wide_slider` in the example above) and it must have an example image and a 
template provided. Example image is used in the admin panel so that the administrators would
know what to expect from the banners when they assign a certain position type (see image below)
and the template is actually used by the plugin to display the banners. This template is the 
core of the application logic - change it and the display of your banners will differ.

In addition to that you can provide various options to your position type, they can be whatever
you need and will be passed to your template (you will be able to access them through an `options` 
parameter).

Here is how a type selection of the position looks like once you have a few of them configured:

![position edit](docs/img/position_edit.png "Position edit")

As you can see, you must select a type that is displayed with an example image.

> Please note, that the plugin comes with a set of preconfigured templates example
images and fixtures for positions and banners that suite them. However they will
only be displayed in the way they are depicted in example images if you are using
`OmniSyliusReusableTheme` if you are not, give it a go, it's worth it. If, however,
you will not be using the reusable theme, we kindely invite you to write your own
templates and make your own example images that suite the front end of yur application. 

### Creating banners from scratch

Before you start adding your banners to the front end of the application,
you first need to create some. For that please go to the admin panel of your 
application. There, in the marketing section of the left sidebar you should
find a link called `Banners`. If you click on it you should see a list of banner
zones like the one depicted in the image here:

![banner zones](docs/img/zones.png "Banner zones")

If the list is empty, just create a new zone and carry on.

Afterwards you need to create at least one banner position. Click show
positions on your newly created zone and add a new position. After that is 
done, click `show banners` on your position. If this is not a new position,
you should see a list of all of it's banners, lite the one depicted bellow, 
otherwise the list will be empty.

![banners](docs/img/banner_list.png "Banners")

Click create new banner and add your new banner:

![banner form](docs/img/banner_create.png "Banner form")

Here you can see that it is possible to add the publishing times and assign required
channels. However, if you want your banner to actually do anything, then press
the add button (marked red). This allows you to add a new image as depicted here:

![banner image form](docs/img/banner_edit.png "Banner image form")

As you can see, the image itself holds a lot of information. The takeaway here is 
that different position type templates may use different information from here and
while some templates may be very concerned with how to form the background and the
content of the image, others may completely ignore this.

### Banner rendering

Now that we have some banners added, it would be nice to actually display some of them
to the frontend of the application. It can be done fairly simply - decide the place that
you want the banners displayed and add this line to the twig template:

```twig
{{ omni_sylius_banner_zone('index') }}
```

Here we assume that the zone that you just created with all the positions and banners
inside of it is called Index and it has the code `index`.

If you need, you can pass a second parameter (array) as options. If you will, it
will be merged to the options of the corresponding position type of your banners and
passed to the templates that will render them. It can also override the options from
your position type configuration. The end result is the display of the banners in your
site:

![banners front](docs/img/banner_homepage_slider.png "Banner home slider")

> Analogously you can add all your zones to the pages of your site and just leave them
there. After that you can move your positions and banners from one place to the next 
simply by changing the zone to which certain position is assigned to.

### Banner preview

It is often the case that your administrators will create banners that will only
be published someday in the future and they will want to see how these banners will
look like without waiting for the due date. 

If you want to see how your site will look like at a certain date just go to your
site with a `preview_date` GET parameter and you will be able to preview the banners
that will only be published at a certain date in the future:

```
www.example.com?preview_date=2025-11-06
```  

It's as simple as that.

### Twig functions

Here is a full list of twig functions available with the bundle:
* `omni_sylius_banner_zone(code, options)` - displays all the banners in the given
zone.
* `omni_sylius_banner_position(code, options)` - displays all the banners in the given
position. Usage is discouraged for the loss of flexibility
* `omni_sylius_banner_display(code, options)` - displays the images of a single banner.
Must be noted, that all the power that is provided by the different banner display of 
position types is lost when using this function. Usage is discouraged for a great loss of 
flexibility.
* `omni_sylius_banner_position_type_config()` - returns the full position types 
configuration that you provided in the `config.yml`.
