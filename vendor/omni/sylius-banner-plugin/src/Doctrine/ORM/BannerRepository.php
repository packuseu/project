<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Doctrine\ORM;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Omni\Sylius\BannerPlugin\Model\Banner;
use Omni\Sylius\BannerPlugin\Model\BannerPositionInterface;
use Omni\Sylius\BannerPlugin\Model\BannerZone;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Channel\Model\ChannelInterface;

/**
 * Class BannerRepository.
 */
class BannerRepository extends EntityRepository
{
    /**
     * @param string $zone
     *
     * @return QueryBuilder
     */
    public function createByZoneCodeQueryBuilder(string $zone): QueryBuilder
    {
        $builder = $this->createQueryBuilder('o');
        $builder
            ->select('o', 'position', 'zone')
            ->innerJoin('o.position', 'position')
            ->innerJoin('position.zone', 'zone', Join::WITH, $builder->expr()->eq('zone.code', ':zone'))
            ->setParameter('zone', $zone);

        return $builder;
    }

    /**
     * @param string $position
     *
     * @return QueryBuilder
     */
    public function createByPositionCodeQueryBuilder(string $position): QueryBuilder
    {
        $builder = $this->createQueryBuilder('o');
        $builder
            ->select('o', 'position')
            ->innerJoin('o.position', 'position', Join::WITH, $builder->expr()->eq('position.code', ':position'))
            ->setParameter('position', $position);

        return $builder;
    }

    /**
     * @param BannerZone        $zone
     * @param DateTimeInterface $date
     *
     * @return Banner[]
     */
    public function findForZone(BannerZone $zone, DateTimeInterface $date = null): array
    {
        if (null === $date) {
            $date = new DateTime();
        }

        $builder = $this->createQueryBuilder('o');
        $builder
            ->addSelect('image')
            ->distinct(true)
            ->innerJoin('o.position', 'position')
            ->innerJoin('position.zone', 'zone', Join::WITH, $builder->expr()->eq('zone', ':zone'))
            ->innerJoin('o.images', 'image', Join::WITH, $builder->expr()->isNotNull('image.path'))
            ->where(
                $builder->expr()->orX(
                    $builder->expr()->lte('o.publishFrom', ':date'),
                    $builder->expr()->isNull('o.publishFrom')
                ),
                $builder->expr()->orX(
                    $builder->expr()->gte('o.publishTo', ':date'),
                    $builder->expr()->isNull('o.publishTo')
                ),
                $builder->expr()->eq('o.enabled', $builder->expr()->literal(true))
            )
            ->orderBy('o.updatedAt', 'asc')
            ->setParameter('date', $date)
            ->setParameter('zone', $zone);

        return $builder->getQuery()->getResult();
    }

    /**
     * Get all enabled and active banners from specific position in channel
     *
     * @param ChannelInterface $channel
     * @param BannerPositionInterface $position
     * @param DateTime|null $date
     *
     * @return array
     * @throws \Exception
     */
    public function getAllActiveByChannelAndPosition(
        ChannelInterface $channel,
        BannerPositionInterface $position,
        \DateTime $date = null
    ): array {
        if (!$date) {
            $date = new DateTime();
        }

        $qb = $this->createQueryBuilder('o');

        return $qb->innerJoin('o.channels', 'c', Join::WITH, 'c.id = :channel')
            ->andWhere('o.position = :position')
            ->andWhere('o.enabled = :enabled')
            ->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->orX(
                        $qb->expr()->lte('o.publishFrom', ':currentDate'),
                        $qb->expr()->isNull('o.publishFrom')
                    ),
                    $qb->expr()->orX(
                        $qb->expr()->gte('o.publishTo', ':currentDate'),
                        $qb->expr()->isNull('o.publishTo')
                    )
                )
            )
            ->setParameters(
                [
                    'channel' => $channel,
                    'position' => $position,
                    'enabled' => true,
                    'currentDate' => $date,
                ]
            )
            ->getQuery()
            ->getResult();
    }
}
