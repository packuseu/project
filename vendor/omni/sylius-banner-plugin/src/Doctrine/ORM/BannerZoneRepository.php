<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Doctrine\ORM;

use Doctrine\ORM\Query\Expr\Join;
use Omni\Sylius\BannerPlugin\Model\BannerZone;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class BannerZoneRepository extends EntityRepository
{
    /**
     * @param string $code
     *
     * @return null|BannerZone|object
     */
    public function findOneByCode(string $code): ?BannerZone
    {
        return $this->findOneBy(['code' => $code]);
    }
}
