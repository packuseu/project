<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Fixture\Factory;

use Omni\Sylius\BannerPlugin\Model\BannerImageInterface;
use Omni\Sylius\BannerPlugin\Model\BannerImageTranslationInterface;
use Omni\Sylius\BannerPlugin\Model\BannerInterface;
use Omni\Sylius\BannerPlugin\Model\BannerPositionInterface;
use Sylius\Bundle\CoreBundle\Fixture\Factory\AbstractExampleFactory;
use Sylius\Bundle\CoreBundle\Fixture\Factory\ExampleFactoryInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Sylius\Component\Channel\Repository\ChannelRepositoryInterface;
use Sylius\Component\Core\Uploader\ImageUploaderInterface;
use Sylius\Component\Locale\Model\LocaleInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BannerExampleFactory extends AbstractExampleFactory implements ExampleFactoryInterface
{
    /**
     * @var FactoryInterface
     */
    private $bannerFactory;

    /**
     * @var FactoryInterface
     */
    private $bannerImageFactory;

    /**
     * @var EntityRepository
     */
    private $localeRepository;

    /**
     * @var ChannelRepositoryInterface
     */
    private $channelRepository;

    /**
     * @var EntityRepository
     */
    private $positionRepository;

    /**
     * @var EntityRepository
     */
    private $bannerRepository;

    /**
     * @var ImageUploaderInterface
     */
    private $imageUploader;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var OptionsResolver
     */
    private $optionsResolver;

    /**
     * @param FactoryInterface $bannerFactory
     * @param FactoryInterface $bannerImageFactory
     * @param ChannelRepositoryInterface $channelRepository
     * @param EntityRepository $localeRepository
     * @param EntityRepository $positionRepository
     * @param EntityRepository $bannerRepository
     * @param ImageUploaderInterface $imageUploader
     * @param string $rootDir
     */
    public function __construct(
        FactoryInterface $bannerFactory,
        FactoryInterface $bannerImageFactory,
        ChannelRepositoryInterface $channelRepository,
        EntityRepository $localeRepository,
        EntityRepository $positionRepository,
        EntityRepository $bannerRepository,
        ImageUploaderInterface $imageUploader,
        string $rootDir
    ) {
        $this->bannerFactory = $bannerFactory;
        $this->bannerImageFactory = $bannerImageFactory;
        $this->channelRepository = $channelRepository;
        $this->localeRepository = $localeRepository;
        $this->positionRepository = $positionRepository;
        $this->bannerRepository = $bannerRepository;
        $this->imageUploader = $imageUploader;
        $this->rootDir = $rootDir;

        $this->optionsResolver = new OptionsResolver();

        $this->configureOptions($this->optionsResolver);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $options = []): BannerInterface
    {
        $options = $this->optionsResolver->resolve($options);
        /** @var BannerPositionInterface $position */
        $position = $this->positionRepository->findOneBy(['code' => $options['position']]);

        if (!$position) {
            throw new \Exception('Position with code "' . $options['position'] . '" not found"');
        }

        /** @var BannerInterface $banner */
        $banner = $this->bannerFactory->createNew();

        $banner->setCode($options['code']);
        $banner->setPublishFrom(new \DateTime($options['published_from']));
        $banner->setPublishTo(new \DateTime($options['published_to']));
        $banner->setPosition($position);

        foreach ($this->channelRepository->findAll() as $channel) {
            $banner->addChannel($channel);
        }

        foreach ($options['images'] as $imageConfig) {
            /** @var BannerImageInterface $image */
            $image = $this->bannerImageFactory->createNew();

            if (file_exists($this->rootDir . '/../web/')) {
                $imagePath = $this->rootDir . '/../web/' . $imageConfig['image'];
            } else {
                $imagePath = $this->rootDir . '/../public/' . $imageConfig['image'];
            }

            $image->setFile(new UploadedFile($imagePath, basename($imagePath)));
            $image->setContentBackground($imageConfig['content_background'] ?? null);
            $image->setContentSpace($imageConfig['content_space'] ?? null);
            $image->setContentPosition($imageConfig['content_position'] ?? null);
            $image->setPosition($imageConfig['position'] ?? 0);

            /** @var LocaleInterface $locale */
            foreach ($this->localeRepository->findAll() as $locale) {
                /** @var BannerImageTranslationInterface $translation */
                $translation = $image->getTranslation($locale->getCode());
                $translation->setTitle($imageConfig['title'] ?? null);
                $translation->setContent($imageConfig['content'] ?? null);
                $translation->setLink($imageConfig['link'] ?? null);
            }

            $this->imageUploader->upload($image);
            $banner->addImage($image);
        }

        return $banner;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired([
                'code',
                'position',
                'published_from',
                'published_to',
                'images',
            ])
            ->setDefault('channels', [])
        ;
    }
}
