<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Fixture;

use Doctrine\Common\Persistence\ObjectManager;
use Sylius\Bundle\CoreBundle\Fixture\AbstractResourceFixture;
use Sylius\Bundle\CoreBundle\Fixture\Factory\ExampleFactoryInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

class BannerPositionFixture extends AbstractResourceFixture
{
    /**
     * @var string[]
     */
    private $types = [];

    /**
     * @param ObjectManager $objectManager
     * @param ExampleFactoryInterface $exampleFactory
     * @param array $types
     */
    public function __construct(ObjectManager $objectManager, ExampleFactoryInterface $exampleFactory, array $types)
    {
        parent::__construct($objectManager, $exampleFactory);

        $this->types = $types;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'omni_banner_position';
    }

    /**
     * {@inheritdoc}
     */
    protected function configureResourceNode(ArrayNodeDefinition $resourceNode): void
    {
        $resourceNode
            ->children()
                ->scalarNode('code')->cannotBeEmpty()->end()
                ->scalarNode('zone')->cannotBeEmpty()->end()
                ->enumNode('type')->cannotBeEmpty()->values(array_keys($this->types))->end()
                ->scalarNode('title')->cannotBeEmpty()->end()
                ->scalarNode('description')->end()
                ->scalarNode('link')->end()
            ->end()
        ;
    }
}
