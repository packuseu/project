<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Manager;

class PositionManager
{
    /**
     * @var array
     */
    protected $typeConfig;

    /**
     * @param array $typeConfig
     */
    public function __construct(array $typeConfig)
    {
        $this->typeConfig = $typeConfig;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return array_keys($this->typeConfig);
    }

    /**
     * @return array
     */
    public function getTypesConfig(): array
    {
        return $this->typeConfig;
    }

    /**
     * @param string $type
     *
     * @return array
     */
    public function getTypeConfig(string $type): array
    {
        return $this->typeConfig[$type] ?? [];
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getTemplate(string $type): string
    {
        return $this->typeConfig[$type]['twig_template'];
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getLabel(string $type): string
    {
        return $this->typeConfig[$type]['label'];
    }

    /**
     * @param string $type
     *
     * @return string
     */
    public function getExampleImagePath(string $type): string
    {
        return $this->typeConfig[$type]['example_image'];
    }
}
