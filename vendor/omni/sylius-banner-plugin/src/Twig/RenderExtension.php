<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Twig;

use Doctrine\Common\Collections\ArrayCollection;
use Omni\Sylius\BannerPlugin\Doctrine\ORM\BannerPositionRepository;
use Omni\Sylius\BannerPlugin\Doctrine\ORM\BannerRepository;
use Omni\Sylius\BannerPlugin\Doctrine\ORM\BannerZoneRepository;
use Omni\Sylius\BannerPlugin\Manager\PositionManager;
use Omni\Sylius\BannerPlugin\Model\BannerInterface;
use Omni\Sylius\BannerPlugin\Model\BannerPositionInterface;
use Omni\Sylius\BannerPlugin\Model\BannerZoneInterface;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Core\Model\ChannelInterface;
use Twig_Environment as Environment;
use Twig_Extension as Extension;
use Twig_SimpleFunction as SimpleFunction;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class RenderExtension extends Extension
{
    /**
     * @var BannerZoneRepository
     */
    protected $zoneRepository;

    /**
     * @var BannerRepository
     */
    protected $bannerRepository;

    /**
     * @var BannerPositionRepository
     */
    protected $positionRepository;

    /**
     * @var PositionManager
     */
    protected $positionManager;

    /**
     * @var ChannelContextInterface
     */
    protected $channelContext;

    /**
     * @param BannerZoneRepository $zoneRepository
     * @param BannerRepository $bannerRepository
     * @param BannerPositionRepository $positionRepository
     * @param PositionManager $positionManager
     * @param ChannelContextInterface $channelContext
     */
    public function __construct(
        BannerZoneRepository $zoneRepository,
        BannerRepository $bannerRepository,
        BannerPositionRepository $positionRepository,
        PositionManager $positionManager,
        ChannelContextInterface $channelContext
    ) {
        $this->zoneRepository = $zoneRepository;
        $this->bannerRepository = $bannerRepository;
        $this->positionRepository = $positionRepository;
        $this->positionManager = $positionManager;
        $this->channelContext = $channelContext;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new SimpleFunction(
                'omni_sylius_banner_zone',
                [$this, 'displayZone'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
            new SimpleFunction(
                'omni_sylius_banner_position',
                [$this, 'displayPosition'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
            new SimpleFunction(
                'omni_sylius_banner_display',
                [$this, 'displayBanner'],
                [
                    'is_safe' => ['html'],
                    'needs_environment' => true,
                ]
            ),
            new SimpleFunction(
                'omni_sylius_banner_position_type_config',
                [$this, 'getPositionTypeConfig']
            ),
        ];
    }

    /**
     * @param Environment $environment
     * @param string $zone
     * @param array $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function displayZone(Environment $environment, string $zone, array $options = []): string
    {
        $output = '';
        /** @var BannerZoneInterface $zone */
        $zone = $this->zoneRepository->findOneBy(['code' => $zone]);

        if (!$zone) {
            return '';
        }

        foreach ($zone->getPositions() as $position) {
            $output .= $this->displayPosition($environment, $position, $options);
        }

        return $environment->render(
            '@OmniSyliusBannerPlugin/_zone.html.twig',
            [
                'zone' => $zone,
                'position_content' => $output,
                'options' => $options,
            ]
        );
    }

    /**
     * @param Environment $environment
     * @param string|BannerPositionInterface $position
     * @param array $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws \Exception
     */
    public function displayPosition(Environment $environment, $position, array $options = []): string
    {
        /** @var BannerPositionInterface $position */
        $position = $position instanceof BannerPositionInterface ? $position :
            $this->positionRepository->findOneBy(['code' => $position]);

        if (!isset($options['channel'])) {
            $options['channel'] = $this->channelContext->getChannel()->getCode();
        }

        $banners = $this->getCurrentChannelAndPositionBanners(
            $position,
            $this->channelContext->getChannel(),
            $this->getPreviewDate($options)
        );
        if (!$position || empty($banners)) {
            return '';
        }

        $config = $this->positionManager->getTypeConfig($position->getType());

        return $environment->render(
            $config['twig_template'],
            [
                'position' => $position,
                'banners' => $banners,
                'options' => array_merge($config, $options),
            ]
        );
    }

    /**
     * @param Environment $environment
     * @param string|BannerInterface $banner
     * @param array $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function displayBanner(Environment $environment, $banner, array $options = []): string
    {
        /** @var BannerInterface $position */
        $banner = $banner instanceof BannerPositionInterface ? $banner :
            $this->bannerRepository->findOneBy(['code' => $banner]);

        if (!$this->isBannerDisplayable($banner, $this->getPreviewDate($options))) {
            return '';
        }

        return $environment->render(
            '@OmniSyliusBannerPlugin/Banner/banner.html.twig',
            [
                'banner' => $banner,
                'options' => $options,
            ]
        );
    }

    /**
     * @return array
     */
    public function getPositionTypeConfig(): array
    {
        return $this->positionManager->getTypesConfig();
    }

    /**
     * @param array $options
     *
     * @return \DateTime|null
     * @throws \Exception
     */
    protected function getPreviewDate(array $options): ?\DateTime
    {
        return isset($options['preview_date']) ? new \DateTime($options['preview_date']) : null;
    }

    /**
     * @param null|BannerInterface $banner
     * @param \DateTime|null $date
     *
     * @return bool
     */
    protected function isBannerDisplayable(?BannerInterface $banner, ?\DateTime $date): bool
    {
        return $banner &&
            $banner->isEnabled() &&
            $banner->isPublished($date) &&
            $banner->hasChannel($this->channelContext->getChannel());
    }

    /**
     * @param BannerPositionInterface $position
     * @param ChannelInterface|null $channel
     *
     * @return array
     * @throws \Exception
     */
    protected function getCurrentChannelAndPositionBanners(
        BannerPositionInterface $position,
        ?ChannelInterface $channel = null,
        \DateTime $date = null
    ): array {
        if (!$channel) {
            $channel = $this->channelContext->getChannel();
        }

        return $this->bannerRepository->getAllActiveByChannelAndPosition(
            $channel,
            $position,
            $date
        );
    }
}
