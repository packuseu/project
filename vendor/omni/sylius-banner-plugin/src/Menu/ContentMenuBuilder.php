<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\Menu;

use Knp\Menu\ItemInterface;
use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class ContentMenuBuilder
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function configureNodeMenu(MenuBuilderEvent $event): void
    {
        $contentMenu = $this->getMarketingMenu($event);

        $contentMenu
            ->addChild('banners', ['route' => 'omni_sylius_banner_zone_index'])
            ->setLabel('omni_sylius.menu.admin.banners')
            ->setLabelAttribute('icon', 'image')
        ;
    }

    /**
     * @param MenuBuilderEvent $event
     *
     * @return ItemInterface
     */
    private function getMarketingMenu(MenuBuilderEvent $event): ItemInterface
    {
        $adminMenu = $event->getMenu();
        $contentMenu = $adminMenu->getChild('marketing');

        if (null === $contentMenu) {
            $contentMenu = $adminMenu
                ->addChild('content')
                ->setLabel('omni_sylius.menu.admin.header')
            ;
        }

        return $contentMenu;
    }
}
