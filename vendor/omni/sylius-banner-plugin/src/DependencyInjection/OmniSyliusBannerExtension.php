<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\BannerPlugin\DependencyInjection;

use Sylius\Bundle\ResourceBundle\DependencyInjection\Extension\AbstractResourceExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class OmniSyliusBannerExtension extends AbstractResourceExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter(
            'omni_banner.position_types',
            $this->getFormattedPositionTypes($config['position_types'])
        );

        $this->registerResources('omni_sylius', $config['driver'], $config['resources'], $container);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $loader->load('services.yml');
    }

    /**
     * @param array $positionConfig
     *
     * @return array
     */
    protected function getFormattedPositionTypes(array $positionConfig): array
    {
        $config = [];

        foreach ($positionConfig as $key => $item) {
            $config[$key] = array_merge($item, $item['options']);
            unset($config[$key]['options']);
        }

        return $config;
    }
}
