<?php

namespace Omni\Sylius\BannerPlugin\Form\Subscriber;

use Omni\Sylius\BannerPlugin\Model\BannerPositionInterface;
use Omni\Sylius\BannerPlugin\Model\BannerZoneInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Adds a default zone to position, since the user initiates position creation in a certain zone
 */
class AddZoneSubscriber implements EventSubscriberInterface
{
    /**
     * @var BannerZoneInterface
     */
    private $zone;

    /**
     * @param BannerZoneInterface $zone
     */
    public function setZone(BannerZoneInterface $zone): void
    {
        $this->zone = $zone;
    }

    public static function getSubscribedEvents()
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    public function preSetData(FormEvent $event)
    {
        /** @var BannerPositionInterface $data */
        $data = $event->getData();

        if ($data->getZone()) {
            return;
        }

        $zone = $this->zone ??
            $event->getForm()->getConfig()->getAttribute('data_collector/passed_options')['zone'];

        $data->setZone($zone);
    }
}
