<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Form\Type;

use Sylius\Bundle\CoreBundle\Form\Type\ImageType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Component\Form\FormBuilderInterface;

class BannerImageType extends ImageType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'translations',
                ResourceTranslationsType::class,
                [
                    'entry_type' => BannerImageTranslationType::class,
                    'label' => 'omni_sylius.form.banner.translations',
                ]
            )
            ->add(
                'contentPosition',
                ChoiceType::class,
                [
                    'label' => 'omni_sylius.ui.content_position',
                    'choices' => [
                        'omni_sylius.ui.position.default' => 'default',
                        'omni_sylius.ui.position.left' => 'left',
                        'omni_sylius.ui.position.right' => 'right',
                        'omni_sylius.ui.position.top' => 'top',
                        'omni_sylius.ui.position.bottom' => 'bottom',
                    ],
                ]
            )->add(
                'contentSpace',
                PercentType::class,
                [
                    'label' => 'omni_sylius.ui.content_space',
                    'type' => 'integer',
                ]
            )->add(
                'contentBackground',
                ColorType::class,
                [
                    'label' => 'omni_sylius.ui.content_background',
                    'empty_data' => '#FFFFFF',
                ]
            )->add(
                'position',
                IntegerType::class,
                [
                    'label' => 'omni_sylius.ui.banner_position',
                    'attr' => [
                        'class' => 'js-sortable-position',
                    ],
                ]
            );
    }
}
