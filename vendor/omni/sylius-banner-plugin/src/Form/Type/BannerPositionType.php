<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Form\Type;

use Omni\Sylius\BannerPlugin\Form\Subscriber\AddZoneSubscriber;
use Omni\Sylius\BannerPlugin\Manager\PositionManager;
use Omni\Sylius\BannerPlugin\Model\BannerZoneInterface;
use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Sylius\Bundle\ResourceBundle\Form\Type\ResourceTranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class BannerPositionType extends AbstractResourceType
{
    /**
     * @var AddZoneSubscriber
     */
    protected $addZoneSubscriber;

    /**
     * @var string
     */
    protected $zoneClass;

    /**
     * @var PositionManager
     */
    protected $positionTypeManager;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param string $dataClass
     * @param array $validationGroups
     * @param AddZoneSubscriber $addZoneSubscriber
     * @param string $zoneClass
     * @param PositionManager $positionTypeManager
     * @param TranslatorInterface $translator
     */
    public function __construct(
        string $dataClass,
        array $validationGroups = [],
        AddZoneSubscriber $addZoneSubscriber,
        string $zoneClass,
        PositionManager $positionTypeManager,
        TranslatorInterface $translator
    ) {
        parent::__construct($dataClass, $validationGroups);

        $this->addZoneSubscriber = $addZoneSubscriber;
        $this->zoneClass = $zoneClass;
        $this->positionTypeManager = $positionTypeManager;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var BannerZoneInterface $zone */
        $zone = $options['zone'];

        if (!$zone instanceof BannerZoneInterface) {
            throw new \InvalidArgumentException('Expected an instance of banner zone');
        }

        $builder
            ->add(
                'code',
                TextType::class,
                [
                    'label' => 'sylius.ui.code',
                    'constraints' => new NotBlank(),
                ]
            )
            ->add(
                'zone',
                EntityType::class,
                [
                    'label' => 'omni_sylius.banner.zone',
                    'class' => $this->zoneClass,
                    'required' => true,
                    'choice_label' => function ($zone) {
                        /** @var BannerZoneInterface $zone */
                        return $zone->getTitle();
                    },
                ]
            )
            ->add(
                'type',
                ConfigurableChoiceType::class,
                [
                    'label' => 'sylius.ui.type',
                    'choices' => $this->getFormattedChoicesForTypes(),
                    'expanded' => true,
                    'multiple' => false,
                    'types_config' => $this->positionTypeManager->getTypesConfig()
                ]
            )
            ->add(
                'priority',
                IntegerType::class,
                [
                    'label' => 'omni_sylius.form.priority',
                    'data' => 0,
                ]
            )
            ->add(
                'translations',
                ResourceTranslationsType::class,
                [
                    'entry_type' => BannerPositionTranslationType::class,
                    'label' => 'omni_sylius.form.banner.translations',
                ]
            )
        ;

        $this->addZoneSubscriber->setZone($zone);
        $builder->addEventSubscriber($this->addZoneSubscriber);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver):void
    {
        parent::configureOptions($resolver);

        $resolver->setRequired('zone');
        $resolver->setAllowedValues(
            'zone',
            function ($value) {
                return $value instanceof BannerZoneInterface;
            }
        );
    }

    /**
     * @return array
     */
    protected function getFormattedChoicesForTypes(): array
    {
        $choices = [];

        foreach ($this->positionTypeManager->getTypesConfig() as $type => $config) {
            $choices[$this->translator->trans($config['label'])] = $type;
        }

        return $choices;
    }
}
