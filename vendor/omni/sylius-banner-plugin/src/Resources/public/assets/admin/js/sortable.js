$(function() {
    const sortableElement = $('#sortable');
    // On new image item creation adds position value
    $('.bannerForm .field').on('click', '.js-add-banner-image', function () {
        let sortableItemsObj = $('.js-sortable-item');
        if (sortableItemsObj.length > 1) {
            let newSortableItem = $('.js-sortable-item:last-child');
            let prevSortableItem = newSortableItem.prev();

            newSortableItem.find('.js-sortable-position').val(parseInt(prevSortableItem.find('.js-sortable-position').val()) + 1);
        }
    });

    var ckeConfigs = [];
    sortableElement.sortable({
        start: function( event, ui ) {
            // Clones current moved ckeditors configs and destroys them.
            // This fixes ckeditor freeze and its data loss on sortable
            let ckeditor = ui.item.find('.ckeditor');
            ckeditor.each(function(){
                var tagId = $(this).attr('id');
                var ckeClone = $(this).next('.cke').clone().addClass('cloned');
                ckeConfigs[tagId] = CKEDITOR.instances[tagId].config;
                CKEDITOR.instances[tagId].destroy();
                $(this).hide().after(ckeClone);
            });
        },
        cancel: "input,textarea,button,select,option,.cke",
        stop: function( event, ui ) {
            // Recreates ckeditors with cloned configs
            // This fixes ckeditor freeze and its data loss on sortable
            let ckeditor = ui.item.find('.ckeditor');
            ckeditor.each(function(){
                var tagId = $(this).attr('id');
                CKEDITOR.replace(tagId, ckeConfigs[tagId]);
                $(this).next('.cloned').remove();
            });

            // Reorders item position field on sortable item sort
            let i = 0;
            $('.js-sortable-position').each(function () {
                $(this).val(i);
                i++;
            });
        }
    });
});
