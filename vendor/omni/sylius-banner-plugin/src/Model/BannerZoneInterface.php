<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;

/**
 * Class BannerZone.
 */
interface BannerZoneInterface extends ResourceInterface, CodeAwareInterface, TranslatableInterface
{
    /**
     * @return string
     */
    public function getTitle(): ?string;

    /**
     * @return Collection|BannerPosition[]
     */
    public function getPositions();

    /**
     * @param BannerPositionInterface $position
     *
     * @return BannerZoneInterface
     */
    public function addPosition(BannerPositionInterface $position): BannerZoneInterface;

    /**
     * @param BannerPositionInterface $position
     *
     * @return BannerZoneInterface
     */
    public function removePosition(BannerPositionInterface $position): BannerZoneInterface;
}
