<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Model;

use Doctrine\Common\Collections\Collection;
use Sylius\Component\Resource\Model\CodeAwareInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;

interface BannerPositionInterface extends ResourceInterface, CodeAwareInterface, TranslatableInterface
{
    /**
     * @return BannerZoneInterface
     */
    public function getZone();

    /**
     * @param BannerZoneInterface $zone
     *
     * @return $this
     */
    public function setZone(BannerZoneInterface $zone);

    /**
     * @return string
     */
    public function getTitle(): ?string;

    /**
     * @return string
     */
    public function getType(): ?string;

    /**
     * @param string $type
     */
    public function setType(?string $type): void;

    /**
     * @return int
     */
    public function getPriority(): ?int;

    /**
     * @param int|null $priority
     *
     * @return BannerPositionInterface
     */
    public function setPriority(?int $priority): BannerPositionInterface;

    /**
     * @return Collection|BannerInterface[]
     */
    public function getBanners();

    /**
     * @param BannerInterface $banner
     *
     * @return $this
     */
    public function addBanner(BannerInterface $banner);

    /**
     * @param BannerInterface $banner
     *
     * @return $this
     */
    public function removeBanner(BannerInterface $banner);
}
