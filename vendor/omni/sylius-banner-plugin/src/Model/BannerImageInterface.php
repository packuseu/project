<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Model;

use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;

interface BannerImageInterface extends ImageInterface, TranslatableInterface
{
    /**
     * @return string|null
     */
    public function getContent(): ?string;

    /**
     * @return string|null
     */
    public function getContentPosition(): ?string;

    /**
     * @param string|null $contentPosition
     *
     * @return BannerImageInterface
     */
    public function setContentPosition(?string $contentPosition): BannerImageInterface;

    /**
     * @return int|null
     */
    public function getContentSpace(): ?int;

    /**
     * @param int|null $contentSpace
     *
     * @return BannerImageInterface
     */
    public function setContentSpace(?int $contentSpace): BannerImageInterface;

    /**
     * @return string|null
     */
    public function getContentBackground(): ?string;

    /**
     * @param string|null $contentBackground
     *
     * @return BannerImageInterface
     */
    public function setContentBackground(?string $contentBackground): BannerImageInterface;

    /**
     * @return int
     */
    public function getPosition(): ?int;

    /**
     * @param int $position
     */
    public function setPosition(int $position): void;
}
