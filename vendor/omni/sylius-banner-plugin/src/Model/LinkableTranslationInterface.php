<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TranslationInterface;

interface LinkableTranslationInterface extends ResourceInterface, TranslationInterface
{
    /**
     * @return null|string
     */
    public function getTitle(): ?string;

    /**
     * @param null|string $title
     *
     * @return LinkableTranslationInterface
     */
    public function setTitle(?string $title): LinkableTranslationInterface;

    /**
     * @return null|string
     */
    public function getDescription(): ?string;

    /**
     * @param null|string $description
     *
     * @return LinkableTranslationInterface
     */
    public function setDescription(?string $description): LinkableTranslationInterface;

    /**
     * @return null|string
     */
    public function getLink(): ?string;

    /**
     * @param null|string $link
     *
     * @return LinkableTranslationInterface
     */
    public function setLink(?string $link): LinkableTranslationInterface;
}
