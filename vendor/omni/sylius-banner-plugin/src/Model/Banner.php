<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Channel\Model\ChannelInterface;
use Sylius\Component\Core\Model\ImageInterface;
use Sylius\Component\Resource\Model\TimestampableTrait;

/**
 * Class Banner.
 */
class Banner implements BannerInterface
{
    use TimestampableTrait;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var Collection|ImageInterface[]
     */
    protected $images;

    /**
     * @var Collection|ChannelInterface[]
     */
    protected $channels;

    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * @var \DateTime
     */
    protected $publishFrom;

    /**
     * @var \DateTime
     */
    protected $publishTo;

    /**
     * @var BannerPosition;
     */
    protected $position;

    /**
     * Banner constructor.
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->channels = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * {@inheritdoc}
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * {@inheritdoc}
     */
    public function getImageByCode(string $code): ?ImageInterface
    {
        foreach ($this->images as $image) {
            if ($code === $image->getCode()) {
                return $image;
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function hasImages(): bool
    {
        return !$this->images->isEmpty();
    }

    /**
     * {@inheritdoc}
     */
    public function hasImage(ImageInterface $image): bool
    {
        return $this->images->contains($image);
    }

    /**
     * {@inheritdoc}
     */
    public function addImage(ImageInterface $image): void
    {
        if (false === $this->hasImage($image)) {
            $image->setOwner($this);
            $this->images->add($image);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeImage(ImageInterface $image): void
    {
        $image->setOwner(null);
        $this->images->removeElement($image);
    }

    /**
     * {@inheritdoc}
     */
    public function getImage(): ?ImageInterface
    {
        if (!$this->hasImages()) {
            return null;
        }

        return $this->getImages()->first();
    }

    /**
     * {@inheritdoc}
     */
    public function getImagesByType(string $type): Collection
    {
        $images = new ArrayCollection();

        foreach ($this->images as $image) {
            if ($type === $image->getType()) {
                $images->add($image);
            }
        }

        return $images;
    }

    /**
     * {@inheritdoc}
     */
    public function hasChannel(ChannelInterface $channel): bool
    {
        return $this->channels->contains($channel);
    }

    /**
     * {@inheritdoc}
     */
    public function hasChannelCode(string $channelCode): bool
    {
        foreach ($this->channels as $channel) {
            if ($channel->getCode() == $channelCode) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function addChannel(ChannelInterface $channel): void
    {
        if (false === $this->channels->contains($channel)) {
            $this->channels->add($channel);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeChannel(ChannelInterface $channel): void
    {
        $this->channels->removeElement($channel);
    }

    /**
     * @return Collection|ChannelInterface[]
     */
    public function getChannels()
    {
        return $this->channels;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function setEnabled(?bool $enabled): void
    {
        $this->enabled = (bool)$enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function enable(): void
    {
        $this->enabled = true;
    }

    /**
     * {@inheritdoc}
     */
    public function disable(): void
    {
        $this->enabled = false;
    }

    /**
     * {@inheritdoc}
     */
    public function getPublishFrom()
    {
        return $this->publishFrom;
    }

    /**
     * {@inheritdoc}
     */
    public function setPublishFrom($publishFrom)
    {
        $this->publishFrom = $publishFrom;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPublishTo()
    {
        return $this->publishTo;
    }

    /**
     * {@inheritdoc}
     */
    public function setPublishTo($publishTo)
    {
        $this->publishTo = $publishTo;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isPublished(\DateTime $date = null)
    {
        if (!$this->isEnabled()) {
            return false;
        }

        if (null === $date) {
            $date = new \DateTime();
        }

        $start = $this->getPublishFrom();
        $end = $this->getPublishTo();

        return $date >= $start && $date <= $end;
    }

    /**
     * {@inheritdoc}
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function setPosition(BannerPositionInterface $position)
    {
        $this->position = $position;

        return $this;
    }
}
