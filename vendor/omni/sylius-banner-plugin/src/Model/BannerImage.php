<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\BannerPlugin\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Sylius\Component\Core\Model\Image;
use Sylius\Component\Resource\Model\TranslatableTrait;
use Sylius\Component\Resource\Model\TranslationInterface;

/**
 * @method BannerImageInterface getTranslation($locale = null)
 */
class BannerImage extends Image implements BannerImageInterface
{
    use TranslatableTrait;

    /**
     * @var string
     */
    protected $contentPosition;

    /**
     * @var int
     */
    protected $contentSpace;

    /**
     * @var string
     */
    protected $contentBackground;

    /**
     * @var int
     */
    protected $position;

    /**
     * BannerImage constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * {@inheritdoc}
     */
    protected function createTranslation(): TranslationInterface
    {
        return new BannerImageTranslation();
    }

    /**
     * {@inheritdoc}
     */
    public function getContent(): ?string
    {
        return $this->getTranslation()->getContent();
    }

    /**
     * {@inheritdoc}
     */
    public function getContentPosition(): ?string
    {
        return $this->contentPosition;
    }

    /**
     * {@inheritdoc}
     */
    public function setContentPosition(?string $contentPosition): BannerImageInterface
    {
        $this->contentPosition = $contentPosition;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getContentSpace(): ?int
    {
        return $this->contentSpace;
    }

    /**
     * {@inheritdoc}
     */
    public function setContentSpace(?int $contentSpace): BannerImageInterface
    {
        $this->contentSpace = $contentSpace;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getContentBackground(): ?string
    {
        return $this->contentBackground;
    }

    /**
     * {@inheritdoc}
     */
    public function setContentBackground(?string $contentBackground): BannerImageInterface
    {
        $this->contentBackground = $contentBackground;

        return $this;
    }
}
