<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Nfq\DpdClient\Exception\InvalidResponseException;
use Nfq\DpdClient\Model\Tracking\Detail;
use Nfq\DpdClient\Model\Tracking\Tracking;
use Omni\Sylius\DpdPlugin\Client\Client;
use Omni\Sylius\DpdPlugin\Constants\Gateway;
use Omni\Sylius\DpdPlugin\Constants\ServiceCode;
use Omni\Sylius\DpdPlugin\Constants\TrackingStatus;
use Omni\Sylius\DpdPlugin\Factory\CheckTrackingRequestFactory;
use Omni\Sylius\DpdPlugin\HttpClient\DPDClientFactory;
use Omni\Sylius\DpdPlugin\Repository\ShipmentRepository;
use Sylius\Component\Core\Model\ShipmentInterface;
use SM\Factory\FactoryInterface as SMFactory;
use Sylius\Component\Shipping\ShipmentTransitions;

class TrackingStatusManager
{
    /** @var ShipmentRepository */
    protected $shipmentRepository;

    /** @var SMFactory */
    protected $stateMachineFactory;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var string */
    protected $shipmentShippedStateCode;

    /** @var string */
    protected $shipmentDeliveredStateCode;

    /** @var string */
    protected $shipmentDeliveredTransitionCode;

    /** @var Client */
    protected $client;

    /** @var Tracking[] */
    protected $trackings = [];

    public function __construct(
        DPDClientFactory $clientFactory,
        ShipmentRepository $shipmentRepository,
        SMFactory $stateMachineFactory,
        EntityManagerInterface $entityManager,
        string $shipmentShippedStatusCode,
        string $shipmentDeliveredStatusCode,
        string $shipmentDeliveredTransitionCode
    ) {
        $this->shipmentRepository = $shipmentRepository;
        $this->stateMachineFactory = $stateMachineFactory;
        $this->entityManager = $entityManager;
        $this->shipmentShippedStateCode = $shipmentShippedStatusCode;
        $this->shipmentDeliveredStateCode = $shipmentDeliveredStatusCode;
        $this->shipmentDeliveredTransitionCode = $shipmentDeliveredTransitionCode;
        $this->client = $clientFactory->create();
    }

    /**
     * @return ShipmentInterface[]
     */
    public function getDeliveredShipments(): array
    {
        $shipments = $this->shipmentRepository->findDpdShipmentsWithState($this->shipmentShippedStateCode);
        $deliveredShipments = [];

        foreach ($shipments as $shipment) {
            if ($this->isShipmentDelivered($shipment)) {
                $deliveredShipments[] = $shipment;
            }
        }

        return $deliveredShipments;
    }

    /**
     * @param ShipmentInterface $shipment
     *
     * @throws \SM\SMException
     */
    public function updateShipment(ShipmentInterface $shipment): void
    {
        if (false === $this->isShipmentDelivered($shipment)) {
            return;
        }

        $stateMachine = $this->stateMachineFactory->get($shipment, ShipmentTransitions::GRAPH);

        if ($stateMachine->apply($this->shipmentDeliveredTransitionCode, true)) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param ShipmentInterface $shipment
     *
     * @return bool
     */
    private function isShipmentDelivered(ShipmentInterface $shipment): bool
    {
        if ($this->shipmentDeliveredStateCode === $shipment->getState()) {
            return true;
        }

        if (false === strpos($shipment->getMethod()->getCode(), Gateway::CODE)) {
            return false;
        }

        if (null === $shipment->getTracking()) {
            return false;
        }

        $tracking = $this->getTrackings($shipment->getTracking())[0] ?? null;
        if (false === $tracking instanceof Tracking) {
            return false;
        }

        $detail = $tracking->getDetails()[0] ?? null;
        if (false === $detail instanceof Detail) {
            return false;
        }

        if ($this->isReturnedToWarehouse($detail)) {
            return false;
        }

        return $this->isStatusDelivered($detail);
    }

    /**
     * @param string $trackingCode
     *
     * @return Tracking[]
     */
    private function getTrackings(string $trackingCode): array
    {
        if (false === isset($this->trackings[$trackingCode])) {
            try {
                $this->trackings[$trackingCode] = $this->client->getTrackings(
                    CheckTrackingRequestFactory::create($trackingCode)
                );
            } catch (InvalidResponseException $e) {
                $error = json_decode(
                    ltrim($e->getMessage(), 'Response is missing required information: '),
                    true
                );

                if (200 !== $error['code']) {
                    throw $e;
                }

                $this->trackings[$trackingCode] = [];
            }
        }

        return $this->trackings[$trackingCode];
    }

    /**
     * @param Detail $detail
     *
     * @return bool
     */
    private function isStatusDelivered(Detail $detail): bool
    {
        return \in_array($detail->getStatusCode(), TrackingStatus::DELIVERED_STATUSES, true);
    }

    /**
     * @param Detail $detail
     *
     * @return bool
     */
    private function isReturnedToWarehouse(Detail $detail): bool
    {
        return \in_array($detail->getServiceCode(), ServiceCode::RETURN_SERVICES, true);
    }
}
