<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Command;

use Omni\Sylius\DpdPlugin\Manager\TrackingStatusManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateShipmentStatusCommand extends Command
{
    protected static $defaultName = 'omni:dpd:shipment-status:update';

    /** @var TrackingStatusManager */
    private $trackingStatusManager;

    /**
     * {@inheritdoc}
     */
    public function __construct(TrackingStatusManager $trackingStatusManager)
    {
        parent::__construct();

        $this->trackingStatusManager = $trackingStatusManager;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Updating DPD statuses for shipments...');

        $shipments = $this->trackingStatusManager->getDeliveredShipments();
        $progressBar = new ProgressBar($output, count($shipments));

        foreach ($shipments as $shipment) {
            $this->trackingStatusManager->updateShipment($shipment);
            $progressBar->advance();
        }

        $progressBar->finish();
        $output->writeln(PHP_EOL . 'Finished updating DPD statuses for shipments.');

        return 0;
    }
}
