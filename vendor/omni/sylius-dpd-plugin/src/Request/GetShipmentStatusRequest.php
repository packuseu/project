<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\DpdPlugin\Request;

use Nfq\DpdClient\Request\RequestInterface;

class GetShipmentStatusRequest implements RequestInterface
{
    /**
     * @var string
     */
    private $parcelNumber;

    /**
     * @return string
     */
    public function getParcelNumber(): string
    {
        return $this->parcelNumber;
    }

    /**
     * @param string $parcelNumber
     */
    public function setParcelNumber(string $parcelNumber): void
    {
        $this->parcelNumber = $parcelNumber;
    }

    public function toArray(): array
    {
        return [
            'parcel_number' => $this->getParcelNumber(),
        ];
    }
}
