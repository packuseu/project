<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Generator;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use Nfq\DpdClient\Client;
use Omni\Sylius\DpdPlugin\Constants\Gateway;
use Omni\Sylius\DpdPlugin\Factory\CallCourierRequestFactory;
use Omni\Sylius\DpdPlugin\Factory\CloseManifestRequestFactory;
use Omni\Sylius\DpdPlugin\HttpClient\DPDClientFactory;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGeneratorInterface;
use Omni\Sylius\ManifestPlugin\Generator\PathGenerator;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;
use Omni\Sylius\ShippingPlugin\Provider\CredentialProviderInterface;
use Sylius\Component\Core\Model\Order;
use Sylius\Component\Shipping\Model\ShipmentInterface;
use Symfony\Component\Filesystem\Filesystem;

class ManifestGenerator implements ManifestGeneratorInterface
{
    /**
     * @var DPDClientFactory
     */
    private $factory;

    /**
     * @var PathGenerator
     */
    private $pathGenerator;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var CredentialProviderInterface
     */
    private $credentialProvider;

    /**
     * @var null|array
     */
    private $credentials = null;

    /**
     * @param DPDClientFactory $factory
     * @param PathGenerator $pathGenerator
     * @param Filesystem $filesystem
     * @param CredentialProvider $credentialProvider
     */
    public function __construct(
        DPDClientFactory $factory,
        PathGenerator $pathGenerator,
        Filesystem $filesystem,
        CredentialProviderInterface $credentialProvider
    ) {
        $this->factory = $factory;
        $this->pathGenerator = $pathGenerator;
        $this->filesystem = $filesystem;
        $this->credentialProvider = $credentialProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function generate(ManifestInterface $manifest): string
    {
        $client = $this->getClient($manifest);

        if ($manifest->isCallCourier()) {
            $totalWeight = 0;
            $totalParcels = $manifest->getShippingExports()->count();

            foreach ($manifest->getShippingExports() as $shippingExport) {
                $shipment = $shippingExport->getShipment();

                if (!$shipment instanceof ShipmentInterface || !$shipment->getTracking()) {
                    continue;
                }
                $totalWeight += $shipment->getShippingWeight();
            }

            $callCourierRequestFactory = CallCourierRequestFactory::create(
                $manifest->getAddress(),
                $manifest->getWorkingHoursUntil(),
                $manifest->getWorkingHoursFrom(),
                $this->credentials['config']['dpdUsername'],
                1,
                $totalParcels,
                $manifest->getPalletsCount(),
                $totalWeight
            );

            $response = $client->callCourier($callCourierRequestFactory);
        }

        $response = $client->closeManifest(CloseManifestRequestFactory::create($manifest));

        if ('err' === $response->getStatus()) {
            throw new \Exception('Could not generate manifest for dpd.');
        }

        $path = $this->pathGenerator->generate(
            sprintf('%s-%s', $manifest->getSender()->getId(), $manifest->getShippingGateway()->getCode())
        );

        $file = $response->getPdf();
        $pdf = $this->isFileEncoded($file) ? base64_decode($file) : $file;

        $this->filesystem->dumpFile($path, $pdf);

        return basename($path);
    }

    public function getShipperName(): string
    {
        return Gateway::CODE;
    }

    /**
     * @param ManifestInterface $manifest
     * @return \Nfq\DpdClient\Client
     */
    private function getClient(ManifestInterface $manifest): Client
    {
        /** @var ShippingExport $shippingExport */
        $shippingExport = $manifest->getShippingExports()->first();
        $sender = $shippingExport->getShipment()->getSender();
        $credentials = $this->getCredentials($sender);

        if ($credentials !== null) {
            return $this->factory->create(
                $credentials['dpdHost'],
                $credentials['dpdUsername'],
                $credentials['dpdPassword']
            );
        }

        return $this->factory->create();
    }

    /**
     * @param $sender
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getCredentials($sender): array
    {
        $this->credentials = $this->credentialProvider->getCredentials($sender->getId(), Gateway::CODE);

        if ($this->credentials === null) {
            throw new \Exception('No credentials found for this shipment.');
        }

        return $this->credentials['config'];
    }

    private function isFileEncoded(string $file): bool
    {
        $finfo = new \finfo(FILEINFO_MIME);
        $mimeType = strstr($finfo->buffer($file), ';', true);

        return $mimeType === 'text/plain';
    }
}
