<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Form\Type;

use Nfq\DpdClient\Constants\ServiceCodes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ShippingGatewayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'service_code',
                ChoiceType::class,
                [
                    'label' => 'omni_sylius_dpd.ui.service_code',
                    'required' => true,
                    'choices' => [
                        'omni_sylius_dpd.ui.standard_parcel_shop' => ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP,
                        'omni_sylius_dpd.ui.standard_parcel' => ServiceCodes::STANDARD_PARCEL,
                        'omni_sylius_dpd.ui.b2c' => ServiceCodes::STANDARD_PARCEL_B2C,
                        'omni_sylius_dpd.ui.b2c_cod' => ServiceCodes::STANDARD_PARCEL_B2C_COD,
                    ]
                ]
            )
            ->add(
                'print_size',
                ChoiceType::class,
                [
                    'label' => 'omni_sylius_dpd.ui.print_size',
                    'required' => true,
                    'choices' => [
                        'A4' => 'A4',
                        'A6' => 'A6',
                        'A4_4xA6' => 'A4_4xA6',
                    ]
                ]
            )
            ->add(
                'print_format',
                ChoiceType::class,
                [
                    'label' => 'omni_sylius_dpd.ui.print_format',
                    'required' => true,
                    'choices' => [
                        'pdf' => 'pdf',
                        'zpl' => 'zpl'
                    ]
                ]
            );
    }
}
