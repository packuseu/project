<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\EventListener;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use BitBag\SyliusShippingExportPlugin\Event\ExportShipmentEvent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Nfq\DpdClient\Client;
use Omni\Sylius\DpdPlugin\Constants\Gateway;
use Omni\Sylius\DpdPlugin\Factory\CreateParcelLabelRequestFactory;
use Omni\Sylius\DpdPlugin\Factory\CreateShipmentRequestFactory;
use Omni\Sylius\DpdPlugin\HttpClient\DPDClientFactory;
use Omni\Sylius\ShippingPlugin\Event\ExportShipmentsEvent;
use Omni\Sylius\ShippingPlugin\Provider\CredentialProviderInterface;
use SM\Factory\Factory;
use Sylius\Component\Core\Model\ShipmentInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Sylius\Component\Shipping\ShipmentTransitions;
use Symfony\Contracts\Translation\TranslatorInterface;
use Webmozart\Assert\Assert;

class ShippingExportEventListener
{
    private const DEFAULT_FORMAT = 'pdf';

    /**
     * @var DPDClientFactory
     */
    private $factory;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Factory
     */
    private $smFactory;

    /**
     * @var CredentialProviderInterface
     */
    private $credentialProvider;

    /**
     * @param DPDClientFactory $factory
     * @param TranslatorInterface $translator
     * @param Factory $smFactory
     * @param CredentialProviderInterface $credentialProvider
     */
    public function __construct(
        DPDClientFactory $factory,
        TranslatorInterface $translator,
        Factory $smFactory,
        CredentialProviderInterface $credentialProvider
    ) {
        $this->factory = $factory;
        $this->translator = $translator;
        $this->smFactory = $smFactory;
        $this->credentialProvider = $credentialProvider;
    }

    public function exportShipment(ExportShipmentEvent $exportShipmentEvent): void
    {
        /** @var ShippingExport $shippingExport */
        $shippingExport = $exportShipmentEvent->getShippingExport();
        /** @var ShippingGatewayInterface $shippingGateway */
        $shippingGateway = $shippingExport->getShippingGateway();

        if (!$shippingGateway || Gateway::CODE !== $shippingGateway->getCode()) {
            return;
        }

        /** @var ShipmentInterface $shipment */
        $shipment = $shippingExport->getShipment();
        /** @var OrderInterface $order */
        $order = $shipment->getOrder();

        try {
            $this->createClient($shippingExport);

            $shipmentRequest = CreateShipmentRequestFactory::create($exportShipmentEvent);
            $shipmentResponse = $this->client->createShipment($shipmentRequest);

            $numbers = $shipmentResponse->getNumbers();
            Assert::count($numbers, 1);
            $externalId = $numbers[0];

            $labelRequest = CreateParcelLabelRequestFactory::create($numbers, $shippingExport->getShippingGateway());
            $labelResponse = $this->client->createParcelLabel($labelRequest);
        } catch (\Exception $e) {
            $exportShipmentEvent->addErrorFlash(
                sprintf('DPD: "%s": %s', $order->getNumber(), $e->getMessage())
            );

            return;
        }

        $extension = $labelRequest->getPrintFormat() ?? self::DEFAULT_FORMAT;
        $shippingExport->setExternalId($externalId);
        $exportShipmentEvent->saveShippingLabel($labelResponse->getContents(), $extension);

        $exportShipmentEvent->addSuccessFlash();
        $exportShipmentEvent->exportShipment();
    }

    public function exportShipments(ExportShipmentsEvent $exportShipmentsEvent): void
    {
        /** @var ShippingExport[] $shippingExport */
        $shippingExports = $exportShipmentsEvent->getShippingExports();

        if (!$this->isDPDProvider($shippingExports)) {
            return;
        }

        try {
            $this->createClient($shippingExports[0]);

            $shipmentRequest = CreateShipmentRequestFactory::createForMultipleShipments($exportShipmentsEvent);
            $shipmentResponse = $this->client->createShipment($shipmentRequest);

            Assert::eq(count($shipmentResponse->getNumbers()), count($shippingExports));

            foreach ($shipmentResponse->getNumbers() as $key => $number) {
                /** @var ShippingExportInterface $exportShipment */
                $exportShipment = $shippingExports[$key];
                /** @var ShipmentInterface $shipment */
                $shipment = $exportShipment->getShipment();
                $shipment->setTracking($number);
                $exportShipment->setExternalId($number);

                $sm = $this->smFactory->get($shipment, ShipmentTransitions::GRAPH);
                if (\in_array(ExportShipmentsEvent::EXPORTED_TRANSITION, $sm->getPossibleTransitions(), true)) {
                    $sm->apply(ExportShipmentsEvent::EXPORTED_TRANSITION);
                }

                $labelRequest = CreateParcelLabelRequestFactory::create([$number], $shippingExports[0]->getShippingGateway());
                $labelResponse = $this->client->createParcelLabel($labelRequest);
                $extension = $labelRequest->getPrintFormat() ?? self::DEFAULT_FORMAT;

                $exportShipmentsEvent->persist($exportShipment);

                $exportShipmentsEvent->saveShippingLabel($exportShipment, $labelResponse->getContents(), $extension);
                $exportShipmentsEvent->exportShipment($exportShipment);
            }

            $exportShipmentsEvent->addSuccessFlash();
        } catch (\Exception $exception) {
            $exportShipmentsEvent->addErrorFlash(
                $this->translator->trans('omni_sylius_dpd.error', ['message' => $exception->getMessage()])
            );
            $exportShipmentsEvent->setException($exception);
        }
    }

    private function createClient(ShippingExportInterface $shippingExport): void
    {
        $sender = $shippingExport->getShipment()->getSender();
        $credentials = $this->getCredentials($sender);

        $this->client = $this->factory->create(
            $credentials['dpdHost'],
            $credentials['dpdUsername'],
            $credentials['dpdPassword']
        );
    }

    /**
     * @param ShippingExport[] $shippingExports
     * @return bool
     */
    private function isDPDProvider(array $shippingExports): bool
    {
        return count($shippingExports) > 0 &&
            $shippingExports[0]->getShippingGateway() instanceof ShippingGatewayInterface &&
            Gateway::CODE === $shippingExports[0]->getShippingGateway()->getCode();
    }

    /**
     * @param $sender
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getCredentials($sender): array
    {
        $credentials = $this->credentialProvider->getCredentials($sender->getId(), Gateway::CODE);

        if ($credentials === null) {
            throw new \Exception('No credentials found for this shipment.');
        }

        return $credentials['config'];
    }
}
