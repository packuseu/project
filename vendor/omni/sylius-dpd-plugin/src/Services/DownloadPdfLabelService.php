<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\DpdPlugin\Services;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Omni\Sylius\DpdPlugin\Constants\Gateway;
use Omni\Sylius\DpdPlugin\Factory\CreateParcelLabelRequestFactory;
use Omni\Sylius\DpdPlugin\HttpClient\DPDClientFactory;
use Omni\Sylius\ShippingPlugin\Provider\CredentialProviderInterface;
use Omni\Sylius\ShippingPlugin\Services\DownloadPdfLabelInterface;

class DownloadPdfLabelService implements DownloadPdfLabelInterface
{
    /**
     * @var CredentialProviderInterface
     */
    private $credentialProvider;

    /**
     * @var DPDClientFactory
     */
    private $factory;

    /**
     * DownloadPdfLabelService constructor.
     *
     * @param DPDClientFactory            $factory
     * @param CredentialProviderInterface $credentialProvider
     */
    public function __construct(
        DPDClientFactory $factory,
        CredentialProviderInterface $credentialProvider
    ) {
        $this->factory = $factory;
        $this->credentialProvider = $credentialProvider;
    }

    /**
     * @param ShippingExportInterface $shippingExport
     *
     * @return string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function download(ShippingExportInterface $shippingExport, array $parcelNumbers): ?string
    {
        $this->createClient($shippingExport);

        $labelRequest = CreateParcelLabelRequestFactory::create(
            $parcelNumbers,
            $shippingExport->getShippingGateway()
        );
        $labelRequest->setPrintFormat('pdf');

        $labelResponse = $this->client->createParcelLabel($labelRequest, $shippingExport->getShippingGateway());

        return $labelResponse->getContents();
    }

    /**
     * @return string
     */
    public function getShipperName(): string
    {
        return Gateway::CODE;
    }

    /**
     * @param ShippingExportInterface $shippingExport
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function createClient(ShippingExportInterface $shippingExport): void
    {
        $sender = $shippingExport->getShipment()->getSender();
        $credentials = $this->getCredentials($sender);

        $this->client = $this->factory->create(
            $credentials['dpdHost'],
            $credentials['dpdUsername'],
            $credentials['dpdPassword']
        );
    }

    /**
     * @param $sender
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getCredentials($sender): array
    {
        $credentials = $this->credentialProvider->getCredentials($sender->getId(), Gateway::CODE);

        if ($credentials === null) {
            throw new \Exception('No credentials found for this shipment.');
        }

        return $credentials['config'];
    }
}
