<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\DependencyInjection;

use Sylius\Component\Core\Model\ShipmentInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('omni_sylius_dpd');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('client_factory')->defaultValue('httplug.factory.guzzle6')->end()
                ->scalarNode('username')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('password')->isRequired()->cannotBeEmpty()->end()
                ->scalarNode('host')->isRequired()->cannotBeEmpty()->end()
                ->booleanNode('test_mode')->setDeprecated()->defaultTrue()->end()
                ->scalarNode('shipment_shipped_state_code')->defaultValue(ShipmentInterface::STATE_SHIPPED)->end()
                ->scalarNode('shipment_delivered_state_code')->defaultNull()->end()
                ->scalarNode('shipment_delivered_transition_code')->defaultNull()->end()
                ->arrayNode('parcel_machine')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('enabled_countries')
                            ->defaultValue(['LT'])
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
