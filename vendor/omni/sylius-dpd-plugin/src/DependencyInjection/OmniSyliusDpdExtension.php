<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\DependencyInjection;

use Omni\Sylius\DpdPlugin\Generator\ManifestGenerator;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

final class OmniSyliusDpdExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $config, ContainerBuilder $container): void
    {
        $config = $this->processConfiguration($this->getConfiguration([], $container), $config);
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $configurationTreeParameters = [
            'username',
            'password',
            'host',
            'shipment_shipped_state_code',
            'shipment_delivered_state_code',
            'shipment_delivered_transition_code',
        ];

        foreach ($configurationTreeParameters as $key) {
            $container->setParameter($this->getAlias() . '.' . $key, $config[$key]);
        }

        $loader->load('services.yml');

        $bundles = $container->getParameter('kernel.bundles');
        if (isset($bundles['OmniSyliusParcelMachinePlugin'])) {
            $container->setParameter(
                $this->getAlias() . '.' . 'parcel_machine.enabled_countries',
                $config['parcel_machine']['enabled_countries']
            );

            $loader->load('parcel_machine.yml');
        }

        $loader->load('shipping_export.yml');
    }

    public function prepend(ContainerBuilder $container)
    {
        $configs = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration($this->getConfiguration([], $container), $configs);

        $config = [
            'clients' => [
                'dpd' => [
                    'factory' => $config['client_factory'],
                    'config' => [
                        'verify' => false,
                    ]
                ],
            ],
        ];

        $container->prependExtensionConfig('httplug', $config);
    }
}
