<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Repository;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingGateway;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Omni\Sylius\DpdPlugin\Constants\Gateway;
use Sylius\Component\Core\Model\ShipmentInterface;
use function Doctrine\ORM\QueryBuilder;

class ShipmentRepository extends EntityRepository
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, $entityManager->getClassMetadata(ShipmentInterface::class));
    }

    /**
     * @return ShipmentInterface[]
     */
    public function findDpdShipmentsWithState(string $state): array
    {
        $qb = $this->createQueryBuilder('shipment');

        $qb->innerJoin(ShippingGateway::class, 'gateway', Join::WITH, $qb->expr()->eq('gateway.code', ':gatewayCode'));
        $qb->innerJoin('gateway.shippingMethods', 'method', Join::WITH, $qb->expr()->eq('method', 'shipment.method'));

        $qb->where(
            $qb->expr()->andX(
                $qb->expr()->eq('shipment.state', ':state'),
                $qb->expr()->isNotNull('shipment.tracking')
            )
        );

        $qb->setParameter('state', $state);
        $qb->setParameter('gatewayCode', Gateway::CODE);

        return $qb->getQuery()->getResult();
    }
}
