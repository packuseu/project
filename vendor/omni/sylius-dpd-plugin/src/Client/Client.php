<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\DpdPlugin\Client;

use Nfq\DpdClient\Client as BaseClient;
use Nfq\DpdClient\Exception\InvalidResponseException;
use Nfq\DpdClient\Factory\Model\Tracking\ErrorFactory;
use Nfq\DpdClient\Factory\Model\Tracking\TrackingFactory;
use Nfq\DpdClient\Model\Tracking\Tracking;
use Nfq\DpdClient\Request\CheckTrackingRequest;
use Omni\Sylius\DpdPlugin\Factory\Model\ShipmentStatusFactory;
use Omni\Sylius\DpdPlugin\Model\ShipmentStatus;
use Omni\Sylius\DpdPlugin\Request\GetShipmentStatusRequest;
use Webmozart\Assert\Assert;
use Nfq\DpdClient\HttpClient\Client as HttpClient;

class Client extends BaseClient
{
    /** @var HttpClient */
    private $httpClient;

    /** @var HttpClient */
    private $statusClient;

    /**
     * @param HttpClient $httpClient
     * @param HttpClient $statusClient
     */
    public function __construct(HttpClient $httpClient, HttpClient $statusClient)
    {
        parent::__construct($httpClient, $statusClient);

        $this->httpClient = $httpClient;
        $this->statusClient = $statusClient;
    }

    /**
     * @param GetShipmentStatusRequest $request
     * @return ShipmentStatus
     */
    public function getShipmentStatus(GetShipmentStatusRequest $request): ShipmentStatus
    {
        $response = $this->httpClient->post('/ws-mapper-rest/parcelStatus_', $request->toArray());
        Assert::isArray($response);

        return ShipmentStatusFactory::fromResponse($response);
    }

    /**
     * @param CheckTrackingRequest $request
     *
     * @return Tracking[]
     * @throws InvalidResponseException
     */
    public function getTrackings(CheckTrackingRequest $request): array
    {
        $response = $this->statusClient->get('/external/tracking?' . http_build_query($request->toArray()));
        /** @var Tracking[] $trackings */
        $trackings = TrackingFactory::fromResponse($response);

        foreach ($trackings as $key => $value) {
            if (false === $value->isValid()) {
                throw new InvalidResponseException(
                    sprintf(
                        'Response is missing required information: %s',
                        \json_encode(ErrorFactory::toResponse($value->getError()))
                    )
                );
            }
        }

        return $trackings;
    }
}
