<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Constants;

class TrackingStatus
{
    public const DROPPED_IN_PICKUP_POINT = 'DOPKY';
    public const PICKED_UP_BY_COURIER = '15';
    public const DELIVERED_TO_PICKUP_POINT = 'DODEI';
    public const DELIVERED_TO_CONSIGNEE = '13';
    public const PICKED_UP_BY_CONSIGNEE_FROM_PICKUP_POINT = 'DODEY';
    public const DELIVERED_BY_DRIVER_TO_PICKUP_POINT = '23';

    public const DELIVERED_STATUSES = [
        self::DELIVERED_TO_CONSIGNEE,
        self::PICKED_UP_BY_CONSIGNEE_FROM_PICKUP_POINT,
    ];
}
