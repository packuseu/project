<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Model;

class ShipmentStatus
{
    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $responseStatus;

    /**
     * @var string
     */
    private $errorMessage;

    /**
     * @param string $status
     * @param string $responseStatus
     * @param string $errorMessage
     */
    public function __construct(string $status, string $responseStatus, string $errorMessage)
    {
        $this->status = $status;
        $this->responseStatus = $responseStatus;
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getResponseStatus(): string
    {
        return $this->responseStatus;
    }

    /**
     * @param string $responseStatus
     */
    public function setResponseStatus(string $responseStatus): void
    {
        $this->responseStatus = $responseStatus;
    }

    /**
     * @return bool
     */
    public function isGetShipmentStatusSuccessful(): bool
    {
        return $this->getResponseStatus() === 'ok';
    }

    /**
     * @return string
     */
    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }
}
