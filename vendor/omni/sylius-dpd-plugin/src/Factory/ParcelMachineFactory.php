<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Factory;

use Nfq\DpdClient\Model\ParcelMachine;
use Omni\Sylius\DpdPlugin\Provider\ParcelMachineProvider;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Sylius\Component\Resource\Factory\FactoryInterface;

class ParcelMachineFactory implements ParcelMachineFactoryInterface
{
    /**
     * @var FactoryInterface
     */
    private $factory;

    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function create(ParcelMachine $parcelMachine): ParcelMachineInterface
    {
        /** @var ParcelMachineInterface $model */
        $model = $this->factory->createNew();

        $model->setCode($parcelMachine->getId());
        $model->setCountry($parcelMachine->getCountry());
        $model->setCity($parcelMachine->getCity());
        $model->setStreet($parcelMachine->getStreet());
        $model->setProvider(ParcelMachineProvider::PROVIDER_CODE);
        $model->setPostcode($parcelMachine->getPostCode());
        $model->setEnabled(true);

        return $model;
    }

    /**
     * @param ParcelMachine[] $parcelMachines
     *
     * @return ParcelMachineInterface[]
     */
    public function createMany(array $parcelMachines): array
    {
        $models = [];

        foreach ($parcelMachines as $parcelMachine) {
            $models[] = $this->create($parcelMachine);
        }

        return $models;
    }
}
