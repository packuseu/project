<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Factory;

use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;
use Nfq\DpdClient\Request\CloseManifestRequest;
use DateTime;

class CloseManifestRequestFactory
{
    /**
     * @param ManifestInterface $manifest
     * @param DateTime|null $closeDate
     *
     * @return CloseManifestRequest
     */
    public static function create(ManifestInterface $manifest, ?DateTime $closeDate = null): CloseManifestRequest
    {
        $request = new CloseManifestRequest();
        $request->setCloseDate($closeDate ?? new DateTime());
        return $request;
    }

}
