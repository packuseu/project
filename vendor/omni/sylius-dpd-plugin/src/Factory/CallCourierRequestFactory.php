<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Factory;

use Nfq\DpdClient\Request\CallCourierRequest;
use Sylius\Component\Core\Model\AddressInterface;

class CallCourierRequestFactory
{
    /**
     * @param AddressInterface $senderAddress
     * @param string $senderWorkingUntilHours
     * @param string $pickupFromHours
     *
     * @return CallCourierRequest
     */
    public static function create(
        AddressInterface $senderAddress,
        string $senderWorkingUntilHours,
        string $pickupFromHours,
        string $payerId,
        int $orderNr,
        int $parcelsCount = 1,
        int $palletsCount = 0,
        float $weight = 1,
        string $comment = null
    ): CallCourierRequest {
        $request = new CallCourierRequest();
        $request->setSenderWorkUntil(self::getPickupDate($pickupFromHours) . ' ' . $senderWorkingUntilHours);
        $request->setPickupTime(self::getPickupDate($pickupFromHours) . ' ' . self::getPickupTime($pickupFromHours));

        $request->setSenderCity($senderAddress->getCity());
        $request->setSenderContact($senderAddress->getPhoneNumber());
        $request->setSenderCountry($senderAddress->getCountryCode());
        $request->setSenderPostalCode($senderAddress->getPostcode());
        $request->setSenderAddress($senderAddress->getStreet());
        $request->setSenderPhone($senderAddress->getPhoneNumber());

        $request->setWeight($weight);
        $request->setParcelsCount($parcelsCount);
        $request->setPalletsCount($palletsCount);
        $request->setRequestOrderNr($orderNr);
        $request->setPayerId($payerId);

        $request->setComments($comment);

        return $request;
    }

    /**
     * @param string $pickupTime
     *
     * @return string
     */
    private static function getPickupDate(string $pickupTime): string
    {
        $dayOfWeek = self::currentTime('w');
        $currentTime = self::currentTime('H:i:s');
        $timeCutOff = strtotime('14:30:00');

        if ($dayOfWeek === 6) {
            $date = date("Y-m-d", strtotime("+ 2 days", strtotime($currentTime)));
        } else {
            if ($dayOfWeek === 7) {
                $date = date("Y-m-d", strtotime("+ 1 day", strtotime($currentTime)));
            } else {
                if ($dayOfWeek === 5) {
                    if (strtotime($currentTime) >= $timeCutOff
                        || strtotime(date('H:m:s', strtotime($pickupTime)))
                        >= $timeCutOff
                    ) {
                        $date = date("Y-m-d", strtotime("+ 3 days", strtotime($currentTime)));
                    } else {
                        $date = self::currentTime("Y-m-d");
                    }
                } else {
                    if (strtotime($currentTime) >= $timeCutOff
                        || strtotime(date('H:m:s', strtotime($pickupTime)))
                        >= $timeCutOff
                    ) {
                        $date = date("Y-m-d", strtotime("+ 1 days", strtotime($currentTime)));
                    } else {
                        $date = self::currentTime("Y-m-d");
                    }
                }
            }
        }

        return $date;
    }

    private static function getPickupTime(string $pickupTime): string
    {
        $currentAddedTime = (new \DateTime('+ 15 minutes'))->format('H:i:s');
        $timeCutOff = strtotime('14:30:00');
        $defaultPickupTime = strtotime('08:00:00');

        if (strtotime($currentAddedTime) >= $timeCutOff) {
            return $pickupTime;
        }

        if (strtotime($currentAddedTime) <= $defaultPickupTime)
        {
            return $pickupTime;
        }

        return $currentAddedTime;
    }

    /**
     * @param string $type
     *
     * @return string
     */
    private static function currentTime(string $type): string
    {
        $dateTime = new \DateTime('now');

        return $dateTime->format($type);
    }
}
