<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Factory;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use BitBag\SyliusShippingExportPlugin\Event\ExportShipmentEvent;
use Nfq\DpdClient\Constants\ServiceCodes;
use Nfq\DpdClient\Request\CreateShipmentRequest;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Omni\Sylius\ShippingPlugin\Event\ExportShipmentsEvent;
use Sylius\Component\Core\Model\AddressInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\ShipmentInterface;

class CreateShipmentRequestFactory
{
    public static function create(ExportShipmentEvent $exportShipmentEvent): CreateShipmentRequest
    {
        /** @var ShippingExport $shippingExport */
        $shippingExport = $exportShipmentEvent->getShippingExport();
        /** @var ShippingGatewayInterface $shippingGateway */
        $shippingGateway = $shippingExport->getShippingGateway();
        /** @var ShipmentInterface $shipment */
        $shipment = $shippingExport->getShipment();

        $request = self::createFromShippingExport($shippingExport);
        $request->setNumberOfParcels(1);

        if (ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP === $shippingGateway->getConfigValue('service_code')) {
            // TODO: if payment method is cod
            $request->setCodAmount($shipment->getShippingUnitTotal() / 100);
        }

        return $request;
    }

    public static function createForMultipleShipments(ExportShipmentsEvent $exportShipmentsEvent): CreateShipmentRequest
    {
        /** @var ShippingExport[] $shippingExport */
        $shippingExports = $exportShipmentsEvent->getShippingExports();
        /** @var ShipmentInterface $shipment */
        $shipment = $shippingExports[0]->getShipment();

        $request = self::createFromShippingExport($shippingExports[0]);
        $request->setNumberOfParcels(count($shippingExports));

        if (method_exists($shipment, 'getShipmentTotal') && $shipment->getShipmentTotal() > 0) {
            $request->setCodAmount($shipment->getShipmentTotal() / 100);
        }

        return $request;
    }

    public static function createFromShippingExport(ShippingExport $shippingExport): CreateShipmentRequest
    {
        /** @var ShippingGatewayInterface $shippingGateway */
        $shippingGateway = $shippingExport->getShippingGateway();
        /** @var ShipmentInterface $shipment */
        $shipment = $shippingExport->getShipment();
        /** @var OrderInterface $order */
        $order = $shippingExport->getShipment()->getOrder();
        /** @var AddressInterface $shippingAddress */
        $shippingAddress = $order->getShippingAddress();
        $customer = $shippingAddress->getCustomer();

        $request = new CreateShipmentRequest();

        $request
            ->setName($shippingAddress->getFullName())
            ->setStreet($shippingAddress->getStreet())
            ->setCity($shippingAddress->getCity())
            ->setCountry($shippingAddress->getCountryCode())
            ->setPostCode(self::getPostCode($shippingAddress))
            ->setPhone($shippingAddress->getPhoneNumber())
            ->setWeight($shipment->getShippingWeight())
            ->setEmail(null === $customer ? null : $customer->getEmail())
            ->setParcelNumber(self::generateParcelNumber($order->getNumber(), $shipment->getId()))
            ->setDocumentReturnReferenceNumber($order->getNumber())
            ->setParcelType(self::getParcelType($shipment, $shippingGateway));

        if ($shipment->getParcelMachine() !== null) {
            $request
                ->setParcelShopId($shipment->getParcelMachine()->getCode());
        }

        if ($shipment->getParcelMachine() && \in_array($request->getStreet(), [null, ''], true)
            && \in_array($request->getCity(), [null, ''], true)
        ) {
            /** @var ParcelMachineInterface $parcelMachine */
            $parcelMachine = $shipment->getParcelMachine();
            $request->setCity($parcelMachine->getCity());
            $request->setStreet($parcelMachine->getStreet());
            $request->setPostCode($parcelMachine->getPostcode());
        }

        if ($order->getNotes()) {
            $request->setRemark(substr($order->getNotes(), 0, 45));
        }

        return $request;
    }

    /**
     * @param string $orderNumber
     * @param int $shipmentId
     *
     * @return string
     */
    private static function generateParcelNumber(string $orderNumber, int $shipmentId): string
    {
        $strPadLimit = 14 - strlen($orderNumber);

        return substr(
            sprintf(
                '%s-%s-%s',
                $orderNumber,
                str_pad((string)$shipmentId, $strPadLimit, '0', STR_PAD_LEFT),
                substr((string)time(), -4)
            ),
            0,
            20
        );
    }

    private static function getParcelType(
        ShipmentInterface $shipment,
        ShippingGatewayInterface $shippingGateway
    ): string {
        $service = $shippingGateway->getConfigValue('service_code');

        if ($shipment->getParcelMachine() !== null) {
            $service = ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP;
        }

        if (method_exists($shipment, 'getShipmentTotal') && $shipment->getShipmentTotal() > 0) {
            $service .= '-COD';
        }

        return $service;
    }

    /**
     * @param AddressInterface $address
     * @return string|string[]|null
     */
    private static function getPostCode(AddressInterface $address)
    {
        return preg_replace(['/^(' . $address->getCountryCode() . '\W*)/',  '/\W*/'], '', $address->getPostcode());
    }
}
