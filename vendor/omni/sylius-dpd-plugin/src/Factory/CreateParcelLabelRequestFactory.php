<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\DpdPlugin\Factory;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Nfq\DpdClient\Request\CreateParcelLabelRequest;

class CreateParcelLabelRequestFactory
{
    public static function create(array $parcelNumbers, ShippingGatewayInterface $shippingGateway): CreateParcelLabelRequest
    {
        $request = new CreateParcelLabelRequest();

        $request->setParcelNumbers($parcelNumbers);
        $request->setPrintSize($shippingGateway->getConfigValue('print_size') ?? 'A4');
        $request->setPrintFormat($shippingGateway->getConfigValue('print_format') ?? 'pdf');

        return $request;
    }
}
