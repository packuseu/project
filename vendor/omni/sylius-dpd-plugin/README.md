# OmniSyliusDpdPlugin

## Installation

* Run `composer require omni/sylius-dpd-plugin:dev-master php-http/guzzle6-adapter`.

* Add the following bundles to `AppKernel.php`:

```php
new Http\HttplugBundle\HttplugBundle(),
new Omni\Plugin\SyliusDpdPlugin\OmniSyliusDpdPlugin(),
```

* Optionally install `omni/sylius-parcel-machine-plugin` package for parcel machine information synchronization.

## Configuration reference

```yaml
omni_sylius_dpd:
    client_factory: httplug.factory.guzzle6
    username: '%dpd.username%'
    password: '%dpd.password%'
    shipment_shipped_state_code: shipped # (default: shipped)
    shipment_delivered_state_code: completed
    shipment_delivered_transition_code: complete
    parcel_machine:
        enabled_countries:
            - LT
            - LV
            - EE
```

### Running plugin tests

```bash
$ bin/phpunit
```
