<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Synchronizer;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Omni\Sylius\ParcelMachinePlugin\Doctrine\ORM\ParcelMachineRepositoryInterface;
use Omni\Sylius\ParcelMachinePlugin\Provider\ParcelMachineProviderInterface;

class ParcelMachineSynchronizer implements ParcelMachineSynchronizerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ParcelMachineRepositoryInterface
     */
    private $repository;

    /**
     * @param EntityManagerInterface $em
     * @param ParcelMachineRepositoryInterface $repository
     */
    public function __construct(EntityManagerInterface $em, ParcelMachineRepositoryInterface $repository)
    {
        $this->em = $em;
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function synchronize(ParcelMachineProviderInterface $provider): void
    {
        $external = $this->indexByCode($provider->getAll());

        if (empty($external)) {
            return;
        }

        $internal = $this->repository->findByProvider($provider->getCode());

        $this->updateExistingParcelMachines($internal, $external);
        $this->addMissingParcelMachines($internal, $external);
        $this->disableExtraParcelMachines($internal, $external);

        $this->em->flush();
    }

    /**
     * @param ParcelMachineInterface[] $parcelMachines
     *
     * @return ParcelMachineInterface[]
     */
    private function indexByCode(array $parcelMachines): array
    {
        $result = [];

        foreach ($parcelMachines as $parcelMachine) {
            $result[$parcelMachine->getCode()] = $parcelMachine;
        }

        return $result;
    }

    /**
     * @param ParcelMachineInterface[] $internal
     * @param ParcelMachineInterface[] $external
     */
    private function updateExistingParcelMachines(array $internal, array $external): void
    {
        foreach ($internal as $code => $internalMachine) {
            if (\array_key_exists($code, $external)) {
                $externalMachine = $external[$code];

                $internalMachine->setCountry($externalMachine->getCountry());
                $internalMachine->setCity($externalMachine->getCity());
                $internalMachine->setStreet($externalMachine->getStreet());
                $internalMachine->setPostcode($externalMachine->getPostcode());
                $internalMachine->setEnabled($externalMachine->isEnabled());
            }
        }
    }

    /**
     * @param ParcelMachineInterface[] $internal
     * @param ParcelMachineInterface[] $external
     */
    private function addMissingParcelMachines(array $internal, array $external): void
    {
        $machinesToAdd = array_diff_key($external, $internal);

        /** @var ParcelMachineInterface $machine */
        foreach ($machinesToAdd as $machine) {
            $this->em->persist($machine);
        }
    }

    /**
     * @param ParcelMachineInterface[] $internal
     * @param ParcelMachineInterface[] $external
     */
    private function disableExtraParcelMachines(array $internal, array $external): void
    {
        $machinesToRemove = array_diff_key($internal, $external);

        /** @var ParcelMachineInterface $machine */
        foreach ($machinesToRemove as $machine) {
            $machine->setEnabled(false);
        }
    }
}
