<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Registry;

use Omni\Sylius\ParcelMachinePlugin\Exception\UnknownParcelMachineProviderException;
use Omni\Sylius\ParcelMachinePlugin\Provider\ParcelMachineProviderInterface;

class ParcelMachineProviderRegistry
{
    /**
     * @var ParcelMachineProviderInterface[]
     */
    private $providers = [];

    /**
     * @param string $code
     * @param ParcelMachineProviderInterface $provider
     */
    public function add(string $code, ParcelMachineProviderInterface $provider): void
    {
        $this->providers[$code] = $provider;
    }

    /**
     * @param string $code
     *
     * @return ParcelMachineProviderInterface
     *
     * @throws UnknownParcelMachineProviderException
     */
    public function get(string $code): ParcelMachineProviderInterface
    {
        if (false === array_key_exists($code, $this->providers)) {
            throw new UnknownParcelMachineProviderException(
                sprintf('Parcel machine provider with code "%s" does not exist', $code)
            );
        }

        return $this->providers[$code];
    }

    /**
     * @return ParcelMachineProviderInterface[]
     */
    public function all(): array
    {
        return $this->providers;
    }
}
