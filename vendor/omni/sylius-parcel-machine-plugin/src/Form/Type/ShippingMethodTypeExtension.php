<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Form\Type;

use Omni\Sylius\ParcelMachinePlugin\Registry\ParcelMachineProviderRegistry;
use Sylius\Bundle\ShippingBundle\Form\Type\ShippingMethodType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ShippingMethodTypeExtension extends AbstractTypeExtension
{
    /**
     * @var ParcelMachineProviderRegistry
     */
    private $registry;

    /**
     * @param ParcelMachineProviderRegistry $registry
     */
    public function __construct(ParcelMachineProviderRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'parcelMachineProviderCode',
                ChoiceType::class,
                [
                    'label' => 'omni.ui.provider',
                    'expanded' => false,
                    'multiple' => false,
                    'required' => false,
                    'choices' => $this->getChoices(),
                    'choice_label' => function ($key) {
                        return $key;
                    }
                ]
            );
    }

    /**
     * @return string[]
     */
    private function getChoices(): array
    {
        return array_keys($this->registry->all());
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return ShippingMethodType::class;
    }
}
