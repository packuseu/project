<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Doctrine\ORM;

use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachineInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;

interface ParcelMachineRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $provider
     * @param string $country
     *
     * @return string[]
     */
    public function getCities(string $provider, string $country): array;

    /**
     * @param string $provider
     * @param string $city
     *
     * @return array
     */
    public function findByCity(string $provider, string $city): array;

    /**
     * @param string $provider
     *
     * @return array
     */
    public function findByProvider(string $provider): array;

    /**
     * @param string $provider
     *
     * @return ParcelMachineInterface[]
     */
    public function findEnabledByProvider(string $provider): array;
}
