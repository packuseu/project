<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Doctrine\ORM;

use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class ParcelMachineRepository extends EntityRepository implements ParcelMachineRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findByCity(string $provider, string $code): array
    {
        $qb = $this->createQueryBuilder('pm');

        $qb
            ->select(
                [
                    'pm.id',
                    'pm.code',
                    'pm.country',
                    'pm.city',
                    'pm.street',
                ]
            )
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('pm.city', ':city'),
                    $qb->expr()->eq('pm.provider', ':provider'),
                    $qb->expr()->eq('pm.enabled', $qb->expr()->literal(true))
                )
            )
            ->setParameter('city', $code)
            ->setParameter('provider', $provider);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findByProvider(string $provider): array
    {
        $qb = $this->getByProviderQueryBuilder($provider);

        $qb->indexBy('pm', 'pm.code');

        return $qb->getQuery()->getResult();
    }

    /**
     * {@inheritdoc}
     */
    public function getCities(string $provider, string $country): array
    {
        $qb = $this->createQueryBuilder('pm');

        $qb
            ->select('pm.city')
            ->distinct()
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('pm.provider', ':provider'),
                    $qb->expr()->eq('pm.country', ':country')
                )
            )
            ->setParameter('provider', $provider)
            ->setParameter('country', $country)
            ->orderBy('pm.city');

        return array_column($qb->getQuery()->getResult(), 'city');
    }

    /**
     * {@inheritdoc}
     */
    public function getCitiesByCountries(string $provider, array $countries): array
    {
        $qb = $this->createQueryBuilder('pm');

        $qb
            ->select('pm.city')
            ->distinct()
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('pm.provider', ':provider'),
                    $qb->expr()->in('pm.country', ':countries')
                )
            )
            ->setParameter('provider', $provider)
            ->setParameter('countries', $countries)
            ->orderBy('pm.city');

        return array_column($qb->getQuery()->getResult(), 'city');
    }

    /**
     * {@inheritdoc}
     */
    public function findEnabledByProvider(string $provider): array
    {
        $qb = $this->getByProviderQueryBuilder($provider);

        $qb
            ->andWhere($qb->expr()->eq('pm.enabled', $qb->expr()->literal(true)));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $provider
     *
     * @return QueryBuilder
     */
    private function getByProviderQueryBuilder(string $provider): QueryBuilder
    {
        $qb = $this->createQueryBuilder('pm');

        $qb
            ->where($qb->expr()->eq('pm.provider', ':provider'))
            ->setParameter('provider', $provider);

        return $qb;
    }
}
