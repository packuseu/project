<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Command;

use Omni\Sylius\ParcelMachinePlugin\Registry\ParcelMachineProviderRegistry;
use Omni\Sylius\ParcelMachinePlugin\Synchronizer\ParcelMachineSynchronizerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParcelMachineSynchronizeCommand extends Command implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected static $defaultName = 'omni:parcel-machine:synchronize';

    /**
     * @var ParcelMachineProviderRegistry
     */
    private $registry;

    /**
     * @var ParcelMachineSynchronizerInterface
     */
    private $synchronizer;

    /**
     * @param ParcelMachineProviderRegistry $registry
     * @param ParcelMachineSynchronizerInterface $synchronizer
     */
    public function __construct(
        ParcelMachineProviderRegistry $registry,
        ParcelMachineSynchronizerInterface $synchronizer
    ) {
        parent::__construct();

        $this->synchronizer = $synchronizer;
        $this->registry = $registry;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setDescription('Synchronizes parcel machine addresses from external sources');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Synchronizing parcel machines...');

        foreach ($this->registry->all() as $provider) {
            try {
                $this->synchronizer->synchronize($provider);
            } catch (\Exception $e) {
                $output->writeln(
                    sprintf('Provider "%s" failed to fetch parcel machines. Continuing work...',
                        $provider->getCode()
                    )
                );
                $this->logger->error(
                    sprintf('Provider: "%s". Error message: %s.',
                        $provider->getCode(),
                        $e->getMessage()
                    )
                );
                continue;
            }
        }

        $output->writeln('Finished synchronizing parcel machines!');
    }
}
