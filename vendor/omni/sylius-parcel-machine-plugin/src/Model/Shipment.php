<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Model;

use Sylius\Component\Core\Model\Shipment as BaseShipment;

class Shipment extends BaseShipment
{
    /**
     * @var ParcelMachineInterface|null
     */
    private $parcelMachine;

    /**
     * @return null|ParcelMachineInterface
     */
    public function getParcelMachine(): ?ParcelMachineInterface
    {
        return $this->parcelMachine;
    }

    /**
     * @param null|ParcelMachineInterface $parcelMachine
     *
     * @return Shipment
     */
    public function setParcelMachine(?ParcelMachineInterface $parcelMachine): Shipment
    {
        $this->parcelMachine = $parcelMachine;

        return $this;
    }
}
