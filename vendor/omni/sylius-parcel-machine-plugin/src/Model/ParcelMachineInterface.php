<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ParcelMachinePlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;

interface ParcelMachineInterface extends ResourceInterface
{
    /**
     * @return string
     */
    public function getCode(): ?string;

    /**
     * @param string $code
     */
    public function setCode(?string $code): void;

    /**
     * @return string
     */
    public function getCountry(): ?string;

    /**
     * @param string $country
     */
    public function setCountry(?string $country): void;

    /**
     * @return string
     */
    public function getCity(): ?string;

    /**
     * @param string $city
     */
    public function setCity(?string $city): void;

    /**
     * @return string
     */
    public function getStreet(): ?string;

    /**
     * @param string $street
     */
    public function setStreet(?string $street): void;

    /**
     * @return string
     */
    public function getProvider(): ?string;

    /**
     * @param string $provider
     */
    public function setProvider(string $provider): void;

    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void;

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface;

    /**
     * @param \DateTimeInterface|null $createdAt
     */
    public function setCreatedAt(?\DateTimeInterface $createdAt): void;

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface;

    /**
     * @param \DateTimeInterface|null $updatedAt
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void;

    /**
     * @return string|null
     */
    public function getPostcode(): ?string;

    /**
     * @param string|null $postcode
     */
    public function setPostcode(?string $postcode): void;
}
