@synchronizing_parcel_machines
Feature: Synchronize parcel machine addresses
  In order to provide valid shipping options
  As an Administrator
  I want parcel machine addresses to be synchronized with data from shipping providers

  # todo
  Scenario: Synchronization from a single source
  Scenario: Synchronization from multiple sources
  Scenario: Shipping provider returns empty address list
