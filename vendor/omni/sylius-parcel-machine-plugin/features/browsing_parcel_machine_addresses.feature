@managing_parcel_machines
Feature: Browsing parcel machine addresses
  In order to verify parcel machine information
  As an Administrator
  I want to be able to browse parcel machine address list

  Background:
    Given the store operates on a single channel
    And the store has a parcel machine with code "DPD001"
    And I am logged in as an administrator

  @ui
  Scenario: Seeing parcel machine in the list
    When I browse parcel machines
    Then I should see a single parcel machine with code "DPD001"
