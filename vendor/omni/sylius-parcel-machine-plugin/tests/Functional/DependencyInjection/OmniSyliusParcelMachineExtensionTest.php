<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Omni\Sylius\ParcelMachinePlugin\Functional\DependencyInjection;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Omni\Sylius\ParcelMachinePlugin\DependencyInjection\OmniSyliusParcelMachineExtension;
use Omni\Sylius\ParcelMachinePlugin\Doctrine\ORM\ParcelMachineRepository;
use Omni\Sylius\ParcelMachinePlugin\Model\ParcelMachine;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;

class OmniSyliusParcelMachineExtensionTest extends AbstractExtensionTestCase
{
    public function testExtensionRegistersResources()
    {
        $this->load();

        $this->assertContainerBuilderHasParameter('omni.model.parcel_machine.class', ParcelMachine::class);
        $this->assertContainerBuilderHasService('omni.manager.parcel_machine');
        $this->assertContainerBuilderHasService('omni.controller.parcel_machine', ResourceController::class);
        $this->assertContainerBuilderHasService('omni.repository.parcel_machine', ParcelMachineRepository::class);
    }
    /**
     * {@inheritdoc}
     */
    protected function getContainerExtensions()
    {
        return [new OmniSyliusParcelMachineExtension()];
    }
}
