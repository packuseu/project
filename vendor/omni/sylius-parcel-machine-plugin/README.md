# Sylius Parcel Machine Plugin

## Installation

* Run `composer require omni/sylius-parcel-machine-plugin`
* Add the plugin to AppKernel.php

```php
new \Omni\Sylius\ParcelMachinePlugin\OmniSyliusParcelMachinePlugin(),
```

* Import plugin configuration in `app/config.yml`

```yaml
imports:
    - { resource: "@OmniSyliusParcelMachinePlugin/Resources/config/app/config.yml" }
```

* Import routing in `app/routing.yml`

```yaml
omni_parcel_machine:
    prefix: /
    resource: "@OmniSyliusParcelMachinePlugin/Resources/config/routing.yml"
```

* For shipping method and parcel machine provider configuration you need to override `@SyliusAdmin/ShippingMethod/_form.html.twig` template and render `parcelMachineProviderCode` row.

* Extend `ShippingMethod` model with the trait of the plugin and add the following mapping:

```
AppBundle\Entity\ShippingMethod:
    type: mappedSuperclass
    table: sylius_shipping_method
    fields:
        parcelMachineProviderCode:
            column: parcel_machine_provider_code
            type: string
            nullable: true
```

## Usage

### Running plugin tests

  - PHPUnit

    ```bash
    $ bin/phpunit
    ```

  - PHPSpec

    ```bash
    $ bin/phpspec run
    ```

  - Behat (non-JS scenarios)

    ```bash
    $ bin/behat --tags="~@javascript"
    ```

  - Behat (JS scenarios)
 
    1. Download [Chromedriver](https://sites.google.com/a/chromium.org/chromedriver/)
    
    2. Run Selenium server with previously downloaded Chromedriver:
    
        ```bash
        $ bin/selenium-server-standalone -Dwebdriver.chrome.driver=chromedriver
        ```
    3. Run test application's webserver on `localhost:8080`:
    
        ```bash
        $ (cd tests/Application && bin/console server:run 127.0.0.1:8080 -d web -e test)
        ```
    
    4. Run Behat:
    
        ```bash
        $ bin/behat --tags="@javascript"
        ```

### Opening Sylius with your plugin

- Using `test` environment:

    ```bash
    $ (cd tests/Application && bin/console sylius:fixtures:load -e test)
    $ (cd tests/Application && bin/console server:run -d web -e test)
    ```
    
- Using `dev` environment:

    ```bash
    $ (cd tests/Application && bin/console sylius:fixtures:load -e dev)
    $ (cd tests/Application && bin/console server:run -d web -e dev)
    ```


### Using PHPStan

- Run analyzer using predefined configuration:

    ```bash
    $ bin/phpstan.phar analyse -c phpstan.neon -l max src/
    ```
