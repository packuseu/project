<?php

/**
 * @copyright C UAB NFQ Technologies
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Model;

use Doctrine\Common\Collections\Collection;
use Sylius\Component\Core\Model\ImageInterface;

interface LogosAwareInterface
{
    /**
     * @return Collection|ImageInterface[]
     */
    public function getLogos(): Collection;

    /**
     * @param string $type
     *
     * @return Collection|ImageInterface[]
     */
    public function getLogosByType(string $type): Collection;

    /**
     * @return bool
     */
    public function hasLogos(): bool;

    /**
     * @param ImageInterface $logo
     *
     * @return bool
     */
    public function hasLogo(ImageInterface $logo): bool;

    /**
     * @param ImageInterface $logo
     */
    public function addLogo(ImageInterface $logo): void;

    /**
     * @param ImageInterface $logo
     */
    public function removeLogo(ImageInterface $logo): void;
}

