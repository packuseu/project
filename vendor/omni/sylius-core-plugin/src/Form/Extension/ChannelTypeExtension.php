<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Form\Extension;

use Omni\Sylius\CorePlugin\Form\Type\ChannelLogoType;
use Omni\Sylius\CorePlugin\Form\Type\ChannelWatermarkType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sylius\Bundle\ChannelBundle\Form\Type\ChannelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;

class ChannelTypeExtension extends AbstractTypeExtension
{
    /**
     * @var string
     */
    private $countryClass;

    /**
     * @param string $countryClass
     */
    public function __construct(string $countryClass)
    {
        $this->countryClass = $countryClass;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add(
                'countries',
                EntityType::class,
                [
                    'class' => $this->countryClass,
                    'multiple' => true,
                    'expanded' => false,
                ]
            )
            ->add(
                'images',
                CollectionType::class,
                [
                    'entry_type' => ChannelWatermarkType::class,
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => 'omni_core.channel_watermark.form.watermark',
                ]
            )
            ->add(
                'logos',
                CollectionType::class,
                [
                    'entry_type' => ChannelLogoType::class,
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'label' => 'omni_core.channel_logo.form.logo',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType(): string
    {
        return ChannelType::class;
    }
}
