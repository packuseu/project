<?php

/**
 * @copyright C UAB NFQ Technologies
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Grid\View;

use Sylius\Bundle\ResourceBundle\Controller\ParametersParserInterface;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Bundle\ResourceBundle\Grid\View\ResourceGridView;
use Sylius\Bundle\ResourceBundle\Grid\View\ResourceGridViewFactoryInterface;
use Sylius\Component\Grid\Data\DataProviderInterface;
use Sylius\Component\Grid\Definition\Grid;
use Sylius\Component\Grid\Parameters;
use Sylius\Component\Resource\Metadata\MetadataInterface;

class ResourceGridViewFactory implements ResourceGridViewFactoryInterface
{
    /** @var DataProviderInterface */
    private $dataProvider;

    /** @var ParametersParserInterface */
    private $parametersParser;

    public function __construct(DataProviderInterface $dataProvider, ParametersParserInterface $parametersParser)
    {
        $this->dataProvider = $dataProvider;
        $this->parametersParser = $parametersParser;
    }

    /**
     * {@inheritdoc}
     */
    public function create(
        Grid $grid,
        Parameters $parameters,
        MetadataInterface $metadata,
        RequestConfiguration $requestConfiguration
    ): ResourceGridView {
        $driverConfiguration = $grid->getDriverConfiguration();
        $request = $requestConfiguration->getRequest();
        $isExportRequest = ($requestConfiguration->getParameters()->has('export') && $requestConfiguration->getParameters()->get('export') === true);

        $grid->setDriverConfiguration($this->parametersParser->parseRequestValues($driverConfiguration, $request));

        if ($isExportRequest === false) {
            $gridData = $this->dataProvider->getData($grid, $parameters);
        } else {
            $gridData = $this->dataProvider->getExportData($grid, $parameters);
        }

        return new ResourceGridView($gridData, $grid, $parameters, $metadata, $requestConfiguration);
    }
}
