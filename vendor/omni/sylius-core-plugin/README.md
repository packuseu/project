# OmniCorePlugin

This is a central plugin for OMNI Sylius implementations.

# List of features

1. Multiple Country Select for Channel
2. Overriden DataSource of the sylius filters. This allows reaching `$queryBuilder` when 
creating a Sylius filter. This is necessary in order to create a filter that works with 
more than one database table
3. Adds `omni_string_with_alias_check` grid filter type which softly avoids adding condition
if field alias does not exists.
4. Adds `omni_sylius:fixtures:load_single {suite} {fixture}` - loads a single fixture 
without purging the database.

## 1. Multiple Country Select for Channel

This feature enables additional multiple select of countries for Channel 

## Installation

### 1. Install the plugin with composer

```bash
composer require omni/sylius-core-plugin
```

### 2. Enable the plugin in Kernel

```php
<?php

class AppKernel extends Kernel
{
    public function registerBundles(): array
    {
        $bundles = [
            ...
            new \Omni\Sylius\CorePlugin\OmniSyliusCorePlugin(),
        ];
 
        return array_merge(parent::registerBundles(), $bundles);
    }
}
```

### 3. Register config if required

```yaml
# app/config/config.yml

sylius_channel:   # override the channel model class that is being used
    resources:
        channel:
            classes:
                model: AppBundle\Entity\Channel
```


### 4. Register routing if required

No routing required yet.

### 5. Extend core models

Make sure your application bundle extends `AbstractResourceBundle`.
Extended entity should look like this:

```php

use Omni\Sylius\CorePlugin\Model\ChannelWatermarkTrait;
use Sylius\Component\Core\Model\ImagesAwareInterface;
use Omni\Sylius\CorePlugin\Model\LogosAwareInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_channel")
 */
class Channel extends BaseChannel implements ImagesAwareInterface, LogosAwareInterface
{
    use ChannelWatermarkTrait;
    use ChannelCountryTrait {
        ChannelCountryTrait::__construct as private _ChannelCountryConstruct;
    }
    use ChannelLogoTrait {
        ChannelLogoTrait::__construct as private _ChannelLogoConstruct;
    }
 
    public function __construct()
    {
        parent::__construct();
 
        $this->_ChannelWatermarkConstruct();
        $this->_ChannelCountryConstruct();
        $this->_ChannelLogoConstruct();
    }
}
```

### 6. Add the mapping manually to Doctrine

```yaml
AppBundle\Entity\Channel:
    type: mappedSuperclass
    table: sylius_channel
    oneToMany:
        images:
            targetEntity: Omni\Sylius\CorePlugin\Model\ChannelWatermarkInterface
            mappedBy: owner
            orphanRemoval: true
            cascade:
                - all
        logos:
            targetEntity: Omni\Sylius\CorePlugin\Model\ChannelLogoInterface
            mappedBy: owner
            orphanRemoval: true
            cascade:
                - all
    manyToMany:
        countries:
            targetEntity: Sylius\Component\Addressing\Model\CountryInterface
            joinTable:
                name: omni_channel_country
                joinColumns:
                -
                    name: channel_id
                    referencedColumnName: id
                inverseJoinColumns:
                -
                    name: country_id
                    referencedColumnName: id
```

### 8. Add form select code to Channel form template

```twig
# ../app/Resources/SyliusAdminBundle/views/Channel/_form.html.twig

...
{# START: OmniSyliusCorePlugin #}
{{ form_row(form.countries) }}
{{ form_row(form.logos) }}
{{ form_row(form.images) }}
{# END: OmniSyliusCorePlugin #}
...
``` 

### 9. Run doctrine command to update / migrate the DB

```bash
bin/console doctrine:migrations:diff
# or
bin/console doctrine:schema:update --force
```

### Possible issues.
1. All existing countries are showed in channel. 
If country is disabled, it's still visible in Channel countries list. 
And will be showed in front end address form.

2. Possible problem with customer existing addresses in checkout (sylius default problem?) when disabling country. 
Cause address might not be showed of that country.

