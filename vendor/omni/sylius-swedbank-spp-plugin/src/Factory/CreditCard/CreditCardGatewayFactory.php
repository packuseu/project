<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Factory\CreditCard;

use Omni\Sylius\SwedbankSpp\Communication\AbstractCommunication;
use Omni\Sylius\SwedbankSpp\Model\CreditCardGateway;
use Omni\Sylius\SwedbankSpp\Payum\Action\CreditCard\CaptureAction;
use Omni\Sylius\SwedbankSpp\Payum\Action\CreditCard\StatusAction;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\GatewayFactory;
use Payum\Core\GatewayFactoryInterface;

class CreditCardGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        $config->defaults(
            [
                'payum.factory_name' => 'swedbank_spp',
                'payum.factory_title' => 'Swedbank SPP factory',
                'payum.action.capture' => new CaptureAction(),
                'payum.action.status' => new StatusAction(),
                'apiClient' => '@Omni\Sylius\SwedbankSpp\Communication\AbstractCommunication',
            ]
        );

        if (false === (bool) $config['payum.api']) {
            $config['payum.api'] = function (ArrayObject $config) {

                $config->defaults($config['payum.default_options']);
                $config['payum.required_options'] = [];

                $communication = $config['apiClient'];

                return $communication;
            };
        }
    }
}
