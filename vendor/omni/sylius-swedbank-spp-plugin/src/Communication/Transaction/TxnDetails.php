<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use JMS\Serializer\Annotation as Annotation;

/**
 * The container for the transaction.
 *
 * @Annotation\AccessType("public_method")
 */
class TxnDetails
{
    /**
     * @const Ecomm is used for website / Internet / mobile environments.
     */
    const CAPTURE_METHOD_DEFAULT = 'ecomm';

    /**
     * The container that contains the data to be risk screened.
     *
     * @var Risk
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\Risk")
     * @Annotation\SerializedName("Risk")
     */
    private $risk;

    /**
     * The unique reference for each transaction which is echoed from the request.
     *
     * @var string
     *
     * @Annotation\SerializedName("merchantreference")
     * @Annotation\Type("string")
     * @Annotation\XmlElement(cdata=false)
     */
    private $merchantReference;

    /**
     * The amount to be authorized and an indication of the currency to be used. EUR only.
     *
     * @var Amount
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\Amount")
     * @Annotation\SerializedName("amount")
     */
    private $amount;

    /**
     * The container for 3-D Secure details.
     *
     * @var ThreeDSecure
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\ThreeDSecure")
     * @Annotation\SerializedName("ThreeDSecure")
     */
    private $threeDSecure;

    /**
     * Indicates the environment from which the transaction has been processed.
     *
     * For example, ecomm is used for website / Internet / mobile environments.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\SerializedName("capturemethod")
     * @Annotation\Type("string")
     */
    private $captureMethod = self::CAPTURE_METHOD_DEFAULT;

    /**
     * TxnDetails constructor.
     *
     * @param Action       $action
     * @param string       $merchantReference
     * @param Amount       $amount
     * @param ThreeDSecure $threeDSecure
     * @param string       $captureMethod
     */
    public function __construct(
        Action $action,
        $merchantReference,
        Amount $amount,
        ThreeDSecure $threeDSecure = null,
        $captureMethod = self::CAPTURE_METHOD_DEFAULT
    ) {
        $this->risk = new Risk($action);
        $this->merchantReference = $merchantReference;
        $this->amount = $amount;
        $this->threeDSecure = $threeDSecure;
        $this->captureMethod = $captureMethod;
    }

    /**
     * @return Risk
     */
    public function getRisk(): Risk
    {
        return $this->risk;
    }

    /**
     * @param Risk $risk
     */
    public function setRisk(Risk $risk): void
    {
        $this->risk = $risk;
    }

    /**
     * @return string
     */
    public function getMerchantReference(): string
    {
        return $this->merchantReference;
    }

    /**
     * @param string $merchantReference
     */
    public function setMerchantReference(string $merchantReference): void
    {
        $this->merchantReference = $merchantReference;
    }

    /**
     * @return Amount
     */
    public function getAmount(): Amount
    {
        return $this->amount;
    }

    /**
     * @param Amount $amount
     */
    public function setAmount(Amount $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return ThreeDSecure
     */
    public function getThreeDSecure(): ?ThreeDSecure
    {
        return $this->threeDSecure;
    }

    /**
     * @param ThreeDSecure $threeDSecure
     */
    public function setThreeDSecure(ThreeDSecure $threeDSecure): void
    {
        $this->threeDSecure = $threeDSecure;
    }

    /**
     * @return string
     */
    public function getCaptureMethod(): string
    {
        return $this->captureMethod;
    }

    /**
     * @param string $captureMethod
     */
    public function setCaptureMethod(string $captureMethod): void
    {
        $this->captureMethod = $captureMethod;
    }
}
