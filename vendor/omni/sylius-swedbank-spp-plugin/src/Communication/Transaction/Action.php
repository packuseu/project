<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use JMS\Serializer\Annotation as Annotation;

/**
 * The container that contains the data to be risk screened.
 *
 * @Annotation\AccessType("public_method")
 */
class Action
{
    /**
     * API version used.
     */
    const API_VERSION = 1;

    /**
     * API version used.
     *
     * @var int
     *
     * @Annotation\XmlAttribute()
     * @Annotation\Type("integer")
     * @Annotation\AccessType("reflection")
     */
    private $service = self::API_VERSION;

    /**
     * The container for the merchant configuration.
     *
     * @var MerchantConfiguration
     *
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\MerchantConfiguration")
     * @Annotation\SerializedName("MerchantConfiguration")
     */
    private $merchantConfiguration;

    /**
     * Customer details.
     *
     * @var CustomerDetails
     *
     * @Annotation\Type("Omni\Sylius\SwedbankSpp\Communication\Transaction\CustomerDetails")
     * @Annotation\SerializedName("CustomerDetails")
     */
    private $customerDetails;

    /**
     * Action constructor.
     *
     * @param MerchantConfiguration $merchantConfiguration
     * @param CustomerDetails       $customerDetails
     */
    public function __construct(
        MerchantConfiguration $merchantConfiguration,
        CustomerDetails $customerDetails
    ) {
        $this->merchantConfiguration = $merchantConfiguration;
        $this->customerDetails = $customerDetails;
    }

    /**
     * MerchantConfiguration getter.
     *
     * @return MerchantConfiguration
     */
    public function getMerchantConfiguration()
    {
        return $this->merchantConfiguration;
    }

    /**
     * MerchantConfiguration setter.
     *
     * @param MerchantConfiguration $merchantConfiguration
     */
    public function setMerchantConfiguration($merchantConfiguration)
    {
        $this->merchantConfiguration = $merchantConfiguration;
    }

    /**
     * CustomerDetails getter.
     *
     * @return CustomerDetails
     */
    public function getCustomerDetails()
    {
        return $this->customerDetails;
    }

    /**
     * CustomerDetails setter.
     *
     * @param CustomerDetails $customerDetails
     */
    public function setCustomerDetails($customerDetails)
    {
        $this->customerDetails = $customerDetails;
    }

    /**
     * @return int
     */
    public function getService(): int
    {
        return $this->service;
    }

    /**
     * @param int $service
     */
    public function setService(int $service): void
    {
        $this->service = $service;
    }
}
