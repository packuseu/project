<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use JMS\Serializer\Annotation as Annotation;

/**
 * The container for the merchant configuration.
 *
 * @Annotation\AccessType("public_method")
 */
class MerchantConfiguration
{
    /**
     * Transaction channel.
     *
     * @var string
     *
     * @Annotation\Type("string")
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\SerializedName("channel")
     */
    private $channel;

    /**
     * This represents the location of stores or outlets or geographical location of the legal entity.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("merchant_location")
     */
    private $merchantLocation;

    /**
     * MerchantConfiguration constructor.
     *
     * @param string $channel
     * @param string $merchantLocation
     */
    public function __construct(string $channel, $merchantLocation)
    {
        $merchantLocation = '';
        $this->channel = $channel;
        $this->merchantLocation = $merchantLocation;
    }

    /**
     * Channel getter.
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Channel setter.
     *
     * @param string $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * MerchantLocation getter.
     *
     * @return string
     */
    public function getMerchantLocation()
    {
        return $this->merchantLocation;
    }

    /**
     * MerchantLocation setter.
     *
     * @param string $merchantLocation
     */
    public function setMerchantLocation($merchantLocation)
    {
        $merchantLocation = '';
        $this->merchantLocation = $merchantLocation;
    }
}
