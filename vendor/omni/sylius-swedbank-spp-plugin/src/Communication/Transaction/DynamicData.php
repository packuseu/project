<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication\Transaction;

use Jms\Serializer\Annotation;

/**
 * The container for the Dynamic Data that is used to display information on the hosted page.
 */
class DynamicData
{
    /**
     * Field is optional and is used to control the display of the cardholder name field.
     *
     * Populate with “show” otherwise Cardholder Name will not be visible on capture page.
     * If value of show is supplied, cardholder name field will be displayed on hosted page.
     * Any other value or omission of this field will not display the cardholder name field.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("dyn_data_3")
     */
    private $cardHolderNameControl;

    /**
     * Fully qualified URL for the Go Back link that is displayed on the capture page.
     * A secure (https) URL must be provided. If left blank, the function will not work.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("dyn_data_4")
     */
    private $goBackLink;

    /**
     * Value supplied in this field should contain “Merchant Name” as it will be displayed on the hosted page.
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("dyn_data_2")
     */
    private $merchanName;

    /**
     * Fully qualified URL for the logo to be displayed at the top left of the capture page.
     * Please note image should be hosted from a secure location within your own system (https).
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("dyn_data_9")
     */
    private $logoUrl;

    /**
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("dyn_data_1")
     */
    private $cardNumber;

    /**
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("dyn_data_6")
     */
    private $creditCardExpMonth;

    /**
     *
     * @var string
     *
     * @Annotation\XmlElement(cdata=false)
     * @Annotation\Type("string")
     * @Annotation\SerializedName("dyn_data_7")
     */
    private $creditCardExpYear;

    /**
     * DynamicData constructor.
     *
     * @param null $cardHolderNameControl
     * @param null $goBackLink
     * @param null $merchanName
     * @param null $logoUrl
     * @param null $cardNumber
     * @param null $creditCardExpMonth
     * @param null $creditCardExpYear
     */
    public function __construct(
        $cardHolderNameControl = null,
        $goBackLink = null,
        $merchanName = null,
        $logoUrl = null,
        $cardNumber = null,
        $creditCardExpMonth = null,
        $creditCardExpYear = null
    ) {
        $this->cardHolderNameControl = $cardHolderNameControl;
        $this->goBackLink = $goBackLink;
        $this->merchanName = $merchanName;
        $this->logoUrl = $logoUrl;
        $this->cardNumber = $cardNumber;
        $this->creditCardExpMonth = $creditCardExpMonth;
        $this->creditCardExpYear = $creditCardExpYear;
    }

    /**
     * CardHolderNameControl getter.
     *
     * @return string
     */
    public function getCardHolderNameControl()
    {
        return $this->cardHolderNameControl;
    }

    /**
     * CardHolderNameControl setter.
     *
     * @param string $cardHolderNameControl
     */
    public function setCardHolderNameControl($cardHolderNameControl)
    {
        $this->cardHolderNameControl = $cardHolderNameControl;
    }

    /**
     * GoBackLink getter.
     *
     * @return string
     */
    public function getGoBackLink()
    {
        return $this->goBackLink;
    }

    /**
     * GoBackLink setter.
     *
     * @param string $goBackLink
     */
    public function setGoBackLink($goBackLink)
    {
        $this->goBackLink = $goBackLink;
    }

    /**
     * MerchantName getter.
     *
     * @return string
     */
    public function getMerchantName()
    {
        return $this->merchanName;
    }

    /**
     * MerchantName setter.
     *
     * @param string $goBackLink
     */
    public function setMerchantName($merchantName)
    {
        $this->merchanName = $merchantName;
    }

    /**
     * Logo getter.
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logoUrl;
    }

    /**
     * Logo setter.
     *
     * @param string $goBackLink
     */
    public function setLogo($logoUrl)
    {
        $this->logoUrl = $logoUrl;
    }

    /**
     * @return string
     */
    public function getMerchanName(): ?string
    {
        return $this->merchanName;
    }

    /**
     * @param string $merchanName
     */
    public function setMerchanName(string $merchanName): void
    {
        $this->merchanName = $merchanName;
    }

    /**
     * @return string
     */
    public function getCardNumber(): ?string
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     */
    public function setCardNumber(string $cardNumber): void
    {
        $this->cardNumber = $cardNumber;
    }

    /**
     * @return string
     */
    public function getCreditCardExpMonth(): ?string
    {
        return $this->creditCardExpMonth;
    }

    /**
     * @param string $creditCardExpMonth
     */
    public function setCreditCardExpMonth(string $creditCardExpMonth): void
    {
        $this->creditCardExpMonth = $creditCardExpMonth;
    }

    /**
     * @return string
     */
    public function getCreditCardExpYear(): ?string
    {
        return $this->creditCardExpYear;
    }

    /**
     * @param string $creditCardExpYear
     */
    public function setCreditCardExpYear(string $creditCardExpYear): void
    {
        $this->creditCardExpYear = $creditCardExpYear;
    }
}
