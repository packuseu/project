<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Communication;

use GuzzleHttp\Client;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use Omni\Sylius\SwedbankSpp\Client\SwedbankSppClient;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Handles communication with guzzle.
 */
class AbstractCommunication implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var Client
     */
    protected $guzzleClient;

    /**
     * AbstractCommunication constructor.
     *
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
        $this->guzzleClient = new Client();
    }

    /**
     * @param      $request
     * @param      $responseClass
     * @param      $endpoint
     * @param bool $returnArray
     *
     * @return array|\JMS\Serializer\scalar|mixed|object
     */
    public function sendData($request, $responseClass, $endpoint, $returnArray = false)
    {
        $requestXml = $this->serializer->serialize($request, 'xml');

        $this->logger->info($requestXml);

        $responseObject = $this->guzzleClient->post(
            $endpoint,
            [
                'body' => $requestXml
            ]
        );

        $this->logger->info($responseObject->getStatusCode());

        if ($responseObject->getStatusCode() !== 200) {
            throw new \RuntimeException('Got not 200 response.(status code: ' . $responseObject->getStatusCode() . ')');
        }

        $rawResponse = (string)$responseObject->getBody();
        $this->logger->info($rawResponse);

        $response = $this->serializer->deserialize($rawResponse, $responseClass, 'xml');

        return $response;
    }
}
