<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class GatewayConfigurationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'login',
                TextType::class,
                [
                    'label' => 'omni_swedbank_spp.form.login.label',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'omni_swedbank_spp.form.login.not_blank',
                                'groups' => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'password',
                TextType::class,
                [
                    'label' => 'omni_swedbank_spp.form.password.label',
                    'constraints' => [
                        new NotBlank(
                            [
                                'message' => 'omni_swedbank_spp.form.password.not_blank',
                                'groups' => 'sylius',
                            ]
                        )
                    ],
                ]
            )
            ->add(
                'test',
                CheckboxType::class,
                [
                    'label' => 'omni_swedbank_spp.form.test_mode',
                ]
            )
            ->add(
                'use_token',
                CheckboxType::class,
                [
                    'label' => 'omni_swedbank_spp.form.use_token',
                ]
            )
            ->add(
                'goBackUrl',
                TextType::class,
                [
                    'label' => 'omni_swedbank_spp.form.go_back_url',
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'sylius_payum_gateway_config';
    }
}
