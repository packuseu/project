<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Payum\Action\CreditCard;

use Sylius\Bundle\PayumBundle\Model\GatewayConfigInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Omni\Sylius\SwedbankSpp\Client\SwedbankSppClient;
use Omni\Sylius\SwedbankSpp\Communication\SetupTransactionRequest;
use Omni\Sylius\SwedbankSpp\Communication\Transaction;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\Action;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\Amount;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\BillingDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\CustomerDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\HpsTxn;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\MerchantConfiguration;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\PersonalDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\RiskDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\ShippingDetails;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\ThreeDSecure;
use Omni\Sylius\SwedbankSpp\Communication\Transaction\TxnDetails;
use Omni\Sylius\SwedbankSpp\Constants\SwedbankConstants;
use Omni\Sylius\SwedbankSpp\Payum\Action\CreditCard\Api\ApiAwareTrait;
use Omni\Sylius\SwedbankSpp\Response\SetupResponse;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpRedirect;
use Payum\Core\Request\Capture;
use Payum\Core\Exception\RequestNotSupportedException;
use Sylius\Component\Core\Model\ChannelInterface;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\PaymentMethodInterface;

class CaptureAction implements ActionInterface, GatewayAwareInterface, ApiAwareInterface
{
    use GatewayAwareTrait, ApiAwareTrait;

    const PROD_PAGE_LANGUAGE_IDS = [
        'en' => 2207,
        'en_us' => 2207,
        'lt' => 2213,
        'lt_lt' => 2213,
        'lv' => 2211,
        'lv_lv' => 2211,
        'et' => 2209,
        'et_ee' => 2209,
        'ru' => 2215,
        'ru_ru' => 2215,
    ];

    const TEST_PAGE_LANGUAGE_IDS = [
        'en' => 164,
        'en_us' => 164,
        'lt' => 170,
        'lt_lt' => 170,
        'lv' => 168,
        'lv_lv' => 168,
        'et' => 166,
        'et_ee' => 166,
        'ru' => 172,
        'ru_ru' => 172,
    ];

    /**
     * @var SwedbankSppClient
     */
    private $sppClient;

    /**
     * @var SwedbankPaymentPortal
     */
    private $sppClientPortal;

    /**
     * CaptureAction constructor.
     */
    public function __construct()
    {
        $this->sppClient = new SwedbankSppClient();
    }

    /**
     * {@inheritDoc}
     *
     * @param Capture $request
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        /** @var PaymentInterface $payment */
        $payment = $request->getFirstModel();

        $order = $payment->getOrder();

        $gatewayConfig = $payment->getMethod()->getGatewayConfig()->getConfig();

        $authentication = $this->sppClient->getAuthentication($gatewayConfig);

        $hpsTxn = $this->initializeHps($request, $order);
        $riskAction = $this->initializePaymentRiskData($order);
        $txnDetails = $this->initializePaymentDetail($riskAction, $order, $payment);

        /** @var Transaction $transaction */
        $transaction = new Transaction(
            $txnDetails,
            $hpsTxn,
            new Transaction\CardTxn()
        );

        /** @var SetupTransactionRequest $setupRequest */
        $setupRequest = new SetupTransactionRequest(
            $authentication,
            $transaction
        );

        /** @var SetupResponse $response */
        $response = $this->communication->sendData(
            $setupRequest,
            SetupResponse::class,
            $this->sppClient->getEndpointUrl($gatewayConfig['test'])
        );

        if ($response->getStatus() === 1) {
            $url = $response->getHpsTxn()->getHpsUrl()
                . '?HPS_SessionID=' .
                $response->getHpsTxn()->getSessionId();

            throw new HttpRedirect($url);
        }
    }

    /**
     * @param mixed $request
     *
     * @return bool
     */
    public function supports($request): bool
    {
        return
            $request instanceof Capture &&
            $request->getModel() instanceof \ArrayAccess;
    }

    /**
     * @param                $request
     * @param OrderInterface $order
     *
     * @return HpsTxn
     */
    private function initializeHps($request, OrderInterface $order): HpsTxn
    {
        /** @var PaymentInterface $payment */
        $payment = $request->getFirstModel();

        $locale = strtolower($order->getLocaleCode());
        $gatewayConfig = $payment->getMethod()->getGatewayConfig()->getConfig();

        if (isset($gatewayConfig['test']) && $gatewayConfig['test'] === true) {
            $pageLanguageIds = self::TEST_PAGE_LANGUAGE_IDS;
        } else {
            $pageLanguageIds = self::PROD_PAGE_LANGUAGE_IDS;
        }

        $pageSetId = $pageLanguageIds[$locale] ?? $pageLanguageIds['en'];

        return new HpsTxn(
            $request->getToken()->getAfterUrl() . '&' . http_build_query(['status' => SwedbankConstants::STATUS_EXPIRY]),
            $request->getToken()->getAfterUrl() . '&' . http_build_query(['status' => SwedbankConstants::STATUS_PROCESSING]),
            $request->getToken()->getAfterUrl() . '&' . http_build_query(['status' => SwedbankConstants::STATUS_FAILED]),
            $pageSetId ?? 2207,
            $this->getDynamicData($order, $payment),
            1,
            $this->getCustomerCardData($order->getCustomer(), $payment->getMethod())
        );
    }

    /**
     * @param Transaction\Action $riskAction
     * @param OrderInterface $order
     * @return TxnDetails
     * @throws \Exception
     */
    private function initializePaymentDetail($riskAction, OrderInterface $order, PaymentInterface $payment): TxnDetails
    {
        return new TxnDetails(
            $riskAction,
            $order->getNumber(),
            new Amount($order->getTotal() / 100 ),
            $this->appendThreeDSecurity($order, $payment)
        );
    }

    /**
     * @param OrderInterface $order
     * @param PaymentInterface $payment
     *
     * @return ThreeDSecure|null
     */
    private function appendThreeDSecurity(OrderInterface $order, PaymentInterface $payment): ?ThreeDSecure
    {
        $customer = $order->getCustomer();
        $gatewayConfig = $payment->getMethod()->getGatewayConfig();

        $threedSecurity = new ThreeDSecure(
            $order->getNumber(),
            $this->getChannelHost($order->getChannel()),
            new \DateTime()
        );

        if ($this->getCreditCardToken($customer, $gatewayConfig) === null || $order->getTotal() > 3000) {
            return $threedSecurity;
        }

        return null;
    }

    /**
     * @param OrderInterface $order
     * @return Action
     */
    private function initializePaymentRiskData(OrderInterface $order): Action
    {
        $riskAction = new Action(
            new MerchantConfiguration(
                'W',
                $order->getChannel()->getCountries()->first()->getName()
            ),
            new CustomerDetails(
                new BillingDetails(
                    'Mr',
                    $order->getBillingAddress()->getFullName(),
                    $order->getBillingAddress()->getPostcode(),
                    $order->getBillingAddress()->getStreet(),
                    '',
                    $order->getBillingAddress()->getCity(),
                    $order->getBillingAddress()->getCountryCode()
                ),
                new PersonalDetails(
                    $order->getBillingAddress()->getFirstName(),
                    $order->getBillingAddress()->getLastName(),
                    $order->getShippingAddress()->getPhoneNumber()
                ),
                new ShippingDetails(
                    'Mr', // title
                    $order->getShippingAddress()->getFirstName(),
                    $order->getShippingAddress()->getLastName(),
                    $order->getShippingAddress()->getStreet(),
                    '',
                    $order->getShippingAddress()->getCity(),
                    $order->getShippingAddress()->getCountryCode(),
                    $order->getShippingAddress()->getPostcode()
                ),
                new RiskDetails(
                    $order->getCustomerIp(),
                    $order->getCustomer()->getEmail()
                )
            )
        );

        return $riskAction;
    }

    /**
     * @param ChannelInterface $channel
     *
     * @return string
     */
    private function getChannelHost(ChannelInterface $channel): string
    {
        return 'https://' . $channel->getHostname();
    }

    /**
     * @param CustomerInterface $customer
     *
     * @return Transaction\Card|null
     */
    private function getCustomerCardData(CustomerInterface $customer, PaymentMethodInterface $paymentMethod): ? Transaction\Card
    {
        $paymentConfig = $paymentMethod->getGatewayConfig();
        $creditCardToken = $this->getCreditCardToken($customer, $paymentConfig);

        if (null !== $creditCardToken) {
            return new Transaction\Card($creditCardToken);
        }

        return null;
    }

    /**
     * @param OrderInterface $order
     * @param PaymentInterface $payment
     *
     * @return Transaction\DynamicData
     * @throws \Exception
     */
    private function getDynamicData(OrderInterface $order, PaymentInterface $payment): Transaction\DynamicData
    {
        $gatewayConfig = $payment->getMethod()->getGatewayConfig();
        $customer = $order->getCustomer();

        $goBackUrl = $this->getChannelHost($order->getChannel());

        if (false === empty($gatewayConfig->getConfig()['goBackUrl'])) {
            $goBackUrl .= $gatewayConfig->getConfig()['goBackUrl'];
        }

        if (null !== $this->getCreditCardToken($customer, $gatewayConfig)) {
            return new Transaction\DynamicData(
                null,
                $goBackUrl,
                $order->getChannel()->getName(),
                null,
                $customer->getPan(),
                $customer->getCardExpMonth(),
                $customer->getCardExpYear()
            );
        }

        return new Transaction\DynamicData(
            null,
            $goBackUrl,
            $order->getChannel()->getName()
        );
    }

    /**
     * @param CustomerInterface $customer
     * @param GatewayConfigInterface $paymentGatewayConfig
     * @return string|null
     */
    private function getCreditCardToken(
        CustomerInterface $customer,
        GatewayConfigInterface $paymentGatewayConfig
    ):? string {
        if (false === $this->useCreditCardTokenToken($paymentGatewayConfig)) {
            return null;
        }

        return $customer->getCreditCardToken();
    }

    /**
     * @param GatewayConfigInterface $paymentGatewayConfig
     * @return bool
     */
    private function useCreditCardTokenToken(GatewayConfigInterface $paymentGatewayConfig): bool
    {
        return true === $paymentGatewayConfig->getConfig()['use_token'];
    }
}
