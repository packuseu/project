<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Model;

trait CustomerCreditCardTrait
{
    /**
     * @var string|null
     */
    protected $creditCardToken;

    /**
     * @var string|null
     */
    protected $pan;

    /**
     * @var string|null
     */
    protected $cardExpMonth;

    /**
     * @var string|null
     */
    protected $cardExpYear;

    /**
     * @return string|null
     */
    public function getCreditCardToken(): ?string
    {
        return $this->creditCardToken;
    }

    /**
     * @param string|null $creditCardToken
     */
    public function setCreditCardToken(?string $creditCardToken): void
    {
        $this->creditCardToken = $creditCardToken;
    }

    /**
     * @return string|null
     */
    public function getPan(): ?string
    {
        return $this->pan;
    }

    /**
     * @param string|null $pan
     */
    public function setPan(?string $pan): void
    {
        $this->pan = $pan;
    }

    /**
     * @return string|null
     */
    public function getCardExpMonth(): ?string
    {
        return $this->cardExpMonth;
    }

    /**
     * @param string|null $cardExpMonth
     */
    public function setCardExpMonth(?string $cardExpMonth): void
    {
        $this->cardExpMonth = $cardExpMonth;
    }

    /**
     * @return istringnt|null
     */
    public function getCardExpYear(): ?string
    {
        return $this->cardExpYear;
    }

    /**
     * @param string|null $cardExpYear
     */
    public function setCardExpYear(?string $cardExpYear): void
    {
        $this->cardExpYear = $cardExpYear;
    }
}
