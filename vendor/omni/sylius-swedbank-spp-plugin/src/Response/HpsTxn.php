<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Response;

use JMS\Serializer\Annotation;

/**
 * The container for the XML response.
 *
 * @Annotation\AccessType("public_method")
 */
class HpsTxn
{
    /**
     * The gateway URL to which the customer is to be redirected.
     *
     * @var string
     *
     * @Annotation\Type("string")
     * @Annotation\SerializedName("hps_url")
     * @Annotation\XmlElement(cdata=false)
     */
    private $hpsUrl;

    /**
     * The hosted session which is used in conjunction with the URL to redirect the customer.
     *
     * @var string
     *
     * @Annotation\Type("string")
     * @Annotation\SerializedName("session_id")
     * @Annotation\XmlElement(cdata=false)
     */
    private $sessionId;

    /**
     * HpsTXN constructor.
     *
     * @param string $hpsUrl
     * @param string $sessionId
     */
    public function __construct($hpsUrl, $sessionId)
    {
        $this->hpsUrl = $hpsUrl;
        $this->sessionId = $sessionId;
    }

    /**
     * HpsUrl getter.
     *
     * @return string
     */
    public function getHpsUrl()
    {
        return $this->hpsUrl;
    }

    /**
     * HpsUrl setter.
     *
     * @param string $hpsUrl
     */
    public function setHpsUrl($hpsUrl)
    {
        $this->hpsUrl = $hpsUrl;
    }

    /**
     * SessionId getter.
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * SessionId setter.
     *
     * @param string $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }
}
