<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\SwedbankSpp\Resolver;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\SwedbankSpp\Client\SwedbankSppClient;
use Omni\Sylius\SwedbankSpp\Constants\SwedbankConstants;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use SM\Factory\FactoryInterface;
use SwedbankPaymentPortal\Options\CommunicationOptions;
use SwedbankPaymentPortal\Options\ServiceOptions;
use SwedbankPaymentPortal\SharedEntity\Authentication;
use SwedbankPaymentPortal\SwedbankPaymentPortal;
use Sylius\Component\Core\Model\CustomerInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Sylius\Component\Core\Model\PaymentMethodInterface;
use Sylius\Component\Payment\PaymentTransitions;

class PaymentStateResolver implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var FactoryInterface
     */
    private $stateMachineFactory;

    /**
     * @var AmazonPayApiClientInterface
     */
    private $amazonPayApiClient;

    /**
     * @var EntityManagerInterface
     */
    private $paymentEntityManager;

    /**
     * @var SwedbankSppClient
     */
    private $swedbankSppClient;

    /**
     * @var
     */
    private $sppClient;

    /**
     * PaymentStateResolver constructor.
     * @param FactoryInterface $stateMachineFactory
     * @param EntityManagerInterface $paymentEntityManager
     * @param SwedbankSppClient $swedbankSppClient
     */
    public function __construct(
        FactoryInterface $stateMachineFactory,
        EntityManagerInterface $paymentEntityManager,
        SwedbankSppClient $swedbankSppClient
    ) {
        $this->stateMachineFactory = $stateMachineFactory;
        $this->paymentEntityManager = $paymentEntityManager;
        $this->swedbankSppClient = $swedbankSppClient;
    }

    /**
     * @param PaymentInterface $payment
     * @throws \SM\SMException
     */
    public function resolve(PaymentInterface $payment): void
    {
        /** @var PaymentMethodInterface $paymentMethod */
        $paymentMethod = $payment->getMethod();

        /** @var CustomerInterface $customer */
        $customer = $payment->getOrder()->getCustomer();
        $gatewayConfig = $paymentMethod->getGatewayConfig()->getConfig();

        if (!$this->sppClient instanceof SwedbankPaymentPortal) {
            $this->sppClient = $this->swedbankSppClient->initializePortal($gatewayConfig);
        }

        $result = $this->sppClient->getPaymentCardHostedPagesGateway()->query($payment->getOrder()->getNumber());

        $this->logger->info(
            json_encode(
                [
                    'orderNumber' => $payment->getOrder()->getNumber(),
                    'body' => $result,
                ]
            )
        );

        if (null === $result) {
            return;
        }

        $paymentStateMachine = $this->stateMachineFactory->get($payment, PaymentTransitions::GRAPH);

        if ((int)$result['status'] === SwedbankConstants::STATUS_CODE_ACCEPTED) {
            if ($paymentStateMachine->can(PaymentTransitions::TRANSITION_COMPLETE)) {
                $paymentStateMachine->apply(PaymentTransitions::TRANSITION_COMPLETE);
            }

            if (isset($result['QueryTxnResult']['Card']) && $gatewayConfig['use_token'] === true) {
                $customer->setCreditCardToken($result['QueryTxnResult']['Card']['token']);
                $customer->setPan($result['QueryTxnResult']['Card']['pan']);

                $expDate = explode('/', $result['QueryTxnResult']['Card']['expirydate']);
                $dt = \DateTime::createFromFormat('y', $expDate[1]);
                $customer->setCardExpYear($dt->format('Y'));
                $customer->setCardExpMonth($expDate[0]);

                $this->paymentEntityManager->persist($customer);
            }
        }

        $this->paymentEntityManager->flush();
    }
}
