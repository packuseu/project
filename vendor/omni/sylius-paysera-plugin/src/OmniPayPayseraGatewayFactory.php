<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin;

use Omni\Sylius\PayseraPlugin\Action\CaptureAction;
use Omni\Sylius\PayseraPlugin\Action\NotifyAction;
use Omni\Sylius\PayseraPlugin\Action\StatusAction;
use Omni\Sylius\PayseraPlugin\Extension\PresetPaymentMethodExtension as PresetMethod;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\LogicException;
use Payum\Core\GatewayFactory;
use Payum\Core\GatewayFactoryInterface;

class OmniPayPayseraGatewayFactory extends GatewayFactory
{
    /**
     * {@inheritDoc}
     */
    protected function populateConfig(ArrayObject $config)
    {
        if (false == class_exists(\WebToPay::class)) {
            throw new LogicException('You must install "WebToPay" library.');
        }

        if ($config['paymentMethod']) {
            $config->defaults(
                [
                    'payum.extension.paysera.payment_method' => new PresetMethod($config['paymentMethod'])
                ]
            );
        }


        $config->defaults(
            [
                'payum.factory_name' => 'omnipay_paysera',
                'payum.factory_title' => 'Paysera',
                'payum.action.status' => new StatusAction(),
                'payum.action.capture' => new CaptureAction(),
            ]
        );

        if (false == $config['payum.api']) {
            $projectConfig = $this->getProjectConfig($config);

            $config['payum.default_options'] = array(
                'test' => $projectConfig['test_mode'],
                'projectid' => $projectConfig['project_id'],
                'sign_password' => $projectConfig['password'],
            );

            if ($config['options']) {
                $config->defaults($config['options']);
            }

            $config->defaults($config['payum.default_options']);

            $config['payum.required_options'] = [
                'projectid', 'sign_password'
            ];

            $config['payum.api'] = function (ArrayObject $config) {
                $config->validateNotEmpty($config['payum.required_options']);

                return new Api((array)$config);
            };
        }
    }

    /**
     * @param ArrayObject $config
     *
     * @return array
     */
    protected function getProjectConfig(ArrayObject $config): array
    {
        if ($config['project']) {
            if (isset($config['projects'][$config['project']])) {
                return $config['projects'][$config['project']];
            }

            throw new LogicException(sprintf('Paysera project \'%s\' is not configured.', $config['project']));
        }

        return $config['projects']['default'];
    }
}
