<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Action;

use Payum\Core\Action\ActionInterface;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Request\Convert;
use Payum\Core\Security\GenericTokenFactoryAwareInterface;
use Payum\Core\Security\GenericTokenFactoryAwareTrait;
use Sylius\Component\Core\Context\ShopperContextInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\PaymentInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ConvertPaymentAction implements ActionInterface, GenericTokenFactoryAwareInterface, GatewayAwareInterface
{
    use GatewayAwareTrait;
    use GenericTokenFactoryAwareTrait;

    const BASE_CURRENCY = 'EUR';
    const BASE_COUNTRY_CODE = 'LIT';

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var ShopperContextInterface
     */
    private $context;

    /**
     * @var bool
     */
    private $useOrderNumber = false;

    /**
     * ConvertPaymentPayseraAction constructor.
     *
     * @param UrlGeneratorInterface $router
     * @param ShopperContextInterface $context
     * @param bool $useOrderNumber
     */
    public function __construct(UrlGeneratorInterface $router, ShopperContextInterface $context, bool $useOrderNumber)
    {
        $this->router = $router;
        $this->context = $context;
        $this->useOrderNumber = $useOrderNumber;
    }

    /**
     * {@inheritdoc}
     *
     * @param Convert $request
     */
    public function execute($request): void
    {
        /** @var PaymentInterface $payment */
        $payment = $request->getSource();
        /** @var OrderInterface $order */
        $order = $payment->getOrder();
        $customer = $order->getCustomer();
        $shipping = $order->getShippingAddress();

        $notifyToken = $this->tokenFactory->createNotifyToken(
            $request->getToken()->getGatewayName(),
            $request->getToken()->getDetails()
        );

        $request->setResult(
            [
                'version' => \WebToPay::VERSION,
                'orderid' => $this->useOrderNumber ? $order->getNumber() : (string)$order->getId(),
                'accepturl' => $request->getToken()->getAfterUrl(),
                'callbackurl' => $notifyToken->getTargetUrl(),
                'cancelurl' => $this->router->generate(
                    'sylius_shop_account_order_index',
                    [
                        '_locale' => $this->context->getLocaleCode(),
                    ],
                    UrlGeneratorInterface::ABSOLUTE_URL
                ),
                'payment' => null,
                'language' => self::BASE_COUNTRY_CODE,
                'currency' => self::BASE_CURRENCY,
                'amount' => $payment->getAmount(),
                'p_firstname' => $customer->getFirstName(),
                'p_lastname' => $customer->getLastName(),
                'p_email' => $customer->getEmail(),
                'p_city' => $shipping->getCity(),
                'p_street' => $shipping->getStreet(),
                'p_zip' => $shipping->getPostcode(),
                'p_countrycode' => $shipping->getCountryCode()
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request): bool
    {
        return $request instanceof Convert
            && $request->getSource() instanceof PaymentInterface
            && $request->getTo() === 'array';
    }
}
