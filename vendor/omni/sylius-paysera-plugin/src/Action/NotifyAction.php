<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Action;

use Omni\Sylius\PayseraPlugin\Api;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\RequestNotSupportedException;
use Payum\Core\GatewayAwareInterface;
use Payum\Core\GatewayAwareTrait;
use Payum\Core\Reply\HttpResponse;
use Payum\Core\Request\GetHttpRequest;
use Payum\Core\Request\Notify;
use Sylius\Component\Payment\PaymentTransitions;
use SM\Factory\FactoryInterface;

class NotifyAction implements ActionInterface, ApiAwareInterface, GatewayAwareInterface
{
    use ApiAwareTrait;
    use GatewayAwareTrait;

    /**
     * @var FactoryInterface
     */
    private $smFactory;

    /**
     * NotifyAction constructor.
     *
     * @param FactoryInterface $smFactory
     */
    public function __construct(FactoryInterface $smFactory)
    {
        $this->apiClass = Api::class;
        $this->smFactory = $smFactory;
    }

    /**
     * @param mixed $request
     * @throws \WebToPayException
     */
    public function execute($request)
    {
        RequestNotSupportedException::assertSupports($this, $request);

        $model = ArrayObject::ensureArrayObject($request->getModel());

        $this->gateway->execute($httpRequest = new GetHttpRequest());

        $response = $this->api->doNotify($httpRequest->query);

        if (!$response) {
            throw new \WebToPayException('Wrong parameters');
        }

        switch ($response['status']) {
            case '0':
                $model['status'] = 'FAILED';
                break;
            case '1':
                $model['status'] = 'COMPLETED';
                if ($request->getFirstModel()->getState() !== 'completed') {
                    $sm = $this->smFactory->get($request->getFirstModel(), PaymentTransitions::GRAPH);
                    $sm->apply(PaymentTransitions::TRANSITION_COMPLETE);
                }
                break;
            case '2':
                $model['status'] = 'NOT_EXECUTED';
                break;
        }

        throw new HttpResponse('OK');
    }

    /**
     * {@inheritDoc}
     */
    public function supports($request)
    {
        return
            $request instanceof Notify &&
            $request->getModel() instanceof \ArrayAccess;
    }
}
