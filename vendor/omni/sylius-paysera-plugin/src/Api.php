<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin;

use function League\Uri\create;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Reply\HttpPostRedirect;
use WebToPay;

/**
 * Class Api
 *
 * @package Omni\Sylius\PayseraPlugin
 */
class Api
{
    /**
     * @var mixed
     */
    protected $options = [];

    /**
     * Api constructor.
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $options = ArrayObject::ensureArrayObject($options);

        $options->defaults($this->options);
        $this->options = $options;
    }

    /**
     * @param array $fields
     *
     * @throws \WebToPayException
     */
    public function doPayment(array $fields)
    {
        $fields['projectid'] = $this->options['projectid'];
        $fields['sign_password'] = $this->options['sign_password'];
        $fields['test'] = $this->options['test'];

        $authorizeTokenUrl = $this->getApiEndpoint();

        $data = WebToPay::buildRequest($fields);

        throw new HttpPostRedirect($authorizeTokenUrl, $data);
    }

    /**
     * @param array $fields
     *
     * @return array
     * @throws \WebToPayException
     */
    public function doNotify(array $fields)
    {
        return WebToPay::validateAndParseData($fields, $this->options['projectid'], $this->options['sign_password']);
    }

    public function setProjectConfigs($projectConfigs)
    {
        $this->projectConfigs = $projectConfigs;
    }

    /**
     * @return string
     */
    protected function getApiEndpoint()
    {
        return WebToPay::PAY_URL;
    }

    public function getOptions()
    {
        return $this->options;
    }
}
