<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('omni_sylius_paysera');

        $rootNode
            ->beforeNormalization()
            ->ifTrue(function ($v) {
                return is_array($v) && ! array_key_exists('projects', $v);
            })
            ->then(function ($v) {
                $project = [];
                foreach ($v as $key => $value) {
                    $project[$key] = $v[$key];
                    unset($v[$key]);
                }
                $v['default_project'] = isset($v['default_project']) ? (string) $v['default_project'] : 'default';
                $v['projects']        = [$v['default_project'] => $project];

                return $v;
            })
            ->end()
            ->children()
                ->scalarNode('default_project')->end()
                ->booleanNode('use_order_number')->defaultFalse()->end()
                ->arrayNode('projects')
                    ->requiresAtLeastOneElement()
                    ->prototype('array')
                        ->children()
                            ->scalarNode('project_id')->isRequired()->end()
                            ->scalarNode('password')->isRequired()->end()
                            ->scalarNode('test_mode')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
