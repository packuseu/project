<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Model;

use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableInterface;

interface SeoMetadataInterface extends ResourceInterface, TimestampableInterface
{
    /**
     * @param string|null $lang
     * @return $this
     */
    public function setLang(?string $lang);

    /**
     * @return string|null
     */
    public function getLang(): ?string;

    /**
     * @param string|null $metaDescription
     * @return $this
     */
    public function setMetaDescription(?string $metaDescription);

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string;

    /**
     * @param string|null $metaKeywords
     * @return $this
     */
    public function setMetaKeywords(?string $metaKeywords);

    /**
     * @return string|null
     */
    public function getMetaKeywords(): ?string;

    /**
     * @param string|null $metaRobots
     * @return $this
     */
    public function setMetaRobots(?string $metaRobots);

    /**
     * @return string|null
     */
    public function getMetaRobots(): ?string;

    /**
     * @param string|null $originalUrl
     * @return $this
     */
    public function setOriginalUrl(?string $originalUrl);

    /**
     * @return string|null
     */
    public function getOriginalUrl(): ?string;

    /**
     * @param string|null $title
     * @return $this
     */
    public function setTitle(?string $title);

    /**
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * @return array
     */
    public function getExtraHttp(): array;

    /**
     * @param array $extraHttp
     * @return $this
     */
    public function setExtraHttp(array $extraHttp);

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function addExtraHttp(string $key, string $value);

    /**
     * @param string $key
     * @return $this
     */
    public function removeExtraHttp(string $key);

    /**
     * @return array
     */
    public function getExtraNames(): array;

    /**
     * @param array $extraNames
     * @return $this
     */
    public function setExtraNames(array $extraNames);

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function addExtraName(string $key, string $value);

    /**
     * @param string $key
     * @return $this
     */
    public function removeExtraName(string $key);

    /**
     * @return array
     */
    public function getExtraProperties(): array;

    /**
     * @param array $extraProperties
     * @return $this
     */
    public function setExtraProperties(array $extraProperties);

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function addExtraProperty(string $key, string $value);

    /**
     * @param string $key
     * @return $this
     */
    public function removeExtraProperty(string $key);
}
