<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Model;

use Sylius\Component\Resource\Model\TimestampableTrait;

class SeoMetadata implements SeoMetadataInterface
{
    use TimestampableTrait;

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $lang;

    /**
     * @var string|null
     */
    private $metaDescription;

    /**
     * @var string|null
     */
    private $metaKeywords;

    /**
     * @var string|null
     */
    private $metaRobots;

    /**
     * @var string|null
     */
    private $originalUrl;

    /**
     * @var string|null
     */
    private $title;

    /**
     * @var array
     */
    private $extraHttp;

    /**
     * @var array
     */
    private $extraNames;

    /**
     * @var array
     */
    private $extraProperties;

    public function __construct()
    {
        $this->extraHttp = array();
        $this->extraNames = array();
        $this->extraProperties = array();
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setLang(?string $lang): self
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLang(): ?string
    {
        return $this->lang;
    }

    /**
     * {@inheritdoc}
     */
    public function setMetaDescription(?string $metaDescription): self
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * {@inheritdoc}
     */
    public function setMetaKeywords(?string $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * {@inheritdoc}
     */
    public function setMetaRobots(?string $metaRobots): self
    {
        $this->metaRobots = $metaRobots;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getMetaRobots(): ?string
    {
        return $this->metaRobots;
    }

    /**
     * {@inheritdoc}
     */
    public function setOriginalUrl(?string $originalUrl): self
    {
        $this->originalUrl = $originalUrl;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getOriginalUrl(): ?string
    {
        return $this->originalUrl;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function getExtraHttp(): array
    {
        return $this->extraHttp;
    }

    /**
     * {@inheritdoc}
     */
    public function setExtraHttp(array $extraHttp): self
    {
        $this->extraHttp = $extraHttp;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addExtraHttp(string $key, string $value): self
    {
        $this->extraHttp[$key] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeExtraHttp(string $key): self
    {
        if (isset($this->extraHttp[$key])) {
            unset($this->extraHttp[$key]);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getExtraNames(): array
    {
        return $this->extraNames;
    }

    /**
     * {@inheritdoc}
     */
    public function addExtraName(string $key, string $value): self
    {
        $this->extraNames[$key] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeExtraName(string $key): self
    {
        if (isset($this->extraNames[$key])) {
            unset($this->extraNames[$key]);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setExtraNames(array $extraNames): self
    {
        $this->extraNames = $extraNames;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getExtraProperties(): array
    {
        return $this->extraProperties;
    }

    /**
     * {@inheritdoc}
     */
    public function setExtraProperties(array $extraProperties): self
    {
        $this->extraProperties = $extraProperties;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addExtraProperty(string $key, string $value): self
    {
        $this->extraProperties[$key] = $value;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeExtraProperty(string $key): self
    {
        if (isset($this->extraProperties[$key])) {
            unset($this->extraProperties[$key]);
        }

        return $this;
    }
}
