<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Controller;

use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler as RestViewHandler;
use Omni\Sylius\SeoPlugin\Model\SeoAwareInterface;
use Omni\Sylius\SeoPlugin\SeoPresentation;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Bundle\ResourceBundle\Controller\ViewHandlerInterface;
use Sylius\Component\Resource\Model\ResourceInterface;
use Symfony\Component\HttpFoundation\Response;

final class ViewHandler implements ViewHandlerInterface
{
    /**
     * @var RestViewHandler
     */
    private $restViewHandler;

    /**
     * @var SeoPresentation
     */
    private $presentation;

    /**
     * @var array
     */
    private $viewParameters;

    /**
     * @param RestViewHandler $restViewHandler
     * @param SeoPresentation $presentation
     * @param array $viewParameters
     */
    public function __construct(
        RestViewHandler $restViewHandler,
        SeoPresentation $presentation,
        array $viewParameters
    ) {
        $this->restViewHandler = $restViewHandler;
        $this->presentation = $presentation;
        $this->viewParameters = $viewParameters;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(RequestConfiguration $requestConfiguration, View $view): Response
    {
        $this->handleSeoMetadata($view);

        if (!$requestConfiguration->isHtmlRequest()) {
            $this->restViewHandler->setExclusionStrategyGroups($requestConfiguration->getSerializationGroups());

            if ($requestConfiguration->getSerializationVersion()) {
                $this->restViewHandler->setExclusionStrategyVersion($requestConfiguration->getSerializationVersion());
            }

            $view->getContext()->enableMaxDepth();
        }

        return $this->restViewHandler->handle($view);
    }

    /**
     * @param View $view
     */
    private function handleSeoMetadata(View $view): void
    {
        $resource = $this->getResource($view);

        if ($this->isResourceSeoCompatible($resource)) {
            $this->presentation->updateSeoPage($resource->getSeoMetadata());
        }
    }

    /**
     * @param View $view
     * @return ResourceInterface
     */
    private function getResource(View $view): ?ResourceInterface
    {
        if (false === is_array($view->getData())) {
            return null;
        }

        foreach ($this->viewParameters as $parameter) {
            if (isset($view->getData()[$parameter])) {
                return $view->getData()[$parameter];
            }
        }

        return null;
    }

    /**
     * @param ResourceInterface $resource
     * @return bool
     */
    private function isResourceSeoCompatible(?ResourceInterface $resource): bool
    {
        return null !== $resource && $resource instanceof SeoAwareInterface && null !== $resource->getSeoMetadata();
    }
}
