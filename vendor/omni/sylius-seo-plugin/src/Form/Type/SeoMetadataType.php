<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Form\Type;

use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class SeoMetadataType extends AbstractResourceType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'title',
            TextType::class,
            [
                'label' => 'omni_sylius.form.seo.page_title',
                'required' => false,
                'constraints' => [
                    new Length(
                        [
                            'max' => 155,
                            'groups' => ['omni', 'sylius'],
                        ]
                    )
                ],
            ]
        );

        $builder->add(
            'originalUrl',
            TextType::class,
            [
                'label' => 'omni_sylius.form.seo.original_url',
                'required' => false,
            ]
        );

        $builder->add(
            'metaDescription',
            TextareaType::class,
            [
                'label' => 'omni_sylius.form.seo.meta_description',
                'required' => false,
                'constraints' => [
                    new Length(
                        [
                            'max' => 344,
                            'groups' => ['omni', 'sylius'],
                        ]
                    )
                ],
            ]
        );

        $builder->add(
            'metaKeywords',
            TextType::class,
            [
                'label' => 'omni_sylius.form.seo.meta_keywords',
                'required' => false,
            ]
        );

        $builder->add(
            'metaRobots',
            TextareaType::class,
            [
                'label' => 'omni_sylius.form.seo.meta_robots',
                'required' => false,
            ]
        );

        $builder->add(
            'extraHttp',
            KeyValueType::class,
            [
                'label' => 'omni_sylius.form.seo.extra_http',
                'required' => false,
                'value_type' => 'text',
            ]
        );

        $builder->add(
            'extraNames',
            KeyValueType::class,
            [
                'label' => 'omni_sylius.form.seo.extra_names',
                'required' => false,
                'value_type' => 'text',
            ]
        );

        $builder->add(
            'extraProperties',
            KeyValueType::class,
            [
                'label' => 'omni_sylius.form.seo.extra_properties',
                'required' => false,
                'value_type' => 'text',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'omni_sylius_seo_metadata';
    }
}
