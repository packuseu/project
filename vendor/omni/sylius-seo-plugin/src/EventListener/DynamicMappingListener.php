<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\EventListener;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Omni\Sylius\SeoPlugin\Model\SeoAwareTranslationInterface;

class DynamicMappingListener
{
    /**
     * @var string
     */
    private $metadataClass;

    public function __construct(string $metadataClass)
    {
        $this->metadataClass = $metadataClass;
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        /** @var \Doctrine\ORM\Mapping\ClassMetadata $metadata */
        $metadata = $eventArgs->getClassMetadata();

        if (false === in_array(SeoAwareTranslationInterface::class, class_implements($metadata->getName()))) {
            return;
        }

        $metadata->mapOneToOne([
            'targetEntity'  => $this->metadataClass,
            'fieldName'     => 'seoMetadata',
            'cascade'       => ['persist'],
            'joinColumn'    => [
                'name'                  => 'seo_metadata_id',
                'referencedColumnName'  => 'id',
                'nullable'  => true,
                'onDelete'  => 'CASCADE',
            ],
            'joinColumns'    => [
                [
                    'name'                  => 'seo_metadata_id',
                    'referencedColumnName'  => 'id',
                    'nullable'  => true,
                    'onDelete'  => 'CASCADE',
                ]
            ],
        ]);
    }
}
