<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Omni\Sylius\SeoPlugin\Templating;

use Omni\Sylius\SeoPlugin\SeoPage;

class SeoEngine
{
    /**
     * @var string
     */
    private $encoding;

    /**
     * @var SeoPage
     */
    private $seoPage;

    /**
     * @param string $encoding
     * @param SeoPage $seoPage
     */
    public function __construct(string $encoding, SeoPage $seoPage)
    {
        $this->encoding = $encoding;
        $this->seoPage = $seoPage;
    }

    /**
     * @return string
     */
    public function renderHeadAttributes(): string
    {
        $headAttributes = '';

        foreach ($this->seoPage->getHeadAttributes() as $name => $value) {
            $headAttributes .= sprintf('%s="%s" ', $name, $value);
        }

        return rtrim($headAttributes);
    }

    /**
     * @return string
     */
    public function renderHtmlAttributes(): string
    {
        $htmlAttributes = '';

        foreach ($this->seoPage->getHtmlAttributes() as $name => $value) {
            $htmlAttributes .= sprintf('%s="%s" ', $name, $value);
        }

        return rtrim($htmlAttributes);
    }

    /**
     * @return string
     */
    public function renderLangAlternates(): string
    {
        $langAlternatives = '';

        foreach ($this->seoPage->getLangAlternates() as $href => $hrefLang) {
            $langAlternatives .= sprintf("<link rel=\"alternate\" href=\"%s\" hreflang=\"%s\"/>\n", $href, $hrefLang);
        }

        return $langAlternatives;
    }

    /**
     * @return string
     */
    public function renderLinkCanonical(): string
    {
        if ($this->seoPage->getLinkCanonical()) {
            return sprintf("<link rel=\"canonical\" href=\"%s\"/>\n", $this->seoPage->getLinkCanonical());
        }

        return '';
    }

    /**
     * @return string
     */
    public function renderLinks(): string
    {
        $links = '';

        foreach ($this->seoPage->getLinks() as $rel => $href) {
            $links .= sprintf("<link rel=\"%s\" href=\"%s\"/>\n", $rel, $href);
        }

        return $links;
    }

    /**
     * @return string
     */
    public function renderMeta(): string
    {
        $meta = '';

        foreach ($this->seoPage->getMetas() as $type => $metadatas) {
            foreach ((array) $metadatas as $name => $metadata) {
                list($content, $extras) = $metadata;

                $name = $this->normalize($name);

                if (false === empty($content)) {
                    $meta .= sprintf("<meta %s=\"%s\" content=\"%s\">\n", $type, $name, $this->normalize($content));
                } else {
                    $meta .= sprintf("<meta %s=\"%s\">\n", $type, $name);
                }
            }
        }

        return $meta;
    }

    /**
     * @return string
     */
    public function renderTitle(): string
    {
        return sprintf("<title>%s</title>\n", strip_tags($this->seoPage->getTitle()));
    }

    /**
     * @param string $string
     * @return string
     */
    private function normalize(string $string): string
    {
        return str_replace("'", '', str_replace('"', '', strip_tags($string)));
    }
}
