<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Manager;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use Omni\Sylius\ShippingPlugin\Services\DownloadPdfLabelInterface;

class LabelPdfDownloadManager
{
    /**
     * @var array
     */
    private $downloadLabelProviders;

    /**
     * @param DownloadPdfLabelInterface $downloadPdfLabel
     */
    public function addDownloadLabel(DownloadPdfLabelInterface $downloadPdfLabel)
    {
        $this->downloadLabelProviders[] = $downloadPdfLabel;
    }

    /**
     * @param ShippingExportInterface $shippingExport
     * @param array                   $parcelNumbers
     *
     * @return string|null
     */
    public function downloadPdfLabel(ShippingExportInterface $shippingExport, array $parcelNumbers): ?string
    {
        /** @var DownloadPdfLabelInterface $downloadLabelProvider */
        foreach ($this->downloadLabelProviders as $downloadLabelProvider) {
            if ($downloadLabelProvider->getShipperName() === $shippingExport->getShippingGateway()->getCode()) {
                return $downloadLabelProvider->download($shippingExport, $parcelNumbers);
            }
        }

        return null;
    }
}
