<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Services;

use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemUnit;
use Sylius\Component\Core\Model\Shipment;
use Sylius\Component\Core\Model\ShipmentInterface;

class PayOnDeliveryParser
{
    /**
     * @param OrderInterface $order
     * @return null|OrderInterface
     */
    public function processPayOnDelivery(OrderInterface $order): ?OrderInterface
    {
        if ($order->isEmpty() || !$this->isPayOnDelivery($order)) {
            return null;
        }

        /** @var ShipmentInterface $shipment */
        foreach ($order->getShipments() as $shipment) {
            $this->addPayOnDeliveryTotal($shipment);
        }

        // Add shipment payment to first shipment.
        /** @var Shipment $shipment */
        $shipment = $order->getShipments()->first();
        if ($shipment instanceof ShipmentInterface && $shipment->getShipmentTotal() > 0) {
            $adjustedShippingTotal = intval($shipment->getShipmentTotal()) + $order->getAdjustmentsTotal();
            $shipment->setShipmentTotal($adjustedShippingTotal);
        }

        return $order;
    }

    /**
     * @param OrderInterface $order
     * @return bool
     */
    public function isPayOnDelivery(OrderInterface $order): bool
    {
        $payment = $order->getPayments()->first();
        if (!$payment || !$payment->getMethod()) {
            return false;
        }

        return (strpos($payment->getMethod()->getCode(), 'cash_on_delivery') !== false);
    }

    /**
     * @param ShipmentInterface $shipment
     * @return ShipmentInterface
     */
    public function addPayOnDeliveryTotal(ShipmentInterface &$shipment)
    {
        $total = 0;
        /** @var OrderItemUnit[] $orderItems */
        $orderItems = $shipment->getUnits();
        foreach ($orderItems as $orderItem) {
            $total += $orderItem->getTotal();
        }

        $shipment->setShipmentTotal($total);

        return $shipment;
    }
}
