<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Services;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ShippingPlugin\Event\ExportShipmentsEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Translation\TranslatorInterface;

class ExportShipmentsEventFactory
{
    /** @var FlashBagInterface $session */
    private $flashBag;
    /** @var EntityManagerInterface */
    private $shippingExportManager;
    /** @var Filesystem */
    private $filesystem;
    /** @var TranslatorInterface */
    private $translator;
    /** @var string */
    private $shippingLabelsPath;

    /**
     * ExportShipmentsEventFactory constructor.
     * @param FlashBagInterface $flashBag
     * @param EntityManagerInterface $shippingExportManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param Filesystem $filesystem
     * @param TranslatorInterface $translator
     * @param string $shippingLabelsPath
     */
    public function __construct(
        FlashBagInterface $flashBag,
        EntityManagerInterface $shippingExportManager,
        Filesystem $filesystem,
        TranslatorInterface $translator,
        string $shippingLabelsPath
    ) {
        $this->flashBag = $flashBag;
        $this->shippingExportManager = $shippingExportManager;
        $this->filesystem = $filesystem;
        $this->translator = $translator;
        $this->shippingLabelsPath = $shippingLabelsPath;
    }


    public function createShipmentExportsEvent(array $shipmentExports = [])
    {
        return new ExportShipmentsEvent(
            $shipmentExports,
            $this->flashBag,
            $this->shippingExportManager,
            $this->filesystem,
            $this->translator,
            $this->shippingLabelsPath
        );
    }
}
