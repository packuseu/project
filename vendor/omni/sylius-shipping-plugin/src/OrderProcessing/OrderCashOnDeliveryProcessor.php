<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\OrderProcessing;

use AppBundle\Entity\Shipment;
use Omni\Sylius\ShippingPlugin\Services\PayOnDeliveryParser;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\OrderItemUnit;
use Sylius\Component\Core\Model\ShipmentInterface;
use Sylius\Component\Core\OrderPaymentStates;
use Sylius\Component\Order\Model\OrderInterface as BaseOrderInterface;
use Sylius\Component\Order\Processor\OrderProcessorInterface;
use Webmozart\Assert\Assert;

class OrderCashOnDeliveryProcessor implements OrderProcessorInterface
{
    /**
     * @var PayOnDeliveryParser
     */
    private $payOnDeliveryParser;

    /**
     * OrderCashOnDeliveryProcessor constructor.
     * @param PayOnDeliveryParser $payOnDeliveryParser
     */
    public function __construct(PayOnDeliveryParser $payOnDeliveryParser)
    {
        $this->payOnDeliveryParser = $payOnDeliveryParser;
    }


    /**
     * {@inheritdoc}
     */
    public function process(BaseOrderInterface $order): void
    {
        /** @var OrderInterface $order */
        Assert::isInstanceOf($order, OrderInterface::class);

        if ($order->isEmpty() || !$this->payOnDeliveryParser->isPayOnDelivery($order)) {
            return;
        }

        $this->payOnDeliveryParser->processPayOnDelivery($order);
    }
}
