<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Provider;

use Doctrine\ORM\EntityManagerInterface;

interface CredentialProviderInterface
{
    /**
     * @param int $senderId
     * @param string $shippingGatewayCode
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCredentials(int $senderId, string $shippingGatewayCode): ?array;

    /**
     * @param int $shipperConfigId
     * @param string $shippingGatewayCode
     * @param array $configParams
     */
    public function updateCredentials(int $shipperConfigId, string $shippingGatewayCode, array $configParams): void;
}
