<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Entity;

use Omni\Sylius\ShippingPlugin\Model\ShipperConfigInterface;
use Sylius\Component\Resource\Model\ResourceInterface;

class ShipperConfig implements ShipperConfigInterface, ResourceInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * Get sender relations
     * Override this parameter adding relations with your table
     */
    private $sender;

    /**
     * @var string
     */
    private $gateway;

    /**
     * @var array
     */
    private $config = [];

    /**
     * @return array
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config): void
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $sender
     */
    public function setSender($sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getGateway(): ?string
    {
        return $this->gateway;
    }

    /**
     * @param string $gateway
     */
    public function setGateway(?string $gateway): void
    {
        $this->gateway = $gateway;
    }
}
