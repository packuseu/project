<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Form\Extension;

use BitBag\SyliusShippingExportPlugin\Context\ShippingGatewayContextInterface;
use BitBag\SyliusShippingExportPlugin\Form\Type\ShippingGatewayType;
use BitBag\SyliusShippingExportPlugin\Repository\ShippingGatewayRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;

class ShippingGatewayTypeExtension extends AbstractTypeExtension
{
    /** @var string */
    protected $shippingMethodModelClass;

    /** @var ShippingGatewayContextInterface */
    protected $shippingGatewayTypeContext;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var RequestStack */
    protected $requestStack;

    /**
     * @param string $shippingMethodModelClass
     * @param ShippingGatewayContextInterface $shippingGatewayTypeContext
     * @param EntityManagerInterface $em
     * @param RequestStack $requestStack
     */
    public function __construct(
        string $shippingMethodModelClass,
        ShippingGatewayContextInterface $shippingGatewayTypeContext,
        EntityManagerInterface $em,
        RequestStack $requestStack
    ) {
        $this->shippingMethodModelClass = $shippingMethodModelClass;
        $this->shippingGatewayTypeContext = $shippingGatewayTypeContext;
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $controller = $this->requestStack->getCurrentRequest()->get('_controller');

        if (strpos($controller, 'createAction') !== false) {
            return;
        }

        $builder->remove('shippingMethods');
        // This field is removed, to be added again at the end of form.
        $builder->remove('config');

        $builder
            ->addEventListener(
                FormEvents::PRE_SET_DATA,
                function (FormEvent $event) {
                    $shippingGatewayType = $this->shippingGatewayTypeContext->getFormType();
                    $form = $event->getForm();

                    $form
                        ->add(
                            'shippingMethods',
                            EntityType::class,
                            [
                                'label' => 'sylius.ui.shipping_methods',
                                'class' => $this->shippingMethodModelClass,
                                'placeholder' => 'bitbag.ui.choose_shipping_method',
                                'multiple' => true,
                                'data' => $this->getShippingMethodsData($event),
                            ]
                        )
                        ->add(
                            'config',
                            $shippingGatewayType,
                            [
                                'label' => false,
                                'auto_initialize' => false,
                            ]
                        );
                }
            );
    }

    /**
     * @return string
     */
    public function getExtendedType(): string
    {
        return ShippingGatewayType::class;
    }

    /**
     * @param FormEvent $event
     *
     * @return array
     * @throws ORMException
     */
    protected function getShippingMethodsData(FormEvent $event): array
    {
        $shippingMethodsData = [];

        $shippingMethods = $event->getData()->getShippingMethods();

        foreach ($shippingMethods as $shippingMethod) {
            $shippingMethodsData[] = $this->em->getReference(
                $this->shippingMethodModelClass,
                $shippingMethod->getId()
            );
        }

        return $shippingMethodsData;
    }
}
