<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Model\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Omni\Sylius\ShippingPlugin\Entity\ShipperConfig;

trait SenderShipperConfigAwareTrait
{
    /** @var Collection */
    protected $shipperConfigs;

    /**
     * @return Collection
     */
    public function getShipperConfigs(): Collection
    {
        return $this->shipperConfigs;
    }

    /**
     * @param ShipperConfig $shipperConfig
     */
    public function addShipperConfig(ShipperConfig $shipperConfig): void
    {
        $this->shipperConfigs->add($shipperConfig);
    }

    /**
     * @param ShipperConfig $shipperConfig
     */
    public function removeShipperConfig(ShipperConfig $shipperConfig): void
    {
        $shipperConfig->setSender(null);
        $this->shipperConfigs->removeElement($shipperConfig);
    }
}
