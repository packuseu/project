<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Model\Traits;

use Doctrine\ORM\Mapping as ORM;

trait ShippingShipperAwareTrait
{
    /**
     * @var string
     * @ORM\Column(name="shipper_status", nullable=true, type="string")
     */
    private $shipperStatus;

    /**
     * @var string
     * @ORM\Column(name="shipper_info", nullable=true, type="string")
     */
    private $shipperInfo;

    /**
     * @return string
     */
    public function getShipperStatus(): ?string
    {
        return $this->shipperStatus;
    }

    /**
     * @param string $shipperStatus
     */
    public function setShipperStatus(string $shipperStatus): void
    {
        $this->shipperStatus = $shipperStatus;
    }

    /**
     * @return string
     */
    public function getShipperInfo(): ?string
    {
        return $this->shipperInfo;
    }

    /**
     * @param string $shipperInfo
     */
    public function setShipperInfo(string $shipperInfo): void
    {
        $this->shipperInfo = empty($shipperInfo) ? '' : date('Y-m-d H:i:s') . '. ' . $shipperInfo;
    }
}
