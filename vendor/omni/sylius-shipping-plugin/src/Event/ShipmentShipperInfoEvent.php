<?php
/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Event;

use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Core\Model\OrderInterface;
use Sylius\Component\Core\Model\Shipment;

class ShipmentShipperInfoEvent
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param ExportShipmentsEvent $event
     */
    public function setShipperInfo(ExportShipmentsEvent $event): void
    {
        if (null !== $event->getException()) {
            foreach ($event->getShippingExports() as $shippingExport) {
                /** @var Shipment $shipment */
                $shipment = $shippingExport->getShipment();
                $shipment->setShipperInfo($event->getException()->getMessage());
                $this->em->persist($shipment);
            }

            $this->em->flush();
        }
    }
}
