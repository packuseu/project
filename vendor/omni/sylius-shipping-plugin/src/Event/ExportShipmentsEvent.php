<?php
/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ShippingPlugin\Event;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sylius\Component\Order\Model\OrderInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Exception;

class ExportShipmentsEvent extends Event
{
    public const EXPORT = 'omni.export_shipments';
    public const EXPORTED_TRANSITION = 'shipment_exported';

    /**
     * @var array|ShippingExportInterface[]
     */
    private $shippingExports = [];

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @var EntityManagerInterface|EntityManager
     */
    private $shippingExportManager;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var string
     */
    private $shippingLabelsPath;

    /**
     * @var Exception|null
     */
    private $exception;

    /**
     * @var OrderInterface|null
     */
    private $order;

    /**
     * ExportShipmentsEvent constructor.
     * @param array|ShippingExportInterface[] $shippingExports
     * @param FlashBagInterface $flashBag
     * @param EntityManager|EntityManagerInterface $shippingExportManager
     * @param Filesystem $filesystem
     * @param TranslatorInterface $translator
     * @param string $shippingLabelsPath
     */
    public function __construct(
        array $shippingExports,
        FlashBagInterface $flashBag,
        $shippingExportManager,
        Filesystem $filesystem,
        TranslatorInterface $translator,
        string $shippingLabelsPath
    ) {
        $this->shippingExports = $shippingExports;
        $this->flashBag = $flashBag;
        $this->shippingExportManager = $shippingExportManager;
        $this->filesystem = $filesystem;
        $this->translator = $translator;
        $this->shippingLabelsPath = $shippingLabelsPath;
    }


    /**
     * @return array|ShippingExportInterface[]
     */
    public function getShippingExports()
    {
        return $this->shippingExports;
    }

    /**
     * @param array|ShippingExportInterface[] $shippingExports
     */
    public function setShippingExports($shippingExports): void
    {
        $this->shippingExports = $shippingExports;
    }

    /**
     * @param string $messageId
     */
    public function addSuccessFlash(string $messageId = 'bitbag.ui.shipment_data_has_been_exported'): void
    {
        $message = $this->translator->trans($messageId);

        if (false === $this->flashBag->has('success')) {
            $this->flashBag->add('success', $message);
        }
    }

    /**
     * @param string $message
     */
    public function addErrorFlash(string $message): void
    {
        if (false === $this->flashBag->has('error')) {
            $this->flashBag->add('error', $message);
        }
    }

    /**
     * @param ShippingExportInterface $shippingExport
     * @param $labelContent
     * @param string $labelExtension
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveShippingLabel(ShippingExportInterface $shippingExport, $labelContent, string $labelExtension): void
    {
        $shipment = $shippingExport->getShipment();
        $orderNumber = str_replace('#', '', $shipment->getOrder()->getNumber());
        $shipmentId = $shipment->getId();
        $labelPath = $this->shippingLabelsPath
            . '/' . $shipmentId
            . '_' . $orderNumber
            . '.' . $labelExtension
        ;

        $this->filesystem->dumpFile($labelPath, $labelContent);
        $shippingExport->setLabelPath($labelPath);
        $this->shippingExportManager->flush($shippingExport);
    }

    public function exportShipment(ShippingExportInterface $shippingExport): void
    {
        $shippingExport->setState(ShippingExportInterface::STATE_EXPORTED);
        $shippingExport->setExportedAt(new \DateTime());

        $this->shippingExportManager->flush($shippingExport);
        $this->shippingExportManager->flush($shippingExport->getShipment());
    }

    public function persist(ShippingExportInterface $shippingExport)
    {
        if (!$this->shippingExportManager->contains($shippingExport)) {
            $this->shippingExportManager->persist($shippingExport);
            $this->shippingExportManager->flush($shippingExport);
        }
        if (!$this->shippingExportManager->contains($shippingExport->getShipment())) {
            $this->shippingExportManager->persist($shippingExport->getShipment());
            $this->shippingExportManager->flush($shippingExport->getShipment());
        }
    }

    /**
     * @return Exception|null
     */
    public function getException(): ?Exception
    {
        return $this->exception;
    }

    /**
     * @param Exception|null $exception
     */
    public function setException(?Exception $exception): void
    {
        $this->exception = $exception;
    }

    /**
     * @return null|OrderInterface
     */
    public function getOrder(): ?OrderInterface
    {
        return $this->order;
    }

    /**
     * @param null|OrderInterface $order
     */
    public function setOrder(?OrderInterface $order): void
    {
        $this->order = $order;
    }
}
