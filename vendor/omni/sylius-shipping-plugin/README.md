## Overview

This plugin alows you to integrate all shipping providers

Features:
* Shipping providers credentials inteface
* support for `omni/sylius-dpd-plugin`
* support for Venipak
* support for Omniva 
* shipment method pickup in checkout page

## Installation

```bash
$ composer require omni/sylius-shipping-plugin

```

Add plugin dependencies to `app/AppKernel.php`

```php
public function registerBundles()
{
    return array_merge(
        parent::registerBundles(),
        [
            new \BitBag\SyliusShippingExportPlugin\BitBagSyliusShippingExportPlugin(),
            ...
            new \Omni\Sylius\ShippingPlugin\OmniSyliusShippingPlugin(),
            
        ]
    );
}
```

Import configuration to `config.yml` or `package/omni.yml`

```yml
imports:
    ...
    - { resource: "@BitBagSyliusShippingExportPlugin/Resources/config/config.yml" }
    
    - { resource: "@OmniSyliusShippingPlugin/Resources/config/config.yml" }
```

Copy migration file from src/migrations to app/migrations

Then run command:

```
bin/console doctrine:migration:migrate
```

### Extend models

Make sure your application bundle extends `AbstractResourceBundle`.
Extended entity should look like this:

```php
<?php

namespace App\Entity\Shipment;

use Doctrine\ORM\Mapping as ORM;
use Omni\Sylius\ShippingPlugin\Model\Traits\ShippingExportAwareTrait;
use Omni\Sylius\ShippingPlugin\Model\Traits\ShippingPayOnDeliveryAwareTrait;
use Sylius\Component\Core\Model\Shipment as BaseShipment;

/**
 * @ORM\Entity
 * @ORM\Table(name="sylius_shipment")
 */
class Shipment extends BaseShipment
{
    use ShippingPayOnDeliveryAwareTrait;
    use ShippingShipperAwareTrait;
    use ShippingExportAwareTrait;
}
```

TODO: Create readme how integrate shipper credentials
