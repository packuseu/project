# Sylius Translator plugin

A plugin that allows editing Sylius UI texts and translations in admin. Translations are saved to DB.
Plugin extends Lexik Translation Bundle.

## Installation

#### Composer

    composer require omni/sylius-translator-plugin

#### Add the plugin to Kernel

```php
# bundles.php

public function registerBundles()
{
  $bundles = [
    // ...
    Omni\Sylius\TranslatorPlugin\OmniSyliusTranslatorPlugin::class => ['all' => true],
    Lexik\Bundle\TranslationBundle\LexikTranslationBundle::class => ['all' => true],
    // ...
  ];
}
```

#### Add configs

```yaml
# config/packages/lexik_translation.yaml

    lexik_translation:
        fallback_locale: [en]         # (required) default locale(s) to use
        managed_locales: [en, lt, lv] # (required) locales that the bundle has to manage
        grid_input_type: textarea
        base_layout: 'OmniSyliusTranslatorPlugin:Lexik:layout.html.twig'
```

```yaml
# config/packages/routes.yaml

omni_sylius_translation:
    resource: "@OmniSyliusTranslatorPlugin/Resources/config/routing.yml"
    prefix:   /admin/lexik
```

#### DB migration

    bin/console doctrine:migration:diff 
    bin/console doctrine:migration:migrate 


## Import translations

    bin/console lexik:translations:import
    
## Suggested command to run as step of deployment

    bin/console lexik:translations:import -p translations SyliusUiBundle -c
