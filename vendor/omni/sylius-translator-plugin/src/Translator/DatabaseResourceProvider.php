<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\TranslatorPlugin\Translator;

use Lexik\Bundle\TranslationBundle\EventDispatcher\Event\GetDatabaseResourcesEvent;
use Lexik\Bundle\TranslationBundle\Manager\LocaleManagerInterface;
use Lexik\Bundle\TranslationBundle\Translation\DatabaseFreshResource;
use Sylius\Bundle\ThemeBundle\Translation\Provider\Resource\TranslatorResourceProviderInterface;
use Symfony\Component\Config\ConfigCache;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class DatabaseResourceProvider implements TranslatorResourceProviderInterface
{
    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var bool
     */
    private $debug;

    /**
     * @var LocaleManagerInterface
     */
    private $localeManager;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param string $cacheDir
     * @param bool $debug
     * @param LocaleManagerInterface $localeManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        string $cacheDir,
        bool $debug,
        LocaleManagerInterface $localeManager,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->cacheDir = $cacheDir;
        $this->debug = $debug;
        $this->localeManager = $localeManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * {@inheritdoc}
     */
    public function getResourcesLocales(): array
    {
        return $this->localeManager->getLocales();
    }

    /**
     * {@inheritdoc}
     */
    public function getResources(): array
    {
        $file = sprintf('%s/database.resources.php', $this->cacheDir);
        $cache = new ConfigCache($file, $this->debug);

        if ($cache->isFresh()) {
            $resources = include $file;
        } else {
            $resources = $this->createResources($cache);
        }

        $syliusResources = [];
        foreach ($resources as $resource) {
            if (null === $resource['locale']) {
                continue;
            }

            $syliusResources[] = new TranslationResource('DB', $resource['locale'], 'database', $resource['domain']);
        }

        return $syliusResources;
    }

    /**
     * @param ConfigCache $cache
     *
     * @return array
     */
    private function createResources(ConfigCache $cache): array
    {
        $event = new GetDatabaseResourcesEvent();
        $this->eventDispatcher->dispatch(
            'lexik_translation.event.get_database_resources',
            $event
        );

        $resources = $event->getResources();
        $metadata = [];

        foreach ($resources as $resource) {
            $metadata[] = new DatabaseFreshResource($resource['locale'], $resource['domain']);
        }

        $content = sprintf('<?php return %s;', var_export($resources, true));
        $cache->write($content, $metadata);

        return $resources;
    }
}
