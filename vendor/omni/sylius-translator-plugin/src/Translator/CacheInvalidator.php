<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\TranslatorPlugin\Translator;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Finder\Finder;

class CacheInvalidator
{
    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param string $cacheDir
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(string $cacheDir, EventDispatcherInterface $eventDispatcher)
    {
        $this->cacheDir = $cacheDir;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Remove the cache file corresponding to the given locale.
     *
     * @param string $locale
     *
     * @return bool
     */
    public function removeCacheFile($locale): bool
    {
        $localeExploded = explode('_', $locale);
        $finder = new Finder();
        $finder->files()->in($this->cacheDir)->name(sprintf('/catalogue\.%s.*\.php$/', $localeExploded[0]));
        $deleted = true;

        foreach ($finder as $file) {
            $path = $file->getRealPath();
            $this->invalidateSystemCacheForFile($path);
            $deleted = unlink($path);

            $metadata = $path . '.meta';
            if (file_exists($metadata)) {
                $this->invalidateSystemCacheForFile($metadata);
                unlink($metadata);
            }
        }

        return $deleted;
    }

    /**
     * Remove the cache file corresponding to each given locale.
     *
     * @param array $locales
     */
    public function removeLocalesCacheFiles(array $locales): void
    {
        foreach ($locales as $locale) {
            $this->removeCacheFile($locale);
        }

        // also remove database.resources.php cache file
        $file = sprintf('%s/database.resources.php', $this->cacheDir);
        if (file_exists($file)) {
            $this->invalidateSystemCacheForFile($file);
            unlink($file);
        }

        $metadata = $file . '.meta';
        if (file_exists($metadata)) {
            $this->invalidateSystemCacheForFile($metadata);
            unlink($metadata);
        }

        $this->eventDispatcher->dispatch('lexik_translation.event.invalidate_locale_cache');
    }

    /**
     * @param string $path
     *
     * @throws \RuntimeException
     */
    protected function invalidateSystemCacheForFile($path): void
    {
        if (ini_get('apc.enabled') && \function_exists('apc_delete_file')) {
            if (apc_exists($path) && !apc_delete_file($path)) {
                throw new \RuntimeException(sprintf('Failed to clear APC Cache for file %s', $path));
            }
        } elseif ('cli' === PHP_SAPI ? ini_get('opcache.enable_cli') : ini_get('opcache.enable')) {
            if (\function_exists('opcache_invalidate') && !opcache_invalidate($path, true)) {
                throw new \RuntimeException(sprintf('Failed to clear OPCache for file %s', $path));
            }
        }
    }
}
