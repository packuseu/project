<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\TranslatorPlugin\EventListener;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;

class AdminMenuListener
{
    /**
     * @param MenuBuilderEvent $event
     */
    public function onAdminMenuItems(MenuBuilderEvent $event): void
    {
        $configuration = $event->getMenu()->getChild('configuration');

        $configuration
            ->addChild('lexik_translations', ['route' => 'lexik_translation_overview'])
            ->setLabel('sylius.ui.translations')
            ->setLabelAttribute('icon', 'globe');
    }
}
