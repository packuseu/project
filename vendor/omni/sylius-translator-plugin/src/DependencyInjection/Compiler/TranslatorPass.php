<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\TranslatorPlugin\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class TranslatorPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container): void
    {
//        if ($container->hasDefinition('lexik_translation.translator')) {
//            $container->removeDefinition('lexik_translation.translator');
//        }

        // Add database resource provider to provider list.
        if ($container->hasDefinition('sylius.theme.translation.resource_provider')) {
            $compositeProvider = $container->getDefinition('sylius.theme.translation.resource_provider');
            $providers = $compositeProvider->getArgument(0);
            $providers[] = new Reference('omni_sylius_translator.translator.resource_provider');
            $compositeProvider->replaceArgument(0, $providers);
        }
    }
}
