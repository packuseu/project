## Overview

This plugin allows you to integrate Manifest generation for your shipping providers.

Features:
* Generate PDF formatted manifest documents for shipments.
* Integrates without coupling shipping providers

## Installation

```bash
$ composer require omni/manifest-plugin
```

Add plugin dependencies to `app/AppKernel.php`

```php
public function registerBundles()
{
    return array_merge(
        parent::registerBundles(),
        [
            ...
            new Omni\Sylius\ManifestPlugin\OmniSyliusManifestPlugin(),
        ]
    );
}
```

Extend the shipment repository and add `ShipmentManifestAwareTrait` to it.

Add configuration:

```yaml
omni_manifest:
    pdf_path: /somepath # default: %kernel.project_dir%/var/manifests
    shipment_ready_for_courier_states: [a, b, c] # default: [ready]
    menu_item: false # default true (adds an item in the admin menu to access manifest list)
```

If you want to have full advantage of the plugin it is advisable to
have `omni/sylius-business-unit-plugin` installed as well. If you do,
be sure to extend the manifest model and add a Many to one ralation to
a business unit entity (use `BusinessUnitAwareTrait` from BU plugin).

Another thing is that when generating manifests in generators of the 
courier plugins, there will be a lot of additional information needed 
for the manifest generation. In order to handle it. You must implement
the methods that can be found in `ShippingUnitAwareTrait` depending on 
the logic of your project.  

If you keep `menu_item` set as true, you should add this in your project 
configuration:

```yaml
parameters:
    omni_sylius.model.manifest.class: Nfq\Omni\Shipping\Entity\Manifest

imports:
    - { resource: '@OmniSyliusManifestPlugin/Resources/config/app/config.yaml' }
```

This will add grids for the admin link to use.

## Usage

Run the following command in your console.

```bash
php bin/console shipment:manifest:generate
```

This command generates the manifests one business unit at a time,
thus it iterates through all available business units and checks if
its time to generate a manifest for them. 

When designing a new provider plugin, one should create a time checker
that would check this exact thing so that the generator for the provider
would be executed. This can be done by writing a service that implements
the `Omni\Sylius\ManifestPlugin\Generator\GenerationTimeCheckerInterface`
and tag it with the `omni_manifest.generator.time_checker` tag.  

You can extend how the manifest is generated by overriding the service 
`omni_sylius_manifest.shipping_export_collector` which fetches the 
ShippingExportInterface objects to generate the manifest from.

```
    omni_sylius_manifest.shipping_export_collector:
        class: Omni\Sylius\ManifestPlugin\Service\ShippingExportCollector
        arguments:
            - "@bitbag.repository.shipping_gateway"
            - "@bitbag.repository.shipping_export"
```
