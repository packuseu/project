<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Generator;

use Knp\Snappy\GeneratorInterface;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;
use Symfony\Component\Templating\EngineInterface;

class ManifestGenerator implements ManifestGeneratorInterface
{
    /**
     * @var PathGenerator
     */
    private $pathGenerator;

    /**
     * @var GeneratorInterface
     */
    private $pdfFileGenerator;

    /**
     * @var EngineInterface
     */
    private $templatingEngine;


    public function __construct(
        PathGenerator $pathGenerator,
        GeneratorInterface $pdfFileGenerator,
        EngineInterface $templatingEngine
    ) {
        $this->pathGenerator = $pathGenerator;
        $this->pdfFileGenerator = $pdfFileGenerator;
        $this->templatingEngine = $templatingEngine;
    }

    public function generate(ManifestInterface $manifest): string
    {
        $html = $this->templatingEngine->render(
            'OmniSyliusManifestPlugin::manifest.html.twig',
            [
                'manifest' => $manifest,
            ]
        );

        $path = $this->pathGenerator->generate($manifest->getTitle() ?? PathGenerator::FILE_PREFIX);
        $this->pdfFileGenerator->generateFromHtml($html, $path);

        return basename($path);
    }

    public function getShipperName(): string
    {
        return '';
    }
}
