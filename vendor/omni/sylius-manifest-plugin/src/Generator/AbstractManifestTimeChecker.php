<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Generator;

use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitInterface;
use Omni\Sylius\ShippingPlugin\Entity\BusinessUnitShipperConfig;

class AbstractManifestTimeChecker implements GenerationTimeCheckerInterface
{
    /**
     * @param BusinessUnitInterface $businessUnit
     * @return bool
     * @throws \Exception
     */
    public function isGenerationDue(BusinessUnitInterface $businessUnit): bool
    {
        return $this->getManifestTime($businessUnit) instanceof \DateTime &&
            (new \DateTime())->format('H:i') === $this->getManifestTime($businessUnit)->format('H:i');
    }

    /**
     * @param BusinessUnitInterface $businessUnit
     * @return \DateTime|null
     * @throws \Exception
     */
    public function getManifestTime(BusinessUnitInterface $businessUnit): ?\DateTime
    {
        /** @var BusinessUnitShipperConfig $shipperConfig */
        foreach ($businessUnit->getShipperConfigs() as $shipperConfig) {
            $time = $this->extractManifestDateTime($shipperConfig->getConfig());
            if ($time) {
                return $time;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getProviderCode(): string
    {
        return '';
    }

    protected function extractManifestDateTime(array $shipperConfig): ?\DateTime
    {
        if (isset($shipperConfig[$this->getProviderCode() . 'ManifestTime'])) {
            $manifestTime = null;
            $time = $shipperConfig[$this->getProviderCode() . 'ManifestTime'];

            if (is_int($time)) {
                $manifestTime = new \DateTime();
                $manifestTime->setTimestamp($time);
            } elseif (is_array($time)) {
                $hour = $time['hour'] ?? null;
                $minute = $time['minute'] ?? null;

                if ($hour !== null && $minute !== null) {
                    $manifestTime = new \DateTime(
                        sprintf(
                            '2000-10-10 %s:%s',
                            strlen($hour) == 1 ? '0' . $hour : $hour,
                            strlen($minute) == 1 ? '0' . $minute : $minute
                        )
                    );
                }
            }

            return $manifestTime;
        }
    }
}
