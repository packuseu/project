<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Generator;

class PathGenerator
{
    const FILE_PREFIX = 'manifest';
    private const FILE_EXTENSION = 'pdf';

    /**
     * @var string
     */
    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;
    }

    public function generate(string $prefix = self::FILE_PREFIX): string
    {
        $date = new \DateTime();
        $filename = sprintf(
            '%s-%s-%s.%s',
            $date->format('Y-m-d'),
            $prefix,
            rand(time() % 1000, time()),
            self::FILE_EXTENSION
        );

        return $this->getPath($filename);
    }

    public function getPath(string $filename)
    {
        return $this->path . DIRECTORY_SEPARATOR . $filename;
    }
}
