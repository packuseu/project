<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Generator;

use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitInterface;

class ManifestGenerationProviderPicker
{
    /**
     * @var GenerationTimeCheckerInterface[]
     */
    protected $checkers = [];

    /**
     * {@inheritdoc}
     */
    public function addChecker(GenerationTimeCheckerInterface $checker): void
    {
        $this->checkers[] = $checker;
    }

    /**
     * Returns an array with the provider codes whose manifest generation time is due
     *
     * @param BusinessUnitInterface $businessUnit
     *
     * @return array
     */
    public function getProvidersForManifestGeneration(BusinessUnitInterface $businessUnit): array
    {
        $providers = [];

        foreach ($this->checkers as $checker) {
            if ($checker->isGenerationDue($businessUnit)) {
                $provider = [
                    'code' => $checker->getProviderCode(),
                    'manifestTime' => $checker->getManifestTime($businessUnit),
                ];

                $providers[] = $provider;
            }
        }

        return $providers;
    }
}
