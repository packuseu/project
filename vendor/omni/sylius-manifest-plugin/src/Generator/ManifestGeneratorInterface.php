<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Generator;

use Omni\Sylius\ManifestPlugin\Exception\ManifestException;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;


interface ManifestGeneratorInterface
{
    /**
     * @param ManifestInterface $manifest
     *
     * @return string
     *
     * @throws ManifestException
     */
    public function generate(ManifestInterface $manifest): string;

    /**
     * @return string
     */
    public function getShipperName(): string;
}
