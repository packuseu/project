<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Controller;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use BitBag\SyliusShippingExportPlugin\Repository\ShippingExportRepositoryInterface;
use Omni\Sylius\ManifestPlugin\Doctrine\ORM\ManifestRepository;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGenerator;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGeneratorInterface;
use Omni\Sylius\ManifestPlugin\Generator\PathGenerator;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;
use Sylius\Bundle\ResourceBundle\Controller\ResourceController;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ManifestController extends ResourceController
{
    public function generateAction(string $code): Response
    {
        $shippingGateway = $this->getShippingMethodOr404($code);

        $repository = $this->getShippingExportRepository();

        $shippingExports = $repository->findBy([
            'shippingGateway' => $shippingGateway,
            'state' => ShippingExportInterface::STATE_EXPORTED,
        ]);

        if (empty($shippingExports)) {
            $this->addFlash(
                'warning',
                $this->get('translator')->trans('omni_sylius_manifest.flash.nothing_to_generate')
            );

            return $this->redirectToRoute('bitbag_admin_shipping_export_index');
        }

        /** @var ManifestInterface $manifest */
        $manifest = $this->getManifestFactory()->createNew();

        $manifest->setShippingGateway($shippingGateway);
        $manifest->setShippingExports($shippingExports);

        $path = $this->getManifestGenerator()->generate($manifest);

        $manifest->setPath($path);

        $em = $this->getDoctrine()->getManager();
        $em->persist($manifest);
        $em->flush();

        $this->addFlash(
            'success',
            $this->get('translator')->trans('omni_sylius_manifest.flash.generated_successfully')
        );

        return $this->redirectToRoute('bitbag_admin_shipping_export_index');
    }

    public function downloadAction(string $filename): Response
    {
        $filesystem = $this->get('filesystem');
        $pathGenerator = $this->get('omni_sylius_manifest.generator.path');
        $pathToManifest = $pathGenerator->getPath($filename);

        if (false === $filesystem->exists($pathToManifest)) {
            throw new NotFoundHttpException();
        }

        $response = new BinaryFileResponse($pathToManifest);
        $disposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($filename)
        );
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    private function getShippingExportRepository(): ShippingExportRepositoryInterface
    {
        return $this->get('bitbag.repository.shipping_export');
    }

    private function getManifestGenerator(): ManifestGeneratorInterface
    {
        return $this->get('omni_sylius_manifest.generator.manifest');
    }

    private function getManifestRepository(): ManifestRepository
    {
        return $this->get('omni_sylius_manifest.repository.manifest');
    }

    private function getManifestFactory(): FactoryInterface
    {
        return $this->get('omni_sylius_manifest.factory.manifest');
    }

    private function getPathGenerator(): PathGenerator
    {
        return $this->get('omni_sylius_manifest.generator.path');
    }

    private function getShippingMethodOr404(string $code): ShippingGatewayInterface
    {
        /** @var ShippingGatewayInterface $shippingGateway */
        $shippingGateway = $this->get('bitbag.repository.shipping_gateway')->findOneByCode($code);

        if (null === $shippingGateway) {
            throw new NotFoundHttpException('Shipping method with does not exist');
        }

        return $shippingGateway;
    }

    private function getLastManifest(ShippingGatewayInterface $shippingGateway): ?ManifestInterface
    {
        return $this->getManifestRepository()->findLastByShippingGateway($shippingGateway);
    }
}
