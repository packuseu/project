<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Menu;

use Sylius\Bundle\UiBundle\Menu\Event\MenuBuilderEvent;
use Knp\Menu\Util\MenuManipulator;

class ContentMenuBuilder
{
    /**
     * @var bool
     */
    protected $isMenuRequired;

    public function __construct(bool $isMenuRequired)
    {
        $this->isMenuRequired = $isMenuRequired;
    }

    /**
     * @param MenuBuilderEvent $event
     */
    public function configureMenu(MenuBuilderEvent $event): void
    {
        if (!$this->isMenuRequired) {
            return;
        }

        $configurationMenu = $event->getMenu()->getChild('sales');
        ;
        $configurationMenu
            ->addChild('manifests', ['route' => 'omni_sylius_admin_manifest_index'])
            ->setLabel('omni_sylius_manifest.ui.manifests')
            ->setLabelAttribute('icon', 'folder open outline')
        ;
    }
}
