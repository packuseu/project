<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Model\Traits;

use Sylius\Component\Core\Model\AddressInterface;

trait ShippingUnitAwareTrait
{
    /**
     * Parameter with relations table
     */
    private $sender;

    /**
     * @var AddressInterface
     */
    private $address;

    /**
     * @var string
     */
    private $publicName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var bool
     */
    private $isCallCourier;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime|null
     */
    private $callCourierDate;

    /**
     * @var string
     */
    private $workingHoursFrom;

    /**
     * @var string
     */
    private $workingHoursUntil;

    /**
     * @var int
     */
    private $palletsCount;

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $sender
     */
    public function setSender($sender): void
    {
        $this->sender = $sender;
    }

    /**
     * @return AddressInterface|null
     */
    public function getAddress(): ?AddressInterface
    {
        return $this->address;
    }

    /**
     * @return string|null
     */
    public function getPublicName(): ?string
    {
        return $this->publicName;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isCallCourier(): bool
    {
        return $this->isCallCourier;
    }

    /**
     * @param bool $isCallCourier
     */
    public function setIsCallCourier(bool $isCallCourier): void
    {
        $this->isCallCourier = $isCallCourier;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return \DateTime|null
     */
    public function getCallCourierDate(): ?\DateTime
    {
        return $this->callCourierDate;
    }

    /**
     * @param \DateTime|null $callCourierDate
     */
    public function setCallCourierDate(?\DateTime $callCourierDate): void
    {
        $this->callCourierDate = $callCourierDate;
    }

    /**
     * @return string
     */
    public function getWorkingHoursFrom(): string
    {
        return $this->workingHoursFrom;
    }

    /**
     * @param string $workingHoursFrom
     */
    public function setWorkingHoursFrom(string $workingHoursFrom): void
    {
        $this->workingHoursFrom = $workingHoursFrom;
    }

    /**
     * @return string
     */
    public function getWorkingHoursUntil(): string
    {
        return $this->workingHoursUntil;
    }

    /**
     * @param string $workingHoursUntil
     */
    public function setWorkingHoursUntil(string $workingHoursUntil): void
    {
        $this->workingHoursUntil = $workingHoursUntil;
    }

    /**
     * @return int
     */
    public function getPalletsCount(): int
    {
        return $this->palletsCount;
    }

    /**
     * @param int $palletsCount
     */
    public function setPalletsCount(int $palletsCount): void
    {
        $this->palletsCount = $palletsCount;
    }
}
