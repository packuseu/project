<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Model;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExportInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Doctrine\Common\Collections\Collection;
use Sylius\Component\Resource\Model\ResourceInterface;
use Sylius\Component\Resource\Model\TimestampableInterface;

interface ManifestInterface extends TimestampableInterface, ResourceInterface
{
    /**
     * @return string
     */
    public function getPath(): string;

    /**
     * @param string $path
     * @return ManifestInterface
     */
    public function setPath(string $path): ManifestInterface;

    /**
     * @return ShippingGatewayInterface
     */
    public function getShippingGateway(): ShippingGatewayInterface;

    /**
     * @param ShippingGatewayInterface $shippingGateway
     * @return ManifestInterface
     */
    public function setShippingGateway(ShippingGatewayInterface $shippingGateway): ManifestInterface;

    /**
     * @return Collection|ShippingExportInterface[]
     */
    public function getShippingExports(): Collection;

    /**
     * @param ShippingExportInterface $shippingExport
     * @return ManifestInterface
     */
    public function addShippingExport(ShippingExportInterface $shippingExport): ManifestInterface;

    /**
     * @param array $shippingExports
     * @return ManifestInterface
     */
    public function setShippingExports(array $shippingExports): ManifestInterface;
}
