<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Manager;

use Omni\Sylius\ManifestPlugin\Generator\ManifestGeneratorInterface;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;

class ManifestGeneratorManager
{
    /**
     * @var ManifestGeneratorInterface
     */
    protected $defaultManifestGenerator;

    /**
     * @param ManifestGeneratorInterface $defaultManifestGenerator
     */
    public function __construct(ManifestGeneratorInterface $defaultManifestGenerator)
    {
        $this->defaultManifestGenerator = $defaultManifestGenerator;
    }

    /**
     * @var array
     */
    private $manifestGenerators;
    /**
     * @param ManifestGeneratorInterface $manifestGenerator
     */
    public function addGenerator(ManifestGeneratorInterface $manifestGenerator)
    {
        $this->manifestGenerators[] = $manifestGenerator;
    }

    /**
     * @param ManifestInterface $manifest
     *
     * @return string
     *
     * @throws \Omni\Sylius\ManifestPlugin\Exception\ManifestException
     */
    public function generate(ManifestInterface $manifest): string
    {
        /** @var ManifestGeneratorInterface $manifestGenerator */
        foreach ($this->manifestGenerators as $manifestGenerator) {
            if ($manifestGenerator->getShipperName() === $manifest->getShippingGateway()->getCode()) {
                return $manifestGenerator->generate($manifest);
            }
        }

        return $this->defaultManifestGenerator->generate($manifest);
    }
}
