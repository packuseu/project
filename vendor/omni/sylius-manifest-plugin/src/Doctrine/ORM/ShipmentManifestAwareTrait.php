<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Doctrine\ORM;

use Doctrine\DBAL\Types\Type;
use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitInterface;

trait ShipmentManifestAwareTrait
{
    /**
     * @param array $shipmentStates
     * @param BusinessUnit $businessUnit
     * @param \DateTime $timeShouldBeClosed
     * @return mixed
     */
    public function findShipmentForManifest(
        array $shipmentStates,
        BusinessUnitInterface $businessUnit
    ) {
        $association = null;

        if (property_exists($this->getClassName(), 'businessUnit')) {
            $association = 'businessUnit';
        } elseif (property_exists($this->getClassName(), 'pickUpPoint')) {
            $association = 'pickUpPoint';
        }

        $qb = $this->createQueryBuilder('shipment');

        return $qb
            ->where($qb->expr()->in('shipment.state', ':states'))
            ->andWhere('shipment.' . $association . ' = :unit')
            ->setParameter(':unit', $businessUnit)
            ->setParameter(':states', $shipmentStates, Type::SIMPLE_ARRAY)
            ->getQuery()
            ->getResult();
    }
}
