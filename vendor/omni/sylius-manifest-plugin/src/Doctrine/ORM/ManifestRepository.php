<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Doctrine\ORM;

use BitBag\SyliusShippingExportPlugin\Entity\ShippingExport;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class ManifestRepository extends EntityRepository
{
    public function findLastByShippingGateway(ShippingGatewayInterface $shippingGateway): ?ManifestInterface
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->select(['m', 'se'])
            ->innerJoin('m.shippingExports', 'se')
            ->where(
                $qb->expr()->eq('m.shippingGateway', ':shipping_gateway')
            )
            ->orderBy('m.createdAt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('shipping_gateway', $shippingGateway);

        return $qb->getQuery()->getOneOrNullResult();
    }


    public function findManifestByExport(ShippingExport $export): ?ManifestInterface
    {
        $qb = $this->createQueryBuilder('m');

        $qb
            ->select(['m', 'se'])
            ->innerJoin('m.shippingExports', 'se')
            ->where(':export MEMBER OF m.shippingExports')
            ->orderBy('m.createdAt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('export', $export);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
