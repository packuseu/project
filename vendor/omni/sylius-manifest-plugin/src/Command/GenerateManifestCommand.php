<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\Command;

use Doctrine\ORM\EntityManagerInterface;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGenerationProviderPicker;
use Omni\Sylius\ManifestPlugin\Manager\ManifestGeneratorManager;
use Omni\Sylius\OrganizationBusinessUnitPlugin\Model\BusinessUnitInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Sylius\Component\Shipping\Model\ShipmentInterface;
use BitBag\SyliusShippingExportPlugin\Entity\ShippingGatewayInterface;
use Omni\Sylius\ManifestPlugin\Generator\ManifestGeneratorInterface;
use Omni\Sylius\ManifestPlugin\Model\Manifest;
use Sylius\Component\Resource\Factory\FactoryInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class GenerateManifestCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    protected static $defaultName = 'shipment:manifest:generate';

    /**
     * @var RepositoryInterface
     */
    protected $businessUnitRepository;

    /**
     * @var RepositoryInterface
     */
    protected $shippingGatewayRepository;

    /**
     * @var RepositoryInterface
     */
    protected $shipmentRepository;

    /**
     * @var RepositoryInterface
     */
    protected $manifestRepository;

    /**
     * @var ManifestGeneratorManager
     */
    protected $manifestGeneratorManager;

    /**
     * @var ManifestGenerationProviderPicker
     */
    protected $providerPicker;

    /**
     * @var FactoryInterface
     */
    protected $manifestFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var array
     */
    protected $shipmentsReadyForCourierStates;

    public function __construct(
        RepositoryInterface $businessUnitRepository,
        RepositoryInterface $shippingGatewayRepository,
        RepositoryInterface $shipmentRepository,
        RepositoryInterface $manifestRepository,
        ManifestGeneratorManager $manifestGeneratorManager,
        ManifestGenerationProviderPicker $providerPicker,
        FactoryInterface $manifestFactory,
        EntityManagerInterface $em,
        array $shipmentsReadyForCourierStates
    ) {
        parent::__construct();

        $this->businessUnitRepository = $businessUnitRepository;
        $this->shippingGatewayRepository = $shippingGatewayRepository;
        $this->shipmentRepository = $shipmentRepository;
        $this->manifestRepository = $manifestRepository;
        $this->manifestGeneratorManager = $manifestGeneratorManager;
        $this->providerPicker = $providerPicker;
        $this->manifestFactory = $manifestFactory;
        $this->em = $em;
        $this->shipmentsReadyForCourierStates = $shipmentsReadyForCourierStates;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $businessUnits = $this->businessUnitRepository->findAll();

        $current = new \DateTime();

        foreach ($businessUnits as $businessUnit) {
            foreach ($this->providerPicker->getProvidersForManifestGeneration($businessUnit) as $provider) {
                $generated = $this->generateManifest($businessUnit, $provider['code']);
                $output->writeln(sprintf('[%s] %s generated %s', $businessUnit->getErpCode(), $provider, $generated));
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function generateManifest(BusinessUnitInterface $businessUnit, string $gatewayCode) {
        $gateway = $this->shippingGatewayRepository->findOneBy(['code' => $gatewayCode]);
        $exports = $this->getShipmentExports($businessUnit, $gatewayCode);

        if (count($exports) > 0) {
            /** @var Manifest $manifest */
            $manifest = $this->manifestFactory->createNew();

            $manifest->setShippingGateway($gateway);
            $manifest->setShippingExports($exports);
            $manifest->setBusinessUnit($businessUnit);

            $path = $this->manifestGeneratorManager->generate($manifest);
            $manifest->setPath($path);
            $this->em->persist($manifest);
            $this->em->flush();
        }

        return count($exports);
    }

    /**
     * @param BusinessUnitInterface $businessUnit
     * @param string $gatewayCode
     *
     * @return array
     */
    protected function getShipmentExports(
        BusinessUnitInterface $businessUnit,
        string $gatewayCode
    ): array {
        $states = $this->shipmentsReadyForCourierStates;
        $shipments = $this->shipmentRepository->findShipmentForManifest($states, $businessUnit);
        $gatewayRepo = $this->shippingGatewayRepository;

        $exports = [];

        foreach ($shipments as $shipment) {
            $gateway = $gatewayRepo->findOneByShippingMethod($shipment->getMethod());

            if (!$gateway instanceof ShippingGatewayInterface
                || strpos($gateway->getCode(), $gatewayCode) === false
            ) {
                continue;
            }

            $export = $shipment->getShippingExport();

            if (!$export) {
                continue;
            }

            $manifest = $this->manifestRepository->findManifestByExport($export);

            if (!$manifest instanceof Manifest) {
                $exports[] = $export;
            }
        }

        return $exports;
    }
}
