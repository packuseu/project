<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\ManifestPlugin\DependencyInjection;

use Omni\Sylius\ManifestPlugin\Controller\ManifestController;
use Omni\Sylius\ManifestPlugin\Doctrine\ORM\ManifestRepository;
use Omni\Sylius\ManifestPlugin\Model\Manifest;
use Omni\Sylius\ManifestPlugin\Model\ManifestInterface;
use Sylius\Bundle\ResourceBundle\SyliusResourceBundle;
use Sylius\Component\Resource\Factory\Factory;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('omni_manifest');

        $rootNode
            ->children()
                ->scalarNode('pdf_path')->defaultValue('%kernel.project_dir%/var/manifests')->end()
                ->scalarNode('driver')->defaultValue(SyliusResourceBundle::DRIVER_DOCTRINE_ORM)->end()
                ->arrayNode('shipment_ready_for_courier_states')->prototype('scalar')->end()->end()
                ->booleanNode('menu_item')->defaultTrue()->end()
            ->end();

        $this->addResourcesSection($rootNode);

        return $treeBuilder;
    }

    private function addResourcesSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('resources')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('manifest')
                                ->addDefaultsIfNotSet()
                                    ->children()
                                        ->variableNode('options')->end()
                                        ->arrayNode('classes')
                                            ->addDefaultsIfNotSet()
                                            ->children()
                                                ->scalarNode('model')->defaultValue(Manifest::class)->cannotBeEmpty()
            ->end()
                                                ->scalarNode('interface')->defaultValue(ManifestInterface::class)
            ->cannotBeEmpty()->end()
                                                ->scalarNode('controller')->defaultValue(ManifestController::class)
            ->cannotBeEmpty()->end()
                                                ->scalarNode('repository')->defaultValue(ManifestRepository::class)
            ->cannotBeEmpty()->end()
                                                ->scalarNode('factory')->defaultValue(Factory::class)->end()
                                                ->scalarNode('form')->cannotBeEmpty()->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end();
    }
}
