<?php declare(strict_types=1);

namespace Sylius\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180910124401 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE omni_manifest (id INT AUTO_INCREMENT NOT NULL, shipping_gateway_id INT DEFAULT NULL, path VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D1698F05EF84DE5E (shipping_gateway_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE omni_manifest_shipping_exports (manifest_id INT NOT NULL, shipping_export_id INT NOT NULL, INDEX IDX_882596F7E697B2FB (manifest_id), UNIQUE INDEX UNIQ_882596F72238FEB5 (shipping_export_id), PRIMARY KEY(manifest_id, shipping_export_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE omni_manifest ADD CONSTRAINT FK_D1698F05EF84DE5E FOREIGN KEY (shipping_gateway_id) REFERENCES bitbag_shipping_gateway (id)');
        $this->addSql('ALTER TABLE omni_manifest_shipping_exports ADD CONSTRAINT FK_882596F7E697B2FB FOREIGN KEY (manifest_id) REFERENCES omni_manifest (id)');
        $this->addSql('ALTER TABLE omni_manifest_shipping_exports ADD CONSTRAINT FK_882596F72238FEB5 FOREIGN KEY (shipping_export_id) REFERENCES bitbag_shipping_export (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE omni_manifest_shipping_exports DROP FOREIGN KEY FK_882596F7E697B2FB');
        $this->addSql('DROP TABLE omni_manifest');
        $this->addSql('DROP TABLE omni_manifest_shipping_exports');
    }
}
