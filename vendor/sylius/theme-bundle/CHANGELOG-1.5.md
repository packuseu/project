## CHANGELOG FOR `1.5.x`

### v1.5.1 (2020-05-05)

- [#50](https://github.com/Sylius/SyliusThemeBundle/issues/50) Disable Twig services if templating is disabled ([@pamil](https://github.com/pamil))

### v1.5.0 (2019-10-10)

- [#29](https://github.com/Sylius/SyliusThemeBundle/issues/29) Ambiguous class resolution for symfony/contracts ([@Prometee](https://github.com/Prometee))
- [#30](https://github.com/Sylius/SyliusThemeBundle/issues/30) Support for Symfony 3.4 / 4.3+ ([@pamil](https://github.com/pamil))
- [#31](https://github.com/Sylius/SyliusThemeBundle/issues/31) Enforce coding standard ([@pamil](https://github.com/pamil))
- [#32](https://github.com/Sylius/SyliusThemeBundle/issues/32) Introduce Psalm ([@pamil](https://github.com/pamil))
